$.ajax({
    type: "GET",
    headers: { 'Access-Control-Allow-Origin': '*' },
    crossDomain: true,
    dataType: "json",
    url: "webresources/tools/build",
    success: function(data){
        $("#version").html(data.artifact_id+"."+data.version+"."+data.build_number);
        $("#build").html(data.build_date);
        $("#database_information").html(data.database_name+"@"+data.database_ip);
        $("#hostname").html(location.hostname+":"+location.port);
        $("#short_version").html(data.version);
        $("#nodetype").html(data.nodetype);
    }
});

$.ajax({
    type: "GET",
    headers: { 'Access-Control-Allow-Origin': '*' },
    crossDomain: true,
    dataType: "text",
    url: "webresources/tools/cacheStatus",
    success: function(data){
        $("#cacheStatus").html(data);
    }
});