package CustomClasses;


import javax.ws.rs.core.Response;
import java.util.ArrayList;
import java.util.List;

import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeParseException;

import static java.lang.Integer.parseInt;

/*
Author: Paul Crocker
This class is a utility class to validate input parameters to the end points
In case if mal formed parameters a custom exception is thrown
Where appropriate the methods can also return data in types suitable for use in constructing the sql queries
 */

public  class Validator {

    static private Response.Status badRequest = Response.Status.BAD_REQUEST;

    public Validator() {
        //This class should not be instanced
        //This assert is in case someone tries to instance this class :)
        throw new AssertionError();
    }

    public static Tuple2<Integer, Integer> ValidatePageEntries(String page, String perPage) throws  ValidatorException {
        int pageNumber = 0, perPageNumber = 50;
        if ( !page.isEmpty() )
        {
            try {
                pageNumber = Integer.parseInt(page);
                if (pageNumber < 0)
                    throw new ValidatorException(badRequest,"{ \"Invalid Request\":\"page number must be an integer >=0\" }");
            } catch (NumberFormatException ex) {
                throw new ValidatorException(badRequest,"{ \"Invalid Request\":\"page number must be an integer >=0\" }");
            }
            if (!perPage.isEmpty()) {
                try {
                    perPageNumber = Integer.parseInt(perPage);
                    if (perPageNumber < 1)
                        throw new ValidatorException(badRequest,"{ \"Invalid Request\":\"Number of records per page must be an integer >=1\" }");
                } catch (NumberFormatException ex) {
                    throw new ValidatorException(badRequest,"{ \"Invalid Request\":\"Number of records per page must be an integer >=1\" }");
                }
            }
        }
        return (new Tuple2<>(pageNumber,perPageNumber));
    }

    /*
        Simple Validator to check that the Marker value is after trimming exactly 9chars
        i.e - no Wildcards allowed
     */
    public static String ValidateMarker9(String markerIn) throws  ValidatorException{
        String marker = markerIn.trim();
        if (marker.length() != 9)
            throw new ValidatorException(badRequest,"{ \"Invalid Request\":\"marker must be 9chars\" }");
        return marker.toUpperCase();
    }

    /*
    Simple Validator to check that the Marker value is after trimming three por more characters
    This is for use in sql queries such as where "marker ilike narker
    */
    public static String ValidateMarkerMin3(String markerIn) throws  ValidatorException{
        String marker = markerIn.trim();
        if (marker.length() <3 )
            throw new ValidatorException(badRequest,"{ \"Invalid Request\":\"marker must be at least three characters\" }");
        return marker.toUpperCase();
    }

    /*
    Return an array list of integer status values.
     */
    public static List<Integer> ValidateStatus(String status) throws  ValidatorException{
        List<Integer> statusInts = new ArrayList<Integer>();
        try{
            String[] status_array = status.split(",");
            for (String s : status_array) {
                int stat = parseInt(s);
                if ((stat > 3) || (stat < -3)) {
                    String errMsg="status parameter not in the correct format. Use numeric values between -3 and 3, which can be separated by commas.";
                    throw new ValidatorException(badRequest, "{ \"Invalid Request\":\" "+ errMsg+ "\" }" );
                }
                statusInts.add( stat );
            }
            return statusInts;
        }catch(NumberFormatException e) {
            String errMsg="status parameter not in the correct format. Use numeric values between -3 and 3, which can be separated by commas.";
            throw new ValidatorException(badRequest, "{ \"Invalid Request\":\" "+ errMsg+ "\" }" );
        }
    }


    /*
        data validator that returns a LocalDateTime object
     */
    public static LocalDateTime ValidateLocalDateFormat( String dateIn ) throws ValidatorException {
        try{
            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-mm-dd");
            LocalDate parse = LocalDate.parse(dateIn);
            /*
             	atStartOfDay()
                Combines this date with the time of midnight to create a LocalDateTime at the start of this date.
                returns a LocalDateTime formed from this date at the time of midnight, 00:00, at the start of this date.
             */
            LocalDateTime localDateTime = parse.atStartOfDay();
            return localDateTime;
        }
        catch (DateTimeParseException e) {
            throw new ValidatorException(badRequest, "{ \"Invalid Request\":\""+dateIn+" The Date Format is YYYY-MM-DD\" } ");
        }
    }

    public static String ValidateSeveralDateFormats( String dateIn ) throws ValidatorException {
        try {
            dateIn=dateIn.trim();
            // Check if it's YYYYMMDD and add the -
            if (dateIn.length() == 8)  {
                    StringBuilder sb = new StringBuilder(dateIn);
                    sb.insert(4, '-').insert(7, '-');
                    dateIn = sb.toString();
            }
            // Check if the string actually parses to a LocalDate
            LocalDate.parse(dateIn);
            return dateIn;
        }
        catch (DateTimeParseException e) {
            throw new ValidatorException(badRequest, "{ \"Invalid Request\":\""+dateIn+" The Date Format is YYYY-MM-DD\" } ");
        }

    }
    public static int ValidateBadFileExcluder(String bad_files ) throws ValidatorException {
        try {
            return Integer.parseInt(bad_files);
        }
        catch (NumberFormatException e) {
            throw new ValidatorException(badRequest, "{ \"Invalid Request\":\""+bad_files+" :The optional Bad File Exclusion should be 1 to exclude files with bad status" +
                    " and 0 or any other valid integer to include these files\" } ");
        }

    }
    public static int calculateTotalPages(int totalElements, int perPage) {
        if(totalElements<=0)return 0;
        int totalCount = totalElements; // Total de registros
        int elementsPerPage = perPage; // Número de registros por página

        int totalPages = totalCount / elementsPerPage; // Divisão inteira
        if (totalCount % elementsPerPage != 0) {
            totalPages++; // Adiciona 1 se houver uma página com registros extras
        }
        return totalPages;
    }
}


