package CustomClasses;

/**
 * Created by arodrigues on 9/14/17.
 */
public class VelocityField {

    private String marker;
    private double lenghtvector;
    private double angle;

    public VelocityField(String marker, double lenghtvector, double angle) {
        this.marker = marker;
        this.lenghtvector = lenghtvector;
        this.angle = angle;
    }

    public VelocityField() {
    }


    public String getMarker() {
        return marker;
    }

    public void setMarker(String marker) {
        this.marker = marker;
    }

    public double getLenghtvector() {
        return lenghtvector;
    }

    public void setLenghtvector(double lenghtvector) {
        this.lenghtvector = lenghtvector;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }
}
