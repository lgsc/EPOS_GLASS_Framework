package CustomClasses;

public class FilesInfo {
    public String file_name;
    public String marker_long_name;
    public String[] errors;
    public String acronym;
    public int file_status;
    public String reference_date;

    public FilesInfo(String file_name, String marker_long_name, int file_status, String reference_date,String acronym) {
        this.file_name = file_name;
        this.marker_long_name = marker_long_name;
        this.file_status = file_status;
        this.reference_date = reference_date;
        this.acronym = acronym;
    }

    public void setErrors(String[] errors){
        this.errors = errors;
    }

    public String getFile_name() {
        return file_name;
    }

    public String getMarker_long_name() {
        return marker_long_name;
    }

    public String[] getErrors() {
        return errors;
    }

    public int getFile_status() {
        return file_status;
    }

    public String getReference_date() {
        return reference_date;
    }

    public String getAcronym() {
        return acronym;
    }

    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }
}
