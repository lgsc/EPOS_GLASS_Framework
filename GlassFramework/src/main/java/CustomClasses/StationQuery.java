package CustomClasses;


import java.util.ArrayList;
import java.util.Date;

//Created by José Manteigueiro on 31/10/2017 for V2 glass
public class StationQuery {
    private ArrayList<String> names = new ArrayList<>();
    private ArrayList<String> markers = new ArrayList<>();
    private ArrayList<String> markerlongnames = new ArrayList<>();
    private ArrayList<String> agencies = new ArrayList<>();
    private ArrayList<String> networks = new ArrayList<>();
    private ArrayList<String> antenna = new ArrayList<>();
    private ArrayList<String> receiver = new ArrayList<>();
    private ArrayList<String> radome = new ArrayList<>();
    private ArrayList<String> country = new ArrayList<>();
    private ArrayList<String> state = new ArrayList<>();
    private ArrayList<String> city = new ArrayList<>();
    private ArrayList<String> satellites = new ArrayList<>();
    private ArrayList<String> invertedNetworks = new ArrayList<>();
    private ArrayList<Integer> coordIDs = new ArrayList<>();
    private ArrayList<Integer> countryIDs = new ArrayList<>();
    private ArrayList<Integer> stateIDs = new ArrayList<>();
    private ArrayList<Integer> cityIDs = new ArrayList<>();

    private String stationType = "";
    private String date_to = "", date_from = "";
    private String coordinates = "";
    private String installedDateMin = "", installedDateMax = "", removedDateMin = "", removedDateMax = "";
    private double minLat = -999, minLon = -999, maxLat = -999, maxLon = -999, minAlt = -999, maxAlt = -999;
    private double centerLat = -999, centerLon = -999, radius = -999;
    private int lifetime = 0; // How many days was the station installed
    private Boolean fullV = false; // Full version or Short
    private Boolean showEpos = true; // Show EPOS stations
    private Boolean showNonEpos = true; // Show non EPOS stations

    public int getStationsWith() {
        return stationsWith;
    }

    public void setStationsWith(int stationsWith) {
        this.stationsWith = stationsWith;
    }

    private int stationsWith=0; // show dont care, 1 stns with rinex, 2 stns with products

    //T2
    private String t2DateStart = "", t2DateEnd = ""; // Date range
    private String t2SamplingFrequency = "", t2SamplingWindow = "";
    private String t2FileType = "";
    private String t2DataAvailability = "";
    private int t2minimumObservationDays = 0; // How many days have files
    private float t2minimumObservationYears = 0; // How many years of files
    private int t2statusfile = -10;
    
    //T3
    private ArrayList<String> t3ObsType = new ArrayList<>();
    private ArrayList<String> t3Frequency = new ArrayList<>();
    private ArrayList<String> t3Channel = new ArrayList<>();
    private ArrayList<String> t3Constellation = new ArrayList<>();
    private float t3Ratioepoch = 0;
    private float t3Elevangle = 0;
    private float t3Multipath = 0;
    private int t3Cycslips = 0;
    private int t3Clockjumps = 0;
    private float t3Spprms = 0;
    
    public int getT2statusfile() {
        return t2statusfile;
    }

    public void setT2statusfile(int t2statusfile) {
        this.t2statusfile = t2statusfile;
    }
    
    public ArrayList<String> getT3Observationtype() {
        return t3ObsType;
    }

    public void setT3Observationtype(ArrayList<String> t3ObsType) {
        this.t3ObsType = t3ObsType;
    }
    
    public ArrayList<String> getT3Frequency() {
        return t3Frequency;
    }

    public void setT3Frequency(ArrayList<String> t3Frequency) {
        this.t3Frequency = t3Frequency;
    }
    
    public ArrayList<String> getT3Channel() {
        return t3Channel;
    }

    public void setT3Channel(ArrayList<String> t3Channel) {
        this.t3Channel = t3Channel;
    }
    
    public ArrayList<String> getT3Constellation() {
        return t3Constellation;
    }

    public void setT3Constellation(ArrayList<String> t3Constellation) {
        this.t3Constellation = t3Constellation;
    }
    
    public float getT3Ratioepoch() {
        return t3Ratioepoch; 
    }
    
    public void setT3Ratioepoch(float t3Ratioepoch) {
        this.t3Ratioepoch = t3Ratioepoch;
    }
    
    public float getT3Elevangle() {
        return t3Elevangle;
    }
    
    public void setT3Elevangle(float t3Elevangle) {
        this.t3Elevangle = t3Elevangle;
    }
    
    public float getT3Multipathvalue() {
        return t3Multipath;
    }
    
    public void setT3Multipathvalue(float t3Multipath) {
        this.t3Multipath = t3Multipath;
    }
    
    public float getT3Spprms() {
        return t3Spprms;
    }
    
    public void setT3Spprms(float t3Spprms) {
        this.t3Spprms = t3Spprms;
    }
    
    public int getT3Nbcycleslips() {
        return t3Cycslips;
    }
    
    public void setT3Nbcycleslips(int t3Cycslips) {
        this.t3Cycslips = t3Cycslips;
    }
    
    public int getT3Nbclockjumps() {
        return t3Clockjumps;
    }
    
    public void setT3Nbclockjumps(int t3Clockjumps) {
        this.t3Clockjumps = t3Clockjumps;
    }
    
    public int getT2minimumObservationDays() {
        return t2minimumObservationDays;
    }

    public void setT2minimumObservationDays(int t2minimumObservationDays) {
        this.t2minimumObservationDays = t2minimumObservationDays;
    }

    public float getT2minimumObservationYears() {
        return t2minimumObservationYears;
    }

    public void setT2minimumObservationYears(float t2minimumObservationYears) {
        this.t2minimumObservationYears = t2minimumObservationYears;
    }

    public int getLifetime() {
        return lifetime;
    }

    public void setLifetime(int lifetime) {
        this.lifetime = lifetime;
    }

    public String getT2DateStart() {
        return t2DateStart;
    }

    public void setT2DateStart(String t2DateStart) {
        this.t2DateStart = t2DateStart;
    }

    public String getT2DateEnd() {
        return t2DateEnd;
    }

    public void setT2DateEnd(String t2DateEnd) {
        this.t2DateEnd = t2DateEnd;
    }

    public String getT2SamplingFrequency() {
        return t2SamplingFrequency;
    }

    public void setT2SamplingFrequency(String t2SamplingFrequency) {
        this.t2SamplingFrequency = t2SamplingFrequency;
    }

    public String getT2SamplingWindow() {
        return t2SamplingWindow;
    }

    public void setT2SamplingWindow(String t2SamplingWindow) {
        this.t2SamplingWindow = t2SamplingWindow;
    }

    public String getT2FileType() {
        return t2FileType;
    }

    public void setT2FileType(String t2FileType) {
        this.t2FileType = t2FileType;
    }

    public String getT2DataAvailability() {
        return t2DataAvailability;
    }

    public void setT2DataAvailability(String t2DataAvailability) {
        this.t2DataAvailability = t2DataAvailability;
    }

    public ArrayList<String> getInvertedNetworks() {
        return invertedNetworks;
    }

    public void setInvertedNetworks(ArrayList<String> inversedNetworks) {
        this.invertedNetworks = inversedNetworks;
    }

    public ArrayList<String> getSatellites() {
        return satellites;
    }

    public void setSatellites(ArrayList<String> satellites) {
        this.satellites = satellites;
    }

    public String getInstalledDateMin() {
        return installedDateMin;
    }

    public void setInstalledDateMin(String installedDateMin) {
        this.installedDateMin = installedDateMin;
    }

    public String getInstalledDateMax() {
        return installedDateMax;
    }

    public void setInstalledDateMax(String installedDateMax) {
        this.installedDateMax = installedDateMax;
    }

    public String getRemovedDateMin() {
        return removedDateMin;
    }

    public void setRemovedDateMin(String removedDateMin) {
        this.removedDateMin = removedDateMin;
    }

    public String getRemovedDateMax() {
        return removedDateMax;
    }

    public void setRemovedDateMax(String removedDateMax) {
        this.removedDateMax = removedDateMax;
    }

    public double getCenterLat() {
        return centerLat;
    }

    public void setCenterLat(double centerLat) {
        this.centerLat = centerLat;
    }

    public double getCenterLon() {
        return centerLon;
    }

    public void setCenterLon(double centerLon) {
        this.centerLon = centerLon;
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public void setMinLat(double minLat) {
        this.minLat = minLat;
    }

    public void setMinLon(double minLon) {
        this.minLon = minLon;
    }

    public void setMaxLat(double maxLat) {
        this.maxLat = maxLat;
    }

    public void setMaxLon(double maxLon) {
        this.maxLon = maxLon;
    }

    public double getMinAlt() {
        return minAlt;
    }

    public void setMinAlt(double minAlt) {
        this.minAlt = minAlt;
    }

    public double getMaxAlt() {
        return maxAlt;
    }

    public void setMaxAlt(double maxAlt) {
        this.maxAlt = maxAlt;
    }

    public double getMinLat() {
        return minLat;
    }

    public double getMaxLat() {
        return maxLat;
    }

    public double getMinLon() {
        return minLon;
    }

    public double getMaxLon() {
        return maxLon;
    }

    public String getCoordinates() {
        return coordinates;
    }

    public void setCoord(ArrayList<Integer> ids, String coord_type){
        this.coordIDs = ids;
        this.coordinates = coord_type;
    }

    public ArrayList<Integer> getCoordIDs() {
        return coordIDs;
    }

    public ArrayList<String> getRadome() {
        return radome;
    }

    public void setRadome(ArrayList<String> radome) {
        this.radome = radome;
    }

    public Boolean getFullV() {
        return fullV;
    }

    public void setFullV(Boolean fullV) {
        this.fullV = fullV;
    }

    public ArrayList<String> getAntenna() {
        return antenna;
    }

    public void setAntenna(ArrayList<String> antenna) {
        this.antenna = antenna;
    }

    public ArrayList<String> getReceiver() {
        return receiver;
    }

    public void setReceiver(ArrayList<String> receiver) {
        this.receiver = receiver;
    }

    public String getStationType() {
        return stationType;
    }

    public void setStationType(String stationType) {
        this.stationType = stationType;
    }

    public ArrayList<String> getNetworks() {
        return networks;
    }

    public void setNetworks(ArrayList<String> networks) {
        this.networks = networks;
    }

    public void setNetwork(String network) { this.networks.add(network); }

    public ArrayList<String> getAgencies() {
        return agencies;
    }

    public void setAgencies(ArrayList<String> agencies) {
        this.agencies = agencies;
    }

    public void setAgency(String agency) { this.agencies.add(agency); }

    public String getDate_to() {
        return date_to;
    }

    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }

    public String getDate_from() {
        return date_from;
    }

    public void setDate_from(String date_from) {
        this.date_from = date_from;
    }

    public ArrayList<String> getNames() {
        return names;
    }

    public void setNames(ArrayList<String> names) {
        this.names = names;
    }

    public ArrayList<String> getMarkers() {
        return markers;
    }

    public void setMarkers(ArrayList<String> markers) {
        this.markers = markers;
    }

    public ArrayList<String> getCountry() {
        return country;
    }

    public void setCountry(ArrayList<String> country) {
        this.country = country;
    }

    public ArrayList<String> getState() {
        return state;
    }

    public void setState(ArrayList<String> state) {
        this.state = state;
    }

    public ArrayList<String> getCity() {
        return city;
    }

    public void setCity(ArrayList<String> city) {
        this.city = city;
    }

    public ArrayList<Integer> getCountryIDs() {
        return countryIDs;
    }

    public void setCountryIDs(ArrayList<Integer> countryIDs) {
        this.countryIDs = countryIDs;
    }

    public ArrayList<Integer> getStateIDs() {
        return stateIDs;
    }

    public void setStateIDs(ArrayList<Integer> stateIDs) {
        this.stateIDs = stateIDs;
    }

    public ArrayList<Integer> getCityIDs() {
        return cityIDs;
    }

    public void setCityIDs(ArrayList<Integer> cityIDs) {
        this.cityIDs = cityIDs;
    }

    public Boolean getShowEpos() {
        return showEpos;
    }

    public void setShowEpos(Boolean showEpos) {
        this.showEpos = showEpos;
    }

    public Boolean getShowNonEpos() {
        return showNonEpos;
    }

    public void setShowNonEpos(Boolean showNonEpos) {
        this.showNonEpos = showNonEpos;
    }

    public ArrayList<String> getMarkerlongnames() {
        return markerlongnames;
    }

    public void setMarkerlongnames(ArrayList<String> markerlongnames) {
        this.markerlongnames = markerlongnames;
    }

    //Add to query ONE (or ALL) analysisCenter - the abbreviations p.ex INGV, SGO-EPND etc. ALL means all the AC's
    private String analysisCenter="ALL";
    public void setProductACAbr(String analysisCenter) {
        this.analysisCenter=analysisCenter;
    }
    public String getProductACAbr() {
        return analysisCenter;
    }
}
