package Configuration;

/**
 *
 * @author crocker
 * 
 * In this class database constants may be placed
 * 
 * Note that the DB Connection string needs the IP / PORT  / DBNAME
 * THE IP will be obtained from the IP where this server is running 
 * This is done in the SiteConfig method in this package
 * However we can also define it statically here - and this is maybe a better idea.
 * 
 * 
 */
public class DBConsts {

    static protected String DB_PORT = ""; // Port for the connection with EPOS database
    static protected String DB_NAME = ""; // Name of the EPOS databse

    static public String DB_USERNAME = ""; // Username for the EPOS database
    static public String DB_PASSWORD = ""; // Password for the EPOS database

    static public String GLASS_EMAIL_ADDRESS = "";
    //TODO: Password field protected?
    public static String GLASS_EMAIL_PASSWORD = "";
    static public String GLASS_EMAIL_SMTP_SERVER = "";
    static public String GLASS_EMAIL_PORT = "587";
    static public String GLASS_EMAIL_DEBUG = "false";

    final static public String ViewStn = "view_stations_with_products";


    DBConsts(){
        //This class should not be instanced
        //This assert is in case someone tries to instance this class :)
        throw new AssertionError();
    }
    
    
}
