package EposTables;

/**
* STATION_ITEM
* This class represents the station_item table of the EPOS database.
*/
public class Station_item extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the station_item table
    private int id_station;             // Id_station field of the station_item table
    private Item item;                  // Item field of the station_item table
    private String date_from;           // date_from field of the station_item table
    private String date_to;             // date_to field of the station_item table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * STATION_ITEM
    * This is the default constructor for the Station_item class.
    */
    public Station_item() {
        id = 0;
        id_station = 0;
        item = new Item();
        date_from = "";
        date_to = "";
    }

    /**
    * STATION_ITEM
    * This constructor for the Station_item class accepts values as parameters.
    */
    public Station_item(int id, int id_station, Item item, String date_from, String date_to) {
        this.id = id;
        this.id_station = id_station;
        this.item = item;
        this.date_from = date_from;
        this.date_to = date_to;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET ID_STATION
    * Returns the id_station value.
    */
    public int getId_station() {
        return id_station;
    }

    /**
    * SET ID_STATION
    * Sets the value for the id_station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /**
    * GET ITEM
    * Returns the item value.
    */
    public Item getItem() {
        return item;
    }

    /**
    * SET ITEM
    * Sets the values for the item.
    */
    public void setItem(Item item) {
        this.item = item;
    }

    /**
    * GET DATE_FROM
    * Returns the date_from value.
    */
    public String getDate_from() {
        return date_from;
    }

    /**
    * SET DATE_FROM
    * Sets the value for the date_from.
    */
    public void setDate_from(String date_from) {
        this.date_from = date_from;
    }

    /**
    * GET DATE_TO
    * Returns the date_to value.
    */
    public String getDate_to() {
        return date_to;
    }

    /**
    * SET DATE_TO
    * Sets the value for the date_to.
    */
    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }
}
