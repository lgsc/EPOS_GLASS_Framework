package EposTables;

/**
 * Created by arodrigues on 9/18/17.
 */
public class Processing_Parameters extends EposTablesMasterClass {

    private int id;
    private String sampling_period;
    private String sinex_version;
    private String otl_model;
    private String antena_model;
    private double cut_of_angle;
    private int id_helmet;

    public Processing_Parameters(int id, String sampling_period, String sinex_version, String otl_model, String antena_model, double cut_of_angle, int id_helmet) {
        this.id = id;
        this.sampling_period = sampling_period;
        this.sinex_version = sinex_version;
        this.otl_model = otl_model;
        this.antena_model = antena_model;
        this.cut_of_angle = cut_of_angle;
        this.id_helmet = id_helmet;
    }

    public Processing_Parameters() {
        this.id = id;
        this.sampling_period = sampling_period;
        this.sinex_version = sinex_version;
        this.otl_model = otl_model;
        this.antena_model = antena_model;
        this.cut_of_angle = cut_of_angle;
        this.id_helmet = id_helmet;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getSampling_period() {
        return sampling_period;
    }

    public void setSampling_period(String sampling_period) {
        this.sampling_period = sampling_period;
    }

    public String getSinex_version() {
        return sinex_version;
    }

    public void setSinex_version(String sinex_version) {
        this.sinex_version = sinex_version;
    }

    public String getOtl_model() {
        return otl_model;
    }

    public void setOtl_model(String otl_model) {
        this.otl_model = otl_model;
    }

    public String getAntena_model() {
        return antena_model;
    }

    public void setAntena_model(String antena_model) {
        this.antena_model = antena_model;
    }

    public double getCut_of_angle() {
        return cut_of_angle;
    }

    public void setCut_of_angle(double cut_of_angle) {
        this.cut_of_angle = cut_of_angle;
    }

    public int getId_helmet() {
        return id_helmet;
    }

    public void setId_helmet(int id_helmet) {
        this.id_helmet = id_helmet;
    }
}
