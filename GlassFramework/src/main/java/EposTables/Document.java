package EposTables;

import java.util.ArrayList;

/**
* DOCUMENT
* This class represents the document table of the EPOS database.
*/
public class Document extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                         // Id field of the document table
    private String date;                    // Date field of the document table
    private String title;                   // Title field of the document table
    private String description;             // Description field of the document table
    private String link;                    // Link field of the document table
    private int id_station;                 // Id_station field of the document table
    private Item item;                      // Item field of the document table
    private Document_type document_type;    // Document_type field of the document table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * DOCUMENT
    * This is the default constructor for the Document class.
    */
    public Document() {
        id = 0;
        date = "";
        title = "";
        description = "";
        link = "";
        id_station = 0;
        item = new Item();
        document_type = new Document_type();
    }

    /**
    * DOCUMENT
    * This constructor for the Document class accepts values as parameters.
    */
    public Document(int id, String date, String title, String description, String link, int id_station, Item item, Document_type document_type) {
        this.id = id;
        this.date = date;
        this.title = title;
        this.description = description;
        this.link = link;
        this.id_station = id_station;
        this.item = item;
        this.document_type = document_type;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET DATE
    * Returns the date value.
    */
    public String getDate() {
        return date;
    }

    /**
    * SET DATE
    * Sets the value for the date.
    */
    public void setDate(String date) {
        this.date = date;
    }

    /**
    * GET TITLE
    * Returns the title value.
    */
    public String getTitle() {
        return title;
    }

    /**
    * SET TITLE
    * Sets the value for the title.
    */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
    * GET DESCRIPTION
    * Returns the description value.
    */
    public String getDescription() {
        return description;
    }

    /**
    * SET DESCRIPTION
    * Sets the value for the description.
    */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * GET LINK
    * Returns the link value.
    */
    public String getLink() {
        return link;
    }

    /**
    * SET LINK
    * Sets the value for the link.
    */
    public void setLink(String link) {
        this.link = link;
    }

    /**
    * GET ID_STATION
    * Returns the id_station value.
    */
    public int getId_station() {
        return id_station;
    }

    /**
    * SET ID_STATION
    * Sets the value for the id_station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /**
    * GET ITEM
    * Returns the item value.
    */
    public Item getItem() {
        return item;
    }

    /**
    * SET ITEM
    * Sets the values for the item.
    */
    public void setItem(int id_item, Item_type item_type, 
            Contact contact_as_producer, Contact contact_as_owner, 
            String comment, ArrayList<Item_attribute> item_attributes) {
        this.item.setId(id_item);
        this.item.setItem_type(item_type.getId(), item_type.getName());
        this.item.setContact_as_producer(contact_as_producer.getId(),
                contact_as_producer.getName(), contact_as_producer.getTitle(), 
                contact_as_producer.getEmail(), contact_as_producer.getPhone(), 
                contact_as_producer.getGsm(), contact_as_producer.getComment(), 
                contact_as_producer.getAgency(), contact_as_producer.getRole());
        this.item.setContact_as_owner(contact_as_owner.getId(), 
                contact_as_owner.getName(), contact_as_owner.getTitle(), 
                contact_as_owner.getEmail(), contact_as_owner.getPhone(), 
                contact_as_owner.getGsm(), contact_as_owner.getComment(), 
                contact_as_owner.getAgency(), contact_as_owner.getRole());
        this.item.setComment(comment);
        for (int i=0; i<item_attributes.size(); i++)
        {
            this.item.AddItemAttribute(item_attributes.get(i));
        }
    }

    /**
    * GET DOCUMENT_TYPE
    * Returns the document_type value.
    */
    public Document_type getDocument_type() {
        return document_type;
    }

    /**
    * SET DOCUMENT_TYPE
    * Sets the values for the document_type.
    */
    public void setDocument_type(int id_document_type, String name) {
        this.document_type.setId(id_document_type);
        this.document_type.setName(name);
    }
}
