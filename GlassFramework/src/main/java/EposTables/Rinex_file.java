package EposTables;

/*
* RINEX FILE
* This class represents the rinex file table of the EPOS database.
*/
public class Rinex_file extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the rinex file table
    private String name;                // String field of the rinex file table
    private int id_station;             // Id station field of the rinex file table
    private String station_marker;      // Marker of the station
    private Data_center_structure data_center_structure;    // Data center_structure field of the rinex file table
    private int file_size;              // File size field of the rinex file table
    private File_type file_type;        // File type field of the rinex file table
    private String relative_path;       // Relative path field of the rinex file table
    private String reference_date;      // Reference date field of the rinex file table
    private String published_date;      // Published date field of the rinex file table
    private String creation_date;       // Creation date field of the rinex file table
    private String revision_date;       // Revision date field of the rinex file table
    private String md5checksum;         // MD5 checksum field of the rinex file table
    private String md5uncompressed;     // MD5 uncompressed field of the rinex file table
    private int status;                 // Status field of the rinex file table
    private QC_Report_Summary qc_report_summary;
    private Quality_file quality_file; 

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * RINEX_FILE
    * This is the default constructor for the Rinex_file class.
    */
    public Rinex_file() {
        id = 0;
        name = "";
        id_station = 0;
        station_marker = "";
        data_center_structure = new Data_center_structure();
        file_size = 0;
        file_type = new File_type();
        relative_path = "";
        reference_date = "";
        published_date = "";
        creation_date = "";
        revision_date = "";
        md5checksum = "";
        md5uncompressed = "";
        status = 0;
        qc_report_summary = new QC_Report_Summary();
        quality_file = new Quality_file();
    }

    /*
    * RINEX_FILE
    * This constructor for the Rinex_file class accepts values as parameters.
    */
    public Rinex_file(int id, String name, int id_station, String marker, int file_size, 
            String relative_path, String reference_date, String published_date, 
            String creation_date, String revision_date, String md5checksum,
            String md5uncompressed, int status) {
        this.id = id;
        this.name = name;
        this.id_station = id_station;
        this.station_marker = marker;
        this.data_center_structure = new Data_center_structure();
        this.file_size = file_size;
        this.file_type = new File_type();
        this.relative_path = relative_path;
        this.reference_date = reference_date;
        this.published_date = published_date;
        this.creation_date = creation_date;
        this.revision_date = revision_date;
        this.md5checksum = md5checksum;
        this.md5uncompressed = md5uncompressed;
        this.status = status;
        this.qc_report_summary = new QC_Report_Summary();
        this.quality_file = new Quality_file();
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /*
    * GET ID STATION
    * Returns the id station value.
    */
    public int getId_station() {
        return id_station;
    }

    /*
    * SET ID STATION
    * Sets the value for the id station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /*
    * GET DATA CENTER STRUCTURE
    * Returns the data center structure value.
    */
    public Data_center_structure getData_center_structure() {
        return data_center_structure;
    }

    /*
    * SET DATA CENTER STRUCTURE
    * Sets the value for the data center structure.
    */
    public void setData_center_structure(int id, String directory_naming) {
        this.data_center_structure.setId(id);
        this.data_center_structure.setDirectory_naming(directory_naming);
    }
    
    /*
    * SET DATA CENTER STRUCTURE
    * Sets the value for the data center structure.
    */
    public void setData_center_structure(int id, String directory_naming, Data_center data_center) {
        this.data_center_structure.setId(id);
        this.data_center_structure.setDirectory_naming(directory_naming);
        this.data_center_structure.setDataCenter(data_center);
    }

    /*
    * GET FILE SIZE
    * Returns the file size value.
    */
    public int getFile_size() {
        return file_size;
    }

    /*
    * SET FILE SIZE
    * Sets the value for the file size.
    */
    public void setFile_size(int file_size) {
        this.file_size = file_size;
    }

    /*
    * GET FILE TYPE
    * Returns the file type value.
    */
    public File_type getFile_type() {
        return file_type;
    }

    /*
    * SET FILE TYPE
    * Sets the value for the file type.
    */
    public void setFile_type(int id, String format, String samplig_window,
            String sampling_frequency) {
        this.file_type.setId(id);
        this.file_type.setFormat(format);
        this.file_type.setSampling_window(samplig_window);
        this.file_type.setSampling_frequency(sampling_frequency);
    }

    /*
    * GET RELATIVE PATH
    * Returns the relative path value.
    */
    public String getRelative_path() {
        return relative_path;
    }

    /*
    * SET RELATIVE PATH
    * Sets the value for the relative path.
    */
    public void setRelative_path(String relative_path) {
        this.relative_path = relative_path;
    }

    /*
    * GET REFERENCE DATE
    * Returns the reference date value.
    */
    public String getReference_date() {
        return reference_date;
    }

    /*
    * SET REFERENCE DATE
    * Sets the value for the reference date.
    */
    public void setReference_date(String reference_date) {
        this.reference_date = reference_date;
    }

    /*
    * GET PUBLISHED DATE
    * Returns the published date value.
    */
    public String getPublished_date() {
        return published_date;
    }

    /*
    * SET PUBLISHED DATE
    * Sets the value for the published date.
    */
    public void setPublished_date(String published_date) {
        this.published_date = published_date;
    }

    /*
    * GET CREATION DATE
    * Returns the creation date value.
    */
    public String getCreation_date() {
        return creation_date;
    }

    /*
    * SET CREATION DATE
    * Sets the value for the creation date.
    */
    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    /*
    * GET REVISION date
    * Returns the revision date value.
    */
    public String getRevision_date() {
        return revision_date;
    }

    /*
    * SET REVISION date
    * Sets the value for the revision date.
    */
    public void setRevision_date(String revision_date) {
        this.revision_date = revision_date;
    }

    /*
    * GET MD5 CHECKSUM
    * Returns the md5 checksum value.
    */
    public String getMd5checksum() {
        return md5checksum;
    }

    /*
    * SET MD5 CHECKSUM
    * Sets the value for the md5 checksum.
    */
    public void setMd5checksum(String md5checksum) {
        this.md5checksum = md5checksum;
    }

    /*
    * GET MD5 UNCOMPRESSED
    * Returns the md5 uncompressed value.
    */
    public String getMd5uncompressed() {
        return md5uncompressed;
    }

    /*
    * SET MD5 UNCOMPRESSED
    * Sets the value for the md5 uncompressed.
    */
    public void setMd5uncompressed(String md5uncompressed) {
        this.md5uncompressed = md5uncompressed;
    }

    /*
    * GET STATUS
    * Returns the status value.
    */
    public int getStatus() {
        return status;
    }

    /*
    * SET STATUS
    * Sets the value for the status.
    */
    public void setStatus(int status) {
        this.status = status;
    }

    /*
    * GET STATION MARKER
    * Returns the station marker.
    */
    public String getStation_marker() {
        return station_marker;
    }

     /*
    * SET STATION MARKER
    * Sets the value for the station marker.
    */
    public void setStation_marker(String station_marker) {
        this.station_marker = station_marker;
    }
    
    /*
    * GET QC_Report_Summary
    * Returns the QC_Report_Summary.
    */
    public QC_Report_Summary getQCReportSummary()
    {  
        return qc_report_summary;
    }
    
    
    /*
    * SET QC_Report_Summary
    * Sets the value for the QC_Report_Summary.
    */
    public void setQCReportSummary(QC_Report_Summary qc_report_summary)
    {  
        this.qc_report_summary = qc_report_summary;
    }
    
    /*
    * GET QUALITY FILE
    * Returns the Quality_file.
    */
    public Quality_file getQualityfile()
    {  
        return quality_file;
    }
    
    
    /*
    * SET QUALITY FILE
    * Sets the value for the Quality_file.
    */
    public void setQualityfile(Quality_file quality_file)
    {  
        this.quality_file = quality_file;
    }
}
