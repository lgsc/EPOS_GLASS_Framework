/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EposTables;

/**
 *
 * @author kngo
 */
public class QC_Observation_Summary_C extends EposTablesMasterClass {
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     
    private int id_qc_constellation_summary;                
    private Gnss_Obsnames gnss_obsnames;                        
    private int obs_sats;        
    private int obs_have;       
    private int obs_expt;      
    private int user_have;       
    private int user_expt;
    private Float cod_mpth;

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * QC_Observation_Summary_C
    * This is the default constructor for the QC_Observation_Summary_C class.
    */
    public QC_Observation_Summary_C() {
        id = 0;
        id_qc_constellation_summary = 0;
        gnss_obsnames = new Gnss_Obsnames();
        obs_sats = 0;
        obs_have = 0;
        obs_expt = 0;
        user_have = 0;
        user_expt = 0;
        cod_mpth = null;
    }

    /*
    * QC_Observation_Summary_C
    * This constructor for the QC_Observation_Summary_C class accepts values as parameters.
    */
    public QC_Observation_Summary_C(int id, int id_qc_constellation_summary, Gnss_Obsnames gnss_obsnames, int obs_sats, 
            int obs_have, int obs_expt, int user_have,int user_expt, Float cod_mpth)
    {
        this.id = id;
        this.id_qc_constellation_summary = id_qc_constellation_summary;
        this.gnss_obsnames = gnss_obsnames;
        this.obs_sats = obs_sats;
        this.obs_have = obs_have;
        this.obs_expt = obs_expt;
        this.user_have = user_have;
        this.user_expt = user_expt;
        this.cod_mpth = cod_mpth;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET ID QC CONSTELLATION SUMMARY
    * Returns the id_qc_constellation_summary value.
    */
    public int getIdQCConstellationSummary() {
        return id_qc_constellation_summary;
    }

    /*
    * SET ID QC CONSTELLATION SUMMARY
    * Sets the value for the id_qc_constellation_summary.
    */
    public void setIdQCConstellationSummary(int id_qc_constellation_summary) {
        this.id_qc_constellation_summary = id_qc_constellation_summary;
    }

    /*
    * GET GNSS OBSNAMES
    * Returns the gnss_obsnames value.
    */
    public Gnss_Obsnames getGnssObsnames() {
        return gnss_obsnames;
    }

    /*
    * SET ID GNSS OBSNAMES
    * Sets the value for the gnss_obsnames.
    */
    public void setGnssObsnames(Gnss_Obsnames gnss_obsnames) {
        this.gnss_obsnames = gnss_obsnames;
    }
    
    /*
    * GET OBS SATS
    * Returns the obs sats value.
    */
    public int getObsSats() {
        return obs_sats;
    }

    /*
    * SET OBS SATS
    * Sets the value for the obs sats.
    */
    public void setObsSats(int obs_sats) {
        this.obs_sats = obs_sats;
    }
    
    /*
    * GET OBS HAVE
    * Returns the obs have value.
    */
    public int getObsHave() {
        return obs_have;
    }

    /*
    * SET OBS HAVE
    * Sets the value for the obs have.
    */
    public void setObsHave(int obs_have) {
        this.obs_have = obs_have;
    }

    /*
    * GET OBS EXPT
    * Returns the obs expt value.
    */
    public int getObsExpt() {
        return obs_expt;
    }

    /*
    * SET OBS EXPT
    * Sets the value for the obs expt.
    */
    public void setObsExpt(int obs_expt) {
        this.obs_expt = obs_expt;
    }
    
    /*
    * GET USER HAVE
    * Returns the user have value.
    */
    public int getUserHave() {
        return user_have;
    }

    /*
    * SET USER HAVE
    * Sets the value for the user have.
    */
    public void setUserHave(int user_have) {
        this.user_have = user_have;
    }

    /*
    * GET USER EXPT
    * Returns the user expt value.
    */
    public int getUserExpt() {
        return user_expt;
    }

    /*
    * SET USER EXPT
    * Sets the value for the obs expt.
    */
    public void setUserExpt(int user_expt) {
        this.user_expt = user_expt;
    }
    
    /*
    * GET COD MPTH
    * Returns the cod mpth value.
    */
    public Float getCodMpth() {
        return cod_mpth;
    }

    /*
    * SET COD MPTH
    * Sets the value for the cod mpth.
    */
    public void setCodMpth(float cod_mpth) {
        this.cod_mpth = cod_mpth;
    }
}
