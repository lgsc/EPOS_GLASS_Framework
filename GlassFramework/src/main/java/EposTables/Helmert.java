package EposTables;

/*
* HELMERT
* This class represents the Helmert table of the EPOS product database.
*/
public class Helmert extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                         // Id field of the helmert table
    private double cx;                      // Cx field of the helmert table
    private double cy;                      // Cy field of the helmert table
    private double cz;                      // Cy field of the helmert table
    private double s;                       // S field of the helmert table
    private double rx;                      // Rx field of the helmert table
    private double ry;                      // Ry field of the helmert table
    private double rz;                      // Rz field of the helmert table
    private Reference_frame old_frame;      // Old frame field of the helmert table
    private Reference_frame new_frame;      // New frame field of the helmert table
    private Agency agency;                  // Agency field of the helmert table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * HELMET
    * This is the default constructor for the Hermet class.
    */
    public Helmert() {
        this.id = 0;
        this.cx = 0;
        this.cy = 0;
        this.cz = 0;
        this.s = 0;
        this.rx = 0;
        this.ry = 0;
        this.rz = 0;
        this.old_frame = new Reference_frame();
        this.new_frame = new Reference_frame();
        this.agency = new Agency();
    }

    /*
    * HELMERT
    * This constructor for the Hermert class accepts values as parameters.
    */
    public Helmert(int id, double cx, double cy, double cz, double s, double rx, 
            double ry, double rz) {
        this.id = id;
        this.cx = cx;
        this.cy = cy;
        this.cz = cz;
        this.s = s;
        this.rx = rx;
        this.ry = ry;
        this.rz = rz;
        this.old_frame = new Reference_frame();
        this.new_frame = new Reference_frame();
        this.agency = new Agency();
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET CX
    * Returns the cx value.
    */
    public double getCx() {
        return cx;
    }

    /*
    * SET CX
    * Sets the value for the cx.
    */
    public void setCx(double cx) {
        this.cx = cx;
    }

    /*
    * GET CY
    * Returns the cy value.
    */
    public double getCy() {
        return cy;
    }

    /*
    * SET CY
    * Sets the value for the cy.
    */
    public void setCy(double cy) {
        this.cy = cy;
    }

    /*
    * GET CZ
    * Returns the cz value.
    */
    public double getCz() {
        return cz;
    }

    /*
    * SET CZ
    * Sets the value for the cz.
    */
    public void setCz(double cz) {
        this.cz = cz;
    }

    /*
    * GET S
    * Returns the s value.
    */
    public double getS() {
        return s;
    }

    /*
    * SET S
    * Sets the value for the s.
    */
    public void setS(double s) {
        this.s = s;
    }

    /*
    * GET RX
    * Returns the rx value.
    */
    public double getRx() {
        return rx;
    }

    /*
    * SET RX
    * Sets the value for the rx.
    */
    public void setRx(double rx) {
        this.rx = rx;
    }

    /*
    * GET RY
    * Returns the ry value.
    */
    public double getRy() {
        return ry;
    }

    /*
    * SET RY
    * Sets the value for the ry.
    */
    public void setRy(double ry) {
        this.ry = ry;
    }

    /*
    * GET RZ
    * Returns the rz value.
    */
    public double getRz() {
        return rz;
    }

    /*
    * SET RZ
    * Sets the value for the rz.
    */
    public void setRz(double rz) {
        this.rz = rz;
    }

    /*
    * GET OLD FRAME
    * Returns the old frame value.
    */
    public Reference_frame getOld_frame() {
        return old_frame;
    }

    /*
    * SET OLD FRAME
    * Sets the value for the old frame.
    */
    public void setOld_frame(int id, String name, String epoch) {
        this.old_frame.setId(id);
        this.old_frame.setName(name);
        this.old_frame.setEpoch(epoch);
    }

    /*
    * GET NEW FRAME
    * Returns the new frame value.
    */
    public Reference_frame getNew_frame() {
        return new_frame;
    }

    /*
    * SET NEW FRAME
    * Sets the value for the new frame.
    */
    public void setNew_frame(int id, String name, String epoch) {
        this.new_frame.setId(id);
        this.new_frame.setName(name);
        this.new_frame.setEpoch(epoch);
    }

    /*
    * GET AGENCY
    * Returns the agency value.
    */
    public Agency getAgency() {
        return agency;
    }

    /*
    * SET AGENCY
    * Sets the value for the agency.
    */
    public void setAgency(int id, String name, String abbreviation, String address, 
            String www, String infos) {
        this.agency.setId(id);
        this.agency.setName(name);
        this.agency.setAbbreviation(abbreviation);
        this.agency.setAddress(address);
        this.agency.setWww(www);
        this.agency.setInfos(infos);
    }
}
