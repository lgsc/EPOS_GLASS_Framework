package EposTables;

/*
* REFERENCE FRAME
* This class represents the Reference Frame table of the EPOS product database.
*/
public class Reference_frame extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the reference frame table
    private String name;            // Name field of the reference frame table
    private String epoch;           // Epoch field of the reference frame table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * REFERENCE FRAME
    * This is the default constructor for the Reference Frame class.
    */
    public Reference_frame() {
    }

    /*
    * REFERENCE FRAME
    * This constructor for the Reference Frame class accepts values as parameters.
    */
    public Reference_frame(int id, String name, String epoch) {
        this.id = id;
        this.name = name;
        this.epoch = epoch;
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /*
    * GET EPOCH
    * Returns the epoch value.
    */
    public String getEpoch() {
        return epoch;
    }

    /*
    * SET EPOCH
    * Sets the value for the epoch.
    */
    public void setEpoch(String epoch) {
        this.epoch = epoch;
    }
}
