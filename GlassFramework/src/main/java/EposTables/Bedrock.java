package EposTables;

/**
* BEDROCK
* This class represents the bedrock table of the EPOS database.
*/
public class Bedrock extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                         // Id field of the bedrock table
    private String condition;               // Condition field of the bedrock table
    private String type;                    // Type field of the bedrock table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * BEDROCK
    * This is the default constructor for the Bedrock class.
    */
    public Bedrock() {
        id = 0;
        condition = "";
        type = "";
    }

    /**
    * BEDROCK
    * This constructor for the Bedrock class accepts values as parameters.
    */
    public Bedrock(int id, String condition, String type) {
        this.id = id;
        this.condition = condition;
        this.type = type;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET CONDITION
    * Returns the condition value.
    */
    public String getCondition() {
        return condition;
    }

    /**
    * SET CONDITION
    * Sets the value for the condition.
    */
    public void setCondition(String condition) {
        this.condition = condition;
    }

    /**
    * GET TYPE
    * Returns the type value.
    */
    public String getType() {
        return type;
    }

    /**
    * SET TYPE
    * Sets the value for the type.
    */
    public void setType(String type) {
        this.type = type;
    }
}
