package EposTables;

import java.util.Objects;

public class Queries {
    private int id;
    private String query;
    private String metaData;
    private Station station;
    private Node node;

    public Queries() {
    }

    public Queries(int id, String query, String metaData, Station station, Node node) {
        this.id = id;
        this.query = query;
        this.metaData = metaData;
        this.station = station;
        this.node = node;
    }


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getQuery() {
        return query;
    }

    public void setQuery(String query) {
        this.query = query;
    }

    public String getMetaData() {
        return metaData;
    }

    public void setMetaData(String metaData) {
        this.metaData = metaData;
    }

    public Station getStation() {
        return station;
    }

    public void setStation(Station station) {
        this.station = station;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }
}
