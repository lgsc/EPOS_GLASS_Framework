package EposTables;

/*
* RINEX ERROR FILES
* This class represents the rinex_error_files table of the EPOS database.
*/
public class Rinex_error_types extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int  id;                // Id field of the rinex_error_files table
    private String error_type;      // Error type field of the rinex_error_files table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * RINEX_ERROR_FILES
    * This is the default constructor for the Rinex_error_types class.
    */
    public Rinex_error_types() {
        this.id = 0;
        this.error_type = "";
    }

    /*
    * RINEX_ERROR_FILES
    * This constructor for the Rinex_error_types class accepts values as parameters.
    */
    public Rinex_error_types(int id, String error_type) {
        this.id = id;
        this.error_type = error_type;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET ERROR TYPE
    * Returns the error type value.
    */
    public String getError_type() {
        return error_type;
    }

    /*
    * SET ERROR TYPE
    * Sets the value for the error type.
    */
    public void setError_type(String error_type) {
        this.error_type = error_type;
    }
    
    
    
    
}
