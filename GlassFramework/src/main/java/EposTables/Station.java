package EposTables;

import Glass.DataBaseT4Connection;

import java.util.ArrayList;
import java.util.HashMap;

import CustomClasses.VelocityByPlateAndAc;

/**
* STATION
* This class represents the station table of the EPOS database.
*/
public class Station extends EposTablesMasterClass  {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                                                     // Id field of the station table
    private String name;                                                // Name field of the station table
    private String marker;                                              // Marker field of the station table
    private String description;                                         // Description field of the station table
    private String date_from;                                           // Date_from field of the station table
    private String date_to;                                             // Date_to field of the station table
    private Station_type station_type;                                  // Station_type field of the station table
    private String comment;                                             // Comment field of the station table
    private Location location;                                          // Location field of the station table
    private Monument monument;                                          // Monument field of the station table
    private Geological geological;                                      // Geological field of the station table
    private String iers_domes;                                          // Iers_domes field of the station table
    private String cpd_num;                                             // Cpd_num field of the station table
    private int monument_num;                                            // monument_num field of the station table
    private int receiver_num;                                           // Receiver_num field of the station table
    private String country_code;                                        // Country_code field of the station table
    private ArrayList<Condition> conditions;                            // List of all conditons of the station
    private ArrayList<Local_ties> local_ties;                           // List of all local_ties of the station
    private ArrayList<Instrument_collocation> Instrument_collocation;   // List of all collocations_instrument of the station
    private ArrayList<User_group_station> user_group_station;           // List of all user_groups of the station
    private ArrayList<Log> logs;                                        // List of all station logs
    private ArrayList<Station_contact> station_contacts;               // List of all station contacts
    private ArrayList<Station_network> station_networks;                // List of all station networks
    private ArrayList<Station_item> station_items;                      // List of all items of the station
    private ArrayList<File_generated> files_generated;                  // List of all files generated by the station
    private ArrayList<Document> documents;                              // List of all documents of the station
    private ArrayList<Colocation_offset> colocation_offsets;            // List of all colocation offsets of the station
    private ArrayList<String> rinex_files;                              // List of all the date rinex files of the station        
    private Double angle;
    private Double vectorLength;
    private ArrayList<ArrayList<VelocityByPlateAndAc>> V_marker;
    private ArrayList<Analysis_Centers> PA_timeseries;
    private ArrayList<Analysis_Centers> PA_velocities;
    private ArrayList<Analysis_Centers> PA_coordinates;
    private ArrayList<Method_identification> MI_timeseries;
    private String markerLongName;
    private ArrayList<Analysis_Centers> power_spectral_density;

    private int nbrinexfiles;
    private String user_group_station_name;

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /***
     *
    * STATION
    * This is the default constructor for the Station class.
    */
    public Station() {
        id = 0;
        name = "";
        marker = "";
        markerLongName = "";
        description = "";
        date_from = "";
        date_to = "";
        station_type = new Station_type();
        comment = "";
        location = new Location();
        monument = new Monument();
        geological = new Geological();
        iers_domes = "";
        cpd_num = "";
        monument_num = 0;
        receiver_num = 0;
        country_code = "";
        user_group_station_name = "";

        rinex_files = new ArrayList<>();

        conditions = new ArrayList<>();
        local_ties = new ArrayList<>();
        Instrument_collocation = new ArrayList<>();
        user_group_station = new ArrayList<>();
        logs = new ArrayList<>();
        station_contacts = new ArrayList<>();
        station_networks = new ArrayList<>();
        station_items = new ArrayList<>();
        files_generated = new ArrayList<>();
        documents = new ArrayList<>();
        colocation_offsets = new ArrayList<>();

        angle = 0.0;
        vectorLength = 0.0;

        V_marker = new ArrayList<>();
        
        PA_timeseries = new ArrayList<>();
        PA_velocities = new ArrayList<>();
        PA_coordinates = new ArrayList<>();

        power_spectral_density = new ArrayList<>();


        nbrinexfiles = 0;
    }

    /***
    * STATION
    * This constructor for the Station class accepts values as parameters.
    * @param id ID
    * @param name Name
    * @param marker Marker
    * @param markerLongName MarkerLongName
    * @param description Description
    * @param date_from Date From
    * @param date_to Date To
    * @param comment Comment
    * @param iers_domes IERS Dome
    * @param cpd_num CPD Number
    * @param monument_num Antenna Number
    * @param receiver_num Receiver Number
    * @param country_code Country Code
    */
    public Station(int id, String name, String marker, String markerLongName, String description, String date_from, String date_to, String comment, String iers_domes, String cpd_num, int monument_num, int receiver_num, String country_code) {
        this.id = id;
        this.name = name;
        this.marker = marker;
        this.markerLongName = markerLongName;
        this.description = description;
        this.date_from = date_from;
        this.date_to = date_to;
        this.station_type = new Station_type();
        this.comment = comment;
        this.location = new Location();
        this.monument = new Monument();
        this.geological = new Geological();
        this.iers_domes = iers_domes;
        this.cpd_num = cpd_num;
        this.monument_num = monument_num;
        this.receiver_num = receiver_num;
        this.country_code = country_code;


        this.rinex_files = new ArrayList<>();
        this.conditions = new ArrayList<>();
        this.local_ties = new ArrayList<>();
        this.Instrument_collocation = new ArrayList<>();
        this.user_group_station = new ArrayList<>();
        this.logs = new ArrayList<>();
        this.station_contacts = new ArrayList<>();
        this.station_networks = new ArrayList<>();
        this.station_items = new ArrayList<>();
        this.files_generated = new ArrayList<>();
        this.documents = new ArrayList<>();
        this.colocation_offsets = new ArrayList<>();

        this.angle = 0.0;
        this.vectorLength = 0.0;

        this.V_marker = new ArrayList<>();

        PA_timeseries = new ArrayList<>();
        PA_velocities = new ArrayList<>();
        PA_coordinates = new ArrayList<>();

        power_spectral_density = new ArrayList<>();
        this.nbrinexfiles = 0;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /**
    * GET MARKER
    * Returns the marker value.
    */
    public String getMarker() {
        return marker;
    }

    /**
    * SET MARKER
    * Sets the value for the marker.
    */
    public void setMarker(String marker) {
        this.marker = marker;
    }

    /**
    * GET DESCRIPTION
    * Returns the description value.
    */
    public String getDescription() {
        return description;
    }

    /**
    * SET DESCRIPTION
    * Sets the value for the description.
    */
    public void setDescription(String description) {
        this.description = description;
    }

    /**
    * GET DATE_FROM
    * Returns the date_from value.
    */
    public String getDate_from() {
        return date_from;
    }

    /**
    * SET DATE_FROM
    * Sets the value for the date_from.
    */
    public void setDate_from(String date_from) {
        this.date_from = date_from;
    }

    /**
    * GET DATE_TO
    * Returns the date_to value.
    */
    public String getDate_to() {
        return date_to;
    }

    /**
    * SET DATE_TO
    * Sets the value for the date_to.
    */
    public void setDate_to(String date_to) {
        this.date_to = date_to;
    }

    /**
    * GET STATION_TYPE
    * Returns the station_type object.
    */
    public Station_type getStation_type() {
        return station_type;
    }

    /**
    * SET ID_STATION_TYPE
    * Sets the value for the id_station_type.
    */
    public void setStation_type(int id_station_type, String name, String type) {
        this.station_type.setId(id_station_type);
        this.station_type.setName(name);
        this.station_type.setType(type);
    }

    /**
    * GET COMMENT
    * Returns the comment value.
    */
    public String getComment() {
        return comment;
    }

    /**
    * SET COMMENT
    * Sets the value for the comment.
    */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
    * GET LOCATION
    * Returns the Location object.
    */
    public Location getLocation() {
        return location;
    }

    /**
    * SET ID_LOCATION
    * Sets the value for the id_location.
    */
    public void setLocation(Location location) {
        this.location = location;
    }

    /**
    * GET MONUMENT
    * Returns the Monument object.
    */
    public Monument getMonument() {
        return monument;
    }

    /**
    * SET MONUMENT
    * Sets the values for the monument.
            */
    public void setMonument(Monument monument) {
        this.monument = monument;
    }

    /**
    * GET GEOLOGICAL
    * Returns the geological object.
    */
    public Geological getGeological() {
        return geological;
    }

    /**
     * SET GEOLOGICAL
     * Sets the values for the geological.
     */
    public void setGeological(Geological geological) {
        this.geological = geological;
    }

    /**
    * GET IERS_DOMES
    * Returns the iers_domes value.
    */
    public String getIers_domes() {
        return iers_domes;
    }

    /**
    * SET IERS_DOMES
    * Sets the value for the iers_domes.
    */
    public void setIers_domes(String iers_domes) {
        this.iers_domes = iers_domes;
    }

    /**
    * GET CPD_NUM
    * Returns the cpd_num value.
    */
    public String getCpd_num() {
        return cpd_num;
    }

    /**
    * SET CPD_NUM
    * Sets the value for the cpd_num.
    */
    public void setCpd_num(String cpd_num) {
        this.cpd_num = cpd_num;
    }

    /**
    * GET MONUMENT_NUM
    * Returns the monument_num value.
    */
    public int getMonument_num() {
        return monument_num;
    }

    /**
    * SET MONUMENT_NUM
    * Sets the value for the monument_num.
    */
    public void setMonument_num(int monument_num) {
        this.monument_num = monument_num;
    }

    /**
    * GET RECEIVER_NUM
    * Returns the receiver_num value.
    */
    public int getReceiver_num() {
        return receiver_num;
    }

    /**
    * SET RECEIVER_NUM
    * Sets the value for the receiver_num.
    */
    public void setReceiver_num(int receiver_num) {
        this.receiver_num = receiver_num;
    }

    /**
    * GET COUNTRY_CODE
    * Returns the country_code value.
    */
    public String getCountry_code() {
        return country_code;
    }

    /**
    * SET COUNTRY_CODE
    * Sets the value for the country_code.
    */
    public void setCountry_code(String country_code) {
        this.country_code = country_code;
    }
    
    /**
    * GET CONDITIONS
    * Returns the list of conditions.
    */
    public ArrayList<Condition> getCondition() {
        return conditions;
    }
    
    /**
    * ADD CONDITION
    * Add a new condition to the list.
    */
    public void addCondition(Condition condition) {
        this.conditions.add(condition);
    }
    
    /**
    * GET LOCAL_TIES
    * Returns the list of local_ties.
    */
    public ArrayList<Local_ties> getLocalTies() {
        return local_ties;
    }
    
    /**
    * ADD LOCAL_TIES
    * Add a new local_ties to the list.
    */
    public void addLocalTie(Local_ties local_ties) {
        this.local_ties.add(local_ties);
    }
    
    /**
    * GET Instrument_collocation
    * Returns the list of Instrument_collocation.
    */
    public ArrayList<Instrument_collocation> getCollocationInstrument() {
        return Instrument_collocation;
    }
    
    /**
    * ADD Instrument_collocation
    * Add a new Instrument_collocation to the list.
    */
    public void addCollocationInstrument(Instrument_collocation Instrument_collocation) {
        this.Instrument_collocation.add(Instrument_collocation);
    }
    
    /**
    * GET USER_GROUP_STATION
    * Returns the list of user_group_station.
    */
    public ArrayList<User_group_station> getUserGroupStation() {
        return user_group_station;
    }
    
    /**
    * ADD USER_GROUP_STATION
    * Add a new user_group_station to the list.
    */
    public void addUserGroupStation(User_group_station user_group_station) {
        this.user_group_station.add(user_group_station);
    }
    
    /**
    * GET LOGS
    * Returns the list of logs.
    */
    public ArrayList<Log> getLogs() {
        return logs;
    }
    
    /**
    * ADD LOG
    * Add a new log to the list.
    */
    public void addLog(Log log) {
        this.logs.add(log);
    }
    
    /**
    * GET STATION_CONTACTS
    * Returns the list of station contacts.
    */
    public ArrayList<Station_contact> getStationContacts() {
        return station_contacts;
    }
    
    /**
    * ADD STATION_CONTACTS
    * Add a new station_contact to the list.
    */
    public void addStationConacts(Station_contact station_contact) {
        this.station_contacts.add(station_contact);
    }

    /**
     * ADD STATION_CONTACTS
     * Add a new station_contact to the list.
     */
    public void addStationContacts(Station_contact station_contact) {
        this.station_contacts.add(station_contact);
    }

    /**
    * GET STATION_NETWORKS
    * Returns the list of station networks.
    */
    public ArrayList<Station_network> getStationNetworks() {
        return station_networks;
    }
    
    /**
    * ADD STATION_NETWORK
    * Add a new station_network to the list.
    */
    public void addStationNetwork(Station_network station_network) {
        this.station_networks.add(station_network);
    }
    
    /**
    * GET STATION ITEMS
    * Returns the list of items.
    */
    public ArrayList<Station_item> getStationItems() {
        return station_items;
    }
    
    /**
    * ADD STATION ITEM
    * Add a new station item to the list.
    */
    public void addStationItem(Station_item station_item) {
        this.station_items.add(station_item);
    }
    
    /**
    * GET FILES GENERATED
    * Returns the list of generated files.
    */
    public ArrayList<File_generated> getFilesGenerated() {
        return files_generated;
    }
    
    /**
    * ADD FILE GENERATED
    * Add a new file generated to the list.
    */
    public void addFileGenerated(File_generated file_generated) {
        this.files_generated.add(file_generated);
    }
    
    /**
    * GET DOCUMENTS
    * Returns the list of documents.
    */
    public ArrayList<Document> getDocument() {
        return documents;
    }
    
    /**
    * ADD DOCUMENT
    * Add a new document to the list.
    */
    public void addDocument(Document document) {
        this.documents.add(document);
    }
    
    /**
    * GET STATION COLOCATION OFFSETS
    * Returns the list of station colocation offsets.
    */
    public ArrayList<Colocation_offset> getStationColocationOffsets() {
        return colocation_offsets;
    }
    
    /**
    * ADD STATION COLOCATION OFFSET
    * Add a new station colocation offset to the list.
    */
    public void addStationColocationOffset(Colocation_offset colocation_offset) {
        this.colocation_offsets.add(colocation_offset);
    }
    
    /**
    * GET RINEX FILES
    * Returns the list of date rinex files.
    */
    public ArrayList<String> getRinexFiles() {
        return rinex_files;
    }
    
    /**
    * ADD RINEX FILES
    * Add a date rinex files to the list.
    */
    public void addRinexFiles(String rinex_files) {
        this.rinex_files.clear();
        this.rinex_files.add(rinex_files);
    }
    
    /**
     * GET NB RINEX FILE
     * @return number of rinex files
     */
    public int getNbRinexFiles() {
        return nbrinexfiles;
    }

    /**
     * SET NB RINEX FILE
     * @param nbrinexfiles of rinex files
     */
    public void setNbRinexFiles(int nbrinexfiles) {
        this.nbrinexfiles = nbrinexfiles;
    }

    /**
     * GET VELOCITY FIELD ANGLE
     * @return angle of vector
     */
    public Double getAngle() {
        return angle;
    }

    /**
     * SET VELOCITY FIELD ANGLE
     * @param angle angle of vector
     */
    public void setAngle(Double angle) {
        this.angle = angle;
    }

    /**
     * GET VELOCITY FIELD VECTOR LENGTH
     * @return length of vector
     */
    public Double getVectorLength() {
        return vectorLength;
    }

    /**
     * SET VELOCITY FIELD VECTOR LENGTH
     * @param vectorLength length of vector
     */
    public void setVectorLength(Double vectorLength) {
        this.vectorLength = vectorLength;
    }

    /**
     *  GET processing agencies based on estimated coordinates
     * @return PA_timeseries agencies list
     */
    public ArrayList<Analysis_Centers> getPA_timeseries() {
        return PA_timeseries;
    }

    /**
     *  SET processing agencies based on estimated coordinates
     * @param PA_timeseries agencies list
     */
    public void setPA_timeseries(ArrayList<Analysis_Centers> PA_timeseries) {
        this.PA_timeseries = PA_timeseries;
    }

    /**
     *  GET processing agencies based on reference position velocities
     * @return PA_velocities agencies list
     */
    public ArrayList<Analysis_Centers> getPA_velocities() {
        return PA_velocities;
    }

    /**
     *  SET processing agencies based on reference position velocities
     * @param PA_velocities agencies list
     */
    public void setPA_velocities(ArrayList<Analysis_Centers> PA_velocities) {
        this.PA_velocities = PA_velocities;
    }

    /**
     *  GET processing agencies based on coordinates
     * @return PA_coordinates agencies list
     */
    public ArrayList<Analysis_Centers> getPA_coordinates() {
        return PA_coordinates;
    }



    /**
     *  SET processing agencies based on coordinates
     * @param PA_coordinates agencies list
     */


    public void setPA_coordinates(ArrayList<Analysis_Centers> PA_coordinates) {
        this.PA_coordinates = PA_coordinates;
    }


    public ArrayList<Analysis_Centers> getPower_spectral_density() {
        return power_spectral_density;
    }

    public void setPower_spectral_density(ArrayList<Analysis_Centers> power_spectral_density) {
        this.power_spectral_density = power_spectral_density;
    }
    public String getMarkerLongName() {
        return markerLongName;
    }

    public void setMarkerLongName(String markerLongName) {
        this.markerLongName = markerLongName;
    }

    /*public ArrayList<DataBaseT4Connection.Tuple> getStartAndEndEpochs() {
        return startAndEndEpochs;
    }

    public void setStartAndEndEpochs(ArrayList<DataBaseT4Connection.Tuple> startAndEndEpochs) {
        this.startAndEndEpochs = startAndEndEpochs;
    }*/

    public String getUserGroupStationName() {
        return user_group_station_name;
    }

    public void setUserGroupStationName(String user_group_station_name) {
        this.user_group_station_name = user_group_station_name;
    }

    public ArrayList<Method_identification> getMI_timeseries() {
        return MI_timeseries;
    }

    public void setMI_timeseries(ArrayList<Method_identification> MI_timeseries) {
        this.MI_timeseries = MI_timeseries;
    }

    public ArrayList<ArrayList<VelocityByPlateAndAc>> getV_marker() {
        return V_marker;
    }

    public void setV_marker(ArrayList<ArrayList<VelocityByPlateAndAc>> v_marker) {
        V_marker = v_marker;
    }

    
}
