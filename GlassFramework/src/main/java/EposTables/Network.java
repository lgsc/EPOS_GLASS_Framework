package EposTables;

/**
* NETWORK
* This class represents the network table of the EPOS database.
*/
public class Network extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the network table
    private String name;                // Name field of the network table
    private Contact contact;            // contact field of the network table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * NETWORK
    * This is the default constructor for the Network class.
    */
    public Network() {
        id = 0;
        name = "";
        contact = new Contact();
    }

    /**
    * NETWORK
    * This constructor for the Network class accepts values as parameters.
    */
    public Network(int id, String name, Contact contact) {
        this.id = id;
        this.name = name;
        this.contact = contact;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /**
    * GET CONTACT
    * Returns the contact object.
    */
    public Contact getContact() {
        return contact;
    }

    /**
    * SET CONTACT
    * Sets the values for the contact.
    */
    public void setContact(Contact contact) {
        this.contact = contact;
    }
}
