package EposTables;

/*
* METHOD IDENTIFICATION
* This class represents the Method Indentification table of the EPOS product database.
*/
public class Method_identification extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                                 // Id field of the method indentification table
    private Analysis_Centers analysis_center;       // Analysis Center field of the method indentification table
    private Reference_frame reference_frame;        // Reference Frame field of the method indentification table
    private String creation_date;
    private String doi;
    private String software;                      // Software field of the method indentification table
    private double version;

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * METHOD IDENTIFICATION
    * This is the default constructor for the Method Identification class.
    */
    public Method_identification() {
        this.analysis_center = new Analysis_Centers();
        this.reference_frame = new Reference_frame();
    }
    /*
    * METHOD IDENTIFICATION
    * This constructor for the Method Identification class accepts values as parameters.
    */

    public Method_identification(int id, Analysis_Centers analysis_center, Reference_frame reference_frame, String creation_date, String doi, String software, float version) {
        this.id = id;
        this.analysis_center = analysis_center;
        this.reference_frame = reference_frame;
        this.creation_date = creation_date;
        this.doi = doi;
        this.software = software;
        this.version = version;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET SOFTWARE
    * Returns the software value.
    */
    public String getSoftware() {
        return software;
    }

    /*
    * SET SOFTWARE
    * Sets the value for the software.
    */
    public void setSoftware(String name) {
        this.software = name;
    }

    /*
    * GET ANALYSIS_CENTER
    * Returns object of Analysis_center class.
    */
    public Analysis_Centers getAnalysis_center() {
        return analysis_center;
    }

    /*
    * SET ANALYSIS_CENTER
    * Sets the member value for the analysis_center class.
    */
    public void setAnalysis_center(int id, String name, String abbreviation, String contact,
            String email, String url) {
        this.analysis_center.setId(id);
        this.analysis_center.setName(name);
        this.analysis_center.setAbbreviation(abbreviation);
        this.analysis_center.setContact(contact);
        this.analysis_center.setEmail(email);
        this.analysis_center.setUrl(url);
    }

    public void setAnalysis_center(Analysis_Centers ac){
        this.analysis_center = ac;
    }

    /*
    * GET REFERENCE FRAME
    * Returns the reference frame value.
    */
    public Reference_frame getReference_frame() {
        return reference_frame;
    }

    /*
    * SET REFERENCE FRAME
    * Sets the value for the reference frame.
    */
    public void setReference_frame(int id, String name, String epoch) {
        this.reference_frame.setId(id);
        this.reference_frame.setName(name);
        this.reference_frame.setEpoch(epoch);
    }


    public void setReference_frame(Reference_frame reference_frame) {
        this.reference_frame = reference_frame;
    }

    public String getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(String creation_date) {
        this.creation_date = creation_date;
    }

    public String getDoi() {
        return doi;
    }

    public void setDoi(String doi) {
        this.doi = doi;
    }


    public void setVersion(double version) {this.version = version; }

    public double getVersion() { return version; }


}
