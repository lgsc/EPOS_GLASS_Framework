package EposTables;

/**
* LOCAL_TIES
* This class represents the local_ties table of the EPOS database.
*/
public class Local_ties extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the local_ties table
    private int id_station;             // Id_station field of the local_ties table
    private String name;                // Name field of the local_ties table
    private String usage;               // Usage field of the local_ties table
    private String cpd_num;             // Cpd_num field of the local_ties table
    private String iers_domes;          // IERS Domes field of the local_ties table
    private float dx;                   // Dx field of the local_ties table
    private float dy;                   // Dy field of the local_ties table
    private float dz;                   // Dz field of the local_ties table
    private float accuracy;             // Accuracy field of the local_ties table
    private String survey_method;       // Survey_method field of the local_ties table
    private String date_at;             // Date_at field of the local_ties table
    private String comment;             // Comment field of the local_ties table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * LOCAL_TIES
    * This is the default constructor for the Local_ties class.
    */
    public Local_ties() {
        id = 0;
        id_station = 0;
        name = "";
        usage = "";
        cpd_num = "";
        iers_domes = "";
        dx = 0;
        dy = 0;
        dz = 0;
        accuracy = 0;
        survey_method = "";
        date_at = "";
        comment = "";
    }

    /**
    * LOCAL_TIES
    * This constructor for the Local_ties class accepts values as parameters.
    */
    public Local_ties(int id, int id_station, String name, String usage,
                      String cpd_num, String iers_domes, float dx, float dy, float dz, float accuracy,
                      String survey_method, String date_at, String comment) {
        this.id = id;
        this.id_station = id_station;
        this.name = name;
        this.usage = usage;
        this.cpd_num = cpd_num;
        this.iers_domes = iers_domes;
        this.dx = dx;
        this.dy = dy;
        this.dz = dz;
        this.accuracy = accuracy;
        this.survey_method = survey_method;
        this.date_at = date_at;
        this.comment = comment;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET ID_STATION
    * Returns the id_station value.
    */
    public int getId_station() {
        return id_station;
    }

    /**
    * SET ID_STATION
    * Sets the value for the id_station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /**
    * GET USAGE
    * Returns the usage value.
    */
    public String getUsage() {
        return usage;
    }

    /**
    * SET USAGE
    * Sets the value for the usage.
    */
    public void setUsage(String usage) {
        this.usage = usage;
    }

    /**
    * GET CPD_NUM
    * Returns the cpd_num value.
    */
    public String getCpd_num() {
        return cpd_num;
    }

    /**
    * SET CPD_NUM
    * Sets the value for the cpd_num.
    */
    public void setCpd_num(String cpd_num) {
        this.cpd_num = cpd_num;
    }

    /**
     * SET IERS Domes
     * Sets the value for the iers_domes.
     */
    public String getIers_domes() {
        return iers_domes;
    }

    /**
     * SET IERS Domes
     * Sets the value for the iers_domes.
     */
    public void setIers_domes(String iers_domes) {
        this.iers_domes = iers_domes;
    }
    
    /**
    * GET DX
    * Returns the dx value.
    */
    public float getDx() {
        return dx;
    }

    /**
    * SET DX
    * Sets the value for the dx.
    */
    public void setDx(float dx) {
        this.dx = dx;
    }

    /**
    * GET DY
    * Returns the dy value.
    */
    public float getDy() {
        return dy;
    }

    /**
    * SET DY
    * Sets the value for the dy.
    */
    public void setDy(float dy) {
        this.dy = dy;
    }

    /**
    * GET DZ
    * Returns the dz value.
    */
    public float getDz() {
        return dz;
    }

    /**
    * SET DZ
    * Sets the value for the dz.
    */
    public void setDz(float dz) {
        this.dz = dz;
    }

    /**
    * GET ACCURACY
    * Returns the accuracy value.
    */
    public float getAccuracy() {
        return accuracy;
    }

    /**
    * SET ACCURACY
    * Sets the value for the accuracy.
    */
    public void setAccuracy(float accuracy) {
        this.accuracy = accuracy;
    }

    /**
    * GET SURVEY_METHOD
    * Returns the survey_method value.
    */
    public String getSurvey_method() {
        return survey_method;
    }

    /**
    * SET SURVEY_METHOD
    * Sets the value for the survey_method.
    */
    public void setSurvey_method(String survey_method) {
        this.survey_method = survey_method;
    }

    /**
    * GET DATE_AT
    * Returns the date_at value.
    */
    public String getDate_at() {
        return date_at;
    }

    /**
    * SET DATE_AT
    * Sets the value for the date_at.
    */
    public void setDate_at(String date_at) {
        this.date_at = date_at;
    }

    /**
    * GET COMMENT
    * Returns the comment value.
    */
    public String getComment() {
        return comment;
    }

    /**
    * SET COMMENT
    * Sets the value for the comment.
    */
    public void setComment(String comment) {
        this.comment = comment;
    }

}
