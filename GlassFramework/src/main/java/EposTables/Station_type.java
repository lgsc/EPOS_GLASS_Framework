package EposTables;

/*
* STATION_TYPE
* This class represents the station_type table of the EPOS database.
*/
public class Station_type extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the station_type table
    private String name;            // Name field of the station_type table
    private String type;            // Type field of the station_type table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * STATION_TYPE
    * This is the default constructor for the Station_type class.
    */
    public Station_type() {
        id = 0;
        name = "";
        type = "";
    }

    /*
    * STATION_TYPE
    * This constructor for the Station_type class accepts values as parameters.
    */
    public Station_type(int id, String name, String type) {
        this.id = id;
        this.name = name;
        this.type = type;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

     /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

     /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /*
    * GET TYPE
    * Returns the type value.
    */
    public String getType() {
        return type;
    }

     /*
    * SET TYPE
    * Sets the value for the type.
    */
    public void setType(String type) {
        this.type = type;
    }
}
