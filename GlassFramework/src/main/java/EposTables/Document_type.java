package EposTables;

/**
* DOCUMENT_TYPE
* This class represents the document_type table of the EPOS database.
*/
public class Document_type extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the document_type table
    private String name;                // Name field of the document_type table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * DOCUMENT_TYPE
    * This is the default constructor for the Document_type class.
    */
    public Document_type() {
        id = 0;
        name = "";
    }

    /**
    * DOCUMENT_TYPE
    * This constructor for the Document_type class accepts values as parameters.
    */
    public Document_type(int id, String name) {
        this.id = id;
        this.name = name;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
}
