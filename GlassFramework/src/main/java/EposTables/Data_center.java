package EposTables;

import javax.xml.crypto.Data;
import java.util.Objects;

/*
* DATA CENTER
* This class represents the data center table of the EPOS database.
*/
public class Data_center extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                         // Id field of the data center table
    private String name;                    // Name of the Data Center
    private String root_path;               // URL to the Data Center
    private String protocol;                // Access conditions field of the data center table
    private String acronym;                 // Data center accronym field of the data center table
    private Node node;                      // Agency field of the data_center table
    private String hostname;                // Hostname field of the data center table
    private Data_center_structure data_center_structure;
    private Agency agency;

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * DATA_CENTER
    * This is the default constructor for the Data_center class.
    */
    public Data_center() {
        id = 0;
        name = "";
        root_path = "";
        protocol = "";
        acronym = "";
        node = new Node();
        hostname = "";
        // data_center_structure = new Data_center_structure();
    }

    /*
    * DATA_CENTER
    * This constructor for the Data_center class accepts values as parameters.
    */
    public Data_center(int id, String name, String root_path, String acronym, Node node , String hostname, String protocol, Data_center_structure dcs) {
        this.id = id;
        this.name = name;
        this.root_path = root_path;
        this.acronym = acronym;
        this.node = node ;
        this.hostname = hostname;
        this.protocol = protocol;
        this.data_center_structure = dcs;
    }

    public Data_center(int id, String name, String root_path, String protocol, String acronym, Agency agency, String hostname, Data_center_structure dcs) {
        this.id = id;
        this.name = name;
        this.root_path = root_path;
        this.protocol = protocol;
        this.acronym = acronym;
        this.hostname = hostname;
        this.agency = agency;
        this.data_center_structure = dcs;
    }

    public Data_center(int id, String name, String root_path, String acronym , String hostname, String protocol) {
        this.id = id;
        this.name = name;
        this.root_path = root_path;
        this.acronym = acronym;
        this.hostname = hostname;
        this.protocol = protocol;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET name
    * Returns the name value.
            */
    public String getName() {
        return name;
    }

    /*
        * SET Name
        * Sets the value for the Name.
        */
    public void setName(String name) {
        this.name = name;
    }

    /*
    * GET ROOT PATH
    * Returns the root path value.
    */
    public String getRoot_path() {
        return root_path;
    }

    /*
        * SET ROOT PATH
        * Sets the value for the Root path.
        */
    public void setRoot_path(String root_path) {
        this.root_path = root_path;
    }


    /*
    * GET DATA CENTER ACCRONYM
    * Returns the accronym value.
    */
    public String getAcronym() {
        return acronym;
    }

    /*
    * SET DATA CENTER ACCRONYM
    * Sets the value for the accronym.
    */
    public void setAcronym(String acronym) {
        this.acronym = acronym;
    }

    /*
    * GET AGENCY
    * Returns the agency value.
    */
    /*public Agency getAgency() {
        return agency;
    }*/

    /*
    * SET AGENCY
    * Sets the value for the agency.
    */
   /* public void setAgency(int id, String name, String abbreviation, String address,
            String www, String infos) {
        this.agency.setId(id);
        this.agency.setName(name);
        this.agency.setAbbreviation(abbreviation);
        this.agency.setAddress(address);
        this.agency.setWww(www);
        this.agency.setInfos(infos);
    }*/

    /*
    * GET HOSTNAME
    * Returns the hostname value.
    */
    public String getHostname() {
        return hostname;
    }

    /*
    * SET HOSTNAME
    * Sets the value for the hostname.
    */
    public void setHostname(String hostname) {
        this.hostname = hostname;
    }

    /*
    * GET PROTOCOL
    * Returns the protocol value.
    */
    public String getProtocol() {
        return protocol;
    }

    /*
    * SET PROTOCOL
    * Sets the value for protocol.
    */
    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }
    
    /*
    * GET Data_center_structure
    * Returns the list of Data_center_structure.
    */
     public Data_center_structure getDataCenterStructure() {
        return data_center_structure;
    }
    
    /*
    * Set Data_center_structure
    * Set a new Data_center_structure.
    */
    public void setDataCenterStructure(Data_center_structure data_center_structure) {
        this.data_center_structure = data_center_structure;
    }

    public Node getNode() {
        return node;
    }

    public void setNode(Node node) {
        this.node = node;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }

    @Override
    public boolean equals(Object o) {
        if (o == this)
            return true;
        if (!(o instanceof Data_center)){
            return false;
        }

        Data_center dc = (Data_center) o;


        return
                this.getId() == dc.getId()
                && this.getAcronym().equals(dc.getAcronym())
                && this.getHostname().equals(dc.getHostname())
                && this.getName().equals(dc.getName())
                && this.getProtocol().equals(dc.getProtocol())
                && this.getRoot_path().equals(dc.getRoot_path());
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, acronym, hostname, name, protocol, root_path);
    }


}
