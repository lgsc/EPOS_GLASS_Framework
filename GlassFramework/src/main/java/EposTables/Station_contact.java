package EposTables;

/**
* STATION_CONTACTS
* This class represents the station_contacts table of the EPOS database.
*/
public class Station_contact extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the station_contacts table
    private int id_station;         // Id_station field of the station_contacts table
    private Contact contact;        // Contact field of the station_contacts table
    private String role;            // Role field of the station_contacts table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * STATION_CONTACTS
    * This is the default constructor for the Station_contact class.
    */
    public Station_contact() {
        id = 0;
        id_station = 0;
        contact = new Contact();
        role = "";
    }

    /**
    * STATION_CONTACTS
    * This constructor for the Station_contact class accepts values as parameters.
    */
    public Station_contact(int id, int id_station, Contact contact, String role) {
        this.id = id;
        this.id_station = id_station;
        this.contact = contact;
        this.role = role;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET ID_STATION
    * Returns the id_station value.
    */
    public int getId_station() {
        return id_station;
    }

    /**
    * SET ID_STATION
    * Sets the value for the id_station.
    */
    public void setId_station(int id_station) {
        this.id_station = id_station;
    }

    /**
    * GET CONTACT
    * Returns the contact object.
    */
    public Contact getContact() {
        return contact;
    }

    /**
    * SET CONTACT
    * Sets the values for the contact.
    */
    public void setContact(Contact contact) {
        this.contact = contact;
    }

    /**
    * GET ROLE
    * Returns the role value.
    */
    public String getRole() {
        return role;
    }

    /**
    * SET ROLE
    * Sets the value for the role.
    */
    public void setRole(String role) {
        this.role = role;
    }
}
