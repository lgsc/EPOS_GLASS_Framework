package EposTables;

/**
* LOG_TYPE
* This class represents the log_type table of the EPOS database.
*/
public class Log_type extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the log_type table
    private String name;                // Name field of the log_type table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * LOG_TYPE
    * This is the default constructor for the Log_type class.
    */
    public Log_type() {
        id = 0;
        name = "";
    }

    /**
    * LOG_TYPE
    * This constructor for the Log_type class accepts values as parameters.
    */
    public Log_type(int id, String name) {
        this.id = id;
        this.name = name;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }
    
    
}
