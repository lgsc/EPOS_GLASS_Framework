package EposTables;

/*
* JUMP
* This class represents the Jump table of the EPOS product database.
*/
public class Jump extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                                     // Id field of the jump table
    private String epoch;                               // Epoch field of the jump table
    private String description;                         // Description field of the jump table
    private double x;                                   // X field of the jump table
    private double y;                                   // Y field of the jump table
    private double z;                                   // Z field of the jump table
    private Jump_type jump_type;                        // Jump Type field of the jump table
    private Jump_method_identification jump_method;     // Jump Method Indentification field of the jump table
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * JUMP
    * This is the default constructor for the Jump class.
    */
    public Jump() {
        this.id = 0;
        this.epoch = "";
        this.description = "";
        this.x = 0;
        this.y = 0;
        this.z = 0;
        this.jump_type = new Jump_type();
        this.jump_method = new Jump_method_identification();
    }

    /*
    * JUMP
    * This constructor for the Jump class accepts values as parameters.
    */
    public Jump(int id, String epoch, String description, double x, double y, double z) {
        this.id = id;
        this.epoch = epoch;
        this.description = description;
        this.x = x;
        this.y = y;
        this.z = z;
        this.jump_type = new Jump_type();
        this.jump_method = new Jump_method_identification();
    }
    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET EPOCH
    * Returns the epoch value.
    */
    public String getEpoch() {
        return epoch;
    }

    /*
    * SET EPOCH
    * Sets the value for the epoch.
    */
    public void setEpoch(String epoch) {
        this.epoch = epoch;
    }

    /*
    * GET DESCRIPTION
    * Returns the description value.
    */
    public String getDescription() {
        return description;
    }

    /*
    * SET DESCRIPTION
    * Sets the value for the description.
    */
    public void setDescription(String description) {
        this.description = description;
    }

    /*
    * GET X
    * Returns the x value.
    */
    public double getX() {
        return x;
    }

    /*
    * SET X
    * Sets the value for the x.
    */
    public void setX(double x) {
        this.x = x;
    }

    /*
    * GET Y
    * Returns the y value.
    */
    public double getY() {
        return y;
    }

    /*
    * SET Y
    * Sets the value for the y.
    */
    public void setY(double y) {
        this.y = y;
    }

    /*
    * GET Z
    * Returns the z value.
    */
    public double getZ() {
        return z;
    }

    /*
    * SET Z
    * Sets the value for the z.
    */
    public void setZ(double z) {
        this.z = z;
    }

    /*
    * GET JUMP TYPE
    * Returns the jump type value.
    */
    public Jump_type getJump_type() {
        return jump_type;
    }

    /*
    * SET JUMP TYPE
    * Sets the value for the jump type.
    */
    public void setJump_type(int id, String type) {
        this.jump_type.setId(id);
        this.jump_type.setType(type);
    }

    /*
    * GET JUMP METHOD IDENTIFICATION
    * Returns the jump method identification value.
    */
    public Jump_method_identification getJump_method() {
        return jump_method;
    }

    /*
    * SET JUMP METHOD IDENTIFICATION
    * Sets the value for the jump method identification.
    */
    public void setJump_method(int id, Agency agency, Software software, String processing_scheme) {
        this.jump_method.setId(id);
        this.jump_method.setAgency(agency.getId(), agency.getName(), agency.getAbbreviation(), 
                agency.getAddress(), agency.getWww(), agency.getInfos());
        this.jump_method.setSoftware(software.getId(), software.getName());
        this.jump_method.setProcessing_scheme(processing_scheme);
    }
}
