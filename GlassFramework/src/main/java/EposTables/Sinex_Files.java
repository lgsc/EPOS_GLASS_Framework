package EposTables;

/**
 * Created by arodrigues on 9/18/17.
 */
public class Sinex_Files extends EposTablesMasterClass {

    private int id;
    private String url;
    private String sampling_period;
    private String epoch;
    private String date_type;
    private Agency agency;

    public Sinex_Files(int id, String url, String sampling_period, String epoch, String date_type, Agency agency) {
        this.id = id;
        this.url = url;
        this.sampling_period = sampling_period;
        this.epoch = epoch;
        this.date_type = date_type;
        this.agency = agency;
    }

    public Sinex_Files() {
        this.id = id;
        this.url = url;
        this.sampling_period = sampling_period;
        this.epoch = epoch;
        this.date_type = date_type;
        this.agency = agency;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getSampling_period() {
        return sampling_period;
    }

    public void setSampling_period(String sampling_period) {
        this.sampling_period = sampling_period;
    }

    public String getEpoch() {
        return epoch;
    }

    public void setEpoch(String epoch) {
        this.epoch = epoch;
    }

    public String getDate_type() {
        return date_type;
    }

    public void setDate_type(String date_type) {
        this.date_type = date_type;
    }

    public Agency getAgency() {
        return agency;
    }

    public void setAgency(Agency agency) {
        this.agency = agency;
    }
}
