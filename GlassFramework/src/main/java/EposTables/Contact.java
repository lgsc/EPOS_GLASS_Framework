package EposTables;

/**
* CONTACT
* This class represents the contact table of the EPOS database.
*/
public class Contact extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the contact table
    private String name;            // Name field of the contact table
    private String title;           // Title field of the contact table
    private String email;           // Email field of the contact table
    private String phone;           // Phone field of the contact table
    private String gsm;             // GSM field of the contact table
    private String comment;         // Comment field of the contact table
    private Agency agency;          // Agency field of the contact table
    private String role;            // Role field of the contact table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * CONTACT
    * This is the default constructor for the Contact class.
    */
    public Contact() {
        id = 0;
        name = "";
        title = "";
        email = "";
        phone = "";
        gsm = "";
        comment = "";
        agency = new Agency();
        role = "";
    }

    /**
    * CONTACT
    * This constructor for the Contact class accepts values as parameters.
    */
    public Contact(int id, String name, String title, String email, String phone,
            String gsm, String comment, Agency agency, String role) {
        this.id = id;
        this.name = name;
        this.title = title;
        this.email = email;
        this.phone = phone;
        this.gsm = gsm;
        this.comment = comment;
        this.agency = agency;
        this.role = role;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /**
    * GET TITLE
    * Returns the title value.
    */
    public String getTitle() {
        return title;
    }

    /**
    * SET TITLE
    * Sets the value for the title.
    */
    public void setTitle(String title) {
        this.title = title;
    }

    /**
    * GET EMAIL
    * Returns the email value.
    */
    public String getEmail() {
        return email;
    }

    /**
    * SET EMAIL
    * Sets the value for the email.
    */
    public void setEmail(String email) {
        this.email = email;
    }

    /**
    * GET PHONE
    * Returns the phone value.
    */
    public String getPhone() {
        return phone;
    }
    
    /**
    * SET PHONE
    * Sets the value for the phone.
    */
    public void setPhone(String phone) {
        this.phone = phone;
    }

    /**
    * GET GSM
    * Returns the gsm value.
    */
    public String getGsm() {
        return gsm;
    }

    /**
    * SET GSM
    * Sets the value for the gsm.
    */
    public void setGsm(String gsm) {
        this.gsm = gsm;
    }

    /**
    * GET COMMENT
    * Returns the comment value.
    */
    public String getComment() {
        return comment;
    }

    /**
    * SET COMMENT
    * Sets the value for the comment.
    */
    public void setComment(String comment) {
        this.comment = comment;
    }

    /**
    * GET AGENCY
    * Returns the agency object.
    */
    public Agency getAgency() {
        return agency;
    }

    /**
    * SET AGENCY
    * Sets the values for the agency.
    */
    public void setAgency(int id_agency, String name, String abbreviation, 
            String address, String www, String infos) {
        this.agency.setId(id_agency);
        this.agency.setName(name);
        this.agency.setAbbreviation(abbreviation);
        this.agency.setAddress(address);
        this.agency.setWww(www);
        this.agency.setInfos(infos);
    }

    public void setAgency(Agency agency){
        this.agency = agency;
    }

    /**
    * GET ROLE
    * Returns the role value.
    */
    public String getRole() {
        return role;
    }

    /**
    * SET ROLE
    * Sets the value for the role.
    */
    public void setRole(String role) {
        this.role = role;
    }
}
