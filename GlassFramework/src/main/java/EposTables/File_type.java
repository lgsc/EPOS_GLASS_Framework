package EposTables;

/*
* FILe_TYPE
* This class represents the file_type table of the EPOS database.
*/
public class File_type extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                 // Id field of the file_type table
    //private String name;            // Name field of the file_type table
    private String format;          // Format field of the file_type table
    private String sampling_window;     // Length field of the file_type table
    private String sampling_frequency;        // Sampling field of the file_type table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * FILE_TYPE
    * This is the default constructor for the File_type class.
    */
    public File_type() {
        id = 0;
  //      name = "";
        format = "";
        sampling_window = "";
        sampling_frequency = "";
    }

    /*
    * FILE_TYPE
    * This constructor for the File_type class accepts values as parameters.
    */
    public File_type(int id, String format, String sampling_window,
            String sampling_frequency) {
        this.id = id;
//        this.name = name;
        this.format = format;
        this.sampling_window = sampling_window;
        this.sampling_frequency = sampling_frequency;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET NAME
    * Returns the name value.
    *
    public String getName() {
        return name;
    }

    *
    * SET NAME
    * Sets the value for the name.
    *
    public void setName(String name) {
        this.name = name;
    }
    */


    /*
    * GET FORMAT
    * Returns the format value.
    */
    public String getFormat() {
        return format;
    }

    /*
    * SET FORMAT
    * Sets the value for the format.
    */
    public void setFormat(String format) {
        this.format = format;
    }

    /*
    * GET FILE LENGTH
    * Returns the file length value.
    */
    public String getSampling_window() {
        return sampling_window;
    }

    /*
    * SET FILE LENGTH
    * Sets the value for the file length.
    */
    public void setSampling_window(String sampling_window) {
        this.sampling_window = sampling_window;
    }

    /*
    * GET SAMPLING
    * Returns the sampling value.
    */
    public String getSampling_frequency() {
        return sampling_frequency;
    }

    /*
    * SET SAMPLING
    * Sets the value for the sampling.
    */
    public void setSampling_frequency(String sampling_frequency) {
        this.sampling_frequency = sampling_frequency;
    }
}
