package EposTables;

/*
* FILE
* This class represents the file table of the EPOS database.
*/
public class File extends EposTablesMasterClass  {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                         // Id field of the file table
    private String name;                    // Name field of the file table
    private String path;                    // Path field of the file table
    private int file_size;                  // File_size field of the file table
    private String data_start_time;         // Data_start_time field of the file table
    private String data_stop_time;          // Data_stop_time field of the file table
    private String published_date;          // Published_date field of the file table
    private String revision_time;           // Revision_time field of the file table
    private String embargo_after_date;      // Embargo_after_date field of the file table
    private int embargo_duration_hours;     // Embargo_duration_hours field of the file table
    private int access_permission_id;       // Access_permission_id field of the file table
    private File_generated file_generated;  // File_generated field of the file table
    private String md5checksum;             // Md5checksum field of the file table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * FILE
    * This is the default constructor for the File class.
    */
    public File() {
        id = 0;
        name = "";
        path = "";
        file_size = 0;
        data_start_time = "";
        data_stop_time = "";
        published_date = "";
        revision_time = "";
        embargo_after_date = "";
        embargo_duration_hours = 0;
        access_permission_id = 0;
        file_generated = new File_generated();
        md5checksum = "";
    }

    /*
    * FILE
    * This constructor for the File class accepts values as parameters.
    */
    public File(String name) {
        this.id = id;
        this.name = name;
        this.path = path;
        this.file_size = file_size;
        this.data_start_time = data_start_time;
        this.data_stop_time = data_stop_time;
        this.published_date = published_date;
        this.revision_time = revision_time;
        this.embargo_after_date = embargo_after_date;
        this.embargo_duration_hours = embargo_duration_hours;
        this.access_permission_id = access_permission_id;
        this.file_generated = new File_generated();
        this.md5checksum = md5checksum;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /*
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /*
    * GET PATH
    * Returns the path value.
    */
    public String getPath() {
        return path;
    }

    /*
    * SET PATH
    * Sets the value for the path.
    */
    public void setPath(String path) {
        this.path = path;
    }

    /*
    * GET FILE_SIZE
    * Returns the file_size value.
    */
    public int getFile_size() {
        return file_size;
    }

    /*
    * SET FILE_SIZE
    * Sets the value for the file_size.
    */
    public void setFile_size(int file_size) {
        this.file_size = file_size;
    }

    /*
    * GET DATA_START_TIME
    * Returns the data_start_time value.
    */
    public String getData_start_time() {
        return data_start_time;
    }

    /*
    * SET DATA_START_TIME
    * Sets the value for the data_start_time.
    */
    public void setData_start_time(String data_start_time) {
        this.data_start_time = data_start_time;
    }

    /*
    * GET DATA_STOP_TIME
    * Returns the data_stop_time value.
    */
    public String getData_stop_time() {
        return data_stop_time;
    }

    /*
    * SET DATA_STOP_TIME
    * Sets the value for the data_stop_time.
    */
    public void setData_stop_time(String data_stop_time) {
        this.data_stop_time = data_stop_time;
    }

    /*
    * GET PUBLISHED_DATE
    * Returns the published_date value.
    */
    public String getPublished_date() {
        return published_date;
    }

    /*
    * SET PUBLISHED_DATE
    * Sets the value for the published_date.
    */
    public void setPublished_date(String published_date) {
        this.published_date = published_date;
    }

    /*
    * GET REVISION_TIME
    * Returns the revision_time value.
    */
    public String getRevision_time() {
        return revision_time;
    }

    /*
    * SET REVISION_TIME
    * Sets the value for the revision_time.
    */
    public void setRevision_time(String revision_time) {
        this.revision_time = revision_time;
    }

    /*
    * GET EMBARGO_AFTER_DATE
    * Returns the emabrgo_after_date value.
    */
    public String getEmbargo_after_date() {
        return embargo_after_date;
    }

    /*
    * SET EMBARGO_AFTER_DATE
    * Sets the value for the embargo_after_date.
    */
    public void setEmbargo_after_date(String embargo_after_date) {
        this.embargo_after_date = embargo_after_date;
    }

    /*
    * GET EMBARGO_DURATION_HOURS
    * Returns the embargo_duration_hours value.
    */
    public int getEmbargo_duration_hours() {
        return embargo_duration_hours;
    }

    /*
    * SET EMBARGO_DURATION_HOURS
    * Sets the value for the emabrgo_duration_hours.
    */
    public void setEmbargo_duration_hours(int embargo_duration_hours) {
        this.embargo_duration_hours = embargo_duration_hours;
    }

    /*
    * GET ACCESS_PERMISSION_ID
    * Returns the access_permission_id value.
    */
    public int getAccess_permission_id() {
        return access_permission_id;
    }

    /*
    * SET ACCESS_PERMISSION_ID
    * Sets the value for the access_permission_id.
    */
    public void setAccess_permission_id(int access_permission_id) {
        this.access_permission_id = access_permission_id;
    }

    /*
    * GET FILE_GENERATED
    * Returns the file_generated value.
    */
    public File_generated getFile_generated() {
        return file_generated;
    }

    /*
    * SET FILE_GENERATED
    * Sets the values for the file_generated.
    */
    public void setFile_generated(int id_file_generated, int id_station, 
            File_type file_type, String station_marker) {
        this.file_generated.setId(id_file_generated);
        this.file_generated.setId_station(id_station);
        //this.file_generated.setFile_type(file_type.getId(), file_type.getName(), file_type.getFormat());
        this.file_generated.setFile_type(file_type.getId(), file_type.getFormat());
        this.file_generated.setStation_marker(station_marker);
    }

    /*
    * GET MD5CHECKSUM
    * Returns the md5checksum value.
    */
    public String getMd5checksum() {
        return md5checksum;
    }

    /*
    * SET MD5CHECKSUM
    * Sets the value for the md5checksum.
    */
    public void setMd5checksum(String md5checksum) {
        this.md5checksum = md5checksum;
    }
}
