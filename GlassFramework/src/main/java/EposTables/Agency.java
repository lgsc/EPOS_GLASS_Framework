package EposTables;

import org.json.simple.JSONObject;

/**
* AGENCY
* This class represents the agency table of the EPOS database.
*/
public class Agency extends EposTablesMasterClass {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the agency table
    private String name;                // Name field of the agency table
    private String abbreviation;        // Abbreviation field of the agency table
    private String address;             // Address field of the agency table
    private String www;                 // WWW field of the agency table
    private String infos;               // Infos field of the agency table

    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /**
    * AGENCY
    * This is the default constructor for the Agency class.
    */
    public Agency() {
    }

    /**
    * AGENCY
    * This constructor for the Agency class accepts values as parameters.
    */
    public Agency(int id, String name, String abbreviation, String address, String www, String infos) {
        this.id = id;
        this.name = name;
        this.abbreviation = abbreviation;
        this.address = address;
        this.www = www;
        this.infos = infos;
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /**
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /**
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /**
    * GET NAME
    * Returns the name value.
    */
    public String getName() {
        return name;
    }

    /**
    * SET NAME
    * Sets the value for the name.
    */
    public void setName(String name) {
        this.name = name;
    }

    /**
    * GET ABBREVIATION
    * Returns the abbreviation value.
    */
    public String getAbbreviation() {
        return abbreviation;
    }

    /**
    * SET ABBREVIATION
    * Sets the value for the abbreviation.
    */
    public void setAbbreviation(String abbreviation) {
        this.abbreviation = abbreviation;
    }

    /**
    * GET ADDRESS
    * Returns the address value.
    */
    public String getAddress() {
        return address;
    }

    /**
    * SET ADDRESS
    * Sets the value for the address.
    */
    public void setAddress(String address) {
        this.address = address;
    }

    /**
    * GET WWW
    * Returns the www value.
    */
    public String getWww() {
        return www;
    }

    /**
    * SET WWW
    * Sets the value for the www.
    */
    public void setWww(String www) {
        this.www = www;
    }

    /**
    * GET INFOS
    * Returns the infos value.
    */
    public String getInfos() {
        return infos;
    }

    /**
    * SET INFOS
    * Sets the value for the infos.
    */
    public void setInfos(String infos) {
        this.infos = infos;
    }

    public JSONObject toJsonObject() {
        JSONObject agency = new JSONObject();
        agency.put("name", this.getName());
        agency.put("abbreviation", this.getAbbreviation());
        agency.put("address", this.getAddress());
        agency.put("www", this.getWww());
        agency.put("infos", this.getInfos());
        return agency;
    }
}
