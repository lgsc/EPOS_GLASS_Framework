/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package EposTables;

import java.util.ArrayList;

/**
 *
 * @author kngo
 */
public class QC_Constellation_Summary extends EposTablesMasterClass {
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     
    private int id_qc_report_summary;                
    private Constellation constellation;             
    private int nsat;      
    private int xele;
    private int epo_expt;              
    private int epo_have;        
    private int epo_usbl;       
    private int xcod_epo;      
    private int xcod_sat;       
    private int xpha_epo;       
    private int xpha_sat;       
    private int xint_epo;         
    private int xint_sat;     
    private int xint_sig;
    private int xint_slp;
    private float x_crd;
    private float y_crd;
    private float z_crd;
    private float x_rms;
    private float y_rms;
    private float z_rms;
    private ArrayList qc_observation_summary_s;
    private ArrayList qc_observation_summary_c;
    private ArrayList qc_observation_summary_d;
    private ArrayList qc_observation_summary_l;
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * QC_Constellation_Summary
    * This is the default constructor for the QC_Constellation_Summary class.
    */
    public QC_Constellation_Summary() {
        id = 0;
        id_qc_report_summary = 0;
        constellation = new Constellation();
        nsat = 0;
        xele = 0;
        epo_expt = 0;
        epo_have = 0;
        epo_usbl = 0;
        xcod_epo = 0;
        xcod_sat = 0;
        xpha_epo = 0;
        xpha_sat = 0;
        xint_epo = 0;
        xint_sat = 0;
        xint_sig = 0;
        xint_slp = 0;
        x_crd = 0;
        y_crd = 0;
        z_crd = 0;
        x_rms = 0;
        y_rms = 0;
        z_rms = 0;
        qc_observation_summary_s = new ArrayList();
        qc_observation_summary_c = new ArrayList();
        qc_observation_summary_d = new ArrayList();
        qc_observation_summary_l = new ArrayList();
    }

    /*
    * QC_Constellation_Summary
    * This constructor for the QC_Constellation_Summary class accepts values as parameters.
    */
    public QC_Constellation_Summary(int id, int id_qc_report_summary, Constellation constellation, int nsat, 
            int xele, int epo_expt, int epo_have, 
            int epo_usbl, int xcod_epo, int xcod_sat,
            int xpha_epo, int xpha_sat,int xint_epo, int xint_sat, int xint_sig, int xint_sip, float x_crd, float y_crd, float z_crd, float x_rms, float y_rms, float z_rms) {
        this.id = id;
        this.id_qc_report_summary = id_qc_report_summary;
        this.constellation = constellation;
        this.nsat = nsat;
        this.xele = xele;
        this.epo_expt = epo_expt;
        this.epo_have = epo_have;
        this.epo_usbl = epo_usbl;
        this.xcod_epo = xcod_epo;
        this.xcod_sat = xcod_sat;
        this.xpha_epo = xpha_epo;
        this.xpha_sat = xpha_sat;
        this.xint_epo = xint_epo;
        this.xint_sat = xint_sat;
        this.xint_sig = xint_sig;
        this.xint_slp = xint_sip;
        this.x_crd = x_crd;
        this.y_crd = y_crd;
        this.z_crd = z_crd;
        this.x_rms = x_rms;
        this.y_rms = y_rms;
        this.z_rms = z_rms;
        this.qc_observation_summary_s = new ArrayList();
        this.qc_observation_summary_c = new ArrayList();
        this.qc_observation_summary_d = new ArrayList();
        this.qc_observation_summary_l = new ArrayList();
    }

    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET id_qc_report_summary
    * Returns the id_qc_report_summary value.
    */
    public int getIdQcReportSummary() {
        return id_qc_report_summary;
    }

    /*
    * SET id_qc_report_summary
    * Sets the value for the id_qc_report_summary.
    */
    public void setIdQcReportSummary(int id_qc_report_summary) {
        this.id_qc_report_summary = id_qc_report_summary;
    }

    /*
    * GET CONSTELLATION
    * Returns the constellation value.
    */
    public Constellation getConstellation() {
        return constellation;
    }

    /*
    * SET CONSTELLATION
    * Sets the value for the constellation.
    */
    public void setConstellation(Constellation constellation) {
        this.constellation = constellation;
    }
    
    /*
    * GET NSAT
    * Returns the nsat value.
    */
    public int getNsat() {
        return nsat;
    }

    /*
    * SET NSAT
    * Sets the value for the nsat.
    */
    public void setNsat(int nsat) {
        this.nsat = nsat;
    }

    /*
    * GET XELE
    * Returns the xele value.
    */
    public int getXele() {
        return xele;
    }

    /*
    * SET XELE
    * Sets the value for the xele.
    */
    public void setXele(int xele) {
        this.xele = xele;
    }
    
    /*
    * GET EPO HAVE
    * Returns the epo_have value.
    */
    public int getEpoHave() {
        return epo_have;
    }

    /*
    * SET EPO HAVE
    * Sets the value for the epo have.
    */
    public void setEpoHave(int epo_have) {
        this.epo_have = epo_have;
    }

    /*
    * GET EPO EXPT
    * Returns the epo expt value.
    */
    public int getEpoExpt() {
        return epo_expt;
    }

    /*
    * SET EPO EXPT
    * Sets the value for the epo expt.
    */
    public void setEpoExpt(int epo_expt) {
        this.epo_expt = epo_expt;
    }
    
    /*
    * GET EPO USBL
    * Returns the epo usbl value.
    */
    public int getEpoUsbl() {
        return epo_usbl;
    }

    /*
    * SET EPO USBL
    * Sets the value for the epo usbl.
    */
    public void setEpoUsbl(int epo_usbl) {
        this.epo_usbl = epo_usbl;
    }
    
    /*
    * GET XCOD EPO
    * Returns the xcod epo value.
    */
    public int getXcodEpo() {
        return xcod_epo;
    }

    /*
    * SET XCOD EPO
    * Sets the value for the xcod epo.
    */
    public void setXcodEpo(int xcod_epo) {
        this.xcod_epo = xcod_epo;
    }
    
    /*
    * GET XCOD SAT
    * Returns the xcod sat value.
    */
    public int getXcodSat() {
        return xcod_sat;
    }

    /*
    * SET XCOD SAT
    * Sets the value for the xcod sat.
    */
    public void setXcodSat(int xcod_sat) {
        this.xcod_sat = xcod_sat;
    }
    
    
    /*
    * GET XPHA EPO
    * Returns the xpha epo value.
    */
    public int getXphaEpo() {
        return xpha_epo;
    }

    /*
    * SET XPHA EPO
    * Sets the value for the xpha epo.
    */
    public void setXphaEpo(int xpha_epo) {
        this.xpha_epo = xpha_epo;
    }
    
    /*
    * GET XPHA SAT
    * Returns the xpha sat value.
    */
    public int getXphaSat() {
        return xpha_sat;
    }

    /*
    * SET XPHA SAT
    * Sets the value for the xpha sat.
    */
    public void setXphaSat(int xpha_sat) {
        this.xpha_sat = xpha_sat;
    }
    
    /*
    * GET XINT EPO
    * Returns the xint epo value.
    */
    public int getXintEpo() {
        return xint_epo;
    }

    /*
    * SET XINT EPO
    * Sets the value for the xint epo.
    */
    public void setXintEpo(int xint_epo) {
        this.xint_epo = xint_epo;
    }
    
    /*
    * GET XINT SAT
    * Returns the xint sat value.
    */
    public int getXintSat() {
        return xint_sat;
    }

    /*
    * SET XINT SAT
    * Sets the value for the xint sat.
    */
    public void setXintSat(int xint_sat) {
        this.xint_sat = xint_sat;
    }
    
    /*
    * GET XINT SIG
    * Returns the xint sig value.
    */
    public int getXintSig() {
        return xint_sig;
    }

    /*
    * SET XINT SIG
    * Sets the value for the xint sig.
    */
    public void setXintSig(int xint_sig) {
        this.xint_sig = xint_sig;
    }
    
    /*
    * GET XINT SLP
    * Returns the xint slp value.
    */
    public int getXintSlp() {
        return xint_slp;
    }

    /*
    * SET XINT SLP
    * Sets the value for the xint slp.
    */
    public void setXintSip(int xint_slp) {
        this.xint_slp = xint_slp;
    }
    
    /*
    * GET X CRD
    * Returns the x crd value.
    */
    public float getXCrd() {
        return x_crd;
    }

    /*
    * SET X CRD
    * Sets the value for the x crd.
    */
    public void setXCrd(int x_crd) {
        this.x_crd = x_crd;
    }
    
    /*
    * GET Y CRD
    * Returns the y crd value.
    */
    public float getYCrd() {
        return y_crd;
    }

    /*
    * SET Y CRD
    * Sets the value for the y crd.
    */
    public void setYCrd(int y_crd) {
        this.y_crd = y_crd;
    }
    
    /*
    * GET Z CRD
    * Returns the z crd value.
    */
    public float getZCrd() {
        return z_crd;
    }

    /*
    * SET Z CRD
    * Sets the value for the z crd.
    */
    public void setZCrd(int z_crd) {
        this.z_crd = z_crd;
    }
    
    /*
    * GET X RMS
    * Returns the x rms value.
    */
    public float getXRms() {
        return x_rms;
    }

    /*
    * SET X RMS
    * Sets the value for the x rms.
    */
    public void setXRms(int x_rms) {
        this.x_rms = x_rms;
    }
    
    /*
    * GET Y RMS
    * Returns the y rms value.
    */
    public float getYRms() {
        return y_rms;
    }

    /*
    * SET Y RMS
    * Sets the value for the y rms.
    */
    public void setYRms(int y_rms) {
        this.y_rms = y_rms;
    }
    
    /*
    * GET Z RMS
    * Returns the z rms value.
    */
    public float getZRms() {
        return z_rms;
    }

    /*
    * SET Z RMS
    * Sets the value for the z rms.
    */
    public void setZRms(int z_rms) {
        this.z_rms = z_rms;
    }
    
    /**
    * ADD QC OBSERVATION SUMMARY S
    * Adds a new qc_observation_summary_s to the list.
    */
    public void AddQCObservationSummaryS(QC_Observation_Summary_S qc_observation_summary_s)
    {  
        this.qc_observation_summary_s.add(qc_observation_summary_s);
    }
    
    /**
    * GET QC OBSERVATION SUMMARY S
    * Returns the list of qc_observation_summary_s.
    */
    public ArrayList<QC_Observation_Summary_S> getQCObservationSummaryS()
    {
        return qc_observation_summary_s;
    }
    
    /**
    * ADD QC OBSERVATION SUMMARY C
    * Adds a new qc_observation_summary_c to the list.
    */
    public void AddQCObservationSummaryC(QC_Observation_Summary_C qc_observation_summary_c)
    {  
        this.qc_observation_summary_c.add(qc_observation_summary_c);
    }
    
    /**
    * GET QC OBSERVATION SUMMARY C
    * Returns the list of qc_observation_summary_c.
    */
    public ArrayList<QC_Observation_Summary_C> getQCObservationSummaryC()
    {
        return qc_observation_summary_c;
    }
    
    /**
    * ADD QC OBSERVATION SUMMARY D
    * Adds a new qc_observation_summary_d to the list.
    */
    public void AddQCObservationSummaryD(QC_Observation_Summary_D qc_observation_summary_d)
    {  
        this.qc_observation_summary_d.add(qc_observation_summary_d);
    }
    
    /**
    * GET QC OBSERVATION SUMMARY D
    * Returns the list of qc_observation_summary_d.
    */
    public ArrayList<QC_Observation_Summary_D> getQCObservationSummaryD()
    {
        return qc_observation_summary_d;
    }
    
    /**
    * ADD QC OBSERVATION SUMMARY L
    * Adds a new qc_observation_summary_l to the list.
    */
    public void AddQCObservationSummaryL(QC_Observation_Summary_L qc_observation_summary_l)
    {  
        this.qc_observation_summary_l.add(qc_observation_summary_l);
    }
    
    /**
    * GET QC OBSERVATION SUMMARY L
    * Returns the list of qc_observation_summary_l.
    */
    public ArrayList<QC_Observation_Summary_L> getQCObservationSummaryL()
    {
        return qc_observation_summary_l;
    }
}
