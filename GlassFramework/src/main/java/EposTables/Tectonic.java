package EposTables;

/*
* TECTONIC
* This class represents the tectonic table of the EPOS database.
*/
public class Tectonic extends EposTablesMasterClass  {
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    private int id;                     // Id field of the tectonic table
    private String plate_name;          // Plate_name field of the tectonic table

    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * TECTONIC
    * This is the default constructor for the Tectonic class.
    */
    public Tectonic() {
        id = 0;
        plate_name = "";
    }

    /*
    * TECTONIC
    * This constructor for the Tectonic class accepts values as parameters.
    */
    public Tectonic(int id, String plate_name) {
        this.id = id;
        this.plate_name = plate_name;
    }

    
    // =========================================================================
    //  GETTERS & SETTERS
    // =========================================================================
    /*
    * GET ID
    * Returns the id value.
    */
    public int getId() {
        return id;
    }

    /*
    * SET ID
    * Sets the value for the id.
    */
    public void setId(int id) {
        this.id = id;
    }

    /*
    * GET PLATE_NAME
    * Returns the plate_name value.
    */
    public String getPlate_name() {
        return plate_name;
    }

    /*
    * SET PLATE_NAME
    * Sets the value for the plate_name.
    */
    public void setPlate_name(String plate_name) {
        this.plate_name = plate_name;
    }
}
