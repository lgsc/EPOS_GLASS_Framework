package Glass;

import EposTables.Estimated_coordinates;
import EposTables.Station;
import org.apache.commons.math.linear.MatrixUtils;
import org.apache.commons.math.linear.RealMatrix;
import org.apache.commons.math.stat.correlation.Covariance;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
//General Imports


public class PBOHandler {


    private PBOHandler() {
    }

    /**
     * Generates MJD
     * Based on Machiel's docs.
     *
     * @return            integer with modified julian date
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */
    public static int[] calculateData(double MJD) {

        int jul = (int) (Math.floor(MJD) + 2400001);

        int l = jul + 68569;
        int n = 4 * l / 146097;
        l = l - (((146097 * n) + 3) / 4);
        int i = (4000 * (l + 1)) / 1461001;
        l = l - (1461 * i / 4) + 31;
        int j = (80 * l) / 2447;
        int k = l - (2447 * (j / 80));
        l = j / 11;
        j = j + 2 - (12 * l);
        i = (100 * (n - 49)) + i + l;

        //Legivel
        int year = i;
        int month = j;
        int day = k;

        if (year < 1801 || year > 2099) {
            System.err.println("year {0:d} is out of possible range!");
            //This will crash the GlassFish Server so do not do it
            //System.exit(1);
        }

        int[] A = {year, month, day, 00, 00, 00};

        return A;
    }

    /**
     * Convert XYZ to LBH
     * Based on Machiel's docs.
     *
     * @return            Double array
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */
    public static double[] convertXYZ2LBH(double X, double Y, double Z) {

        double eps = 1.0e-6;

        double[] LBH = {0.0, 0.0, 0.0};
        double[] XYZ = {X, Y, Z};

        double a = 6378137.0;
        double f = 1.0 / 298.257223563;
        double b = a - (f * a);
        double E = Math.sqrt((a * a) - (b * b));
        double e = E / a;

        if (Math.abs(XYZ[0]) < eps && Math.abs(XYZ[1]) < eps) {
            LBH[0] = 0.0;
        } else
            LBH[0] = Math.atan2(XYZ[1], XYZ[0]);

        double p = Math.sqrt((XYZ[0] * XYZ[0]) + (XYZ[1] * XYZ[1]));
        double h_old = 0.0;
        LBH[1] = Math.atan2(XYZ[2], p * (1.0 - (e * e)));
        double cs = Math.cos(LBH[1]);
        double sn = Math.sin(LBH[1]);
        double N = (a * a) / (Math.sqrt((a * a * cs * cs) + (b * b * sn * sn)));
        double h;

        if (Math.abs(p) < eps) {
            if (XYZ[2] < 0.0) {
                h = -b - XYZ[2];
                LBH[1] = - Math.PI / 2.0;
            } else {
                h = XYZ[2] - b;
                LBH[1] = Math.PI / 2.0;
            }
        } else {
            h = (p / cs) - N;
            while (Math.abs(h - h_old) > eps) {
                h_old = h;
                LBH[1] = Math.atan2(XYZ[2], p * (1.0 - ((e * e * N) / (N + h))));
                cs = Math.cos(LBH[1]);
                sn = Math.sin(LBH[1]);
                N = (a * a) / Math.sqrt((a * a * cs * cs) + (b * b * sn * sn));
                h = (p / cs) - N;
            }
        }
        LBH[2] = h;

        return LBH;
    }


    /* Just convert date into PBO date
     */
    public static String PBOdate(String date_in) {

        String date_out;

        date_out = date_in.replace("-", "");
        return date_out.replace(":", "");
    }


    /**
     * Generates the pbo content
     * Based on Machiel's docs.
     *
     * @return            String with pbo content
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */
    public static String PBOEstimatedCoordinates(ArrayList<Estimated_coordinates> data, String site, String sampling_period) {

        double  rad = Math.PI / 180.0;
        final double eps = 1.0e-8;
        double  Rxy,Rxz,Ryz,Rne,Rnu,Reu;
        HashMap<String, String> long_names = new HashMap<String, String>();
        String  fr_name,ac_abbr;

        double speriod=0;
        if(sampling_period.equals("weekly") || sampling_period.equals("Weekly") || sampling_period.equals("WEEKLY")){
            speriod=7;
        }
        else{
            speriod=1;
        }


        StringBuilder pbo = new StringBuilder();
        try {

            double velx = data.get(0).getX();
            double vely = data.get(0).getY();
            double velz = data.get(0).getZ();


            double X0 = velx;
            double Y0 = vely;
            double Z0 = velz;
            double[] LBH = convertXYZ2LBH(X0, Y0, Z0);
            double lon0 = LBH[0]/rad;
            double lat0 = LBH[1]/rad;
            double cl0  = Math.cos(LBH[0]);
            double sl0  = Math.sin(LBH[0]);
            double ct0  = Math.cos(LBH[1]);
            double st0  = Math.sin(LBH[1]);
            double h0 = LBH[2];

            //Transformar matrix de covariância
            RealMatrix mx = MatrixUtils.createRealMatrix(new double[][]{
                    {-sl0, cl0, 0},
                    {-st0 * cl0, -st0 * sl0, ct0},
                    {ct0 * cl0, ct0 * sl0, st0}
            });
            RealMatrix R = new Covariance(mx).getCovarianceMatrix();

            DataBaseConnection db1c = new DataBaseConnection();
            Station station =db1c.getStationsShortByMarker(site);

            //--- Since the dawn of time, ITRF2008 was hard coded as the Reference frame of
            //    the time series. This is rubbish of course. Here I get the correct frame name.
            fr_name = data.get(0).getMethod_identification().getReference_frame().getName();
            ac_abbr = data.get(0).getMethod_identification().getAnalysis_center().getAbbreviation();

            pbo.append("PBO Station Position Time Series. Reference Frame : " + fr_name + "\n"
                    +"Format Version: 1.1.0\n"
                    +"4-character ID: " + site + "\n"
                    +"Station name  : " + station.getName().toString() + "\n");


            pbo.append("First Epoch   : " + PBOdate(data.get(0).getEpoch())+ "\n");
            pbo.append("Last Epoch    : " + PBOdate(data.get(data.size()-1).getEpoch())+"\n");
            pbo.append("Release Date  : "+ PBOdate(data.get(0).getMethod_identification().getCreation_date()) +"\n"
            +"XYZ Reference position :  " + String.format("%14.5f", X0) + " " + String.format("%14.5f", Y0) + " " + String.format("%14.5f", Z0) + " (" + fr_name + ")\n"
            +"NEU Reference position :  " + String.format("%15.10f", lon0) + " " + String.format("%15.10f", lat0) + " " + String.format("%11.5f", h0) + " (" + fr_name + "/WGS84)\n"
            +"Start Field Description\n"
            +"YYYYMMDD      Year, month, day for the given position epoch\n"
            +"HHMMSS        Hour, minute, second for the given position epoch\n"
            +"JJJJJ.JJJJJ   Modified Julian day for the given position epoch\n"
            +"X             X coordinate, Specified Reference Frame, meters\n"
            +"Y             Y coordinate, Specified Reference Frame, meters\n"
            +"Z             Z coordinate, Specified Reference Frame, meters\n"
            +"Sx            Standard deviation of the X position, meters\n"
            +"Sy            Standard deviation of the Y position, meters\n"
            +"Sz            Standard deviation of the Z position, meters\n"
            +"Rxy           Correlation of the X and Y position\n"
            +"Rxz           Correlation of the X and Z position\n"
            +"Ryz           Correlation of the Y and Z position\n"
            +"Nlat          North latitude, WGS-84 ellipsoid, decimal degrees\n"
            +"Elong         East longitude, WGS-84 ellipsoid, decimal degrees\n"
            +"Height (Up)   Height relative to WGS-84 ellipsoid, m\n"
            +"dN            Difference in North component from NEU reference position, meters\n"
            +"dE            Difference in East component from NEU reference position, meters\n"
            +"du            Difference in vertical component from NEU reference position, meters\n"
            +"Sn            Standard deviation of dN, meters\n"
            +"Se            Standard deviation of dE, meters\n"
            +"Su            Standard deviation of dU, meters\n"
            +"Rne           Correlation of dN and dE\n"
            +"Rnu           Correlation of dN and dU\n"
            +"Reu           Correlation of dE and dU\n"
            +"Soln          \"rapid\", \"final\", \"suppl/suppf\", \"campd\", or \"repro\" corresponding to products  generated with rapid or final orbit products, in supplemental processing, campaign data processing or reprocessing\n"
            +"End Field Description\n"
            +"*YYYYMMDD HHMMSS JJJJJ.JJJJ         X             Y             Z            Sx        Sy       Sz     Rxy   Rxz    Ryz            NLat         Elong         Height         dN        dE        dU         Sn       Se       Su      Rne    Rnu    Reu  Sol\n");


            //Salvar linhas
            String enu="";
            for (int i=0; i<data.size(); i++) {

                enu="";
                double MJD=0;
                int year = 0;
                int month = 0;
                int day = 0;
                int hour = 0;
                int minute = 0;
                int second = 0;

                //MJD = calculate_GPSweek(data.get(i).getEpoch()) + 0.5 * sampling_period;
                //int[] date = calculateData(MJD);

                double mjddate=calculate_GPSweek(data.get(i).getEpoch());

                String[] epoch_split = data.get(i).getEpoch().split("-|\\ ");
                year = Integer.parseInt(epoch_split[0]);
                month = Integer.parseInt(epoch_split[1]);
                day = Integer.parseInt(epoch_split[2]);
                //year = date[0];
                //month = date[1];
                //day = date[2];
                //hour = date[3];
                //minute = date[4];
                //second = date[5];

                double X = data.get(i).getX();
                double Y = data.get(i).getY();
                double Z = data.get(i).getZ();

                double varXX = data.get(i).getVar_xx();
                double varXY = data.get(i).getVar_xy();
                double varYY = data.get(i).getVar_yy();
                double varXZ = data.get(i).getVar_xz();
                double varYZ = data.get(i).getVar_yz();
                double varZZ = data.get(i).getVar_zz();

                double Sx = Math.sqrt(varXX);
                double Sy = Math.sqrt(varYY);
                double Sz = Math.sqrt(varZZ);

                if (Math.abs(Sx)<eps || Math.abs(Sy)<eps) {
                    Rxy = 0.0;
                } else {
                    Rxy = varXY / (Sx * Sy);
                }
                if (Math.abs(Sx)<eps || Math.abs(Sz)<eps) {
                    Rxz = 0.0;
                } else {
                    Rxz = varXZ / (Sx * Sz);
                }
                if (Math.abs(Sy)<eps || Math.abs(Sz)<eps) {
                    Ryz = 0.0;
                } else {
                    Ryz = varYZ / (Sy * Sz);
                }

                double[] LBH_final = convertXYZ2LBH(X, Y, Z);
                double lon = LBH_final[0];
                double lat = LBH_final[1];
                double h = LBH_final[2];

                //Remover offset entre estação e geocentro
                double dX = X - X0;
                double dY = Y - Y0;
                double dZ = Z - Z0;

                //Rodar e converte metros em milimetros
                double e = -sl0 * dX + cl0 * dY;
                double n = -cl0 * st0 * dX - st0 * sl0 * dY + ct0 * dZ;
                double u = cl0 * ct0 * dX + ct0 * sl0 * dY + st0 * dZ;

                //Construir Matrix de Covariância XYZ
                RealMatrix mtx = MatrixUtils.createRealMatrix(new double[][]{
                        {varXX, varXY, varXZ},
                        {varXY, varYY, varYZ},
                        {varXZ, varYZ, varZZ}
                });

               // RealMatrix VCV_XYZ = new Covariance(mtx).getCovarianceMatrix();

                //Tornar matrix de Covariância em East, North, Up (ENU)
                RealMatrix VCV_ENU = R.multiply(mtx).multiply(R.transpose());

                //Diagonais
                double Sn = Math.sqrt(VCV_ENU.getEntry(1, 1));
                double Se = Math.sqrt(VCV_ENU.getEntry(0, 0));
                double Su = Math.sqrt(VCV_ENU.getEntry(2, 2));

                //Corelações Cruzadas
                if (Math.abs(Sn)<eps || Math.abs(Se)<eps) {
                    Rne = 0.0;
                } else {
                    Rne = VCV_ENU.getEntry(0, 1) / (Sn * Se);
                }
                if (Math.abs(Sn)<eps || Math.abs(Su)<eps) {
                    Rnu = 0.0;
                } else {
                    Rnu = VCV_ENU.getEntry(1, 2) / (Sn * Su);
                }
                if (Math.abs(Se)<eps || Math.abs(Su)<eps) {
                    Reu = 0.0;
                } else {
                    Reu = VCV_ENU.getEntry(0, 2) / (Su * Se);
                }

                pbo.append(" " + String.format("%04d", year) + String.format("%02d", month) + String.format("%02d", day) + " " + String.format("%02d", hour) + String.format("%02d", minute) + String.format("%02d", second)+" "
                +String.format("%10.4f", mjddate) + " " + String.format("%14.5f", X) + " " + String.format("%14.5f", Y) + " " + String.format("%14.5f", Z) + " "
                +String.format("%8.5f", Sx) + " " + String.format("%8.5f", Sy) + " " + String.format("%8.5f", Sz) + " "
                +String.format("%6.3f", Rxy) + " " + String.format("%6.3f", Rxz) + " " + String.format("%6.3f", Ryz) + "    "
                +String.format("%15.10f", (lat / rad)) + " " + String.format("%15.10f", (lon / rad)) + " " + String.format("%10.5f", h) + "  "
                +String.format("%10.5f", n) + " " + String.format("%10.5f", e) + " " + String.format("%10.5f", u) + "  "
                +String.format("%9.5f", Sn) + " " + String.format("%9.5f", Se) + " " + String.format("%9.5f", Su) + " "
                +String.format("%6.3f", Rne) + " " + String.format("%6.3f", Rnu) + " " + String.format("%6.3f", Reu) + " "
                + ac_abbr + "\n");
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR on writting PBO output!");
        }

        return pbo.toString();
    }


    /**
     * Convert pbo to midas
     * Based on Machiel's docs.
     *
     * @return            ArrayList
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */
    public static ArrayList<Estimated_coordinates> PBOEstimatedCoordinates2MIDAS(ArrayList<Estimated_coordinates> data, String site, String sampling_period) {

        double rad = Math.PI / 180.0;


        ArrayList<Estimated_coordinates> results = new ArrayList<>();

        double speriod=0;
        if(sampling_period.equals("weekly") || sampling_period.equals("Weekly") || sampling_period.equals("WEEKLY")){
            speriod=7;
        }
        else{
            speriod=1;
        }


        try {

            double velx = data.get(0).getX();
            double vely = data.get(0).getY();
            double velz = data.get(0).getZ();


            double X0 = velx;
            double Y0 = vely;
            double Z0 = velz;
            double[] LBH = convertXYZ2LBH(X0, Y0, Z0);
            double lon0 = LBH[0]/rad;
            double lat0 = LBH[1]/rad;
            double cl0  = Math.cos(LBH[0]);
            double sl0  = Math.sin(LBH[0]);
            double ct0  = Math.cos(LBH[1]);
            double st0  = Math.sin(LBH[1]);
            double h0 = LBH[2];

            //Transformar matrix de covariância
            RealMatrix mx = MatrixUtils.createRealMatrix(new double[][]{
                    {-sl0, cl0, 0},
                    {-st0 * cl0, -st0 * sl0, ct0},
                    {ct0 * cl0, ct0 * sl0, st0}
            });
            RealMatrix R = new Covariance(mx).getCovarianceMatrix();


            for (int i=0; i<data.size(); i++) {

                Estimated_coordinates et = new Estimated_coordinates();

                double MJD=0;
                int year = 0;
                int month = 0;
                int day = 0;
                int hour = 0;
                int minute = 0;
                int second = 0;

                //MJD = calculate_GPSweek(data.get(i).getEpoch()) + 0.5 * sampling_period;
                //int[] date = calculateData(MJD);

                double mjddate=calculate_GPSweek(data.get(i).getEpoch());

                String[] epoch_split = data.get(i).getEpoch().split("-|\\ ");
                year = Integer.parseInt(epoch_split[0]);
                month = Integer.parseInt(epoch_split[1]);
                day = Integer.parseInt(epoch_split[2]);


                double X = data.get(i).getX();
                double Y = data.get(i).getY();
                double Z = data.get(i).getZ();

                double varXX = data.get(i).getVar_xx();
                double varXY = data.get(i).getVar_xy();
                double varYY = data.get(i).getVar_yy();
                double varXZ = data.get(i).getVar_xz();
                double varYZ = data.get(i).getVar_yz();
                double varZZ = data.get(i).getVar_zz();

                double Sx = Math.sqrt(varXX);
                double Sy = Math.sqrt(varYY);
                double Sz = Math.sqrt(varZZ);

                double Rxy = varXY / (Sx * Sy);
                double Rxz = varXZ / (Sx * Sz);
                double Ryz = varYZ / (Sy * Sz);

                double[] LBH_final = convertXYZ2LBH(X, Y, Z);
                double lon = LBH_final[0];
                double lat = LBH_final[1];
                double h = LBH_final[2];

                //Remover offset entre estação e geocentro
                double dX = X - X0;
                double dY = Y - Y0;
                double dZ = Z - Z0;

                //Rodar e converte metros em milimetros
                double e = -sl0 * dX + cl0 * dY;
                double n = -cl0 * st0 * dX - st0 * sl0 * dY + ct0 * dZ;
                double u = cl0 * ct0 * dX + ct0 * sl0 * dY + st0 * dZ;

                //Construir Matrix de Covariância XYZ
                RealMatrix mtx = MatrixUtils.createRealMatrix(new double[][]{
                        {varXX, varXY, varXZ},
                        {varXY, varYY, varYZ},
                        {varXZ, varYZ, varZZ}
                });

                //RealMatrix VCV_XYZ = new Covariance(mtx).getCovarianceMatrix();

                //Tornar matrix de Covariância em East, North, Up (ENU)
                RealMatrix VCV_ENU = R.multiply(mtx).multiply(R.transpose());

                //Diagonais
                double Sn = Math.sqrt(VCV_ENU.getEntry(1, 1));
                double Se = Math.sqrt(VCV_ENU.getEntry(0, 0));
                double Su = Math.sqrt(VCV_ENU.getEntry(2, 2));

                //Corelações Cruzadas
                double Rne = VCV_ENU.getEntry(0, 1) / (Sn * Se);
                double Rnu = VCV_ENU.getEntry(1, 2) / (Sn * Su);
                double Reu = VCV_ENU.getEntry(0, 2) / (Su * Se);

                et.setMJD(mjddate);
                et.setX(n);
                et.setY(e);
                et.setZ(u);

                results.add(et);
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("ERROR on writting PBO output!");
        }

        return results;
    }



    public static String mjdcalc(String data){
    DateFormat df = new SimpleDateFormat("yyyyMMdd");
		df.setLenient(false);

    Date date = new Date();
		if (data != null) {
        try {
            date = df.parse(data);
        } catch (ParseException pex) {
            System.out.println("Invalid date: " + data);
        }
    }
        long mjd = calculateMJD(date);
        return String.valueOf(mjd);
    }

    /**
     * @return Modified Julian Day (integer number) for given date.
     * NB. Default calendar (default time zone and locale) are used.
     */
    public static long calculateMJD(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.set(1858, Calendar.NOVEMBER, 17, 0, 0, 0);
        cal.set(Calendar.MILLISECOND, 0);
        long utc0 = cal.getTimeInMillis();
        long dstOffset0 = cal.get(Calendar.DST_OFFSET);

        cal.setTime(date);
        long utc1 = cal.getTimeInMillis();
        long dstOffset1 = cal.get(Calendar.DST_OFFSET);

        long ms = utc1 - utc0 + dstOffset1 - dstOffset0;
        return ms / (24L * 3600L * 1000L);
    }




    /**
     * Calculate GPS week
     * Based on Machiel's docs.
     *
     * @return            double with gps week
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */
    public static double calculate_GPSweek(String epoch) {

        String[] epoch_split = epoch.split("-|\\ ");
        int year = Integer.parseInt(epoch_split[0]);
        int month = Integer.parseInt(epoch_split[1]);
        int day = Integer.parseInt(epoch_split[2]);


        if (month<10){
            if (day<10) {
                return Double.parseDouble(mjdcalc(String.valueOf(year) + "0"+String.valueOf(month) + "0"+String.valueOf(day)));
            }else{
                return Double.parseDouble(mjdcalc(String.valueOf(year) + "0"+String.valueOf(month) +String.valueOf(day)));
            }
        }else{
            if (day<10) {
                return Double.parseDouble(mjdcalc(String.valueOf(year) + String.valueOf(month) + "0"+String.valueOf(day)));
            }else{
                return Double.parseDouble(mjdcalc(String.valueOf(year) + String.valueOf(month) + String.valueOf(day)));
            }
        }
    }


    public static int calculate_DayOfWeek(String epoch) {

        Calendar c = Calendar.getInstance();
        String[] epoch_split = epoch.split("-");
        int year = Integer.parseInt(epoch_split[0]);
        int month = Integer.parseInt(epoch_split[1]);
        int day = Integer.parseInt(epoch_split[2]);

        c.set(year, month - 1, day);
        return (c.get(Calendar.DAY_OF_WEEK) - 1);

    }


    /**
     * Create Hector file
     * Based on Machiel's docs.
     *
     * @return            String with hector file content
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */

    public static String CreateHectorFile(String samplingperiod, ArrayList<Estimated_coordinates> data) {
        StringBuilder result= new StringBuilder();

        if (samplingperiod.equalsIgnoreCase("weekly") )
        {
            result.append("# sampling period 7.0\n");
        }
        else
        {
            result.append("# sampling period 1.0\n");

        }

        for (int i=0; i<data.size();i++){
            double mjd = data.get(i).getMJD();
            double n = 1000.0*data.get(i).getX();
            double e = 1000.0*data.get(i).getY();
            double u = 1000.0*data.get(i).getZ();
            result.append(mjd+"  "+String.format("%9.2f", n)+"  "+String.format("%9.2f", e)+"  "+String.format("%9.2f", u) + "\n");
        }
        return result.toString();
    }


    /**
     * Create Midas file
     * Based on Machiel's docs.
     *
     * @return            String with midas file content
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */
    public static String CreateMidasFile(String station, ArrayList<Estimated_coordinates> data) {
        StringBuilder result = new StringBuilder();
        for (int i=0; i<data.size();i++){
            double year = (data.get(i).getMJD()-40587.0)/365.25 + 1970;
            double n = 1.0*data.get(i).getX();
            double e = 1.0*data.get(i).getY();
            double u = 1.0*data.get(i).getZ();
            result.append(station+"  "+String.format("%04f", year)+"  "+String.format("%11.6f", n)+"  "+String.format("%11.6f", e)+"  "+String.format("%11.6f", u) + "\n");
        }
        return result.toString();
    }
}
