package Glass;

import Configuration.SiteConfig;
import org.zeroturnaround.zip.ZipException;
import org.zeroturnaround.zip.ZipUtil;
import org.zeroturnaround.zip.commons.FileUtils;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.*;
import java.lang.reflect.InvocationTargetException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.Base64;
import java.util.Date;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author José Manteigueiro
 * Version 1.1
 * @since 24/09/2018
 * Last modified: 14/11/2018
 * By: José Manteigueiro
 */
@Path("log")
public class LogFiles {
    @EJB
    private final
    SiteConfig siteConfig;

    // =========================================================================
    //  FIELDS
    // =========================================================================
    @Context
    private UriInfo context;

    public LogFiles(){
        siteConfig = lookupSiteConfigBean();
    }

    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    /**
     * <b>GET GeodesyML file by marker</b> - Returns the GeodesyML file for a station.
     *
     * @param marker Marker of the station e.g. SOPH
     * @return GeodesyML file
     * @author José Manteigueiro
     * Version 1.1
     * @since 24/09/2018
     * Last modified: 14/11/2018
     * By: José Manteigueiro
     */
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    @Path("geodesyml/{marker}")
    @GET
    public Response getGeodesyMLByMarker(@PathParam("marker") String marker) {

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        String markerlongname, filename;
        String[] markers;
        //String user_home = System.getProperty("user.home");
        String user_home = "";
        try (InputStream is = new FileInputStream("./GLASS.conf") ) {

            Properties prop = new Properties();
            prop.load(is);
            is.close();

            if(prop.getProperty("filepath") != null){
                user_home = prop.getProperty("filepath");
            }
            else{
                System.err.format("Constant filepath not defined in GLASS.conf");
                return Response.serverError().entity("Missing constant in config file.").build();
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            System.err.format("Couldn't get hash SHA-256");
            e.printStackTrace();
            return Response.noContent().entity("Something went wrong").build();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String hash = new String(Base64.getEncoder().encode(digest.digest(timeStamp.getBytes())))
                .replace("/","0")
                .replace("+","0");

        String temporary_folder = "tempEPOS"+hash;

        if(marker.contains(",")) {
            markers = marker.split(",");
        }else{
            markers = new String[1];
            markers[0] = marker;
        }

        new java.io.File(user_home + "/" + temporary_folder).mkdir();
        for (String marker1 : markers) {
            markerlongname = dbcv2.getMarkerLongName(marker1);

            if (markerlongname == null) {
                System.err.format("Markerlongname null for marker: %s.", marker1);
                return Response.noContent().entity("Couldn't determine file name for this station.").build();
            }

            filename = markerlongname + ".xml";

            new File(filename);

            if (copyFileToDirectory(user_home + "/metadata/geodesyML/" + filename, user_home + "/" + temporary_folder + "/" + filename)) {
                continue;
            } else {
                System.err.format("GeodesyML not found for marker: %s.", marker1);
            }
        }

        if(user_home.endsWith("/")){
            user_home = user_home.substring(0, user_home.length()-1);
        }

        try {
            ZipUtil.pack(new File(user_home + "/" + temporary_folder), new File(user_home + "/geodesyML-"+hash+".zip"));
            FileUtils.deleteDirectory(new File(user_home + "/" + temporary_folder));
        } catch (IOException e) {
            e.printStackTrace();
            return Response.noContent().build();
        }
        File returnFile = new File(user_home + "/geodesyML-"+hash+".zip");
        return Response.ok(returnFile, MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=geodesyML.zip")
                .build();

        //TODO: Cronjob to delete the file
    }

    /**
     * <b>GET log file by marker</b> - Returns the latest log file for a station.
     *
     * @param marker Marker of the station e.g. SOPH
     * @return GeodesyML file
     * @author José Manteigueiro
     * Version 1.1
     * @since 24/09/2018
     * Last modified: 14/11/2018
     * By: José Manteigueiro
     */
    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    @Path("{marker}")
    @GET
    public Response getLatestLogFileByMarker(@PathParam("marker") String marker){

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();

        String marker_value = null, filename, output_filename;
        String[] markers;
        String user_path = "";
        String sitelogs_relative_path = "metadata/sitelogs/";

        int counter = 0;
        boolean logs_use_markerlongname = false, logs_use_lowercase = false;
        boolean singleMarker = true;


        try (InputStream is = new FileInputStream("./GLASS.conf") ) {

            Properties prop = new Properties();
            prop.load(is);
            is.close();

            if(prop.getProperty("filepath") != null){
                user_path = prop.getProperty("filepath");
            }
            else{
                System.err.format("Constant filepath not defined in GLASS.conf");
                return Response.serverError().entity("Missing constant in config file.").build();
            }

            if(prop.getProperty("sitelogs_relative_path") != null){
                sitelogs_relative_path = prop.getProperty("sitelogs_relative_path");
            }else{
                System.err.format("Constant sitelogs_relative_path not defined in GLASS.conf, assuming as /metadata/sitelogs/.");
            }

            if(prop.getProperty("log_markerlongname") != null){
                if(prop.getProperty("log_markerlongname").equalsIgnoreCase("true") || prop.getProperty("log_markerlongname").equals("1")){
                    logs_use_markerlongname = true;
                }
            }
            else{
                System.err.format("Constant log_markerlongname not defined in GLASS.conf, assuming as false (CASC.log).");
            }

            if(prop.getProperty("log_lowercase") != null){
                if(prop.getProperty("log_lowercase").equalsIgnoreCase("true") || prop.getProperty("log_lowercase").equals("1")){
                    logs_use_lowercase = true;
                }
            }
            else{
                System.err.format("Constant log_lowercase not defined in GLASS.conf, assuming as false (CASC(...).log).");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }

        MessageDigest digest = null;
        try {
            digest = MessageDigest.getInstance("SHA-256");
        } catch (NoSuchAlgorithmException e) {
            System.err.format("Couldn't get hash SHA-256");
            e.printStackTrace();
            return Response.noContent().entity("Something went wrong").build();
        }
        String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
        String hash = new String(Base64.getEncoder().encode(digest.digest(timeStamp.getBytes())))
                .replace("/","0")
                .replace("+","0");
        String temporary_folder = "tempEPOS"+hash;

        if(marker.contains(",")) {
            markers = marker.split(",");
            singleMarker = false;
        }else{
            markers = new String[1];
            markers[0] = marker;
        }

        if(user_path.endsWith("/")){
            user_path = user_path.substring(0, user_path.length()-1);
        }

        if(sitelogs_relative_path.endsWith("/")){
            sitelogs_relative_path = sitelogs_relative_path.substring(0, sitelogs_relative_path.length()-1);
        }

        new java.io.File(user_path + "/" + temporary_folder).mkdir();
        for (String marker1 : markers) {

            // logs in the format of SOPH00FRA
            if(logs_use_markerlongname) {
                if (marker1.length() == 4) {
                    marker_value = dbcv2.getMarkerLongName(marker1);
                }
                if (marker1.length() == 9) {
                    marker_value = marker1;
                }
            }
            // logs in the format of SOPH
            else {
                if(marker1.length() == 4){
                    marker_value = marker1;
                }
                if(marker1.length() == 9){
                    marker_value = marker1.substring(0,4);
                }
            }

            if (marker_value == null) {
                System.err.format("Marker null for marker: %s.", marker1);
                return Response.noContent().entity("Couldn't determine file name for this station: " + marker1).build();
            }

            // For file_names only
            String markerlongname;
            if(logs_use_markerlongname){
                markerlongname = marker_value;
            }else{
                markerlongname = dbcv2.getMarkerLongName(marker_value);
            }

            if(logs_use_lowercase)
                marker_value = marker_value.toLowerCase();

            filename = marker_value + ".log";
            output_filename = markerlongname + ".log";

            new File(filename);

            if(singleMarker){
                File returnSingleFile = new File(user_path + "/" + sitelogs_relative_path + "/" + marker_value + "/" + filename);
                if(returnSingleFile.exists())
                    return Response.ok(returnSingleFile)
                            .header("Content-Disposition", "attachment; filename=" + output_filename)
                            .build();
                else
                    return Response.noContent().build();
            }

            if (copyFileToDirectory(user_path + "/" + sitelogs_relative_path + "/" + marker_value + "/" + filename, user_path + "/" + temporary_folder + "/" + output_filename)) {
                counter++;
                continue;
            } else {
                System.err.format("Sitelog not found for marker: %s. (%s)", marker_value, filename);
            }
        }

        if(counter == 0){
            return Response.noContent().build();
        }

        new File(user_path + "/tmp").mkdir();

        try {
            ZipUtil.pack(new File(user_path + "/" + temporary_folder), new File(user_path + "/tmp/sitelogs-"+hash+".zip"));
            FileUtils.deleteDirectory(new File(user_path + "/" + temporary_folder));
        } catch (IOException | ZipException e) {
            e.printStackTrace();
            return Response.noContent().build();
        }

        File returnFile = new File(user_path + "/tmp/sitelogs-"+hash+".zip");
        return Response.ok(returnFile, MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=siteLog.zip")
                .build();

        //TODO: Cronjob to delete the file
    }

    /**
     * Reads file and returns the StringBuilder
     *
     * @param filename fullpath
     * @return StringBuilder with the file contents
     * @throws IOException File not found
     * @author José Manteigueiro
     * Version 1.0
     * @since 24/09/2018
     * Last modified: 25/09/2018
     * By: José Manteigueiro
     */
    private StringBuilder readFile(String filename) throws IOException {
        StringBuilder data = new StringBuilder();
        String line;

        BufferedReader reader = new BufferedReader(new FileReader(filename));
        while ((line = reader.readLine()) != null)
        {
            data.append(line);
        }
        reader.close();

        return data;
    }

    /**
     * Copies a file to a directory
     *
     * @param sourcePath fullpath of source
     * @param directoryPath fullpath of directory
     * @return boolean with true(success) or false(failed)
     * @author José Manteigueiro
     * Version 1.0
     * @since 24/09/2018
     * Last modified: 25/09/2018
     * By: José Manteigueiro
     */
    private boolean copyFileToDirectory(String sourcePath, String directoryPath){
        File source = new File(sourcePath);
        File dest = new File(directoryPath);
        try {
            FileUtils.copyFile(source, dest);
        } catch (IOException e) {
            //e.printStackTrace();
            return false;
        }
        return true;
    }
}