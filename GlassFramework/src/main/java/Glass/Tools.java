package Glass;

/*
*****************************************************
**                                                 **
**      Created by Andre Rodrigues on 11/8/17     **
**                                                 **
**      andre.rodrigues@ubi.pt                     **
**                                                 **
**      Covilha, Portugal                          **
**                                                 **
*****************************************************
*/

import Configuration.SiteConfig;
import Configuration.Version;
import EposTables.Analysis_Centers;
import EposTables.Station;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.ejb.EJB;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


@SuppressWarnings("Duplicates")
@Path("tools")
public class Tools extends DataBaseT4Connection{
    @EJB
    SiteConfig siteConfig;

    public Tools() {
        this.siteConfig = lookupSiteConfigBean();
    }


    /**
     * <b>Creates auxiliary files with pre-prepared timeseries to improve response time</b> - Returns status of job.
     *
     * @return The status of the job
     * @author André Rodrigues
     * @since 31/12/2017
     * Last modified: 31/12/2017
     */
    @Path("timeseries")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getTimeseries() throws Exception {

        ArrayList<Station> stations;
        DataBaseT4Connection dc = new DataBaseT4Connection();

        String site;
        String agency;
        String sampling_period;
        String format;
        String coordinates_system;
        String reference_frame = null;
        String otl_model = null;
        String antenna_model = null;
        String cut_of_angle = null;
        String epoch_start = null;
        String epoch_end = null;
        String remove_outliers = null;
        String apply_offsets = null;

        stations = dc.getAllStationsWithProducts();

        for (Station station : stations) {

            ArrayList<Analysis_Centers> ac = dc.processingAgencyFromT4T5("timeseries", station.getId());

            for (Analysis_Centers anAc : ac) {

                site = station.getMarker();
                agency = anAc.getAbbreviation();

                sampling_period = "daily";
                format = "json";
                coordinates_system = "xyz";

                if (ScriptTimeSeries.ScriptCreatetimeseries(coordinates_system, site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries")) {

                    sampling_period = "weekly";
                    format = "json";
                    coordinates_system = "xyz";
                    ScriptTimeSeries.ScriptCreatetimeseries(coordinates_system, site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries");

                    antenna_model = "Unknown";
                    sampling_period = "weekly";
                    format = "json";
                    coordinates_system = "xyz";
                    ScriptTimeSeries.ScriptCreatetimeseries(coordinates_system, site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries");

                    otl_model = "Unknown";
                    antenna_model = null;

                    sampling_period = "weekly";
                    format = "json";
                    coordinates_system = "xyz";
                    ScriptTimeSeries.ScriptCreatetimeseries(coordinates_system, site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries");

                    otl_model = "Unknown";
                    antenna_model = "Unknown";

                    sampling_period = "weekly";
                    format = "json";
                    coordinates_system = "xyz";
                    ScriptTimeSeries.ScriptCreatetimeseries(coordinates_system, site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries");


                    otl_model = null;
                    antenna_model = null;
                    sampling_period = "daily";
                    format = "json";
                    coordinates_system = "xyz";

                    ScriptTimeSeries.ScriptCreatetimeseries(coordinates_system, site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries");

                    antenna_model = "Unknown";
                    sampling_period = "daily";
                    format = "json";
                    coordinates_system = "xyz";
                    ScriptTimeSeries.ScriptCreatetimeseries(coordinates_system, site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries");

                    otl_model = "Unknown";
                    antenna_model = null;

                    sampling_period = "daily";
                    format = "json";
                    coordinates_system = "xyz";
                    ScriptTimeSeries.ScriptCreatetimeseries(coordinates_system, site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries");

                    otl_model = "Unknown";
                    antenna_model = "Unknown";

                    sampling_period = "daily";
                    format = "json";
                    coordinates_system = "xyz";
                    ScriptTimeSeries.ScriptCreatetimeseries(coordinates_system, site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries");


                }

            }
        }

        return Response.status(Response.Status.CREATED).build();
    }


    /**
     * <b>GET GLASS VERSION</b> - Returns current version of GLASS-API.
     *
     * @return The current version of GLASS-API
     * @author Rafael Couto, P. Crocker
     * @since 14/12/2017
     * Last modified: 20/01/2018
     */
    @Path("version")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getVersion() {

        Version v = new Version();

        return v.getArtifactId()+"-"+v.getVersion()+"."+v.getBuildNumber();

    }


    /**
     * <b>GET GLASS Last Build Info</b> - Returns current build info of GLASS-API.
     *
     * @return The current build info of GLASS-API
     * @author P. Crocker
     * @since 20/01/2018
     */
    @Path("build")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public String getLastBuild() {

        Version v = new Version();

        JSONObject buildData = new JSONObject();
        buildData.put("build_date", v.getBuildDate());
        buildData.put("artifact_id", v.getArtifactId());
        buildData.put("version", v.getVersion());
        buildData.put("build_number", v.getBuildNumber());
        buildData.put("database_name", v.getDatabaseName());
        buildData.put("database_ip", v.getDatabaseIP());
        buildData.put("nodetype", v.getNodeType());

        return buildData.toJSONString();
    }

    @Path("cacheStatus")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getCacheStatus() {
        JsonCache jsC = new JsonCache();
        return jsC.cacheSatus();
    }

    /* ------------------------------------------------------------ */
    /* ------------------------------------------------------------ */
    /*                      AUXILIARY FUNCTIONS                     */
    /* ------------------------------------------------------------ */
    /* ------------------------------------------------------------ */

    /**
     * Filters timeseries to return data inbetween a set of boundary dates
     * @param timeseriesFile file with timeseries
     * @param dateStart start of timeseries
     * @param dateEnd end of timeseries
     * @return String with filtered timeseries
     * @throws IOException
     * @throws ParseException
     * @throws java.text.ParseException
     * @author André Rodrigues
     * @since 31/12/2017
     * Last modified: 09/01/2018
     */
    static String filterDate(File timeseriesFile, String dateStart, String dateEnd) throws IOException, ParseException, java.text.ParseException {

        String timeseriesFileContent = new String(Files.readAllBytes(Paths.get(timeseriesFile.toString())));
        JSONParser parser = new JSONParser();
        JSONArray timeseriesFileJSONArray = (JSONArray) parser.parse(timeseriesFileContent);

        int aux = 0;

        DateFormat format = new SimpleDateFormat("yyyyy-mm-dd hh:mm:ss");
        Date date1 = null;
        Date date2 = null;
        Date bd;

        if(dateStart != null)
        {
            aux=1;
            String datesaux = dateStart+" 00:00:00";
            date1 = format.parse(datesaux);

            if (dateEnd != null)
            {
                String dateeaux = dateEnd+" 23:59:59";
                date2 = format.parse(dateeaux);
                aux=3;
            }

        } else
        {
            if (dateEnd != null)
            {
                String dateeaux = dateEnd+" 23:59:59";
                date2 = format.parse(dateeaux);
                aux=2;
            }
        }

        JSONArray save = new JSONArray();

        for (Object timeseriesFileJSONObject : timeseriesFileJSONArray) {

            JSONObject j2 = (JSONObject) timeseriesFileJSONObject;

            String date = String.valueOf(j2.get("epoch"));
            bd = format.parse(date);

            switch (aux) {
                case 1:
                    if (bd.before(date1)) {
                        continue;
                    }
                    break;
                case 2:
                    if (bd.after(date2)) {
                        continue;
                    }
                    break;
                case 3:
                    if (!(bd.before(date1) && bd.after(date2))) {
                        continue;
                    }
                    break;
            }

            Object ob = parser.parse(timeseriesFileJSONObject.toString());
            save.add(ob);
        }
        return save.toString();
    }

    /**
     * <b>GET cache Consistency Check </b> - Checks that all the stations in the database are in the cache - errors are in the server l
     *
     * @return A string true or false
     * @author P. Crocker
     * @since 18/03/2022
     * Last modified:
     */
    @Path("cacheconsistency")
    @GET
    @Produces(MediaType.TEXT_PLAIN)
    public String getCheckConsistency() {

        String result = "Cache Consistency : "+new JsonCache().checkCacheConsistency();
        return result;

    }
}