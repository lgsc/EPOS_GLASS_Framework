/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Glass;

import Configuration.DBConsts;
import Configuration.SiteConfig;
import CustomClasses.Tuple2;
import EposTables.*;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;

/**
 *
 * @author kngo
 */
public class DataBaseT3Connection {
    
    @EJB
    SiteConfig siteConfig;
    private String myIP = null;      // This String will store the ip address of the machine
    private String DBConnectionString=null;
    
    private Connection c = null;                // Connection to EPOS database
    private Statement s = null;                 // Statement for queries on the EPOS database

    private Statement s_file_generated = null;  // Statement for the file generated queries on the EPOS database
    private ResultSet rs_file_generated = null; // Result Set for the file generated on the EPOS database
    private Statement s_file = null;            // Statement for the file queries on the EPOS database
    private ResultSet rs_file = null;           // Result Set for the file on the EPOS database
    private Statement s_file_type = null;       // Statement for the file type queries on the EPOS database
    private ResultSet rs_file_type = null;      // Result Set for the file type on the EPOS database
    private Statement s_station = null;         // Statement for the file station queries on the EPOS database
    private ResultSet rs_station = null;        // Result Set for the file station queries on the EPOS database
    private Statement s_rinex_file = null;      // Statement for the rinex file queries on the EPOS database
    private ResultSet rs_rinex_file = null;     // Result Set for the rinex file on the EPOS database
    private Statement s_quality_file = null;    // Statement for the quality file queries on the EPOS database
    private ResultSet rs_quality_file = null;   // Result Set for the quality file on the EPOS database
    private Statement s_data_center = null;     // Statement for the data center queries on the EPOS database
    private ResultSet rs_data_center = null;    // Result Set for the data center on the EPOS database
    private Statement s_data_center_structure = null;     // Statement for the data center structure queries on the EPOS database
    private ResultSet rs_data_center_structure = null;    // Result Set for the data center structure on the EPOS database

    //HUGO
    private Statement s_other_files = null;     // Statement for the other files queries on the EPOS database
    private ResultSet rs_other_files = null;    // Result Set for the other files on the EPOS database
    private Statement s_rinex_errors = null;     // Statement for the rinex errors queries on the EPOS database
    private ResultSet rs_rinex_errors = null;    // Result Set for the rinex errors on the EPOS database
    private Statement s_rinex_error_types = null;     // Statement for the rinex error types queries on the EPOS database
    private ResultSet rs_rinex_error_types = null;    // Result Set for the rinex error types on the EPOS database
    private Statement s_node = null;     
    private ResultSet rs_node = null;

    public DataBaseT3Connection() {
       this.siteConfig = lookupSiteConfigBean();
       myIP= //"10.0.7.65";
            siteConfig.getLocalIpValue();
       DBConnectionString = siteConfig.getDBConnectionString();
    }
     
    // =========================================================================
    // IP & CONNECTION WITH DATABASE
    // =========================================================================
    protected Connection NewConnection(Boolean AutoComit) throws SocketException, ClassNotFoundException, SQLException {

        // Create connection with the local database
        Class.forName("org.postgresql.Driver");
       Connection c = DriverManager.getConnection(DBConnectionString,DBConsts.DB_USERNAME, DBConsts.DB_PASSWORD);
        c.setAutoCommit(AutoComit);
        return c;
    }

    protected Connection NewConnection() throws SocketException, ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user",DBConsts.DB_USERNAME);
        props.setProperty("password",DBConsts.DB_PASSWORD);
        Connection c = DriverManager.getConnection(DBConnectionString, props);
        c.setAutoCommit(false);
        return c;
    }

    protected SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    protected Quality_file  getQualityFile_for_Rinex_id(Connection c, int rinexFileId) throws SQLException {
        Quality_file quality_file = new Quality_file();

        // *****************************************************************
        // Handle Quality File
        // *****************************************************************
        s_quality_file = c.createStatement();
        rs_quality_file = s_quality_file.executeQuery(new StringBuilder().append("SELECT * FROM ").append("quality_file WHERE id_rinexfile=").append( rinexFileId ).toString());

        while (rs_quality_file.next())
        {
            quality_file.setId(rs_quality_file.getInt("id"));
            quality_file.setName(rs_quality_file.getString("name"));
            quality_file.setFile_size(rs_quality_file.getInt("file_size"));
            quality_file.setRelative_path(rs_quality_file.getString("relative_path"));
            quality_file.setCreation_date(rs_quality_file.getString("creation_date"));
            quality_file.setRevision_time(rs_quality_file.getString("revision_time"));
            quality_file.setMd5checksum(rs_quality_file.getString("md5checksum"));
            quality_file.setStatus(rs_quality_file.getInt("status"));

            // *****************************************************************
            // Handle Data Center Structure
            // *****************************************************************
            Data_center_structure data_center_structure = new Data_center_structure();
            s_data_center_structure = c.createStatement();
            rs_data_center_structure = s_data_center_structure.executeQuery(new StringBuilder().append("SELECT * FROM ").append("data_center_structure WHERE id_data_center=").append(rs_quality_file.getInt("id_data_center_structure")).toString());
            rs_data_center_structure.next();

            data_center_structure.setId(rs_data_center_structure.getInt("id"));
            data_center_structure.setDirectory_naming(rs_data_center_structure.getString("directory_naming"));

            // *****************************************************************
            // Handle Data Center
            // *****************************************************************
            Data_center data_center = new Data_center();
            s_data_center = c.createStatement();
            rs_data_center = s_data_center.executeQuery(new StringBuilder().append("SELECT * FROM ").append("data_center WHERE id=").append(rs_quality_file.getInt("id_data_center")).toString());

            while (rs_data_center.next())
            {
                data_center.setId(rs_data_center.getInt("id"));
                data_center.setAcronym(rs_data_center.getString("acronym"));
                data_center.setHostname(rs_data_center.getString("hostname"));
                data_center.setProtocol(rs_data_center.getString("protocol"));
                data_center.setName(rs_data_center.getString("name"));
                data_center.setRoot_path(rs_data_center.getString("root_path"));
            }

            rs_data_center.close();
            s_data_center.close();

            quality_file.setData_center_structure(data_center_structure.getId(),data_center_structure.getDirectory_naming(), data_center);


            // *************************************************************
            // Handle File Type
            // *************************************************************
            File_type file_type = new File_type();
            s_file_type = c.createStatement();
            rs_file_type = s_file_type.executeQuery(new StringBuilder().append("SELECT * FROM ").append("file_type WHERE id=").append(rs_quality_file.getInt("id_file_type")).toString());

            while (rs_file_type.next())
            {
                file_type.setId(rs_file_type.getInt("id"));
                file_type.setFormat(rs_file_type.getString("format"));
                file_type.setSampling_window(rs_file_type.getString("sampling_window"));
                file_type.setSampling_frequency(rs_file_type.getString("sampling_frequency"));
            }

            rs_file_type.close();
            s_file_type.close();

            quality_file.setFile_type(file_type.getId(), file_type.getFormat(),file_type.getSampling_window(), file_type.getSampling_frequency());

            return quality_file;
        }
        return null ; //nothing found
    }

    protected QC_Report_Summary  getQC_Report_Summary_for_Rinex_Id(Connection c, int rinexFileId) throws SQLException {
        Statement s_qc_summary_report;
        ResultSet rs_qc_summary_report;
        Statement s_qc_constellation_summary;
        ResultSet rs_qc_constellation_summary;
        Statement s_qc_navigation_msg;
        ResultSet rs_qc_navigation_msg;
        Statement s_qc_observation_summary_s;
        ResultSet rs_qc_observation_summary_s;
        Statement s_qc_observation_summary_c;
        ResultSet rs_qc_observation_summary_c;
        Statement s_qc_observation_summary_d;
        ResultSet rs_qc_observation_summary_d;
        Statement s_qc_observation_summary_l;
        ResultSet rs_qc_observation_summary_l;
        // *************************************************************
        // Handle QC Summary Report
        // *************************************************************

        // Initialized statement for select
        s_qc_summary_report = c.createStatement();

        StringBuilder queryString = new StringBuilder().append("SELECT qcrepsum.*,qcparams.* FROM qc_report_summary as qcrepsum JOIN qc_parameters as qcparams ON qcrepsum.id_qc_parameters = qcparams.id WHERE qcrepsum.id_rinexfile = ").append(rinexFileId).append(";");

        rs_qc_summary_report = s_qc_summary_report.executeQuery( queryString.toString() );

        //While qc summary report
        while (rs_qc_summary_report.next())
        {

            QC_Parameters qc_parameters = new QC_Parameters(rs_qc_summary_report.getInt("id"), rs_qc_summary_report.getString("software_version"), rs_qc_summary_report.getInt("elevation_cutoff"), rs_qc_summary_report.getBoolean("gps"), rs_qc_summary_report.getBoolean("glo"), rs_qc_summary_report.getBoolean("gal"), rs_qc_summary_report.getBoolean("bds"), rs_qc_summary_report.getBoolean("qzs"), rs_qc_summary_report.getBoolean("sbs"),rs_qc_summary_report.getBoolean("nav_gps"), rs_qc_summary_report.getBoolean("nav_glo"), rs_qc_summary_report.getBoolean("nav_gal"), rs_qc_summary_report.getBoolean("nav_bds"), rs_qc_summary_report.getBoolean("nav_qzs"), rs_qc_summary_report.getBoolean("nav_sbs"), rs_qc_summary_report.getString("software_name"));
            QC_Report_Summary qc_summary_report = new QC_Report_Summary(rs_qc_summary_report.getInt("id"), rs_qc_summary_report.getInt("id_rinexfile"), qc_parameters,rs_qc_summary_report.getString("date_beg"),rs_qc_summary_report.getString("date_end"),rs_qc_summary_report.getFloat("data_smp"),rs_qc_summary_report.getFloat("obs_elev"),rs_qc_summary_report.getInt("obs_have"),rs_qc_summary_report.getInt("obs_expt"),rs_qc_summary_report.getInt("user_have"),rs_qc_summary_report.getInt("user_expt"),rs_qc_summary_report.getInt("cyc_slps"),rs_qc_summary_report.getInt("clk_jmps"),rs_qc_summary_report.getInt("xbeg"),rs_qc_summary_report.getInt("xend"),rs_qc_summary_report.getInt("xint"),rs_qc_summary_report.getInt("xsys"));

            //Initialized statement for selecting qc_constellation_summary
            s_qc_constellation_summary = c.createStatement();
            rs_qc_constellation_summary = s_qc_constellation_summary.executeQuery(new StringBuilder().append("SELECT qcconstsum.*,const.identifier_1ch, const.identifier_3ch,const.name,const.official FROM qc_constellation_summary as qcconstsum JOIN qc_report_summary as qcrepsum ON qcrepsum.id = qcconstsum.id_qc_report_summary JOIN constellation as const ON const.id = qcconstsum.id_constellation WHERE qcrepsum.id_rinexfile = ").append(rinexFileId).append(";").toString());

            while (rs_qc_constellation_summary.next()) {

                Constellation constellation = new Constellation(rs_qc_constellation_summary.getInt("id"), rs_qc_constellation_summary.getString("identifier_1ch"), rs_qc_constellation_summary.getString("identifier_3ch"), rs_qc_constellation_summary.getString("name"), rs_qc_constellation_summary.getBoolean("official"));
                QC_Constellation_Summary qc_constellation_summary = new QC_Constellation_Summary(
                        rs_qc_constellation_summary.getInt("id"),
                        rs_qc_constellation_summary.getInt("id_qc_report_summary"),
                        constellation,
                        rs_qc_constellation_summary.getInt("nsat"),
                        rs_qc_constellation_summary.getInt("xele"),
                        rs_qc_constellation_summary.getInt("epo_expt"),
                        rs_qc_constellation_summary.getInt("epo_have"),
                        rs_qc_constellation_summary.getInt("epo_usbl"),
                        rs_qc_constellation_summary.getInt("xcod_epo"),
                        rs_qc_constellation_summary.getInt("xcod_sat"),
                        rs_qc_constellation_summary.getInt("xpha_epo"),
                        rs_qc_constellation_summary.getInt("xpha_sat"),
                        rs_qc_constellation_summary.getInt("xint_epo"),
                        rs_qc_constellation_summary.getInt("xint_sat"),
                        rs_qc_constellation_summary.getInt("xint_sig"),
                        rs_qc_constellation_summary.getInt("xint_slp"),
                        rs_qc_constellation_summary.getFloat("x_crd"),
                        rs_qc_constellation_summary.getFloat("y_crd"),
                        rs_qc_constellation_summary.getFloat("z_crd"),
                        rs_qc_constellation_summary.getFloat("x_rms"),
                        rs_qc_constellation_summary.getFloat("y_rms"),
                        rs_qc_constellation_summary.getFloat("z_rms"));

                // *************************************************************
                // Handle QC Observation Summary
                // *************************************************************

                // Initialized statement for select
                s_qc_observation_summary_s = c.createStatement();
                rs_qc_observation_summary_s = s_qc_observation_summary_s.executeQuery(new StringBuilder().append("SELECT qcobssum_s.*,gnss.name, gnss.obstype, gnss.channel, gnss.frequency_band, gnss.official FROM qc_observation_summary_s as qcobssum_s JOIN qc_constellation_summary as qcconstsum ON qcobssum_s.id_qc_constellation_summary = qcconstsum.id JOIN qc_report_summary as qcrepsum ON qcrepsum.id = qcconstsum.id_qc_report_summary JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_s.id_gnss_obsnames WHERE qcrepsum.id_rinexfile = ").append(rinexFileId).append(" AND qcconstsum.id = ").append(rs_qc_constellation_summary.getInt("id")).append(";").toString());


                while (rs_qc_observation_summary_s.next())
                {
                    Gnss_Obsnames gnss_obsnames = new Gnss_Obsnames(rs_qc_observation_summary_s.getInt("id"), rs_qc_observation_summary_s.getString("name"), rs_qc_observation_summary_s.getString("obstype"), rs_qc_observation_summary_s.getString("channel"), rs_qc_observation_summary_s.getInt("frequency_band"), rs_qc_observation_summary_s.getBoolean("official"));
                    QC_Observation_Summary_S qc_observation_summary_s = new QC_Observation_Summary_S(rs_qc_observation_summary_s.getInt("id"), rs_qc_observation_summary_s.getInt("id_qc_constellation_summary"), gnss_obsnames,rs_qc_observation_summary_s.getInt("obs_sats"),rs_qc_observation_summary_s.getInt("obs_have"),rs_qc_observation_summary_s.getInt("obs_expt"),rs_qc_observation_summary_s.getInt("user_have"),rs_qc_observation_summary_s.getInt("user_expt"));
                    qc_constellation_summary.AddQCObservationSummaryS(qc_observation_summary_s);
                }
                s_qc_observation_summary_s.close();
                rs_qc_observation_summary_s.close();

                // Initialized statement for select
                s_qc_observation_summary_c = c.createStatement();
                rs_qc_observation_summary_c = s_qc_observation_summary_c.executeQuery(new StringBuilder().append("SELECT qcobssum_c.*,gnss.name, gnss.obstype, gnss.channel, gnss.frequency_band, gnss.official FROM qc_observation_summary_c as qcobssum_c JOIN qc_constellation_summary as qcconstsum ON qcobssum_c.id_qc_constellation_summary = qcconstsum.id JOIN qc_report_summary as qcrepsum ON qcrepsum.id = qcconstsum.id_qc_report_summary JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_c.id_gnss_obsnames WHERE qcrepsum.id_rinexfile = ").append( rinexFileId ).append(" AND qcconstsum.id = ").append(rs_qc_constellation_summary.getInt("id")).append(";").toString());


                while (rs_qc_observation_summary_c.next())
                {
                    Gnss_Obsnames gnss_obsnames = new Gnss_Obsnames(rs_qc_observation_summary_c.getInt("id"), rs_qc_observation_summary_c.getString("name"), rs_qc_observation_summary_c.getString("obstype"), rs_qc_observation_summary_c.getString("channel"), rs_qc_observation_summary_c.getInt("frequency_band"), rs_qc_observation_summary_c.getBoolean("official"));

                    float codval = rs_qc_observation_summary_c.getFloat("cod_mpth");
                    Float floatVal=null;
                    if (!(rs_qc_observation_summary_c.wasNull()))
                        floatVal=new Float(codval);

                    QC_Observation_Summary_C qc_observation_summary_c = new QC_Observation_Summary_C(rs_qc_observation_summary_c.getInt("id"), rs_qc_observation_summary_c.getInt("id_qc_constellation_summary"), gnss_obsnames,rs_qc_observation_summary_c.getInt("obs_sats"),rs_qc_observation_summary_c.getInt("obs_have"),rs_qc_observation_summary_c.getInt("obs_expt"),rs_qc_observation_summary_c.getInt("user_have"),rs_qc_observation_summary_c.getInt("user_expt"), floatVal );
                    qc_constellation_summary.AddQCObservationSummaryC(qc_observation_summary_c);
                }
                s_qc_observation_summary_c.close();
                rs_qc_observation_summary_c.close();

                // Initialized statement for select
                s_qc_observation_summary_d = c.createStatement();
                rs_qc_observation_summary_d = s_qc_observation_summary_d.executeQuery(new StringBuilder().append("SELECT qcobssum_d.*,gnss.name, gnss.obstype, gnss.channel, gnss.frequency_band, gnss.official FROM qc_observation_summary_d as qcobssum_d JOIN qc_constellation_summary as qcconstsum ON qcobssum_d.id_qc_constellation_summary = qcconstsum.id JOIN qc_report_summary as qcrepsum ON qcrepsum.id = qcconstsum.id_qc_report_summary JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_d.id_gnss_obsnames WHERE qcrepsum.id_rinexfile = ").append( rinexFileId ).append(" AND qcconstsum.id = ").append(rs_qc_constellation_summary.getInt("id")).append(";").toString());


                while (rs_qc_observation_summary_d.next())
                {
                    Gnss_Obsnames gnss_obsnames = new Gnss_Obsnames(rs_qc_observation_summary_d.getInt("id"), rs_qc_observation_summary_d.getString("name"), rs_qc_observation_summary_d.getString("obstype"), rs_qc_observation_summary_d.getString("channel"), rs_qc_observation_summary_d.getInt("frequency_band"), rs_qc_observation_summary_d.getBoolean("official"));
                    QC_Observation_Summary_D qc_observation_summary_d = new QC_Observation_Summary_D(rs_qc_observation_summary_d.getInt("id"), rs_qc_observation_summary_d.getInt("id_qc_constellation_summary"), gnss_obsnames,rs_qc_observation_summary_d.getInt("obs_sats"),rs_qc_observation_summary_d.getInt("obs_have"),rs_qc_observation_summary_d.getInt("obs_expt"),rs_qc_observation_summary_d.getInt("user_have"),rs_qc_observation_summary_d.getInt("user_expt"));
                    qc_constellation_summary.AddQCObservationSummaryD(qc_observation_summary_d);
                }
                s_qc_observation_summary_d.close();
                rs_qc_observation_summary_d.close();

                // Initialized statement for select
                s_qc_observation_summary_l = c.createStatement();
                rs_qc_observation_summary_l = s_qc_observation_summary_l.executeQuery(new StringBuilder().append("SELECT qcobssum_l.*,gnss.name, gnss.obstype, gnss.channel, gnss.frequency_band, gnss.official FROM qc_observation_summary_l as qcobssum_l JOIN qc_constellation_summary as qcconstsum ON qcobssum_l.id_qc_constellation_summary = qcconstsum.id JOIN qc_report_summary as qcrepsum ON qcrepsum.id = qcconstsum.id_qc_report_summary JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_l.id_gnss_obsnames WHERE qcrepsum.id_rinexfile = ").append(rinexFileId).append(" AND qcconstsum.id = ").append(rs_qc_constellation_summary.getInt("id")).append(";").toString());


                while (rs_qc_observation_summary_l.next())
                {
                    Gnss_Obsnames gnss_obsnames = new Gnss_Obsnames(rs_qc_observation_summary_l.getInt("id"), rs_qc_observation_summary_l.getString("name"), rs_qc_observation_summary_l.getString("obstype"), rs_qc_observation_summary_l.getString("channel"), rs_qc_observation_summary_l.getInt("frequency_band"), rs_qc_observation_summary_l.getBoolean("official"));
                    QC_Observation_Summary_L qc_observation_summary_l = new QC_Observation_Summary_L(rs_qc_observation_summary_l.getInt("id"), rs_qc_observation_summary_l.getInt("id_qc_constellation_summary"), gnss_obsnames,rs_qc_observation_summary_l.getInt("obs_sats"),rs_qc_observation_summary_l.getInt("obs_have"),rs_qc_observation_summary_l.getInt("obs_expt"),rs_qc_observation_summary_l.getInt("user_have"),rs_qc_observation_summary_l.getInt("user_expt"),rs_qc_observation_summary_l.getInt("pha_slps"));
                    qc_constellation_summary.AddQCObservationSummaryL(qc_observation_summary_l);
                }
                s_qc_observation_summary_l.close();
                rs_qc_observation_summary_l.close();

                qc_summary_report.AddQCConstellationSummary(qc_constellation_summary);

            }

            s_qc_constellation_summary.close();
            rs_qc_constellation_summary.close();

            //Initialized statement for selecting qc_navigation_msg
            s_qc_navigation_msg = c.createStatement();
            rs_qc_navigation_msg = s_qc_navigation_msg.executeQuery(new StringBuilder().append("SELECT qcnavmsg.*, const.identifier_1ch, const.identifier_3ch,const.name,const.official FROM qc_navigation_msg as qcnavmsg JOIN qc_report_summary as qcrepsum ON qcrepsum.id = qcnavmsg.id_qc_report_summary JOIN constellation as const ON const.id = qcnavmsg.id_constellation WHERE qcrepsum.id_rinexfile = ").append(rinexFileId).append(";").toString());

            while (rs_qc_navigation_msg.next()) {

                Constellation constellation = new Constellation(rs_qc_navigation_msg.getInt("id"), rs_qc_navigation_msg.getString("identifier_1ch"), rs_qc_navigation_msg.getString("identifier_3ch"), rs_qc_navigation_msg.getString("name"), rs_qc_navigation_msg.getBoolean("official"));
                QC_Navigation_Msg qc_navigation_msg = new QC_Navigation_Msg(
                        rs_qc_navigation_msg.getInt("id"),
                        rs_qc_navigation_msg.getInt("id_qc_report_summary"),
                        constellation,
                        rs_qc_navigation_msg.getInt("nsat"),
                        rs_qc_navigation_msg.getInt("have"));
                qc_summary_report.AddQCNavigationMsg(qc_navigation_msg);

            }

            s_qc_navigation_msg.close();
            rs_qc_navigation_msg.close();

            return qc_summary_report; //paul it seems that there is only one qc report summary per rinex fiule

        }
        return null; //error
    }
    
    // =========================================================================
    // T3 METHODS
    // =========================================================================
    /*
    * EXECUTE FILES T3 QUERY
    * This method executes a given query on the file T3 inserted on the EPOS
    * database and returns all the QC.
    * Returns an array fo Rinex Files with associated T3 data
    */
    private ArrayList executeFilesT3Query(ArrayList<Rinex_file> results, String query, 
            Connection c) throws SQLException
    {
        // *********************************************************************
        // Handle Files
        // *********************************************************************
        // Initialized statement for select
        //s = c.createStatement();
        s = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
        ResultSet rs = s.executeQuery(query);

        /* Code to get the rowcounnt
        // This is commented as it has a high cost  (the rs.last) and is only useful for debugging situations
        int rowcount = 0;
        if (rs.last()) {
            rowcount = rs.getRow();
            rs.beforeFirst(); // not rs.first() because the rs.next() below will move on, missing the first element
        }
        System.out.println("rowcount "+rowcount);
        */
        // Check if there are any results
        while (rs.next() == true)
        {
            // Create Rinex File object
            Rinex_file rinex_file = new Rinex_file();

            // *****************************************************************
            // Handle Rinex File
            // *****************************************************************
            rinex_file.setName(rs.getString("name"));
            rinex_file.setReference_date(rs.getString("reference_date"));
            rinex_file.setStatus(rs.getInt("status"));

            // *************************************************************
            // Handle QC Summary Report
            // *************************************************************
            QC_Report_Summary qc_summary_report = getQC_Report_Summary_for_Rinex_Id(c,rs.getInt("id"));
            if ( qc_summary_report != null )
                 rinex_file.setQCReportSummary(qc_summary_report);

            // *************************************************************
            // Handle Quality File : Not yet Being Used
            // *************************************************************
            Quality_file quality_file = getQualityFile_for_Rinex_id(c,rs.getInt("id"));
            if (quality_file != null )
                rinex_file.setQualityfile(quality_file);

            results.add(rinex_file);                
        }

        rs.close();
        s.close();
        
        return results;
    }
    
    /*
    * GET FILES COMBINED T3
    * This method retuns a ArrayList with all the T3 files with the given parameter.
    */
    public Tuple2 getFilesCombinedT3(String request, int pageNumber, int perPageNumber, int bad_files) throws ParseException, SocketException, ClassNotFoundException, SQLException {
        Connection connT3Combo = null;
        Connection c = NewConnection();
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        Set<Integer> results_marker = new HashSet();
        Set<Integer> results_site = new HashSet();
        Set<Integer> results_height = new HashSet();
        Set<Integer> results_latitude = new HashSet();
        Set<Integer> results_longitude = new HashSet();
        Set<Integer> results_altitude = new HashSet();
        Set<Integer> results_installed_date = new HashSet();
        Set<Integer> results_removed_date = new HashSet();
        Set<Integer> results_network = new HashSet();
        Set<Integer> results_agency = new HashSet();
        Set<Integer> results_antenna_type = new HashSet();
        Set<Integer> results_receiver_type = new HashSet();
        Set<Integer> results_radome_type = new HashSet();
        Set<Integer> results_country = new HashSet();
        Set<Integer> results_state = new HashSet();
        Set<Integer> results_city = new HashSet();
        Set<Integer> results_coordinates = new HashSet();
        Set<Integer> results_satellite = new HashSet();
        Set<Integer> results_station_type = new HashSet();
        Set<Integer> results_inverse_networks = new HashSet();
        Set<Integer> results_date_range = new HashSet();
        Set<Integer> results_published_date = new HashSet();
        Set<Integer> results_file_type = new HashSet();
        Set<Integer> results_sampling_frequency = new HashSet();
        Set<Integer> results_sampling_window = new HashSet();
        Set<Integer> results_statusfile = new HashSet();
        RequestT3 request_holder = new RequestT3(request);
        String query = "";
        int counter = 0;
        String dates_range0 = "";
        String dates_range1 = "";
        String dates_pub0 = "";
        String dates_pub1 = "";
        ResultSet rs = null;
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        try {
        
            connT3Combo = NewConnection();

            // Parse request
            request_holder.parseRequest();

            // Check marker
            for(int index=0; index<request_holder.charCode_list.size(); index++)
            {
                query = "SELECT id FROM station WHERE marker ILIKE '" + request_holder.charCode_list.get(index) + "%' ORDER BY id;";
                results_marker.addAll(dbct1.executeIDQuery(query, connT3Combo));
            }
            if (request_holder.charCode_list.size() >0 && results_marker.size() ==0) //no stations found
                return count_and_rinex; //results will be an empty array list of type Rinex_file

            // Check site
            for(int index=0; index<request_holder.siteName_list.size(); index++)
            {
                query = "SELECT id FROM station WHERE name ILIKE '" + request_holder.siteName_list.get(index) + "%' ORDER BY id;";
                results_site.addAll(dbct1.executeIDQuery(query, connT3Combo));
            }
            if (request_holder.siteName_list.size() >0 && results_site.size() ==0)  //no stations found
                return count_and_rinex; //results will be an empty array list of type Rinex_file


            // Check height
            for(int index=0; index<request_holder.height_list.size(); index++)
            {
                // Get list of Monument IDs
                query = "SELECT id FROM monument WHERE height='" + request_holder.height_list.get(index) + "' ORDER BY id;";
                ArrayList<Integer> results_monument = new ArrayList();
                results_monument = dbct1.executeIDQuery(query, connT3Combo);
                if(results_monument.size() > 0)
                {    
                    // Get list of Station IDs
                    query = "SELECT id FROM station WHERE ";
                    for (int i=0; i<results_monument.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_monument=" + results_monument.get(i);
                        else
                            query = query + " OR id_monument=" + results_monument.get(i);
                    }
                    query = query + ";";
                    results_height.addAll(dbct1.executeIDQuery(query, connT3Combo));
                }
            }
            if (request_holder.height_list.size() >0 && results_height.size() ==0)  //no stations found
                return count_and_rinex; //results will be an empty array list of type Rinex_file

            // Check latitude
            if(request_holder.latitude_list.size()>0)
            {
                if (request_holder.minLatitude!=0.0f && request_holder.maxLatitude!=0.0f)
                {
                    query = "SELECT id FROM coordinates WHERE lat>" + request_holder.minLatitude + " AND lat<" + request_holder.maxLatitude + ";";
                }else if (request_holder.minLatitude!=0.0f && request_holder.maxLatitude==0.0f){
                    query = "SELECT id FROM coordinates WHERE lat>" + request_holder.minLatitude + ";";
                }else if (request_holder.minLatitude==0.0f && request_holder.maxLatitude!=0.0f){
                    query = "SELECT id FROM coordinates WHERE lat<" + request_holder.maxLatitude + ";";
                }

                ArrayList<Integer> results_coordinatesid_lat = new ArrayList();
                results_coordinatesid_lat = dbct1.executeIDQuery(query, connT3Combo);

                if (results_coordinatesid_lat.size() > 0)
                {
                    // Get location list
                    query = "SELECT id FROM location WHERE ";
                    for (int i=0; i<results_coordinatesid_lat.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_coordinates=" + results_coordinatesid_lat.get(i);
                        else
                            query = query + " OR id_coordinates=" + results_coordinatesid_lat.get(i);
                    }
                    query = query + ";";
                    ArrayList<Integer> results_location = new ArrayList();
                    results_location = dbct1.executeIDQuery(query, connT3Combo);

                    if (results_location.size() > 0)
                    {
                        query = "SELECT * FROM station WHERE ";
                        for (int i=0; i<results_location.size(); i++)
                        {
                            if (i==0)
                                query = query + "id_location=" + results_location.get(i);
                            else
                                query = query + " OR id_location=" + results_location.get(i);
                        }
                        query = query + ";";
                        results_latitude.addAll(dbct1.executeIDQuery(query, connT3Combo));
                    }
                }
            }
            if (request_holder.latitude_list.size() >0 && results_latitude.size() ==0)  //no stations found
                return count_and_rinex; //results will be an empty array list of type Rinex_file

            // Check longitude
            if(request_holder.longitude_list.size()>0)
            {
                if ((request_holder.minLongitude!=0.0f) && (request_holder.maxLongitude!=0.0f))
                {
                    query = "SELECT id FROM coordinates WHERE lon>" + request_holder.minLongitude + " AND lon<" + request_holder.maxLongitude + ";";
                }else if (request_holder.minLongitude!=0.0f && request_holder.maxLongitude==0.0f){
                    query = "SELECT id FROM coordinates WHERE lon>" + request_holder.minLongitude + ";";
                }else if (request_holder.minLongitude==0.0f && request_holder.maxLongitude!=0.0f){
                    query = "SELECT id FROM coordinates WHERE lon<" + request_holder.maxLongitude + ";";
                }


                ArrayList<Integer> results_coordinatesid_lon = new ArrayList();
                results_coordinatesid_lon = dbct1.executeIDQuery(query, connT3Combo);

                if (results_coordinatesid_lon.size() > 0)
                {
                    // Get location list
                    query = "SELECT id FROM location WHERE ";
                    for (int i=0; i<results_coordinatesid_lon.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_coordinates=" + results_coordinatesid_lon.get(i);
                        else
                            query = query + " OR id_coordinates=" + results_coordinatesid_lon.get(i);
                    }
                    query = query + ";";
                    ArrayList<Integer> results_location = new ArrayList();
                    results_location = dbct1.executeIDQuery(query, connT3Combo);

                    if (results_location.size() > 0)
                    {
                        query = "SELECT id FROM station WHERE ";
                        for (int i=0; i<results_location.size(); i++)
                        {
                            if (i==0)
                                query = query + "id_location=" + results_location.get(i);
                            else
                                query = query + " OR id_location=" + results_location.get(i);
                        }
                        query = query + ";";
                        results_longitude.addAll(dbct1.executeIDQuery(query, connT3Combo));
                    }
                }
            }
            if (request_holder.longitude_list.size() >0 && results_longitude.size() ==0)  //no stations found
                return count_and_rinex; //results will be an empty array list of type Rinex_file

            // Check altitude
            if(request_holder.altitude_list.size()>0)
            {
                if (request_holder.minAlt!=0.0f && request_holder.maxAlt!=0.0f)
                {
                    query = "SELECT id FROM coordinates WHERE altitude>" + request_holder.minAlt + " AND altitude<" + request_holder.maxAlt + ";";
                }else if (request_holder.minAlt!=0.0f && request_holder.maxAlt==0.0f){
                    query = "SELECT id FROM coordinates WHERE altitude>" + request_holder.minAlt + ";";
                }else if (request_holder.minAlt==0.0f && request_holder.maxAlt!=0.0f){
                    query = "SELECT id FROM coordinates WHERE altitude<" + request_holder.maxAlt + ";";
                }

                ArrayList<Integer> results_coordinatesid = new ArrayList();
                results_coordinatesid = dbct1.executeIDQuery(query, connT3Combo);

                if (results_coordinatesid.size() > 0)
                {
                    // Get location list
                    query = "SELECT id FROM location WHERE ";
                    for (int i=0; i<results_coordinatesid.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_coordinates=" + results_coordinatesid.get(i);
                        else
                            query = query + " OR id_coordinates=" + results_coordinatesid.get(i);
                    }
                    query = query + ";";
                    ArrayList<Integer> results_location = new ArrayList();
                    results_location = dbct1.executeIDQuery(query, connT3Combo);

                    if (results_location.size() > 0)
                    {
                        query = "SELECT id FROM station WHERE ";
                        for (int i=0; i<results_location.size(); i++)
                        {
                            if (i==0)
                                query = query + "id_location=" + results_location.get(i);
                            else
                                query = query + " OR id_location=" + results_location.get(i);
                        }
                        query = query + ";";
                        results_altitude.addAll(dbct1.executeIDQuery(query, connT3Combo));
                    }
                }
            }
            if (request_holder.altitude_list.size() >0 && results_altitude.size() ==0)  //no stations found
                return count_and_rinex; //results will be an empty array list of type Rinex_file

            // Check installed date
            if(request_holder.installdate == true)
            {
                if (request_holder.minInstall!="" && request_holder.maxInstall!="")
                {
                    query = "SELECT id FROM station WHERE date_from>'" + request_holder.minInstall + "' AND date_from<'" + request_holder.maxInstall + "';";
                }else if (request_holder.minInstall=="" && request_holder.maxInstall!=""){
                    query = "SELECT id FROM station WHERE date_from<'" + request_holder.maxInstall + "';";
                }else if (request_holder.minInstall!="" && request_holder.maxInstall==""){
                    query = "SELECT id FROM station WHERE date_from>'" + request_holder.minInstall + "';";
                }   
                results_installed_date.addAll(dbct1.executeIDQuery(query, connT3Combo));

                if (results_installed_date.size() ==0)  //no stations found
                    return count_and_rinex; //results will be an empty array list of type Rinex_file
            }


            // Check removed date
            if(request_holder.removedate == true)
            {
                DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
                String date = dateformat.format(new Date());
                Date mydatemin = null;
                Date mydatemax = null;
                try{
                    if (request_holder.minRemoved!="")
                    {    
                        mydatemin = dateformat.parse(request_holder.minRemoved);
                    }else{
                        mydatemin = dateformat.parse(date); 
                    }
                    if (request_holder.maxRemoved!="")
                    {
                        mydatemax = dateformat.parse(request_holder.maxRemoved);
                    }else{
                        mydatemin = dateformat.parse(date);
                    }
                    Date currentdate = dateformat.parse(date);
                    if ((mydatemin.after(currentdate) || mydatemin.equals(currentdate))||(mydatemax.after(currentdate) || mydatemax.equals(currentdate)))
                    {
                        query = "SELECT id FROM station WHERE NULLIF(date_to, date_to) IS NULL;";
                        results_removed_date.addAll(dbct1.executeIDQuery(query, connT3Combo));
                    }else{
                        if (request_holder.minRemoved!="" && request_holder.maxRemoved!="")
                        {
                            query = "SELECT id FROM station WHERE date_to>'" + request_holder.minRemoved + "' AND date_to<'" + request_holder.maxRemoved + "';";
                        }else if (request_holder.minRemoved=="" && request_holder.maxRemoved!=""){
                            query = "SELECT id FROM station WHERE date_to<'" + request_holder.maxRemoved + "';";
                        }else if (request_holder.minRemoved!="" && request_holder.maxRemoved==""){
                            query = "SELECT id FROM station WHERE date_to>'" + request_holder.minRemoved + "';";
                        }
                        results_removed_date.addAll(dbct1.executeIDQuery(query, connT3Combo));
                    }

                    if (results_removed_date.size() ==0)  //no stations found
                        return count_and_rinex; //results will be an empty array list of type Rinex_file

                } catch (Exception e) {
                   throw e;
                }
            }

            /*

              The network and inverse network criteria need to be looked at carefully
              Paul 29-4-22

              All the remaining criteria should be checked for correctly returning station ids
              and if they return none then the call should end immediately
             */

            // Check network
            for(int index=0; index<request_holder.network_list.size(); index++)
            {
                query = "SELECT DISTINCT id_station FROM station_network WHERE id_network IN ("
                        + "SELECT id FROM network WHERE name ILIKE '" + request_holder.network_list.get(index) + "%');";
                
                ArrayList<Integer> results_station_ids = new ArrayList();
                results_station_ids = dbct1.executeIDStationQuery(query, connT3Combo);
                for (int i=0; i<results_station_ids.size(); i++)
                {
                    if(!results_network.contains(results_station_ids.get(i)))
                        results_network.add(results_station_ids.get(i));
                }
            }
            if(request_holder.network_list.size()>0 && results_network.size() == 0)
                return count_and_rinex;

            // Check inverse network
            for(int index=0; index<request_holder.inverse_networks_list.size(); index++)
            {
                query = "SELECT DISTINCT id_station FROM station_network WHERE id_station NOT IN (" +"" +
                        "SELECT id_station FROM station_network WHERE id_network in ( SELECT id FROM network  WHERE name ILIKE '" + request_holder.inverse_networks_list.get(index) + "%'));";
                
                ArrayList<Integer> results_station_ids = new ArrayList();
                results_station_ids = dbct1.executeIDStationQuery(query, connT3Combo);
                for (int i=0; i<results_station_ids.size(); i++)
                {
                    if(!results_inverse_networks.contains(results_station_ids.get(i)))
                        results_inverse_networks.add(results_station_ids.get(i));
                }
            }
            if(request_holder.inverse_networks_list.size()>0 && results_inverse_networks.size() == 0)
                return count_and_rinex;

            // Check agency
            for(int index=0; index<request_holder.agency_list.size(); index++)
            {
                ArrayList<Integer> results_contacts = new ArrayList();
                // Get list of Contact IDs
                query = "SELECT id FROM agency WHERE name ILIKE '" + request_holder.agency_list.get(index) + "%';";
                ArrayList<Integer> results_agency_id = new ArrayList();
                results_agency_id = dbct1.executeIDQuery(query, connT3Combo);
                if (results_agency_id.size() > 0)
                {

                    // Get list of Station IDs
                    query = "SELECT id FROM contact  WHERE ";
                    for (int i=0; i<results_agency_id.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_agency=" + results_agency_id.get(i);
                        else
                            query = query + " OR id_agency=" + results_agency_id.get(i);
                    }
                    query = query + ";";
                    results_contacts = dbct1.executeIDQuery(query, connT3Combo);

                }

                if (results_contacts.size() > 0)
                {

                    // Get list of Station IDs
                    query = "SELECT * FROM station_contact WHERE ";
                    for (int i=0; i<results_contacts.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_contact=" + results_contacts.get(i);
                        else
                            query = query + " OR id_contact=" + results_contacts.get(i);
                    }
                    query = query + ";";
                    results_agency.addAll(dbct1.executeIDStationQuery(query, connT3Combo));

                }
            }
            if(request_holder.agency_list.size()>0 && results_agency.size() == 0)
                return count_and_rinex;


            // Check antenna type
            for(int index=0; index<request_holder.antennaType_list.size(); index++)
            {
                // Get item list
                query = "SELECT item.id FROM filter_antenna\n" +
                        "INNER JOIN antenna_type ON antenna_type.id = filter_antenna.id_antenna_type \n" +
                        "INNER JOIN item_attribute ON item_attribute.id = filter_antenna.id_item_attribute \n" +
                        "INNER JOIN item ON item.id = item_attribute.id_item \n" +
                        "WHERE antenna_type.name ILIKE'" + request_holder.antennaType_list.get(index) + "'";
                ArrayList<Integer> results_item = new ArrayList();
                results_item = dbct1.executeIDQuery(query, connT3Combo);

                if(results_item.size() > 0)
                {    
                    query = "SELECT * FROM station_item WHERE ";
                    for (int i=0; i<results_item.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_item=" + results_item.get(i);
                        else
                            query = query + " OR id_item=" + results_item.get(i);
                    }
                    query = query + ";";
                    results_antenna_type.addAll(dbct1.executeIDStationQuery(query, connT3Combo));
                }
            }
            if(request_holder.antennaType_list.size()>0 && results_antenna_type.size() == 0)
                return count_and_rinex;


            // Check receiver type
            for(int index=0; index<request_holder.receiverType_list.size(); index++)
            {
                // Get item list
                query = "SELECT item.id FROM filter_receiver\n" +
                        "INNER JOIN receiver_type ON receiver_type.id = filter_receiver.id_receiver_type \n" +
                        "INNER JOIN item_attribute ON item_attribute.id = filter_receiver.id_item_attribute \n" +
                        "INNER JOIN item ON item.id = item_attribute.id_item \n" +
                        "WHERE receiver_type.name ILIKE'" + request_holder.receiverType_list.get(index) + "'";
                ArrayList<Integer> results_item = new ArrayList();
                results_item = dbct1.executeIDQuery(query, connT3Combo);

                if(results_item.size() > 0)
                {
                    query = "SELECT * FROM station_item WHERE ";
                    for (int i=0; i<results_item.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_item=" + results_item.get(i);
                        else
                            query = query + " OR id_item=" + results_item.get(i);
                    }
                    query = query + ";";
                    results_receiver_type.addAll(dbct1.executeIDStationQuery(query, connT3Combo));
                }    
            }
            if(request_holder.receiverType_list.size()>0 && results_receiver_type.size() == 0)
                return count_and_rinex;

            // Check radome type
            for(int index=0; index<request_holder.radomeType_list.size(); index++)
            {
                // Get item list
                query = "SELECT item.id FROM filter_radome\n" +
                        "INNER JOIN radome_type ON radome_type.id = filter_radome.id_radome_type \n" +
                        "INNER JOIN item_attribute ON item_attribute.id = filter_radome.id_item_attribute \n" +
                        "INNER JOIN item ON item.id = item_attribute.id_item \n" +
                        "WHERE radome_type.name ILIKE'" + request_holder.radomeType_list.get(index) + "'";
                ArrayList<Integer> results_item = new ArrayList();
                results_item = dbct1.executeIDQuery(query, connT3Combo);

                if(results_item.size() > 0)
                {
                    query = "SELECT * FROM station_item WHERE ";
                    for (int i=0; i<results_item.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_item=" + results_item.get(i);
                        else
                            query = query + " OR id_item=" + results_item.get(i);
                    }
                    query = query + ";";
                    results_radome_type.addAll(dbct1.executeIDStationQuery(query, connT3Combo));
                 }
            }
            if(request_holder.radomeType_list.size()>0 && results_radome_type.size() == 0)
                return count_and_rinex;

            // Check country
            for(int index=0; index<request_holder.country_list.size(); index++)
            {
                // Get state list
                query = "SELECT * FROM state WHERE id_country=("
                        + "SELECT id FROM country WHERE name ILIKE '" + request_holder.country_list.get(index) + "%');";
                ArrayList<Integer> results_states = new ArrayList();
                results_states = dbct1.executeIDQuery(query, connT3Combo);

                if (results_states.size() > 0)
                {
                    query = "SELECT * FROM city WHERE ";
                    // Get city list
                    for (int i=0; i<results_states.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_state=" + results_states.get(i);
                        else
                            query = query + " OR id_state=" + results_states.get(i);
                    }
                    query = query + ";";
                    ArrayList<Integer> results_cities = new ArrayList();
                    results_cities = dbct1.executeIDQuery(query, connT3Combo);

                    if (results_cities.size() > 0)
                    {
                        // Get location list
                        query = "SELECT * FROM location WHERE ";
                        for (int i=0; i<results_cities.size(); i++)
                        {
                            if (i==0)
                                query = query + "id_city=" + results_cities.get(i);
                            else
                                query = query + " OR id_city=" + results_cities.get(i);
                        }
                        query = query + ";";
                        ArrayList<Integer> results_location = new ArrayList();
                        results_location = dbct1.executeIDQuery(query, connT3Combo);

                        if (results_location.size() > 0)
                        {
                            query = "SELECT * FROM station WHERE ";
                            for (int i=0; i<results_location.size(); i++)
                            {
                                if (i==0)
                                    query = query + "id_location=" + results_location.get(i);
                                else
                                    query = query + " OR id_location=" + results_location.get(i);
                            }
                            query = query + ";";
                            results_country.addAll(dbct1.executeIDQuery(query, connT3Combo));
                        }
                    }
                }
            }
            if(request_holder.country_list.size()>0 && results_country.size() == 0)
                return count_and_rinex;

            // Check state
            for(int index=0; index<request_holder.state_list.size(); index++)
            {
                // Get state list
                query = "SELECT * FROM state WHERE name ILIKE '" + request_holder.state_list.get(index) + "%';";
                ArrayList<Integer> results_states = new ArrayList();
                results_states = dbct1.executeIDQuery(query, connT3Combo);

                if (results_states.size() > 0)
                {
                    // Get city list
                    query = "SELECT * FROM city WHERE ";
                    for (int i=0; i<results_states.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_state=" + results_states.get(i);
                        else
                            query = query + " OR id_state=" + results_states.get(i);
                    }
                    query = query + ";";
                    ArrayList<Integer> results_cities = new ArrayList();
                    results_cities = dbct1.executeIDQuery(query, connT3Combo);

                    if (results_cities.size() > 0)
                    {
                        // Get location list
                        query = "SELECT id FROM location WHERE ";
                        for (int i=0; i<results_cities.size(); i++)
                        {
                            if (i==0)
                                query = query + "id_city=" + results_cities.get(i);
                            else
                                query = query + " OR id_city=" + results_cities.get(i);
                        }
                        query = query + ";";
                        ArrayList<Integer> results_location = new ArrayList();
                        results_location = dbct1.executeIDQuery(query, connT3Combo);

                        if (results_location.size() > 0)
                        {
                            query = "SELECT * FROM station WHERE ";
                            for (int i=0; i<results_location.size(); i++)
                            {
                                if (i==0)
                                    query = query + "id_location=" + results_location.get(i);
                                else
                                    query = query + " OR id_location=" + results_location.get(i);
                            }
                            query = query + ";";
                            results_state.addAll(dbct1.executeIDQuery(query, connT3Combo));
                        }
                    }
                }
            }
            if(request_holder.state_list.size()>0 && results_state.size() == 0)
                return count_and_rinex;

            // Check city
            for(int index=0; index<request_holder.city_list.size(); index++)
            {
                // Get state list
                query = "SELECT * FROM city WHERE name ILIKE '" + request_holder.city_list.get(index) + "%';";
                ArrayList<Integer> results_cities= new ArrayList();
                results_cities = dbct1.executeIDQuery(query, connT3Combo);

                if (results_cities.size() > 0)
                {
                    // Get location list
                    query = "SELECT id FROM location WHERE ";
                    for (int i=0; i<results_cities.size(); i++)
                    {
                        if (i==0)
                            query = query + "id_city=" + results_cities.get(i);
                        else
                            query = query + " OR id_city=" + results_cities.get(i);
                    }
                    query = query + ";";
                    ArrayList<Integer> results_location = new ArrayList();
                    results_location = dbct1.executeIDQuery(query, connT3Combo);

                    if (results_location.size() > 0)
                    {
                        query = "SELECT * FROM station WHERE ";
                        for (int i=0; i<results_location.size(); i++)
                        {
                            if (i==0)
                                query = query + "id_location=" + results_location.get(i);
                            else
                                query = query + " OR id_location=" + results_location.get(i);
                        }
                        query = query + ";";
                        results_city.addAll(dbct1.executeIDQuery(query, connT3Combo));
                    }
                }
            }
            if(request_holder.city_list.size()>0 && results_city.size() == 0)
                return count_and_rinex;

            // Check coordinates
            switch (request_holder.coordinates_type)
            {
                // Handle rectangle queries
                case "rectangle":
                    ArrayList<Station> coord_rec = new ArrayList();
                    coord_rec = dbct1.getStationCoordinates(request_holder.minLat, request_holder.maxLat,
                            request_holder.minLon, request_holder.maxLon);
                    for (int i=0; i<coord_rec.size(); i++)
                        results_coordinates.add(coord_rec.get(i).getId());
                    break;

                // Handle circle queries
                case "circle":
                    ArrayList<Station> coord_cir = new ArrayList();
                    coord_cir = dbct1.getStationCoordinates(request_holder.lat, request_holder.lon,
                            request_holder.radius);
                    for (int i=0; i<coord_cir.size(); i++)
                        results_coordinates.add(coord_cir.get(i).getId());
                    break;

                // Handle polygon queries
                case "polygon":
                    ArrayList<Station> coord_pol = new ArrayList();
                    coord_pol = dbct1.getStationCoordinates(request_holder.coordinates_list);
                    for (int i=0; i<coord_pol.size(); i++)
                        results_coordinates.add(coord_pol.get(i).getId());
                    break;
            }
            if(!request_holder.coordinates_type.isEmpty() && results_coordinates.size() == 0)
                return count_and_rinex;
            
            // Check satellite
            for(int index=0; index<request_holder.satellite_list.size(); index++)
            {
                
                query = "SELECT DISTINCT st_it.id_station\n" +
                        "FROM station_item as st_it\n" +
                        "JOIN item as it ON it.id = st_it.id_item\n" +
                        "JOIN item_attribute as it_at ON it_at.id_item = it.id AND it_at.value_varchar ILIKE '%" + request_holder.satellite_list.get(index) + "%';";
                results_satellite.addAll(dbct1.executeIDStationQuery(query, connT3Combo));
            }
            if(request_holder.satellite_list.size()>0 && results_satellite.size() == 0)
                return count_and_rinex;

            // Check station type
            for(int index=0; index<request_holder.station_type_list.size(); index++)
            {
                // Query to be executed
                query = "SELECT * FROM station WHERE id_station_type=("
                        + "SELECT id FROM station_type WHERE name='" + request_holder.station_type_list.get(index) + "') "
                        + "ORDER BY id;";

                //Execute query
                results_station_type.addAll(dbct1.executeIDQuery(query, connT3Combo));
            }
            if(request_holder.station_type_list.size()>0 && results_station_type.size() == 0)
                return count_and_rinex;
            
            // Check date range
            if(request_holder.date_range_list.size()>0){    
                String strdate_range ="";
                for(int index=0; index<request_holder.date_range_list.size(); index++)
                {
                    strdate_range += request_holder.date_range_list.get(index);
                }
                
                if (strdate_range.substring(0,1).equalsIgnoreCase(",")==true)//check if the user has entered the dates_range0 
                {
                    dates_range0 = "";
                }else{
                    dates_range0 = strdate_range.substring(0,10);
                }
                
                if (strdate_range.substring(strdate_range.length() - 1).equalsIgnoreCase(",")==true)//check if the user has entered the dates_range1 
                {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();//take the current date
                    dates_range1 = dateFormat.format(date);
                }else{
                    if (strdate_range.substring(0,1).equalsIgnoreCase(",")==true)
                    {
                        dates_range1 = strdate_range.substring(1,11);
                    }else{    
                        dates_range1 = strdate_range.substring(11,21);
                    }    
                }
                if(dates_range0.equalsIgnoreCase("")==true)
                {
                    query = "SELECT id_station FROM rinex_file WHERE reference_date<='" +dates_range1+ " 23:59:59'";
                }else{
                    query = "SELECT id_station FROM rinex_file WHERE reference_date>='" + dates_range0 + "' AND reference_date<='" +dates_range1+ " 23:59:59'";
                }
                results_date_range.addAll(dbct1.executeIDStationQuery(query, connT3Combo));

                if(results_date_range.size() == 0) return count_and_rinex;
            }
            // Check published date
            if(request_holder.publish_date_list.size()>0){
                String strdate_range ="";
                for(int index=0; index<request_holder.publish_date_list.size(); index++)
                {
                    strdate_range += request_holder.publish_date_list.get(index);
                }

                if (strdate_range.substring(0,1).equalsIgnoreCase(",")==true)//check if the user has entered the dates_range0
                {
                    dates_pub0 = "";
                }else{
                    dates_pub0 = strdate_range.substring(0,10);
                }

                if (strdate_range.substring(strdate_range.length() - 1).equalsIgnoreCase(",")==true)//check if the user has entered the dates_range1
                {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();//take the current date
                    dates_pub1 = dateFormat.format(date);
                }else{
                    if (strdate_range.substring(0,1).equalsIgnoreCase(",")==true)
                    {
                        dates_pub1 = strdate_range.substring(1,11);
                    }else{
                        dates_pub1 = strdate_range.substring(11,21);
                    }
                }
                if(dates_pub0.equalsIgnoreCase("")==true)
                {
                    query = "SELECT id_station FROM rinex_file WHERE published_date<='" +dates_pub1+ " 23:59:59'";
                }else{
                    query = "SELECT id_station FROM rinex_file WHERE published_date>='" + dates_pub0 + "' AND published_date<='" +dates_pub1+ " 23:59:59'";
                }
                results_published_date.addAll(dbct1.executeIDStationQuery(query, connT3Combo));
                if(results_published_date.size() == 0) return count_and_rinex;
            }
            
            // Check file type
            for(int index=0; index<request_holder.file_type_list.size(); index++)
            {
                query = "SELECT rf.id_station FROM rinex_file as rf JOIN file_type as ft ON rf.id_file_type = ft.id WHERE ft.format = '" +
                        request_holder.file_type_list.get(index) + "'";
                results_file_type.addAll(dbct1.executeIDStationQuery(query, connT3Combo));
            }
            if(request_holder.file_type_list.size()>0 && results_file_type.size() == 0)
                return count_and_rinex;

            // Check sampling frequency
            for(int index=0; index<request_holder.sampling_frequency_list.size(); index++)
            {
                query = "SELECT rf.id_station FROM rinex_file as rf JOIN file_type as ft ON rf.id_file_type = ft.id WHERE ft.sampling_frequency = '" +
                        request_holder.sampling_frequency_list.get(index) + "'";
                results_sampling_frequency.addAll(dbct1.executeIDStationQuery(query, connT3Combo));
            }
            if(request_holder.sampling_frequency_list.size()>0 && results_sampling_frequency.size() == 0)
                return count_and_rinex;
            // Check sampling window
            for(int index=0; index<request_holder.sampling_frequency_list.size(); index++) {
                query = "SELECT rf.id_station FROM rinex_file as rf JOIN file_type as ft ON rf.id_file_type = ft.id WHERE ft.sampling_window = '" +
                        request_holder.sampling_window_list.get(index) + "'";
                results_sampling_window.addAll(dbct1.executeIDStationQuery(query, connT3Combo));
            }
            if(request_holder.sampling_frequency_list.size()>0 && results_sampling_window.size() == 0)
                return count_and_rinex;

            // Check status file
            for(int index=0; index<request_holder.statusfile_list.size(); index++)
            {
                query = "SELECT rf.id_station FROM rinex_file as rf WHERE rf.status = " +
                        Integer.parseInt(request_holder.statusfile_list.get(index));
                results_statusfile.addAll(dbct1.executeIDStationQuery(query, connT3Combo));
            }
            if(request_holder.statusfile_list.size()>0 && results_statusfile.size() == 0)
                return count_and_rinex;
            
            // Count valid parameters
            if (request_holder.charCode_list.size() > 0)
                counter++;
            if (request_holder.siteName_list.size() > 0)
                counter++;
            if (request_holder.height_list.size() > 0)
                counter++;
            if (request_holder.latitude_list.size() > 0)
                counter++;
            if (request_holder.longitude_list.size() > 0)
                counter++;
            if (request_holder.altitude_list.size() > 0)
                counter++;
            if (request_holder.installdate == true)
                counter++;
            if (request_holder.removedate == true)
                counter++;
            if (request_holder.network_list.size() > 0)
                counter++;
            if (request_holder.agency_list.size() > 0)
                counter++;
            if (request_holder.antennaType_list.size() > 0)
                counter++;
            if (request_holder.receiverType_list.size() > 0)
                counter++;
            if (request_holder.radomeType_list.size() > 0)
                counter++;
            if (request_holder.country_list.size() > 0)
                counter++;
            if (request_holder.state_list.size() > 0)
                counter++;
            if (request_holder.city_list.size() > 0)
                counter++;
            if (request_holder.coordinates_type.contentEquals("rectangle") ||
                    request_holder.coordinates_type.contentEquals("circle") ||
                    request_holder.coordinates_type.contentEquals("polygon"))
                counter++;
            if (request_holder.satellite_list.size() > 0)
                counter++;
            if (request_holder.station_type_list.size() > 0)
                counter++;
            if (request_holder.inverse_networks_list.size() > 0)
                counter++;
            if (request_holder.date_range_list.size() > 0)
                counter++;
            if (request_holder.publish_date_list.size() > 0)
                counter++;
            if (request_holder.file_type_list.size() > 0)
                counter++;
            if (request_holder.sampling_frequency_list.size() > 0)
                counter++;
            if (request_holder.sampling_window_list.size() > 0)
                counter++;
            if (request_holder.statusfile_list.size() > 0)
                counter++;

            // Merge results
            ArrayList<Integer> merged_Ids = new ArrayList();
            merged_Ids.addAll(results_marker);
            merged_Ids.addAll(results_site);
            merged_Ids.addAll(results_height);
            merged_Ids.addAll(results_latitude);
            merged_Ids.addAll(results_longitude);
            merged_Ids.addAll(results_altitude);
            merged_Ids.addAll(results_installed_date);
            merged_Ids.addAll(results_removed_date);
            merged_Ids.addAll(results_network);
            merged_Ids.addAll(results_agency);
            merged_Ids.addAll(results_antenna_type);
            merged_Ids.addAll(results_receiver_type);
            merged_Ids.addAll(results_radome_type);
            merged_Ids.addAll(results_country);
            merged_Ids.addAll(results_state);
            merged_Ids.addAll(results_city);
            merged_Ids.addAll(results_coordinates);
            merged_Ids.addAll(results_satellite);
            merged_Ids.addAll(results_station_type);
            merged_Ids.addAll(results_inverse_networks);
            merged_Ids.addAll(results_date_range);
            merged_Ids.addAll(results_file_type);
            merged_Ids.addAll(results_sampling_frequency);
            merged_Ids.addAll(results_sampling_window);
            merged_Ids.addAll(results_statusfile);



            // Check frequency of results
            ArrayList<Integer> stations_Ids = new ArrayList();
            for (int i=0; i<merged_Ids.size(); i++)
            {
                if (Collections.frequency(merged_Ids, merged_Ids.get(i)) == counter)
                    // each station to add to the final merged collection must be in all the returned sets
                    if (stations_Ids.contains(merged_Ids.get(i)) == false)
                        stations_Ids.add(merged_Ids.get(i));
            }

            //we could probably do the above better using intersections on the results_ sets that are NON-ZERO

            //should now check that if we have a station criteria that we actually have stations
            //if not then just return
            if (counter>0 && stations_Ids.size()==0)
                return count_and_rinex; //results will be an empty array list of type Rinex_file
            //

           
            query = "SELECT DISTINCT rf.id,rf.id_station,rf.name,rf.reference_date,rf.status,qf.id,qf.name,qf.id_rinexfile,qf.id_data_center_structure,qf.file_size,qf.id_file_type,qf.relative_path,qf.creation_date,qf.revision_time,qf.md5checksum,qf.status "
                        + "FROM rinex_file as rf "
                        + "LEFT JOIN quality_file as qf ON qf.id_rinexfile =rf.id "
                        + "JOIN station as st ON st.id = rf.id_station "
                        + "JOIN file_type as ft ON ft.id=rf.id_file_type "
                        + "LEFT JOIN qc_report_summary as qcrepsum ON qcrepsum.id_rinexfile = rf.id "
                        + "JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id_qc_report_summary = qcrepsum.id "
                        + "JOIN constellation as const ON const.id = qcconstsum.id_constellation "
                        + "LEFT JOIN qc_observation_summary_s as qcobssum_s ON qcobssum_s.id_qc_constellation_summary = qcconstsum.id "
                        + "LEFT JOIN qc_observation_summary_c as qcobssum_c ON qcobssum_c.id_qc_constellation_summary = qcconstsum.id "
                        + "LEFT JOIN qc_observation_summary_d as qcobssum_d ON qcobssum_d.id_qc_constellation_summary = qcconstsum.id "
                        + "LEFT JOIN qc_observation_summary_l as qcobssum_l ON qcobssum_l.id_qc_constellation_summary = qcconstsum.id "
                        + "JOIN gnss_obsnames as gnss ON (gnss.id = qcobssum_s.id_gnss_obsnames) OR (gnss.id = qcobssum_c.id_gnss_obsnames) OR (gnss.id = qcobssum_d.id_gnss_obsnames) OR (gnss.id = qcobssum_l.id_gnss_obsnames) "
                        + "WHERE";
            
            
            // Check date range
            if(request_holder.date_range_list.size()>0){    
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    if(dates_range0.equalsIgnoreCase("")==true)
                    {
                        query = query + " rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }else{
                        query = query + " rf.reference_date>='" + dates_range0 + "' AND rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }    
                }else{
                    if(dates_range0.equalsIgnoreCase("")==true)
                    {
                        query = query + " AND rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }else{
                        query = query + " AND rf.reference_date>='" + dates_range0 + "' AND rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }    
                }    

            }
            // Check Published Date
            if(request_holder.publish_date_list.size()>0){
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    if(dates_pub0.equalsIgnoreCase("")==true)
                    {
                        query = query + " rf.published_date<='" +dates_pub1 + " 23:59:59" + "'";
                    }else{
                        query = query + " rf.published_date>='" + dates_pub0 + "' AND rf.published_date<='" +dates_pub1 + " 23:59:59" + "'";
                    }
                }else{
                    if(dates_pub0.equalsIgnoreCase("")==true)
                    {
                        query = query + " AND rf.published_date<='" +dates_pub1 + " 23:59:59" + "'";
                    }else{
                        query = query + " AND rf.published_date>='" + dates_pub0 + "' AND rf.published_date<='" +dates_pub1 + " 23:59:59" + "'";
                    }
                }

            }
            
            // Check file type
            if(request_holder.file_type_list.size()>0){
                for(int index=0; index<request_holder.file_type_list.size(); index++)
                {
                    
                    if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    {
                        query = query + " ft.format ='" + request_holder.file_type_list.get(index) + "'";
                    }else{
                        query = query + " AND ft.format ='" + request_holder.file_type_list.get(index) + "'";
                    } 

                }
            }    
            // Check sampling frequency
            if(request_holder.sampling_frequency_list.size()>0){
                for(int index=0; index<request_holder.sampling_frequency_list.size(); index++)
                {
                    if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    {
                        query = query + " ft.sampling_frequency ='" + request_holder.sampling_frequency_list.get(index) + "'";
                    }else{
                        query = query + " AND ft.sampling_frequency ='" + request_holder.sampling_frequency_list.get(index) + "'";
                    }

                }
            }
            // Check sampling window
            if(request_holder.sampling_window_list.size()>0){
                for(int index=0; index<request_holder.sampling_window_list.size(); index++)
                {
                    if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    {
                        query = query + " ft.sampling_window ='" + request_holder.sampling_window_list.get(index) + "'";
                    }else{
                        query = query + " AND ft.sampling_window ='" + request_holder.sampling_window_list.get(index) + "'";
                    }

                }
            }

            // Check data availability
            if(request_holder.data_availability_value!=0.0)
            {
                if (stations_Ids.isEmpty()&&counter==0)
                {
                    String queryallstation = "Select id from station";
                    try {
                                s = connT3Combo.createStatement();
                                rs = s.executeQuery(queryallstation);
                                while (rs.next())
                                {
                                    stations_Ids.add(rs.getInt("id"));
                                }
                                
                                
                        } catch (SQLException e) {
                                throw e;
                        }
                }    
                    
                for (int i=stations_Ids.size()-1; i>=0; i--)//inverse loop because possibility to remove from arraylist to avoid to disturb the index     
                {

                    if(request_holder.date_range_list.size()>0)
                    {
                        if(dates_range0.equalsIgnoreCase("")==true)//the user hasn't entered the dates_range0
                        {    
                            String queryfirstdate = "Select date_from from station where id=" + stations_Ids.get(i);
                            s = connT3Combo.createStatement();
                            rs = s.executeQuery(queryfirstdate);
                            rs.next();
                            dates_range0 = rs.getString(1).substring(0,10);
                        }
                        LocalDate startDate = LocalDate.parse(dates_range0);
                        LocalDate endtDate = LocalDate.parse(dates_range1);

                        int nbtotaldaysbetween = (int)ChronoUnit.DAYS.between(startDate, endtDate) + 1;//100%

                        String querycount = "Select COUNT(DISTINCT reference_date) from  rinex_file where reference_date >='" + dates_range0 + "' AND reference_date <='" + dates_range1 + " 23:59:59" + "' AND id_station=" + stations_Ids.get(i);
                        try {
                            s = connT3Combo.createStatement();
                            rs = s.executeQuery(querycount);
                            rs.next();
                            int nbfiles = rs.getInt(1);
                            int data_availability_percent = 0;
                            if(nbfiles>=nbtotaldaysbetween)
                            {
                                data_availability_percent = 100;
                            }else{    
                                data_availability_percent = (nbfiles * 100)/nbtotaldaysbetween;
                            }    

                            if(data_availability_percent < request_holder.data_availability_value)
                            {
                                stations_Ids.remove(stations_Ids.get(i));
                            }


                        } catch (SQLException e) {
                            throw e;
                        }

                    }else{//the user hasn't entered dates_range0 and dates_range1
                        String queryextremedate = "Select date_from,date_to from station where id=" + stations_Ids.get(i);
                        try {
                                s = connT3Combo.createStatement();
                                rs = s.executeQuery(queryextremedate);
                                String enddate = "";
                                while (rs.next())    
                                {
                                    if(rs.getString(2)==null)
                                    {
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                        enddate = df.format(new Date());
                                    }else{
                                        enddate = rs.getString(2).substring(0,10);
                                    }
                                    String begindate = rs.getString(1).substring(0,10);                                
                                    LocalDate startDate = LocalDate.parse(begindate);
                                    LocalDate endtDate = LocalDate.parse(enddate);
                                    int nbtotaldaysbetween = (int)ChronoUnit.DAYS.between(startDate, endtDate) + 1;//100%
                                    String querycount = "Select COUNT(DISTINCT reference_date) from rinex_file where id_station=" + stations_Ids.get(i);
                                    rs = s.executeQuery(querycount);
                                    rs.next();
                                    int nbfiles = rs.getInt(1);
                                    int data_availability_percent = 0;
                                    if(nbfiles>=nbtotaldaysbetween)
                                    {
                                        data_availability_percent = 100;
                                    }else{    
                                        data_availability_percent = (nbfiles * 100)/nbtotaldaysbetween;
                                    }

                                    if(data_availability_percent < request_holder.data_availability_value)
                                    {
                                        stations_Ids.remove(stations_Ids.get(i));
                                    }
                                }

                            } catch (SQLException e) {
                                throw e;
                            }
                    }
                    rs.close();
                    s.close();
                        
                }
                
            }
            
            // Check minimum observation
            if(request_holder.minimumObservation>0.0)
            {
                if (stations_Ids.isEmpty()&&counter==0)
                {
                    String queryallstation = "Select id from station";
                    try {
                                s = connT3Combo.createStatement();
                                rs = s.executeQuery(queryallstation);
                                while (rs.next())
                                {
                                    stations_Ids.add(rs.getInt("id"));
                                }
                                
                        } catch (SQLException e) {
                                throw e;
                        }
                }
                String querycount = "";
                for (int i=stations_Ids.size()-1; i>=0; i--)//inverse loop because possibility to remove from arraylist to avoid to disturb the index     
                {
                    if(request_holder.date_range_list.size()>0)
                    {
                        if(dates_range0.equalsIgnoreCase("")==true)//the user hasn't entered the dates_range0
                        {    
                            String queryfirstdate = "Select date_from from station where id=" + stations_Ids.get(i);
                            s = connT3Combo.createStatement();
                            rs = s.executeQuery(queryfirstdate);
                            rs.next();
                            dates_range0 = rs.getString(1).substring(0,10);
                        }
                        querycount = "Select COUNT(DISTINCT reference_date) from  rinex_file where reference_date >='" + dates_range0 + "' AND reference_date <='" + dates_range1 + " 23:59:59" + "' AND id_station=" + stations_Ids.get(i);
                        
                    }else{
                        querycount = "Select COUNT(DISTINCT reference_date) from rinex_file where id_station=" + stations_Ids.get(i);
                    }    
                    try {
                            s = connT3Combo.createStatement();
                            rs = s.executeQuery(querycount);

                            while (rs.next())    
                            {
                                if(rs.getInt(1)>0)
                                {
                                    int nbfiles = rs.getInt(1);
                                    float nbyear = (float)nbfiles/365;

                                    if(nbyear < request_holder.minimumObservation)
                                    {    
                                        stations_Ids.remove(stations_Ids.get(i));
                                    }    

                                }else{
                                    stations_Ids.remove(stations_Ids.get(i));
                                }
                            }

                        } catch (SQLException e) {
                            throw e;
                        }
                    
                    rs.close();
                    s.close();    
                }
                
            }
            
            // Check status of file
            if(request_holder.statusfile_list.size()>0){
                for(int index=0; index<request_holder.statusfile_list.size(); index++)
                {
                    if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    {
                        query = query + " rf.status =" + Integer.parseInt(request_holder.statusfile_list.get(index));
                    }else{
                        query = query + " AND rf.status =" + Integer.parseInt(request_holder.statusfile_list.get(index));
                    }
                }
            }
            
            ///////////////T3////////////////////
            // Check constellation
            for(int index=0; index<request_holder.constellation_list.size(); index++)
            {   
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " const.name = '" + request_holder.constellation_list.get(index) + "'";
                }else{
                    query = query + " AND const.name = '" + request_holder.constellation_list.get(index) + "'";
                }
            }

            // Check frequency
            for(int index=0; index<request_holder.frequency_list.size(); index++)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " gnss.frequency_band = '" + request_holder.frequency_list.get(index) + "'";
                }else{
                    query = query + " AND gnss.frequency_band = '" + request_holder.frequency_list.get(index) + "'";
                }
            }
            
            // Check channel
            for(int index=0; index<request_holder.channel_list.size(); index++)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {if (request_holder.channel_list.get(index).equalsIgnoreCase("No Attr"))
                    {
                        query = query + " gnss.channel IS NULL";
                    }else{    
                        query = query + " gnss.channel = '" + request_holder.channel_list.get(index) + "'";
                    }    
                }else{
                    if (request_holder.channel_list.get(index).equalsIgnoreCase("No Attr"))
                    {
                        query = query + " AND gnss.channel IS NULL";
                    }else{
                        query = query + " AND gnss.channel = '" + request_holder.channel_list.get(index) + "'";
                    } 
                }
            }
            
            // Check observation type
            for(int index=0; index<request_holder.observationtype_list.size(); index++)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    if (request_holder.observationtype_list.get(index).equalsIgnoreCase("P/C"))
                    {    
                        query = query + " (gnss.obstype = 'P' OR gnss.obstype = 'C')";
                    }else{
                        query = query + " gnss.obstype = '" + request_holder.observationtype_list.get(index) + "'";
                    }    
                }else{
                    if (request_holder.observationtype_list.get(index).equalsIgnoreCase("P/C"))
                    {    
                        query = query + " AND (gnss.obstype = 'P' OR gnss.obstype = 'C')";
                    }else{
                        query = query + " AND gnss.obstype = '" + request_holder.observationtype_list.get(index) + "'";
                    }    
                }  
            }
            
            // Check epoch completeness
            if(request_holder.ratioepoch>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " (qcrepsum.obs_have * 100)/qcrepsum.obs_expt > " + request_holder.ratioepoch;
                }else{
                    query = query + " AND (qcrepsum.obs_have * 100)/qcrepsum.obs_expt > " + request_holder.ratioepoch;
                }
            }
            
            // Check elevation angle
            if(request_holder.elevangle>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " qcrepsum.obs_elev < " + request_holder.elevangle;
                }else{
                    query = query + " AND qcrepsum.obs_elev < " + request_holder.elevangle;
                }
            }
            
            // Check Multipath
            if(request_holder.multipathvalue>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " ((gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) AND (SELECT AVG(qcobssum_c.cod_mpth)\n" +
                        "FROM qc_observation_summary_c as qcobssum_c\n" +
                        "JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id = qcobssum_c.id_qc_constellation_summary\n" +
                        "JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_c.id_gnss_obsnames\n" +
                        "JOIN constellation as const ON const.id = qcconstsum.id_constellation\n" +
                        "WHERE (gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) < " + request_holder.multipathvalue;
                }else{
                    query = query + " AND ((gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) AND (SELECT AVG(qcobssum_c.cod_mpth)\n" +
                        "FROM qc_observation_summary_c as qcobssum_c\n" +
                        "JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id = qcobssum_c.id_qc_constellation_summary\n" +
                        "JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_c.id_gnss_obsnames\n" +
                        "JOIN constellation as const ON const.id = qcconstsum.id_constellation\n" +
                        "WHERE (gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) < " + request_holder.multipathvalue;
                }
            }
            
            // Check cycle slips
            if(request_holder.nbcycleslips>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " qcrepsum.cyc_slps = " + request_holder.nbcycleslips;
                }else{
                    query = query + " AND qcrepsum.cyc_slps = " + request_holder.nbcycleslips;
                }
            }
            
            // Check clock jumps
            if(request_holder.nbclockjumps>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " qcrepsum.clk_jmps = " + request_holder.nbclockjumps;
                }else{
                    query = query + " AND qcrepsum.clk_jmps = " + request_holder.nbclockjumps;
                }
            }
            
            // Check SPP RMS
            if(request_holder.spprms>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " const.name = 'GPS' AND sqrt((qcconstsum.x_rms*qcconstsum.x_rms) + (qcconstsum.y_rms*qcconstsum.y_rms) + (qcconstsum.z_rms*qcconstsum.z_rms)) < " + request_holder.spprms;
                }else{
                    query = query + " AND const.name = 'GPS' AND sqrt((qcconstsum.x_rms*qcconstsum.x_rms) + (qcconstsum.y_rms*qcconstsum.y_rms) + (qcconstsum.z_rms*qcconstsum.z_rms)) < " + request_holder.spprms;
                }
            }

            if (stations_Ids.size() > 0)
            {
                
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    for (int i=0; i<stations_Ids.size(); i++)
                    {
                        s = connT3Combo.createStatement();
                        rs = s.executeQuery("SELECT marker FROM station WHERE id=" + + stations_Ids.get(i));//find marker because id station is not the same on each node
                        while (rs.next())
                        {
                            if (i==0)
                                query = query + " st.marker='" + rs.getString("marker") + "'";
                            else
                                query = query + " OR st.marker='" + rs.getString("marker") + "'";
                        }
                        
                    }    
                }else{
                    for (int i=0; i<stations_Ids.size(); i++)
                    {
                        s = connT3Combo.createStatement();
                        rs = s.executeQuery("SELECT marker FROM station WHERE id=" + + stations_Ids.get(i));
                        while (rs.next())
                        {
                            if (i==0)
                                query = query + " AND (st.marker='" + rs.getString("marker") + "'";
                            else
                                query = query + " OR st.marker='" + rs.getString("marker") + "'";
                        }
                        
                    }
                    query = query + ")";
                }
                    
            }
            if(bad_files == 1){
               query = query + " and rf.status > 0";
            }
            String modifiedQuery = query.replaceFirst("SELECT.*?FROM", "SELECT COUNT(*) FROM");
            try (Statement stmt2 = c.createStatement();
                 ResultSet rs2 = stmt2.executeQuery(modifiedQuery)) {
                if (rs2.next()) {
                    totalCount = rs2.getInt(1);
                }
            }

            query = query + " ORDER BY rf.reference_date ";
           if (pageNumber>0){
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY;" );
            }else{
            query = query +(";");
            }

            
            String querynode = "SELECT DISTINCT no.host,no.port,no.dbname,no.username,no.password "
                            + "FROM node as no "
                            + "JOIN connections as cx ON cx.destiny=no.id "
                            + "JOIN station as st ON st.id=cx.station "
                            + "WHERE cx.metadata='T1'";

            if (stations_Ids.size() > 0)
            {
                for (int i=0; i<stations_Ids.size(); i++)
                {
                    if (i==0)
                        querynode = querynode + " AND (cx.station=" + stations_Ids.get(i);
                    else
                        querynode = querynode + " OR cx.station=" + stations_Ids.get(i);
                }
                querynode = querynode + ")";
            }

            querynode = querynode + ";";


            s_node = connT3Combo.createStatement();
            rs_node = s_node.executeQuery(querynode);
            Connection cnode = null;
            while (rs_node.next())
            {
                try{ 
                    Class.forName("org.postgresql.Driver");
                    String url = "jdbc:postgresql://" + rs_node.getString("host") + ":" + rs_node.getString("port") + "/" + rs_node.getString("dbname");
                    Properties props = new Properties();
                    props.setProperty("user",rs_node.getString("username"));
                    props.setProperty("password",rs_node.getString("password"));
                    props.setProperty("loginTimeout","2");
                    cnode = DriverManager.getConnection(url, props);
                    cnode.setAutoCommit(false);
                    ArrayList<Rinex_file> results_from_node = new ArrayList();
                    results_from_node = executeFilesT3Query(results_from_node, query, cnode);//Execute from each leaf node
                    for(int i=0; i<results_from_node.size(); i++)
                    {
                        results.add(results_from_node.get(i));
                    }
                }catch (Exception e) {
                    System.out.println("Unable to connect to DB");
                    throw e;
                }    
                //Execute from sub level node
                /*Statement s_subnode = cnode.createStatement();
                ResultSet rs_subnode = s_subnode.executeQuery(querynode);
                while (rs_subnode.next())
                {
                    Class.forName("org.postgresql.Driver");
                    Connection csubnode = DriverManager.getConnection("jdbc:postgresql://" + rs_subnode.getString("host") + ":" + rs_subnode.getString("port") + "/" + rs_subnode.getString("dbname"),rs_subnode.getString("username"), rs_subnode.getString("password"));
                    cnode.setAutoCommit(false);
                    ArrayList<Rinex_file> results_from_subnode = new ArrayList();
                    results_from_subnode = executeFilesT3Query(results_from_subnode, query, csubnode);//Execute from each leaf node
                    for(int i=0; i<results_from_subnode.size(); i++)
                    {
                        results.add(results_from_subnode.get(i));
                    }    
                }
                rs_subnode.close();
                s_subnode.close();*/
            }
            
            rs_node.close();
            s_node.close();

            //Execute from itself
            ArrayList<Rinex_file> results_from_node = new ArrayList();
            results_from_node = executeFilesT3Query(results_from_node, query, connT3Combo);
            for(int i=0; i<results_from_node.size(); i++)
            {
                results.add(results_from_node.get(i));
            }    
            //results = executeFilesT3Query(results, query, c);
           
            
        } catch (SocketException e) {
            throw e;
        } catch (ClassNotFoundException e) {
            throw e;
        } catch (SQLException e) {
            throw e;
        }
        finally {
            if (connT3Combo != null) {
                try {
                    connT3Combo.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT3Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
}
