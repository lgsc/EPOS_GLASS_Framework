package Glass;

import Configuration.DBConsts;
import Configuration.SiteConfig;
import CustomClasses.VelocityByPlateAndAc;
import CustomClasses.VelocityField;
import EposTables.*;
import Geometry.Point;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.net.SocketException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.Date;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static Glass.GlassConstants.oldDBVersion;

/**
 * DATABASE CONNECTION T1
 * This class is responsible for the EPOS database connection and provides several
 * methods for executing queries on the database.
 */
class DataBaseConnection {

    private Statement s = null;                 // Statement for queries on the EPOS database
    private ResultSet rs = null;                // Result Set for the queries on the EPOS database

    @EJB
    private SiteConfig siteConfig;
    private String DBConnectionString;

    DataBaseConnection() {
        this.siteConfig = lookupSiteConfigBean();
        DBConnectionString = siteConfig.getDBConnectionString();
    }

    private SiteConfig lookupSiteConfigBean() {
        try {
            Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    
    /**
     * EXECUTE FILE QUERY
     * This method executes a given query and returns a Json object of reference date list.
     */
    Map<Integer, String> executeFilesQuery(String query, Connection c)
    {
        Map<Integer, String> map = new HashMap<Integer, String>();
 
        try {
            s = c.createStatement();
            rs = s.executeQuery(query);
            
            while (rs.next())
            {
               int id_station = Integer.parseInt(rs.getString("id")); 
               map.put(id_station, rs.getString("list_reference_date"));   
            }

            rs.close();
            s.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return map;
    }

    /**
     * GET ALL ID STATIONS
     * This method returns a ArrayList of id stations on the database.
     */
    ArrayList<Integer> getAllIdStations(){
        return getAllIdStations(null);
    }

    ArrayList<Integer> getAllIdStations(Connection c)
    {
        boolean persistConnection=false;
        // ArrayList with the results of the query
        ArrayList<Integer> results = new ArrayList<>();

        try {
            if (null==c)
                c = NewConnection();
            else
                persistConnection=true;
            // Query to be executed
            String query = "SELECT id FROM station ORDER BY id";
            //Execute query
            results = executeIDQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null & !persistConnection) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }
    /**
     * GET ALL Stations names
     * This method returns a ArrayList of id stations on the database.
     */
    public ArrayList<Station> getStationsName(String query, Connection c, int stationID) {
        ArrayList<Station> ret = new ArrayList<>();
        PreparedStatement pst = null;
        try {
            if (stationID != 0) {
                pst = c.prepareStatement(query);
                pst.setInt(1, stationID);
                rs = pst.executeQuery();
            }
            else
            {
                s = c.createStatement();
                rs = s.executeQuery(query);
            }


            while (rs.next())
            {
                Station res = new Station();
                res.setId(rs.getInt("id"));
                res.setName(rs.getString("name"));
                res.setMarkerLongName(rs.getString("markerlongname"));
                ret.add(res);
            }

        }
        catch (SQLException ex){
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (s != null) {
                try {
                    s.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }

        return ret;
    }

    
    /**
     * EXECUTE STATION QUERY
     * This method executes a given query on the stations inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    ArrayList<Station> executeStationQuery(String query, Connection c) {


        ArrayList<Station> results = new ArrayList<>();

        Statement s_local;           // Statement for queries on the EPOS database
        ResultSet rs;                // Result Set for the queries on the EPOS database
        Statement s_conditions;      // Statement for the condition queries on the EPOS database
        ResultSet rs_conditions;     // Result Set for the condition queries on the EPOS database
        Statement s_local_ties;      // Statement for the local_ties queries on the EPOS database
        ResultSet rs_local_ties;     // Result Set for the local_ties queries on the EPOS database
        Statement s_coll_inst;       // Statement for the Instrument_collocation queries on the EPOS database
        ResultSet rs_coll_inst;      // Result Set for the Instrument_collocation queries on the EPOS database
        Statement s_user_group_st;   // Statement for the user_group_station queries on the EPOS database
        ResultSet rs_user_group_st;  // Result Set for the user_group_station queries on the EPOS database
        Statement s_log;             // Statement for the log queries on the EPOS database
        ResultSet rs_log;            // Result Set for the log queries on the EPOS database
        Statement s_station_cont;    // Statement for the station contacts queries on the EPOS database
        ResultSet rs_station_cont;   // Result Set for the station contacts queries on the EPOS database
        Statement s_station_net;     // Statement for the station network queries on the EPOS database
        ResultSet rs_station_net;    // Result Set for the station network queries on the EPOS database
        Statement s_station_item;    // Statement for the station item queries on the EPOS database
        ResultSet rs_station_item;   // Result Set for the station item queries on the EPOS database
//        Statement s_item;            // Statement for the item queries on the EPOS database
//        ResultSet rs_item;           // Result Set for the item queries on the EPOS database
        Statement s_item_attribute;  // Statement for the item attribute queries on the EPOS database
        ResultSet rs_item_attribute; // Result Set for the item attribute queries on the EPOS database
        Statement s_document;        // Statement for the document queries on the EPOS database
        ResultSet rs_document;       // Result Set for the document on the EPOS database
        Statement s_station_col;     // Statement for the station collocation queries on the EPOS database
        ResultSet rs_station_col;    // Result Set for the station collocation on the EPOS database
        Statement s_col_offset;      // Statement for the collocation offset queries on the EPOS database
        ResultSet rs_col_offset;     // Result Set for the collocation offset on the EPOS database
        Statement s_s_colocated;     // Statement for the station colocated queries on the EPOS database
        ResultSet rs_s_colocated;    // Result Set for the station colocated on the EPOS database
        Statement s_network;         // Statement for the network queries on the EPOS database
        ResultSet rs_network;        // Result Set for the network queries on the EPOS database
        Statement s_multi;
        ResultSet rs_multi;
        Statement s_rinex_files;        // Statement for the rinex_files queries on the EPOS database
        ResultSet rs_rinex_files;       // Result Set for the rinex_files queries on the EPOS database
        City city;                      // Auxiliary object for City
        State state;                    // Auxiliary object for State
        Country country;                // Auxiliary object for Country
        Coordinates coordinates;        // Auxiliary object for Coordinates
        Tectonic tectonic;              // Auxiliary object for Tectonic
        Bedrock bedrock;                // Auxiliary object for Bedrock
        String ref_dates;

        Statement s_markerlongname;        // Statement for the makerlongname
        ResultSet rs_markerlongname;       // Result Set for the markerlongname



        // *********************************************************************
        // STEP 1
        // Get the IP address of the machine.
        // *********************************************************************
        //getLocalIP();

        // *********************************************************************
        // STEP 2
        // Check data from query.
        // *********************************************************************
        try {

            // Initialized statement for select
            s_local = c.createStatement();
            rs = s_local.executeQuery(query);

            // Check if there are any queries
            while (rs.next()) {

                // Create Station object
                Station s = new Station();
                s.setId(rs.getInt("id"));
                s.setName(rs.getString("name"));
                s.setMarker(rs.getString("marker"));
                s.setDescription(rs.getString("description"));
                s.setDate_from(rs.getString("date_from"));
                s.setDate_to(rs.getString("date_to"));
                s.setComment(rs.getString("comment"));
                s.setIers_domes(rs.getString("iers_domes"));
                s.setCpd_num(rs.getString("cpd_num"));
                s.setMonument_num(rs.getInt("monument_num"));
                s.setReceiver_num(rs.getInt("receiver_num"));
                s.setCountry_code(rs.getString("country_code"));


                //
                //Marker Long Name (9 char)
                //
                s_markerlongname = c.createStatement();
                rs_markerlongname = s_markerlongname.executeQuery("SELECT markerlongname('" +
                        rs.getString("marker") + "'," + String.valueOf(rs.getInt("monument_num")) + "," + String.valueOf(rs.getInt("receiver_num")) + ",'"
                        + String.valueOf(rs.getString("country_code")) + "') FROM station;");

                while (rs_markerlongname.next()) {
                    s.setMarkerLongName(rs_markerlongname.getString("markerlongname"));
                    break;
                }

                rs_markerlongname.close();
                s_markerlongname.close();




                // ___________________
                // RINEX FILES (02/05/2018)
                //
           /*     s_rinex_files = c.createStatement();
                rs_rinex_files = s_rinex_files.executeQuery("SELECT array_to_string(array_agg(distinct(rf.reference_date)), ',') as reference_dates FROM rinex_file rf WHERE rf.id_station =" + s.getId() +";");
                while(rs_rinex_files.next()) {
                    ref_dates = rs_rinex_files.getString("reference_dates");
                    if(!(ref_dates == null))
                        s.addRinexFiles(ref_dates);
                }

                rs_rinex_files.close();
                s_rinex_files.close();*/
                // ___________________
                
                //-----------------------
                // RINEX FILES (22/11/2018)
                String reference_dates_string = ""; 
                s_rinex_files = c.createStatement();

                rs_rinex_files = s_rinex_files.executeQuery("(SELECT MIN(reference_date) as res\n" +
                                                                "FROM rinex_file\n" +
                                                                "WHERE id_station = " + rs.getInt("id") + "\n" +
                                                                " )\n" +
                                                                "UNION ALL\n" +
                                                                "(select unnest(array[r.reference_date,r.reference_date + (next_date - reference_date)]) as my_date\n" +
                                                                "from (select r.*, lead(reference_date) over (order by reference_date) as next_date\n" +
                                                                "      from rinex_file r where id_station = " + rs.getInt("id") + "\n" +
                                                                "     ) r\n" +
                                                                "where next_date > reference_date + interval '1 day'\n" +
                                                                ")\n" +
                                                                "UNION ALL\n" +
                                                                "(SELECT MAX(reference_date)\n" +
                                                                "FROM rinex_file\n" +
                                                                "WHERE id_station = " + rs.getInt("id") + "\n" +
                                                                ")");
                //int cnt=0;
                while(rs_rinex_files.next()) {
                    reference_dates_string += rs_rinex_files.getString("res") + ",";
                    //cnt++;
                }
                //System.out.println("Counted file "+cnt);
                reference_dates_string = reference_dates_string.substring(0, reference_dates_string.length() - 1);
                s.addRinexFiles(reference_dates_string);
                reference_dates_string = "";
                rs_rinex_files.close();
                s_rinex_files.close();
                
                //Number of rinex files
                    
                Statement s_nbfiles = c.createStatement();
                ResultSet rs_nbfiles = s_nbfiles.executeQuery("select count(reference_date) from rinex_file where id_station =" + rs.getInt("id")+";");
                rs_nbfiles.next();                                            
                int nbrinex = rs_nbfiles.getInt(1);
                s.setNbRinexFiles(nbrinex);
                
                s_nbfiles.close();
                rs_nbfiles.close();
                //----------------------    
                
                // Initialized statement for select
                s_multi = c.createStatement();

                rs_multi = s_multi.executeQuery(new StringBuilder().append("SELECT loc.description as loc_desc, tec.plate_name as plate_name, coord.x, coord.y, coord.z, coord.lat, coord.lon, coord.altitude, cit.name as city, stat.name as state, count.name as country, count.iso_code, mon.description as mon_desc, mon.inscription, mon.height, mon.foundation, mon.foundation_depth, stt.name as stt_name, stt.type as stt_type, geo.characteristic, geo.fracture_spacing, geo.fault_zone, geo.distance_to_fault, bed.condition as bed_condition, bed.type as bed_type FROM station as s JOIN location as loc ON s.id_location = loc.id JOIN coordinates as coord ON loc.id_coordinates = coord.id JOIN city as cit ON loc.id_city = cit.id JOIN state as stat ON cit.id_state = stat.id JOIN country as count ON stat.id_country = count.id JOIN tectonic as tec ON loc.id_tectonic = tec.id JOIN monument as mon ON s.id_monument = mon.id JOIN station_type as stt ON s.id_station_type = stt.id JOIN geological as geo ON s.id_geological = geo.id JOIN bedrock as bed ON geo.id_bedrock = bed.id WHERE s.id = ").append(rs.getInt("id")).append(";").toString());

                while (rs_multi.next())
                {

                    country = new Country(0, rs_multi.getString("country"), rs_multi.getString("iso_code"));
                    state = new State(0, country, rs_multi.getString("state"));
                    city = new City(0, state, rs_multi.getString("city"));
                    coordinates = new Coordinates(0, rs_multi.getDouble("x"), rs_multi.getDouble("y"), rs_multi.getDouble("z"), rs_multi.getDouble("lat"), rs_multi.getDouble("lon"), rs_multi.getDouble("altitude"));
                    tectonic = new Tectonic(0, rs_multi.getString("plate_name"));
                    bedrock = new Bedrock(0, rs_multi.getString("bed_condition"), rs_multi.getString("bed_type"));


                    s.setStation_type(0, rs_multi.getString("stt_name"), rs_multi.getString("stt_type"));
                    s.setLocation(new Location(0, city, coordinates, tectonic, rs_multi.getString("loc_desc")));
                    s.setMonument(new Monument(0, rs_multi.getString("mon_desc"), rs_multi.getString("inscription"), rs_multi.getString("height"), rs_multi.getString("foundation"), rs_multi.getString("foundation_depth")));
                    s.setGeological(new Geological(0, bedrock, rs_multi.getString("characteristic"), rs_multi.getString("fracture_spacing"), rs_multi.getString("fault_zone"), rs_multi.getString("distance_to_fault")));
                }

                rs_multi.close();
                s_multi.close();

                // *************************************************************
                // Handle Colocation Offset
                // *************************************************************
                // Initialized statement for select
                s_station_col = c.createStatement();
                rs_station_col = s_station_col.executeQuery(new StringBuilder().append("SELECT * FROM station_colocation WHERE id_station = ").append(s.getId()).append(";").toString());
                while (rs_station_col.next())
                {
                    Station_colocation station_colocation;
                    Colocation_offset colocation_offset;
                    Station station_colocated;

                    // Initialized statement for select
                    s_col_offset = c.createStatement();
                    colocation_offset = new Colocation_offset();
                    rs_col_offset = s_col_offset.executeQuery(new StringBuilder().append("SELECT * FROM colocation_offset WHERE id_station_colocation = ").append(rs_station_col.getInt("id")).append(";").toString());
                    if (rs_col_offset.next())
                    {
                        // Save Colocation Offset data
                        colocation_offset = new Colocation_offset(rs_col_offset.getInt("id"), rs_col_offset.getFloat("offset_x"), rs_col_offset.getFloat("offset_y"), rs_col_offset.getFloat("offset_z"), rs_col_offset.getString("date_measured"));
                    }

                    rs_col_offset.close();
                    s_col_offset.close();

                    // Initialized statement for select
                    s_s_colocated = c.createStatement();
                    rs_s_colocated = s_s_colocated.executeQuery(new StringBuilder().append("SELECT * FROM station WHERE id=").append(rs_station_col.getInt("id_station_colocated")).append(";").toString());
                    station_colocated = new Station();
                    while (rs_s_colocated.next())
                    {
                        // Save Station Colocated data
                        station_colocated = new Station(rs_s_colocated.getInt("id"), rs_s_colocated.getString("name"), rs_s_colocated.getString("marker"), null, rs_s_colocated.getString("description"),
                                rs_s_colocated.getString("date_from"), rs_s_colocated.getString("date_to"), rs_s_colocated.getString("comment"),
                                rs_s_colocated.getString("iers_domes"), rs_s_colocated.getString("cpd_num"), rs_s_colocated.getInt("monument_num"),
                                rs_s_colocated.getInt("receiver_num"), rs_s_colocated.getString("country_code"));
                    }

                    rs_s_colocated.close();
                    s_s_colocated.close();

                    // Save Station Collocation data
                    station_colocation = new Station_colocation(rs_station_col.getInt("id"), s, station_colocated);

                    // Add Station Collocation to Colocation Offset
                    colocation_offset.setStation_colocation(station_colocation);

                    // Add Collocation Offset to list
                    s.addStationColocationOffset(colocation_offset);
                }

                rs_station_col.close();
                s_station_col.close();

                // *************************************************************
                // Handle Local Conditions and Effects
                // *************************************************************

                // Initialized statement for select
                s_conditions = c.createStatement();
                rs_conditions = s_conditions.executeQuery(new StringBuilder().append("SELECT condition.id AS condition_id, condition.id_station AS condition_id_station, condition.date_from AS condition_date_from, condition.date_to AS condition_date_to, condition.degradation AS condition_degradation, condition.comments AS condition_comments, effects.id AS effects_id, effects.type AS effects_type FROM condition INNER JOIN effects on effects.id = condition.id_effect WHERE condition.id_station = ").append(s.getId()).append(";").toString());

                while (rs_conditions.next())
                {
                    Effects effect = new Effects(rs_conditions.getInt("effects_id"), rs_conditions.getString("effects_type"));
                    // Add to conditions list
                    s.addCondition(new Condition(rs_conditions.getInt("condition_id"), s.getId(), effect, rs_conditions.getString("condition_date_from"), rs_conditions.getString("condition_date_to"), rs_conditions.getString("condition_degradation"), rs_conditions.getString("condition_comments")));
                }

                rs_conditions.close();
                s_conditions.close();

                // *************************************************************
                // Handle Local Ties
                // *************************************************************

                // Initialized statement for select
                s_local_ties = c.createStatement();
                rs_local_ties = s_local_ties.executeQuery(new StringBuilder().append("SELECT * FROM local_ties WHERE id_station=").append(s.getId()).append(";").toString());
                while (rs_local_ties.next())
                {

                    // Add to local_ties list
                    s.addLocalTie(new Local_ties(
                            rs_local_ties.getInt("id"),
                            s.getId(),
                            rs_local_ties.getString("name"),
                            rs_local_ties.getString("usage"),
                            rs_local_ties.getString("cpd_num"),
                            rs_local_ties.getString("iers_domes"),
                            rs_local_ties.getFloat("dx"),
                            rs_local_ties.getFloat("dy"),
                            rs_local_ties.getFloat("dz"),
                            rs_local_ties.getFloat("accuracy"),
                            rs_local_ties.getString("survey_method"),
                            rs_local_ties.getString("date_at"),
                            rs_local_ties.getString("comment")));
                }

                rs_local_ties.close();
                s_local_ties.close();

                // *************************************************************
                // Handle Collocation Instrument
                // *************************************************************

                // Initialized statement for select
                s_coll_inst = c.createStatement();
                rs_coll_inst = s_coll_inst.executeQuery(new StringBuilder().append("SELECT * FROM instrument_collocation WHERE id_station=").append(s.getId()).append(";").toString());
                while (rs_coll_inst.next())
                {

                    // Add to Instrument_collocation list
                    s.addCollocationInstrument(new Instrument_collocation(
                            rs_coll_inst.getInt("id"),
                            rs_coll_inst.getInt("id_station"),
                            rs_coll_inst.getString("type"),
                            rs_coll_inst.getString("status"),
                            rs_coll_inst.getString("date_from"),
                            rs_coll_inst.getString("date_to"),
                            rs_coll_inst.getString("comment")));
                }

                rs_coll_inst.close();
                s_coll_inst.close();

                // *************************************************************
                // Handle Access Control
                // *************************************************************

                /**
                 * Last modified: 05/12/2019 by José Manteigueiro
                 */

                // Initialized statement for select
                if(oldDBVersion)
                // DB Version < 1.3.0
                {
                    s_user_group_st = c.createStatement();
                    rs_user_group_st = s_user_group_st.executeQuery("select " +
                            "ugroupstation.id as ugroupstation_id, " +
                            "ugroupstation.id_stations as ugroupstation_idstations, " +
                            "ugroup.id as ugroup_id, " +
                            "ugroup.name as ugroup_name " +
                            "from user_group_station as ugroupstation " +
                            "inner join user_group as ugroup " +
                            "on ugroup.id = ugroupstation.id_user_groups " +
                            "where ugroupstation.id_stations = "
                            + s.getId() +";");
                    while (rs_user_group_st.next()) {
                        // *********************************************************
                        // Handle User Group
                        // *********************************************************

                        User_group user_group = new User_group(rs_user_group_st.getInt("ugroup_id"), rs_user_group_st.getString("ugroup_name"));
                        // Add to user_group_station list
                        s.addUserGroupStation(new User_group_station(rs_user_group_st.getInt("ugroupstation_id"), user_group, rs_user_group_st.getInt("ugroupstation_idstations")));
                    }

                    rs_user_group_st.close();
                    s_user_group_st.close();
                }else{
                    s_user_group_st = c.createStatement();
                    rs_user_group_st = s_user_group_st.executeQuery("select " +
                            "ugroupstation.id as ugroupstation_id, " +
                            "ugroupstation.id_station as ugroupstation_idstation, " +
                            "ugroup.id as ugroup_id, " +
                            "ugroup.name as ugroup_name " +
                            "from user_group_station as ugroupstation " +
                            "inner join user_group as ugroup " +
                            "on ugroup.id = ugroupstation.id_user_group " +
                            "where ugroupstation.id_station = "
                            + s.getId() +";");
                    while (rs_user_group_st.next()) {
                        // *********************************************************
                        // Handle User Group
                        // *********************************************************

                        User_group user_group = new User_group(rs_user_group_st.getInt("ugroup_id"), rs_user_group_st.getString("ugroup_name"));
                        // Add to user_group_station list
                        s.addUserGroupStation(new User_group_station(rs_user_group_st.getInt("ugroupstation_id"), user_group, rs_user_group_st.getInt("ugroupstation_idstation")));
                    }

                    rs_user_group_st.close();
                    s_user_group_st.close();
                }

                // *************************************************************
                // Handle Site Logs
                // Modified on 22/03/2021 by José Manteigueiro
                // *************************************************************

                PreparedStatement _psLog = c.prepareStatement("SELECT l.*, lt.id AS lt_id, lt.name AS lt_name FROM log l INNER JOIN log_type lt ON l.id_log_type = lt.id WHERE l.id_station = ?;");
                PreparedStatement _psContact = c.prepareStatement("SELECT c.*, a.abbreviation, a.address, a.infos, a.www, a.name AS agency_name, a.id AS agency_id FROM contact c INNER JOIN agency a ON a.id = c.id_agency WHERE c.id = ?;");
                ResultSet _rsLog = null, _rsContact = null;
                Agency _agency;
                Contact _contact;
                Log _log;

                try {
                    _psLog.setInt(1, s.getId());
                    _rsLog = _psLog.executeQuery();

                    while (_rsLog.next()) {
                        // Log Object
                        _log = new Log();
                        _log.setId(_rsLog.getInt("id"));
                        try {
                            // May be NULL
                            _log.setDate(_rsLog.getString("date"));
                        } catch (SQLException _sqlException) {
                            continue;
                        }
                        _log.setTitle(_rsLog.getString("title"));
                        _log.setId_station(_rsLog.getInt("id_station"));
                        _log.setModified(_rsLog.getString("modified"));
                        _log.setPrevious(_rsLog.getString("previous"));
                        _log.setLog_type(_rsLog.getInt("lt_id"), _rsLog.getString("lt_name"));

                        try {
                            // id_contact may be null
                            _psContact.setInt(1, _rsLog.getInt("id_contact"));
                            _rsContact = _psContact.executeQuery();
                            if (_rsContact.next()) {
                                _agency = new Agency();
                                _agency.setId(_rsContact.getInt("agency_id"));
                                _agency.setName(_rsContact.getString("agency_name"));
                                _agency.setAbbreviation(_rsContact.getString("abbreviation"));
                                _agency.setAddress(_rsContact.getString("address"));
                                _agency.setInfos(_rsContact.getString("infos"));
                                _agency.setWww(_rsContact.getString("www"));

                                _contact = new Contact();
                                _contact.setId(_rsContact.getInt("id"));
                                _contact.setName(_rsContact.getString("name"));
                                _contact.setComment(_rsContact.getString("comment"));
                                _contact.setEmail(_rsContact.getString("email"));
                                _contact.setGsm(_rsContact.getString("gsm"));
                                _contact.setPhone(_rsContact.getString("phone"));
                                _contact.setRole(_rsContact.getString("role"));
                                _contact.setTitle(_rsContact.getString("title"));

                                _contact.setAgency(_agency);

                                _log.setContact(_contact); //paul
                            }
                        }
                        catch (SQLException _sqlException) {
                            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                            _log.setContact(new Contact());
                        }

                        // Add to Log list
                        s.addLog(_log);
                    }
                }
                catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                }
                finally {
                    if (_rsContact != null) {
                        try {
                            _rsContact.close();
                        } catch (SQLException _sqlException) {
                            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                        }
                    }
                    if (_rsLog != null) {
                        try {
                            _rsLog.close();
                        } catch (SQLException _sqlException) {
                            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                        }
                    }
                    if (_psContact != null) {
                        try {
                            _psContact.close();
                        } catch (SQLException _sqlException) {
                            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                        }
                    }
                    if (_psLog != null) {
                        try {
                            _psLog.close();
                        } catch (SQLException _sqlException) {
                            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                        }
                    }
                }

                // *************************************************************
                // Handle Organisational
                // *************************************************************

                // Initialized statement for select
                s_station_cont = c.createStatement();
                rs_station_cont = s_station_cont.executeQuery(new StringBuilder().append("SELECT stc.role as stc_role, cont.name as cont_name, cont.title, cont.email, cont.phone, cont.gsm, cont.comment, cont.role as cont_role, ag.name as ag_name, ag.abbreviation, ag.address, ag.www, ag.infos FROM station_contact as stc JOIN contact as cont on cont.id = stc.id_contact JOIN agency as ag on ag.id = cont.id_agency WHERE stc.id_station = ").append(s.getId()).append(";").toString());

                while (rs_station_cont.next())
                {
                    Agency agency = new Agency(
                            0,
                            rs_station_cont.getString("ag_name"),
                            rs_station_cont.getString("abbreviation"),
                            rs_station_cont.getString("address"),
                            rs_station_cont.getString("www"),
                            rs_station_cont.getString("infos"));

                    Contact contact = new Contact(
                            0,
                            rs_station_cont.getString("cont_name"),
                            rs_station_cont.getString("title"),
                            rs_station_cont.getString("email"),
                            rs_station_cont.getString("phone"),
                            rs_station_cont.getString("gsm"),
                            rs_station_cont.getString("comment"),
                            agency,
                            rs_station_cont.getString("cont_role"));

                    // Add to station contacts list
                    s.addStationConacts(new Station_contact(0, s.getId(), contact, rs_station_cont.getString("stc_role")));
                }

                rs_station_cont.close();
                s_station_cont.close();
                
                // Check if id_contact is present in network table
                s_network = c.createStatement();
                rs_network = s_network.executeQuery(new StringBuilder().append("SELECT count(net.id) FROM network as net JOIN station_network as stn on net.id=stn.id_network WHERE net.id_contact is not null AND stn.id_station =").append(s.getId()).append(";").toString());
                rs_network.next();
                int nb_id_contact_in_network = rs_network.getInt(1);        
                // Initialized statement for select
                s_station_net = c.createStatement();
                if(nb_id_contact_in_network>0)
                {

                    rs_station_net = s_station_net.executeQuery(new StringBuilder().append("SELECT net.name as net_name, cont.name as cont_name, cont.title, cont.email, cont.phone, cont.gsm, cont.comment, cont.role as cont_role, ag.name as ag_name, ag.abbreviation, ag.address, ag.www, ag.infos FROM station_network as stn JOIN network as net on net.id=stn.id_network JOIN contact as cont on cont.id=net.id_contact JOIN agency as ag on ag.id=cont.id_agency WHERE stn.id_station = ").append(s.getId()).append(";").toString());

                    while (rs_station_net.next())
                    {

                        Agency agency = new Agency(
                                0,
                                rs_station_net.getString("ag_name"),
                                rs_station_net.getString("abbreviation"),
                                rs_station_net.getString("address"),
                                rs_station_net.getString("www"),
                                rs_station_net.getString("infos"));

                        Contact contact = new Contact(
                                0,
                                rs_station_net.getString("cont_name"),
                                rs_station_net.getString("title"),
                                rs_station_net.getString("email"),
                                rs_station_net.getString("phone"),
                                rs_station_net.getString("gsm"),
                                rs_station_net.getString("comment"),
                                agency,
                                rs_station_net.getString("cont_role"));


                        // Save Network data
                        Network network = new Network(0, rs_station_net.getString("net_name"), contact);

                        // Add to station network list
                        s.addStationNetwork(new Station_network(0, s.getId(), network));
                    }
                }else{
                    rs_station_net = s_station_net.executeQuery(new StringBuilder().append("SELECT net.name as net_name FROM station_network as stn JOIN network as net on net.id=stn.id_network WHERE stn.id_station= ").append(s.getId()).append(";").toString());
                   
                    while (rs_station_net.next() == true)
                    {
                        Station_network station_network = new Station_network();
                        Network network = new Network();
                        
                       
                        // Save Network data
                        network.setName(rs_station_net.getString("net_name"));
                        
                        // Save Station Network

                        station_network.setNetwork(network);

                        // Add to station network list
                        s.addStationNetwork(station_network);
                    }
                }    

                rs_station_net.close();
                s_station_net.close();

                // *************************************************************
                // Handle Items
                // *************************************************************

                // Initialized statement for select
                s_station_item = c.createStatement();
                rs_station_item = s_station_item.executeQuery(new StringBuilder().append("SELECT si.id AS si_id, si.date_from AS si_date_from, si.date_to AS si_date_to, i.id AS i_id, i.comment AS i_comment,it.id AS it_id, it.name AS it_name FROM station_item AS si INNER JOIN item AS i ON i.id = si.id_item INNER JOIN item_type AS it ON it.id = i.id_item_type WHERE si.id_station = ").append(s.getId()).append(";").toString());

                //While station item
                while (rs_station_item.next())
                {

                    Item_type item_type = new Item_type(rs_station_item.getInt("it_id"), rs_station_item.getString("it_name"));
                    Item item = new Item(rs_station_item.getInt("i_id"), item_type, rs_station_item.getString("i_comment"));

                    //Initialized statement for selecting item_attribute
                    s_item_attribute = c.createStatement();
                    rs_item_attribute = s_item_attribute.executeQuery(new StringBuilder().append("SELECT ia.id AS ia_id, ia.id_item AS ia_id_item,ia.date_from AS ia_date_from, ia.date_to AS ia_date_to, ia.value_varchar AS ia_value_varchar, ia.value_date AS ia_value_date, ia.value_numeric AS ia_value_numeric, a.id AS a_id, a.name AS a_name FROM item_attribute AS ia JOIN attribute AS a ON a.id = ia.id_attribute WHERE ia.id_item = ").append(rs_station_item.getInt("i_id")).append(";").toString());

                    //while attributes
                    while (rs_item_attribute.next()) {

                        Attribute attribute = new Attribute(rs_item_attribute.getInt("a_id"), rs_item_attribute.getString("a_name"));
                        Item_attribute item_attribute = new Item_attribute(
                                rs_item_attribute.getInt("ia_id"),
                                rs_item_attribute.getInt("ia_id_item"),
                                attribute,
                                rs_item_attribute.getString("ia_date_from"),
                                rs_item_attribute.getString("ia_date_to"),
                                rs_item_attribute.getString("ia_value_varchar"),
                                rs_item_attribute.getString("ia_value_date"),
                                rs_item_attribute.getFloat("ia_value_numeric"));
                        item.AddItemAttribute(item_attribute);

                    }

                    // Add station item to the list
                    s.addStationItem(new Station_item(
                            rs_station_item.getInt("si_id"),
                            s.getId(),
                            item,
                            rs_station_item.getString("si_date_from"),
                            rs_station_item.getString("si_date_to")));

                }

                rs_station_item.close();
                s_station_item.close();


                // *************************************************************
                // Handle T4T5
                // *************************************************************
                if (GlassConstants.PRODUCTSPORTAL) {
                    DataBaseT4Connection dbt4 = new DataBaseT4Connection();
                    //the below is wrong - mixing site with marker
                    //ArrayList<VelocityField> vf = dbt4.VelocitiesField("site:" + s.getMarker());


                    /*
                    ArrayList<ArrayList<VelocityByPlateAndAc>> vf = dbt4.VelocitiesField(s.getMarker());
                    if (vf.size() == 0) {
                        s.setAngle(0.0);
                        s.setVectorLength(0.0);
                    } else {
                        //s.setAngle(vf.get(0).getAngle());
                        //s.setVectorLength(vf.get(0).getLenghtvector());
                        s.setV_marker(vf);
                    }

                    */

                    //Processing agency from estimated coordinates and reference position velocities
                    //s.setPA_timeseries(dbt4.processingAgencyFromT4T5("timeseries", s.getId()));
                    s = dbt4.setTimeseries(s);
                    s.setPA_velocities(dbt4.processingAgencyFromT4T5("velocities", s.getId()));
                    s.setPA_coordinates(dbt4.processingAgencyFromT4T5("coordinates", s.getId()));
                }
                // *************************************************************
                // Handle Documents
                // *************************************************************

                // Initialized statement for select
                s_document = c.createStatement();
                rs_document = s_document.executeQuery(new StringBuilder().append("SELECT d.id AS d_id, d.date AS d_date, d.title AS d_title, d.description AS d_description, d.link AS d_link, d.id_station AS d_id_station, d.id_item AS d_id_item, d.id_document_type AS d_id_document_type, dt.id as dt_id, dt.name AS dt_name FROM document AS d INNER JOIN document_type AS dt ON dt.id = d.id_document_type WHERE id_station = ").append(s.getId()).append(";").toString());

                while (rs_document.next())
                {

                    Document_type document_type = new Document_type(rs_document.getInt("dt_id"), rs_document.getString("dt_name"));

                    // *********************************************************
                    // Handle Item
                    // *********************************************************
                    // Initialized statement for select
                   /*s_item = c.createStatement();
                    rs_item = s_item.executeQuery( "SELECT " +
                            "i.id AS i_id, i.comment AS i_comment, " +
                            "it.id AS it_id, it.name AS it_name " +
                            "FROM item AS i " +
                            "INNER JOIN item_type AS it ON it.id = i.id_item_type " +
                            "WHERE i.id = " + rs_document.getInt("d_id_item") + ";" );
                    Item item = new Item();
                    if (rs_item.next())
                    {

                        Item_type item_type = new Item_type(rs_item.getInt("it_id"), rs_item.getString("it_name"));
                        item = new Item(rs_item.getInt("i_id"), item_type, rs_item.getString("i_comment"));

                        //Initialized statement for selecting item_attribute
                        s_item_attribute = c.createStatement();
                        rs_item_attribute = s_item_attribute.executeQuery("SELECT " +
                                "ia.id AS ia_id, ia.id_item AS ia_id_item, ia.date_from AS ia_date_from, ia.date_to AS ia_date_to, ia.value_varchar AS ia_value_varchar, ia.value_date AS ia_value_date, ia.value_numeric AS ia_value_numeric, " +
                                "a.id AS a_id, a.name AS a_name " +
                                "FROM item_attribute AS ia " +
                                "JOIN attribute AS a ON a.id = ia.id_attribute " +
                                "WHERE ia.id_item = " + item.getId() + ";" );

                        //while attributes
                        while (rs_item_attribute.next()) {

                            Attribute attribute = new Attribute(rs_item_attribute.getInt("a_id"), rs_item_attribute.getString("a_name"));
                            Item_attribute item_attribute = new Item_attribute(
                                    rs_item_attribute.getInt("ia_id"),
                                    rs_item_attribute.getInt("ia_id_item"),
                                    attribute,
                                    rs_item_attribute.getString("ia_date_from"),
                                    rs_item_attribute.getString("ia_date_to"),
                                    rs_item_attribute.getString("ia_value_varchar"),
                                    rs_item_attribute.getString("ia_value_date"),
                                    rs_item_attribute.getFloat("ia_value_numeric"));
                            item.AddItemAttribute(item_attribute);

                        }


                    }

                    rs_item.close();
                    s_item.close();*/
                   Item item = new Item();

                    // Add document to station
                    s.addDocument(new Document(
                            rs_document.getInt("d_id"),
                            rs_document.getString("d_date"),
                            rs_document.getString("d_title"),
                            rs_document.getString("d_description"),
                            rs_document.getString("d_link"),
                            s.getId(),
                            item,
                            document_type));
                }

                rs_document.close();
                s_document.close();

                // Add object to ArrayList
                results.add(s);
            }

            // Close connections
            rs.close();
            s_local.close();
            ///////////////////c.close(); Paul . why is this being closed ? It was supposed to persist

        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }

        return results;
    }






    /**
     * GET STATIONS
     * This method returns a ArrayList with data from all stations on the database.
     */
    ArrayList<Station> getStations() throws ParseException {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();
            // Query to be executed
            String query = "SELECT * FROM station ORDER BY id";
            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATION MARKER
     * This method returns a ArrayList with data from all stations with the
     * defined marker.
     */
    ArrayList getStationMarker(String marker) throws ParseException {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList results = new ArrayList();

        try {
            c = NewConnection();
            // Query to be executed
            String query = "SELECT * FROM station WHERE marker ILIKE '" + marker + "%' ORDER BY id;";
            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }
    
    /**
     * GET STATION ID
     * This method returns a ArrayList with data from all stations with the
     * defined id.
     */
    ArrayList getStationId(int id_station) {
        return getStationId(id_station, null);
    }

    ArrayList getStationId(int id_station, Connection c)
    {
        boolean persistConnection=false;
        // Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList();

        try {
            if (null==c)
                c= NewConnection();
            else
                persistConnection=true;
            //else use existing connection

            // Query to be executed
            String query = "SELECT * FROM station WHERE id = " + id_station + ";";

            results = executeStationQuery(query, c);   //Execute query

            // Get markerlongname for every station
            // DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();

            /*  What use is this code below ?
                We already have the marker long name from the executeStationQuery
                so why get it again ??
                Also the result set must be 1 station (or zero if the station id does not exist)
                for(int i=0; i<results.size();i++){
                Station aux = results.get(i);
                try{
                    String mln = dbcv2.getMarkerLongName(results.get(i).getMarker());
                    aux.setMarkerLongName(mln);
                }catch(Exception e){
                    aux.setMarkerLongName("---");
                }
                results.set(i, aux);
            }*/


        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null && !persistConnection) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATION SITE
     * This method returns a ArrayList with data from all stations with the
     * defined site name.
     */
    ArrayList getStationSite(String site)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList results = new ArrayList();

        try {
            c = NewConnection();
            // Query to be executed
            String query = "SELECT * FROM station WHERE name ILIKE '" + site + "%' ORDER BY id;";
            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATION COORDINATES
     * This method returns a ArrayList with data from all stations within the
     * defined coordinate space.
     */
    ArrayList<Station> getStationCoordinates(float minLat, float maxLat, float minLon, float maxLon) throws ParseException {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();
            // Query the coordinates
            String query = "SELECT " +
                    "station.* " +
                    "FROM station " +
                    "INNER JOIN location ON location.id = station.id_location " +
                    "INNER JOIN coordinates ON coordinates.id = location.id_coordinates " +
                    "WHERE coordinates.lat >= " + minLat + " " +
                    "AND coordinates.lat <= " + maxLat + " " +
                    "AND coordinates.lon >= " + minLon + " " +
                    "AND coordinates.lon <= " + maxLon + " " +
                    "ORDER BY id;";

            // Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * EXECUTE COORDINATES QUERY
     * This method executes a given query on the coordinates inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    private ArrayList<Coordinates> executeCoordinatesQuery(String query, Connection c) throws SQLException
    {
        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************

        ArrayList<Coordinates> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add state to result ArrayList
            results.add(new Coordinates(
                    rs.getInt("id"),
                    rs.getDouble("x"),
                    rs.getDouble("y"),
                    rs.getDouble("z"),
                    rs.getDouble("lat"),
                    rs.getDouble("lon"),
                    rs.getDouble("altitude")));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * EXECUTE LOCATION QUERY
     * This method executes a given query on the locations inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    @SuppressWarnings("unused")
    private ArrayList<Location> executeLocationQuery(String query, Connection c) throws SQLException
    {
        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************


        ArrayList<Location> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add state to result ArrayList
            results.add(new Location(
                    rs.getInt("id"),
                    null,
                    null,
                    null,
                    rs.getString("description")));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * GET DISTANCE COORDINATES
     * Return the distance in Km between two coordinates (Haversine formula)
     */
    private double getDistanceCoordinates(double lat1, double lon1, double lat2, double lon2)
    {
        double R = 6372.8; // In kilometers
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }

    /**
     * GET STATION COORDINATES
     * This method returns a ArrayList with data from all stations within the
     * defined circle of coordinate space.
     */
    ArrayList<Station> getStationCoordinates(float lat, float lon, float radius) throws ParseException {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();
        ArrayList<Coordinates> coordinates_all;
        ArrayList<Coordinates> coordinates_result = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            StringBuilder query = new StringBuilder("SELECT * FROM coordinates ORDER BY id");

            // Execute the query to get list of coordinates
            coordinates_all = executeCoordinatesQuery(query.toString(), c);

            // Check if coordinates are on the circle
            for (Coordinates coordinate : coordinates_all) {
                if (getDistanceCoordinates(lat, lon, coordinate.getLat(), coordinate.getLon()) < radius) {
                    coordinates_result.add(coordinate);
                }
            }

            // Query to be executed
            query = new StringBuilder("SELECT " +
                    "station.* " +
                    "FROM station " +
                    "INNER JOIN location ON location.id = station.id_location " +
                    "WHERE ");

            for (int i=0; i<coordinates_result.size(); i++)
            {
                if (i==0)
                    query.append(" location.id_coordinates = ").append(coordinates_result.get(i).getId());
                else
                    query.append(" OR location.id_coordinates = ").append(coordinates_result.get(i).getId());
            }
            query.append(";");

            //Execute query
            results = executeStationQuery(query.toString(), c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }


    /**
     * GET STATION COORDINATES
     * This method returns a ArrayList with data from all stations within the
     * defined coordinate polygon.
     */
    ArrayList<Station> getStationCoordinates(ArrayList<Coordinates> polygon) throws ParseException {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();
        // ArrayList with all Coordinates
        ArrayList<Coordinates> coordinates_all;
        // ArrayList with ids from polygon coordinates
        ArrayList<Integer> in_polygon = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            StringBuilder query = new StringBuilder("SELECT * FROM coordinates ORDER BY id;");
            
            // Execute the query to get list of coordinates
            coordinates_all = executeCoordinatesQuery(query.toString(), c);
            
            // Check for coordinates inside the polygon
            for (Coordinates coordinate : coordinates_all) {
                if (Point.insidePolygon(coordinate.getLat(), coordinate.getLon(), polygon))
                    in_polygon.add(coordinate.getId());
            }

            // Query to be executed
            query = new StringBuilder("SELECT " +
                    "station.* " +
                    "FROM station " +
                    "INNER JOIN location ON location.id = station.id_location " +
                    "WHERE ");

            for (int i=0; i<in_polygon.size(); i++)
            {
                if (i==0)
                    query.append(" location.id_coordinates = ").append(in_polygon.get(i));
                else
                    query.append(" OR location.id_coordinates = ").append(in_polygon.get(i));
            }
            query.append(";");
           
            //Execute query
            results = executeStationQuery(query.toString(), c);
            
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATION DATE
     * This method returns a ArrayList with data from all stations within the
     * defined date range.
     */
    ArrayList getStationDate(String date_from, String date_to)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();
            // Query to be executed

            DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Date date = new Date();

            String query;
            if(date_to.equals("0") || date_to.equals("null") || date_to.equals(dateFormat.format(date))) //if date_to is 0, null or today's date, the active stations appear
                query = "SELECT * FROM station WHERE date_from >= '"+ date_from + "' AND date_to is null ORDER BY id;";
            else
                query = "SELECT * FROM station WHERE date_from >= '"+ date_from + "' AND date_to <= '" + date_to + "' ORDER BY id;";

            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATIONS NETWORK
     * This method returns a ArrayList with data from all stations within the
     * defined network.
     */
    ArrayList getStationsNetwork(String network)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();
            // Query to be executed
            String query = "SELECT " +
                    "station.* " +
                    "FROM station_network " +
                    "INNER JOIN network ON network.id = station_network.id_network " +
                    "INNER JOIN station ON station.id = station_network.id_station " +
                    "WHERE network.name ILIKE '%" + network + "%' "+
                    "ORDER BY id";

            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }






    ArrayList <String> getStationsNameByNetwork(String network)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<String> results = new ArrayList<>();

        try {
            c = NewConnection();
            // Query to be executed
            String query = "SELECT " +
                    "station.marker " +
                    "FROM station_network " +
                    "INNER JOIN network ON network.id = station_network.id_network " +
                    "INNER JOIN station ON station.id = station_network.id_station " +
                    "WHERE network.name ILIKE '%" + network + "%' ";
            PreparedStatement preparedStatement = c.prepareStatement(query);

            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                results.add(resultSet.getString("marker"));
            }

            preparedStatement.close();
            resultSet.close();

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }








    /**
     * GET STATIONS AGENCY
     * This method returns a ArrayList with data from all stations within the
     * defined agency.
     */
    ArrayList<Station> getStationsAgency(String agency)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();
            String query = "SELECT " +
                    "station.* " +
                    "FROM station_contact " +
                    "INNER JOIN contact ON contact.id = station_contact.id_contact " +
                    "INNER JOIN agency ON agency.id = contact.id_agency " +
                    "INNER JOIN station ON station.id = station_contact.id_station " +
                    "WHERE agency.name LIKE '%" + agency + "%';";

            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATIONS TYPE
     * This method returns a ArrayList with data from all stations within the
     * defined network.
     */
    ArrayList<Station> getStationsType(String type)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();
            // Query to be executed
            String query = "SELECT * FROM STATION WHERE id_station_type = (SELECT id from station_type where name = '" + type + "');";

            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATIONS TYPE
     * This method returns a ArrayList with data from all stations within the
     * defined network.
     */
    ArrayList<Station> getStationsTypeShort(String type)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();
            // Query to be executed
            String query = "select station.id, station.name as name, station.marker, array_to_string(array_agg(distinct(agency.name)), ' & ') as agency, array_to_string(array_agg(distinct(network.name)), ' & ') as network, station.date_from, station.date_to, coordinates.lat, coordinates.lon, coordinates.altitude, city.name as city, state.name as state, country.name as country" + " from station, coordinates, city, state, country, location, station_contact, contact, agency, station_network, network" + " where id_station_type = (select id from station_type where name='"+type+"') AND" + " station.id_location = location.id" + " and location.id_coordinates = coordinates.id" + " and location.id_city = city.id" + " and state.id = city.id_state" + " and state.id_country = country.id" + " and station_contact.id_station = station.id" + " and station_contact.id_contact = contact.id" + " and contact.id_agency = agency.id" + " and station_network.id_station = station.id" + " and station_network.id_network = network.id" + " group by station.id, coordinates.lat, coordinates.lon, coordinates.altitude, city.name, state.name, country.name";


            //Execute query
            results = executeStationShortQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }


    /**
     * GET STATIONS ANTENNA TYPE
     * This method returns a ArrayList with data from all stations within the
     * defined antenna type.
     */
    ArrayList<Station> getStationsAntennaType(String antennaType)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();
        ArrayList<Integer> id_items = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            StringBuilder query = new StringBuilder("SELECT " +
                    "DISTINCT item.id AS item_id " +
                    "FROM filter_antenna " +
                    "INNER JOIN antenna_type ON antenna_type.id = filter_antenna.id_antenna_type " +
                    "INNER JOIN item_attribute ON item_attribute.id = filter_antenna.id " +
                    "INNER JOIN item ON item.id = item_attribute.id_item " +
                    "WHERE antenna_type.name ILIKE '%" + antennaType + "%' " +
                    "ORDER BY item.id;");

            id_items.addAll(executeIDItemQuery(query.toString(), c));

            query = new StringBuilder("SELECT " +
                    "station.* " +
                    "FROM station_item " +
                    "INNER JOIN station ON station.id = station_item.id_station " +
                    "WHERE ");

            for (int i=0; i<id_items.size(); i++)
            {
                if (i == id_items.size()-1)
                    query.append("station_item.id_item = ").append(id_items.get(i)).append(";");
                else
                    query.append("station_item.id_item = ").append(id_items.get(i)).append(" OR ");
            }

            //Execute query
            results = executeStationQuery(query.toString(), c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATIONS ANTENNA TYPE SHORT
     * This method returns a ArrayList with data from all stations within the
     * defined antenna type.
     */
    ArrayList<Station> getStationsAntennaTypeShort(String antennaType)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();
        ArrayList<Integer> id_items = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            StringBuilder query = new StringBuilder("SELECT " +
                    "DISTINCT item.id AS item_id " +
                    "FROM filter_antenna " +
                    "INNER JOIN antenna_type ON antenna_type.id = filter_antenna.id_antenna_type " +
                    "INNER JOIN item_attribute ON item_attribute.id = filter_antenna.id " +
                    "INNER JOIN item ON item.id = item_attribute.id_item " +
                    "WHERE antenna_type.name ILIKE '%" + antennaType + "%' " +
                    "ORDER BY item.id;");

            id_items.addAll(executeIDItemQuery(query.toString(), c));

            query = new StringBuilder("SELECT " +
                    "s.marker, s.name, s.date_from, s.date_to, " +
                    "coo.x, coo.y, coo.z, coo.lat, coo.lon, coo.altitude, " +
                    "co.name AS country, " +
                    "st.name AS state, " +
                    "ci.name AS city, " +
                    "array_to_string(array_agg(distinct(ag.name)), ' ') AS agency, " +
                    "array_to_string(array_agg(distinct(net.name)), ' ') AS network " +
                    "FROM station_item AS si " +
                    "INNER JOIN station AS s ON s.id = si.id_station " +
                    "JOIN location AS l ON s.id_location = l.id " +
                    "JOIN coordinates AS coo ON l.id_coordinates = coo.id " +
                    "JOIN city AS ci ON l.id_city = ci.id " +
                    "JOIN state AS st ON ci.id_state = st.id " +
                    "JOIN country AS co ON st.id_country = co.id " +
                    "JOIN station_contact AS stco ON stco.id_station = s.id " +
                    "JOIN contact AS cont ON stco.id_contact = cont.id " +
                    "JOIN agency AS ag ON cont.id_agency = ag.id " +
                    "JOIN station_network AS stnet ON stnet.id_station = s.id " +
                    "JOIN network AS net ON stnet.id_network = net.id " +
                    "WHERE ");

            for (int i=0; i<id_items.size(); i++)
            {
                if (i == id_items.size()-1)
                    query.append("si.id_item = ").append(id_items.get(i));
                else
                    query.append("si.id_item = ").append(id_items.get(i)).append(" OR ");
            }

            query.append(" GROUP BY " +
                    "   s.marker, s.name, s.date_from, s.date_to, " +
                    "   coo.lat, coo.lon, coo.altitude, " +
                    "   co.name, " +
                    "   st.name, " +
                    "   ci.name; ");

            //Execute query
            results = executeStationShortQuery(query.toString(), c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }
    /**
     * GET STATIONS RECEIVER TYPE
     * This method returns a ArrayList with data from all stations within the
     * defined receiver type.
     */
    ArrayList getStationsReceiverType(String receiverType)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            // Not sure if this query is correct since there are lots of changes in the DB atm
            String query = new StringBuilder().append("select * from station where id in (").append("select station_item.id_station from station_item where station_item.id_item in (").append("select item.id from item where item.id in (").append("select item_attribute.id_item from item_attribute where item_attribute.id in (").append("select filter_receiver.id_item_attribute from filter_receiver where filter_receiver.id_receiver_type in (").append("select receiver_type.id from receiver_type ").append("where receiver_type.name ilike '").append(receiverType).append("'").append(")))));").toString();

            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATIONS RADOME TYPE
     * This method returns a ArrayList with data from all stations within the
     * defined radome type.
     */
    ArrayList getStationsRadomeType(String radomeType)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            String query = "select * from station where station.id in (" + "select station_item.id_station from station_item where station_item.id_item in (" + "select item.id from item where item.id in (" + "select item_attribute.id_item from item_attribute where item_attribute.id in (" + "select filter_radome.id_attribute from filter_radome where filter_radome.id_radome_type in (" + "select radome_type.id from radome_type where radome_type.name ilike '" + radomeType + "'" + ")))));";


            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATIONS COUNTRY
     * This method returns a ArrayList with data from all stations of a country.
     */
    ArrayList getStationsCountry(String country)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            String query = "SELECT " +
                    "station.* " +
                    "FROM station " +
                    "INNER JOIN location ON location.id = station.id_location " +
                    "INNER JOIN city ON city.id = location.id_city " +
                    "INNER JOIN state ON state.id = city.id_state " +
                    "INNER JOIN country ON country.id = state.id_country " +
                    "WHERE country.name ILIKE '%" + country + "%' " +
                    "ORDER BY id;";

            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATIONS STATE
     * This method returns a ArrayList with data from all stations of a state.
     */
    ArrayList getStationsState(String state)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            String query = "SELECT " +
                    "station.* " +
                    "FROM station " +
                    "INNER JOIN location ON location.id = station.id_location " +
                    "INNER JOIN city ON city.id = location.id_city " +
                    "INNER JOIN state ON state.id = city.id_state " +
                    "WHERE state.name ILIKE '%" + state + "%' " +
                    "ORDER BY id;";

            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * GET STATIONS CITY
     * This method returns a ArrayList with data from all stations of a city.
     */
    ArrayList<Station> getStationsCity(String city)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            String query = "SELECT " +
                    "station.* " +
                    "FROM station " +
                    "INNER JOIN location ON location.id = station.id_location " +
                    "INNER JOIN city ON city.id = location.id_city " +
                    "WHERE city.name ILIKE '%" + city + "%' " +
                    "ORDER BY id;";

            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * EXECUTE ID QUERY
     * This method executes a given query and returns ID's.
     */
    ArrayList<Integer> executeIDQuery(String query, Connection c)
    {
        ArrayList<Integer> idList = new ArrayList<>();

        // Initialized statement for select
        try {
            s = c.createStatement();
            rs = s.executeQuery(query);

            // Check if there are any results
            while (rs.next())
            {
                idList.add(rs.getInt("id"));
            }

            rs.close();
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return idList;
    }

    /**
     * EXECUTE ID STATION QUERY
     * This method executes a given query and returns ID's.
     */
    ArrayList<Integer> executeIDStationQuery(String query, Connection c)
    {
        ArrayList<Integer> idList = new ArrayList<>();

        // Initialized statement for select
        try {
            s = c.createStatement();
            rs = s.executeQuery(query);

            // Check if there are any results
            while (rs.next())
            {
                idList.add(rs.getInt("id_station"));
            }

            rs.close();
            s.close();

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return idList;
    }

    /**
     * EXECUTE CONTACT QUERY
     * This method performs a query on the contacts.
     */
    ArrayList<Contact> executeContactQuery(String query, Connection c) throws SQLException
    {

        ArrayList<Contact> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {

            // Add contact to result ArrayList
            results.add(new Contact(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("title"),
                    rs.getString("email"),
                    rs.getString("phone"),
                    rs.getString("gsm"),
                    rs.getString("comment"),
                    null,
                    rs.getString("role")));

        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * EXECUTE NETWORK QUERY
     * This method performs a query on the networks.
     */
    ArrayList<Network> executeNetworkQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Network> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add network to result ArrayList
            results.add(new Network(rs.getInt("id"), rs.getString("name"), null));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * EXECUTE STATION NETWORK QUERY
     * This method performs a query on the station network.
     */
    ArrayList<Station_network> executeStationNetworkQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Station_network> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add network to result ArrayList
            results.add(new Station_network(rs.getInt("id_station"), 0, null));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * EXECUTE AGENCY QUERY
     * This method executes a given query on the agencies inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    ArrayList<Agency> executeAgencyQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Agency> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add agency to result ArrayList
            results.add(new Agency(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("abbreviation"),
                    rs.getString("address"),
                    rs.getString("www"),
                    rs.getString("infos")));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * GET STATION CONTACT LIST
     * This method returns a ArrayList with all the station_contacts.
     */
    ArrayList<Station_contact> executeStationContactQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Station_contact> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add stationcontact to result ArrayList
            results.add(new Station_contact(rs.getInt("id"), rs.getInt("id_station"), null, null));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }




    /**
     * GET AGENCY LIST
     * This method returns a ArrayList with all the agencies on the database.
     */
    ArrayList<Agency> getAgencyList()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Agency> results = new ArrayList<>();

        try{
            c = NewConnection();
            // Query to be executed
            String query = "SELECT * FROM agency ORDER BY id;";

            //Execute query
            results = executeAgencyQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * EXECUTE COUNTRY QUERY
     * This method executes a given query on the countries inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    private ArrayList<Country> executeCountryQuery(String query, Connection c) throws SQLException
    {

        ArrayList<Country> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add country to result ArrayList
            results.add(new Country(rs.getInt("id"), rs.getString("name"), rs.getString("iso_code")));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * GET COUNTRY LIST
     * This method returns a ArrayList with all the countries on the database.
     */
    ArrayList getCountryList()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Country> results = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM country ORDER BY id;";

            //Execute query
            results = executeCountryQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * EXECUTE STATE QUERY
     * This method executes a given query on the states inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    private ArrayList<State> executeStateQuery(String query, Connection c) throws SQLException
    {
        ArrayList<State> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Create State object
            Country country = new Country(rs.getInt("id_country"), rs.getString("country_name"), rs.getString("country_iso_code"));
            State state = new State(rs.getInt("id"), country, rs.getString("name"));

            // Add state to result ArrayList
            results.add(state);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * GET STATE LIST
     * This method returns a ArrayList with all the state on the database.
     */
    ArrayList getStateList()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<State> results = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT state.id AS id, state.id_country AS id_country, state.name AS name, country.name AS country_name, country.iso_code AS country_iso_code" +
                    " FROM state, country" +
                    " WHERE country.id = state.id_country" +
                    " ORDER BY state.id";

            //Execute query
            results = executeStateQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }


    /**
     * EXECUTE CITY QUERY
     * This method executes a given query on the states inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    private ArrayList<City> executeCityQuery(String query, Connection c) throws SQLException
    {
        ArrayList<City> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Create State object
            Country country = new Country(rs.getInt("country_id"), rs.getString("country_name"), rs.getString("country_iso_code"));
            State state = new State(rs.getInt("state_id"), country, rs.getString("state_name"));
            City city = new City(rs.getInt("city_id"), state, rs.getString("city_name"));

            // Add city to result ArrayList
            results.add(city);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * GET CITY LIST
     * This method returns a ArrayList with all the cities on the database.
     */
    ArrayList<City> getCityList()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<City> results = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT " +
                    "city.id AS city_id, city.name AS city_name, " +
                    "state.id AS state_id, state.name AS state_name, " +
                    "country.id AS country_id, country.name AS country_name, country.iso_code AS country_iso_code " +
                    "FROM city " +
                    "INNER JOIN state ON state.id = city.id_state " +
                    "INNER JOIN country ON country.id = state.id_country " +
                    "ORDER BY city.id;";

            //Execute query
            results = executeCityQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * EXECUTE RECEIVER TYPE QUERY
     * This method executes a given query on the receiver type inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    private ArrayList<Receiver_type> executeReceiverTypeQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Receiver_type> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {

            // Add receiver type to result ArrayList
            results.add(new Receiver_type(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("igs_defined"),
                    rs.getString("model")
            ));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }


    /**
     * GET RECEIVER TYPE LIST
     * This method returns a ArrayList with all the receiver type on the database.
     */
    ArrayList getReceiverTypeList()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Receiver_type> results = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM receiver_type ORDER BY id;";

            //Execute query
            results = executeReceiverTypeQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * EXECUTE ANTENNA TYPE QUERY
     * This method executes a given query on the receiver type inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    private ArrayList<Antenna_type> executeAntennaTypeQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Antenna_type> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add antenna type to result ArrayList
            results.add(new Antenna_type(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("igs_defined"),
                    rs.getString("model")));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }


    /**
     * GET ANTENNA TYPE LIST
     * This method returns a ArrayList with all the antenna type on the database.
     */
    ArrayList<Antenna_type> getAntennaTypeList()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Antenna_type> results = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM antenna_type ORDER BY id;";

            //Execute query
            results = executeAntennaTypeQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * EXECUTE RADOME TYPE QUERY
     * This method executes a given query on the receiver type inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    private ArrayList<Radome_type> executeRadomeTypeQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Radome_type> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add radome type to result ArrayList
            results.add(new Radome_type(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("igs_defined"),
                    rs.getString("description")
            ));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }


    /**
     * GET RADOME TYPE LIST
     * This method returns a ArrayList with all the radome type on the database.
     */
    ArrayList getRadomeTypeList()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Radome_type> results = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM radome_type ORDER BY id;";

            //Execute query
            results = executeRadomeTypeQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * EXECUTE STATION NAME QUERY
     * This method executes a given query on the receiver type inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    ArrayList<Station> executeStationNameQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Station> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add radome type to result ArrayList
            results.add(new Station(
                    rs.getInt("id"),
                    rs.getString("name"),
                    rs.getString("marker"),
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    null,
                    0,
                    0,
                    null
            ));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }


    /**
     * GET STATION NAME LIST
     * This method returns a ArrayList with all the station names on the database.
     */
    ArrayList getStationNameList()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM station ORDER BY id;";

            //Execute query
            results = executeStationNameQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * EXECUTE ID ITEM QUERY
     * This method executes a given query and returns ID's.
     */
    public ArrayList<Integer> executeIDItemQuery(String query, Connection c)
    {
        ArrayList<Integer> idList = new ArrayList<>();

        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************

        // Initialized statement for select
        try {
            s = c.createStatement();
            rs = s.executeQuery(query);

            // Check if there are any results
            while (rs.next())
            {
                idList.add(rs.getInt("id_item"));
            }

            rs.close();
            s.close();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return idList;
    }


    /**
     * GET STATIONS COMBINED
     * This method returns a ArrayList with all the stations with the given parameter.
     */
    @SuppressWarnings({"Duplicates", "ConstantConditions"})
    ArrayList getStationsCombined(String request) {

        Connection c;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();
        Set<Integer> results_marker = new HashSet<>();
        Set<Integer> results_site = new HashSet<>();
        Set<Integer> results_height = new HashSet<>();
        Set<Integer> results_latitude = new HashSet<>();
        Set<Integer> results_longitude = new HashSet<>();
        Set<Integer> results_altitude = new HashSet<>();
        Set<Integer> results_installed_date = new HashSet<>();
        Set<Integer> results_removed_date = new HashSet<>();
        Set<Integer> results_network = new HashSet<>();
        Set<Integer> results_agency = new HashSet<>();
        Set<Integer> results_antenna_type = new HashSet<>();
        Set<Integer> results_receiver_type = new HashSet<>();
        Set<Integer> results_radome_type = new HashSet<>();
        Set<Integer> results_country = new HashSet<>();
        Set<Integer> results_state = new HashSet<>();
        Set<Integer> results_city = new HashSet<>();
        Set<Integer> results_coordinates = new HashSet<>();
        Set<Integer> results_satellite = new HashSet<>();
        Set<Integer> results_station_type = new HashSet<>();
        Set<Integer> results_inverse_networks = new HashSet<>();
        Set<Integer> results_date_range = new HashSet();
        Set<Integer> results_publish_date = new HashSet();
        Set<Integer> results_file_type = new HashSet();
        Set<Integer> results_sampling_frequency = new HashSet();
        Set<Integer> results_sampling_window = new HashSet();
        Request request_holder = new Request(request);
        StringBuilder query;
        int counter = 0;
        String dates_range0 = "";
        String dates_range1 = "";
        String dates_publish0 = "";
        String dates_publish1 = "";

        try {
            c = NewConnection();

            // Parse request
            request_holder.parseRequest();

            // Check marker
            for(int index=0; index<request_holder.marker_list.size(); index++)
            {
                //query = "SELECT * FROM station WHERE marker LIKE '%" + request_holder.marker_list.get(index) + "%' ORDER BY id;";
                query = new StringBuilder("SELECT * FROM station WHERE marker ILIKE '" + request_holder.marker_list.get(index) + "%' ORDER BY id;");
                results_marker.addAll(executeIDQuery(query.toString(), c));
            }

            // Check site
            for(int index=0; index<request_holder.site_list.size(); index++)
            {
                //query = "SELECT * FROM station WHERE name LIKE '%" + request_holder.site_list.get(index) + "%' ORDER BY id;";
                query = new StringBuilder("SELECT * FROM station WHERE name ILIKE '" + request_holder.site_list.get(index) + "%' ORDER BY id;");
                results_site.addAll(executeIDQuery(query.toString(), c));
            }

            // Check height
            for(int index=0; index<request_holder.height_list.size(); index++)
            {
                // Get list of Station IDs
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("station.* ").append("FROM station ").append("INNER JOIN monument ON monument.id = station.id_monument ").append("WHERE monument.height='").append(request_holder.height_list.get(index)).append("'").toString());

                query.append(";");
                results_height.addAll(executeIDQuery(query.toString(), c));
            }

            // Check latitude
            if(request_holder.latitude_list.size()>0)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("station.* ").append("FROM station ").append("INNER JOIN location ON location.id = station.id_location ").append("INNER JOIN coordinates ON coordinates.id = location.id_coordinates ").toString());
                if (request_holder.minLatitude!=0.0f && request_holder.maxLatitude!=0.0f)
                {
                    query.append(" WHERE coordinates.lat > ").append(request_holder.minLatitude).append(" AND coordinates.lat < ").append(request_holder.maxLatitude).append(";");
                }else if (request_holder.minLatitude!=0.0f && request_holder.maxLatitude==0.0f){
                    query.append(" WHERE coordinates.lat > ").append(request_holder.minLatitude).append(";");
                }else if (request_holder.minLatitude==0.0f && request_holder.maxLatitude!=0.0f){
                    query.append(" WHERE coordinates.lat < ").append(request_holder.maxLatitude).append(";");
                }

                results_latitude.addAll(executeIDQuery(query.toString(), c));
            }

            // Check longitude
            if(request_holder.longitude_list.size()>0)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("station.* ").append("FROM station ").append("INNER JOIN location ON location.id = station.id_location ").append("INNER JOIN coordinates ON coordinates.id = location.id_coordinates ").toString());
                if ((request_holder.minLongitude!=0.0f) && (request_holder.maxLongitude!=0.0f))
                {
                    query.append(" WHERE coordinates.lon > ").append(request_holder.minLongitude).append(" AND coordinates.lon < ").append(request_holder.maxLongitude).append(";");
                }else if (request_holder.minLongitude!=0.0f && request_holder.maxLongitude==0.0f){
                    query.append(" WHERE coordinates.lon > ").append(request_holder.minLongitude).append(";");
                }else if (request_holder.minLongitude==0.0f && request_holder.maxLongitude!=0.0f){
                    query.append(" WHERE coordinates.lon < ").append(request_holder.maxLongitude).append(";");
                }

                results_longitude.addAll(executeIDQuery(query.toString(), c));
            }

            // Check altitude
            if(request_holder.altitude_list.size()>0)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("station.* ").append("FROM station ").append("INNER JOIN location ON location.id = station.id_location ").append("INNER JOIN coordinates ON coordinates.id = location.id_coordinates ").toString());
                if (request_holder.minAlt!=0.0f && request_holder.maxAlt!=0.0f)
                {
                    query.append(" WHERE coordinates.altitude > ").append(request_holder.minAlt).append(" AND coordinates.altitude < ").append(request_holder.maxAlt).append(";");
                }else if (request_holder.minAlt!=0.0f && request_holder.maxAlt==0.0f){
                    query.append(" WHERE coordinates.altitude > ").append(request_holder.minAlt).append(";");
                }else if (request_holder.minAlt==0.0f && request_holder.maxAlt!=0.0f){
                    query.append(" WHERE coordinates.altitude<").append(request_holder.maxAlt).append(";");
                }

                results_altitude.addAll(executeIDQuery(query.toString(), c));

            }

            // Check installed date
            if(request_holder.installed_list.size()>0)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT * FROM station ").toString());
                if (request_holder.minInstall!="" && request_holder.maxInstall!="")
                {
                    query = query.append("WHERE date_from>'").append(request_holder.minInstall).append("' AND date_from<'").append(request_holder.maxInstall).append("';");
                }else if (request_holder.minInstall=="" && request_holder.maxInstall!=""){
                    query = query.append("WHERE date_from<'").append(request_holder.maxInstall).append("';");
                }else if (request_holder.minInstall!="" && request_holder.maxInstall==""){
                    query = query.append("WHERE date_from>'").append(request_holder.minInstall).append("';");
                }   
                results_installed_date.addAll(executeIDQuery(query.toString(), c));
            }

            // Check removed date
            if(request_holder.removed_list.size()>0)
            {
                DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
                String date = dateformat.format(new Date());
                Date mydatemin = null;
                Date mydatemax = null;
                try{
                    if (request_holder.minRemoved!="")
                    {    
                        mydatemin = dateformat.parse(request_holder.minRemoved);
                    }else{
                        mydatemin = dateformat.parse(date); 
                    }
                    if (request_holder.maxRemoved!="")
                    {
                        mydatemax = dateformat.parse(request_holder.maxRemoved);
                    }else{
                        mydatemin = dateformat.parse(date);
                    }    
                    Date currentdate = dateformat.parse(date);
                    query = new StringBuilder(new StringBuilder().append("SELECT * FROM station ").toString());
                    if ((mydatemin.after(currentdate) || mydatemin.equals(currentdate))||(mydatemax.after(currentdate) || mydatemax.equals(currentdate)))
                    {
                        query = query.append("WHERE NULLIF(date_to, date_to) IS NULL;");
                        results_removed_date.addAll(executeIDQuery(query.toString(), c));
                    }else{
                        if (request_holder.minRemoved!="" && request_holder.maxRemoved!="")
                        {
                            query = query.append("WHERE date_to>'").append(request_holder.minRemoved).append("' AND date_to<'").append(request_holder.maxRemoved).append("';");
                        }else if (request_holder.minRemoved=="" && request_holder.maxRemoved!=""){
                            query = query.append("WHERE date_to<'").append(request_holder.maxRemoved).append("';");
                        }else if (request_holder.minRemoved!="" && request_holder.maxRemoved==""){
                            query = query.append("WHERE date_to>'").append(request_holder.minRemoved).append("';");
                        }
                        results_removed_date.addAll(executeIDQuery(query.toString(), c));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            // Check network
            for(int index=0; index<request_holder.network_list.size(); index++)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("station_network.* ").append("FROM station_network ").append("INNER JOIN network ON network.id = station_network.id_network ").append("WHERE name ILIKE '").append(request_holder.network_list.get(index)).append("%';").toString());
                results_network.addAll(executeIDStationQuery(query.toString(), c));
            }

            // Check inverse network
            for(int index=0; index<request_holder.inverse_networks_list.size(); index++)
            {
                //query = new StringBuilder(new StringBuilder().append("SELECT * FROM station_network ").append("WHERE id_network != (").append("SELECT id FROM network ").append("WHERE name ILIKE '").append(request_holder.inverse_networks_list.get(index)).append("%'").append(") ").append("AND id_station NOT IN (").append("SELECT id_station FROM station_network ").append("WHERE id_network = ( ").append("SELECT id FROM network ").append(" WHERE name ILIKE '").append(request_holder.inverse_networks_list.get(index)).append("%'").append(")").append(")").append("ORDER BY id_station;").toString());
                query = new StringBuilder(new StringBuilder().append("SELECT DISTINCT id_station FROM station_network WHERE id_station NOT IN (SELECT id_station FROM station_network WHERE id_network in ( SELECT id FROM network  WHERE name ILIKE '").append(request_holder.inverse_networks_list.get(index)).append("%'").append("));").toString());
                ArrayList<Integer> results_station_ids = executeIDStationQuery(query.toString(), c);
                for (Integer results_station_id : results_station_ids) {
                    if (!results_inverse_networks.contains(results_station_id))
                        results_inverse_networks.add(results_station_id);
                }
            }

            // Check agency
            for(int index=0; index<request_holder.agency_list.size(); index++)
            {
                // Get list of Contact IDs
                query = new StringBuilder(new StringBuilder().append("SELECT station_contact.* ").append("FROM station_contact ").append("INNER JOIN contact ON contact.id = station_contact.id_contact ").append("INNER JOIN agency ON agency.id = contact.id_agency ").append("WHERE agency.name ILIKE '").append(request_holder.agency_list.get(index)).append("%';").toString());

                results_agency.addAll(executeIDStationQuery(query.toString(), c));
            }

            // Check antenna type
            for(int index=0; index<request_holder.antennaT_list.size(); index++)
            {
                // Get item list
                query = new StringBuilder(new StringBuilder().append("SELECT item.id ").append("FROM filter_antenna ").append("INNER JOIN antenna_type ON antenna_type.id = filter_antenna.id_antenna_type ").append("INNER JOIN item_attribute ON item_attribute.id = filter_antenna.id_item_attribute ").append("INNER JOIN item ON item.id = item_attribute.id_item ").append("WHERE antenna_type.name ILIKE'").append(request_holder.antennaT_list.get(index)).append("' ").append("ORDER BY item.id;").toString());

                ArrayList<Integer> results_item = executeIDQuery(query.toString(), c);

                if(results_item.size() > 0)
                {    
                    query = new StringBuilder("SELECT * FROM station_item WHERE ");
                    for (int i=0; i<results_item.size(); i++)
                    {
                        if (i==0)
                            query.append(" id_item = ").append(results_item.get(i));
                        else
                            query.append(" OR id_item = ").append(results_item.get(i));
                    }
                    query.append(";");
                    results_antenna_type.addAll(executeIDStationQuery(query.toString(), c));
                }              
            }

            // Check receiver type
            for(int index=0; index<request_holder.receiverT_list.size(); index++)
            {
                // Get item list
                query = new StringBuilder(new StringBuilder().append("SELECT item.id ").append("FROM filter_receiver ").append("INNER JOIN receiver_type ON receiver_type.id = filter_receiver.id_receiver_type ").append("INNER JOIN item_attribute ON item_attribute.id = filter_receiver.id_item_attribute ").append("INNER JOIN item ON item.id = item_attribute.id_item ").append("WHERE receiver_type.name ILIKE'").append(request_holder.receiverT_list.get(index)).append("' ").append("ORDER BY item.id;").toString());

                ArrayList<Integer> results_item = executeIDQuery(query.toString(), c);

                if(results_item.size() > 0)
                {
                    query = new StringBuilder("SELECT * FROM station_item WHERE ");
                    for (int i=0; i<results_item.size(); i++)
                    {
                        if (i==0)
                            query.append(" id_item = ").append(results_item.get(i));
                        else
                            query.append(" OR id_item = ").append(results_item.get(i));
                    }
                    query.append(";");
                    results_receiver_type.addAll(executeIDStationQuery(query.toString(), c));
                }    
            }

            // Check radome type
            for(int index=0; index<request_holder.radomeT_list.size(); index++)
            {
                // Get item list
                query = new StringBuilder(new StringBuilder().append("SELECT item.id ").append("FROM filter_radome ").append("INNER JOIN radome_type ON radome_type.id = filter_radome.id_radome_type ").append("INNER JOIN item_attribute ON item_attribute.id = filter_radome.id_item_attribute ").append("INNER JOIN item ON item.id = item_attribute.id_item ").append("WHERE radome_type.name ILIKE'").append(request_holder.radomeT_list.get(index)).append("' ").append("ORDER BY item.id;").toString());

                ArrayList<Integer> results_item = executeIDQuery(query.toString(), c);
                
                if(results_item.size() > 0)
                {
                    query = new StringBuilder("SELECT * FROM station_item WHERE ");
                    for (int i=0; i<results_item.size(); i++)
                    {
                        if (i==0)
                            query.append(" id_item = ").append(results_item.get(i));
                        else
                            query.append(" OR id_item = ").append(results_item.get(i));
                    }
                    query.append(";");
                    results_radome_type.addAll(executeIDStationQuery(query.toString(), c));
                }    
            }

            // Check country
            for(int index=0; index<request_holder.country_list.size(); index++)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("station.* ").append("FROM station ").append("INNER JOIN location ON location.id = station.id_location ").append("INNER JOIN city ON city.id = location.id_city ").append("INNER JOIN state ON state.id = city.id_state ").append("INNER JOIN country ON country.id = state.id_country ").append("WHERE country.name ILIKE '%").append(request_holder.country_list.get(index)).append("%' ").append("ORDER BY id;").toString());
                results_country.addAll(executeIDQuery(query.toString(), c));
            }

            // Check state
            for(int index=0; index<request_holder.state_list.size(); index++)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("station.* ").append("FROM station ").append("INNER JOIN location ON location.id = station.id_location ").append("INNER JOIN city ON city.id = location.id_city ").append("INNER JOIN state ON state.id = city.id_state ").append("WHERE state.name ILIKE '%").append(request_holder.state_list.get(index)).append("%' ").append("ORDER BY id;").toString());
                results_state.addAll(executeIDQuery(query.toString(), c));
            }

            // Check city
            for(int index=0; index<request_holder.city_list.size(); index++)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("station.* ").append("FROM station ").append("INNER JOIN location ON location.id = station.id_location ").append("INNER JOIN city ON city.id = location.id_city ").append("WHERE city.name ILIKE '%").append(request_holder.city_list.get(index)).append("%' ").append("ORDER BY id;").toString());
                results_city.addAll(executeIDQuery(query.toString(), c));
            }

            // Check coordinates
            switch (request_holder.coordinates_type)
            {
                // Handle rectangle queries
                case "rectangle":
                    ArrayList<Station> coord_rec = getStationCoordinates(request_holder.minLat, request_holder.maxLat, request_holder.minLon, request_holder.maxLon);
                    for (Station aCoord_rec : coord_rec) results_coordinates.add(aCoord_rec.getId());
                    break;

                // Handle circle queries
                case "circle":
                    ArrayList<Station> coord_cir = getStationCoordinates(request_holder.lat, request_holder.lon, request_holder.radius);
                    for (Station aCoord_cir : coord_cir) results_coordinates.add(aCoord_cir.getId());
                    break;

                // Handle polygon queries
                case "polygon":
                    ArrayList<Station> coord_pol = getStationCoordinates(request_holder.coordinates_list);
                    for (Station aCoord_pol : coord_pol) results_coordinates.add(aCoord_pol.getId());
                    break;
            }

            // Check satellite
            for(int index=0; index<request_holder.satellite_list.size(); index++)
            {
                // Get item list
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("* ").append("FROM item_attribute ").append("WHERE value_varchar ILIKE '%").append(request_holder.satellite_list.get(index)).append("%' ").append("AND id_attribute = (").append("                       SELECT id ").append("                       FROM attribute ").append("                       WHERE name = 'satellite_system'").append("                   );").toString());

                ArrayList<Integer> results_item = executeIDItemQuery(query.toString(), c);

                if(results_item.size() > 0)
                {    
                    query = new StringBuilder("SELECT * FROM station_item WHERE ");
                    for (int i=0; i<results_item.size(); i++)
                    {
                        if (i==0)
                            query.append("id_item=").append(results_item.get(i));
                        else
                            query.append(" OR id_item=").append(results_item.get(i));
                    }
                    query.append(";");
                    ArrayList<Integer> results_station_ids = executeIDStationQuery(query.toString(), c);
                    for (Integer results_station_id : results_station_ids) {
                        if (!results_satellite.contains(results_station_id))
                            results_satellite.add(results_station_id);
                    }
                }
            }

            // Check station type
            for(int index=0; index<request_holder.station_type_list.size(); index++)
            {
                // Query to be executed
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("* ").append("FROM station ").append("WHERE id_station_type = (").append("                           SELECT ").append("                           id ").append("                           FROM station_type ").append("                           WHERE name='").append(request_holder.station_type_list.get(index)).append("'").append("                         ) ").append("ORDER BY id;").toString());
                //Execute query
                results_station_type.addAll(executeIDQuery(query.toString(), c));
            }
            
            // Check date range
            if(request_holder.date_range_list.size()>0){    
                String strdate_range ="";
                for(int index=0; index<request_holder.date_range_list.size(); index++)
                {
                    strdate_range += request_holder.date_range_list.get(index);
                }

                if (strdate_range.substring(0,1).equalsIgnoreCase("|")==true)//check if the user has entered the dates_range0 
                {
                    dates_range0 = "";
                }else{
                    dates_range0 = strdate_range.substring(0,10);
                }
                
                if (strdate_range.substring(strdate_range.length() - 1).equalsIgnoreCase("|")==true)//check if the user has entered the dates_range1 
                {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();//take the current date
                    dates_range1 = dateFormat.format(date);
                }else{
                    if (strdate_range.substring(0,1).equalsIgnoreCase("|")==true)
                    {
                        dates_range1 = strdate_range.substring(1,11);
                    }else{    
                        dates_range1 = strdate_range.substring(11,21);
                    }    
                }
                if(dates_range0.equalsIgnoreCase("")==true)
                {
                    query = new StringBuilder(new StringBuilder().append("SELECT ").append("id_station ").append("FROM rinex_file ").append("WHERE reference_date<='").append(dates_range1).append(" 23:59:59';").toString());
                }else{    
                    query = new StringBuilder(new StringBuilder().append("SELECT ").append("id_station ").append("FROM rinex_file ").append("WHERE reference_date >='").append(dates_range0).append("' AND reference_date<='").append(dates_range1).append(" 23:59:59';").toString());
                }
                results_date_range.addAll(executeIDStationQuery(query.toString(), c));
            }

            // Check file type
            for(int index=0; index<request_holder.file_type_list.size(); index++)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("rf.id_station ").append("FROM rinex_file as rf JOIN file_type as ft ON rf.id_file_type = ft.id WHERE ft.format = '").append(request_holder.file_type_list.get(index)).append("';").toString());
                results_file_type.addAll(executeIDStationQuery(query.toString(), c));
            }
            // Check sampling_frequency
            for(int index=0; index<request_holder.sampling_frequency_list.size(); index++)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("rf.id_station ").append("FROM rinex_file as rf JOIN file_type as ft ON rf.id_file_type = ft.id WHERE ft.sampling_frequency = '").append(request_holder.sampling_frequency_list.get(index)).append("';").toString());
                results_sampling_frequency.addAll(executeIDStationQuery(query.toString(), c));
            }
            // Check sampling_window
            for(int index=0; index<request_holder.sampling_window_list.size(); index++)
            {
                query = new StringBuilder(new StringBuilder().append("SELECT ").append("rf.id_station ").append("FROM rinex_file as rf JOIN file_type as ft ON rf.id_file_type = ft.id WHERE ft.sampling_window = '").append(request_holder.sampling_window_list.get(index)).append("';").toString());
                results_sampling_window.addAll(executeIDStationQuery(query.toString(), c));
            }

            // Count valid parameters
            if (request_holder.marker_list.size() > 0)
                counter++;
            if (request_holder.site_list.size() > 0)
                counter++;
            if (request_holder.height_list.size() > 0)
                counter++;
            if (request_holder.latitude_list.size() > 0)
                counter++;
            if (request_holder.longitude_list.size() > 0)
                counter++;
            if (request_holder.altitude_list.size() > 0)
                counter++;
            if (request_holder.installed_list.size() > 0)
                counter++;
            if (request_holder.removed_list.size() > 0)
                counter++;
            if (request_holder.network_list.size() > 0)
                counter++;
            if (request_holder.agency_list.size() > 0)
                counter++;
            if (request_holder.antennaT_list.size() > 0)
                counter++;
            if (request_holder.receiverT_list.size() > 0)
                counter++;
            if (request_holder.radomeT_list.size() > 0)
                counter++;
            if (request_holder.country_list.size() > 0)
                counter++;
            if (request_holder.state_list.size() > 0)
                counter++;
            if (request_holder.city_list.size() > 0)
                counter++;
            if (request_holder.coordinates_type.contentEquals("rectangle") ||
                    request_holder.coordinates_type.contentEquals("circle") ||
                    request_holder.coordinates_type.contentEquals("polygon"))
                counter++;
            if (request_holder.satellite_list.size() > 0)
                counter++;
            if (request_holder.station_type_list.size() > 0)
                counter++;
            if (request_holder.inverse_networks_list.size() > 0)
                counter++;
            if (request_holder.date_range_list.size() > 0)
                counter++;
            if (request_holder.file_type_list.size() > 0)
                counter++;
            if (request_holder.sampling_frequency_list.size() > 0)
                counter++;
            if (request_holder.sampling_window_list.size() > 0)
                counter++;

            // Merge results
            ArrayList<Integer> merged_Ids = new ArrayList<>();
            merged_Ids.addAll(results_marker);
            merged_Ids.addAll(results_site);
            merged_Ids.addAll(results_height);
            merged_Ids.addAll(results_latitude);
            merged_Ids.addAll(results_longitude);
            merged_Ids.addAll(results_altitude);
            merged_Ids.addAll(results_installed_date);
            merged_Ids.addAll(results_removed_date);
            merged_Ids.addAll(results_network);
            merged_Ids.addAll(results_agency);
            merged_Ids.addAll(results_antenna_type);
            merged_Ids.addAll(results_receiver_type);
            merged_Ids.addAll(results_radome_type);
            merged_Ids.addAll(results_country);
            merged_Ids.addAll(results_state);
            merged_Ids.addAll(results_city);
            merged_Ids.addAll(results_coordinates);
            merged_Ids.addAll(results_satellite);
            merged_Ids.addAll(results_station_type);
            merged_Ids.addAll(results_inverse_networks);
            merged_Ids.addAll(results_date_range);
            merged_Ids.addAll(results_file_type);
            merged_Ids.addAll(results_sampling_frequency);
            merged_Ids.addAll(results_sampling_window);
            
            // Check frequency of results
            ArrayList<Integer> stations_Ids = new ArrayList<>();
            for (int i=0; i<merged_Ids.size(); i++)
            {
                if (Collections.frequency(merged_Ids, merged_Ids.get(i)) == counter)
                    if (!stations_Ids.contains(merged_Ids.get(i))) {
                        stations_Ids.add(merged_Ids.get(i));
                    }
            }
            
            
            // T2 treatment
            String queryT2 = "SELECT st.id,string_agg(CAST(rf.reference_date as varchar), ',' ORDER BY rf.reference_date) as list_reference_date "
                    + "FROM station as st "
                    + "JOIN rinex_file as rf ON rf.id_station=st.id "
                    + "JOIN file_type as ft ON ft.id=rf.id_file_type "
                    + "JOIN data_center_structure as dtcs ON dtcs.id=rf.id_data_center_structure "
                    + "JOIN data_center as dtc ON dtc.id=dtcs.id_data_center";
                    
            // Check date range
            if(request_holder.date_range_list.size()>0){    
                if (queryT2.substring(queryT2.length() - 6).equalsIgnoreCase("center"))
                {
                    if(dates_range0.equalsIgnoreCase("")==true)
                    {    
                        queryT2 = queryT2 + " WHERE rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }else{
                        queryT2 = queryT2 + " WHERE rf.reference_date>='" + dates_range0 + "' AND rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }
                }else{
                    if(dates_range0.equalsIgnoreCase("")==true)
                    {    
                        queryT2 = queryT2 + " AND rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }else{
                        queryT2 = queryT2 + " AND rf.reference_date>='" + dates_range0 + "' AND rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }    
                }    

            }

            // Check file type
            if(request_holder.file_type_list.size()>0){
                for(int index=0; index<request_holder.file_type_list.size(); index++)
                {
                    
                    if (queryT2.substring(queryT2.length() - 6).equalsIgnoreCase("center"))
                    {
                        queryT2 = queryT2 + " WHERE ft.format ='" + request_holder.file_type_list.get(index) + "'";
                    }else{
                        queryT2 = queryT2 + " AND ft.format ='" + request_holder.file_type_list.get(index) + "'";
                    } 

                }
            }    
            // Check sampling frequency
            if(request_holder.sampling_frequency_list.size()>0){
                for(int index=0; index<request_holder.sampling_frequency_list.size(); index++)
                {
                    if (queryT2.substring(queryT2.length() - 6).equalsIgnoreCase("center"))
                    {
                        queryT2 = queryT2 + " WHERE ft.sampling_frequency ='" + request_holder.sampling_frequency_list.get(index) + "'";
                    }else{
                        queryT2 = queryT2 + " AND ft.sampling_frequency ='" + request_holder.sampling_frequency_list.get(index) + "'";
                    }

                }
            }
            // Check sampling window
            if(request_holder.sampling_window_list.size()>0){
                for(int index=0; index<request_holder.sampling_window_list.size(); index++)
                {
                    if (queryT2.substring(queryT2.length() - 6).equalsIgnoreCase("center"))
                    {
                        queryT2 = queryT2 + " WHERE ft.sampling_window ='" + request_holder.sampling_window_list.get(index) + "'";
                    }else{
                        queryT2 = queryT2 + " AND ft.sampling_window ='" + request_holder.sampling_window_list.get(index) + "'";
                    }

                }
            }

            // Check data availability
            if(request_holder.data_availability_list.size()>0)
            {
                if (stations_Ids.isEmpty()&&counter==0)
                {
                    String queryallstation = "Select id from station";
                    try {
                                s = c.createStatement();
                                rs = s.executeQuery(queryallstation);
                                while (rs.next())
                                {
                                    stations_Ids.add(rs.getInt("id"));
                                }
                                
                        } catch (SQLException e) {
                                e.printStackTrace();
                        }
                }
                
                for (int i=stations_Ids.size()-1; i>=0; i--)//inverse loop because possibility to remove from arraylist to avoid to disturb the index     
                {

                    if(request_holder.date_range_list.size()>0)
                    {
                        if(dates_range0.equalsIgnoreCase("")==true)//the user hasn't entered the dates_range0
                        {    
                            String queryfirstdate = "Select date_from from station where id=" + stations_Ids.get(i);
                            s = c.createStatement();
                            rs = s.executeQuery(queryfirstdate);
                            rs.next();
                            dates_range0 = rs.getString(1).substring(0,10);
                        }
                        LocalDate startDate = LocalDate.parse(dates_range0);
                        LocalDate endtDate = LocalDate.parse(dates_range1);

                        int nbtotaldaysbetween = (int)ChronoUnit.DAYS.between(startDate, endtDate) + 1;//100%

                        String querycount = "Select COUNT(DISTINCT reference_date) from  rinex_file where reference_date >='" + dates_range0 + "' AND reference_date <='" + dates_range1 + " 23:59:59" + "' AND id_station=" + stations_Ids.get(i);
                        try {
                            s = c.createStatement();
                            rs = s.executeQuery(querycount);
                            rs.next();
                            int nbfiles = rs.getInt(1);
                            int data_availability_percent = 0;
                            if(nbfiles>=nbtotaldaysbetween)
                            {
                                data_availability_percent = 100;
                            }else{    
                                data_availability_percent = (nbfiles * 100)/nbtotaldaysbetween;
                            }    

                            for(int index=0; index<request_holder.data_availability_list.size(); index++)
                            {
                                if(data_availability_percent < Integer.parseInt(request_holder.data_availability_list.get(index)))
                                {    
                                    stations_Ids.remove(stations_Ids.get(i));
                                }    
                            }

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }

                    }else{//the user hasn't entered dates_range0 and dates_range1
                        String queryextremedate = "Select date_from,date_to from station where id=" + stations_Ids.get(i);
                        try {
                                s = c.createStatement();
                                rs = s.executeQuery(queryextremedate);
                                String enddate = "";
                                while (rs.next())    
                                {
                                    if(rs.getString(2)==null)
                                    {
                                        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
                                        enddate = df.format(new Date());
                                    }else{
                                        enddate = rs.getString(2).substring(0,10);
                                    }
                                    String begindate = rs.getString(1).substring(0,10);                                
                                    LocalDate startDate = LocalDate.parse(begindate);
                                    LocalDate endtDate = LocalDate.parse(enddate);
                                    int nbtotaldaysbetween = (int)ChronoUnit.DAYS.between(startDate, endtDate) + 1;//100%
                                    String querycount = "Select COUNT(DISTINCT reference_date) from rinex_file where id_station=" + stations_Ids.get(i);
                                    rs = s.executeQuery(querycount);
                                    rs.next();
                                    int nbfiles = rs.getInt(1);
                                    int data_availability_percent = 0;
                                    if(nbfiles>=nbtotaldaysbetween)
                                    {
                                        data_availability_percent = 100;
                                    }else{    
                                        data_availability_percent = (nbfiles * 100)/nbtotaldaysbetween;
                                    }    

                                    for(int index=0; index<request_holder.data_availability_list.size(); index++)
                                    {
                                        if(data_availability_percent < Integer.parseInt(request_holder.data_availability_list.get(index)))
                                        {    
                                            stations_Ids.remove(stations_Ids.get(i));
                                        }    
                                    }
                                }

                            } catch (SQLException e) {
                                e.printStackTrace();
                            }
                    }
                    rs.close();
                    s.close();    
                }
                
            }
            
            // Check minimum observation
            if(request_holder.minimumObservation>0.0)
            {
                if (stations_Ids.isEmpty()&&counter==0)
                {
                    String queryallstation = "Select id from station";
                    try {
                                s = c.createStatement();
                                rs = s.executeQuery(queryallstation);
                                while (rs.next())
                                {
                                    stations_Ids.add(rs.getInt("id"));
                                }
                                
                        } catch (SQLException e) {
                                e.printStackTrace();
                        }
                }
                String querycount = "";
                for (int i=stations_Ids.size()-1; i>=0; i--)//inverse loop because possibility to remove from arraylist to avoid to disturb the index     
                {
                    if(request_holder.date_range_list.size()>0)
                    {
                        if(dates_range0.equalsIgnoreCase("")==true)//the user hasn't entered the dates_range0
                        {    
                            String queryfirstdate = "Select date_from from station where id=" + stations_Ids.get(i);
                            s = c.createStatement();
                            rs = s.executeQuery(queryfirstdate);
                            rs.next();
                            dates_range0 = rs.getString(1).substring(0,10);
                        }
                        querycount = "Select COUNT(DISTINCT reference_date) from  rinex_file where reference_date >='" + dates_range0 + "' AND reference_date <='" + dates_range1 + " 23:59:59" + "' AND id_station=" + stations_Ids.get(i);
                    }else{
                        querycount = "Select COUNT(DISTINCT reference_date) from rinex_file where id_station=" + stations_Ids.get(i);
                    }    
                    try {
                            s = c.createStatement();
                            rs = s.executeQuery(querycount);

                            while (rs.next())    
                            {
                                if(rs.getInt(1)>0)
                                {
                                    int nbfiles = rs.getInt(1);
                                    float nbyear = (float)nbfiles/365;

                                    if(nbyear < request_holder.minimumObservation)
                                    {    
                                        stations_Ids.remove(stations_Ids.get(i));
                                    }    

                                }else{
                                    stations_Ids.remove(stations_Ids.get(i));
                                }
                            }

                        } catch (SQLException e) {
                            e.printStackTrace();
                        }   
                    
                    rs.close();
                    s.close();    
                }
                
            }

            queryT2 = queryT2 + " GROUP BY st.id ";

            if (stations_Ids.size() > 0)
            {
                queryT2 = queryT2 + " HAVING";
                for (int i=0; i<stations_Ids.size(); i++)
                {
                    if (i==0)
                        queryT2 = queryT2 + " st.id=" + stations_Ids.get(i);
                    else
                        queryT2 = queryT2 + " OR st.id=" + stations_Ids.get(i);
                }
            }else{
                queryT2 = queryT2 + " HAVING st.id=-1";//To avoid return anything
            }
            
            queryT2 = queryT2 + ";";
            
            //Execute files query
            Map<Integer, String> dict_reference_date = executeFilesQuery(queryT2, c);
                       
            //Search results T1 in dictionary and add T2
            JsonCache mycache = new JsonCache();
            
            
            if (stations_Ids.size() > 0)
            {
                for (int i=0; i<stations_Ids.size(); i++)
                {
                    Station mystation = new Station();
                    mystation = mycache.get(stations_Ids.get(i));
                    if(dict_reference_date.get(stations_Ids.get(i))!=null)//check if station have a rinex files
                    {    
                        mystation.addRinexFiles(dict_reference_date.get(stations_Ids.get(i)));
                    }    
                    results.add(mystation);
                }
            }
               

        } catch (SocketException | ClassNotFoundException | SQLException | ParseException e) {
            e.printStackTrace();
        }

        return results;
    }

    /**
     * GET STATIONS SHORT
     * This method returns a ArrayList with data from all stations on the database.
     * @deprecated
     */
    ArrayList<Station> getStationsShort()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "select station.id, station.name as name, station.marker, array_to_string(array_agg(distinct(agency.name)), ' & ') as agency, array_to_string(array_agg(distinct(network.name)), ' & ') as network, station.date_from, station.date_to, coordinates.lat, coordinates.lon, coordinates.altitude, city.name as city, state.name as state, country.name as country" + " from station, coordinates, city, state, country, location, station_contact, contact, agency, station_network, network" + " where station.id_location = location.id" + " and location.id_coordinates = coordinates.id" + " and location.id_city = city.id" + " and state.id = city.id_state" + " and state.id_country = country.id" + " and station_contact.id_station = station.id" + " and station_contact.id_contact = contact.id" + " and contact.id_agency = agency.id" + " and station_network.id_station = station.id" + " and station_network.id_network = network.id" + " group by station.id, coordinates.lat, coordinates.lon, coordinates.altitude, city.name, state.name, country.name";

            //Execute query
            //results = executeStationShortQueryOld(results, query, c);
            results = executeStationShortQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     *
     * GET STATIONS SHORT
     * This method returns a ArrayList with data from all stations on the database.
     */
    Station getStationsShortByMarker(String marker)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "select " +
                    " station.id, station.name as name, station.marker, " +
                    " array_to_string(array_agg(distinct(agency.name)), ' & ') as agency, " +
                    " array_to_string(array_agg(distinct(network.name)), ' & ') as network, " +
                    " station.date_from, station.date_to, " +
                    " coordinates.lat, coordinates.lon, coordinates.altitude, " +
                    " city.name as city, " +
                    " state.name as state, " +
                    " country.name as country, " +
                    " markerlongname(station.marker, station.monument_num, station.receiver_num, station.country_code) " +
                    " from station, coordinates, city, state, country, location, station_contact, contact, agency, station_network, network " +
                    " where station.id_location = location.id " +
                    " and location.id_coordinates = coordinates.id " +
                    " and location.id_city = city.id" +
                    " and state.id = city.id_state" +
                    " and state.id_country = country.id" +
                    " and station_contact.id_station = station.id" +
                    " and station_contact.id_contact = contact.id" +
                    " and contact.id_agency = agency.id" +
                    " and station_network.id_station = station.id" +
                    " and station_network.id_network = network.id" +
                    " and station.marker = '" + marker +"' " +
                    " group by station.id, coordinates.lat, coordinates.lon, coordinates.altitude, city.name, state.name, country.name";

            //Execute query
            //results = executeStationShortQueryOld(results, query, c);
            results = executeStationShortQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results.get(0);
    }

    /**
     * EXECUTE STATION SHORT QUERY
     * This method executes a query that returns a short information about the stations
     */
    ArrayList<Station> executeStationShortQuery(String query, Connection c) throws SQLException
    {
        ArrayList<Station> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        while (rs.next())
        {
            // Create Station object
            Station s = new Station();

            s.setId(rs.getInt("id"));
            s.setName(rs.getString("name"));
            s.setMarker(rs.getString("marker"));
            s.setMarkerLongName(rs.getString("markerlongname"));
            s.setDate_from(rs.getString("date_from"));
            s.setDate_to(rs.getString("date_to"));


            Country country = new Country(0, rs.getString("country"), "");
            State state = new State(0, country, rs.getString("state"));
            City city = new City(0, state, rs.getString("city"));
            Coordinates coordinates = new Coordinates(0, 0, 0, 0, rs.getDouble("lat"), rs.getDouble("lon"), rs.getDouble("altitude"));
            Location location = new Location(0, city, coordinates, null, "");
            s.setLocation(location);

            Agency agency = new Agency(0, rs.getString("agency"), "", "", "", "");
            Contact contact = new Contact(0, "", "", "", "", "", "", agency, "");
            Station_contact station_contact = new Station_contact(0, 0, contact, "");
            s.addStationConacts(station_contact);

            Network network = new Network(0, rs.getString("network"), contact);
            Station_network station_network = new Station_network(0, 0, network);
            s.addStationNetwork(station_network);

            // Add object to ArrayList
            s.addStationConacts(station_contact);
            s.addStationNetwork(station_network);
            results.add(s);

        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }



    /**
     * EXECUTE STATION SHORT ID
     * This method executes a query that returns the ID of all the stations with products
     */
    ArrayList<Station> executeStationShortQueryID(String query, Connection c) throws SQLException
    {
        ArrayList<Station> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        while (rs.next())
        {
            // Create Station object
            Station s = new Station();

            s.setId(rs.getInt("id"));
            results.add(s);

        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }


    /**
     * GET STATIONS EMBARGO
     * This method returns a ArrayList with data from all stations that are 'embargo'.
     */
    ArrayList getStationsEmbargo(Integer embargo)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();

        try {
            c = NewConnection();
            // Query to be executed
            String query = "SELECT " +
                    "* " +
                    "FROM station " +
                    "INNER JOIN rinex_file ON rinex_file.id_station = station.id " +
                    "WHERE rinex_file.status = '" + embargo +"'";
            System.out.println(query);

            //Execute query
            results = executeStationQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private Connection NewConnection() throws SocketException, ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user",DBConsts.DB_USERNAME);
        props.setProperty("password",DBConsts.DB_PASSWORD);
        //props.setProperty("logUnclosedConnections","true");
        Connection c = DriverManager.getConnection(DBConnectionString, props);
        c.setAutoCommit(false);
        return c;
    }

   /* public Station loadStationInformation(String site) {

        Connection c = null;
        Station station = new Station();


        PreparedStatement prepS = null;
        try {
            prepS = c.prepareStatement(
                    "SELECT * FROM station where marker = '"+site+"'");
            rs = prepS.executeQuery();

            while (rs.next() == true){
                station.setId(rs.getInt("id"));
                station.setName(rs.getString("name"));
                station.setMarker(rs.getString("marker"));
                station.setDescription(rs.getString("description"));
                station.setDate_from(rs.getString("date_from"));
                station.setDate_to(rs.getString("date_to"));
                station.setComment(rs.getString("comment"));
                station.setLocation(rs.getInt("id_location"));
                station.setMonument((rs.getInt("id_monument"));
                station.setGeological(rs.getInt("id_geological"));
                station.setIers_domes(rs.getString("iers_domes"));
                station.setCpd_num(rs.getString("cpd_num"));
                station.setReceiver_num(rs.getInt("receiver_num"));
                station.setCountry_code(rs.getString("country_code"));

            }
            rs.close();
            prepS.close();


        } catch (SQLException e) {
            e.printStackTrace();
        }        finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return station;
    }*/
}
