package Glass;

import Configuration.DBConsts;
import CustomClasses.FilesInfo;
import CustomClasses.ProductDataAvailability;
import EposTables.*;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;


import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.io.IOException;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

//TODO: verify if (when there are multiple agencies/networks) they all appear in the results AND not multiple times the same one

public class ParsersV2 {

    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public String getStationShortV2CSV(ArrayList<Station> data) {
        // Header
        StringBuilder header = new StringBuilder();
        header.append("name, marker_long_name, marker, date_from, date_to, x, y, z, lat, lon, altitude, city, state, country, agencies ,networks\n");

        StringBuilder csv = new StringBuilder();

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            csv.append(aData.getName()).append(", ")
                .append(aData.getMarkerLongName()).append(", ")
                .append(aData.getMarker()).append(", ");

            csv.append(aData.getDate_from()).append(", ").append(aData.getDate_to()).append(", ");

            // *****************************************************************
            // Handle Coordinates
            // *****************************************************************
            csv.append(aData.getLocation().getCoordinates().getX()).append(", ").append(aData.getLocation().getCoordinates().getY()).append(", ").append(aData.getLocation().getCoordinates().getZ()).append(", ").append(aData.getLocation().getCoordinates().getLat()).append(", ").append(aData.getLocation().getCoordinates().getLon()).append(", ").append(aData.getLocation().getCoordinates().getAltitude()).append(", ").append(aData.getLocation().getCity().getName()).append(", ").append(aData.getLocation().getCity().getState().getName()).append(", ").append(aData.getLocation().getCity().getState().getCountry().getName()).append(", ");


            // *****************************************************************
            // Handle Agencies
            // *****************************************************************

            ArrayList agencies_arrayList = new ArrayList();
            for(int i=0; i<aData.getStationContacts().size(); i++){
                if(!agencies_arrayList.contains(aData.getStationContacts().get(i).getContact().getAgency().getName()))
                    agencies_arrayList.add(aData.getStationContacts().get(i).getContact().getAgency().getName());
            }


            StringBuilder agencies = new StringBuilder();
            for(int i=0; i<agencies_arrayList.size(); i++) {
                agencies.append(agencies_arrayList.get(i));
                if(i<agencies_arrayList.size()-1)
                    agencies.append(" & ");
            }
            if (agencies_arrayList.size() > 0)
                csv.append(agencies.toString() + ",");



            for (int j=0; j<aData.getStationNetworks().size(); j++) {
                csv.append(aData.getStationNetworks().get(j).getNetwork().getName());
                if (j + 1 < aData.getStationNetworks().size())
                    csv.append(" & ");
                else
                    csv.append(",");
            }

            csv.append("\n");

        }

        // Join header
        header.append(csv);
        return header.toString();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getStationShortV2JSON(ArrayList<Station> data) {
        JSONArray station_list = new JSONArray();

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            JSONObject station = new JSONObject();
            station.put("marker_long_name", aData.getMarkerLongName());
            station.put("marker", aData.getMarker());
            station.put("name", aData.getName());
            station.put("date_from", aData.getDate_from());
            station.put("date_to", aData.getDate_to());

            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getLocation().getCity().getState().getCountry().getName());

            // *****************************************************************
            // Handle State
            // *****************************************************************
            JSONObject state = new JSONObject();
            state.put("name", aData.getLocation().getCity().getState().getName());

            // *****************************************************************
            // Handle City
            // *****************************************************************
            JSONObject city = new JSONObject();
            city.put("name", aData.getLocation().getCity().getName());

            // *****************************************************************
            // Handle Coordinates
            // *****************************************************************
            JSONObject coordinates = new JSONObject();
            coordinates.put("x", aData.getLocation().getCoordinates().getX());
            coordinates.put("y", aData.getLocation().getCoordinates().getY());
            coordinates.put("z", aData.getLocation().getCoordinates().getZ());
            coordinates.put("lat", aData.getLocation().getCoordinates().getLat());
            coordinates.put("lon", aData.getLocation().getCoordinates().getLon());
            coordinates.put("altitude", aData.getLocation().getCoordinates().getAltitude());

            // *****************************************************************
            // Handle Location
            // *****************************************************************
            JSONObject location = new JSONObject();
            location.put("coordinates", coordinates);
            location.put("country", country);
            location.put("state", state);
            location.put("city", city);
            station.put("location", location);

            // *****************************************************************
            // Handle Agencies
            // *****************************************************************
            JSONObject agency = new JSONObject();

            ArrayList agencies_arrayList = new ArrayList();
            for(int i=0; i<aData.getStationContacts().size(); i++){
                if(!agencies_arrayList.contains(aData.getStationContacts().get(i).getContact().getAgency().getName()))
                    agencies_arrayList.add(aData.getStationContacts().get(i).getContact().getAgency().getName());
            }


            StringBuilder agencies = new StringBuilder();
            for(int i=0; i<agencies_arrayList.size(); i++) {
                agencies.append(agencies_arrayList.get(i));
                if(i<agencies_arrayList.size()-1)
                    agencies.append(" & ");
            }

            agency.put("name", agencies.toString());
            station.put("agency", agency);

            // *****************************************************************
            // Handle Network in String format
            // *****************************************************************
            JSONObject network = new JSONObject();

            StringBuilder networks = new StringBuilder();
            for(int i=0; i<aData.getStationNetworks().size(); i++) {
                networks.append(aData.getStationNetworks().get(i).getNetwork().getName());
                if(i<aData.getStationNetworks().size()-1)
                    networks.append(" & ");
            }

            network.put("name", networks.toString());

            station.put("network", network);

            // *****************************************************************
            // Handle Networks in Array format
            // *****************************************************************
            JSONArray network_array = new JSONArray();
            for (int j = 0; j < aData.getStationNetworks().size(); j++) {
                // *************************************************************
                // Handle Network
                // *************************************************************
                network_array.add(aData.getStationNetworks().get(j).getNetwork().getName());
            }
            station.put("network_array", network_array);


            station_list.add(station);
        }

        return station_list;
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONObject getStationShortV2GEOJSON(ArrayList<Station> data) {
        JSONObject geoJSON = new JSONObject();
        geoJSON.put("type", "FeatureCollection");

        JSONArray features = new JSONArray();
        for (Station aData : data) {

            // *****************************************************************
            // Handle Geometry         Coordinates: "coordinates" : [ lon, lat ]
            // *****************************************************************
            JSONObject geometry = new JSONObject();
            JSONArray coordinates = new JSONArray();
            coordinates.add(aData.getLocation().getCoordinates().getLon());
            coordinates.add(aData.getLocation().getCoordinates().getLat());
            geometry.put("type","Point");
            geometry.put("coordinates", coordinates);

            // *****************************************************************
            // Handle Properties
            // *****************************************************************
            JSONObject properties = new JSONObject();
            properties.put("name", aData.getName());
            properties.put("marker", aData.getMarker());
            properties.put("date_from", aData.getDate_from());
            properties.put("date_to", aData.getDate_to());
            properties.put("city", aData.getLocation().getCity().getName());
            properties.put("state", aData.getLocation().getCity().getState().getName());
            properties.put("country", aData.getLocation().getCity().getState().getCountry().getName());

            JSONObject geocoords = new JSONObject();
            geocoords.put("x", aData.getLocation().getCoordinates().getX());
            geocoords.put("y", aData.getLocation().getCoordinates().getY());
            geocoords.put("z", aData.getLocation().getCoordinates().getZ());
            geocoords.put("latitude", aData.getLocation().getCoordinates().getLat());
            geocoords.put("longitude", aData.getLocation().getCoordinates().getLon());
            geocoords.put("altitude", aData.getLocation().getCoordinates().getAltitude());
            properties.put("geolocation", geocoords);

            // *****************************************************************
            // Handle Agencies
            // *****************************************************************
            JSONObject agency = new JSONObject();

            for(int i=0; i<aData.getStationContacts().size(); i++){
                if(!agency.containsValue(aData.getStationContacts().get(i).getContact().getAgency().getName()))
                    agency.put("name", aData.getStationContacts().get(i).getContact().getAgency().getName());
            }

            properties.put("agency", agency);

            // *****************************************************************
            // Handle Network in String format
            // *****************************************************************
            JSONObject network = new JSONObject();

            for(int i=0; i<aData.getStationNetworks().size(); i++) {
                network.put("name", aData.getStationNetworks().get(i).getNetwork().getName());
            }

            properties.put("network", network);

            // *****************************************************************
            // Add all to the station
            // *****************************************************************
            JSONObject station = new JSONObject();
            station.put("type","Feature");
            station.put("id", aData.getMarkerLongName());
            station.put("properties", properties);
            station.put("geometry", geometry);


            features.add(station);
        }
        geoJSON.put("features", features);

        return geoJSON;
    }
    public JSONObject constructEposStyle(){

        JSONObject specs = new JSONObject();
        specs.put("character","S");
        specs.put("pin","true");
        specs.put("clustering","false");

        JSONObject eposStation = new JSONObject();
        eposStation.put("label","GNSS Station");
        eposStation.put("marker",specs);

        JSONObject eposStyle = new JSONObject();
        eposStyle.put("station",eposStation);

        return eposStyle;
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONObject getStationShortV2GEOJSONics(ArrayList<Station> data, int limit) {

        //These two variables are only really needed on the products portal to get the list of timeserier providers
        JsonCache cache = new JsonCache();
        DataBaseT4Connection dbc = new DataBaseT4Connection();
        Connection t4C = null;
        //setup a database connection to use during the iterative proces  NOTE only needed for productsportal
        if (GlassConstants.PRODUCTSPORTAL) {
            try {
                t4C = dbc.NewConnection();
            } catch (SocketException e) {
                throw new RuntimeException(e);
            } catch (SQLException e) {
                throw new RuntimeException(e);
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        JSONArray mapkeys = new JSONArray();
        mapkeys.add("GNSS Station ID");
        mapkeys.add("Country");
        mapkeys.add("City");
        mapkeys.add("Latitude");
        mapkeys.add("Longitude");
        mapkeys.add("Installed at");
        mapkeys.add("Data Providers");
        mapkeys.add("Networks");
        mapkeys.add("@epos_links");

        JSONArray datakeys = new JSONArray();
        datakeys.add("GNSS Station ID");
        datakeys.add("GNSS Station Marker");
        datakeys.add("Country");
        datakeys.add("Installed at");
        datakeys.add("Data Providers");
        datakeys.add("Networks");
        datakeys.add("longitude , latitude");
        datakeys.add("City");
        datakeys.add("Altitude");
        datakeys.add("x");
        datakeys.add("y");
        datakeys.add("z");
        datakeys.add("Longitude");
        datakeys.add("Latitude");
       // datakeys.add("TimeSeries Data Providers");

        JSONObject geoJSON = new JSONObject();
        geoJSON.put("type", "FeatureCollection");
        geoJSON.put("@epos_style",    constructEposStyle() );

        JSONArray features = new JSONArray();

        //Used when constructing a list of timeseriesproviders
        Set<String> timeSeriesProvidersSet = new HashSet<>(); // has default initial capacity (16)

        int cnt=0;
        System.out.println( "Stations found "+data.size() );
        for (Station aData : data) {
            cnt++;
            //if (cnt%100==0)   System.out.println("Cnt "+cnt);
            if ( limit > 0 ) { if (cnt> limit) break; }
            // *****************************************************************
            // Handle Geometry         Coordinates: "coordinates" : [ lon, lat ]
            // *****************************************************************
            JSONObject geometry = new JSONObject();
            JSONArray coordinates = new JSONArray();
            coordinates.add(aData.getLocation().getCoordinates().getLon());
            coordinates.add(aData.getLocation().getCoordinates().getLat());
            geometry.put("type","Point");
            geometry.put("coordinates", coordinates);

            // *****************************************************************
            // Add all to the station
            // *****************************************************************
            JSONObject station = new JSONObject();
            station.put("type","Feature");
            station.put("geometry", geometry);

            JSONObject properties = new JSONObject();

            properties.put("@epos_type","station");

            properties.put("@epos_map_keys", mapkeys);

            properties.put("@epos_data_keys", datakeys);

            JSONArray eposlinks = new JSONArray();
            JSONObject link = new JSONObject();
            link.put("type","image/png");
            link.put("label","TimeSeries Image");
            //Hard Code the EPOS products portal address

            //String tsimageurl = "https://gnssproducts.epos.ubi.pt/GlassFramework/webresources/products/time-series-plot/ALL/"+aData.getMarker();
            String tsimageurl = "https://gnssproducts.epos.ubi.pt/GlassFramework/webresources/products/time-series-plot/ICS/"+aData.getMarker();


            link.put("href",tsimageurl);
            eposlinks.add(link);
            properties.put("@epos_links",eposlinks);

            station.put("id",                   aData.getMarkerLongName());  //I know this is duplicated below but its useful
            properties.put("Country",              aData.getLocation().getCity().getState().getCountry().getName());
            properties.put("City",                 aData.getLocation().getCity().getName());
            properties.put("GNSS Station Marker",  aData.getMarker());
            properties.put("GNSS Station ID",      aData.getMarkerLongName());
            properties.put("Installed at",         aData.getDate_from());

            properties.put("x",         aData.getLocation().getCoordinates().getX());
            properties.put("y",         aData.getLocation().getCoordinates().getY());
            properties.put("z",         aData.getLocation().getCoordinates().getZ());
            properties.put("Latitude",  aData.getLocation().getCoordinates().getLat());
            properties.put("Longitude", aData.getLocation().getCoordinates().getLon());
            properties.put("Altitude",  aData.getLocation().getCoordinates().getAltitude());

            // *****************************************************************
            // Handle Agencies each one seperated by a space
            // *****************************************************************
            String agency = "";
            for(int i=0; i<aData.getStationContacts().size(); i++){
               if(!agency.contains(aData.getStationContacts().get(i).getContact().getAgency().getName())) {
                   if (i>0) agency = agency + " & ";
                   agency = agency + aData.getStationContacts().get(i).getContact().getAgency().getName() ;
               }
            }
            properties.put("Data Providers", agency);


            if (GlassConstants.PRODUCTSPORTAL) {
                // *****************************************************************
                // Handle Product Data Sets each one seperated by a space
                // *****************************************************************
                String timeseriesproviders = "";
                Integer idStation;
                ArrayList<ProductDataAvailability> dList;

                try {
                    idStation = cache.getKeyByValue(aData.getMarker());
                    dList = dbc.getDataAvailabilityIndividualV2(idStation, GlassConstants.PRODUCTS_ALLTIMESERIES, t4C);
                    if (null == dList || 0 == dList.size())
                        timeseriesproviders = "No Data set provider found";
                    else {
                        timeSeriesProvidersSet.clear();
                        for (ProductDataAvailability p : dList) {
                            timeSeriesProvidersSet.add(p.getAbbreviation().trim());
                        }
                        //convert set to a single string
                        timeseriesproviders = String.join(" ", timeSeriesProvidersSet);
                    }
                } catch (Exception ex) {
                    timeseriesproviders = "Error: No Data set provider found";
                }

                properties.put("TimeSeries Data Providers", timeseriesproviders);
            }


            // *****************************************************************
            // Handle Network in String format - each network seperated by a space
            // *****************************************************************
            String network = "";
            for(int i=0; i<aData.getStationNetworks().size(); i++) {
                if (i>0) network = " & " + network;
                network = aData.getStationNetworks().get(i).getNetwork().getName() + network;
            }
            properties.put("Networks", network);

            station.put("properties",properties);

            features.add(station);
        }
        geoJSON.put("features", features);

        if (t4C!=null) {
            try {
                t4C.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        return geoJSON;
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getStationFullV2JSON(ArrayList<Station> data) {
        JSONArray station_list = new JSONArray();

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            JSONObject station = new JSONObject();
            station.put("name", aData.getName());
            station.put("marker", aData.getMarker());
            station.put("marker_long_name", aData.getMarkerLongName());
            station.put("description", aData.getDescription());
            station.put("date_from", aData.getDate_from());
            station.put("date_to", aData.getDate_to());
            station.put("comment", aData.getComment());
            station.put("iers_domes", aData.getIers_domes());
            station.put("cdp_num", aData.getCpd_num());
            station.put("monument_num", aData.getMonument_num());
            station.put("receiver_num", aData.getReceiver_num());
            station.put("country_code", aData.getCountry_code());

            // *****************************************************************
            // Handle Station Type
            // *****************************************************************
            JSONObject station_type = new JSONObject();
            station_type.put("name", aData.getStation_type().getName());
            station_type.put("type", aData.getStation_type().getType());
            station.put("station_type", station_type);

            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getLocation().getCity().getState().getCountry().getName());
            country.put("iso_code", aData.getLocation().getCity().getState().getCountry().getIso_code());

            // *****************************************************************
            // Handle State
            // *****************************************************************
            JSONObject state = new JSONObject();
            state.put("name", aData.getLocation().getCity().getState().getName());
            state.put("country", country);

            // *****************************************************************
            // Handle City
            // *****************************************************************
            JSONObject city = new JSONObject();
            city.put("name", aData.getLocation().getCity().getName());
            city.put("state", state);

            // *****************************************************************
            // Handle Coordinates
            // *****************************************************************
            JSONObject coordinates = new JSONObject();
            coordinates.put("x", aData.getLocation().getCoordinates().getX());
            coordinates.put("y", aData.getLocation().getCoordinates().getY());
            coordinates.put("z", aData.getLocation().getCoordinates().getZ());
            coordinates.put("lat", aData.getLocation().getCoordinates().getLat());
            coordinates.put("lon", aData.getLocation().getCoordinates().getLon());
            coordinates.put("altitude", aData.getLocation().getCoordinates().getAltitude());

            // *****************************************************************
            // Handle Tectonic
            // *****************************************************************
            JSONObject tectonic = new JSONObject();
            tectonic.put("plate_name", aData.getLocation().getTectonic().getPlate_name());

            // *****************************************************************
            // Handle Location
            // *****************************************************************
            JSONObject location = new JSONObject();
            location.put("city", city);
            location.put("coordinates", coordinates);
            location.put("tectonic", tectonic);
            location.put("description", aData.getLocation().getDescription());
            station.put("location", location);

            // *****************************************************************
            // Handle Monument
            // *****************************************************************
            JSONObject monument = new JSONObject();
            monument.put("description", aData.getMonument().getDescription());
            monument.put("inscription", aData.getMonument().getInscription());
            monument.put("height", aData.getMonument().getHeight());
            monument.put("foundation", aData.getMonument().getFoundation());
            monument.put("foundation_depth", aData.getMonument().getFoundation_depth());
            station.put("monument", monument);

            // *****************************************************************
            // Handle Bedrock
            // *****************************************************************
            JSONObject bedrock = new JSONObject();
            bedrock.put("condition", aData.getGeological().getBedrock().getCondition());
            bedrock.put("type", aData.getGeological().getBedrock().getType());

            // *****************************************************************
            // Handle Geological
            // *****************************************************************
            JSONObject geological = new JSONObject();
            geological.put("characteristic", aData.getGeological().getCharacteristic());
            geological.put("fracture_spacing", aData.getGeological().getFracture_spacing());
            geological.put("fault_zone", aData.getGeological().getFault_zone());
            geological.put("distance_to_fault", aData.getGeological().getDistance_to_fault());
            geological.put("bedrock", bedrock);
            station.put("geological", geological);

            // *****************************************************************
            // Handle Station Colocation Offsets
            // *****************************************************************
            JSONArray colocation_offsets = new JSONArray();
            for (int j = 0; j < aData.getStationColocationOffsets().size(); j++) {
                // *************************************************************
                // Handle Station Colocated
                // *************************************************************
                JSONObject station_colocated = new JSONObject();
                station_colocated.put("name", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getName());
                station_colocated.put("marker", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getMarker());
                station_colocated.put("description", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDescription());
                station_colocated.put("date_from", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDate_from());
                station_colocated.put("date_to", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDate_to());
                station_colocated.put("comment", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getComment());
                station_colocated.put("iers_domes", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getIers_domes());
                station_colocated.put("cpd_num", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getCpd_num());
                station_colocated.put("monument_num", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getMonument_num());
                station_colocated.put("receiver_num", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getReceiver_num());
                station_colocated.put("country_code", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getCountry_code());

                // *************************************************************
                // Handle Colocation Offset
                // *************************************************************
                JSONObject colocation_offset = new JSONObject();
                colocation_offset.put("station_colocated", station_colocated);
                colocation_offset.put("offset_x", aData.getStationColocationOffsets().get(j).getOffset_x());
                colocation_offset.put("offset_y", aData.getStationColocationOffsets().get(j).getOffset_y());
                colocation_offset.put("offset_z", aData.getStationColocationOffsets().get(j).getOffset_z());
                colocation_offset.put("date_measured", aData.getStationColocationOffsets().get(j).getDate_measured());
                colocation_offsets.add(colocation_offset);
            }
            station.put("colocation_offsets", colocation_offsets);

            // *****************************************************************
            // Handle Conditions and Effects
            // *****************************************************************
            JSONArray conditions = new JSONArray();
            for (int j = 0; j < aData.getCondition().size(); j++) {
                // *************************************************************
                // Handle effects
                // *************************************************************
                JSONObject effects = new JSONObject();
                effects.put("type", aData.getCondition().get(j).getEffect().getType());

                // *************************************************************
                // Handle condition
                // *************************************************************
                JSONObject condition = new JSONObject();
                condition.put("date_from", aData.getCondition().get(j).getDate_from());
                condition.put("date_to", aData.getCondition().get(j).getDate_to());
                condition.put("degradation", aData.getCondition().get(j).getDegradation());
                condition.put("comments", aData.getCondition().get(j).getComments());
                condition.put("effect", effects);

                conditions.add(condition);
            }
            station.put("conditions", conditions);

            // *****************************************************************
            // Handle Local Ties
            // *****************************************************************
            JSONArray local_ties = new JSONArray();
            for (int j = 0; j < aData.getLocalTies().size(); j++) {
                JSONObject local_tie = new JSONObject();
                local_tie.put("name", aData.getLocalTies().get(j).getName());
                local_tie.put("usage", aData.getLocalTies().get(j).getUsage());
                local_tie.put("cpd_num", aData.getLocalTies().get(j).getCpd_num());
                local_tie.put("dx", aData.getLocalTies().get(j).getDx());
                local_tie.put("dy", aData.getLocalTies().get(j).getDy());
                local_tie.put("dz", aData.getLocalTies().get(j).getDz());
                local_tie.put("accuracy", aData.getLocalTies().get(j).getAccuracy());
                local_tie.put("survey_method", aData.getLocalTies().get(j).getSurvey_method());
                local_tie.put("date_at", aData.getLocalTies().get(j).getDate_at());
                local_tie.put("comment", aData.getLocalTies().get(j).getComment());
                local_ties.add(local_tie);
            }
            station.put("local_ties", local_ties);

            // *****************************************************************
            // Handle Collocation Instrument
            // *****************************************************************
            JSONArray collocation_instruments = new JSONArray();
            for (int j = 0; j < aData.getCollocationInstrument().size(); j++) {
                JSONObject collocation_instrument = new JSONObject();
                collocation_instrument.put("type", aData.getCollocationInstrument().get(j).getType());
                collocation_instrument.put("status", aData.getCollocationInstrument().get(j).getStatus());
                collocation_instrument.put("date_from", aData.getCollocationInstrument().get(j).getDate_from());
                collocation_instrument.put("date_to", aData.getCollocationInstrument().get(j).getDate_to());
                collocation_instrument.put("comment", aData.getCollocationInstrument().get(j).getComment());
                collocation_instruments.add(collocation_instrument);
            }
            station.put("collocation_instruments", collocation_instruments);

            // *****************************************************************
            // Handle Access Control
            // *****************************************************************
            JSONArray user_groups = new JSONArray();
            for (int j = 0; j < aData.getUserGroupStation().size(); j++) {
                // *************************************************************
                // Handle User Group
                // *************************************************************
                JSONObject user_group = new JSONObject();
                user_group.put("name", aData.getUserGroupStation().get(j).getUser_groups().getName());

                // *************************************************************
                // Handle User Group Station
                // *************************************************************
                JSONObject user_group_station = new JSONObject();
                user_group_station.put("user_group", user_group);
                user_groups.add(user_group_station);
            }
            station.put("user_groups_station", user_groups);

            // *****************************************************************
            // Handle Site Logs
            // *****************************************************************
            JSONArray logs = new JSONArray();
            for (int j = 0; j < aData.getLogs().size(); j++) {
                // *************************************************************
                // Handle Log Type
                // *************************************************************
                JSONObject log_type = new JSONObject();
                log_type.put("name", aData.getLogs().get(j).getLog_type().getName());

                // *************************************************************
                // Handle Agency
                // *************************************************************
                JSONObject agency =aData.getLogs().get(j).getContact().getAgency().toJsonObject();

                // *************************************************************
                // Handle Contact
                // *************************************************************
                JSONObject contact = new JSONObject();
                contact.put("name", aData.getLogs().get(j).getContact().getName());
                contact.put("title", aData.getLogs().get(j).getContact().getTitle());
                contact.put("email", aData.getLogs().get(j).getContact().getEmail());
                contact.put("phone", aData.getLogs().get(j).getContact().getPhone());
                contact.put("gsm", aData.getLogs().get(j).getContact().getGsm());
                contact.put("comment", aData.getLogs().get(j).getContact().getComment());
                contact.put("role", aData.getLogs().get(j).getContact().getRole());
                contact.put("agency", agency);

                // *************************************************************
                // Handle Log
                // *************************************************************
                JSONObject log = new JSONObject();
                log.put("title", aData.getLogs().get(j).getTitle());
                log.put("date", aData.getLogs().get(j).getDate());
                log.put("modified", aData.getLogs().get(j).getModified());
                log.put("log_type", log_type);
                log.put("contact", contact);
                logs.add(log);
            }
            station.put("logs", logs);

            // *****************************************************************
            // Handle Station Contacts
            // *****************************************************************
            JSONArray station_contacts = new JSONArray();
            for (int j = 0; j < aData.getStationContacts().size(); j++) {
                // *************************************************************
                // Handle Agency
                // *************************************************************
                JSONObject agency = aData.getStationContacts().get(j).getContact().getAgency().toJsonObject();

                // *************************************************************
                // Handle Contact
                // *************************************************************
                JSONObject contact = new JSONObject();
                contact.put("name", aData.getStationContacts().get(j).getContact().getName());
                contact.put("title", aData.getStationContacts().get(j).getContact().getTitle());
                contact.put("email", aData.getStationContacts().get(j).getContact().getEmail());
                contact.put("phone", aData.getStationContacts().get(j).getContact().getPhone());
                contact.put("gsm", aData.getStationContacts().get(j).getContact().getGsm());
                contact.put("comment", aData.getStationContacts().get(j).getContact().getComment());
                contact.put("role", aData.getStationContacts().get(j).getContact().getRole());
                contact.put("agency", agency);

                // *************************************************************
                // Handle Station Contact
                // *************************************************************
                JSONObject station_contact = new JSONObject();
                station_contact.put("role", aData.getStationContacts().get(j).getRole());
                station_contact.put("contact", contact);
                station_contacts.add(station_contact);
            }
            station.put("station_contacts", station_contacts);

            // *****************************************************************
            // Handle Station Networks
            // *****************************************************************
            JSONArray station_networks = new JSONArray();
            for (int j = 0; j < aData.getStationNetworks().size(); j++) {
                // *************************************************************
                // Handle Agency
                // *************************************************************
                JSONObject agency = aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().toJsonObject();


                // *************************************************************
                // Handle Contact
                // *************************************************************
                JSONObject contact = new JSONObject();
                contact.put("name", aData.getStationNetworks().get(j).getNetwork().getContact().getName());
                contact.put("title", aData.getStationNetworks().get(j).getNetwork().getContact().getTitle());
                contact.put("email", aData.getStationNetworks().get(j).getNetwork().getContact().getEmail());
                contact.put("phone", aData.getStationNetworks().get(j).getNetwork().getContact().getPhone());
                contact.put("gsm", aData.getStationNetworks().get(j).getNetwork().getContact().getGsm());
                contact.put("comment", aData.getStationNetworks().get(j).getNetwork().getContact().getComment());
                contact.put("role", aData.getStationNetworks().get(j).getNetwork().getContact().getRole());
                contact.put("agency", agency);

                // *************************************************************
                // Handle Network
                // *************************************************************
                JSONObject network = new JSONObject();
                network.put("name", aData.getStationNetworks().get(j).getNetwork().getName());
                network.put("contact", contact);

                // *************************************************************
                // Handle Station Network
                // *************************************************************
                JSONObject station_network = new JSONObject();
                station_network.put("network", network);
                station_networks.add(station_network);
            }
            station.put("station_networks", station_networks);

            // *****************************************************************
            // Handle Station Items
            // *****************************************************************
            JSONArray station_items = new JSONArray();
            for (int j = 0; j < aData.getStationItems().size(); j++) {

                // *************************************************************
                // Handle Item Type
                // *************************************************************
                JSONObject item_type = new JSONObject();
                item_type.put("name", aData.getStationItems().get(j).getItem().getItem_type().getName());

                // *************************************************************
                // Handle Item Attributes
                // *************************************************************
                JSONArray item_attributes = new JSONArray();
                for (int k = 0; k < aData.getStationItems().get(j).getItem().GetItemAttribues().size(); k++) {

                    // *********************************************************
                    // Handle Attribute
                    // *********************************************************
                    JSONObject attribute = new JSONObject();
                    attribute.put("name", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getAttribute().getName());

                    // *********************************************************
                    // Handle Item Attribute
                    // *********************************************************
                    JSONObject item_attribute = new JSONObject();
                    item_attribute.put("date_from", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getDate_from());
                    item_attribute.put("date_to", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getDate_to());
                    item_attribute.put("value_varchar", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_varchar());
                    item_attribute.put("value_date", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_date());
                    item_attribute.put("value_numeric", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_numeric());
                    item_attribute.put("attribute", attribute);


                    item_attributes.add(item_attribute);
                }

                // *************************************************************
                // Handle Item
                // *************************************************************
                JSONObject item = new JSONObject();
                item.put("comment", aData.getStationItems().get(j).getItem().getComment());
                item.put("item_type", item_type);
                item.put("item_attributes", item_attributes);

                // *************************************************************
                // Handle Station Item
                // *************************************************************
                JSONObject station_item = new JSONObject();
                station_item.put("date_from", aData.getStationItems().get(j).getDate_from());
                station_item.put("date_to", aData.getStationItems().get(j).getDate_to());
                station_item.put("item", item);
                station_items.add(station_item);
            }
            station.put("station_items", station_items);

            // *****************************************************************
            // Handle Data Files
            // *****************************************************************
            JSONArray files_generated = new JSONArray();
            for (int j = 0; j < aData.getFilesGenerated().size(); j++) {
                // *************************************************************
                // Handle File Type
                // *************************************************************
                JSONObject file_type = new JSONObject();
                file_type.put("format", aData.getFilesGenerated().get(j).getFile_type().getFormat());

                // *************************************************************
                // Handle File Generated
                // *************************************************************
                JSONObject file_generated = new JSONObject();
                file_generated.put("file_type", file_type);
                files_generated.add(file_generated);
            }
            station.put("files_generated", files_generated);

            // *****************************************************************
            // Handle Documents
            // *****************************************************************
            JSONArray documents = new JSONArray();
            for (int j = 0; j < aData.getDocument().size(); j++) {
                // *************************************************************
                // Handle Document Type
                // *************************************************************
                JSONObject document_type = new JSONObject();
                document_type.put("name", aData.getDocument().get(j).getDocument_type().getName());

                // *************************************************************
                // Handle Item Type
                // *************************************************************
                JSONObject item_type = new JSONObject();
                item_type.put("name", aData.getDocument().get(j).getItem().getItem_type().getName());

                // *************************************************************
                // Handle Document
                // *************************************************************
                JSONObject document = new JSONObject();
                document.put("date", aData.getDocument().get(j).getDate());
                document.put("title", aData.getDocument().get(j).getTitle());
                document.put("description", aData.getDocument().get(j).getDescription());
                document.put("link", aData.getDocument().get(j).getLink());
                //document.put("item", item);
                document.put("document_type", document_type);
                documents.add(document);
            }
            station.put("documents", documents);

            // *************************************************************
            // Handle Rinex files
            // *************************************************************
            station.put("rinex_files_dates", aData.getRinexFiles());
            station.put("nb_rinex_files", aData.getNbRinexFiles());
            station_list.add(station);
        }

        return station_list;
    }

    @Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public String getStationFullV2CSV(ArrayList<Station> data) {
        // Header
        StringBuilder header = new StringBuilder();
        header.append("station_name, station_marker, marker_long_name, station_description, station_date_from, ").append("station_date_to, station_comment, station_iers_domes, station_cpd_num, ").append("station_monument_num, station_receiver_num, ").append("station_type_name, station_type_type, ").append("city, ").append("state, ").append("country, country_iso_code, ").append("x, y, z, lat, lon, altitude, ").append("plate_name, tectonic_description, ").append("monument_description, monument_inscription, ").append("monument_height, monument_foundation, monument_foundation_depth, ").append("geological_characteristic, geological_fracture_spacing, ").append("geological_fault_zone, geological_distance_to_fault, ").append("bedrock_condition, bedrock_type, ").append("station_contact_role, contact_name, contact_title, ").append("contact_email, contact_phone, contact_gsm, ").append("contact_comment, contact_role, agency_name, ").append("agency_abbreviation, agency_address, agency_www, ").append("agency_infos\n");
        StringBuilder csv = new StringBuilder();

        for (Station aData : data) {


            // *****************************************************************
            // Handle Station
            // *****************************************************************
            csv.append(aData.getName()).append(", ").append(aData.getMarker()).append(", ").append(aData.getMarkerLongName()).append(", ").append(aData.getDescription()).append(", ").append(aData.getDate_from()).append(", ").append(aData.getDate_to()).append(", ").append(aData.getComment()).append(", ").append(aData.getIers_domes()).append(", ").append(aData.getCpd_num()).append(", ").append(aData.getMonument_num()).append(", ").append(aData.getReceiver_num()).append(", ");

            // *****************************************************************
            // Handle Station Type
            // *****************************************************************
            csv.append(aData.getStation_type().getName()).append(", ").append(aData.getStation_type().getType()).append(", ");

            // *****************************************************************
            // Handle City
            // *****************************************************************
            csv.append(aData.getLocation().getCity().getName()).append(", ");

            // *****************************************************************
            // Handle State
            // *****************************************************************
            csv.append(aData.getLocation().getCity().getState().getName()).append(", ");

            // *****************************************************************
            // Handle Country
            // *****************************************************************
            csv.append(aData.getLocation().getCity().getState().getCountry().getName()).append(", ").append(aData.getLocation().getCity().getState().getCountry().getIso_code()).append(", ");

            // *****************************************************************
            // Handle Coordinates
            // *****************************************************************
            csv.append(aData.getLocation().getCoordinates().getX()).append(", ").append(aData.getLocation().getCoordinates().getY()).append(", ").append(aData.getLocation().getCoordinates().getZ()).append(", ").append(aData.getLocation().getCoordinates().getLat()).append(", ").append(aData.getLocation().getCoordinates().getLon()).append(", ").append(aData.getLocation().getCoordinates().getAltitude()).append(", ");


            // *****************************************************************
            // Handle Tectonic
            // *****************************************************************
            csv.append(aData.getLocation().getTectonic().getPlate_name()).append(", ").append(aData.getLocation().getDescription()).append(", ");

            // *****************************************************************
            // Handle Monument
            // *****************************************************************
            csv.append(aData.getMonument().getDescription()).append(", ").append(aData.getMonument().getInscription()).append(", ").append(aData.getMonument().getHeight()).append(", ").append(aData.getMonument().getFoundation()).append(", ").append(aData.getMonument().getFoundation_depth()).append(", ");

            // *****************************************************************
            // Handle Geological
            // *****************************************************************
            csv.append(aData.getGeological().getCharacteristic()).append(", ").append(aData.getGeological().getFracture_spacing()).append(", ").append(aData.getGeological().getFault_zone()).append(", ").append(aData.getGeological().getDistance_to_fault()).append(", ");

            // *****************************************************************
            // Handle Bedrock
            // *****************************************************************
            csv.append(aData.getGeological().getBedrock().getCondition()).append(", ").append(aData.getGeological().getBedrock().getType()).append(", ");

            // *****************************************************************
            // Handle Station Contacts
            // *****************************************************************
            if (aData.getStationContacts().size() >= 1) {
                // *************************************************************
                // Handle Station Contact
                // *************************************************************
                csv.append(aData.getStationContacts().get(0).getRole()).append(", ");

                // *************************************************************
                // Handle Contact
                // *************************************************************
                csv.append(aData.getStationContacts().get(0).getContact().getName()).append(", ").append(aData.getStationContacts().get(0).getContact().getTitle()).append(", ").append(aData.getStationContacts().get(0).getContact().getEmail()).append(", ").append(aData.getStationContacts().get(0).getContact().getPhone()).append(", ").append(aData.getStationContacts().get(0).getContact().getGsm()).append(", ").append(aData.getStationContacts().get(0).getContact().getComment()).append(", ").append(aData.getStationContacts().get(0).getContact().getRole()).append(", ");

                // *************************************************************
                // Handle Agency
                // *************************************************************
                csv.append(aData.getStationContacts().get(0).getContact().getAgency().getName()).append(", ").append(aData.getStationContacts().get(0).getContact().getAgency().getAbbreviation()).append(", ").append(aData.getStationContacts().get(0).getContact().getAgency().getAddress()).append(", ").append(aData.getStationContacts().get(0).getContact().getAgency().getWww()).append(", ").append(aData.getStationContacts().get(0).getContact().getAgency().getInfos()).append("\n");
            }
        }

        // Join header
        header.append(csv);
        return header.toString();
    }

    @Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationFullV2XML(ArrayList<Station> data) {

        StringBuilder xml = new StringBuilder();
        try {
            xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>").append("<stations>");
            for (Station aData : data) {
                // *****************************************************************
                // Handle Station
                // *****************************************************************
                xml.append("<station>");
                xml.append("<name>");
                xml.append(aData.getName());
                xml.append("</name>");
                xml.append("<marker>");
                xml.append(aData.getMarker());
                xml.append("</marker>");
                xml.append("<markerlongname>");
                xml.append(aData.getMarkerLongName());
                xml.append("</markerlongname>");
                xml.append("<description>");
                xml.append(aData.getDescription());
                xml.append("</description>");
                xml.append("<date_from>");
                xml.append(aData.getDate_from());
                xml.append("</date_from>");
                xml.append("<date_to>");
                xml.append(aData.getDate_to());
                xml.append("</date_to>");
                xml.append("<comment>");
                xml.append(aData.getComment());
                xml.append("</comment>");
                xml.append("<iers_domes>");
                xml.append(aData.getIers_domes());
                xml.append("</iers_domes>");
                xml.append("<cpd_num>");
                xml.append(aData.getCpd_num());
                xml.append("</cpd_num>");
                xml.append("<monument_num>");
                xml.append(aData.getMonument_num());
                xml.append("</monument_num>");
                xml.append("<receiver_num>");
                xml.append(aData.getReceiver_num());
                xml.append("</receiver_num>");
                xml.append("<country_code>");
                xml.append(aData.getCountry_code());
                xml.append("</country_code>");
                xml.append("<station_type>");
                xml.append("<name>");
                xml.append(aData.getStation_type().getName());
                xml.append("</name>");
                xml.append("<type>");
                xml.append(aData.getStation_type().getType());
                xml.append("</type>");
                xml.append("</station_type>");
                xml.append("<location>");
                xml.append("<city>");
                xml.append("<name>");
                xml.append(aData.getLocation().getCity().getName());
                xml.append("</name>");
                xml.append("<state>");
                xml.append("<name>");
                xml.append(aData.getLocation().getCity().getState().getName());
                xml.append("</name>");
                xml.append("<country>");
                xml.append("<name>");
                xml.append(aData.getLocation().getCity().getState().getCountry().getName());
                xml.append("</name>");
                xml.append("<iso_code>");
                xml.append(aData.getLocation().getCity().getState().getCountry().getIso_code());
                xml.append("</iso_code>");
                xml.append("</country>");
                xml.append("</state>");
                xml.append("</city>");
                xml.append("<coordinates>");
                xml.append("<x>");
                xml.append(aData.getLocation().getCoordinates().getX());
                xml.append("</x>");
                xml.append("<y>");
                xml.append(aData.getLocation().getCoordinates().getY());
                xml.append("</y>");
                xml.append("<z>");
                xml.append(aData.getLocation().getCoordinates().getZ());
                xml.append("</z>");
                xml.append("<lat>");
                xml.append(aData.getLocation().getCoordinates().getLat());
                xml.append("</lat>");
                xml.append("<lon>");
                xml.append(aData.getLocation().getCoordinates().getLon());
                xml.append("</lon>");
                xml.append("<altitude>");
                xml.append(aData.getLocation().getCoordinates().getAltitude());
                xml.append("</altitude>");
                xml.append("</coordinates>");
                xml.append("<tectonic>");
                xml.append("<plate_name>");
                xml.append(aData.getLocation().getTectonic().getPlate_name());
                xml.append("</plate_name>");
                xml.append("</tectonic>");
                xml.append("<description>");
                xml.append(aData.getLocation().getDescription());
                xml.append("</description>");
                xml.append("</location>");
                xml.append("<monument>");
                xml.append("<description>");
                xml.append(aData.getMonument().getDescription());
                xml.append("</description>");
                xml.append("<inscription>");
                xml.append(aData.getMonument().getInscription());
                xml.append("</inscription>");
                xml.append("<height>");
                xml.append(aData.getMonument().getHeight());
                xml.append("</height>");
                xml.append("<foundation>");
                xml.append(aData.getMonument().getFoundation());
                xml.append("</foundation>");
                xml.append("<foundation_depth>");
                xml.append(aData.getMonument().getFoundation_depth());
                xml.append("</foundation_depth>");
                xml.append("</monument>");
                xml.append("<geological>");
                xml.append("<characteristic>");
                xml.append(aData.getGeological().getCharacteristic());
                xml.append("</characteristic>");
                xml.append("<fracture_spacing>");
                xml.append(aData.getGeological().getFracture_spacing());
                xml.append("</fracture_spacing>");
                xml.append("<fault_zone>");
                xml.append(aData.getGeological().getFault_zone());
                xml.append("</fault_zone>");
                xml.append("<distance_to_fault>");
                xml.append(aData.getGeological().getDistance_to_fault());
                xml.append("</distance_to_fault>");
                xml.append("<bedrock>");
                xml.append("<condition>");
                xml.append(aData.getGeological().getBedrock().getCondition());
                xml.append("</condition>");
                xml.append("<type>");
                xml.append(aData.getGeological().getBedrock().getType());
                xml.append("</type>");
                xml.append("</bedrock>");
                xml.append("</geological>");
                xml.append("<colocation_offsets>");

                for (int j = 0; j < aData.getStationColocationOffsets().size(); j++) {
                    // *************************************************************
                    // Handle Colocation Offset
                    // *************************************************************
                    xml.append("<colocation_offset>").append("<offset_x>").append(aData.getStationColocationOffsets().get(j).getOffset_x()).append("</offset_x>").append("<offset_y>").append(aData.getStationColocationOffsets().get(j).getOffset_y()).append("</offset_y>").append("<offset_z>").append(aData.getStationColocationOffsets().get(j).getOffset_z()).append("</offset_z>").append("<date_measured>").append(aData.getStationColocationOffsets().get(j).getDate_measured()).append("</date_measured>").append("<station_colocated>").append("<name>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getName()).append("</name>").append("<marker>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getMarker()).append("</marker>").append("<description>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDescription()).append("</description>").append("<date_from>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDate_from()).append("</date_from>").append("<date_to>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDate_to()).append("</date_to>").append("<comment>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getComment()).append("</comment>").append("<iers_domes>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getIers_domes()).append("</iers_domes>").append("<cpd_num>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getCpd_num()).append("</cpd_num>").append("<monument_num>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getMonument_num()).append("</monument_num>").append("<receiver_num>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getReceiver_num()).append("</receiver_num>").append("<country_code>").append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getCountry_code()).append("</country_code>").append("</station_colocated>").append("</colocation_offset>");
                }
                xml.append("</colocation_offsets>").append("<conditions>");

                for (int j = 0; j < aData.getCondition().size(); j++) {
                    // *************************************************************
                    // Handle condition
                    // *************************************************************
                    xml.append("<condition>").append("<date_from>").append(aData.getCondition().get(j).getDate_from()).append("</date_from>").append("<date_to>").append(aData.getCondition().get(j).getDate_to()).append("</date_to>").append("<degradation>").append(aData.getCondition().get(j).getDegradation()).append("</degradation>").append("<comments>").append(aData.getCondition().get(j).getComments()).append("</comments>").append("<effects>").append("<type>").append(aData.getCondition().get(j).getEffect().getType()).append("</type>").append("</effects>").append("</condition>");
                }
                xml.append("</conditions>").append("<local_ties>");
                for (int j = 0; j < aData.getLocalTies().size(); j++) {
                    xml.append("<local_tie>").append("<name>").append(aData.getLocalTies().get(j).getName()).append("</name>").append("<usage>").append(aData.getLocalTies().get(j).getUsage()).append("</usage>").append("<cpd_num>").append(aData.getLocalTies().get(j).getCpd_num()).append("</cpd_num>").append("<dx>").append(aData.getLocalTies().get(j).getDx()).append("</dx>").append("<dy>").append(aData.getLocalTies().get(j).getDy()).append("</dy>").append("<dz>").append(aData.getLocalTies().get(j).getDz()).append("</dz>").append("<accuracy>").append(aData.getLocalTies().get(j).getAccuracy()).append("</accuracy>").append("<survey_method>").append(aData.getLocalTies().get(j).getSurvey_method()).append("</survey_method>").append("<date_at>").append(aData.getLocalTies().get(j).getDate_at()).append("</date_at>").append("<comment>").append(aData.getLocalTies().get(j).getComment()).append("</comment>").append("</local_tie>");
                }

                xml.append("</local_ties>").append("<collocation_instruments>");

                for (int j = 0; j < aData.getCollocationInstrument().size(); j++) {
                    xml.append("<collocation_instrument>").append("<type>").append(aData.getCollocationInstrument().get(j).getType()).append("</type>").append("<status>").append(aData.getCollocationInstrument().get(j).getStatus()).append("</status>").append("<date_from>").append(aData.getCollocationInstrument().get(j).getDate_from()).append("</date_from>").append("<date_to>").append(aData.getCollocationInstrument().get(j).getDate_to()).append("</date_to>").append("<comment>").append(aData.getCollocationInstrument().get(j).getComment()).append("</comment>").append("</collocation_instrument>");
                }
                xml.append("</collocation_instruments>").append("<user_groups>");
                for (int j = 0; j < aData.getUserGroupStation().size(); j++) {
                    // *************************************************************
                    // Handle User Group Station
                    // *************************************************************
                    xml.append("<user_group_station>").append("<user_group>").append("<name>").append(aData.getUserGroupStation().get(j).getUser_groups().getName()).append("</name>").append("</user_group>").append("</user_group_station>");
                }
                xml.append("</user_groups>").append("<logs>");
                for (int j = 0; j < aData.getLogs().size(); j++) {
                    // *************************************************************
                    // Handle Log
                    // *************************************************************
                    xml.append("<log>").append("<title>").append(aData.getLogs().get(j).getTitle()).append("</title>").append("<date>").append(aData.getLogs().get(j).getDate()).append("</date>").append("<modified>").append(aData.getLogs().get(j).getModified()).append("</modified>").append("<log_type>").append("<name>").append(aData.getLogs().get(j).getLog_type().getName()).append("</name>").append("</log_type>").append("<contact>").append("<name>").append(aData.getLogs().get(j).getContact().getName()).append("</name>").append("<title>").append(aData.getLogs().get(j).getContact().getTitle()).append("</title>").append("<email>").append(aData.getLogs().get(j).getContact().getEmail()).append("</email>").append("<phone>").append(aData.getLogs().get(j).getContact().getPhone()).append("</phone>").append("<gsm>").append(aData.getLogs().get(j).getContact().getGsm()).append("</gsm>").append("<comment>").append(aData.getLogs().get(j).getContact().getComment()).append("</comment>").append("<role>").append(aData.getLogs().get(j).getContact().getRole()).append("</role>").append("<agency>").append("<name>").append(aData.getLogs().get(j).getContact().getAgency().getName()).append("</name>").append("<abbreviation>").append(aData.getLogs().get(j).getContact().getAgency().getAbbreviation()).append("</abbreviation>").append("<address>").append(aData.getLogs().get(j).getContact().getAgency().getAddress()).append("</address>").append("<www>").append(aData.getLogs().get(j).getContact().getAgency().getWww()).append("</www>").append("<infos>").append(aData.getLogs().get(j).getContact().getAgency().getInfos()).append("</infos>").append("</agency>").append("</contact>").append("</log>");
                }
                xml.append("</logs>").append("<station_contacts>");
                for (int j = 0; j < aData.getStationContacts().size(); j++) {
                    // *************************************************************
                    // Handle Station Contact
                    // *************************************************************
                    xml.append("<station_contact>").append("<role>").append(aData.getStationContacts().get(j).getRole()).append("</role>").append("<contact>").append("<name>").append(aData.getStationContacts().get(j).getContact().getName()).append("</name>").append("<title>").append(aData.getStationContacts().get(j).getContact().getTitle()).append("</title>").append("<email>").append(aData.getStationContacts().get(j).getContact().getEmail()).append("</email>").append("<phone>").append(aData.getStationContacts().get(j).getContact().getPhone()).append("</phone>").append("<gsm>").append(aData.getStationContacts().get(j).getContact().getGsm()).append("</gsm>").append("<comment>").append(aData.getStationContacts().get(j).getContact().getComment()).append("</comment>").append("<role>").append(aData.getStationContacts().get(j).getContact().getRole()).append("</role>").append("<agency>").append("<name>").append(aData.getStationContacts().get(j).getContact().getAgency().getName()).append("</name>").append("<abbreviation>").append(aData.getStationContacts().get(j).getContact().getAgency().getAbbreviation()).append("</abbreviation>").append("<address>").append(aData.getStationContacts().get(j).getContact().getAgency().getAddress()).append("</address>").append("<www>").append(aData.getStationContacts().get(j).getContact().getAgency().getWww()).append("</www>").append("<infos>").append(aData.getStationContacts().get(j).getContact().getAgency().getInfos()).append("</infos>").append("</agency>").append("</contact>").append("</station_contact>");
                }
                xml.append("</station_contacts>").append("<station_networks>");
                for (int j = 0; j < aData.getStationNetworks().size(); j++) {
                    // *************************************************************
                    // Handle Station Network
                    // *************************************************************
                    xml.append("<station_network>").append("<network>").append("<name>").append(aData.getStationNetworks().get(j).getNetwork().getName()).append("</name>").append("<contact>").append("<name>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getName()).append("</name>").append("<title>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getTitle()).append("</title>").append("<email>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getEmail()).append("</email>").append("<phone>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getPhone()).append("</phone>").append("<gsm>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getGsm()).append("</gsm>").append("<comment>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getComment()).append("</comment>").append("<role>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getRole()).append("</role>").append("<agency>").append("<name>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getName()).append("</name>").append("<abbreviation>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getAbbreviation()).append("</abbreviation>").append("<address>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getAddress()).append("</address>").append("<www>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getWww()).append("</www>").append("<infos>").append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getInfos()).append("</infos>").append("</agency>").append("</contact>").append("</network>").append("</station_network>");
                }
                xml.append("</station_networks>").append("<station_items>");
                for (int j = 0; j < aData.getStationItems().size(); j++) {
                    // *************************************************************
                    // Handle Station Item
                    // *************************************************************
                    xml.append("<station_item>").append("<date_from>").append(aData.getStationItems().get(j).getDate_from()).append("</date_from>").append("<date_to>").append(aData.getStationItems().get(j).getDate_to()).append("</date_to>").append("<item>").append("<comment>").append(aData.getStationItems().get(j).getItem().getComment()).append("</comment>").append("<item_type>").append("<name>").append(aData.getStationItems().get(j).getItem().getItem_type().getName()).append("</name>").append("</item_type>").append("<contact_as_producer>").append("<name>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getName()).append("</name>").append("<title>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getTitle()).append("</title>").append("<email>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getEmail()).append("</email>").append("<phone>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getPhone()).append("</phone>").append("<gsm>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getGsm()).append("</gsm>").append("<comment>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getComment()).append("</comment>").append("<role>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getRole()).append("</role>").append("<agency_as_producer>").append("<name>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getName()).append("</name>").append("<abbreviation>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAbbreviation()).append("</abbreviation>").append("<address>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAddress()).append("</address>").append("<www>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getWww()).append("</www>").append("<infos>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getInfos()).append("</infos>").append("</agency_as_producer>").append("</contact_as_producer>").append("<contact_as_owner>").append("<name>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getName()).append("</name>").append("<title>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getTitle()).append("</title>").append("<email>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getEmail()).append("</email>").append("<phone>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getPhone()).append("</phone>").append("<gsm>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getGsm()).append("</gsm>").append("<comment>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getComment()).append("</comment>").append("<role>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getRole()).append("</role>").append("<agency_as_owner>").append("<name>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getName()).append("</name>").append("<abbreviation>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAbbreviation()).append("</abbreviation>").append("<address>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAddress()).append("</address>").append("<www>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getWww()).append("</www>").append("<infos>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getInfos()).append("</infos>").append("</agency_as_owner>").append("</contact_as_owner>").append("<item_attributes>");
                    for (int k = 0; k < aData.getStationItems().get(j).getItem().GetItemAttribues().size(); k++) {
                        // *********************************************************
                        // Handle Item Attribute
                        // *********************************************************
                        xml.append("<item_attribute>").append("<date_from>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getDate_from()).append("</date_from>").append("<date_to>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getDate_to()).append("</date_to>").append("<value_varchar>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_varchar()).append("</value_varchar>").append("<value_date>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_date()).append("</value_date>").append("<value_numeric>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_numeric()).append("</value_numeric>").append("<attribute>").append("<name>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getAttribute().getName()).append("</name>").append("</attribute>").append("<filter_receivers>");
                        for (int p = 0; p < aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().size(); p++) {
                            // *****************************************************
                            // Handle Filter Receiver
                            // *****************************************************
                            xml.append("<filter_receiver>").append("<filter_attribute>").append("<name>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getAttribute().getName()).append("</name>").append("</filter_attribute>").append("<receiver_type>").append("<name>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getName()).append("</name>").append("<igs_defined>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getIgs_defined()).append("</igs_defined>").append("<model>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getModel()).append("</model>").append("</receiver_type>").append("</filter_receiver>");
                        }
                        xml.append("</filter_receivers>").append("<filter_antennas>");
                        for (int p = 0; p < aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().size(); p++) {
                            // *****************************************************
                            // Handle Filter Antenna
                            // *****************************************************
                            xml.append("<filter_antenna>").append("<filter_attribute>").append("<name>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAttribute().getName()).append("</name>").append("</filter_attribute>").append("<antenna_type>").append("<name>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getName()).append("</name>").append("<igs_defined>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getIgs_defined()).append("</igs_defined>").append("<model>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getModel()).append("</model>").append("</antenna_type>").append("</filter_antenna>");
                        }
                        xml.append("</filter_antennas>").append("<filter_radomes>");
                        for (int p = 0; p < aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().size(); p++) {
                            // *****************************************************
                            // Handle Filter Radome
                            // *****************************************************
                            xml.append("<filter_radome>").append("<filter_attribute>").append("<name>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getAttribute().getName()).append("</name>").append("</filter_attribute>").append("<radome_type>").append("<name>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getName()).append("</name>").append("<igs_defined>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getIgs_defined()).append("</igs_defined>").append("<description>").append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getDescription()).append("</description>").append("</radome_type>").append("</filter_radome>");
                        }
                        xml.append("</filter_radomes>").append("</item_attribute>");
                    }
                    xml.append("</item_attributes>").append("</item>").append("</station_item>");
                }
                xml.append("</station_items>").append("<files_generated>");
                for (int j = 0; j < aData.getFilesGenerated().size(); j++) {
                    // *************************************************************
                    // Handle File Generated
                    // *************************************************************
                    xml.append("<file_generated>").append("<file_type>").append("<format>").append(aData.getFilesGenerated().get(j).getFile_type().getFormat()).append("</format>").append("</file_type>").append("</file_generated>");
                }
                xml.append("</files_generated>").append("<documents>");
                for (int j = 0; j < aData.getDocument().size(); j++) {
                    // *************************************************************
                    // Handle Document
                    // *************************************************************
                    xml.append("<document>").append("<date>").append(aData.getDocument().get(j).getDate()).append("</date>").append("<title>").append(aData.getDocument().get(j).getTitle()).append("</title>").append("<description>").append(aData.getDocument().get(j).getDescription()).append("</description>").append("<link>").append(aData.getDocument().get(j).getLink()).append("</link>").append("<item>").append("<comment>").append(aData.getStationItems().get(j).getItem().getComment()).append("</comment>").append("<item_type>").append("<name>").append(aData.getDocument().get(j).getItem().getItem_type().getName()).append("</name>").append("</item_type>").append("<contact_as_producer>").append("<name>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getName()).append("</name>").append("<title>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getTitle()).append("</title>").append("<email>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getEmail()).append("</email>").append("<phone>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getPhone()).append("</phone>").append("<gsm>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getGsm()).append("</gsm>").append("<comment>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getComment()).append("</comment>").append("<role>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getRole()).append("</role>").append("<agency_as_producer>").append("<name>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getName()).append("</name>").append("<abbreviation>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAbbreviation()).append("</abbreviation>").append("<address>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAddress()).append("</address>").append("<www>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getWww()).append("</www>").append("<infos>").append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getInfos()).append("</infos>").append("</agency_as_producer>").append("</contact_as_producer>").append("<contact_as_owner>").append("<name>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getName()).append("</name>").append("<title>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getTitle()).append("</title>").append("<email>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getEmail()).append("</email>").append("<phone>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getPhone()).append("</phone>").append("<gsm>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getGsm()).append("</gsm>").append("<comment>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getComment()).append("</comment>").append("<role>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getRole()).append("</role>").append("<agency_as_owner>").append("<name>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getName()).append("</name>").append("<abbreviation>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAbbreviation()).append("</abbreviation>").append("<address>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAddress()).append("</address>").append("<www>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getWww()).append("</www>").append("<infos>").append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getInfos()).append("</infos>").append("</agency_as_owner>").append("</contact_as_owner>").append("<document_type>").append("<name>").append(aData.getDocument().get(j).getDocument_type().getName()).append("</name>").append("</document_type>").append("</item>").append("</document>");
                }
                xml.append("</documents>").append("</station>");
            }
            xml.append("</stations>");
        }

        catch(IndexOutOfBoundsException e){
            System.out.println("Error " + e .getMessage());
        }

        return xml.toString().replace('&',' '); // & gives erros in xml
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getStationListV2JSON(ArrayList<Station> data) {
        JSONArray station_list = new JSONArray();

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            JSONObject station = new JSONObject();
            station.put("marker", aData.getMarker());
            station.put("name", aData.getName());
            station.put("marker_long_name", aData.getMarkerLongName());
            if(aData.getDate_to() != null){
                station.put("date_to",aData.getDate_to());
            }else{
                station.put("date_to","");
            }
            if(aData.getUserGroupStationName() != null)
                station.put("epos", aData.getUserGroupStationName());
            else
                station.put("epos", "");

            // Add to result list
            station_list.add(station);
        }

        return station_list;
    }

    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getStationListV2TXT(ArrayList<Station> data) {
        StringBuilder stationList = new StringBuilder();
        stationList.append("Marker").append("\n");
        for (Station aData : data) {
            // Handle Station
            stationList.append(aData.getMarker()).append("\n");
        }

        return stationList.toString();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getAgencyListV2JSON(ArrayList<Agency> data) {
        JSONArray agency_list = new JSONArray();

        for (Agency aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            JSONObject agency = aData.toJsonObject();

            // Add to result list
            agency_list.add(agency);
        }

        return agency_list;
    }
    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getAgencyListV2TXT(ArrayList<Agency> data) {
        Set<String> uniqueNames = new HashSet<>();

        StringBuilder stationList = new StringBuilder();
        stationList.append("Agency").append("\n");
        for (Agency aData : data) {
            // Handle Station
            String name = aData.getName();

                stationList.append(name).append("\n");

        }

        return stationList.toString();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getNetworkListV2JSON(ArrayList<Network> data) {
        JSONArray network_list = new JSONArray();

        for (Network aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            JSONObject network = new JSONObject();
            network.put("name", aData.getName());

            // Add to result list
            network_list.add(network);
        }

        return network_list;
    }
    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getNetworkListV2TXT(ArrayList<Network> data){
        StringBuilder stationList = new StringBuilder();
        stationList.append("Network").append("\n");
        for (Network aData : data) {
            // Handle Station
            stationList.append(aData.getName()).append("\n");
        }

        return stationList.toString();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getCountryListV2JSON(ArrayList<Country> data) {
        JSONArray country_list = new JSONArray();

        for (Country aData : data) {
            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getName());
            country.put("iso_code", aData.getIso_code());

            // Add to result list
            country_list.add(country);
        }

        return country_list;
    }
    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getCountryListV2TXT(ArrayList<Country> data){
        StringBuilder stationList = new StringBuilder();
        stationList.append("Country").append("\n");
        for (Country aData : data) {
            // Handle Station
            stationList.append(aData.getName()).append("\n");
        }

        return stationList.toString();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getStateListV2JSON(ArrayList<State> data) {
        JSONArray state_list = new JSONArray();

        for (State aData : data) {

            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getCountry().getName());
            country.put("iso_code", aData.getCountry().getIso_code());

            // *****************************************************************
            // Handle State
            // *****************************************************************
            JSONObject state = new JSONObject();
            state.put("name", aData.getName());
            state.put("country", country);

            // Add to result list
            state_list.add(state);
        }

        return state_list;
    }
    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getStateListV2TXT(ArrayList<State> data){
        StringBuilder stationList = new StringBuilder();
        stationList.append("State").append("\n");
        for (State aData : data) {
            // Handle Station
            stationList.append(aData.getName()).append("\n");
        }

        return stationList.toString();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getCityListV2JSON(ArrayList<City> data) {
        JSONArray city_list = new JSONArray();

        for (City aData : data) {

            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getState().getCountry().getName());
            country.put("iso_code", aData.getState().getCountry().getIso_code());

            // *****************************************************************
            // Handle State
            // *****************************************************************
            JSONObject state = new JSONObject();
            state.put("name", aData.getName());
            state.put("country", country);

            // *****************************************************************
            // Handle City
            // *****************************************************************
            JSONObject city = new JSONObject();
            city.put("name", aData.getName());
            city.put("state", state);

            // Add to result list
            city_list.add(city);
        }

        return city_list;
    }
    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getCityListV2TXT(ArrayList<City> data){
        StringBuilder stationList = new StringBuilder();
        stationList.append("City").append("\n");
        for (City aData : data) {
            // Handle Station
            stationList.append(aData.getName()).append("\n");
        }

        return stationList.toString();
    }

    /**
     * Last modified: 05/12/2018 by José Manteigueiro
     * Modified for version gnss-europe-v1.1.0 of DB
     *  Only parses the name of the receiver now
     */
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getReceiverTypeListV2JSON(ArrayList<Receiver_type> data) {
        JSONArray receiver_list = new JSONArray();

        for (Receiver_type aData : data) {
            // *****************************************************************
            // Handle Receiver
            // *****************************************************************
            JSONObject receiver = new JSONObject();
            receiver.put("name", aData.getName());
            //receiver.put("igs_defined", aData.getIgs_defined());
            //receiver.put("model", aData.getModel());

            // Add to result list
            receiver_list.add(receiver);
        }

        return receiver_list;
    }
    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getReceiverTypeListV2TXT(ArrayList<Receiver_type> data){
        StringBuilder stationList = new StringBuilder();
        stationList.append("Receiver Type").append("\n");
        for (Receiver_type aData : data) {
            // Handle Station
            stationList.append(aData.getName()).append("\n");
        }

        return stationList.toString();
    }

    /**
     * Last modified: 05/12/2018 by José Manteigueiro
     * Modified for version gnss-europe-v1.1.0 of DB
     *  Only parses the name of the receiver now
     */
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getAntennaTypeListV2JSON(ArrayList<Antenna_type> data) {
        JSONArray antenna_list = new JSONArray();

        for (Antenna_type aData : data) {
            // *****************************************************************
            // Handle Antenna
            // *****************************************************************
            JSONObject antenna = new JSONObject();
            antenna.put("name", aData.getName());
            //antenna.put("igs_defined", aData.getIgs_defined());
            //antenna.put("model", aData.getModel());

            // Add to result list
            antenna_list.add(antenna);
        }

        return antenna_list;
    }

    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getAntennaTypeListV2TXT(ArrayList<Antenna_type> data) {
        StringBuilder stationList = new StringBuilder();
        stationList.append("Antenna Type").append("\n");
        for (Antenna_type aData : data) {
            // Handle Station
            stationList.append(aData.getName()).append("\n");
        }

        return stationList.toString();
    }

    /**
     * Last modified: 05/12/2018 by José Manteigueiro
     * Modified for version gnss-europe-v1.1.0 of DB
     *  Only parses the name of the receiver now
     */
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getRadomeTypeListV2JSON(ArrayList<Radome_type> data) {
        JSONArray radome_list = new JSONArray();

        for (Radome_type aData : data) {
            // *****************************************************************
            // Handle Radome
            // *****************************************************************
            JSONObject radome = new JSONObject();
            radome.put("name", aData.getName());
            //radome.put("igs_defined", aData.getIgs_defined());
            //radome.put("description", aData.getDescription());

            // Add to result list
            radome_list.add(radome);
        }

        return radome_list;
    }
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getRadomeTypeListV2TXT(ArrayList<Radome_type> data){
        StringBuilder stationList = new StringBuilder();
        stationList.append("Radome Type").append("\n");
        for (Radome_type aData : data) {
            // Handle Station
            stationList.append(aData.getName()).append("\n");
        }

        return stationList.toString();
    }

    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public JSONArray getFileTypeListV2JSON(ArrayList<File_type> data) {
        JSONArray file_type_list = new JSONArray();

        for (File_type aData : data) {
            // *****************************************************************
            // Handle File Type
            // *****************************************************************

            JSONObject file_type = new JSONObject();
            file_type.put("format", aData.getFormat());
            file_type.put("sampling_window", aData.getSampling_window());
            file_type.put("sampling_frequency", aData.getSampling_frequency());

            // Add to result list
            file_type_list.add(file_type);
        }

        return file_type_list;
    }

    @Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public String getFileTypeListV2TXT(ArrayList<File_type> data) {
        StringBuilder stationList = new StringBuilder();
        stationList.append("File Type").append("\n");
        for (File_type aData : data) {
            // Handle Station
            stationList.append(aData.getFormat()).append("\n");
        }

        return stationList.toString();
    }
    public static ArrayList<Coordinates> parsePolygonV2(String polygonString) {
        // Arraylist of polygon coordinates
        ArrayList<Coordinates> polygon = new ArrayList<>();
        // *****************************************************************
        // STEP 1
        // Divide the request string by the & character
        // *****************************************************************
        String[] coordinatePoints = polygonString.split(";");
        // *****************************************************************
        // STEP 2
        // Handle each coordinate point
        // *****************************************************************
        for (String coordinatePoint : coordinatePoints) {
            // *************************************************************
            // STEP 3
            // Separate coordinate values
            // *************************************************************
            String[] individualValues = coordinatePoint.split(",");
            Coordinates coord = new Coordinates();
            coord.setLat(Float.parseFloat(individualValues[0]));
            coord.setLon(Float.parseFloat(individualValues[1]));
            polygon.add(coord);
        }
        return polygon;
    }
    
    @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getFilesInfoJSON(ArrayList<FilesInfo> data) {
        Gson gson = new GsonBuilder().setPrettyPrinting().create();
        return gson.toJson(data);
    }


}
