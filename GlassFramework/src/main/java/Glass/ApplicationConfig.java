/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Glass;

import javax.ws.rs.core.Application;
import java.util.Set;

/**
 *
 * @author pedro
 */
@javax.ws.rs.ApplicationPath("webresources")
public class ApplicationConfig extends Application {

    @Override
        public Set<Class<?>> getClasses() {
            Set<Class<?>> resources = new java.util.HashSet<>();
            addRestResourceClasses(resources);
            return resources;
        }

    /**
     * Do not modify addRestResourceClasses() method.
     * It is automatically populated with
     * all resources defined in the project.
     * If required, comment out calling this method in getClasses().
     */
    private void addRestResourceClasses(Set<Class<?>> resources) {
        resources.add(Glass.FilesV2.class);
        resources.add(Glass.LogFiles.class);
        resources.add(Glass.Products.class);
        resources.add(Glass.Stations.class);
        resources.add(Glass.StationsV2.class);
        resources.add(Glass.T0Manager.class);
        resources.add(Glass.Tools.class); 
    }
}
