package Glass;

import Configuration.SiteConfig;
import CustomClasses.StationQuery;
import CustomClasses.Validator;
import CustomClasses.ValidatorException;
import CustomClasses.markerinfo;
import EposTables.*;
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;

import static javax.ws.rs.core.MediaType.*;

@SuppressWarnings("Duplicates")
@Path("stations/v2")
public class StationsV2 {

    @EJB
    private final
    SiteConfig siteConfig;

    // =========================================================================
    //  FIELDS
    // =========================================================================
    @Context
    private UriInfo context;

    public StationsV2() {
        siteConfig = lookupSiteConfigBean();
        //String myIP = siteConfig.getLocalIpValue();
        //String DBConnectionString = siteConfig.getDBConnectionString();
    }

    @SuppressWarnings("Duplicates")
    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    /**
     * <b>GET STATION V2</b> - Returns all the station in the expected format.
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>geojson (only for short size)</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     * @param format output file format
     * @param size short or full data (full is slower)
     * @return All the stations in the expected format.
     * @author José Manteigueiro, Paul Crocker
     * Version 2.0.1
     * @since 27/10/2017
     * Last modified: 30/03/2022
     */
    @Path("station/{size}/{format}")
    @GET
    public Response getStations_V2(@PathParam("size") String size, @PathParam("format") String format) throws IOException {

        size = size.toLowerCase();
        format = format.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                    //SHORT CSV
                    case "csv" :
                        //Get the stations array
                        // res = dbcv2.getStationsShortV2(sq);
                        res = dbcv2.getStationsShortV3();

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON
                    case "json" :
                        //Get the stations array
                        // res = dbcv2.getStationsShortV2(sq);
                        res = dbcv2.getStationsShortV3();

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //SHORT GEOJSON
                    case "geojson":
                        //Get the stations array
                        // res = dbcv2.getStationsShortV2(sq);
                        res = dbcv2.getStationsShortV3();

                        //Parse and return it
                        String geojson = parser.getStationShortV2GEOJSON(res).toString();
                        return Response.ok(geojson, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json or geojson.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
            case "full" :
                sq.setFullV(true);
                switch(format){
                    //FULL CSV
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();
                    //FULL JSON
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String json = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //FULL XML
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
            //Bad request
            default: {
                message = "Wrong output size. Use full or shot.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    @Path("id/{marker}")
    @GET
    public String getStationIdV2(@PathParam("marker") String marker) throws IOException {
        Parsers parser = new Parsers();
        if(!(marker.length() <= 9)){
            return "The marker long name must be <=9chars";
        }
        //marker must be <=9chars - check at begining of function
        List<markerinfo> marker_and_id = new ArrayList<markerinfo>();

        // use new version of cache function
        JsonCache mycache = new JsonCache();
        marker_and_id = mycache.getMarkerandKeyByValue(marker);
        JSONObject all = new JSONObject();
        for (markerinfo aData : marker_and_id) {
            all.put("ID: ",aData.getId());
            all.put("Long Marker Name: ",aData.getLongmarker());
        }
        return all.toJSONString();

    }


    /**
     * <b>GET STATION LIST V2</b> - Returns a list with the name and marker of all the stations or the ones that match the given parameters.
     * <p>
     * Optional parameters are:
     * <ul>
     * <li>name</li>
     * <li>4 char marker</li>
     * <li>epos</li>
     * <li>9 char unique marker</li>
     * </ul>
     * <p>
     * This can be passed as:
     * <b>name=Blieux</b> or <b>marker=BLIX,CASC</b>
     * </p>
     * <p>
     *     Different parameters should be separated by <b>&</b> (AMPERSAND)
     * </p>
     * <p>
     *     Multiple values for a parameter should be separated by <b>,</b> (COMMA)
     * </p>
     *
     * @param name Name of the station(s)
     * @param marker 4 Character Marker of the station(s)
     * @param epos "epos", "non-epos" or leave it blank
     * @param markerlongname 9 Character Unique Marker of the station(s)
     * @return A list with the name and marker of the stations.
     * @author José Manteigueiro
     * Version 2.0
     * @since 30/10/2017
     * Last modified: 30/11/2017
     */
    @Path("list/station")
    @GET
    public Response getStationList_V2(@DefaultValue("") @QueryParam("name") String name,
                                      @DefaultValue("") @QueryParam("marker") String marker,
                                      @DefaultValue("") @QueryParam("epos") String epos,
                                      @DefaultValue("") @QueryParam("markerlongname") String markerlongname,
                                      @DefaultValue("") @QueryParam("format") String format
                                      ){

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        marker = marker.toUpperCase();
        epos = epos.toLowerCase();

            // In case no optional parameters are passed
        if (name.equals("") && marker.equals("") && epos.equals("") && markerlongname.equals("")) {
            //Get the stations array
            ArrayList<Station> res = dbcv2.getStationListV2(null,false);

            //Parse and return it
            String result = "";
            if(format.equalsIgnoreCase("list")){
                result = parser.getStationListV2TXT(res).toString();
                return Response.ok(result, TEXT_PLAIN).build();
            }else{
                result = parser.getStationListV2JSON(res).toString();
                return Response.ok(result, MediaType.APPLICATION_JSON).build();
            }


        }

        // In case any optional parameter is passed
        StationQuery sq = new StationQuery();

        if (!name.equals("")) {
            sq.setNames(new ArrayList<>(Arrays.asList(name.split(","))));
        }
        //The below is not working correclty
        if (!marker.equals("")) {
        //    String cleanMarker = marker.replace("_","\\_");  //markers can contain an underscore e.g BEJ_
           sq.setMarkers(new ArrayList<>(Arrays.asList(marker.split(","))));
        }

        if (epos.equals("epos")){
            sq.setShowNonEpos(false);
        }else if (epos.equals("non-epos")) {
            sq.setShowEpos(false);
        }

        //Not sure if 9 char markers can contain _ if so will need to do as above
        if (!markerlongname.equals("")) {
            sq.setMarkerlongnames(new ArrayList<>(Arrays.asList(markerlongname.split(","))));
        }

        ArrayList<Station> res = dbcv2.getStationListV2(sq,false);

        //Parse and return it
        String result = "";
        if(format.equalsIgnoreCase("list")){
            result = parser.getStationListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else{
            result = parser.getStationListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * <b>GET LIST V2</b> - Returns a list containing existing data of the chosen type of data.
     * <p>
     * Choose the type of data you want to obtain from the list below:
     * </p>
     * <ul>
     *     <li>agency</li>
     *     <li>antenna</li>
     *     <li>city</li>
     *     <li>country</li>
     *     <li>files_type</li>
     *     <li>network</li>
     *     <li>radome</li>
     *     <li>receiver</li>
     *     <li>state</li>
     *     <li>station or station_marker</li>
     * </ul>
     *
     * @param rawData Type of data to be listed.
     * @return A list with the names of the chosen data. json or simple text çist
     * @author José Manteigueiro
     * Version 2.0
     * @since 10/11/2017
     * Last modified: 5/7/2022
     */
    @Path("list/{data}")
    @GET
    public Response getList_V2(@PathParam("data") String rawData,@DefaultValue("json") @QueryParam("format") String format,@DefaultValue("") @QueryParam("station_data") String station_data) {

        String data = rawData.toLowerCase();
        String message;

        switch(data){
            case "agency" : return getAgencyList_V2(format);
            case "country" : return getCountryList_V2(format);
            case "city" : return getCityList_V2(format);
            case "state" : return getStateList_V2(format);
            case "network" : return getNetworkList_V2(format);
            case "receiver" : return getReceiverList_V2(format);
            case "radome" : return getRadomeList_V2(format);
            case "antenna" : return getAntennaList_V2(format);
            case "files_type" : return getFileTypeList_V2(format); //This is not T1
            case "station" :
            case "station-marker" :
                if(station_data.equalsIgnoreCase("true")){
                    return getStation_Data_V2(format);
                }
                return getStationList_V2("","", "", "",format);
            //Bad Request
            default: {
                message = "Wrong data type.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }


    private Response getStation_Data_V2(String format) {
        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<Station> res = dbcv2.getStationListV2(null,true);

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getStationListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else {
            String result = parser.getStationListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }
    }


    /**
     * <b>GET AGENCY LIST V2</b> - Returns a list with the name of all the agencies.
     *
     * @return A list with the name of the agencies.
     * @author José Manteigueiro
     * Version 2.0
     * @since 07/11/2017
     */
    private Response getAgencyList_V2(String format) {

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<Agency> res = dbcv2.getAgencyListV2();

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getAgencyListV2TXT(res).toString();
            return Response.ok(result, MediaType.TEXT_PLAIN).build();
        }else {
            String result = parser.getAgencyListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * <b>GET NETWORK LIST V2</b> - Returns a list of Network names.
     *
     * @return A list of Network names.
     * @author José Manteigueiro
     * Version 2.0
     * @since 20/11/2017
     */
    private Response getNetworkList_V2(String format) {

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<Network> res = dbcv2.getNetworkListV2();

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getNetworkListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else{
            String result = parser.getNetworkListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * <b>GET COUNTRY LIST V2</b> - Returns a list with the names of all the countries.
     *
     * @return A list with the names of the countries.
     * @author José Manteigueiro
     * Version 2.0
     * @since 08/11/2017
     */
    private Response getCountryList_V2(String format) {
        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<Country> res = dbcv2.getCountryListV2();

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getCountryListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else {
            String result = parser.getCountryListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * <b>GET STATE LIST V2</b> - Returns a list with the names of all the states.
     *
     * @return A list with the names of the states.
     * @author José Manteigueiro
     * Version 2.0
     * @since 08/11/2017
     */
    private Response getStateList_V2(String format) {
        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<State> res = dbcv2.getStateListV2();

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getStateListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else{
            String result = parser.getStateListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * <b>GET CITY LIST V2</b> - Returns a list with the names of all the cities.
     *
     * @return A list with the names of the cities.
     * @author José Manteigueiro
     * Version 2.0
     * @since 08/11/2017
     */
    private Response getCityList_V2(String format) {
        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<City> res = dbcv2.getCityListV2();

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getCityListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else {
            String result = parser.getCityListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * <b>GET RECEIVER LIST V2</b> - Returns a list with the names of all the receivers.
     *
     * @return A list with the names of the receivers.
     * @author José Manteigueiro
     * Version 2.0
     * @since 08/11/2017
     */
    private Response getReceiverList_V2(String format) {
        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<Receiver_type> res = dbcv2.getReceiverTypeListV2();

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getReceiverTypeListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else{
            String result = parser.getReceiverTypeListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * <b>GET ANTENNA LIST V2</b> - Returns a list with the names of all the antennas.
     *
     * @return A list with the names of the antennas.
     * @author José Manteigueiro
     * Version 2.0
     * @since 08/11/2017
     */
    private Response getAntennaList_V2(String format) {
        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<Antenna_type> res = dbcv2.getAntennaTypeListV2();

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getAntennaTypeListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else {
            String result = parser.getAntennaTypeListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }
    }

    /**
     * <b>GET RADOME LIST V2</b> - Returns a list with the name of all the radomes.
     *
     * @return A list with the name of the radomes.
     * @author José Manteigueiro
     * Version 2.0
     * @since 08/11/2017
     */
    private Response getRadomeList_V2(String format) {
        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<Radome_type> res = dbcv2.getRadomeTypeListV2();

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getRadomeTypeListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else{
            String result = parser.getRadomeTypeListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }

    }

    /**
     * <b>GET FILE TYPE LIST V2</b> - Returns a list with specifications of all file types.
     *
     * @return A list with specifications of file types.
     * @author José Manteigueiro
     * Version 2.0
     * @since 08/11/2017
     */
    private Response getFileTypeList_V2(String format) {
        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();

        ArrayList<File_type> res = dbcv2.getFileTypeListV2();

        //Parse and return it
        if(format.equalsIgnoreCase("list")){
            String result = parser.getFileTypeListV2TXT(res).toString();
            return Response.ok(result, TEXT_PLAIN).build();
        }else{
            String result = parser.getFileTypeListV2JSON(res).toString();
            return Response.ok(result, MediaType.APPLICATION_JSON).build();
        }

    }

    /**
     * <b>GET STATION BY MARKER V2</b> - Returns the station(s) that match the given marker(s) in the expected format.
     *
     *
     * <p>
     * Please input the size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please input the format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for marker should be separated by <b>,</b> (COMMA)
     * </p>
     * @param marker Marker of the station(s)
     * @param format output file format
     * @param size short or full data (full is slower)
     * @return All the stations in the expected format.
     * @author José Manteigueiro
     * Version 2.0
     * @since 13/11/2017
     * Last modified: 13/11/2017
    */
    @Path("marker/{marker}/{size}/{format}")
    @GET
    public Response getStationMarker_V2(@PathParam("marker") String marker, @PathParam("size") String size, @PathParam("format") String format) throws IOException {

        size = size.toLowerCase();
        format = format.toLowerCase();
        marker = marker.toUpperCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setMarkers(new ArrayList<>(Arrays.asList(marker.split(","))));

        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                    //SHORT CSV MARKER
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_marker_"+ Collections.singletonList(marker) +"_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON MARKER
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full" :
                sq.setFullV(true);
                switch(format){
                    //FULL CSV
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_marker_"+ Collections.singletonList(marker) +"_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();
                    //FULL JSON
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    /**
     * <b>GET STATION BY MARKERLONGNAME</b> - Returns the station(s) that match the given markerlongname(s) in the expected format.
     *
     *
     * <p>
     * Please input the size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please input the format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for markerlongname should be separated by <b>,</b> (COMMA)
     * </p>
     * @param markerlongname Markerlongname of the station(s)
     * @param format output file format
     * @param size short or full data (full is slower)
     * @return All the stations in the expected format.
     * @author José Manteigueiro
     * Version 1.0
     * @since 27/01/2020
     * Last modified: 27/01/2020
     */
    @Path("markerlongname/{markerlongname}/{size}/{format}")
    @GET
    public Response getStationMarkerLongName(@PathParam("markerlongname") String markerlongname, @PathParam("size") String size, @PathParam("format") String format) throws IOException {

        size = size.toLowerCase();
        format = format.toLowerCase();
        markerlongname = markerlongname.toUpperCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        String cleanMarkerlongname = markerlongname.replace("_","\\_");
        sq.setMarkerlongnames(new ArrayList<>(Arrays.asList(cleanMarkerlongname.split(","))));

        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                    //SHORT CSV MARKER
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_marker_"+ Collections.singletonList(markerlongname) +"_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON MARKER
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full" :
                sq.setFullV(true);
                switch(format){
                    //FULL CSV
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_marker_"+ Collections.singletonList(markerlongname) +"_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();
                    //FULL JSON
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    /**
     * <b>GET STATION BY NAME V2</b> - Returns the station(s) that match the given name(s) in the expected format.
     *
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for name should be separated by <b>,</b> (COMMA)
     * </p>
     * @param name Name of the station(s)
     * @param format output file format
     * @param size short or full data (full is slower)
     * @return All the stations in the expected format.
     * @author José Manteigueiro
     * Version 2.0
     * @since 13/11/2017
     * Last modified: 30/11/2017
     */
    @Path("name/{name}/{size}/{format}")
    @GET
    public Response getStationName_V2(@PathParam("name") String name, @PathParam("size") String size, @PathParam("format") String format) throws IOException {
        size = size.toLowerCase();
        format = format.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setNames(new ArrayList<>(Arrays.asList(name.split(","))));

        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                //SHORT CSV NAME
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_name_"+ Collections.singletonList(name) +"_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                //SHORT JSON NAME
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full" :
                sq.setFullV(true);
                switch(format){
                //FULL CSV NAME
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_name_"+ Collections.singletonList(name) +"_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();
                //FULL JSON NAME
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                //FULL XML NAME
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);
                        return Response.ok(xml, APPLICATION_XML).build();

                //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }                }

                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    /**
     * <b>GET STATION BY DATE V2</b> - Returns the station(s) that were active at some point since the initial date (and optionally until the end date).
     *
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     The date(s) should be in the format <b>YYYYMMDD</b> or <b>YYYY-MM-DD</b>.
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param date_from Initial date
     * @param date_to End date
     * @return All the stations in the expected format.
     * @author José Manteigueiro
     * Version 2.0
     * @since 17/11/2017
     * Last modified: 30/11/2017
     */
    @Path("date/{date_from}/{size}/{format}")
    @GET
    public Response getStationDate_V2(@PathParam("format") String format, @PathParam("size") String size, @PathParam("date_from") String date_from, @DefaultValue("") @QueryParam("date_to") String date_to) throws IOException {

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message; //error message

        if(!date_from.contains("-")){
            if(date_from.length()!=8){
                message = "The start date isn't in the right format. Use YYYY-MM-DD or YYYYMMDD.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
            date_from = date_from.substring(0,4)+"-"+date_from.substring(4,6)+"-"+date_from.substring(6,8);
        }else {
            if (date_from.length() != 10) {
                message = "The start date isn't in the right format. Use YYYY-MM-DD or YYYYMMDD.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }

        //Auxiliary calendar to format the date
        Calendar cal = Calendar.getInstance();
        cal.setLenient(false);
        cal.set(Calendar.YEAR,  Integer.parseInt(date_from.split("-")[0]));
        cal.set(Calendar.MONTH, Integer.parseInt(date_from.split("-")[1])-1); //January is 0
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date_from.split("-")[2]));

        //Check if date_from is valid (ex: 30 february)
        if(isDateInvalid(date_from)){
            message = "The start date isn't valid.";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }

        //Today's date
        Date today = new Date();

        Date initialDate = cal.getTime();
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");

        //If date_from is after today
        if(initialDate.after(today)){
            message = "The start date hasn't happened yet.";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }

        sq.setDate_from(dateFormat.format(initialDate));

        //If there's no date_to
        if(date_to.equals(""))
            sq.setDate_to("");
        else {
            if (!date_to.contains("-")) {
                if (date_to.length() != 8) {
                    message = "The end date isn't in the right format. Use YYYY-MM-DD or YYYYMMDD.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
                date_to = date_to.substring(0, 4) + "-" + date_to.substring(4, 6) + "-" + date_to.substring(6, 8);
            } else {
                if (date_to.length() != 10) {
                    message = "The end date isn't in the right format. Use YYYY-MM-DD or YYYYMMDD.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }

            cal.set(Calendar.YEAR, Integer.parseInt(date_to.split("-")[0]));
            cal.set(Calendar.MONTH, Integer.parseInt(date_to.split("-")[1])-1);
            cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date_to.split("-")[2]));
            Date endDate = cal.getTime();

            //Check if date_to is valid (ex: 30 february)
            if (isDateInvalid(date_to)){
                message = "The end date isn't valid.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }

            //date_to is after today
            if(endDate.after(today)){
                message = "The end date hasn't happened yet.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }

            //date_to is before or equals date_from
            if(endDate.before(initialDate) || endDate.equals(initialDate)){
                message = "The end date should be after the starting date.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }


            if (!endDate.equals(today))
                sq.setDate_to(dateFormat.format(endDate));
        }

        ArrayList<Station> res;


        switch(size) {
            case "short":
                switch (format) {
                    //SHORT CSV DATE
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_date_[" + date_from ;
                        if(date_to.equals(""))
                            fileName += "]_short.csv";
                        else
                            fileName += ", " + date_to + "]_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON DATE
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full":
                sq.setFullV(true);
                switch (format) {
                    //FULL CSV DATE
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_date_[" + date_from;
                        if(date_to.equals(""))
                            fileName += "]_full.csv";
                        else
                            fileName += ", " + date_to + "]_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //FULL JSON DATE
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML DATE
                    case "xml":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);
                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default:{
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

                //Bad request
            default:{
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    private static boolean isDateInvalid(String date) {
        try {
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
            df.setLenient(false);
            df.parse(date);
            return false;
        } catch (ParseException e) {
            return true;
        }
    }

    /**
     * <b>GET STATION BY AGENCY V2</b> - Returns the station(s) that belong to the specified agency(ies).
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for agency should be separated by <b>,</b> (COMMA)
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param agency agency(ies) to which the stations belong e.g. OCA-Geoazur
     * @return All the stations in the expected format.
     * @author José Manteigueiro
     * Version 2.0
     * @since 17/11/2017
     * Last modified: 17/11/2017
     */
    @Path("agency/{agency}/{size}/{format}")
    @GET
    public Response getStationAgency_V2(@PathParam("format") String format, @PathParam("size") String size, @PathParam("agency") String agency) throws IOException {
        size = size.toLowerCase();
        format = format.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setAgencies(new ArrayList<>(Arrays.asList(agency.split(","))));

        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                    //SHORT CSV AGENCY
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_agency_"+ Collections.singletonList(agency) +"_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON AGENCY
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full" :
                sq.setFullV(true);
                switch(format){
                    //FULL CSV AGENCY
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_agency_"+ Collections.singletonList(agency) +"_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();


                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //FULL JSON AGENCY
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML AGENCY
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    /**
     * <b>GET STATION BY NETWORK V2</b> - Returns the station(s) that belong to the specified network(s).
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for network should be separated by <b>,</b> (COMMA)
     * </p>
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param network network(s) to which the stations belong
     * @return All the stations in the expected format.
     * @author José Manteigueiro
     * Version 2.0
     * @since 20/11/2017
     * Last modified: 30/11/2017
     */
    @Path("network/{network}/{size}/{format}")
    @GET
    public Response getStationNetwork_V2(@PathParam("format") String format, @PathParam("size") String size, @PathParam("network") String network) throws IOException {
        size = size.toLowerCase();
        format = format.toLowerCase();
        network = network.toUpperCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setNetworks(new ArrayList<>(Arrays.asList(network.split(","))));

        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                    //SHORT CSV NETWORK
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_network_"+ Collections.singletonList(network) +"_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON NETWORK
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full" :
                sq.setFullV(true);
                switch(format){
                    //FULL CSV NETWORK
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_network_"+ Collections.singletonList(network) +"_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //FULL JSON NETWORK
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML NETWORK
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }


    /**
     * <b>GET STATION BY STATION TYPE V2</b> - Returns the station(s) that belong to the specified station type.
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     The station type should be (exactly):
     *     <ul>
     *         <li><b>GPS/GNSS Continuous</b></li>
     *         <li><b>GNSS</b></li>
     *         <li><b>GNSS Continuous</b></li>
     *     </ul>
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param stationType type of the stations
     * @return All the stations in the expected format.
     * @throws IOException ...
     * @author José Manteigueiro
     * Version 2.0
     * @since 20/11/2017
     * Last modified: 30/11/2017
     */
    @Path("type/{station_type}/{size}/{format}")
    @GET
    public Response getStationType_V2(@PathParam ("station_type") String stationType, @PathParam("size") String size, @PathParam("format") String format) throws IOException {
        size = size.toLowerCase();
        format = format.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;
        if(stationType.equals("GNSS") || stationType.equals("GPS/GNSS Continuous") || stationType.equals("GNSS Continuous"))
            sq.setStationType(stationType);
        else{
            message = "Wrong station type. Use 'GPS/GNSS Continuous', 'GNSS Continuous' or 'GNSS'.";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }


        ArrayList<Station> res;

        switch (size) {
            case "short":
                switch (format) {
                    //SHORT CSV STATION TYPE
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_type_" + stationType.replace(" ","_").replace("/","_") + "_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON STATION TYPE
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full":
                sq.setFullV(true);
                switch (format) {
                    //FULL CSV STATION TYPE
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_type_" + stationType.replace(" ","_").replace("/","_") + "_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //FULL JSON STATION TYPE
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML STATION TYPE
                    case "xml":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }


    /**
     * <b>GET STATION BY ANTENNA V2</b> - Returns the station(s) that have the specified antenna.
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for antenna should be separated by <b>,</b> (COMMA)
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param antenna antenna(s) name(s) e.g. LEIAT302-GP
     * @return All the stations in the expected format.
     * @throws IOException ...
     * @author José Manteigueiro
     * Version 2.0
     * @since 21/11/2017
     * Last modified: 19/12/2017
     */
    @Path("antenna/{antenna}/{size}/{format}")
    @GET
    public Response getStationAntenna_V2(@PathParam ("antenna") String antenna, @PathParam ("size") String size, @PathParam("format") String format) throws IOException {
        size = size.toLowerCase();
        format = format.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setAntenna(new ArrayList<>(Arrays.asList(antenna.split(","))));

        ArrayList<Station> res;

        switch (size) {
            case "short":
                switch (format) {
                    //SHORT CSV ANTENNA
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_antenna_" + Collections.singletonList(antenna)  + "_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON ANTENNA
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full":
                sq.setFullV(true);
                switch (format) {
                    //FULL CSV ANTENNA
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_antenna_" + Collections.singletonList(antenna) + "_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //FULL JSON ANTENNA
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML ANTENNA
                    case "xml":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    /**
     * <b>GET STATION BY RECEIVER V2</b> - Returns the station(s) that have the specified receiver.
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for receiver should be separated by <b>,</b> (COMMA)
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param receiver receiver(s) name(s) e.g. ASHTECH GG24C
     * @return All the stations in the expected format.
     * @throws IOException ...
     * @author José Manteigueiro
     * Version 2.0
     * @since 30/11/2017
     * Last modified: 30/11/2017
     */
    @Path("receiver/{receiver}/{size}/{format}")
    @GET
    public Response getStationReceiverType_V2(@PathParam ("receiver") String receiver, @ PathParam ("size") String size, @PathParam("format") String format) throws IOException {
        size = size.toLowerCase();
        format = format.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setReceiver(new ArrayList<>(Arrays.asList(receiver.split(","))));

        ArrayList<Station> res;

        switch (size) {
            case "short":
                switch (format) {
                    //SHORT CSV RECEIVER
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_receiver_" + Collections.singletonList(receiver) + "_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON RECEIVER
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full":
                sq.setFullV(true);
                switch (format) {
                    //FULL CSV RECEIVER
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_receiver_" + Collections.singletonList(receiver) + "_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //FULL JSON RECEIVER
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML RECEIVER
                    case "xml":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    /**
     * <b>GET STATION BY RADOME V2</b> - Returns the station(s) that have the specified radome.
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for radome should be separated by <b>,</b> (COMMA)
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param radome radome(s) name(s) e.g. OLGA
     * @return All the stations in the expected format.
     * @throws IOException ...
     * @author José Manteigueiro
     * Version 2.0
     * @since 05/12/2017
     * Last modified: 06/12/2017
     */
    @Path("radome/{radome}/{size}/{format}")
    @GET
    public Response getStationRadome_V2(@PathParam("radome") String radome, @PathParam("size") String size, @PathParam("format") String format) throws IOException {
        size = size.toLowerCase();
        format = format.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setRadome(new ArrayList<>(Arrays.asList(radome.split(","))));

        ArrayList<Station> res;

        switch (size) {
            case "short":
                switch (format) {
                    //SHORT CSV RADOME
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_radome_" + Collections.singletonList(radome) + "_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON RADOME
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full":
                sq.setFullV(true);
                switch (format) {
                    //FULL CSV RADOME
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_radome_" + Collections.singletonList(radome) + "_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //FULL JSON RADOME
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML RADOME
                    case "xml":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    /**
     * <b>GET STATION BY COORDINATES V2</b> - Returns the station(s) that are inside the specified perimeter.
     *
     * <p>
     * Please specify the output output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the ouput format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     * Please specify the bounding box type as one of the formats below:
     * <ul>
     *     <li>Circle</li>
     *     <li>Polygon</li>
     *     <li>Rectangle</li>
     * </ul>
     * </p>
     *
     * <p>
     *     <b>Circle parameters:</b>
     *     <ul>
     *         <li>centerLat - latitude of the center point</li>
     *         <li>centerLon - longitude of the center point</li>
     *         <li>radius - radius of the circle, in km</li>
     *     </ul>
     * </p>
     *
     * <p>
     *     <b>Polygon parameters:</b>
     *     <ul>
     *          <li>latitude,longitude;latitude,longitude;..</li>
     *     </ul>
     *
     * The polygon vertices follow the order specified in this image: <a href="../../../images/coordinates_polygon_order.jpg" target="_blank">Polygon Order</a>
     * </p>
     *
     * <p>
     *     <b>Rectangle parameters:</b>
     *     <ul>
     *         <li>maxLat - maximum latitude of the rectangle</li>
     *         <li>maxLon - maximum longitude of the rectangle</li>
     *         <li>minLat - minimum latitude of the rectangle</li>
     *         <li>minLon - minimum longitude of the rectangle</li>
     *     </ul>
     * </p>
     *
     * <p>The parameters must be separated by <b>&</b>(AMPERSAND)</p>
     * <p>
     *     <b>Side note:</b> by default, parameters are filled with -999. If this value is changed, is assumed you want to use it;
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param centerLat - for circle only e.g. 43.034
     * @param centerLon - for circle only e.g. 7.013
     * @param radius - for circle only e.g. 50 (km)
     * @param minLat - for rectangle only e.g. 43.0
     * @param maxLat - for rectangle only e.g. 47.0
     * @param minLon - for rectangle only e.g. 6.5
     * @param maxLon - for rectangle only e.g. 8.8
     * @param polygonString - for polygon only e.g. 42.0,7.0;32.1,7.4;37,4.6
     * @param area format of the perimeter, e.g. circle
     * @return All the stations in the expected format.
     * @throws IOException ...
     * @author José Manteigueiro
     * Version 2.0
     * @since 12/12/2017
     * Last modified: 01/02/2018
     */
    @Path("coordinates/{area}/{size}/{format}")
    @GET
    public Response getStationCoordinatesV2(@PathParam("area") String area,
                                            @PathParam("size") String size,
                                            @PathParam("format") String format,
                                            @DefaultValue("-999") @QueryParam("centerlat") double centerLat,
                                            @DefaultValue("-999") @QueryParam("centerlon") double centerLon,
                                            @DefaultValue("-999") @QueryParam("radius") double radius,
                                            @DefaultValue("-999") @QueryParam("minlat") double minLat,
                                            @DefaultValue("-999") @QueryParam("maxlat") double maxLat,
                                            @DefaultValue("-999") @QueryParam("minlon") double minLon,
                                            @DefaultValue("-999") @QueryParam("maxlon") double maxLon,
                                            @DefaultValue("") @QueryParam("polygon") String polygonString
                                          ) throws IOException {
        size = size.toLowerCase();
        format = format.toLowerCase();
        format = format.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        ArrayList<Station> res;

        switch(area) {
            case "rectangle":
                    if(minLat == -999 || maxLat == -999 || minLon == -999 || maxLon == -999 ){
                        message = "If you intend the area in the format of a rectangle, please specify the minLat, maxLat, minLon and maxLon parameters.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                    else{

                        if(minLat < -90.0 || minLat > 90.0){
                            message = "The value of minLat should be between -90.0 and 90.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        else if(maxLat < -90.0 || maxLat > 90.0){
                            message = "The value of maxLat should be between -90.0 and 90.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        else if(minLon < -180.0 || minLon > 180.0){
                            message = "The value of minLon should be between -180.0 and 180.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        else if(maxLon < -180.0 || maxLon > 180.0){
                            message = "The value of maxLon should be between -180.0 and 180.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        else if(minLon >= maxLon){
                            message = "The value of minLon should be lower then maxLon";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        else if(minLat >= maxLat){
                            message = "The value of minLon should be lower then maxLon";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        ArrayList<Integer> allCords;
                        allCords = dbcv2.getCoordinatesRectangleV2(minLat, minLon, maxLat, maxLon);
                        sq=setStationCoordinatesRectangle(sq, allCords);
                        break;
                    }
            case "circle":
                if(centerLat == -999 || centerLon == -999 || radius == -999){
                    message = "If you intend the area in the format of a circle, please specify centerLat, centerLon and radius.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
                else {
                    if(centerLat < -90.0 || centerLat > 90.0) {
                        message = "The value of centerLat should be between -90.0 and 90.0";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                    if(centerLon < -180.0 || centerLon > 180.0) {
                        message = "The value of centerLon should be between -180.0 and 180.0";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                    if(radius <= 0.0){
                        message = "The value of the radius should be positive and in km.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                    ArrayList<Integer> allCords;
                    allCords = dbcv2.getCoordinatesCircleV2(centerLat, centerLon, radius);
                    sq=setStationCoordinatesCircle(sq, allCords);

                    break;
                }
            case "polygon":
                ArrayList<Coordinates> poly = new ArrayList<>();
                poly = ParsersV2.parsePolygonV2(polygonString);

                ArrayList<Integer> allCords;
                allCords = dbcv2.getInsidePolygonIDs(poly);
                sq=setStationCoordinatesPolygon(sq, allCords);

                break;
            default : {
                message = "Wrong format. Use circle, polygon or rectangle.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }


        switch (size) {
            case "full":
                sq.setFullV(true);
                switch (format) {
                    //FULL CSV COORDINATES
                    case "csv":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_coordinates_" + format + "_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //FULL JSON COORDINATES
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML COORDINATES
                    case "xml":
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
            case "short":
                switch (format) {
                    //SHORT CSV COORDINATES
                    case "csv":
                            //Get the stations array
                            res = dbcv2.getStationsShortV2(sq);

                            //Parse and return it
                            String result = parser.getStationShortV2CSV(res);

                            String fileName = "stations_coordinates_" + format + "_short.csv";
                            File file = new File(fileName);
                            FileWriter fw = new FileWriter(file.getAbsoluteFile());
                            BufferedWriter bw = new BufferedWriter(fw);
                            bw.write(result);
                            bw.close();

                            return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                    .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                    .build();


                    //SHORT JSON COORDINATES
                    case "json":
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    default : {
                        message = "Wrong output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
            //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    private StationQuery setStationCoordinatesRectangle(StationQuery sq, ArrayList<Integer> ids){

        sq.setCoord(ids, "rectangle");

        return sq;
    }

    private StationQuery setStationCoordinatesCircle(StationQuery sq, ArrayList<Integer> ids){

        sq.setCoord(ids, "circle");

        return sq;
    }

    private StationQuery setStationCoordinatesPolygon(StationQuery sq, ArrayList<Integer> ids){

        sq.setCoord(ids, "polygon");

        return sq;
    }

    /**
     * <b>GET STATION BY COUNTRY V2</b> - Returns the station(s) from the specified country(ies).
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for country should be separated by <b>,</b> (COMMA)
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param country country(ies) name(s) e.g. Portugal
     * @return All the stations in the expected format.
     * @throws IOException ...
     * @author José Manteigueiro
     * Version 2.0
     * @since 12/12/2017
     * Last modified: 12/12/2017
     */
    @Path("country/{country}/{size}/{format}")
    @GET
    public Response getStationCountryV2(@PathParam("country") String country, @PathParam("size") String size, @PathParam("format") String format) throws IOException {

        size = size.toLowerCase();
        format = format.toLowerCase();
        country = country.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setCountry(new ArrayList<>(Arrays.asList(country.split(","))));

        ArrayList<Integer> locationIDs;
        locationIDs = dbcv2.getCountryV2(sq);
        // If no ids are found, use -1 so that no station shows up
        if(locationIDs.size() == 0)
            locationIDs.add(-1);
        sq.setCountryIDs(locationIDs);

        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                    //SHORT CSV MARKER
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_country_"+ Collections.singletonList(country) +"_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON MARKER
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full" :
                sq.setFullV(true);
                switch(format){
                    //FULL CSV
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_country_"+ Collections.singletonList(country) +"_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();
                    //FULL JSON
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    /**
     * <b>GET STATION BY STATE V2</b> - Returns the station(s) from the specified state(s).
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (valid only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for state should be separated by <b>,</b> (COMMA)
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param state state(s) name(s) e.g. Lisbon
     * @return All the stations in the specified format.
     * @throws IOException ...
     * @author José Manteigueiro
     * Version 2.0
     * @since 12/12/2017
     * Last modified: 12/12/2017
     */
    @Path("state/{state}/{size}/{format}")
    @GET
    public Response getStationStateV2(@PathParam("state") String state, @PathParam("size") String size, @PathParam("format") String format) throws IOException {

        size = size.toLowerCase();
        format = format.toLowerCase();
        state = state.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setState(new ArrayList<>(Arrays.asList(state.split(","))));

        ArrayList<Integer> locationIDs;
        locationIDs = dbcv2.getStateV2(sq);
        // If no ids are found, use -1 so that no station shows up
        if(locationIDs.size() == 0)
            locationIDs.add(-1);
        sq.setStateIDs(locationIDs);

        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                    //SHORT CSV MARKER
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_state_"+ Collections.singletonList(state) +"_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON MARKER
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full" :
                sq.setFullV(true);
                switch(format){
                    //FULL CSV
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_state_"+ Collections.singletonList(state) +"_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();
                    //FULL JSON
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    /**
     * <b>GET STATION BY CITY V2</b> - Returns the station(s) from the specified city(ies).
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     * <li>full</li>
     * <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>csv</li>
     * <li>json</li>
     * <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     Multiple values for city should be separated by <b>,</b> (COMMA)
     * </p>
     *
     * @param format output file format
     * @param size short or full data (full is slower)
     * @param city city(ies) name(s) e.g. Cascais
     * @return All the stations in the expected format.
     * @throws IOException ...
     * @author José Manteigueiro
     * Version 2.0
     * @since 12/12/2017
     * Last modified: 12/12/2017
     */
    @Path("city/{city}/{size}/{format}")
    @GET
    public Response getStationCityV2(@PathParam("city") String city, @PathParam("size") String size, @PathParam("format") String format) throws IOException {

        size = size.toLowerCase();
        format = format.toLowerCase();
        city = city.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        sq.setCity(new ArrayList<>(Arrays.asList(city.split(","))));

        ArrayList<Integer> locationIDs;
        locationIDs = dbcv2.getCityV2(sq);
        // If no ids are found, use -1 so that no station shows up
        if(locationIDs.size() == 0)
            locationIDs.add(-1);
        sq.setCityIDs(locationIDs);

        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                    //SHORT CSV MARKER
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_city_"+ Collections.singletonList(city) +"_short.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON MARKER
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full" :
                sq.setFullV(true);
                switch(format){
                    //FULL CSV
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_city_"+ Collections.singletonList(city) +"_full.csv";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();
                    //FULL JSON
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    //     *         <li>station_type (multiple);</li>
    /**
     * <b>GET STATION COMBINATION V2</b> - Returns the station(s) that match the parameters.
     *
     * <p>
     * Please specify the output size as one of the options below:
     * <ul>
     *      <li>full</li>
     *      <li>short</li>
     * </ul>
     * <p>
     *
     * <p>
     *  Please specify the output format as one of the options below:
     *  <ul>
     *      <li>csv</li>
     *      <li>json</li>
     *      <li>xml (only for full size)</li>
     *
     * </ul>
     * </p>
     *
     * <p>
     *     The parameters accepted are:
     *     <ul>
     *         <li>agency (multiple);</li>
     *         <li>antenna (multiple);</li>
     *         <li>city (multiple);</li>
     *         <li>coordinates:
     *             <ul>
     *                 <li><b>circle</b>, with the following parameters:</li>
     *                 <ul>
     *                 <li>centerLat;</li>
     *                 <li>centerLon;</li>
     *                 <li>radius;</li>
     *                 </ul>
     *             </ul>
     *             <ul>
     *                 <li><b>polygon</b>, with the following parameters:</li>
     *                 <ul>
     *                      <li>latitude,longitude;latitude,longitude;..</li>
     *                 </ul>
     *             </ul>
     *             <ul>
     *                 <li><b>rectangle</b>, with the following parameters:</li>
     *                 <ul>
     *                 <li>maxLat;</li>
     *                 <li>maxLon;</li>
     *                 <li>minLat;</li>
     *                 <li>minLon;</li>
     *                 </ul>
     *             </ul>
     *          </li>
     *         <li>country (multiple);</li>
     *         <li>date_from;</li>
     *         <li>date_to;</li>
     *         <li>geojson; (http://geojson.io)</li>
     *         <li>installedDateMax;</li>
     *         <li>installedDateMin;</li>
     *         <li>invertedNetworks (multiple);</li>
     *         <li>lifetime;</li>
     *         <li>marker (multiple);</li>
     *         <li>maxAlt;</li>
     *         <li>maxLon;</li>
     *         <li>maxLat;</li>
     *         <li>minAlt;</li>
     *         <li>minLat;</li>
     *         <li>minLon;</li>
     *         <li>network (multiple);</li>
     *         <li>satellite (multiple);</li>
     *         <li>site (multiple);</li>
     *         <li>state (multiple);</li>
     *         <li>radome (multiple);</li>
     *         <li>receiver (multiple);</li>
     *         <li>removedDateMax;</li>
     *         <li>removedDateMin;</li>
     *         <li><b>T2 parameters:</b></li>
     *              <ul>
     *                  <li>dataAvailability;</li>
     *                  <li>dateRange;</li>
     *                  <li>fileType;</li>
     *                  <li>minimumObservationDays;</li>
     *                  <li>minimumObservationYears;</li>
     *                  <li>samplingWindow;</li>
     *                  <li>samplingFrequency;</li>
     *              </ul>
     *     </ul>
     * </p>
     *
     * <p>
     *     The parameters should be separated by <b>&</b> (AMPERSAND)
     * </p>
     *
     * <p>
     *     Multiple values for a specified parameter should be separated by <b>,</b> (COMMA)
     * </p>
     *
     * <p>
     *      <b>Note: the results are an <u>INTERSECTION</u> of the parameters, but each parameter may contain multiple values.</b>
     * </p>
     * <p>
     * So if the value of the country parameter is 'France,Italy' and the coordinate parameter is a rectangle large enough to contain stations from France, Italy and Switzerland, only stations from France and Italy will show up.
     * </p>
     * <p></p>
     * <p>An example of a query could be: </p>
     * <p>http://localhost:8080/GlassFramework/webresources/stations/v2/combination/short/csv?country=France,Italy&ampnetwork=RENAG&coordinates=circle&ampcenterlat=44.2&ampcenterlon=7.1&ampradius=100</p>
     *
     * @param size              output size, e.g. <b>short</b>
     * @param format            output file format, e.g. <b>csv</b>
     * @param marker            station's marker, e.g. <b>LAGO</b>
     * @param site              station's site name, e.g. <b>Lagos</b>
     * @param coordinates       format of the perimeter, e.g. <b>circle</b>
     * @param date_from         starting date, stations that were active after this date, e.g. <b>19990125</b>
     * @param date_to           ending date, station was active previously to this date, e.g. <b>20171027</b>
     * @param network           station's network e.g. <b>EUREF</b>
     * @param agency            station's agency e.g. <b>IGN France</b>
     * @param antenna           station's antenna e.g. <b>LEIAT302-GP</b>
     * @param radome            station's radome e.g. <b>OLGA</b>
     * @param receiver          station's receiver e.g. <b>ASHTECH GG24C</b>
     * @param country           station's country e.g. <b>Portugal</b>
     * @param state             station's state e.g. <b>Lisbon</b>
     * @param city              station's city e.g. <b>Cascais</b>
     * @param minLat            for coord. rectangle only e.g. <b>5</b>
     * @param maxLat            for coord. rectangle only e.g. <b>46.2</b>
     * @param minLon            for coord. rectangle only e.g. <b>41</b>
     * @param maxLon            for coord. rectangle only e.g. <b>15.5</b>
     * @param centerLat         for coord. circle only e.g. <b>43.2</b>
     * @param centerLon         for coord. circle only e.g. <b>10.0</b>
     * @param radius            for coord. circle only e.g. <b>50</b>
     * @param polygonString     for polygon only e.g. <b>42.0,7.0;32.1,7.4;37,4.6</b>
     * @param minAlt            station's minimum altitude e.g.  <b>80.5</b>
     * @param maxAlt            station's maximum altitude e.g.  <b>580</b>
     * @param installedDateMax  maximum date for the installation of a station e.g. <b>2007-06-24</b>
     * @param installedDateMin  minimum date for the installation of a station e.g. <b>20070113</b>
     * @param removedDateMax    maximum date for the removal of a station e.g. <b>20170214</b>
     * @param removedDateMin    minimum date for the removal of a station e.g. <b>2017-01-17</b>
     * @param invertedNetworks  networks to which the station doesn't belong e.g.  <b>RENAG,EUREF</b>
     * @param satellite         satellites supported by the station e.g. <b>GPS,GLO</b>
     * @param dataAvailability  T2 minimum percentage of data available (e.g. <b>75</b>) between the number of days the station was installed and the days that have files
     * @param dateRange         T2 date range to use relatively to files e.g. <b>20150115,2017-10-24</b>
     * @param fileType          T2 type of the files e.g. <b>RINEX2</b>
     * @param samplingFrequency T2 sampling frequency e.g. <b>30s</b> or <b>1s</b>
     * @param samplingWindow    T2 sampling window e.g. <b>24h</b> or <b>1h</b>
     * @param lifetime          Minimum amount of days a station was installed e.g. <b>650</b>
     * @param minimumObservationDays Minimum amount of days with files e.g. <b>400</b>
     * @param minimumObservationYears Minimum amount of years with files e.g. <b>5</b>
     * @param altitude          Altitude range e.g. <b>400,900</b>
     * @param latitude          Latitude range e.g. <b>30,40</b>
     * @param longitude         Longitude range e.g. <b>-15,20</b>
     * @param statusfile        T2 status file e.g. <b>-3</b> or <b>2</b> or <b>1</b> or <b>0</b> or <b>1</b> or <b>2</b> or <b>3</b>
     * @param geoJSON           a GeoJSON as bounding box (e.g. http://geojson.io)
     * @throws IOException ...
     * @author José Manteigueiro
     * Version 2.1
     * @since 14/12/2017
     * Last modified: 20/11/2018
     */
    @Path("combination/{size}/{format}")
    @GET
    public Response getStationCombinationV2(@PathParam("size") String size,
                                            @PathParam("format") String format,
                                            //Marker
                                            @DefaultValue("") @QueryParam("marker") String marker,
                                            //Site
                                            @DefaultValue("") @QueryParam("site") String site,
                                            //Coordinates
                                            @DefaultValue("") @QueryParam("coordinates") String coordinates,
                                            //Date_from
                                            @DefaultValue("") @QueryParam("date_from") String date_from,
                                            //Date_to
                                            @DefaultValue("") @QueryParam("date_to") String date_to,
                                            //Network
                                            @DefaultValue("") @QueryParam("network") String network,
                                            //Agency
                                            @DefaultValue("") @QueryParam("agency") String agency,
                                            //Type
                                            //@DefaultValue("") @QueryParam("station_type") String station_type,
                                            //Antenna
                                            @DefaultValue("") @QueryParam("antenna") String antenna,
                                            //Radome
                                            @DefaultValue("") @QueryParam("radome") String radome,
                                            //Receiver
                                            @DefaultValue("") @QueryParam("receiver") String receiver,
                                            //Country
                                            @DefaultValue("") @QueryParam("country") String country,
                                            //State
                                            @DefaultValue("") @QueryParam("state") String state,
                                            //City
                                            @DefaultValue("") @QueryParam("city") String city,


                                            //CoordinatesRectangleLatMin or just min Lat
                                            @DefaultValue("-999") @QueryParam("minLat") double minLat,
                                            //CoordinatesRectangleLatMax or just max Lat
                                            @DefaultValue("-999") @QueryParam("maxLat") double maxLat,
                                            //CoordinatesRectangleLonMin or just min Lon
                                            @DefaultValue("-999") @QueryParam("minLon") double minLon,
                                            //CoordinatesRectangleLonMax or just max Lon
                                            @DefaultValue("-999") @QueryParam("maxLon") double maxLon,
                                            //Minimum altitude
                                            @DefaultValue("-10000.0") @QueryParam("minAlt") double minAlt,
                                            //Maximum altitude
                                            @DefaultValue("20000.0") @QueryParam("maxAlt") double maxAlt,


                                            //Latitude "AND"
                                            @DefaultValue("") @QueryParam("latitude") String latitude,
                                            //Longitude "AND"
                                            @DefaultValue("") @QueryParam("longitude") String longitude,
                                            //Altitude "AND"
                                            @DefaultValue("") @QueryParam("altitude") String altitude,


                                            //CoordinatesCircleCenterLat
                                            @DefaultValue("-999") @QueryParam("centerLat") double centerLat,
                                            //CoordinatesCircleCenterLon
                                            @DefaultValue("-999") @QueryParam("centerLon") double centerLon,
                                            //CoordinatesCircleRadius
                                            @DefaultValue("-999") @QueryParam("radius") double radius,

                                            //Satellite System
                                            @DefaultValue("") @QueryParam("satelliteSystem") String satellite,
                                            //Inversed Network
                                            @DefaultValue("") @QueryParam("invertedNetworks") String invertedNetworks,

                                            //Installed Date Min
                                            @DefaultValue("") @QueryParam("installedDateMin") String installedDateMin,
                                            //Installed Date Max
                                            @DefaultValue("") @QueryParam("installedDateMax") String installedDateMax,
                                            //Removed Date Min
                                            @DefaultValue("") @QueryParam("removedDateMin") String removedDateMin,
                                            //Removed Date Max
                                            @DefaultValue("") @QueryParam("removedDateMax") String removedDateMax,
                                            //Station Lifetime
                                            @DefaultValue("0") @QueryParam("lifetime") int lifetime,


                                            //T2 data_availability
                                            @DefaultValue("") @QueryParam("dataAvailability") String dataAvailability,
                                            //T2 file_type
                                            @DefaultValue("") @QueryParam("fileType") String fileType,
                                            //T2 sampling_frequency
                                            @DefaultValue("") @QueryParam("samplingFrequency") String samplingFrequency,
                                            //T2 sampling_window
                                            @DefaultValue("") @QueryParam("samplingWindow") String samplingWindow,
                                            //T2 date_range    start|end
                                            @DefaultValue("") @QueryParam("dateRange") String dateRange,
                                            //T2 minimum observation days
                                            @DefaultValue("0") @QueryParam("minimumObservationDays") int minimumObservationDays,
                                            //T2 minimum observation years
                                            @DefaultValue("0") @QueryParam("minimumObservationYears") float minimumObservationYears,
                                            //T2 status file
                                            @DefaultValue("-10") @QueryParam("statusfile") int statusfile,
                                            
                                            //CoordinatesPolygon
                                            @DefaultValue("") @QueryParam("polygon") String polygonString,

                                            //GeoJSON
                                            @DefaultValue("") @QueryParam("geojson") String geoJSON,
            
                                            //T3 observations types
                                            @DefaultValue("") @QueryParam("observationtype") String observationtype,
                                            //T3 frequency
                                            @DefaultValue("") @QueryParam("frequency") String frequency,
                                            //T3 channel
                                            @DefaultValue("") @QueryParam("channel") String channel,
                                            //T3 constellation
                                            @DefaultValue("") @QueryParam("constellation") String constellation,
                                            //T3 ratioepoch
                                            @DefaultValue("0.0") @QueryParam("ratioepoch") float ratioepoch,
                                            //T3 elevation angle
                                            @DefaultValue("0.0") @QueryParam("elevangle") float elevangle,
                                            //T3 multipath value
                                            @DefaultValue("0.0") @QueryParam("multipathvalue") float multipathvalue,
                                            //T3 nb cycle slips
                                            @DefaultValue("0") @QueryParam("nbcycleslips") int nbcycleslips,
                                            //T3 ssp rms
                                            @DefaultValue("0.0") @QueryParam("spprms") float spprms,
                                            //T3 nb clock jumps
                                            @DefaultValue("0") @QueryParam("nbclockjumps") int nbclockjumps
                                            ) throws IOException {


        size = size.toLowerCase();
        format = format.toLowerCase();
        marker = marker.toUpperCase();
        coordinates = coordinates.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;
        //ArrayList<Integer> IDs;
        //HashSet<Integer> idHashset = new HashSet<>();
        //HashSet<Integer> aux;
        //boolean firstCondition = true;

        //Check if there's a city specified, or multiple
        if (!city.equals("")) {
            sq.setCity(new ArrayList<>(Arrays.asList(city.split(","))));
        }

        //Check if there's a state specified, or multiple
        if (!state.equals("")) {
            sq.setState(new ArrayList<>(Arrays.asList(state.split(","))));
        }

        //Check if there's a country specified, or multiple
        if (!country.equals("")) {
            sq.setCountry(new ArrayList<>(Arrays.asList(country.split(","))));
        }

        //Check if there's a marker specified, or multiple
        if (!marker.equals("")) {
            String cleanMarker = marker.replace("_","\\_");
            sq.setMarkers(new ArrayList<>(Arrays.asList(cleanMarker.split(","))));
        }

        //Check if there's a site/station name specified, or multiple
        if (!site.equals("")) {
            sq.setNames(new ArrayList<>(Arrays.asList(site.split(","))));
        }

        //Check if there's a network specified (or multiple)
        if (!network.equals("")) {
            sq.setNetworks(new ArrayList<>(Arrays.asList(network.split(","))));
        }

        //Check if there's an agency specified (or multiple)
        if (!agency.equals("")) {
            sq.setAgencies(new ArrayList<>(Arrays.asList(agency.split(","))));
        }

        //Check if there's a date_from specified
        if (!date_from.equals("")) {
            sq.setDate_from(date_from);
        }

        //Check if there's a date_to specified
        if(!date_to.equals("")) {
            sq.setDate_to(date_to);
        }

    /* Disabled
        //Check if there's a station type specified, or multiple
        if(!station_type.equals("")) {
            sq.setStationType(station_type);
            IDs = dbcv2.getStationTypeV2(sq);
            aux = new HashSet<>(IDs);
            if(firstCondition) {
                idHashset = aux;
                firstCondition = false;
            }
            else
                idHashset.retainAll(aux);
        }
    */

        //Check if there's an antenna specified, or multiple
        if(!antenna.equals("")) {
            sq.setAntenna(new ArrayList<>(Arrays.asList(antenna.split(","))));
        }

        //Check if there's a radome type specified, or multiple
        if(!radome.equals("")) {
            sq.setRadome(new ArrayList<>(Arrays.asList(radome.split(","))));
        }

        //Check if there's a receiver type specified, or multiple
        if(!receiver.equals("")) {
            sq.setReceiver(new ArrayList<>(Arrays.asList(receiver.split(","))));
        }

        //Check if installed date min was specified
        if(!installedDateMin.equals("")){
            if(installedDateMin.length() != 8  && installedDateMin.length() != 10)     {
                message = "If you intend specifying an installation date use the format YYYYMMDD or YYYY-MM-DD.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }else{
                try{
                    installedDateMin = installedDateMin.replace("-","");
                    sq.setInstalledDateMin(installedDateMin);
                }catch(Exception e){
                    message = "If you intend specifying an installation date use the format YYYYMMDD or YYYY-MM-DD.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }
        }

        //Check if installed date max was specified
        if(!installedDateMax.equals("")){
            if(installedDateMax.length() != 8 && installedDateMax.length() != 10)     {
                message = "If you intend specifying an installation date use the format YYYYMMDD or YYYY-MM-DD.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }else{
                try{
                    installedDateMax = installedDateMax.replace("-","");
                    sq.setInstalledDateMax(installedDateMax);
                }catch(Exception e){
                    message = "If you intend specifying an installation date use the format YYYYMMDD or YYYY-MM-DD.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }
        }

        //Check if removed date min was specified
        if(!removedDateMin.equals("")){
            if(removedDateMin.length() != 8 && removedDateMin.length() != 10)     {
                message = "If you intend specifying a removed date use the format YYYYMMDD or YYYY-MM-DD.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }else{
                try{
                    removedDateMin = removedDateMin.replace("-","");
                    sq.setRemovedDateMin(removedDateMin);
                }catch(Exception e){
                    message = "If you intend specifying a removed date use the format YYYYMMDD or YYYY-MM-DD.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }
        }

        //Check if removed date max was specified
        if(!removedDateMax.equals("")){
            if(removedDateMax.length() != 8 && removedDateMax.length() != 10)     {
                message = "If you intend specifying a removed date use the format YYYYMMDD or YYYY-MM-DD.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }else{
                try{
                    removedDateMax = removedDateMax.replace("-","");
                    sq.setRemovedDateMax(removedDateMax);
                }catch(Exception e){
                    message = "If you intend specifying a removed date use the format YYYYMMDD or YYYY-MM-DD.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }
        }

        //Check if satellite was specified
        if(!satellite.equals(""))
            sq.setSatellites(new ArrayList<>(Arrays.asList(satellite.split(","))));
            //sq.setSatellite(satellite);


        //Check if altitude was specified
        if(minAlt != -10000.0){
            sq.setMinAlt(minAlt);
        }

        if(maxAlt != 20000.0){
            sq.setMaxAlt(maxAlt);
        }


        //Check altitude, longitude and latitude isolated
        if(!latitude.equals("")){
            try{
                String[] latitude_values = latitude.split(",");
                if(latitude_values.length==2){
                    if(latitude_values[0].length()!=0 && latitude_values[1].length()!=0){
                        sq.setMinLat(Float.parseFloat(latitude_values[0]));
                        sq.setMaxLat(Float.parseFloat(latitude_values[1]));
                    }else if(latitude_values[0].length()==0 && latitude_values[1].length()!=0){
                        sq.setMaxLat(Float.parseFloat(latitude_values[1]));
                    }    
                }else if(latitude_values[0].length()!=0){
                    sq.setMinLat(Float.parseFloat(latitude_values[0]));
                }
                
            }catch(IndexOutOfBoundsException e){
                message = "Latitude parameter is incorrect.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }catch(NumberFormatException e){
                message = "Latitude parameter can't be parsed.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }

        if(!longitude.equals("")){
            try{
                String[] longitude_values = longitude.split(",");
                if(longitude_values.length==2){
                    if(longitude_values[0].length()!=0 && longitude_values[1].length()!=0){
                        sq.setMinLon(Float.parseFloat(longitude_values[0]));
                        sq.setMaxLon(Float.parseFloat(longitude_values[1]));
                    }else if(longitude_values[0].length()==0 && longitude_values[1].length()!=0){
                        sq.setMaxLon(Float.parseFloat(longitude_values[1]));
                    }    
                }else{
                    sq.setMinLon(Float.parseFloat(longitude_values[0]));
                }
            }catch(IndexOutOfBoundsException e){
                message = "Longitude parameter is incorrect.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }catch(NumberFormatException e){
                message = "Longitude parameter can't be parsed.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }

        if(!altitude.equals("")){
            try{
                String[] altitude_values = altitude.split(",");
                if(altitude_values.length==2){
                    if(altitude_values[0].length()!=0 && altitude_values[1].length()!=0){
                        sq.setMinAlt(Float.parseFloat(altitude_values[0]));
                        sq.setMaxAlt(Float.parseFloat(altitude_values[1]));
                    }else if(altitude_values[0].length()==0 && altitude_values[1].length()!=0){
                        sq.setMaxAlt(Float.parseFloat(altitude_values[1]));
                    }    
                }else{
                    sq.setMinAlt(Float.parseFloat(altitude_values[0]));
                }
            }catch(IndexOutOfBoundsException e){
                message = "Altitude parameter is incorrect.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }catch(NumberFormatException e){
                message = "Altitude parameter can't be parsed.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }


        //Check if inverted network was specified
        if(!invertedNetworks.equals(""))
            sq.setInvertedNetworks(new ArrayList<>(Arrays.asList(invertedNetworks.split(","))));

        //Check for station lifetime
        if(lifetime!=0){
            sq.setLifetime(lifetime);
        }

        if(!geoJSON.isEmpty()) {
            String gjsonpolygon = "";

            ObjectMapper mapper = new ObjectMapper();
            JsonFactory factory = mapper.getFactory();
            JsonParser jsonParser = factory.createParser(geoJSON);
            JsonNode node = mapper.readTree(jsonParser);

            if (!node.has("features")){
                message = "Parsing of geoJSON failed, doesn't contain 'features'.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }

            if (!node.get("features").get(0).has("geometry")){
                message = "Parsing of geoJSON failed, doesn't contain feature->'geometry'.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }

            if(!node.get("features").get(0).get("geometry").has("type")){
                message = "Parsing of geoJSON failed, doesn't contain feature->geometry->'type'.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }

            if(!node.get("features").get(0).get("geometry").has("coordinates")){
                message = "Parsing of geoJSON failed, doesn't contain feature->geometry->'coordinates'.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }

            if(!String.valueOf(node.get("features").get(0).get("geometry").get("type")).equalsIgnoreCase("polygon")){

                try {
                    ArrayNode gjsonCoords = (ArrayNode) node.get("features").get(0).get("geometry").get("coordinates").get(0);

                    for(int i=0; i<gjsonCoords.size(); i++) {
                        if (gjsonCoords.get(i).size() % 2 != 0) {
                            message = "Parsing of geoJSON failed, coordinates mal formed. Use [lon,lat].";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                    }

                    if(!gjsonCoords.get(0).equals(gjsonCoords.get(gjsonCoords.size()-1))){
                        message = "Parsing of geoJSON failed, last point should be equal to first point.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }


                    for(int i=0; i < gjsonCoords.size()-1; i++){
                        gjsonpolygon+= gjsonCoords.get(i).get(1) + "," + gjsonCoords.get(i).get(0);
                        if(i<gjsonCoords.size()-2){
                            gjsonpolygon+=";";
                        }
                    }
                    return getStationCoordinatesV2("polygon", size, format, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, gjsonpolygon);

                }catch(Exception e){
                    e.printStackTrace();
                }
            }else{
                message = "Parsing of geoJSON failed, value of key feature->geometry->'type' is not 'polygon'.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }

            message = "Couldn't parse geoJSON.";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }

        //T2
        //Check for results date range
        if(!dateRange.equals("")){
            if(dateRange.contains(",")) {
                sq.setT2DateStart(dateRange.split(",")[0]);
                sq.setT2DateEnd(dateRange.split(",")[1]);
            }
            else{
                sq.setT2DateStart(dateRange);
            }
        }

        //Check for sampling window
        if(!samplingWindow.equals(""))
            sq.setT2SamplingWindow(samplingWindow);

        //Check for sampling frequency
        if(!samplingFrequency.equals(""))
            sq.setT2SamplingFrequency(samplingFrequency);

        //Check for file type
        if(!fileType.equals(""))
            sq.setT2FileType(fileType);

        //Check for data availability
        if(!dataAvailability.equals(""))
            sq.setT2DataAvailability(dataAvailability);

        //Check for minimum observation days
        if(minimumObservationDays!=0)
            sq.setT2minimumObservationDays(minimumObservationDays);


        //Check for minimum observation years
        if(minimumObservationYears!=0)
            sq.setT2minimumObservationYears(minimumObservationYears);

        //Check for status file
        if(statusfile!=-10)
            sq.setT2statusfile(statusfile);

       //T3
       //Check for observation type
        if(!observationtype.equals(""))
            sq.setT3Observationtype(new ArrayList<>(Arrays.asList(observationtype.split(","))));
        //Check for frequency
        if(!frequency.equals(""))
            sq.setT3Frequency(new ArrayList<>(Arrays.asList(frequency.split(","))));
        //Check for channel
        if(!channel.equals(""))
            sq.setT3Channel(new ArrayList<>(Arrays.asList(channel.split(","))));
        //Check for constellation
        if(!constellation.equals(""))
            sq.setT3Constellation(new ArrayList<>(Arrays.asList(constellation.split(","))));
        //Check for ratio epoch
        if(ratioepoch!=0.0)
            sq.setT3Ratioepoch(ratioepoch);
        //Check for elevation angle
        if(elevangle!=0.0)
            sq.setT3Elevangle(elevangle);
        //Check for multipath value
        if(multipathvalue!=0.0)
            sq.setT3Multipathvalue(multipathvalue);
        //Check for nb cycle slips
        if(nbcycleslips!=0)
            sq.setT3Nbcycleslips(nbcycleslips);
        //Check for nb clock jumps
        if(nbclockjumps!=0)
            sq.setT3Nbclockjumps(nbclockjumps);
        //Check for spp rms
        if(spprms!=0.0)
            sq.setT3Spprms(spprms);
    /*
        //Rectangle is chosen
        if(coordinates.equals("rectangle")){
            //But rectangle necessary fields aren't
            if(minLat == -999 && maxLat == -999 && minLon == -999 && maxLon == -999) {
                message = "If you intend an area of a rectangle, fill the coordinates parameter with the value of 'rectangle'.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }


        //Circle is chosen
        if(coordinates.equals("circle")) {
            //Values for points of circle but circle isn't chosen as coordinates type
            if (centerLat == -999 && centerLon == -999 && radius == -999) {
                message = "If you intend an area of a circle, fill the coordinates parameter with the value of 'circle'.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }

        //Polygon is chosen
        if(coordinates.equals("polygon")) {
            //Values for points of polygon but polygon isn't chosen as coordinates type
            if (polygonString.split(";").length % 2 != 0){
                message = "The polygon parameter is badly formed.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    */
        //Check if the coordinates value is correctly chosen. Only 1 type and it's attributes can be chosen
        if(!coordinates.equals("")) {

            switch (coordinates) {
                case "rectangle":
                    if (minLat == -999 || maxLat == -999 || minLon == -999 || maxLon == -999) {
                        message = "If you intend the area in the format of a rectangle, please specify minLat, maxLat, minLon and maxLon as optional parameters.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    } else {

                        if (minLat < -90.0 || minLat > 90.0) {
                            message = "The value of minLat should be between -90.0 and 90.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        } else if (maxLat < -90.0 || maxLat > 90.0) {
                            message = "The value of maxLat should be between -90.0 and 90.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        } else if (minLon < -180.0 || minLon > 180.0) {
                            message = "The value of minLon should be between -180.0 and 180.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        } else if (maxLon < -180.0 || maxLon > 180.0) {
                            message = "The value of maxLon should be between -180.0 and 180.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        } else if (minLon >= maxLon) {
                            message = "The value of minLon should be lower then maxLon";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        } else if (minLat >= maxLat) {
                            message = "The value of minLon should be lower then maxLon";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        ArrayList<Integer> allCords;
                        allCords = dbcv2.getCoordinatesRectangleV2(minLat, minLon, maxLat, maxLon);
                        sq=setStationCoordinatesRectangle(sq, allCords);
/*
                        IDs = dbcv2.getCoordinatesRectangleV2(minLat, minLon, maxLat, maxLon);
                        aux = new HashSet<>(IDs);
                        if(firstCondition) {
                            idHashset = aux;
                            //firstCondition = false; //Since this is the last condition.
                        }
                        else
                            idHashset.retainAll(aux);
*/
                        break;
                    }
                case "circle":
                    if (centerLat == -999 || centerLon == -999 || radius == -999) {
                        message = "If you intend the area in the format of a circle, please specify centerLat, centerLon and radius.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    } else {
                        if (centerLat < -90.0 || centerLat > 90.0) {
                            message = "The value of centerLat should be between -90.0 and 90.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        if (centerLon < -180.0 || centerLon > 180.0) {
                            message = "The value of centerLon should be between -180.0 and 180.0";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        if (radius <= 0.0) {
                            message = "The value of the radius should be a positive value in km.";
                            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                        }
                        ArrayList<Integer> allCords;
                        allCords = dbcv2.getCoordinatesCircleV2(centerLat, centerLon, radius);
                        sq=setStationCoordinatesCircle(sq, allCords);

/*
                        IDs = dbcv2.getCoordinatesCircleV2(centerLat, centerLon, radius);
                        aux = new HashSet<>(IDs);
                        if(firstCondition) {
                            idHashset = aux;
                            //firstCondition = false; //Since this is the last condition.
                        }
                        else
                            idHashset.retainAll(aux);
*/
                        break;
                    }
                case "polygon":
                    ArrayList<Coordinates> poly = new ArrayList<>();
                    poly = ParsersV2.parsePolygonV2(polygonString);

                    ArrayList<Integer> allCords;
                    allCords = dbcv2.getInsidePolygonIDs(poly);
                    sq=setStationCoordinatesPolygon(sq, allCords);

                    /*
                    IDs = dbcv2.getCoordinatesPolygonV2(latA, latB, latC, latD, latE, lonA, lonB, lonC, lonD, lonE);
                    aux = new HashSet<>(IDs);
                    if(firstCondition) {
                        idHashset = aux;
                        //firstCondition = false; //Since this is the last condition.
                    }
                    else
                        idHashset.retainAll(aux);
                    */
                    break;
                default: {
                    message = "Wrong format. Use circle, polygon or rectangle.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }
        }
        else{
            if (minLat >= -90.0 && minLat <= 90.0) {
                sq.setMinLat(minLat);
            } else {
                if(minLat != -999){
                    message = "The value of minLat should be between -90.0 and 90.0";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }

            if (maxLat >= -90.0 && maxLat <= 90.0) {
                sq.setMaxLat(maxLat);
            } else {
                if(maxLat != -999){
                    message = "The value of maxLat should be between -90.0 and 90.0";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }

            if (minLon >= -180.0 && minLon <= 180.0) {
                sq.setMinLon(minLon);
            } else {
                if(minLon != -999) {
                    message = "The value of minLon should be between -180.0 and 180.0";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }

            if (maxLon >= -180.0 && maxLon <= 180.0) {
                sq.setMaxLon(maxLon);
            } else {
                if(maxLon != -999) {
                    message = "The value of maxLon should be between -180.0 and 180.0";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }
            }
        }


        ArrayList<Station> res;

        switch(size){
            case "short" :
                switch(format){
                    //SHORT CSV MARKER
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String csv = parser.getStationShortV2CSV(res);

                        String fileName = "stations_combination_short.csv";
                        File file = new File(fileName);
                        FileWriter fw =     new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(csv);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();

                    //SHORT JSON MARKER
                    case "json" :
                        //Get the stations array
                        res = dbcv2.getStationsShortV2(sq);

                        //Parse and return it
                        String json = parser.getStationShortV2JSON(res).toString();
                        return Response.ok(json, MediaType.APPLICATION_JSON).build();

                    //Bad request
                    default: {
                        message = "Wrong short output type. Use csv or json.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }

            case "full" :
                sq.setFullV(true);
                switch(format){
                    //FULL CSV
                    case "csv" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String result = parser.getStationFullV2CSV(res);

                        String fileName = "stations_combination_full";
                        File file = new File(fileName);
                        FileWriter fw = new FileWriter(file.getAbsoluteFile());
                        BufferedWriter bw = new BufferedWriter(fw);
                        bw.write(result);
                        bw.close();

                        return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                                .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                                .build();
                    //FULL JSON
                    case "json" :
                        //Get the stations array
                         res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String csv = parser.getStationFullV2JSON(res).toString();
                        return Response.ok(csv, MediaType.APPLICATION_JSON).build();

                    //FULL XML
                    case "xml" :
                        //Get the stations array
                        res = dbcv2.getStationsFullV2(sq);

                        //Parse and return it
                        String xml = parser.getStationFullV2XML(res);

                        return Response.ok(xml, APPLICATION_XML).build();

                    //Bad request
                    default: {
                        message = "Wrong full output type. Use csv, json or xml.";
                        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                    }
                }
                //Bad request
            default: {
                message = "Wrong output size. Use full or short.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
    }

    //They usually follow the standard format of:
    //
    //bbox = left,bottom,right,top
    //bbox = min Longitude , min Latitude , max Longitude , max Latitude
    /**
     * <b>GET STATION BOUNDING BOX</b> - Returns all the stations that are inside the bounding box in multiple outputs
     *
     * <p>
     * Please specify the output format as one of the options below in headers:
     * <ul>
     * <li>APPLICATION/JSON</li> - json output
     * <li>APPLICATION/CSV or APPLICATION_OCTET_STREAM</li> - csv output
     * </ul>
     *
     * By default the output is <b>geojson</b>(if no other is specified)
     * </p>
     *
     * Order of bounding box is left, bottom, right, top
     * Which means minLon, minLat, maxLon, maxLat
     *
     * @param maxLat (0-90)
     * @param minLat (0-90)
     * @param maxLon (0-180)
     * @param minLon (0-180)
     * @QueryParam limit(0...)
     * @QueryParam with (0,1-rinex,2-products,3-ts,4-vel)
     * @return All the stations inside the bbox
     * @author José Manteigueiro
     * @author Paul
     * Version 1.1
     * @since 18/03/2019
     * Last modified: 19/03/2019
     *
     * Paul: Introduce a limit parameter in order to limit the number of stations returned
     * Only used in ICS for now and in the parser. Could even sort the station ArrayList before passing to parser
     *
     */
    @Path("station/bbox/{minLon}/{minLat}/{maxLon}/{maxLat}")
    @GET
    public Response getStationsBBOX(@Context HttpHeaders headers,
                                       @PathParam("minLon") double minLon,
                                       @PathParam("minLat") double minLat,
                                       @PathParam("maxLon") double maxLon,
                                       @PathParam("maxLat") double maxLat,
                                       @DefaultValue("0") @QueryParam("limit") int limit,
                                       @DefaultValue("0") @QueryParam("with")  int stnsWith,
                                       @DefaultValue("ALL") @QueryParam("ac")  String analysisCenter)

    {
        List<MediaType> acceptableMediaTypes = headers.getAcceptableMediaTypes();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;
        ArrayList<Station> res;

        if(minLat < -90.0 || minLat > 90.0){
            message = "The value of minLat should be between -90.0 and 90.0";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }
        else if(maxLat < -90.0 || maxLat > 90.0){
            message = "The value of maxLat should be between -90.0 and 90.0";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }
        else if(minLon < -180.0 || minLon > 180.0){
            message = "The value of minLon should be between -180.0 and 180.0";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }
        else if(maxLon < -180.0 || maxLon > 180.0){
            message = "The value of maxLon should be between -180.0 and 180.0";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }
        else if(minLon >= maxLon){
            message = "The value of minLon should be lower then maxLon";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }
        else if(minLat >= maxLat){
            message = "The value of minLon should be lower then maxLon";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }
        ArrayList<Integer> allCords;
        allCords = dbcv2.getCoordinatesRectangleV2(minLat, minLon, maxLat, maxLon);
        sq = setStationCoordinatesRectangle(sq, allCords);

        if (stnsWith>0)
            sq.setStationsWith(stnsWith);

        if (!analysisCenter.equals("ALL"))
            sq.setProductACAbr(analysisCenter);

        //Get the stations array
        res = dbcv2.getStationsShortV2(sq);
        if(res.isEmpty()) {
            message = "No stations are within the given bounding box with the specified parameters.";
            return Response.status(Response.Status.NO_CONTENT).entity(message).build();
        }

        String mediatype = acceptableMediaTypes.get(0).toString().toLowerCase();
        try{
            //Parse and return it
            if(mediatype.contains("csv") || mediatype.contains("octet")){
                String csv = parser.getStationShortV2CSV(res);
                String fileName = "bbox.csv";
                File file = new File(fileName);
                FileWriter fw = new FileWriter(file.getAbsoluteFile());
                BufferedWriter bw = new BufferedWriter(fw);
                bw.write(csv);
                bw.close();
                return Response.ok(file, APPLICATION_OCTET_STREAM + ";charset=utf-8")
                        .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"") //optional
                        .build();


            }
            else if(mediatype.equals("application/json")){
                String json = parser.getStationShortV2JSON(res).toString();
                return Response.ok(json, APPLICATION_JSON).build();
            }else {
                //for integration at ICS
                String geojson = parser.getStationShortV2GEOJSONics(res,limit).toString();
                return Response.ok(geojson, "application/geo+json").build();
            }

        } catch (Exception e) {
            e.printStackTrace();
            message = "Something went wrong.";
            return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
        }
    }


    /**
     * <b>GET STATION GEOJSON</b> - Returns all the stations that are inside the geometry of geoJSON input
     *
     * <p>
     *     Input can be created using http://geojson.io
     * </p>
     *
     * <p>
     * Please specify the output format as one of the options below:
     * <ul>
     * <li>geojson</li>
     * <li>json</li>
     * </ul>
     * </p>
     *
     *
     * @param output_format output format of the request
     * @param geoJSON geoJSON with geometry
     * @return All the stations in the expected output format
     * @author José Manteigueiro
     * Version 1.0
     * @since 26/11/2018
     * Last modified: 26/11/2018
     */
    @Path("station/geojson/{output_format}/{geoJSON}")
    @GET
    public Response getStationsGeoJSON(@PathParam("geoJSON") String geoJSON, @PathParam("output_format") String output_format) {

        output_format = output_format.toLowerCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        ParsersV2 parser = new ParsersV2();
        StationQuery sq = new StationQuery();
        String message;

        ArrayList<Station> res;


        if(!geoJSON.isEmpty()) {
            ObjectMapper mapper = new ObjectMapper();
            JsonFactory factory = mapper.getFactory();
            JsonParser jsonParser = null;
            try {
                jsonParser = factory.createParser(geoJSON);
                assert jsonParser != null;
                JsonNode node = mapper.readTree(jsonParser);

                if (!node.has("features")){
                    message = "Parsing of geoJSON failed, doesn't contain 'features'.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }

                if (!node.get("features").get(0).has("geometry")){
                    message = "Parsing of geoJSON failed, doesn't contain feature->'geometry'.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }

                if(!node.get("features").get(0).get("geometry").has("type")){
                    message = "Parsing of geoJSON failed, doesn't contain feature->geometry->'type'.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }

                if(!node.get("features").get(0).get("geometry").has("coordinates")){
                    message = "Parsing of geoJSON failed, doesn't contain feature->geometry->'coordinates'.";
                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                }

                if(!String.valueOf(node.get("features").get(0).get("geometry").get("type")).equalsIgnoreCase("polygon")){

                    try {
                        JsonNode jnode = node.get("features");
                        ArrayList<Station> results = new ArrayList<>();
                        for(int j=0; j < jnode.size(); j++){
                            ArrayNode gjsonCoords = (ArrayNode) jnode.get(j).get("geometry").get("coordinates").get(0);

                            for(int i=0; i<gjsonCoords.size(); i++) {
                                if (gjsonCoords.get(i).size() % 2 != 0) {
                                    message = "Parsing of geoJSON failed, coordinates mal formed. Use [lon,lat].";
                                    return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                                }
                            }

                            if(!gjsonCoords.get(0).equals(gjsonCoords.get(gjsonCoords.size()-1))){
                                message = "Parsing of geoJSON failed, last point should be equal to first point.";
                                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
                            }

                            String gjsonpolygon = "";
                            for(int i=0; i < gjsonCoords.size()-1; i++){
                                gjsonpolygon+= gjsonCoords.get(i).get(1) + "," + gjsonCoords.get(i).get(0);
                                if(i<gjsonCoords.size()-2){
                                    gjsonpolygon+=";";
                                }
                            }
                            ArrayList<Station> auxResults = getStationsArrayListCoordinates("polygon", "short", 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, gjsonpolygon);
                            ArrayList<Integer> idsArray = new ArrayList<>();
                            for(int k=0; k<results.size(); k++){
                                idsArray.add(results.get(k).getId());
                            }

                            for (Station auxResult : auxResults) {
                                if(!idsArray.contains(auxResult.getId()))
                                    results.add(auxResult);
                            }
                        }
                        if(output_format.equals("json")){
                            //Parse and return it
                            String json = parser.getStationShortV2JSON(results).toString();
                            return Response.ok(json, MediaType.APPLICATION_JSON).build();
                        }
                        else if(output_format.equals("geojson")){
                            //Parse and return it
                            String geojson = parser.getStationShortV2GEOJSON(results).toString();
                            return Response.ok(geojson, MediaType.APPLICATION_JSON).build();
                        }
                    }catch(Exception e){
                        e.printStackTrace();
                    }
                }

                message = "Couldn't parse geoJSON.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            } catch (IOException e) {
                e.printStackTrace();
                message = "Couldn't parse geoJSON.";
                return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
            }
        }
        message = "Couldn't parse geoJSON.";
        return Response.status(Response.Status.BAD_REQUEST).entity(message).build();
    }

    private ArrayList<Station> getStationsArrayListCoordinates(String area,
                                                     String size,
                                                     double centerLat,
                                                     double centerLon,
                                                     double radius,
                                                     double minLat,
                                                     double maxLat,
                                                     double minLon,
                                                     double maxLon,
                                                     String polygonString){
        size = size.toLowerCase();
        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        StationQuery sq = new StationQuery();

        ArrayList<Station> res;

        switch(area) {
            case "rectangle":
                if(minLat == -999 || maxLat == -999 || minLon == -999 || maxLon == -999 ){
                    return new ArrayList<>();
                }
                else{

                    if(minLat < -90.0 || minLat > 90.0){
                        return new ArrayList<>();
                    }
                    else if(maxLat < -90.0 || maxLat > 90.0){
                        return new ArrayList<>();
                    }
                    else if(minLon < -180.0 || minLon > 180.0){
                        return new ArrayList<>();
                    }
                    else if(maxLon < -180.0 || maxLon > 180.0){
                        return new ArrayList<>();
                    }
                    else if(minLon >= maxLon){
                        return new ArrayList<>();
                    }
                    else if(minLat >= maxLat){
                        return new ArrayList<>();
                    }
                    ArrayList<Integer> allCords;
                    allCords = dbcv2.getCoordinatesRectangleV2(minLat, minLon, maxLat, maxLon);
                    sq=setStationCoordinatesRectangle(sq, allCords);
                    break;
                }
            case "circle":
                if(centerLat == -999 || centerLon == -999 || radius == -999){
                    return new ArrayList<>();
                }
                else {
                    if(centerLat < -90.0 || centerLat > 90.0) {
                        return new ArrayList<>();
                    }
                    if(centerLon < -180.0 || centerLon > 180.0) {
                        return new ArrayList<>();
                    }
                    if(radius <= 0.0){
                        return new ArrayList<>();
                    }
                    ArrayList<Integer> allCords;
                    allCords = dbcv2.getCoordinatesCircleV2(centerLat, centerLon, radius);
                    sq=setStationCoordinatesCircle(sq, allCords);

                    break;
                }
            case "polygon":
                ArrayList<Coordinates> poly = ParsersV2.parsePolygonV2(polygonString);

                ArrayList<Integer> allCords;
                allCords = dbcv2.getInsidePolygonIDs(poly);
                sq=setStationCoordinatesPolygon(sq, allCords);

                break;
            default : {
                return new ArrayList<>();
            }
        }


        switch (size) {
            case "full":{
                sq.setFullV(true);
                res = dbcv2.getStationsFullV2(sq);
                return res;
            }
            case "short":{
                res = dbcv2.getStationsShortV2(sq);
                return res;
            }
            default: {
                return new ArrayList<>();
            }
        }
    }

}