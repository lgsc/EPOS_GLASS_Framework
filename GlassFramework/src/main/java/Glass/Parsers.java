/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Glass;

import CustomClasses.StationQuery;
import CustomClasses.VelocityByPlateAndAc;
import EposTables.*;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;

import javax.json.*;
import java.net.URI;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 *
 * @author crocker
 */
public class Parsers {



    // *************************************************************************
    // *************************************************************************
    //
    // T0 PARSER METHODS
    //
    // *************************************************************************
    // *************************************************************************

    /*
     *   GET NODES JSON
     *   Returns a JSON with all nodes.
     *   */
    public JsonArray getNodesJson(ArrayList<Node> nodesData) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Node node : nodesData) {
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("NodeID", node.getId());
            jsonObjectBuilder.add("NodeName", node.getName());
            jsonObjectBuilder.add("NodeHost", node.getHost());
            jsonObjectBuilder.add("NodePort", node.getPort());
            jsonObjectBuilder.add("NodeDbname", node.getDbname());
            jsonObjectBuilder.add("NodeUsername", node.getUsername());
            jsonObjectBuilder.add("NodePassword", "");
            if(node.getContact_name() == null) {
                jsonObjectBuilder.add("NodeContactName", "");
            }
            else {
                jsonObjectBuilder.add("NodeContactName", node.getContact_name());
            }
            if(node.getUrl() == null) {
                jsonObjectBuilder.add("NodeUrl", "");
            }
            else {
                jsonObjectBuilder.add("NodeUrl", node.getUrl());
            }
            if(node.getEmail() == null) {
                jsonObjectBuilder.add("NodeEmail", "");
            }
            else {
                jsonObjectBuilder.add("NodeEmail", node.getEmail());
            }

            builder.add(jsonObjectBuilder);
        }

        return builder.build();
    }


    /*
     *   GET Failed Queries JSON
     *   Returns a JSON with failed queries.
     *   */
    public JsonArray getFailedQueriesJSON(ArrayList<Failed_queries> failed_queries) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for(Failed_queries f: failed_queries) {
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("fID", f.getId());
            jsonObjectBuilder.add("fQuery", f.getQuery());
            JsonObjectBuilder jsonObjectBuilder2 = Json.createObjectBuilder();
            jsonObjectBuilder2.add("NodeID", f.getDestiny().getId());
            jsonObjectBuilder2.add("NodeName", f.getDestiny().getName());
            jsonObjectBuilder2.add("NodeHost", f.getDestiny().getHost());
            jsonObjectBuilder2.add("NodePort", f.getDestiny().getPort());
            jsonObjectBuilder2.add("NodeDbname", f.getDestiny().getDbname());
            jsonObjectBuilder2.add("NodeUsername", f.getDestiny().getUsername());
            jsonObjectBuilder2.add("NodePassword", "");
            if(f.getDestiny().getContact_name() == null) {
                jsonObjectBuilder2.add("NodeContactName", "");
            }
            else {
                jsonObjectBuilder2.add("NodeContactName", f.getDestiny().getContact_name());
            }
            if(f.getDestiny().getUrl() == null) {
                jsonObjectBuilder2.add("NodeUrl", "");
            }
            else {
                jsonObjectBuilder2.add("NodeUrl", f.getDestiny().getUrl());
            }
            if(f.getDestiny().getEmail() == null) {
                jsonObjectBuilder2.add("NodeEmail", "");
            }
            else {
                jsonObjectBuilder2.add("NodeEmail", f.getDestiny().getEmail());
            }
            jsonObjectBuilder.add("nodeDestiny", jsonObjectBuilder2);

            DateFormat df = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
            String timeStampString = df.format(f.getTimeStamp());
            jsonObjectBuilder.add("fTimeStamp", timeStampString);
            jsonObjectBuilder.add("fReason", f.getReason());
            builder.add(jsonObjectBuilder);
        }

        return builder.build();
    }


    /*
     *   GET connections JSON
     *   Returns a JSON with all connections.
     *   */
    public JsonArray getConnectionsJSON(ArrayList<Connections> connectionsArrayList) {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for(Connections c: connectionsArrayList){
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("id",c.getId());
            // source node
            JsonObjectBuilder jsonObjectBuilderN  = Json.createObjectBuilder();
            jsonObjectBuilderN.add("idS",c.getSource().getId());
            jsonObjectBuilderN.add("nameS",c.getSource().getName());
            jsonObjectBuilderN.add("hostS",c.getSource().getHost());
            jsonObjectBuilderN.add("portS",c.getSource().getPort());
            jsonObjectBuilderN.add("dbnameS",c.getSource().getDbname());
            jsonObjectBuilderN.add("userS",c.getSource().getUsername());
            jsonObjectBuilderN.add("passS", "");
            if(c.getSource().getContact_name() == null) {
                jsonObjectBuilderN.add("contactS", "");
            }
            else {
                jsonObjectBuilderN.add("contactS", c.getSource().getContact_name());
            }
            if(c.getSource().getUrl() == null) {
                jsonObjectBuilderN.add("urlS", "");
            }
            else {
                jsonObjectBuilderN.add("urlS", c.getSource().getUrl());
            }
            if(c.getSource().getEmail() == null) {
                jsonObjectBuilderN.add("emailS", "");
            }
            else {
                jsonObjectBuilderN.add("emailS", c.getSource().getEmail());
            }

            JsonArrayBuilder arrayBuilderDcSource = Json.createArrayBuilder();
            for (Data_center dc : c.getSource().getData_centers()) {
                JsonObjectBuilder jsonObjectBuilderDC = Json.createObjectBuilder();
                jsonObjectBuilderDC.add("idDC", dc.getId());
                jsonObjectBuilderDC.add("nameDC", dc.getName());

                arrayBuilderDcSource.add(jsonObjectBuilderDC);
            }
            jsonObjectBuilderN.add("data_centers", arrayBuilderDcSource);

            jsonObjectBuilder.add("source",jsonObjectBuilderN);

            //destiny node
            JsonObjectBuilder jsonObjectBuilderN1  = Json.createObjectBuilder();
            jsonObjectBuilderN1.add("idD",c.getDestiny().getId());
            jsonObjectBuilderN1.add("nameD",c.getDestiny().getName());
            jsonObjectBuilderN1.add("hostD",c.getDestiny().getHost());
            jsonObjectBuilderN1.add("portD",c.getDestiny().getPort());
            jsonObjectBuilderN1.add("dbnameD",c.getDestiny().getDbname());
            jsonObjectBuilderN1.add("userD",c.getDestiny().getUsername());

            jsonObjectBuilderN1.add("passD", "");
            if(c.getDestiny().getContact_name() == null) {
                jsonObjectBuilderN1.add("contactD", "");
            }
            else {
                jsonObjectBuilderN1.add("contactD", c.getDestiny().getContact_name());
            }
            if(c.getDestiny().getUrl() == null) {
                jsonObjectBuilderN1.add("urlD", "");
            }
            else {
                jsonObjectBuilderN1.add("urlD", c.getDestiny().getUrl());
            }
            if(c.getDestiny().getEmail() == null) {
                jsonObjectBuilderN1.add("emailD", "");
            }
            else {
                jsonObjectBuilderN1.add("emailD", c.getDestiny().getEmail());
            }
            // add data center to destiny node
            JsonArrayBuilder arrayBuilderDcDestiny = Json.createArrayBuilder();
            for (Data_center dc : c.getDestiny().getData_centers()) {
                JsonObjectBuilder jsonObjectBuilderDC = Json.createObjectBuilder();
                jsonObjectBuilderDC.add("idDC", dc.getId());
                jsonObjectBuilderDC.add("nameDC", dc.getName());

                arrayBuilderDcDestiny.add(jsonObjectBuilderDC);
            }
            jsonObjectBuilderN1.add("data_centers", arrayBuilderDcDestiny);


            jsonObjectBuilder.add("destiny",jsonObjectBuilderN1);

            // node station
            if (c.getStation().getId() != 0) {
                JsonObjectBuilder station  = Json.createObjectBuilder();
                station.add("stationID",c.getStation().getId());
                station.add("stationName",c.getStation().getName());

                jsonObjectBuilder.add("station",station);
            }
            jsonObjectBuilder.add("metadata", c.getMetadata());

            builder.add(jsonObjectBuilder);
        }

        return builder.build();
    }


    /*
     *   GET stations JSON
     *   Returns a JSON with stations.
     *   */
    public JsonArray getStationsJSON(ArrayList<Station> resultS) {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        for(Station s: resultS) {
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("StationID", s.getId());
            jsonObjectBuilder.add("StationName", s.getName());
            jsonObjectBuilder.add("markerLongName", s.getMarkerLongName());
            builder.add(jsonObjectBuilder);
        }

        return builder.build();
    }


    /*
     *   GET Daata center JSON
     *   Returns a JSON with all data centers.
     *   */
    public JsonArray getDataCenter(ArrayList<Data_center> arraDc) {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        for(Data_center dc: arraDc) {
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("Dc_ID", dc.getId());
            jsonObjectBuilder.add("Dc_Name", dc.getName());
            jsonObjectBuilder.add("Dc_Acronym", dc.getAcronym());
            builder.add(jsonObjectBuilder);
        }


        return builder.build();
    }


    /*
     *   GET stations by DataCenter id JSON
     *   Returns a JSON with all stations which have the data center id.
     *   */
    public JsonArray getStationByDataCenterID(ArrayList<Station> arraStations) {
        JsonArrayBuilder builder = Json.createArrayBuilder();


        for(Station s: arraStations){
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("StationID", s.getId());
            jsonObjectBuilder.add("StationName", s.getName());
            jsonObjectBuilder.add("markerLongName", s.getMarkerLongName());
            builder.add(jsonObjectBuilder);
        }


        return builder.build();
    }

    public JsonArray getNodeDc(ArrayList<String[]> data) {
        JsonArrayBuilder builder = Json.createArrayBuilder();

        for(String[] c : data) {
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("nodeID", c[0]);
            jsonObjectBuilder.add("nodeName", c[1]);
            jsonObjectBuilder.add("dcID", c[2]);
            jsonObjectBuilder.add("dcName", c[3]);


            builder.add(jsonObjectBuilder);
        }

        return builder.build();

    }

    public JsonObject getDCbyID(Data_center_structure dc) {
        JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
        jsonObjectBuilder.add("dcID", dc.getDataCenter().getId());
        jsonObjectBuilder.add("name", dc.getDataCenter().getName());
        jsonObjectBuilder.add("acronym", dc.getDataCenter().getAcronym());
        jsonObjectBuilder.add("hostname", dc.getDataCenter().getHostname());
        jsonObjectBuilder.add("rootPath", dc.getDataCenter().getRoot_path());
        jsonObjectBuilder.add("protocol", dc.getDataCenter().getProtocol());

        JsonObjectBuilder agencyObjectBuilder = Json.createObjectBuilder();
        agencyObjectBuilder.add("agID", dc.getDataCenter().getAgency().getId());
        agencyObjectBuilder.add("agName", dc.getDataCenter().getAgency().getName());

        jsonObjectBuilder.add("agency", agencyObjectBuilder);

        JsonArrayBuilder dirStructs = Json.createArrayBuilder();
        for (DataBaseT0Connection.DirStruct dirStruct: dc.getDirStructs()) {
            JsonObjectBuilder dirStructObjectBuilder = Json.createObjectBuilder();
            dirStructObjectBuilder.add("dirName", dirStruct.getDirname());
            dirStructObjectBuilder.add("dcsID", dirStruct.getId());
            dirStructObjectBuilder.add("fileID", dirStruct.getFileType().getId());
            dirStructObjectBuilder.add("format", dirStruct.getFileType().getFormat());
            dirStructObjectBuilder.add("sampFreq", dirStruct.getFileType().getSampling_frequency());
            dirStructObjectBuilder.add("sampWindow", dirStruct.getFileType().getSampling_window());
            dirStructs.add(dirStructObjectBuilder);
        }

        jsonObjectBuilder.add("dirStructs", dirStructs);

        JsonArrayBuilder nodes = Json.createArrayBuilder();
        for (Node n : dc.getNodeArrayList()){
            JsonObjectBuilder objectBuilderNodes = Json.createObjectBuilder();
            objectBuilderNodes.add("idNode", n.getId());
            objectBuilderNodes.add("nodeName", n.getName());
            nodes.add(objectBuilderNodes);
        }

        jsonObjectBuilder.add("nodes", nodes);

        return  jsonObjectBuilder.build();
    }

    public JsonArray getAgencyT0JSON(ArrayList<Agency> data)
    {
        JsonArrayBuilder builder = Json.createArrayBuilder();
        for (Agency ag: data){
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("id", ag.getId());
            jsonObjectBuilder.add("name", ag.getName());
            jsonObjectBuilder.add("abbreviation", ag.getAbbreviation());
            builder.add(jsonObjectBuilder);
        }


        return builder.build();
    }

    public JsonArray getFileTypeT0JSON(ArrayList<File_type> fileTypes) {
        JsonArrayBuilder arrayBuilder = Json.createArrayBuilder();

        for (File_type f: fileTypes){
            JsonObjectBuilder jsonObjectBuilder = Json.createObjectBuilder();
            jsonObjectBuilder.add("id", f.getId());
            jsonObjectBuilder.add("format", f.getFormat());
            jsonObjectBuilder.add("sampling_window", f.getSampling_window());
            jsonObjectBuilder.add("sampling_frequency", f.getSampling_frequency());
            arrayBuilder.add(jsonObjectBuilder);
        }

        return arrayBuilder.build();
    }

    public JsonArray getStationsForDataCenter(HashMap<Data_center, ArrayList<Station>> hashData) {
        JsonArrayBuilder jsonArrayBuilder = Json.createArrayBuilder();

        for (Map.Entry<Data_center, ArrayList<Station>> entry: hashData.entrySet()) {
            Data_center dc = entry.getKey();
            ArrayList<Station> stationArrayList = entry.getValue();
            JsonObjectBuilder dcBuilder = Json.createObjectBuilder();

            dcBuilder.add("dcID", dc.getId());
            dcBuilder.add("acronym", dc.getAcronym());
            dcBuilder.add("hostname", dc.getHostname());
            dcBuilder.add("rootPath", dc.getRoot_path());
            dcBuilder.add("dcName", dc.getName());
            dcBuilder.add("protocol", dc.getProtocol());
            JsonArrayBuilder jsonArrayStations = Json.createArrayBuilder();
            for (Station st: stationArrayList) {
                JsonObjectBuilder stBuilder = Json.createObjectBuilder();
                stBuilder.add("sID", st.getId());
                stBuilder.add("sName", st.getName());
                stBuilder.add("sMarker", st.getMarker());
                jsonArrayStations.add(stBuilder);
            }
            dcBuilder.add("stations", jsonArrayStations);
            jsonArrayBuilder.add(dcBuilder);


        }
        return jsonArrayBuilder.build();

    }

    public JsonArray getQueueQueries(ArrayList<Queries> qData) {
        JsonArrayBuilder jsonArray = Json.createArrayBuilder();
        for (Queries q: qData) {
            JsonObjectBuilder jsonObject = Json.createObjectBuilder();
            jsonObject.add("idQ", q.getId());
            jsonObject.add("query", q.getQuery());
            jsonObject.add("metadata", q.getMetaData());
            if(q.getStation().getId() != 0){
                jsonObject.add("idS", q.getStation().getId());
                jsonObject.add("SName", q.getStation().getName());
                jsonObject.add("SMarker", q.getStation().getMarker());
            }
            else{
                jsonObject.add("idS", "N/A");
                jsonObject.add("SName", "N/A");
                jsonObject.add("SMarker", "N/A");
            }
            if (q.getNode().getId() != 0){
                jsonObject.add("nID", q.getNode().getId());
                jsonObject.add("nName", q.getNode().getName());
            }
            else{
                jsonObject.add("nID", "N/A");
                jsonObject.add("nName", "N/A");
            }

            jsonArray.add(jsonObject);
        }

        return jsonArray.build();
    }




    // *************************************************************************
    // *************************************************************************
    //
    // T1 PARSER METHODS
    //
    // *************************************************************************
    // *************************************************************************

    /*
    * GET SHORT CSV
    * This method returns the full data in XML.
    */
    public String getShortCSV(ArrayList<Station> data)
    {
        // Header
        StringBuilder header = new StringBuilder().append("station_name, station_marker, x, y, z, lat, lon, altitude, city, state, country, agencies, networks\n");

        StringBuilder csv = new StringBuilder();

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            csv.append(aData.getName()).append(", ").append(aData.getMarker()).append(", ");

            // *****************************************************************
            // Handle Coordinates
            // *****************************************************************
            csv.append(aData.getLocation().getCoordinates().getX());
            csv.append(aData.getLocation().getCoordinates().getY());
            csv.append(aData.getLocation().getCoordinates().getZ());
            csv.append(aData.getLocation().getCoordinates().getLat());
            csv.append(", ");
            csv.append(aData.getLocation().getCoordinates().getLon());
            csv.append(", ");
            csv.append(aData.getLocation().getCoordinates().getAltitude());
            csv.append(", ");
            csv.append(aData.getLocation().getCity().getName());
            csv.append(", ");
            csv.append(aData.getLocation().getCity().getState().getName());
            csv.append(", ");
            csv.append(aData.getLocation().getCity().getState().getCountry().getName());
            csv.append(", ");

            for (int j=0; j<aData.getStationContacts().size(); j++)
                csv.append(aData.getStationContacts().get(j).getContact().getAgency().getName());

            csv.append("\n");

        }

        // Join header
        header.append(csv);
        return header.toString();
    }



    /*
    * GET STATIONS SHORT CSV
    * This method returns stations short CSV file.
    */
    public String getStationsShortCSV(ArrayList<Station> data)
    {
        String delimiter = "\",\"";
        // Header
        StringBuilder header = new StringBuilder();
        header.append("\"");
        header.append("station_name");
        header.append(delimiter);
        header.append("station_marker");
        header.append(delimiter);
        header.append("date_from");
        header.append(delimiter);
        header.append("date_to");
        header.append(delimiter);
        header.append("x");
        header.append(delimiter);
        header.append("y");
        header.append(delimiter);
        header.append("z");
        header.append(delimiter);
        header.append("lat");
        header.append(delimiter);
        header.append("lon");
        header.append(delimiter);
        header.append("altitude");
        header.append(delimiter);
        header.append("city");
        header.append(delimiter);
        header.append("state");
        header.append(delimiter);
        header.append("country");
        header.append(delimiter);
        header.append("agencies");
        header.append(delimiter);
        header.append("networks\"\n");

        StringBuilder csv = new StringBuilder();

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            csv.append("\"");
            csv.append(aData.getName()).append(delimiter).append(aData.getMarker()).append(delimiter);

            // *****************************************************************
            // Handle Station
            // *****************************************************************
            csv.append(aData.getDate_from()).append(delimiter).append(aData.getDate_to()).append(delimiter);

            // *****************************************************************
            // Handle Coordinates and Locations
            // *****************************************************************
            csv.append(aData.getLocation().getCoordinates().getLat()).append(delimiter).append(aData.getLocation().getCoordinates().getLon()).append(delimiter).append(aData.getLocation().getCoordinates().getAltitude()).append(delimiter).append(aData.getLocation().getCity().getName()).append(delimiter).append(aData.getLocation().getCity().getState().getName()).append(delimiter).append(aData.getLocation().getCity().getState().getCountry().getName());


            for (int j=0; j<aData.getStationContacts().size(); j++) {
                csv.append(delimiter).append(aData.getStationContacts().get(j).getContact().getAgency().getName());
            }

            for (int j=0; j<aData.getStationNetworks().size(); j++) {
                csv.append(delimiter).append(aData.getStationNetworks().get(j).getNetwork().getName());
            }

            csv.append("\"\n");

        }

        // Join header
        header.append(csv);
        return header.toString();
    }


    /*
    * GET FULL CSV
    * This method returns the full data in CSV.
    */
    public String getFullCSV(ArrayList<Station> data)
    {
        // Header
        StringBuilder header = new StringBuilder();
        header.append("station_name, station_marker, station_description, station_date_from, ");
        header.append("station_date_to, station_comment, station_iers_domes, station_cpd_num, ");
        header.append("station_monument_num, station_receiver_num, ");
        header.append("station_type_name, station_type_type, ");
        header.append("city, ");
        header.append("state, ");
        header.append("country, country_iso_code, ");
        header.append("x, y, z, lat, lon, altitude, ");
        header.append("plate_name, tectonic_description, ");
        header.append("monument_description, monument_inscription, ");
        header.append("monument_height, monument_foundation, monument_foundation_depth, ");
        header.append("geological_characteristic, geological_fracture_spacing, ");
        header.append("geological_fault_zone, geological_distance_to_fault, ");
        header.append("bedrock_condition, bedrock_type, ");
        header.append("station_contact_role, contact_name, contact_title, ");
        header.append("contact_email, contact_phone, contact_gsm, ");
        header.append("contact_comment, contact_role, agency_name, ");
        header.append("agency_abbreviation, agency_address, agency_www, ");
        header.append("agency_infos\n");
        StringBuilder csv = new StringBuilder();


        for (Station aData : data) {


            // *****************************************************************
            // Handle Station
            // *****************************************************************
            csv.append(aData.getName());
            csv.append(", ");
            csv.append(aData.getMarker());
            csv.append(", ");
            csv.append(aData.getDescription());
            csv.append(", ");
            csv.append(aData.getDate_from());
            csv.append(", ");
            csv.append(aData.getDate_to());
            csv.append(", ");
            csv.append(aData.getComment());
            csv.append(", ");
            csv.append(aData.getIers_domes());
            csv.append(", ");
            csv.append(aData.getCpd_num());
            csv.append(", ");
            csv.append(aData.getMonument_num());
            csv.append(", ");
            csv.append(aData.getReceiver_num());
            csv.append(", ");

            // *****************************************************************
            // Handle Station Type
            // *****************************************************************
            csv.append(aData.getStation_type().getName());
            csv.append(", ");
            csv.append(aData.getStation_type().getType());
            csv.append(", ");

            // *****************************************************************
            // Handle City
            // *****************************************************************
            csv.append(aData.getLocation().getCity().getName()).append(", ");

            // *****************************************************************
            // Handle State
            // *****************************************************************
            csv.append(aData.getLocation().getCity().getState().getName());
            csv.append(", ");

            // *****************************************************************
            // Handle Country
            // *****************************************************************
            csv.append(aData.getLocation().getCity().getState().getCountry().getName());
            csv.append(", ");
            csv.append(aData.getLocation().getCity().getState().getCountry().getIso_code());
            csv.append(", ");

            // *****************************************************************
            // Handle Coordinates
            // *****************************************************************
            csv.append(aData.getLocation().getCoordinates().getX());
            csv.append(", ");
            csv.append(aData.getLocation().getCoordinates().getY());
            csv.append(", ");
            csv.append(aData.getLocation().getCoordinates().getZ());
            csv.append(", ");
            csv.append(aData.getLocation().getCoordinates().getLat());
            csv.append(", ");
            csv.append(aData.getLocation().getCoordinates().getLon());
            csv.append(", ");
            csv.append(aData.getLocation().getCoordinates().getAltitude());
            csv.append(", ");


            // *****************************************************************
            // Handle Tectonic
            // *****************************************************************
            csv.append(aData.getLocation().getTectonic().getPlate_name());
            csv.append(", ");
            csv.append(aData.getLocation().getDescription());
            csv.append(", ");

            // *****************************************************************
            // Handle Monument
            // *****************************************************************
            csv.append(aData.getMonument().getDescription());
            csv.append(", ");
            csv.append(aData.getMonument().getInscription());
            csv.append(", ");
            csv.append(aData.getMonument().getHeight());
            csv.append(", ");
            csv.append(aData.getMonument().getFoundation());
            csv.append(", ");
            csv.append(aData.getMonument().getFoundation_depth());
            csv.append(", ");

            // *****************************************************************
            // Handle Geological
            // *****************************************************************
            csv.append(aData.getGeological().getCharacteristic());
            csv.append(", ");
            csv.append(aData.getGeological().getFracture_spacing());
            csv.append(", ");
            csv.append(aData.getGeological().getFault_zone());
            csv.append(", ");
            csv.append(aData.getGeological().getDistance_to_fault());
            csv.append(", ");

            // *****************************************************************
            // Handle Bedrock
            // *****************************************************************
            csv.append(aData.getGeological().getBedrock().getCondition());
            csv.append(", ");
            csv.append(aData.getGeological().getBedrock().getType());
            csv.append(", ");

            // *****************************************************************
            // Handle Station Contacts
            // *****************************************************************
            if (aData.getStationContacts().size() >= 1) {
                // *************************************************************
                // Handle Station Contact
                // *************************************************************
                csv.append(aData.getStationContacts().get(0).getRole());
                csv.append(", ");

                // *************************************************************
                // Handle Contact
                // *************************************************************
                csv.append(aData.getStationContacts().get(0).getContact().getName());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getTitle());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getEmail());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getPhone());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getGsm());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getComment());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getRole());
                csv.append(", ");

                // *************************************************************
                // Handle Agency
                // *************************************************************
                csv.append(aData.getStationContacts().get(0).getContact().getAgency().getName());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getAgency().getAbbreviation());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getAgency().getAddress());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getAgency().getWww());
                csv.append(", ");
                csv.append(aData.getStationContacts().get(0).getContact().getAgency().getInfos());
                csv.append("\n");


            }
        }

        // Join header
        header.append(csv);
        return header.toString();
    }

    /*
    * GET FULL JSON
    * This method returns the full data in JSON
    */
    public JSONArray getFullJSON(ArrayList<Station> data)
    {
        JSONArray station_list = new JSONArray();

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            JSONObject station = new JSONObject();
            station.put("name", aData.getName());
            station.put("marker", aData.getMarker());
            station.put("description", aData.getDescription());
            station.put("date_from", aData.getDate_from());
            station.put("date_to", aData.getDate_to());
            station.put("comment", aData.getComment());
            station.put("iers_domes", aData.getIers_domes());
            station.put("cdp_num", aData.getCpd_num());
            station.put("monument_num", aData.getMonument_num());
            station.put("receiver_num", aData.getReceiver_num());
            station.put("country_code", aData.getCountry_code());

            // *****************************************************************
            // Handle Station Type
            // *****************************************************************
            JSONObject station_type = new JSONObject();
            station_type.put("name", aData.getStation_type().getName());
            station_type.put("type", aData.getStation_type().getType());
            station.put("station_type", station_type);

            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getLocation().getCity().getState().getCountry().getName());
            country.put("iso_code", aData.getLocation().getCity().getState().getCountry().getIso_code());

            // *****************************************************************
            // Handle State
            // *****************************************************************
            JSONObject state = new JSONObject();
            state.put("name", aData.getLocation().getCity().getState().getName());
            state.put("country", country);

            // *****************************************************************
            // Handle City
            // *****************************************************************
            JSONObject city = new JSONObject();
            city.put("name", aData.getLocation().getCity().getName());
            city.put("state", state);

            // *****************************************************************
            // Handle Coordinates
            // *****************************************************************
            JSONObject coordinates = new JSONObject();
            coordinates.put("x", aData.getLocation().getCoordinates().getX());
            coordinates.put("y", aData.getLocation().getCoordinates().getY());
            coordinates.put("z", aData.getLocation().getCoordinates().getZ());
            coordinates.put("lat", aData.getLocation().getCoordinates().getLat());
            coordinates.put("lon", aData.getLocation().getCoordinates().getLon());
            coordinates.put("altitude", aData.getLocation().getCoordinates().getAltitude());

            // *****************************************************************
            // Handle Tectonic
            // *****************************************************************
            JSONObject tectonic = new JSONObject();
            tectonic.put("plate_name", aData.getLocation().getTectonic().getPlate_name());

            // *****************************************************************
            // Handle Location
            // *****************************************************************
            JSONObject location = new JSONObject();
            location.put("city", city);
            location.put("coordinates", coordinates);
            location.put("tectonic", tectonic);
            location.put("description", aData.getLocation().getDescription());
            station.put("location", location);

            // *****************************************************************
            // Handle Monument
            // *****************************************************************
            JSONObject monument = new JSONObject();
            monument.put("description", aData.getMonument().getDescription());
            monument.put("inscription", aData.getMonument().getInscription());
            monument.put("height", aData.getMonument().getHeight());
            monument.put("foundation", aData.getMonument().getFoundation());
            monument.put("foundation_depth", aData.getMonument().getFoundation_depth());
            station.put("monument", monument);

            // *****************************************************************
            // Handle Bedrock
            // *****************************************************************
            JSONObject bedrock = new JSONObject();
            bedrock.put("condition", aData.getGeological().getBedrock().getCondition());
            bedrock.put("type", aData.getGeological().getBedrock().getType());

            // *****************************************************************
            // Handle Geological
            // *****************************************************************
            JSONObject geological = new JSONObject();
            geological.put("characteristic", aData.getGeological().getCharacteristic());
            geological.put("fracture_spacing", aData.getGeological().getFracture_spacing());
            geological.put("fault_zone", aData.getGeological().getFault_zone());
            geological.put("distance_to_fault", aData.getGeological().getDistance_to_fault());
            geological.put("bedrock", bedrock);
            station.put("geological", geological);

            // *****************************************************************
            // Handle Station Colocation Offsets
            // *****************************************************************
            JSONArray colocation_offsets = new JSONArray();
            for (int j = 0; j < aData.getStationColocationOffsets().size(); j++) {
                // *************************************************************
                // Handle Station Colocated
                // *************************************************************
                JSONObject station_colocated = new JSONObject();
                station_colocated.put("name", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getName());
                station_colocated.put("marker", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getMarker());
                station_colocated.put("description", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDescription());
                station_colocated.put("date_from", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDate_from());
                station_colocated.put("date_to", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDate_to());
                station_colocated.put("comment", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getComment());
                station_colocated.put("iers_domes", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getIers_domes());
                station_colocated.put("cpd_num", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getCpd_num());
                station_colocated.put("monument_num", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getMonument_num());
                station_colocated.put("receiver_num", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getReceiver_num());
                station_colocated.put("country_code", aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getCountry_code());

                // *************************************************************
                // Handle Colocation Offset
                // *************************************************************
                JSONObject colocation_offset = new JSONObject();
                colocation_offset.put("station_colocated", station_colocated);
                colocation_offset.put("offset_x", aData.getStationColocationOffsets().get(j).getOffset_x());
                colocation_offset.put("offset_y", aData.getStationColocationOffsets().get(j).getOffset_y());
                colocation_offset.put("offset_z", aData.getStationColocationOffsets().get(j).getOffset_z());
                colocation_offset.put("date_measured", aData.getStationColocationOffsets().get(j).getDate_measured());
                colocation_offsets.add(colocation_offset);
            }
            station.put("colocation_offsets", colocation_offsets);

            // *****************************************************************
            // Handle Conditions and Effects
            // *****************************************************************
            JSONArray conditions = new JSONArray();
            for (int j = 0; j < aData.getCondition().size(); j++) {
                // *************************************************************
                // Handle effects
                // *************************************************************
                JSONObject effects = new JSONObject();
                effects.put("type", aData.getCondition().get(j).getEffect().getType());

                // *************************************************************
                // Handle condition
                // *************************************************************
                JSONObject condition = new JSONObject();
                condition.put("date_from", aData.getCondition().get(j).getDate_from());
                condition.put("date_to", aData.getCondition().get(j).getDate_to());
                condition.put("degradation", aData.getCondition().get(j).getDegradation());
                condition.put("comments", aData.getCondition().get(j).getComments());
                condition.put("effect", effects);

                conditions.add(condition);
            }
            station.put("conditions", conditions);

            // *****************************************************************
            // Handle Local Ties
            // *****************************************************************
            JSONArray local_ties = new JSONArray();
            for (int j = 0; j < aData.getLocalTies().size(); j++) {
                JSONObject local_tie = new JSONObject();
                local_tie.put("name", aData.getLocalTies().get(j).getName());
                local_tie.put("usage", aData.getLocalTies().get(j).getUsage());
                local_tie.put("cpd_num", aData.getLocalTies().get(j).getCpd_num());
                local_tie.put("dx", aData.getLocalTies().get(j).getDx());
                local_tie.put("dy", aData.getLocalTies().get(j).getDy());
                local_tie.put("dz", aData.getLocalTies().get(j).getDz());
                local_tie.put("accuracy", aData.getLocalTies().get(j).getAccuracy());
                local_tie.put("survey_method", aData.getLocalTies().get(j).getSurvey_method());
                local_tie.put("date_at", aData.getLocalTies().get(j).getDate_at());
                local_tie.put("comment", aData.getLocalTies().get(j).getComment());
                local_ties.add(local_tie);
            }
            station.put("local_ties", local_ties);

            // *****************************************************************
            // Handle Collocation Instrument
            // *****************************************************************
            JSONArray collocation_instruments = new JSONArray();
            for (int j = 0; j < aData.getCollocationInstrument().size(); j++) {
                JSONObject collocation_instrument = new JSONObject();
                collocation_instrument.put("type", aData.getCollocationInstrument().get(j).getType());
                collocation_instrument.put("status", aData.getCollocationInstrument().get(j).getStatus());
                collocation_instrument.put("date_from", aData.getCollocationInstrument().get(j).getDate_from());
                collocation_instrument.put("date_to", aData.getCollocationInstrument().get(j).getDate_to());
                collocation_instrument.put("comment", aData.getCollocationInstrument().get(j).getComment());
                collocation_instruments.add(collocation_instrument);
            }
            station.put("collocation_instruments", collocation_instruments);

            // *****************************************************************
            // Handle Acess Control
            // *****************************************************************
            JSONArray user_groups = new JSONArray();
            for (int j = 0; j < aData.getUserGroupStation().size(); j++) {
                // *************************************************************
                // Handle User Group
                // *************************************************************
                JSONObject user_group = new JSONObject();
                user_group.put("name", aData.getUserGroupStation().get(j).getUser_groups().getName());

                // *************************************************************
                // Handle User Group Station
                // *************************************************************
                JSONObject user_group_station = new JSONObject();
                user_group_station.put("user_group", user_group);
                user_groups.add(user_group_station);
            }
            station.put("user_groups_station", user_groups);

            // *****************************************************************
            // Handle Site Logs
            // *****************************************************************
            JSONArray logs = new JSONArray();
            for (int j = 0; j < aData.getLogs().size(); j++) {
                // *************************************************************
                // Handle Log Type
                // *************************************************************
                JSONObject log_type = new JSONObject();
                log_type.put("name", aData.getLogs().get(j).getLog_type().getName());

                // *************************************************************
                // Handle Agency
                // *************************************************************
                JSONObject agency = new JSONObject();
                agency.put("name", aData.getLogs().get(j).getContact().getAgency().getName());
                agency.put("abbreviation", aData.getLogs().get(j).getContact().getAgency().getAbbreviation());
                agency.put("address", aData.getLogs().get(j).getContact().getAgency().getAddress());
                agency.put("www", aData.getLogs().get(j).getContact().getAgency().getWww());
                agency.put("infos", aData.getLogs().get(j).getContact().getAgency().getInfos());

                // *************************************************************
                // Handle Contact
                // *************************************************************
                JSONObject contact = new JSONObject();
                contact.put("name", aData.getLogs().get(j).getContact().getName());
                contact.put("title", aData.getLogs().get(j).getContact().getTitle());
                contact.put("email", aData.getLogs().get(j).getContact().getEmail());
                contact.put("phone", aData.getLogs().get(j).getContact().getPhone());
                contact.put("gsm", aData.getLogs().get(j).getContact().getGsm());
                contact.put("comment", aData.getLogs().get(j).getContact().getComment());
                contact.put("role", aData.getLogs().get(j).getContact().getRole());
                contact.put("agency", agency);

                // *************************************************************
                // Handle Log
                // *************************************************************
                JSONObject log = new JSONObject();
                log.put("title", aData.getLogs().get(j).getTitle());
                log.put("date", aData.getLogs().get(j).getDate());
                log.put("modified", aData.getLogs().get(j).getModified());
                log.put("log_type", log_type);
                log.put("contact", contact);
                logs.add(log);
            }
            station.put("logs", logs);

            // *****************************************************************
            // Handle Station Contacts
            // *****************************************************************
            JSONArray station_contacts = new JSONArray();
            for (int j = 0; j < aData.getStationContacts().size(); j++) {
                // *************************************************************
                // Handle Agency
                // *************************************************************
                JSONObject agency = new JSONObject();
                agency.put("name", aData.getStationContacts().get(j).getContact().getAgency().getName());
                agency.put("abbreviation", aData.getStationContacts().get(j).getContact().getAgency().getAbbreviation());
                agency.put("address", aData.getStationContacts().get(j).getContact().getAgency().getAddress());
                agency.put("www", aData.getStationContacts().get(j).getContact().getAgency().getWww());
                agency.put("infos", aData.getStationContacts().get(j).getContact().getAgency().getInfos());

                // *************************************************************
                // Handle Contact
                // *************************************************************
                JSONObject contact = new JSONObject();
                contact.put("name", aData.getStationContacts().get(j).getContact().getName());
                contact.put("title", aData.getStationContacts().get(j).getContact().getTitle());
                contact.put("email", aData.getStationContacts().get(j).getContact().getEmail());
                contact.put("phone", aData.getStationContacts().get(j).getContact().getPhone());
                contact.put("gsm", aData.getStationContacts().get(j).getContact().getGsm());
                contact.put("comment", aData.getStationContacts().get(j).getContact().getComment());
                contact.put("role", aData.getStationContacts().get(j).getContact().getRole());
                contact.put("agency", agency);

                // *************************************************************
                // Handle Station Contact
                // *************************************************************
                JSONObject station_contact = new JSONObject();
                station_contact.put("role", aData.getStationContacts().get(j).getRole());
                station_contact.put("contact", contact);
                station_contacts.add(station_contact);
            }
            station.put("station_contacts", station_contacts);

            // *****************************************************************
            // Handle Station Networks
            // *****************************************************************
            JSONArray station_networks = new JSONArray();
            for (int j = 0; j < aData.getStationNetworks().size(); j++) {
                // *************************************************************
                // Handle Agency
                // *************************************************************
                JSONObject agency = new JSONObject();
                agency.put("name", aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getName());
                agency.put("abbreviation", aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getAbbreviation());
                agency.put("address", aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getAddress());
                agency.put("www", aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getWww());
                agency.put("infos", aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getInfos());

                // *************************************************************
                // Handle Contact
                // *************************************************************
                JSONObject contact = new JSONObject();
                contact.put("name", aData.getStationNetworks().get(j).getNetwork().getContact().getName());
                contact.put("title", aData.getStationNetworks().get(j).getNetwork().getContact().getTitle());
                contact.put("email", aData.getStationNetworks().get(j).getNetwork().getContact().getEmail());
                contact.put("phone", aData.getStationNetworks().get(j).getNetwork().getContact().getPhone());
                contact.put("gsm", aData.getStationNetworks().get(j).getNetwork().getContact().getGsm());
                contact.put("comment", aData.getStationNetworks().get(j).getNetwork().getContact().getComment());
                contact.put("role", aData.getStationNetworks().get(j).getNetwork().getContact().getRole());
                contact.put("agency", agency);

                // *************************************************************
                // Handle Network
                // *************************************************************
                JSONObject network = new JSONObject();
                network.put("name", aData.getStationNetworks().get(j).getNetwork().getName());
                network.put("contact", contact);

                // *************************************************************
                // Handle Station Network
                // *************************************************************
                JSONObject station_network = new JSONObject();
                station_network.put("network", network);
                station_networks.add(station_network);
            }
            station.put("station_networks", station_networks);

            // *****************************************************************
            // Handle Station Items
            // *****************************************************************
            JSONArray station_items = new JSONArray();
            for (int j = 0; j < aData.getStationItems().size(); j++) {
                // *************************************************************
                // Handle Agency as Producer
                // *************************************************************
            /*
                JSONObject agency_as_producer = new JSONObject();
                agency_as_producer.put("name", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getName());
                agency_as_producer.put("abbreviation", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAbbreviation());
                agency_as_producer.put("address", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAddress());
                agency_as_producer.put("www", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getWww());
                agency_as_producer.put("infos", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getInfos());

                // *************************************************************
                // Handle Contact as Producer
                // *************************************************************
                JSONObject contact_as_producer = new JSONObject();
                contact_as_producer.put("name", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getName());
                contact_as_producer.put("title", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getTitle());
                contact_as_producer.put("email", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getEmail());
                contact_as_producer.put("phone", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getPhone());
                contact_as_producer.put("gsm", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getGsm());
                contact_as_producer.put("comment", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getComment());
                contact_as_producer.put("role", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getRole());
                contact_as_producer.put("agency", agency_as_producer);

                // *************************************************************
                // Handle Agency as Owner
                // *************************************************************
                JSONObject agency_as_owner = new JSONObject();
                agency_as_owner.put("name", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getName());
                agency_as_owner.put("abbreviation", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAbbreviation());
                agency_as_owner.put("address", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAddress());
                agency_as_owner.put("www", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getWww());
                agency_as_owner.put("infos", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getInfos());

                // *************************************************************
                // Handle Contact as Owner
                // *************************************************************
                JSONObject contact_as_owner = new JSONObject();
                contact_as_owner.put("name", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getName());
                contact_as_owner.put("title", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getTitle());
                contact_as_owner.put("email", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getEmail());
                contact_as_owner.put("phone", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getPhone());
                contact_as_owner.put("gsm", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getGsm());
                contact_as_owner.put("comment", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getComment());
                contact_as_owner.put("role", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getRole());
                contact_as_owner.put("agency", agency_as_owner);
            */

                // *************************************************************
                // Handle Item Type
                // *************************************************************
                JSONObject item_type = new JSONObject();
                item_type.put("name", aData.getStationItems().get(j).getItem().getItem_type().getName());

                // *************************************************************
                // Handle Item Attributes
                // *************************************************************
                JSONArray item_attributes = new JSONArray();
                for (int k = 0; k < aData.getStationItems().get(j).getItem().GetItemAttribues().size(); k++) {
                    // *********************************************************
                    // Handle Receiver
                    // *********************************************************
                /*
                    JSONArray filter_receivers = new JSONArray();
                    for (int p=0; p<data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().size(); p++)
                    {
                        // *****************************************************
                        // Handle Receiver Type
                        // *****************************************************
                        JSONObject receiver_type = new JSONObject();
                        receiver_type.put("name", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getName());
                        receiver_type.put("igs_defined", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getIgs_defined());
                        receiver_type.put("model", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getModel());

                        // *****************************************************
                        // Handle Attribute
                        // *****************************************************
                        JSONObject filter_attribute = new JSONObject();
                        filter_attribute.put("name", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getAttribute().getName());

                        // *****************************************************
                        // Handle Filter Receiver
                        // *****************************************************
                        JSONObject filter_receiver = new JSONObject();
                        filter_receiver.put("attribute", filter_attribute);
                        filter_receiver.put("receiver_type", receiver_type);

                        filter_receivers.add(filter_receiver);
                    }

                    // *********************************************************
                    // Handle Antenna
                    // *********************************************************
                    JSONArray filter_antennas = new JSONArray();
                    for (int p=0; p<data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().size(); p++)
                    {
                        // *****************************************************
                        // Handle Antenna Type
                        // *****************************************************
                        JSONObject antenna_type = new JSONObject();
                        antenna_type.put("name", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getName());
                        antenna_type.put("igs_defined", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getIgs_defined());
                        antenna_type.put("model", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getModel());

                        // *****************************************************
                        // Handle Attribute
                        // *****************************************************
                        JSONObject filter_attribute = new JSONObject();
                        filter_attribute.put("name", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAttribute().getName());

                        // *****************************************************
                        // Handle Filter Antenna
                        // *****************************************************
                        JSONObject filter_antenna = new JSONObject();
                        filter_antenna.put("attribute", filter_attribute);
                        filter_antenna.put("antenna_type", antenna_type);

                        filter_antennas.add(filter_antenna);
                    }

                    // *********************************************************
                    // Handle Radome
                    // *********************************************************
                    JSONArray filter_radomes = new JSONArray();
                    for (int p=0; p<data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().size(); p++)
                    {
                        // *****************************************************
                        // Handle Radome Type
                        // *****************************************************
                        JSONObject radome_type = new JSONObject();
                        radome_type.put("name", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getName());
                        radome_type.put("igs_defined", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getIgs_defined());
                        radome_type.put("description", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getDescription());

                        // *****************************************************
                        // Handle Attribute
                        // *****************************************************
                        JSONObject filter_attribute = new JSONObject();
                        filter_attribute.put("name", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getAttribute().getName());

                        // *****************************************************
                        // Handle Filter Radome
                        // *****************************************************
                        JSONObject filter_radome = new JSONObject();
                        filter_radome.put("attribute", filter_attribute);
                        filter_radome.put("radome_type", radome_type);

                        filter_radomes.add(filter_radome);
                    }
                */

                    // *********************************************************
                    // Handle Attribute
                    // *********************************************************
                    JSONObject attribute = new JSONObject();
                    attribute.put("name", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getAttribute().getName());

                    // *********************************************************
                    // Handle Item Attribute
                    // *********************************************************
                    JSONObject item_attribute = new JSONObject();
                    item_attribute.put("date_from", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getDate_from());
                    item_attribute.put("date_to", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getDate_to());
                    item_attribute.put("value_varchar", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_varchar());
                    item_attribute.put("value_date", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_date());
                    item_attribute.put("value_numeric", aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_numeric());
                    item_attribute.put("attribute", attribute);

                /*
                    if (data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getAttribute().getName().equals("receiver_type")){
                        for (int p=0; p<data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().size(); p++)
                        {
                            item_attribute.put("igs_defined", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getIgs_defined());
                            item_attribute.put("model", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getModel());
                        }
                    }else if (data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getAttribute().getName().equals("antenna_type")){
                        for (int p=0; p<data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().size(); p++)
                        {
                            item_attribute.put("igs_defined", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getIgs_defined());
                            item_attribute.put("model", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getModel());
                        }
                    }else if (data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getAttribute().getName().equals("radome_type")){
                        for (int p=0; p<data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().size(); p++)
                        {
                            item_attribute.put("igs_defined", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getIgs_defined());
                            item_attribute.put("description", data.get(i).getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getDescription());
                        }
                    }
                    item_attribute.put("filter_receivers", filter_receivers);
                    item_attribute.put("filter_antennas", filter_antennas);
                    item_attribute.put("filter_radomes", filter_radomes);
                */

                    item_attributes.add(item_attribute);
                }

                // *************************************************************
                // Handle Item
                // *************************************************************
                JSONObject item = new JSONObject();
                item.put("comment", aData.getStationItems().get(j).getItem().getComment());
                item.put("item_type", item_type);
                /*item.put("contact_as_producer", contact_as_producer);
                item.put("contact_as_owner", contact_as_owner);*/
                item.put("item_attributes", item_attributes);

                // *************************************************************
                // Handle Station Item
                // *************************************************************
                JSONObject station_item = new JSONObject();
                station_item.put("date_from", aData.getStationItems().get(j).getDate_from());
                station_item.put("date_to", aData.getStationItems().get(j).getDate_to());
                station_item.put("item", item);
                station_items.add(station_item);
            }
            station.put("station_items", station_items);

            // *****************************************************************
            // Handle Data Files
            // *****************************************************************
            JSONArray files_generated = new JSONArray();
            for (int j = 0; j < aData.getFilesGenerated().size(); j++) {
                // *************************************************************
                // Handle File Type
                // *************************************************************
                JSONObject file_type = new JSONObject();
                //file_type.put("name", data.get(i).getFilesGenerated().get(j).getFile_type().getName());
                file_type.put("format", aData.getFilesGenerated().get(j).getFile_type().getFormat());

                // *************************************************************
                // Handle File Generated
                // *************************************************************
                JSONObject file_generated = new JSONObject();
                file_generated.put("file_type", file_type);
                files_generated.add(file_generated);
            }
            station.put("files_generated", files_generated);

            // *****************************************************************
            // Handle Documents
            // *****************************************************************
            JSONArray documents = new JSONArray();
            for (int j = 0; j < aData.getDocument().size(); j++) {
                // *************************************************************
                // Handle Document Type
                // *************************************************************
                JSONObject document_type = new JSONObject();
                document_type.put("name", aData.getDocument().get(j).getDocument_type().getName());

                // *************************************************************
                // Handle Item Type
                // *************************************************************
                JSONObject item_type = new JSONObject();
                item_type.put("name", aData.getDocument().get(j).getItem().getItem_type().getName());

                // *************************************************************
                // Handle Agency as Producer
                // *************************************************************
            /*
                JSONObject agency_as_producer = new JSONObject();
                agency_as_producer.put("name", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getName());
                agency_as_producer.put("abbreviation", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAbbreviation());
                agency_as_producer.put("address", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAddress());
                agency_as_producer.put("www", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getWww());
                agency_as_producer.put("infos", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getAgency().getInfos());

                // *************************************************************
                // Handle Contact as Producer
                // *************************************************************
                JSONObject contact_as_producer = new JSONObject();
                contact_as_producer.put("name", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getName());
                contact_as_producer.put("title", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getTitle());
                contact_as_producer.put("email", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getEmail());
                contact_as_producer.put("phone", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getPhone());
                contact_as_producer.put("gsm", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getGsm());
                contact_as_producer.put("comment", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getComment());
                contact_as_producer.put("role", data.get(i).getStationItems().get(j).getItem().getContact_as_producer().getRole());
                contact_as_producer.put("agency", agency_as_producer);

                // *************************************************************
                // Handle Agency as Owner
                // *************************************************************
                JSONObject agency_as_owner = new JSONObject();
                agency_as_owner.put("name", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getName());
                agency_as_owner.put("abbreviation", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAbbreviation());
                agency_as_owner.put("address", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAddress());
                agency_as_owner.put("www", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getWww());
                agency_as_owner.put("infos", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getAgency().getInfos());

                // *************************************************************
                // Handle Contact as Owner
                // *************************************************************
                JSONObject contact_as_owner = new JSONObject();
                contact_as_owner.put("name", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getName());
                contact_as_owner.put("title", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getTitle());
                contact_as_owner.put("email", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getEmail());
                contact_as_owner.put("phone", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getPhone());
                contact_as_owner.put("gsm", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getGsm());
                contact_as_owner.put("comment", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getComment());
                contact_as_owner.put("role", data.get(i).getStationItems().get(j).getItem().getContact_as_owner().getRole());
                contact_as_owner.put("agency", agency_as_owner);


                // *************************************************************
                // Handle Item
                // *************************************************************
                JSONObject item = new JSONObject();
                item.put("comment", data.get(i).getDocument().get(j).getItem().getComment());
                item.put("item_type", item_type);

                item.put("contact_as_producer", contact_as_producer);
                item.put("contact_as_owner", contact_as_owner);
            */

                // *************************************************************
                // Handle Document
                // *************************************************************
                JSONObject document = new JSONObject();
                document.put("date", aData.getDocument().get(j).getDate());
                document.put("title", aData.getDocument().get(j).getTitle());
                document.put("description", aData.getDocument().get(j).getDescription());
                document.put("link", aData.getDocument().get(j).getLink());
                //document.put("item", item);
                document.put("document_type", document_type);
                documents.add(document);
            }
            station.put("documents", documents);

            // *************************************************************
            // Handle Rinex files
            // *************************************************************
            station.put("rinex_files_dates", aData.getRinexFiles());

            station_list.add(station);
        }

        return station_list;
    }


    /*
    * GET SHORT JSON
    * This method returns the short data in JSON.
    */
    public JSONArray getShortJSON(ArrayList<Station> data)
    {
        JSONArray station_list = new JSONArray();

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            JSONObject station = new JSONObject();
            station.put("marker", aData.getMarker());
            station.put("markerlongname", aData.getMarkerLongName());
            station.put("name", aData.getName());
            station.put("date_from", aData.getDate_from());
            station.put("date_to", aData.getDate_to());

            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getLocation().getCity().getState().getCountry().getName());

            // *****************************************************************
            // Handle State
            // *****************************************************************
            JSONObject state = new JSONObject();
            state.put("name", aData.getLocation().getCity().getState().getName());

            // *****************************************************************
            // Handle City
            // *****************************************************************
            JSONObject city = new JSONObject();
            city.put("name", aData.getLocation().getCity().getName());

            // *****************************************************************
            // Handle Coordinates
            // *****************************************************************
            JSONObject coordinates = new JSONObject();
            coordinates.put("x", aData.getLocation().getCoordinates().getX());
            coordinates.put("y", aData.getLocation().getCoordinates().getY());
            coordinates.put("z", aData.getLocation().getCoordinates().getZ());
            coordinates.put("lat", aData.getLocation().getCoordinates().getLat());
            coordinates.put("lon", aData.getLocation().getCoordinates().getLon());
            coordinates.put("altitude", aData.getLocation().getCoordinates().getAltitude());

            // *****************************************************************
            // Handle Location
            // *****************************************************************
            JSONObject location = new JSONObject();
            location.put("coordinates", coordinates);
            location.put("country", country);
            location.put("state", state);
            location.put("city", city);
            station.put("location", location);

            // *****************************************************************
            // Handle Agency
            // *****************************************************************
            JSONObject agency = new JSONObject();
            for (int j = 0; j < aData.getStationContacts().size(); j++) {
                agency.put("name", aData.getStationContacts().get(j).getContact().getAgency().getName());
            }
            station.put("agency", agency);




            // *****************************************************************
            // Handle Network in String format
            // *****************************************************************

            JSONObject network = new JSONObject();
            StringBuilder networks = new StringBuilder();
            for(int i=0; i<aData.getStationNetworks().size(); i++) {
                networks.append(aData.getStationNetworks().get(i).getNetwork().getName());
                if(i<aData.getStationNetworks().size()-1)
                    networks.append(" & ");
            }
            network.put("name", networks.toString());

            station.put("network", network);



            // *****************************************************************
            // Handle Networks in Array format
            // *****************************************************************
            JSONArray network_array = new JSONArray();
            for (int j = 0; j < aData.getStationNetworks().size(); j++) {
                // *************************************************************
                // Handle Network
                // *************************************************************
                network_array.add(aData.getStationNetworks().get(j).getNetwork().getName());
            }
            station.put("network_array", network_array);



            JSONArray velocityField = this.getVelocitiesFieldJSON(aData.getV_marker());
            //velocityField.put("angle", aData.getAngle());
            //velocityField.put("vector_length", aData.getVectorLength());
            JSONObject _velocityField = new JSONObject();  
            _velocityField.put("velocity", velocityField);
            station.put("velocity_field", _velocityField);

            JSONObject analysisCenters = new JSONObject();
            JSONArray pa_ec = new JSONArray();
            JSONObject forcycle;
            for (Method_identification _MI_timeseries : aData.getMI_timeseries()) {
                try {
                    forcycle = new JSONObject();
                    forcycle.put("name", _MI_timeseries.getAnalysis_center().getName());
                    forcycle.put("abbreviation", _MI_timeseries.getAnalysis_center().getAbbreviation());
                    forcycle.put("doi", _MI_timeseries.getDoi());
                    forcycle.put("version", _MI_timeseries.getVersion());
                    forcycle.put("reference_frame", _MI_timeseries.getReference_frame().getName());
                    forcycle.put("epoch", _MI_timeseries.getReference_frame().getEpoch());
                    forcycle.put("creation_date", _MI_timeseries.getCreation_date());
                    pa_ec.add(forcycle);
                } catch (NullPointerException _ignored){
                }
            }
            if (!pa_ec.isEmpty())
                analysisCenters.put("timeseries", pa_ec);

            JSONArray pa_rpv = new JSONArray();
            for (int j = 0; j < aData.getPA_velocities().size(); j++) {
                forcycle = new JSONObject();
                forcycle.put("name", aData.getPA_velocities().get(j).getName());
                forcycle.put("abbreviation", aData.getPA_velocities().get(j).getAbbreviation());

                pa_rpv.add(forcycle);
            }
            if (!pa_rpv.isEmpty())
                analysisCenters.put("velocities", pa_rpv);

            JSONArray pa_coo = new JSONArray();
            for (int j = 0; j < aData.getPA_coordinates().size(); j++) {
                forcycle = new JSONObject();
                forcycle.put("name", aData.getPA_coordinates().get(j).getName());
                forcycle.put("abbreviation", aData.getPA_coordinates().get(j).getAbbreviation());

                pa_coo.add(forcycle);
            }

            if (!pa_coo.isEmpty())
                analysisCenters.put("coordinates", pa_coo);

            JSONArray pa_psd = new JSONArray();
            for (int j = 0; j < aData.getPower_spectral_density().size(); j++) {
                forcycle = new JSONObject();

                try {
                    Analysis_Centers eposAc = aData.getPower_spectral_density().get(j);
                    if ( eposAc != null ) {
                        forcycle.put("name",eposAc.getName());
                        forcycle.put("abbreviation", eposAc.getAbbreviation());
                        pa_psd.add(forcycle);
                    }
                } catch (Exception e) {
                    System.out.println("Error " + e.getMessage());
                    continue;
                }
            }
            if (!pa_psd.isEmpty())
                analysisCenters.put("powerspectraldensity", pa_psd);


            if (!analysisCenters.isEmpty())
                station.put("analysis_centres", analysisCenters);



            station_list.add(station);
        }

        return station_list;
    }

    /*
    * GET AGENCY JSON
    * This method returns the agency data in JSON.
    */
    public JSONArray getAgencyJSON(ArrayList<Agency> data)
    {
        JSONArray agency_list = new JSONArray();

        for (Agency aData : data) {
            // *****************************************************************
            // Handle Agency
            // *****************************************************************
            JSONObject agency = new JSONObject();
            agency.put("name", aData.getName());
            agency.put("abbreviation", aData.getAbbreviation());
            agency.put("address", aData.getAddress());
            agency.put("www", aData.getWww());
            agency.put("infos", aData.getInfos());

            agency_list.add(agency);
        }

        return agency_list;
    }

    /*
    * GET ANTENNA TYPE JSON
    * This method returns the antenna type data in JSON.
    */
    public JSONArray getAntennaTypeJSON(ArrayList<Antenna_type> data)
    {
        JSONArray antenna_type_list = new JSONArray();

        for (Antenna_type aData : data) {
            // *****************************************************************
            // Handle Antenna Type
            // *****************************************************************
            JSONObject antenna_type = new JSONObject();
            antenna_type.put("name", aData.getName());
            antenna_type.put("igs_defined", aData.getIgs_defined());
            antenna_type.put("model", aData.getModel());

            antenna_type_list.add(antenna_type);
        }

        return antenna_type_list;
    }

    /*
    * GET CITY JSON
    * This method returns the city data in JSON.
    */
    public JSONArray getCityJSON(ArrayList<City> data)
    {
        JSONArray city_list = new JSONArray();

        for (City aData : data) {
            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getState().getCountry().getName());
            country.put("iso_code", aData.getState().getCountry().getIso_code());

            // *****************************************************************
            // Handle State
            // *****************************************************************
            JSONObject state = new JSONObject();
            state.put("name", aData.getState().getName());
            state.put("country", country);

            // *****************************************************************
            // Handle City
            // *****************************************************************
            JSONObject city = new JSONObject();
            city.put("name", aData.getName());
            city.put("state", state);

            city_list.add(city);
        }

        return city_list;
    }

    /*
    * GET COUNTRY JSON
    * This method returns the country data in JSON.
    */
    public JSONArray getCountryJSON(ArrayList<Country> data)
    {
        JSONArray country_list = new JSONArray();

        for (Country aData : data) {
            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getName());
            country.put("iso_code", aData.getIso_code());

            country_list.add(country);
        }

        return country_list;
    }

    /*
    * GET RADOME TYPE JSON
    * This method returns the radome type data in JSON.
    */
    public JSONArray getRadomeTypeJSON(ArrayList<Radome_type> data)
    {
        JSONArray radome_type_list = new JSONArray();

        for (Radome_type aData : data) {
            // *****************************************************************
            // Handle Radome Type
            // *****************************************************************
            JSONObject radome_type = new JSONObject();
            radome_type.put("name", aData.getName());
            radome_type.put("igs_defined", aData.getIgs_defined());
            radome_type.put("description", aData.getDescription());

            radome_type_list.add(radome_type);
        }

        return radome_type_list;
    }
    
    /*
    * GET FILE TYPE JSON
    * This method returns the file type data in JSON.
    */
    public JSONArray getFileTypeJSON(ArrayList<File_type> data)
    {
        JSONArray file_type_list = new JSONArray();

        for (File_type aData : data) {
            // *****************************************************************
            // Handle File Type
            // *****************************************************************
            JSONObject file_type = new JSONObject();
            //file_type.put("name", data.get(i).getFormat() + "-" + data.get(i).getSampling_window() + "-" + data.get(i).getSampling_frequency());
            file_type.put("format", aData.getFormat());
            file_type.put("sampling_window", aData.getSampling_window());
            file_type.put("sampling_frequency", aData.getSampling_frequency());

            file_type_list.add(file_type);
        }
        
        return file_type_list;
    }

    /*
     * GET STATE JSON
     * This method returns the state data in JSON.
     */
    JSONArray getStateJSON(ArrayList<State> data)
    {
        JSONArray state_list = new JSONArray();

        for (State aData : data) {
            // *****************************************************************
            // Handle Country
            // *****************************************************************
            JSONObject country = new JSONObject();
            country.put("name", aData.getCountry().getName());
            country.put("iso_code", aData.getCountry().getIso_code());

            // *****************************************************************
            // Handle State
            // *****************************************************************
            JSONObject state = new JSONObject();
            state.put("name", aData.getName());
            state.put("country", country);

            state_list.add(state);
        }

        return state_list;
    }

    /*
    * GET RECEIVER TYPE JSON
    * This method returns the receiver type data in JSON.
    */
    public JSONArray getReceiverTypeJSON(ArrayList<Receiver_type> data)
    {
        JSONArray receiver_type_list = new JSONArray();

        for (Receiver_type aData : data) {
            // *****************************************************************
            // Handle Receiver Type
            // *****************************************************************
            JSONObject receiver_type = new JSONObject();
            receiver_type.put("name", aData.getName());
            receiver_type.put("igs_defined", aData.getIgs_defined());
            receiver_type.put("model", aData.getModel());

            receiver_type_list.add(receiver_type);
        }

        return receiver_type_list;
    }

    /*
    * GET STATION NAME JSON
    * This method returns the station names data in JSON.
    */
    public JSONArray getStationNameJSON(ArrayList<Station> data)
    {
        JSONArray station_name_list = new JSONArray();

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            JSONObject station = new JSONObject();
            station.put("name", aData.getName());
            station.put("marker", aData.getMarker());

            station_name_list.add(station);
        }

        return station_name_list;
    }

    String getFullXML(ArrayList<Station> data)
    {
        StringBuilder xml = new StringBuilder();
        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        xml.append("<stations>");

        for (Station aData : data) {
            // *****************************************************************
            // Handle Station
            // *****************************************************************
            xml.append("<station>");
            xml.append("<name>");
            xml.append(aData.getName());
            xml.append("</name>");
            xml.append("<marker>");
            xml.append(aData.getMarker());
            xml.append("</marker>");
            xml.append("<description>");
            xml.append(aData.getDescription());
            xml.append("</description>");
            xml.append("<date_from>");
            xml.append(aData.getDate_from());
            xml.append("</date_from>");
            xml.append("<date_to>");
            xml.append(aData.getDate_to());
            xml.append("</date_to>");
            xml.append("<comment>");
            xml.append(aData.getComment());
            xml.append("</comment>");
            xml.append("<iers_domes>");
            xml.append(aData.getIers_domes());
            xml.append("</iers_domes>");
            xml.append("<cpd_num>");
            xml.append(aData.getCpd_num());
            xml.append("</cpd_num>");
            xml.append("<monument_num>");
            xml.append(aData.getMonument_num());
            xml.append("</monument_num>");
            xml.append("<receiver_num>");
            xml.append(aData.getReceiver_num());
            xml.append("</receiver_num>");
            xml.append("<country_code>");
            xml.append(aData.getCountry_code());
            xml.append("</country_code>");
            xml.append("<station_type>");
            xml.append("<name>");
            xml.append(aData.getStation_type().getName());
            xml.append("</name>");
            xml.append("<type>");
            xml.append(aData.getStation_type().getType());
            xml.append("</type>");
            xml.append("</station_type>");
            xml.append("<location>");
            xml.append("<city>");
            xml.append("<name>");
            xml.append(aData.getLocation().getCity().getName());
            xml.append("</name>");
            xml.append("<state>");
            xml.append("<name>");
            xml.append(aData.getLocation().getCity().getState().getName());
            xml.append("</name>");
            xml.append("<country>");
            xml.append("<name>");
            xml.append(aData.getLocation().getCity().getState().getCountry().getName());
            xml.append("</name>");
            xml.append("<iso_code>");
            xml.append(aData.getLocation().getCity().getState().getCountry().getIso_code());
            xml.append("</iso_code>");
            xml.append("</country>");
            xml.append("</state>");
            xml.append("</city>");
            xml.append("<coordinates>");
            xml.append("<x>");
            xml.append(aData.getLocation().getCoordinates().getX());
            xml.append("</x>");
            xml.append("<y>");
            xml.append(aData.getLocation().getCoordinates().getY());
            xml.append("</y>");
            xml.append("<z>");
            xml.append(aData.getLocation().getCoordinates().getZ());
            xml.append("</z>");
            xml.append("<lat>");
            xml.append(aData.getLocation().getCoordinates().getLat());
            xml.append("</lat>");
            xml.append("<lon>");
            xml.append(aData.getLocation().getCoordinates().getLon());
            xml.append("</lon>");
            xml.append("<altitude>");
            xml.append(aData.getLocation().getCoordinates().getAltitude());
            xml.append("</altitude>");
            xml.append("</coordinates>");
            xml.append("<tectonic>");
            xml.append("<plate_name>");
            xml.append(aData.getLocation().getTectonic().getPlate_name());
            xml.append("</plate_name>");
            xml.append("</tectonic>");
            xml.append("<description>");
            xml.append(aData.getLocation().getDescription());
            xml.append("</description>");
            xml.append("</location>");
            xml.append("<monument>");
            xml.append("<description>");
            xml.append(aData.getMonument().getDescription());
            xml.append("</description>");
            xml.append("<inscription>");
            xml.append(aData.getMonument().getInscription());
            xml.append("</inscription>");
            xml.append("<height>");
            xml.append(aData.getMonument().getHeight());
            xml.append("</height>");
            xml.append("<foundation>");
            xml.append(aData.getMonument().getFoundation());
            xml.append("</foundation>");
            xml.append("<foundation_depth>");
            xml.append(aData.getMonument().getFoundation_depth());
            xml.append("</foundation_depth>");
            xml.append("</monument>");
            xml.append("<geological>");
            xml.append("<characteristic>");
            xml.append(aData.getGeological().getCharacteristic());
            xml.append("</characteristic>");
            xml.append("<fracture_spacing>");
            xml.append(aData.getGeological().getFracture_spacing());
            xml.append("</fracture_spacing>");
            xml.append("<fault_zone>");
            xml.append(aData.getGeological().getFault_zone());
            xml.append("</fault_zone>");
            xml.append("<distance_to_fault>");
            xml.append(aData.getGeological().getDistance_to_fault());
            xml.append("</distance_to_fault>");
            xml.append("<bedrock>");
            xml.append("<condition>");
            xml.append(aData.getGeological().getBedrock().getCondition());
            xml.append("</condition>");
            xml.append("<type>");
            xml.append(aData.getGeological().getBedrock().getType());
            xml.append("</type>");
            xml.append("</bedrock>");
            xml.append("</geological>");
            xml.append("<colocation_offsets>");
            for (int j = 0; j < aData.getStationColocationOffsets().size(); j++) {
                // *************************************************************
                // Handle Colocation Offset
                // *************************************************************
                xml.append("<colocation_offset>");
                xml.append("<offset_x>");
                xml.append(aData.getStationColocationOffsets().get(j).getOffset_x());
                xml.append("</offset_x>");
                xml.append("<offset_y>");
                xml.append(aData.getStationColocationOffsets().get(j).getOffset_y());
                xml.append("</offset_y>");
                xml.append("<offset_z>");
                xml.append(aData.getStationColocationOffsets().get(j).getOffset_z());
                xml.append("</offset_z>");
                xml.append("<date_measured>");
                xml.append(aData.getStationColocationOffsets().get(j).getDate_measured());
                xml.append("</date_measured>");
                xml.append("<station_colocated>");
                xml.append("<name>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getName());
                xml.append("</name>");
                xml.append("<marker>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getMarker());
                xml.append("</marker>");
                xml.append("<description>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDescription());
                xml.append("</description>");
                xml.append("<date_from>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDate_from());
                xml.append("</date_from>");
                xml.append("<date_to>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getDate_to());
                xml.append("</date_to>");
                xml.append("<comment>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getComment());
                xml.append("</comment>");
                xml.append("<iers_domes>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getIers_domes());
                xml.append("</iers_domes>");
                xml.append("<cpd_num>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getCpd_num());
                xml.append("</cpd_num>");
                xml.append("<monument_num>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getMonument_num());
                xml.append("</monument_num>");
                xml.append("<receiver_num>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getReceiver_num());
                xml.append("</receiver_num>");
                xml.append("<country_code>");
                xml.append(aData.getStationColocationOffsets().get(j).getStation_colocation().getStation_colocated().getCountry_code());
                xml.append("</country_code>");
                xml.append("</station_colocated>");
                xml.append("</colocation_offset>");
            }
            xml.append("</colocation_offsets>").append("<conditions>");

            for (int j = 0; j < aData.getCondition().size(); j++) {
                // *************************************************************
                // Handle condition
                // *************************************************************
                xml.append("<condition>");
                xml.append("<date_from>");
                xml.append(aData.getCondition().get(j).getDate_from());
                xml.append("</date_from>");
                xml.append("<date_to>");
                xml.append(aData.getCondition().get(j).getDate_to());
                xml.append("</date_to>");
                xml.append("<degradation>");
                xml.append(aData.getCondition().get(j).getDegradation());
                xml.append("</degradation>");
                xml.append("<comments>");
                xml.append(aData.getCondition().get(j).getComments());
                xml.append("</comments>");
                xml.append("<effects>");
                xml.append("<type>");
                xml.append(aData.getCondition().get(j).getEffect().getType());
                xml.append("</type>");
                xml.append("</effects>");
                xml.append("</condition>");
            }
            xml.append("</conditions>").append("<local_ties>");
            for (int j = 0; j < aData.getLocalTies().size(); j++) {
                xml.append("<local_tie>");
                xml.append("<name>");
                xml.append(aData.getLocalTies().get(j).getName());
                xml.append("</name>");
                xml.append("<usage>");
                xml.append(aData.getLocalTies().get(j).getUsage());
                xml.append("</usage>");
                xml.append("<cpd_num>");
                xml.append(aData.getLocalTies().get(j).getCpd_num());
                xml.append("</cpd_num>");
                xml.append("<dx>");
                xml.append(aData.getLocalTies().get(j).getDx());
                xml.append("</dx>");
                xml.append("<dy>");
                xml.append(aData.getLocalTies().get(j).getDy());
                xml.append("</dy>");
                xml.append("<dz>");
                xml.append(aData.getLocalTies().get(j).getDz());
                xml.append("</dz>");
                xml.append("<accuracy>");
                xml.append(aData.getLocalTies().get(j).getAccuracy());
                xml.append("</accuracy>");
                xml.append("<survey_method>");
                xml.append(aData.getLocalTies().get(j).getSurvey_method());
                xml.append("</survey_method>");
                xml.append("<date_at>");
                xml.append(aData.getLocalTies().get(j).getDate_at());
                xml.append("</date_at>");
                xml.append("<comment>");
                xml.append(aData.getLocalTies().get(j).getComment());
                xml.append("</comment>");
                xml.append("</local_tie>");
            }

            xml.append("</local_ties>").append("<collocation_instruments>");

            for (int j = 0; j < aData.getCollocationInstrument().size(); j++) {
                xml.append("<collocation_instrument>");
                xml.append("<type>");
                xml.append(aData.getCollocationInstrument().get(j).getType());
                xml.append("</type>");
                xml.append("<status>");
                xml.append(aData.getCollocationInstrument().get(j).getStatus());
                xml.append("</status>");
                xml.append("<date_from>");
                xml.append(aData.getCollocationInstrument().get(j).getDate_from());
                xml.append("</date_from>");
                xml.append("<date_to>");
                xml.append(aData.getCollocationInstrument().get(j).getDate_to());
                xml.append("</date_to>");
                xml.append("<comment>");
                xml.append(aData.getCollocationInstrument().get(j).getComment());
                xml.append("</comment>");
                xml.append("</collocation_instrument>");
            }
            xml.append("</collocation_instruments>").append("<user_groups>");
            for (int j = 0; j < aData.getUserGroupStation().size(); j++) {
                // *************************************************************
                // Handle User Group Station
                // *************************************************************
                xml.append("<user_group_station>");
                xml.append("<user_group>");
                xml.append("<name>");
                xml.append(aData.getUserGroupStation().get(j).getUser_groups().getName());
                xml.append("</name>");
                xml.append("</user_group>");
                xml.append("</user_group_station>");
            }
            xml.append("</user_groups>").append("<logs>");
            for (int j = 0; j < aData.getLogs().size(); j++) {
                // *************************************************************
                // Handle Log
                // *************************************************************
                xml.append("<log>");
                xml.append("<title>");
                xml.append(aData.getLogs().get(j).getTitle());
                xml.append("</title>");
                xml.append("<date>");
                xml.append(aData.getLogs().get(j).getDate());
                xml.append("</date>");
                xml.append("<modified>");
                xml.append(aData.getLogs().get(j).getModified());
                xml.append("</modified>");
                xml.append("<log_type>");
                xml.append("<name>");
                xml.append(aData.getLogs().get(j).getLog_type().getName());
                xml.append("</name>");
                xml.append("</log_type>");
                xml.append("<contact>");
                xml.append("<name>");
                xml.append(aData.getLogs().get(j).getContact().getName());
                xml.append("</name>");
                xml.append("<title>");
                xml.append(aData.getLogs().get(j).getContact().getTitle());
                xml.append("</title>");
                xml.append("<email>");
                xml.append(aData.getLogs().get(j).getContact().getEmail());
                xml.append("</email>");
                xml.append("<phone>");
                xml.append(aData.getLogs().get(j).getContact().getPhone());
                xml.append("</phone>");
                xml.append("<gsm>");
                xml.append(aData.getLogs().get(j).getContact().getGsm());
                xml.append("</gsm>");
                xml.append("<comment>");
                xml.append(aData.getLogs().get(j).getContact().getComment());
                xml.append("</comment>");
                xml.append("<role>");
                xml.append(aData.getLogs().get(j).getContact().getRole());
                xml.append("</role>");
                xml.append("<agency>");
                xml.append("<name>");
                xml.append(aData.getLogs().get(j).getContact().getAgency().getName());
                xml.append("</name>");
                xml.append("<abbreviation>");
                xml.append(aData.getLogs().get(j).getContact().getAgency().getAbbreviation());
                xml.append("</abbreviation>");
                xml.append("<address>");
                xml.append(aData.getLogs().get(j).getContact().getAgency().getAddress());
                xml.append("</address>");
                xml.append("<www>");
                xml.append(aData.getLogs().get(j).getContact().getAgency().getWww());
                xml.append("</www>");
                xml.append("<infos>");
                xml.append(aData.getLogs().get(j).getContact().getAgency().getInfos());
                xml.append("</infos>");
                xml.append("</agency>");
                xml.append("</contact>");
                xml.append("</log>");
            }
            xml.append("</logs>").append("<station_contacts>");
            for (int j = 0; j < aData.getStationContacts().size(); j++) {
                // *************************************************************
                // Handle Station Contact
                // *************************************************************
                xml.append("<station_contact>");
                xml.append("<role>");
                xml.append(aData.getStationContacts().get(j).getRole());
                xml.append("</role>");
                xml.append("<contact>");
                xml.append("<name>");
                xml.append(aData.getStationContacts().get(j).getContact().getName());
                xml.append("</name>");
                xml.append("<title>");
                xml.append(aData.getStationContacts().get(j).getContact().getTitle());
                xml.append("</title>");
                xml.append("<email>");
                xml.append(aData.getStationContacts().get(j).getContact().getEmail());
                xml.append("</email>");
                xml.append("<phone>");
                xml.append(aData.getStationContacts().get(j).getContact().getPhone());
                xml.append("</phone>");
                xml.append("<gsm>");
                xml.append(aData.getStationContacts().get(j).getContact().getGsm());
                xml.append("</gsm>");
                xml.append("<comment>");
                xml.append(aData.getStationContacts().get(j).getContact().getComment());
                xml.append("</comment>");
                xml.append("<role>");
                xml.append(aData.getStationContacts().get(j).getContact().getRole());
                xml.append("</role>");
                xml.append("<agency>");
                xml.append("<name>");
                xml.append(aData.getStationContacts().get(j).getContact().getAgency().getName());
                xml.append("</name>");
                xml.append("<abbreviation>");
                xml.append(aData.getStationContacts().get(j).getContact().getAgency().getAbbreviation());
                xml.append("</abbreviation>");
                xml.append("<address>");
                xml.append(aData.getStationContacts().get(j).getContact().getAgency().getAddress());
                xml.append("</address>");
                xml.append("<www>");
                xml.append(aData.getStationContacts().get(j).getContact().getAgency().getWww());
                xml.append("</www>");
                xml.append("<infos>");
                xml.append(aData.getStationContacts().get(j).getContact().getAgency().getInfos());
                xml.append("</infos>");
                xml.append("</agency>");
                xml.append("</contact>");
                xml.append("</station_contact>");
            }
            xml.append("</station_contacts>").append("<station_networks>");
            for (int j = 0; j < aData.getStationNetworks().size(); j++) {
                // *************************************************************
                // Handle Station Network
                // *************************************************************
                xml.append("<station_network>");
                xml.append("<network>");
                xml.append("<name>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getName());
                xml.append("</name>");
                xml.append("<contact>");
                xml.append("<name>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getName());
                xml.append("</name>");
                xml.append("<title>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getTitle());
                xml.append("</title>");
                xml.append("<email>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getEmail());
                xml.append("</email>");
                xml.append("<phone>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getPhone());
                xml.append("</phone>");
                xml.append("<gsm>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getGsm());
                xml.append("</gsm>");
                xml.append("<comment>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getComment());
                xml.append("</comment>");
                xml.append("<role>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getRole());
                xml.append("</role>");
                xml.append("<agency>");
                xml.append("<name>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getName());
                xml.append("</name>");
                xml.append("<abbreviation>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getAbbreviation());
                xml.append("</abbreviation>");
                xml.append("<address>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getAddress());
                xml.append("</address>");
                xml.append("<www>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getWww());
                xml.append("</www>");
                xml.append("<infos>");
                xml.append(aData.getStationNetworks().get(j).getNetwork().getContact().getAgency().getInfos());
                xml.append("</infos>");
                xml.append("</agency>");
                xml.append("</contact>");
                xml.append("</network>");
                xml.append("</station_network>");
            }
            xml.append("</station_networks>").append("<station_items>");
            for (int j = 0; j < aData.getStationItems().size(); j++) {
                // *************************************************************
                // Handle Station Item
                // *************************************************************
                xml.append("<station_item>");
                xml.append("<date_from>");
                xml.append(aData.getStationItems().get(j).getDate_from());
                xml.append("</date_from>");
                xml.append("<date_to>");
                xml.append(aData.getStationItems().get(j).getDate_to());
                xml.append("</date_to>");
                xml.append("<item>");
                xml.append("<comment>");
                xml.append(aData.getStationItems().get(j).getItem().getComment());
                xml.append("</comment>");
                xml.append("<item_type>");
                xml.append("<name>");
                xml.append(aData.getStationItems().get(j).getItem().getItem_type().getName());
                xml.append("</name>");
                xml.append("</item_type>");
                xml.append("<contact_as_producer>");
                xml.append("<name>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getName());
                xml.append("</name>");
                xml.append("<title>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getTitle());
                xml.append("</title>");
                xml.append("<email>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getEmail());
                xml.append("</email>");
                xml.append("<phone>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getPhone());
                xml.append("</phone>");
                xml.append("<gsm>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getGsm());
                xml.append("</gsm>");
                xml.append("<comment>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getComment());
                xml.append("</comment>");
                xml.append("<role>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getRole());
                xml.append("</role>");
                xml.append("<agency_as_producer>");
                xml.append("<name>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getName());
                xml.append("</name>");
                xml.append("<abbreviation>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAbbreviation());
                xml.append("</abbreviation>");
                xml.append("<address>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAddress());
                xml.append("</address>");
                xml.append("<www>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getWww());
                xml.append("</www>");
                xml.append("<infos>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getInfos());
                xml.append("</infos>");
                xml.append("</agency_as_producer>");
                xml.append("</contact_as_producer>");
                xml.append("<contact_as_owner>");
                xml.append("<name>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getName());
                xml.append("</name>");
                xml.append("<title>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getTitle());
                xml.append("</title>");
                xml.append("<email>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getEmail());
                xml.append("</email>");
                xml.append("<phone>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getPhone());
                xml.append("</phone>");
                xml.append("<gsm>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getGsm());
                xml.append("</gsm>");
                xml.append("<comment>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getComment());
                xml.append("</comment>");
                xml.append("<role>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getRole());
                xml.append("</role>");
                xml.append("<agency_as_owner>");
                xml.append("<name>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getName());
                xml.append("</name>");
                xml.append("<abbreviation>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAbbreviation());
                xml.append("</abbreviation>");
                xml.append("<address>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAddress());
                xml.append("</address>");
                xml.append("<www>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getWww());
                xml.append("</www>");
                xml.append("<infos>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getInfos());
                xml.append("</infos>");
                xml.append("</agency_as_owner>");
                xml.append("</contact_as_owner>");
                xml.append("<item_attributes>");
                for (int k = 0; k < aData.getStationItems().get(j).getItem().GetItemAttribues().size(); k++) {
                    // *********************************************************
                    // Handle Item Attribute
                    // *********************************************************
                    xml.append("<item_attribute>");
                    xml.append("<date_from>");
                    xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getDate_from());
                    xml.append("</date_from>");
                    xml.append("<date_to>");
                    xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getDate_to());
                    xml.append("</date_to>");
                    xml.append("<value_varchar>");
                    xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_varchar());
                    xml.append("</value_varchar>");
                    xml.append("<value_date>");
                    xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_date());
                    xml.append("</value_date>");
                    xml.append("<value_numeric>");
                    xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getValue_numeric());
                    xml.append("</value_numeric>");
                    xml.append("<attribute>");
                    xml.append("<name>");
                    xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getAttribute().getName());
                    xml.append("</name>");
                    xml.append("</attribute>");
                    xml.append("<filter_receivers>");
                    for (int p = 0; p < aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().size(); p++) {
                        // *****************************************************
                        // Handle Filter Receiver
                        // *****************************************************
                        xml.append("<filter_receiver>");
                        xml.append("<filter_attribute>");
                        xml.append("<name>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getAttribute().getName());
                        xml.append("</name>");
                        xml.append("</filter_attribute>");
                        xml.append("<receiver_type>");
                        xml.append("<name>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getName());
                        xml.append("</name>");
                        xml.append("<igs_defined>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getIgs_defined());
                        xml.append("</igs_defined>");
                        xml.append("<model>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterReceivers().get(p).getReceiver_type().getModel());
                        xml.append("</model>");
                        xml.append("</receiver_type>");
                        xml.append("</filter_receiver>");
                    }
                    xml.append("</filter_receivers>").append("<filter_antennas>");
                    for (int p = 0; p < aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().size(); p++) {
                        // *****************************************************
                        // Handle Filter Antenna
                        // *****************************************************
                        xml.append("<filter_antenna>");
                        xml.append("<filter_attribute>");
                        xml.append("<name>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAttribute().getName());
                        xml.append("</name>");
                        xml.append("</filter_attribute>");
                        xml.append("<antenna_type>");
                        xml.append("<name>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getName());
                        xml.append("</name>");
                        xml.append("<igs_defined>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getIgs_defined());
                        xml.append("</igs_defined>");
                        xml.append("<model>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterAntennas().get(p).getAntenna_type().getModel());
                        xml.append("</model>");
                        xml.append("</antenna_type>");
                        xml.append("</filter_antenna>");
                    }
                    xml.append("</filter_antennas>").append("<filter_radomes>");
                    for (int p = 0; p < aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().size(); p++) {
                        // *****************************************************
                        // Handle Filter Radome
                        // *****************************************************
                        xml.append("<filter_radome>");
                        xml.append("<filter_attribute>");
                        xml.append("<name>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getAttribute().getName());
                        xml.append("</name>");
                        xml.append("</filter_attribute>");
                        xml.append("<radome_type>");
                        xml.append("<name>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getName());
                        xml.append("</name>");
                        xml.append("<igs_defined>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getIgs_defined());
                        xml.append("</igs_defined>");
                        xml.append("<description>");
                        xml.append(aData.getStationItems().get(j).getItem().GetItemAttribues().get(k).getFilterRadomes().get(p).getRadome_type().getDescription());
                        xml.append("</description>");
                        xml.append("</radome_type>");
                        xml.append("</filter_radome>");
                    }
                    xml.append("</filter_radomes>").append("</item_attribute>");
                }
                xml.append("</item_attributes>");
                xml.append("</item>");
                xml.append("</station_item>");
            }
            xml.append("</station_items>").append("<files_generated>");
            for (int j = 0; j < aData.getFilesGenerated().size(); j++) {
                // *************************************************************
                // Handle File Generated
                // *************************************************************
                xml.append("<file_generated>");
                xml.append("<file_type>");
                xml.append("<format>");
                xml.append(aData.getFilesGenerated().get(j).getFile_type().getFormat());
                xml.append("</format>");
                xml.append("</file_type>");
                xml.append("</file_generated>");
            }
            xml.append("</files_generated>").append("<documents>");
            for (int j = 0; j < aData.getDocument().size(); j++) {
                // *************************************************************
                // Handle Document
                // *************************************************************
                xml.append("<document>");
                xml.append("<date>");
                xml.append(aData.getDocument().get(j).getDate());
                xml.append("</date>");
                xml.append("<title>");
                xml.append(aData.getDocument().get(j).getTitle());
                xml.append("</title>");
                xml.append("<description>");
                xml.append(aData.getDocument().get(j).getDescription());
                xml.append("</description>");
                xml.append("<link>");
                xml.append(aData.getDocument().get(j).getLink());
                xml.append("</link>");
                xml.append("<item>");
                xml.append("<comment>");
                xml.append(aData.getStationItems().get(j).getItem().getComment());
                xml.append("</comment>");
                xml.append("<item_type>");
                xml.append("<name>");
                xml.append(aData.getDocument().get(j).getItem().getItem_type().getName());
                xml.append("</name>");
                xml.append("</item_type>");
                xml.append("<contact_as_producer>");
                xml.append("<name>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getName());
                xml.append("</name>");
                xml.append("<title>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getTitle());
                xml.append("</title>");
                xml.append("<email>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getEmail());
                xml.append("</email>");
                xml.append("<phone>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getPhone());
                xml.append("</phone>");
                xml.append("<gsm>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getGsm());
                xml.append("</gsm>");
                xml.append("<comment>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getComment());
                xml.append("</comment>");
                xml.append("<role>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getRole());
                xml.append("</role>");
                xml.append("<agency_as_producer>");
                xml.append("<name>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getName());
                xml.append("</name>");
                xml.append("<abbreviation>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAbbreviation());
                xml.append("</abbreviation>");
                xml.append("<address>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getAddress());
                xml.append("</address>");
                xml.append("<www>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getWww());
                xml.append("</www>");
                xml.append("<infos>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_producer().getAgency().getInfos());
                xml.append("</infos>");
                xml.append("</agency_as_producer>");
                xml.append("</contact_as_producer>");
                xml.append("<contact_as_owner>");
                xml.append("<name>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getName());
                xml.append("</name>");
                xml.append("<title>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getTitle());
                xml.append("</title>");
                xml.append("<email>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getEmail());
                xml.append("</email>");
                xml.append("<phone>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getPhone());
                xml.append("</phone>");
                xml.append("<gsm>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getGsm());
                xml.append("</gsm>");
                xml.append("<comment>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getComment());
                xml.append("</comment>");
                xml.append("<role>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getRole());
                xml.append("</role>");
                xml.append("<agency_as_owner>");
                xml.append("<name>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getName());
                xml.append("</name>");
                xml.append("<abbreviation>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAbbreviation());
                xml.append("</abbreviation>");
                xml.append("<address>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getAddress());
                xml.append("</address>");
                xml.append("<www>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getWww());
                xml.append("</www>");
                xml.append("<infos>");
                xml.append(aData.getStationItems().get(j).getItem().getContact_as_owner().getAgency().getInfos());
                xml.append("</infos>");
                xml.append("</agency_as_owner>");
                xml.append("</contact_as_owner>");
                xml.append("<document_type>");
                xml.append("<name>");
                xml.append(aData.getDocument().get(j).getDocument_type().getName());
                xml.append("</name>");
                xml.append("</document_type>");
                xml.append("</item>");
                xml.append("</document>");
            }
            xml.append("</documents>").append("</station>");
        }

        xml.append("</stations>");

        return xml.toString();
    }


    public String getFullXMLTimeSeries(ArrayList<Estimated_coordinates> data){

        StringBuilder xml = new StringBuilder();

        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        xml.append("<TimeSeries>");

        for (Estimated_coordinates aData : data) {
            xml.append("<coordinates>");
            xml.append("<epoch>").append(aData.getEpoch()).append("</epoch>");
            xml.append("<x>").append(aData.getX()).append("</x>");
            xml.append("<y>").append(aData.getY()).append("</y>");
            xml.append("<z>").append(aData.getZ()).append("</z>");
            xml.append("<var_xx>").append(aData.getVar_xx()).append("</var_xx>");
            xml.append("<var_yy>").append(aData.getVar_yy()).append("</var_yy>");
            xml.append("<var_zz>").append(aData.getVar_zz()).append("</var_zz>");
            xml.append("<var_xy>").append(aData.getVar_xy()).append("</var_xy>");
            xml.append("<var_xz>").append(aData.getVar_xz()).append("</var_xz>");
            xml.append("<var_yz>").append(aData.getVar_yz()).append("</var_yz>");
            xml.append("</coordinates>");
        }

        xml.append("</TimeSeries>");

        return xml.toString();
    }


    public String getFullXMLCoordinates(ArrayList<Estimated_coordinates> data){

        StringBuilder xml = new StringBuilder();

        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        xml.append("<Coordinates>");

        for (Estimated_coordinates aData : data) {
            xml.append("<coordinates>");
            xml.append("<epoch>").append(aData.getEpoch()).append("</epoch>");
            xml.append("<x>").append(aData.getX()).append("</x>");
            xml.append("<y>").append(aData.getY()).append("</y>");
            xml.append("<z>").append(aData.getZ()).append("</z>");
            xml.append("<var_xx>").append(aData.getVar_xx()).append("</var_xx>");
            xml.append("<var_yy>").append(aData.getVar_yy()).append("</var_yy>");
            xml.append("<var_zz>").append(aData.getVar_yz()).append("</var_zz>");
            xml.append("<var_xy>").append(aData.getVar_xx()).append("</var_xy>");
            xml.append("<var_xz>").append(aData.getVar_yy()).append("</var_xz>");
            xml.append("<var_yz>").append(aData.getVar_yz()).append("</var_yz>");
            xml.append("</coordinates>");

        }

        xml.append("</Coordinates>");

        return xml.toString();
    }




    // *************************************************************************
    // *************************************************************************
    //
    // T2 PARSER METHODS
    //
    // *************************************************************************
    // *************************************************************************

    /*
    * GET FILE FULL CSV
    * This method returns the full file data in CSV.
    */
    public String getFileFullCSV(ArrayList<Rinex_file> data)
    {
        StringBuilder header = new StringBuilder().append("name, ").append("station_marker, ").append("acronym, ").append("data_center_hostname, ").append("file_size, ").append("file_type_name, ").append("relative_path, ").append("reference_date, ").append("published_date, ").append("creation_date, ").append("revision_date, ").append("md5checksum, ").append("md5uncompressed\n");

        StringBuilder csv = new StringBuilder();

        for (Rinex_file aData : data) {
            csv.append(aData.getName());
            csv.append(", ");
            csv.append(aData.getStation_marker());
            csv.append(", ");
            csv.append(aData.getData_center_structure().getDataCenter().getAcronym());
            csv.append(", ");
            csv.append(aData.getData_center_structure().getDataCenter().getHostname());
            csv.append(", ");
            csv.append(aData.getFile_size());
            csv.append(", ");
            csv.append(aData.getFile_type().getFormat());
            csv.append(", ");
            csv.append(aData.getRelative_path());
            csv.append(", ");
            csv.append(aData.getReference_date());
            csv.append(", ");
            csv.append(aData.getPublished_date());
            csv.append(", ");
            csv.append(aData.getCreation_date());
            csv.append(", ");
            csv.append(aData.getRevision_date());
            csv.append(", ");
            csv.append(aData.getMd5checksum());
            csv.append(", ");
            csv.append(aData.getMd5uncompressed());
            csv.append("\n");
        }

        header.append(csv);

        return header.toString();
    }

    /*
    * GET FILE FULL XML
    * This method returns the full file data in XML.
    */
    public String getFileFullXML(ArrayList<Rinex_file> data)
    {
        StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

        xml.append("<rinex_files>");

        for (Rinex_file aData : data) {
            // *****************************************************************
            // Handle File
            // *****************************************************************
            xml.append("<rinex_file>");
            xml.append("<name>").append(aData.getName()).append("</name>");
            xml.append("<station_marker>").append(aData.getStation_marker()).append("</station_marker>");
            xml.append("<file_size>").append(aData.getFile_size()).append("</file_size>");
            xml.append("<relative_path>").append(aData.getRelative_path()).append("</relative_path>");
            xml.append("<reference_date>").append(aData.getReference_date()).append("</reference_date>");
            xml.append("<published_date>").append(aData.getPublished_date()).append("</published_date>");
            xml.append("<creation_date>").append(aData.getCreation_date()).append("</creation_date>");
            xml.append("<revision_date>").append(aData.getRevision_date()).append("</revision_date>");
            xml.append("<md5_checksum>").append(aData.getMd5checksum()).append("</md5_checksum>");
            xml.append("<md5_uncompressed>").append(aData.getMd5uncompressed()).append("</md5_uncompressed>");

            // *****************************************************************
            // Handle Datacenter
            // *****************************************************************
            xml.append("<data_center>");
            xml.append("<acronym>").append(aData.getData_center_structure().getDataCenter().getAcronym()).append("</acronym>");
            xml.append("<hostname>").append(aData.getData_center_structure().getDataCenter().getHostname()).append("</hostname>");
            xml.append("</data_center>");

            // *****************************************************************
            // Handle File Type
            // *****************************************************************
            xml.append("<file_type>");
            //xml = xml + "<name>" + data.get(i).getFile_type().getName() + "</name>";
            xml.append("<format>").append(aData.getFile_type().getFormat()).append("</format>");
            xml.append("</file_type>");
            xml.append("</rinex_file>");
        }
        // Close list
        xml.append("</rinex_files>");

        return xml.toString();
    }

    /*
    * GET FILE FULL JSON
    * This method returns the full file data in JSON.
    */
    public JSONArray getFileFullJSON(ArrayList<Rinex_file> data)
    {
        JSONArray file_list = new JSONArray();

        for (Rinex_file aData : data) {
            // *****************************************************************
            // Handle Datacenter
            // *****************************************************************
            JSONObject datacenter = new JSONObject();
            datacenter.put("acronym", aData.getData_center_structure().getDataCenter().getAcronym());
            datacenter.put("hostname", aData.getData_center_structure().getDataCenter().getHostname());
            datacenter.put("protocol", aData.getData_center_structure().getDataCenter().getProtocol());

            // *****************************************************************
            // Handle Data center structure
            // *****************************************************************

            JSONObject data_center_structure = new JSONObject();

            data_center_structure.put("directory_naming", aData.getData_center_structure().getDirectory_naming());

            datacenter.put("data_center_structure", data_center_structure);

            // *****************************************************************
            // Handle File Type
            // *****************************************************************
            JSONObject filetype = new JSONObject();
            //filetype.put("name", data.get(i).getFile_type().getName());
            filetype.put("format", aData.getFile_type().getFormat());
            filetype.put("sampling_window", aData.getFile_type().getSampling_window());
            filetype.put("sampling_frequency", aData.getFile_type().getSampling_frequency());

            // *****************************************************************
            // Handle File
            // *****************************************************************
            JSONObject file = new JSONObject();
            file.put("name", aData.getName());
            file.put("station_marker", aData.getStation_marker());
            file.put("file_size", aData.getFile_size());
            file.put("relative_path", aData.getRelative_path());
            file.put("reference_date", aData.getReference_date());
            file.put("published_date", aData.getPublished_date());
            file.put("creation_date", aData.getCreation_date());
            file.put("revision_date", aData.getRevision_date());
            file.put("md5_checksum", aData.getMd5checksum());
            file.put("md5_uncompressed", aData.getMd5uncompressed());
            file.put("data_center", datacenter);
            file.put("file_type", filetype);
	    file.put("status", aData.getStatus());
            // Add file to the list
            file_list.add(file);
        }

        return file_list;
    }
    
    // *************************************************************************
    // *************************************************************************
    //
    // T3 PARSER METHODS
    //
    // *************************************************************************
    // ************************************************************************* 
    /*
    * GET FILE T3 FULL JSON
    * This method returns the full file T3 data in JSON.
    */
    public JSONArray getFileT3FullJSON(ArrayList<Rinex_file> data)
    {
        JSONArray file_list = new JSONArray();

        for (Rinex_file aData : data) {
            

            // *****************************************************************
            // Handle File
            // *****************************************************************
            JSONObject file = new JSONObject();
            file.put("name", aData.getName());
            file.put("reference_date", aData.getReference_date());
            
            
            // *****************************************************************
            // Handle Datacenter
            // *****************************************************************
            JSONObject datacenter = new JSONObject();
            if (aData.getQualityfile().getId() != 0 ){
                datacenter.put("acronym", aData.getQualityfile().getData_center_structure().getDataCenter().getAcronym());
                datacenter.put("hostname", aData.getQualityfile().getData_center_structure().getDataCenter().getHostname());
                datacenter.put("protocol", aData.getQualityfile().getData_center_structure().getDataCenter().getProtocol());

                // *****************************************************************
                // Handle Data center structure
                // *****************************************************************

                JSONObject data_center_structure = new JSONObject();

                data_center_structure.put("directory_naming", aData.getQualityfile().getData_center_structure().getDirectory_naming());

                datacenter.put("data_center_structure", data_center_structure);

                // *****************************************************************
                // Handle File Type
                // *****************************************************************
                JSONObject filetype = new JSONObject();
                filetype.put("format", aData.getQualityfile().getFile_type().getFormat());
                filetype.put("sampling_window", aData.getQualityfile().getFile_type().getSampling_window());
                filetype.put("sampling_frequency", aData.getQualityfile().getFile_type().getSampling_frequency());

                // *****************************************************************
                // Handle QUALITY FILE
                // *****************************************************************
                JSONObject quality_file = new JSONObject();
                quality_file.put("name", aData.getQualityfile().getName());
                quality_file.put("file_size", aData.getQualityfile().getFile_size());
                quality_file.put("relative_path", aData.getQualityfile().getRelative_path());
                quality_file.put("creation_date", aData.getQualityfile().getCreation_date());
                quality_file.put("revision_time", aData.getQualityfile().getRevision_time());
                quality_file.put("md5_checksum", aData.getQualityfile().getMd5checksum());
                quality_file.put("data_center", datacenter);
                quality_file.put("file_type", filetype);

                file.put("quality_file", quality_file);
            }



            // *****************************************************************
            // Handle QC REPORT SUMMARY
            // *****************************************************************
            JSONObject qc_report_summary = new JSONObject();
            
               
            // *************************************************************
            // Handle QC PARAMETERS
            // *************************************************************
            JSONObject qc_parameters = new JSONObject();
            qc_parameters.put("software_version", aData.getQCReportSummary().getQCParameters().getSoftwareVersion());
            qc_parameters.put("elevation_cutoff", aData.getQCReportSummary().getQCParameters().getElevationCutoff());
            qc_parameters.put("gps", aData.getQCReportSummary().getQCParameters().getGPS());
            qc_parameters.put("glo", aData.getQCReportSummary().getQCParameters().getGLO());
            qc_parameters.put("gal", aData.getQCReportSummary().getQCParameters().getGAL());
            qc_parameters.put("bds", aData.getQCReportSummary().getQCParameters().getBDS());
            qc_parameters.put("qzs", aData.getQCReportSummary().getQCParameters().getQZS());
            qc_parameters.put("sbs", aData.getQCReportSummary().getQCParameters().getSBS());
            qc_parameters.put("nav_gps", aData.getQCReportSummary().getQCParameters().getNavGPS());
            qc_parameters.put("nav_glo", aData.getQCReportSummary().getQCParameters().getNavGLO());
            qc_parameters.put("nav_gal", aData.getQCReportSummary().getQCParameters().getNavGAL());
            qc_parameters.put("nav_bds", aData.getQCReportSummary().getQCParameters().getNavBDS());
            qc_parameters.put("nav_qzs", aData.getQCReportSummary().getQCParameters().getNavQZS());
            qc_parameters.put("nav_sbs", aData.getQCReportSummary().getQCParameters().getNavSBS());
            qc_parameters.put("software_name", aData.getQCReportSummary().getQCParameters().getSoftwareName());
            // *************************************************************
            // Handle QC CONSTELLATION SUMMARY
            // *************************************************************
            JSONArray qc_constellation_summaries = new JSONArray();
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                // *********************************************************
                // Handle qc_observation_summary_s
                // *********************************************************

                JSONArray qc_observation_summaries_s = new JSONArray();
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    // *****************************************************
                    // Handle Gnss obsnames
                    // *****************************************************
                    JSONObject gnss_obsnames = new JSONObject();
                    gnss_obsnames.put("name", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getName());
                    gnss_obsnames.put("obstype", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getObsType());
                    gnss_obsnames.put("channel", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getChannel());
                    gnss_obsnames.put("frequency_band", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getFrequencyBand());
                    gnss_obsnames.put("official", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getOfficial());


                    // *****************************************************
                    // Handle qc_observation_summary_s
                    // *****************************************************
                    JSONObject qc_observation_summary_s = new JSONObject();
                    qc_observation_summary_s.put("obs_sats", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getObsSats());
                    qc_observation_summary_s.put("obs_have", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getObsHave());
                    qc_observation_summary_s.put("obs_expt", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getObsExpt());
                    qc_observation_summary_s.put("user_have", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getUserHave());
                    qc_observation_summary_s.put("user_expt", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getUserExpt());
                    qc_observation_summary_s.put("obsnames", gnss_obsnames);

                    qc_observation_summaries_s.add(qc_observation_summary_s);
                }

                // *********************************************************
                // Handle qc_observation_summary_c
                // *********************************************************

                JSONArray qc_observation_summaries_c = new JSONArray();
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    // *****************************************************
                    // Handle Gnss obsnames
                    // *****************************************************
                    JSONObject gnss_obsnames = new JSONObject();
                    gnss_obsnames.put("name", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getName());
                    gnss_obsnames.put("obstype", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getObsType());
                    gnss_obsnames.put("channel", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getChannel());
                    gnss_obsnames.put("frequency_band", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getFrequencyBand());
                    gnss_obsnames.put("official", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getOfficial());


                    // *****************************************************
                    // Handle qc_observation_summary_c
                    // *****************************************************
                    JSONObject qc_observation_summary_c = new JSONObject();
                    qc_observation_summary_c.put("obs_sats", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getObsSats());
                    qc_observation_summary_c.put("obs_have", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getObsHave());
                    qc_observation_summary_c.put("obs_expt", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getObsExpt());
                    qc_observation_summary_c.put("user_have", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getUserHave());
                    qc_observation_summary_c.put("user_expt", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getUserExpt());

                    if ( aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getCodMpth()== null )
                        qc_observation_summary_c.put("cod_mpth", null );
                    else
                    {
                        float codMpth = aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getCodMpth();
                        qc_observation_summary_c.put("cod_mpth", codMpth );
                    }


                    qc_observation_summary_c.put("obsnames", gnss_obsnames);

                    qc_observation_summaries_c.add(qc_observation_summary_c);
                }

                //*********************************************************
                // Handle qc_observation_summary_d
                // *********************************************************

                JSONArray qc_observation_summaries_d = new JSONArray();
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    // *****************************************************
                    // Handle Gnss obsnames
                    // *****************************************************
                    JSONObject gnss_obsnames = new JSONObject();
                    gnss_obsnames.put("name", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getName());
                    gnss_obsnames.put("obstype", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getObsType());
                    gnss_obsnames.put("channel", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getChannel());
                    gnss_obsnames.put("frequency_band", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getFrequencyBand());
                    gnss_obsnames.put("official", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getOfficial());


                    // *****************************************************
                    // Handle qc_observation_summary_d
                    // *****************************************************
                    JSONObject qc_observation_summary_d = new JSONObject();
                    qc_observation_summary_d.put("obs_sats", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getObsSats());
                    qc_observation_summary_d.put("obs_have", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getObsHave());
                    qc_observation_summary_d.put("obs_expt", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getObsExpt());
                    qc_observation_summary_d.put("user_have", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getUserHave());
                    qc_observation_summary_d.put("user_expt", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getUserExpt());
                    qc_observation_summary_d.put("obsnames", gnss_obsnames);

                    qc_observation_summaries_d.add(qc_observation_summary_d);
                }

                // *********************************************************
                // Handle qc_observation_summary_l
                // *********************************************************

                JSONArray qc_observation_summaries_l = new JSONArray();
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    // *****************************************************
                    // Handle Gnss obsnames
                    // *****************************************************
                    JSONObject gnss_obsnames = new JSONObject();
                    gnss_obsnames.put("name", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getName());
                    gnss_obsnames.put("obstype", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getObsType());
                    gnss_obsnames.put("channel", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getChannel());
                    gnss_obsnames.put("frequency_band", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getFrequencyBand());
                    gnss_obsnames.put("official", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getOfficial());


                    // *****************************************************
                    // Handle qc_observation_summary_l
                    // *****************************************************
                    JSONObject qc_observation_summary_l = new JSONObject();
                    qc_observation_summary_l.put("obs_sats", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getObsSats());
                    qc_observation_summary_l.put("obs_have", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getObsHave());
                    qc_observation_summary_l.put("obs_expt", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getObsExpt());
                    qc_observation_summary_l.put("user_have", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getUserHave());
                    qc_observation_summary_l.put("user_expt", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getUserExpt());
                    qc_observation_summary_l.put("pha_slps", aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getPhaSlps());
                    qc_observation_summary_l.put("obsnames", gnss_obsnames);

                    qc_observation_summaries_l.add(qc_observation_summary_l);
                }


                // *********************************************************
                // Handle Constellation
                // *********************************************************
                JSONObject constellation = new JSONObject();
                constellation.put("identifier_1ch", aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getIdentifier1Ch());
                constellation.put("identifier_3ch", aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getIdentifier3Ch());
                constellation.put("name", aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getName());
                constellation.put("official", aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getOfficial());
                
                // *********************************************************
                // Handle qc_constellation_summary
                // *********************************************************
                JSONObject qc_constellation_summary = new JSONObject();
                qc_constellation_summary.put("nsat", aData.getQCReportSummary().getQCConstellationSummary().get(k).getNsat());
                qc_constellation_summary.put("xele", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXele());
                qc_constellation_summary.put("epo_expt", aData.getQCReportSummary().getQCConstellationSummary().get(k).getEpoExpt());
                qc_constellation_summary.put("epo_have", aData.getQCReportSummary().getQCConstellationSummary().get(k).getEpoHave());
                qc_constellation_summary.put("epo_usbl", aData.getQCReportSummary().getQCConstellationSummary().get(k).getEpoUsbl());
                qc_constellation_summary.put("xcod_epo", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXcodEpo());
                qc_constellation_summary.put("xcod_sat", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXcodSat());
                qc_constellation_summary.put("xpha_epo", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXphaEpo());
                qc_constellation_summary.put("xpha_sat", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXphaSat());
                qc_constellation_summary.put("xint_epo", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintEpo());
                qc_constellation_summary.put("xint_sat", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintSat());
                qc_constellation_summary.put("xint_sig", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintSig());
                qc_constellation_summary.put("xint_slp", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintSlp());
                qc_constellation_summary.put("x_crd", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXCrd());
                qc_constellation_summary.put("y_crd", aData.getQCReportSummary().getQCConstellationSummary().get(k).getYCrd());
                qc_constellation_summary.put("z_crd", aData.getQCReportSummary().getQCConstellationSummary().get(k).getZCrd());
                qc_constellation_summary.put("x_rms", aData.getQCReportSummary().getQCConstellationSummary().get(k).getXRms());
                qc_constellation_summary.put("y_rms", aData.getQCReportSummary().getQCConstellationSummary().get(k).getYRms());
                qc_constellation_summary.put("z_rms", aData.getQCReportSummary().getQCConstellationSummary().get(k).getZRms());
                qc_constellation_summary.put("constellation", constellation);
                qc_constellation_summary.put("qc_observation_summary_s", qc_observation_summaries_s);
                qc_constellation_summary.put("qc_observation_summary_c", qc_observation_summaries_c);
                qc_constellation_summary.put("qc_observation_summary_d", qc_observation_summaries_d);
                qc_constellation_summary.put("qc_observation_summary_l", qc_observation_summaries_l);


                qc_constellation_summaries.add(qc_constellation_summary);
            }

            // *************************************************************
            // Handle QC NAVIGATION MSG
            // *************************************************************
            JSONArray qc_navigation_msgs = new JSONArray();
            for (int k = 0; k < aData.getQCReportSummary().getQCNavigationMsg().size(); k++) 
            {
                // *********************************************************
                // Handle Constellation
                // *********************************************************
                JSONObject constellation = new JSONObject();
                constellation.put("identifier_1ch", aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getIdentifier1Ch());
                constellation.put("identifier_3ch", aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getIdentifier3Ch());
                constellation.put("name", aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getName());
                constellation.put("official", aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getOfficial());
                
                // *********************************************************
                // Handle qc_navigation_msg
                // *********************************************************
                JSONObject qc_navigation_msg = new JSONObject();
                qc_navigation_msg.put("nsat", aData.getQCReportSummary().getQCNavigationMsg().get(k).getNsat());
                qc_navigation_msg.put("have", aData.getQCReportSummary().getQCNavigationMsg().get(k).getHave());
                qc_navigation_msg.put("constellation", constellation);
                
                qc_navigation_msgs.add(qc_navigation_msg);
            }
            
            qc_report_summary.put("date_beg", aData.getQCReportSummary().getDateBeg());
            qc_report_summary.put("date_end", aData.getQCReportSummary().getDateEnd());
            qc_report_summary.put("data_smp", aData.getQCReportSummary().getDataSmp());
            qc_report_summary.put("obs_elev", aData.getQCReportSummary().getObsElev());
            qc_report_summary.put("obs_have", aData.getQCReportSummary().getObsHave());
            qc_report_summary.put("obs_expt", aData.getQCReportSummary().getObsExpt());
            qc_report_summary.put("user_have", aData.getQCReportSummary().getUserHave());
            qc_report_summary.put("user_expt", aData.getQCReportSummary().getUserExpt());
            qc_report_summary.put("cyc_slps", aData.getQCReportSummary().getCycSlps());
            qc_report_summary.put("clk_jmps", aData.getQCReportSummary().getClkJmps());
            qc_report_summary.put("xbeg", aData.getQCReportSummary().getXBeg());
            qc_report_summary.put("xend", aData.getQCReportSummary().getXEnd());
            qc_report_summary.put("xint", aData.getQCReportSummary().getXInt());
            qc_report_summary.put("xsys", aData.getQCReportSummary().getXSys());
            qc_report_summary.put("qc_constellation_summary", qc_constellation_summaries);
            qc_report_summary.put("qc_parameters", qc_parameters);
            qc_report_summary.put("qc_navigation_msg", qc_navigation_msgs);    
       
          
            file.put("qc_report_summary", qc_report_summary);
           //file.put("quality_file", quality_file); moved to the above
            
            // Add file to the list
            file_list.add(file);
        }

        return file_list;
    }

    /*
    * GET FILE T3 FULL XML
    * This method returns the full file T3 data in XML.
    */
    public String getFileT3FullXML(ArrayList<Rinex_file> data)
    {
        StringBuilder xml = new StringBuilder("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");

        xml.append("<rinex_files>");

        for (Rinex_file aData : data) {
            // *****************************************************************
            // Handle File
            // *****************************************************************
            xml.append("<rinex_file>");
            xml.append("<name>").append(aData.getName()).append("</name>");
            xml.append("<reference_date>").append(aData.getReference_date()).append("</reference_date>");
            
            // *****************************************************************
            // Handle QUALITY FILE
            // *****************************************************************
            xml.append("<quality_file>");
            xml.append("<name>").append(aData.getQualityfile().getName()).append("</name>");
            xml.append("<file_size>").append(aData.getQualityfile().getFile_size()).append("</file_size>");
            xml.append("<relative_path>").append(aData.getQualityfile().getRelative_path()).append("</relative_path>");
            xml.append("<creation_date>").append(aData.getQualityfile().getCreation_date()).append("</creation_date>");
            xml.append("<revision_time>").append(aData.getQualityfile().getRevision_time()).append("</revision_time>");
            xml.append("<md5_checksum>").append(aData.getQualityfile().getMd5checksum()).append("</md5_checksum>");
            // *****************************************************************
            // Handle Datacenterstructure
            // *****************************************************************
            xml.append("<data_center_structure>");
            // *****************************************************************
            // Handle Datacenter
            // *****************************************************************
            xml.append("<data_center>");
            xml.append("<acronym>").append(aData.getQualityfile().getData_center_structure().getDataCenter().getAcronym()).append("</acronym>");
            xml.append("<hostname>").append(aData.getQualityfile().getData_center_structure().getDataCenter().getHostname()).append("</hostname>");
            xml.append("<protocol>").append(aData.getQualityfile().getData_center_structure().getDataCenter().getHostname()).append("</protocol>");
            xml.append("</data_center>");

            
            // *****************************************************************
            // Handle File Type
            // *****************************************************************
            xml.append("<file_type>");
            xml.append("<format>").append(aData.getQualityfile().getFile_type().getFormat()).append("</format>");
            xml.append("<sampling_window>").append(aData.getQualityfile().getFile_type().getSampling_window()).append("</sampling_window>");
            xml.append("<sampling_frequency>").append(aData.getQualityfile().getFile_type().getSampling_frequency()).append("</sampling_frequency>");
            xml.append("</file_type>");
            
            xml.append("<directory_naming>").append(aData.getQualityfile().getData_center_structure().getDirectory_naming()).append("</directory_naming>");
            xml.append("</data_center_structure>");
            
            xml.append("</quality_file>");
            // *****************************************************************
            // Handle QC Report Summary
            // *****************************************************************
            xml.append("<qc_report_sumary>");
            // *****************************************************************
            // Handle QC Parameters
            // *****************************************************************
            xml.append("<qc_parameters>");
            xml.append("<software_version>").append(aData.getQCReportSummary().getQCParameters().getSoftwareVersion()).append("</software_version>");
            xml.append("<elevation_cutoff>").append(aData.getQCReportSummary().getQCParameters().getElevationCutoff()).append("</elevation_cutoff>");
            xml.append("<gps>").append(aData.getQCReportSummary().getQCParameters().getGPS()).append("</gps>");
            xml.append("<glo>").append(aData.getQCReportSummary().getQCParameters().getGLO()).append("</glo>");
            xml.append("<gal>").append(aData.getQCReportSummary().getQCParameters().getGAL()).append("</gal>");
            xml.append("<bds>").append(aData.getQCReportSummary().getQCParameters().getBDS()).append("</bds>");
            xml.append("<qzs>").append(aData.getQCReportSummary().getQCParameters().getQZS()).append("</qzs>");
            xml.append("<sbs>").append(aData.getQCReportSummary().getQCParameters().getSBS()).append("</sbs>");
            xml.append("<nav_gps>").append(aData.getQCReportSummary().getQCParameters().getNavGPS()).append("</nav_gps>");
            xml.append("<nav_glo>").append(aData.getQCReportSummary().getQCParameters().getNavGLO()).append("</nav_glo>");
            xml.append("<nav_gal>").append(aData.getQCReportSummary().getQCParameters().getNavGAL()).append("</nav_gal>");
            xml.append("<nav_bds>").append(aData.getQCReportSummary().getQCParameters().getNavBDS()).append("</nav_bds>");
            xml.append("<nav_qzs>").append(aData.getQCReportSummary().getQCParameters().getNavQZS()).append("</nav_qzs>");
            xml.append("<nav_sbs>").append(aData.getQCReportSummary().getQCParameters().getNavSBS()).append("</nav_sbs>");
            xml.append("<software_name>").append(aData.getQCReportSummary().getQCParameters().getSoftwareName()).append("</software_name>");
            xml.append("</qc_parameters>");
            
            // *************************************************************
            // Handle QC CONSTELLATION SUMMARY
            // *************************************************************
            
            xml.append("<qc_constellation_summaries>");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                xml.append("<qc_constellation_summary>");
                xml.append("<nsat>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getNsat()).append("</nsat>");
                xml.append("<xele>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXele()).append("</xele>");
                xml.append("<epo_expt>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getEpoExpt()).append("</epo_expt>");
                xml.append("<epo_have>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getEpoHave()).append("</epo_have>");
                xml.append("<epo_usbl>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getEpoUsbl()).append("</epo_usbl>");
                xml.append("<xcod_epo>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXcodEpo()).append("</xcod_epo>");
                xml.append("<xcod_sat>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXcodSat()).append("</xcod_sat>");
                xml.append("<xpha_epo>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXphaEpo()).append("</xpha_epo>");
                xml.append("<xpha_sat>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXphaSat()).append("</xpha_sat>");
                xml.append("<xint_epo>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintEpo()).append("</xint_epo>");
                xml.append("<xint_sat>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintSat()).append("</xint_sat>");
                xml.append("<xint_sig>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintSig()).append("</xint_sig>");
                xml.append("<xint_slp>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintSlp()).append("</xint_slp>");
                xml.append("<x_crd>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXCrd()).append("</x_crd>");
                xml.append("<y_crd>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getYCrd()).append("</y_crd>");
                xml.append("<z_crd>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getZCrd()).append("</z_crd>");
                xml.append("<x_rms>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXRms()).append("</x_rms>");
                xml.append("<y_rms>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getYRms()).append("</y_rms>");
                xml.append("<z_rms>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getZRms()).append("</z_rms>");
                
                // *********************************************************
                // Handle Constellation
                // *********************************************************
                xml.append("<constellation>");
                xml.append("<identifier_1ch>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getIdentifier1Ch()).append("</identifier_1ch>");
                xml.append("<identifier_3ch>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getIdentifier3Ch()).append("</identifier_3ch>");
                xml.append("<name>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getName()).append("</name>");
                xml.append("<official>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getOfficial()).append("</official>");
                xml.append("</constellation>");
                // *********************************************************
                // Handle qc_observation_summary_s
                // *********************************************************
                xml.append("<qc_observation_summary_s>");
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    xml.append("<obs_sats>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getObsSats()).append("</obs_sats>");
                    xml.append("<obs_have>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getObsHave()).append("</obs_have>");
                    xml.append("<obs_expt>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getObsExpt()).append("</obs_expt>");
                    xml.append("<user_have>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getUserHave()).append("</user_have>");
                    xml.append("<user_expt>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getUserExpt()).append("</user_expt>");
                    // *****************************************************
                    // Handle Gnss obsnames
                    // *****************************************************
                    xml.append("<gnss_obsnames>");
                    xml.append("<name>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getName()).append("</name>");
                    xml.append("<obstype>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getObsType()).append("</obstype>");
                    xml.append("<channel>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getChannel()).append("</channel>");
                    xml.append("<frequency_band>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getFrequencyBand()).append("</frequency_band>");
                    xml.append("<official>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getOfficial()).append("</official>");
                    xml.append("</gnss_obsnames>");
                }
                xml.append("</qc_observation_summary_s>");
                // *********************************************************
                // Handle qc_observation_summary_c
                // *********************************************************
                xml.append("<qc_observation_summary_c>");
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    xml.append("<obs_sats>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getObsSats()).append("</obs_sats>");
                    xml.append("<obs_have>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getObsHave()).append("</obs_have>");
                    xml.append("<obs_expt>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getObsExpt()).append("</obs_expt>");
                    xml.append("<user_have>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getUserHave()).append("</user_have>");
                    xml.append("<user_expt>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getUserExpt()).append("</user_expt>");
                    xml.append("<cod_mpth>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getCodMpth()).append("</cod_mpth>");
                    // *****************************************************
                    // Handle Gnss obsnames
                    // *****************************************************
                    xml.append("<gnss_obsnames>");
                    xml.append("<name>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getName()).append("</name>");
                    xml.append("<obstype>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getObsType()).append("</obstype>");
                    xml.append("<channel>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getChannel()).append("</channel>");
                    xml.append("<frequency_band>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getFrequencyBand()).append("</frequency_band>");
                    xml.append("<official>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getOfficial()).append("</official>");
                    xml.append("</gnss_obsnames>");
                }
                xml.append("</qc_observation_summary_c>");
                // *********************************************************
                // Handle qc_observation_summary_d
                // *********************************************************
                xml.append("<qc_observation_summary_d>");
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    xml.append("<obs_sats>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getObsSats()).append("</obs_sats>");
                    xml.append("<obs_have>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getObsHave()).append("</obs_have>");
                    xml.append("<obs_expt>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getObsExpt()).append("</obs_expt>");
                    xml.append("<user_have>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getUserHave()).append("</user_have>");
                    xml.append("<user_expt>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getUserExpt()).append("</user_expt>");
                    // *****************************************************
                    // Handle Gnss obsnames
                    // *****************************************************
                    xml.append("<gnss_obsnames>");
                    xml.append("<name>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getName()).append("</name>");
                    xml.append("<obstype>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getObsType()).append("</obstype>");
                    xml.append("<channel>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getChannel()).append("</channel>");
                    xml.append("<frequency_band>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getFrequencyBand()).append("</frequency_band>");
                    xml.append("<official>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getOfficial()).append("</official>");
                    xml.append("</gnss_obsnames>");
                }
                xml.append("</qc_observation_summary_d>");
                // *********************************************************
                // Handle qc_observation_summary_l
                // *********************************************************
                xml.append("<qc_observation_summary_l>");
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    xml.append("<obs_sats>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getObsSats()).append("</obs_sats>");
                    xml.append("<obs_have>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getObsHave()).append("</obs_have>");
                    xml.append("<obs_expt>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getObsExpt()).append("</obs_expt>");
                    xml.append("<user_have>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getUserHave()).append("</user_have>");
                    xml.append("<user_expt>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getUserExpt()).append("</user_expt>");
                    xml.append("<pha_slps>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getPhaSlps()).append("</pha_slps>");
                    // *****************************************************
                    // Handle Gnss obsnames
                    // *****************************************************
                    xml.append("<gnss_obsnames>");
                    xml.append("<name>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getName()).append("</name>");
                    xml.append("<obstype>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getObsType()).append("</obstype>");
                    xml.append("<channel>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getChannel()).append("</channel>");
                    xml.append("<frequency_band>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getFrequencyBand()).append("</frequency_band>");
                    xml.append("<official>").append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getOfficial()).append("</official>");
                    xml.append("</gnss_obsnames>");
                }
                xml.append("</qc_observation_summary_l>");
                xml.append("</qc_constellation_summary>");
            }
            xml.append("</qc_constellation_summaries>");
            
            // *************************************************************
            // Handle QC NAVIGATION MSG
            // *************************************************************
            
            xml.append("<qc_navigation_msgs>");
            for (int k = 0; k < aData.getQCReportSummary().getQCNavigationMsg().size(); k++) 
            {
                
                // *********************************************************
                // Handle qc_navigation_msg
                // *********************************************************
                xml.append("<qc_navigation_msg>");
                // *********************************************************
                // Handle Constellation
                // *********************************************************
                xml.append("<constellation>");
                xml.append("<identifier_1ch>").append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getIdentifier1Ch()).append("</identifier_1ch>");
                xml.append("<identifier_3ch>").append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getIdentifier3Ch()).append("</identifier_3ch>");
                xml.append("<name>").append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getName()).append("</name>");
                xml.append("<official>").append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getOfficial()).append("</official>");
                xml.append("</constellation>");
                xml.append("<nsat>").append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getNsat()).append("</nsat>");
                xml.append("<have>").append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getHave()).append("</have>");
                xml.append("</qc_navigation_msg>");
            }
            xml.append("</qc_navigation_msgs>");
            xml.append("</qc_report_sumary>");
            
            xml.append("</rinex_file>");
                        
        }
        // Close list
        xml.append("</rinex_files>");

        return xml.toString();
    }
  
    /*
    * GET FILE T3 FULL CSV
    * This method returns the full file T3 data in CSV.
    */
    public String getFileT3FullCSV(ArrayList<Rinex_file> data)
    {
        StringBuilder header = new StringBuilder();

        header.append("name, ").append("reference_date, ");
        header.append("name, ").append("file_size, ").append("relative_path, ").append("creation_date, ").append("revision_time, ").append("md5checksum, ");
        header.append("acronym, ").append("hostname, ").append("protocol, ");
        header.append("format, ").append("sampling_window, ").append("sampling_frequency, ").append("directory_naming, ");
        header.append("software_version, ").append("elevation_cutoff, ").append("gps, ").append("glo, ").append("gal, ").append("bds, ").append("qzs, ").append("sbs, ").append("nav_gps, ").append("nav_glo, ").append("nav_gal, ").append("nav_bds, ").append("nav_qzs, ").append("nav_sbs, ").append("software_name, ");
        header.append("nsat, ").append("xele, ").append("epo_expt, ").append("epo_have, ").append("epo_usbl, ").append("xcod_epo, ").append("xcod_sat, ").append("xpha_epo, ").append("xpha_sat, ").append("xint_epo, ").append("xint_sat, ").append("xint_sig, ").append("xint_slp, ").append("x_crd, ").append("y_crd, ").append("z_crd, ").append("x_rms, ").append("y_rms, ").append("z_rms, ");
        header.append("identifier_1ch, ").append("identifier_3ch, ").append("name, ").append("official, ");
        header.append("obs_sats, ").append("obs_have, ").append("obs_expt, ").append("user_have, ").append("user_expt, ").append("name, ").append("obstype, ").append("channel, ").append("frequency_band, ").append("official, ");
        header.append("obs_sats, ").append("obs_have, ").append("obs_expt, ").append("user_have, ").append("user_expt, ").append("cod_mpth, ").append("name, ").append("obstype, ").append("channel, ").append("frequency_band, ").append("official, ");
        header.append("obs_sats, ").append("obs_have, ").append("obs_expt, ").append("user_have, ").append("user_expt, ").append("name, ").append("obstype, ").append("channel, ").append("frequency_band, ").append("official, ");
        header.append("obs_sats, ").append("obs_have, ").append("obs_expt, ").append("user_have, ").append("user_expt, ").append("pha_slps, ").append("name, ").append("obstype, ").append("channel, ").append("frequency_band, ").append("official, ");
        header.append("identifier_1ch, ").append("identifier_3ch, ").append("name, ").append("official, ").append("nsat, ").append("have\n");
        
        StringBuilder csv = new StringBuilder();

        for (Rinex_file aData : data) {
            csv.append(aData.getName());
            csv.append(", ");
            csv.append(aData.getReference_date());
            csv.append(", ");
            csv.append(aData.getQualityfile().getName());
            csv.append(", ");
            csv.append(aData.getQualityfile().getFile_size());
            csv.append(", ");
            csv.append(aData.getQualityfile().getRelative_path());
            csv.append(", ");
            csv.append(aData.getQualityfile().getCreation_date());
            csv.append(", ");
            csv.append(aData.getQualityfile().getRevision_time());
            csv.append(", ");
            csv.append(aData.getQualityfile().getMd5checksum());
            csv.append(", ");
            csv.append(aData.getCreation_date());
            csv.append(", ");
            csv.append(aData.getRevision_date());
            csv.append(", ");
            csv.append(aData.getMd5checksum());
            csv.append(", ");
            csv.append(aData.getQualityfile().getData_center_structure().getDataCenter().getAcronym());
            csv.append(", ");
            csv.append(aData.getQualityfile().getData_center_structure().getDataCenter().getHostname());
            csv.append(", ");
            csv.append(aData.getQualityfile().getData_center_structure().getDataCenter().getProtocol());
            csv.append(", ");
            csv.append(aData.getQualityfile().getFile_type().getFormat());
            csv.append(", ");
            csv.append(aData.getQualityfile().getFile_type().getSampling_window());
            csv.append(", ");
            csv.append(aData.getQualityfile().getFile_type().getSampling_frequency());
            csv.append(", ");
            csv.append(aData.getQualityfile().getData_center_structure().getDirectory_naming());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getSoftwareVersion());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getElevationCutoff());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getGPS());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getGLO());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getGAL());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getBDS());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getQZS());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getSBS());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getNavGPS());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getNavGLO());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getNavGAL());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getNavBDS());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getNavQZS());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getNavSBS());
            csv.append(", ");
            csv.append(aData.getQCReportSummary().getQCParameters().getSoftwareName());
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getNsat());    
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXele());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getEpoExpt());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getEpoHave());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getEpoUsbl());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXcodEpo());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXcodSat());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXphaEpo());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXphaSat());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintEpo());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintSat());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintSig());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXintSlp());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXCrd());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getYCrd());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getZCrd());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getXRms());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getYRms());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getZRms());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getIdentifier1Ch());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getIdentifier3Ch());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getName());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getConstellation().getOfficial());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getObsSats());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getObsHave());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getObsExpt());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getUserHave());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getUserExpt());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getName());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getObsType());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getChannel());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getFrequencyBand());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryS().get(p).getGnssObsnames().getOfficial());
                    csv.append(" | ");
                }
            }
            csv.append(", ");       
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getObsSats());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getObsHave());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getObsExpt());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getUserHave());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getUserExpt());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getCodMpth());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getName());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getObsType());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getChannel());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getFrequencyBand());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryC().get(p).getGnssObsnames().getOfficial());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getObsSats());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getObsHave());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getObsExpt());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getUserHave());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getUserExpt());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getName());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getObsType());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getChannel());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getFrequencyBand());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryD().get(p).getGnssObsnames().getOfficial());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getObsSats());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getObsHave());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getObsExpt());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getUserHave());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getUserExpt());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getPhaSlps());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getName());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getObsType());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getChannel());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getFrequencyBand());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCConstellationSummary().size(); k++) 
            {
                for (int p=0; p<aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().size(); p++)
                {
                    csv.append(aData.getQCReportSummary().getQCConstellationSummary().get(k).getQCObservationSummaryL().get(p).getGnssObsnames().getOfficial());
                    csv.append(" | ");
                }
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCNavigationMsg().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getIdentifier1Ch());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCNavigationMsg().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getIdentifier3Ch());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCNavigationMsg().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getName());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCNavigationMsg().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getConstellation().getOfficial());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCNavigationMsg().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getNsat());
                csv.append(" | ");
            }
            csv.append(", ");
            for (int k = 0; k < aData.getQCReportSummary().getQCNavigationMsg().size(); k++) 
            {
                csv.append(aData.getQCReportSummary().getQCNavigationMsg().get(k).getHave());
                csv.append(" | ");
            }
            csv.append("\n");
        }

        header.append(csv);

        return header.toString();
    }
    
    // *************************************************************************
    // *************************************************************************
    //
    // T4 PARSER METHODS
    //
    // *************************************************************************
    // *************************************************************************

    /*
    * HECTOR INPUT PARSER
    * This method turns the output from T4 into HECTOR input format.
    */
    public static String hectorInputParser(ArrayList<Estimated_coordinates> results)
    {
        //is 1kb chars sufficiente

        if (results==null) return "An Error Has Been Found";

        StringBuilder returnString=new StringBuilder(1024);

        for (int i = 0; i < results.size(); i++) {
            String[] date = results.get(i).getEpoch().split("-");
            Calendar cal = new GregorianCalendar();
            cal.set(Integer.parseInt(date[0]), Integer.parseInt(date[1]), Integer.parseInt(date[2]));
            returnString.append(results.get(i).getEpoch());
            returnString.append(GlassConstants.SPACE);

            returnString.append(convertToJulian(cal));
            returnString.append(GlassConstants.SPACE);
            returnString.append(results.get(i).getX());
            returnString.append(GlassConstants.SPACE);
            returnString.append(results.get(i).getY());
            returnString.append(GlassConstants.SPACE);
            returnString.append(results.get(i).getZ());
            returnString.append(GlassConstants.SPACE);
            returnString.append(results.get(i).getVar_xx());
            returnString.append(GlassConstants.SPACE);
            returnString.append(results.get(i).getVar_yy());
            returnString.append(GlassConstants.SPACE);
            returnString.append(results.get(i).getVar_zz());
            returnString.append(GlassConstants.SPACE);
            returnString.append(results.get(i).getVar_xy());
            returnString.append(GlassConstants.SPACE);
            returnString.append(results.get(i).getVar_xz());
            returnString.append(GlassConstants.SPACE);
            returnString.append(results.get(i).getVar_yz());
            returnString.append(GlassConstants.NEWLINE);
        }

        return returnString.toString();
    }


    private static double convertToJulian(Calendar gregorianCalendar)
    {
        if (gregorianCalendar == null) {
            throw new IllegalArgumentException("Gregorian date has illegal value. (NULL)");
        }
        int month = gregorianCalendar.get(Calendar.MONTH) + 1; // Java Month starts with 0!
        int year = gregorianCalendar.get(Calendar.YEAR);
        int day = gregorianCalendar.get(Calendar.DAY_OF_MONTH);
        int hour = gregorianCalendar.get(Calendar.HOUR_OF_DAY);
        int minute = gregorianCalendar.get(Calendar.MINUTE);
        int seconds = gregorianCalendar.get(Calendar.SECOND);
        // Timezone offset (including Daylight Saving Time) in hours
        int tzOffset = (gregorianCalendar.get(Calendar.DST_OFFSET) + gregorianCalendar.get(Calendar.ZONE_OFFSET))
                / (60 * 60 * 1000);
        if (month < 3) {
            year--;
            month = month + 12;
        }

        // Calculation of leap year
        double leapYear = Double.NaN;
        Calendar gregStartDate = Calendar.getInstance();
        gregStartDate.set(1583, 9, 16); // 16.10.1583 day of introduction of greg. Calendar. Before that there're no leap
        // years
        if (gregorianCalendar.after(gregStartDate)) {
            leapYear = 2 - (Math.round(year / 100)) + Math.round((Math.round(year / 100)) / 4);
        }
        Calendar gregBeginDate = Calendar.getInstance();
        gregBeginDate.set(1583, 9, 4); // 04.10.1583 last day before gregorian calendar reformation
        if ((gregorianCalendar.before(gregBeginDate))
                || (gregorianCalendar.equals(gregBeginDate))
                ) {
            leapYear = 0;
        }
        if (Double.isNaN(leapYear)) {
            throw new IllegalArgumentException(
                    "Date is not valid. Due to gregorian calendar reformation after the 04.10.1583 follows the 16.10.1583.");
        }


        //GMB APR12,2012 - fixing bug #3514617 ----------------------------------------------------------------------------
        double fracSecs = (double)(((hour - tzOffset) * 3600) + (minute * 60) + seconds) / 86400;

        long c = (long)(365.25 * (year + 4716));
        long d = (long)(30.6001 * (month + 1));

        double julianDate = day + c + d + fracSecs + leapYear - 1524.5;

        return  julianDate;
    }

    /*
     * PARSE POLYGON
     * Tis method parses a string into a coordinate polygon.
     */
    protected static ArrayList parsePolygon(String polygonString) {
        // Arraylist of polygon coordinates
        ArrayList<Coordinates> polygon = new ArrayList();
        // *****************************************************************
        // STEP 1
        // Divide the request string by the & character
        // *****************************************************************
        String[] coordinatePoints = polygonString.split("&");
        // *****************************************************************
        // STEP 2
        // Handle each coordinate point
        // *****************************************************************
        for (String coordinatePoint : coordinatePoints) {
            // *************************************************************
            // STEP 3
            // Separate coordinate values
            // *************************************************************
            String[] individualValues = coordinatePoint.split(":");
            Coordinates coord = new Coordinates();
            coord.setLat(Float.parseFloat(individualValues[0]));
            coord.setLon(Float.parseFloat(individualValues[1]));
            polygon.add(coord);
        }
        return polygon;
    }

    public JSONObject getTimeseriesJSON(ArrayList<Estimated_coordinates> data) {

        JSONArray result = new JSONArray();

        ArrayList<String> dates = new ArrayList<>();
        ArrayList<Double> x = new ArrayList<>();
        ArrayList<Double> y = new ArrayList<>();
        ArrayList<Double> z = new ArrayList<>();

        for (int i = 0; i < data.size(); i++){

            SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
            Date dt = null;
            try {
                dt = sdf.parse(data.get(i).getEpoch());
                long epoch = dt.getTime();
                dates.add(epoch/1000+"");
            } catch (ParseException e) {
                dates.add(data.get(i).getEpoch());
            }
            x.add(data.get(i).getX());
            y.add(data.get(i).getY());
            z.add(data.get(i).getZ());

            JSONObject estimated_coordinates = new JSONObject();
            estimated_coordinates.put("epoch", data.get(i).getEpoch());
            estimated_coordinates.put("x", data.get(i).getX());
            estimated_coordinates.put("y", data.get(i).getY());
            estimated_coordinates.put("z", data.get(i).getZ());
            estimated_coordinates.put("sxx", data.get(i).getVar_xx());
            estimated_coordinates.put("syy", data.get(i).getVar_yy());
            estimated_coordinates.put("szz", data.get(i).getVar_zz());
            estimated_coordinates.put("sxy", data.get(i).getVar_xy());
            estimated_coordinates.put("sxz", data.get(i).getVar_xz());
            estimated_coordinates.put("syz", data.get(i).getVar_yz());

            result.add(estimated_coordinates);
        }

        JSONObject row_data = new JSONObject();
        row_data.put("xData", dates);

        JSONObject datasetXs = new JSONObject();
        datasetXs.put("name", "X");
        datasetXs.put("data", x);
        datasetXs.put("unit", "m");
        datasetXs.put("type", "line");
        datasetXs.put("valueDecimals", 3);

        JSONObject datasetYs = new JSONObject();
        datasetYs.put("name", "Y");
        datasetYs.put("data", y);
        datasetYs.put("unit", "m");
        datasetYs.put("type", "line");
        datasetYs.put("valueDecimals", 3);

        JSONObject datasetZs = new JSONObject();
        datasetZs.put("name", "Z");
        datasetZs.put("data", z);
        datasetZs.put("unit", "m");
        datasetZs.put("type", "line");
        datasetZs.put("valueDecimals", 3);

        ArrayList<JSONObject> datasets = new ArrayList<>();
        datasets.add(datasetXs);
        datasets.add(datasetYs);
        datasets.add(datasetZs);

        row_data.put("datasets", datasets);

        return row_data;
    }

    public JSONArray getTimeSeriesByFormatJSON(ArrayList<Estimated_coordinates> data)
    {
        JSONArray result = new JSONArray();

        for (Estimated_coordinates aData : data) {

            JSONObject estimated_coordinates = new JSONObject();
            estimated_coordinates.put("epoch", aData.getEpoch());
            estimated_coordinates.put("x", aData.getX());
            estimated_coordinates.put("y", aData.getY());
            estimated_coordinates.put("z", aData.getZ());
            estimated_coordinates.put("var_xx", aData.getVar_xx());
            estimated_coordinates.put("var_yy", aData.getVar_yy());
            estimated_coordinates.put("var_zz", aData.getVar_zz());
            estimated_coordinates.put("var_xy", aData.getVar_xy());
            estimated_coordinates.put("var_xz", aData.getVar_xz());
            estimated_coordinates.put("var_yz", aData.getVar_yz());

            result.add(estimated_coordinates);
        }
        return result;
    }

    public JSONArray getTimeSeriesByFormatENUJSON(ArrayList<Estimated_coordinates> data)
    {
        JSONArray result = new JSONArray();
        for (Estimated_coordinates aData : data) {
            JSONObject estimated_coordinates = new JSONObject();
            estimated_coordinates.put("e", aData.getX());
            estimated_coordinates.put("n", aData.getY());
            estimated_coordinates.put("u", aData.getZ());
            estimated_coordinates.put("epoch", aData.getEpoch());
            result.add(estimated_coordinates);
        }
        return result;
    }

    public JSONObject getTimeSeriesByFormatCovJSON(ArrayList<Estimated_coordinates> data,
                                                      String marker,
                                                      String analysis_center,
                                                      String sampling_period,
                                                      String coordinates_system,
                                                      String format,
                                                      String reference_frame,
                                                      String otl_model,
                                                      String calibration_model,
                                                      String cut_of_angle,
                                                      String remove_outliers,
                                                      String apply_offsets,
                                                      String epoch_start,
                                                      String epoch_end,
                                                      String version,
                                                      int coordSystem,
                                                      URI requestUri) {

        // Get info about the station using the marker
        marker = marker.toUpperCase();

        DataBaseConnectionV2 dbcv2 = new DataBaseConnectionV2();
        StationQuery sq = new StationQuery();

        sq.setMarkers(new ArrayList<>(Arrays.asList(marker)));

        ArrayList<Station> res;
        res = dbcv2.getStationsShortV2(sq);
        Station this_station = res.get(0);

        JSONObject covJSON = new JSONObject();

        covJSON.put("type", "Coverage");

        JSONObject domain = new JSONObject();
        domain.put("type", "Domain");
        domain.put("domainType", "PointSeries");

        /*
         * AXES
         *
         * x = longitude
         * y = latitude
         * z = altitude
         *
         * "axes" : {
         *       "x" : {
         *         "values" : [ -8.589 ]  //longitude
         *       },
         *       "y" : {
         *         "values" : [ 41.106 ]  //latitude
         *       },
         *       "z" : {
         *         "values" : [ 287.8 ]   //optional altitude
         *       },
         *       "t" : {   // Time values of the observed timeseries, we should have N values in the array
         *         "values" : [ "2019-04-29T00:00:00.000Z", "2019-04-29T00:01:00.000Z",  "2019-04-29T00:06:00.000Z", ........, "2019-04-29T23:59:00.000Z" ]
         *       }
         *     },
         *
         */
        JSONObject axes = new JSONObject();

        JSONObject x = new JSONObject();
        JSONArray x_values = new JSONArray();
        x_values.add(this_station.getLocation().getCoordinates().getLon());
        x.put("values", x_values);
        axes.put("x", x);

        JSONObject y = new JSONObject();
        JSONArray y_values = new JSONArray();
        y_values.add(this_station.getLocation().getCoordinates().getLat());
        y.put("values", y_values);
        axes.put("y", y);

        JSONObject z = new JSONObject();
        JSONArray z_values = new JSONArray();
        z_values.add(this_station.getLocation().getCoordinates().getAltitude());
        z.put("values", z_values);
        axes.put("z", z);

        JSONObject t = new JSONObject();
        JSONArray t_values = new JSONArray();
        for (Estimated_coordinates datum : data) {
            String epoch = datum.getEpoch();
            // epoch format is '2004-12-22 00:00:00'
            // wanted format is '2004-12-22T00:00:00.000Z'
            String wanted_epoch = epoch.split(" ")[0] + "T" + epoch.split(" ")[1] + ".000Z";
            t_values.add(wanted_epoch);
        }
        t.put("values", t_values);
        axes.put("t", t);
        domain.put("axes", axes);

        JSONArray referencing = new JSONArray();

        JSONObject referencing1 = new JSONObject();
        JSONArray coordinates1 = new JSONArray();
        coordinates1.add("x");
        coordinates1.add("y");
        coordinates1.add("z");
        referencing1.put("coordinates", coordinates1);
        JSONObject system1 = new JSONObject();
        system1.put("id", "http://www.opengis.net/def/crs/OGC/1.3/CRS84");
        system1.put("type", "GeographicCRS");
        referencing1.put("system", system1);

        JSONObject referencing2 = new JSONObject();
        JSONArray coordinates2 = new JSONArray();
        coordinates2.add("t");
        referencing2.put("coordinates", coordinates2);
        JSONObject system2 = new JSONObject();
        system2.put("calendar", "Gregorian");
        system2.put("type", "TemporalRS");
        referencing2.put("system", system2);

        referencing.add(referencing1);
        referencing.add(referencing2);
        domain.put("referencing", referencing);
        covJSON.put("domain", domain);

        JSONObject parameters = new JSONObject();

        //East or X coordinate
        {
            JSONObject e_param = new JSONObject();
            e_param.put("type", "Parameter");
            JSONObject e_description = new JSONObject();
            if (coordSystem == GlassConstants._XYZ)
                e_description.put("en", "Geographic X component");
            if (coordSystem == GlassConstants._ENU)
                e_description.put("en", "Geographic east component (positive east)");
            e_param.put("description", e_description);
            JSONObject e_unit = new JSONObject();
            JSONObject e_label = new JSONObject();
            if (coordSystem == GlassConstants._ENU) {
                e_label.put("en", "East");
            }
            if (coordSystem == GlassConstants._XYZ) {
                e_label.put("en", "X");
            }
            //e_label.put("en", "X");
            e_unit.put("label", e_label);
            JSONObject e_symbol = new JSONObject();
            e_symbol.put("type", "https://gnssproducts.epos.ubi.pt/faq");
            if (coordSystem == GlassConstants._ENU) {
                e_symbol.put("value", "mm");
            }
            if (coordSystem == GlassConstants._XYZ) {
                e_symbol.put("value", "m");
            }
            //e_symbol.put("value", "m");
            e_unit.put("symbol", e_symbol);
            e_param.put("unit", e_unit);
            JSONObject e_observedproperty = new JSONObject();
            e_observedproperty.put("id", "https://gnssproducts.epos.ubi.pt/faq");
            JSONObject e_obsp_label = new JSONObject();
            if (coordSystem == GlassConstants._XYZ) {
                e_obsp_label.put("en", "X component");
                parameters.put("X", e_param);
            }
            if (coordSystem == GlassConstants._ENU) {
                e_obsp_label.put("en", "East component");
                parameters.put("E", e_param);
            }
            e_observedproperty.put("label", e_obsp_label);
            e_param.put("observedProperty", e_observedproperty);
        }

        //North or Y coordinate
        {
            JSONObject n_param = new JSONObject();
            n_param.put("type", "Parameter");
            JSONObject n_description = new JSONObject();
            if (coordSystem == GlassConstants._ENU)
                n_description.put("en", "Geographic Y component");
            if (coordSystem == GlassConstants._XYZ)
                n_description.put("en", "Geographic north component (positive north)");
            n_param.put("description", n_description);
            JSONObject n_unit = new JSONObject();
            JSONObject n_label = new JSONObject();
            if (coordSystem == GlassConstants._ENU) {
                n_label.put("en", "North");
            }
            if (coordSystem == GlassConstants._XYZ) {
                n_label.put("en", "Y");
            }
            //n_label.put("en", "Y");
            n_unit.put("label", n_label);
            JSONObject n_symbol = new JSONObject();
            n_symbol.put("type", "https://gnssproducts.epos.ubi.pt/faq");
            if (coordSystem == GlassConstants._ENU) {
                n_symbol.put("value", "mm");
            }
            if (coordSystem == GlassConstants._XYZ) {
                n_symbol.put("value", "m");
            }
            //n_symbol.put("value", "m");
            n_unit.put("symbol", n_symbol);
            n_param.put("unit", n_unit);
            JSONObject n_observedproperty = new JSONObject();
            n_observedproperty.put("id", "https://gnssproducts.epos.ubi.pt/faq");
            JSONObject n_obsp_label = new JSONObject();
            if (coordSystem == GlassConstants._ENU) {
                n_obsp_label.put("en", "North component");
                parameters.put("N", n_param);
            }
            if (coordSystem == GlassConstants._XYZ) {
                n_obsp_label.put("en", "Y component");
                parameters.put("Y", n_param);
            }
            n_observedproperty.put("label", n_obsp_label);
            n_param.put("observedProperty", n_observedproperty);
        }

        //Up or z coordinate
        {
            JSONObject u_param = new JSONObject();
            u_param.put("type", "Parameter");
            JSONObject u_description = new JSONObject();
            if (coordSystem == GlassConstants._XYZ)
                u_description.put("en", "Geographic Z component");
            if (coordSystem == GlassConstants._ENU)
                u_description.put("en", "Geographic UP component (positive UP)");
            u_param.put("description", u_description);
            JSONObject u_unit = new JSONObject();
            JSONObject u_label = new JSONObject();
            if (coordSystem == GlassConstants._ENU) {
                u_label.put("en", "Up");
            }
            if (coordSystem == GlassConstants._XYZ) {
                u_label.put("en", "Z");
            }
            u_unit.put("label", u_label);
            JSONObject u_symbol = new JSONObject();
            u_symbol.put("type", "https://gnssproducts.epos.ubi.pt/faq");
            if (coordSystem == GlassConstants._ENU) {
                u_symbol.put("value", "mm");
            }
            if (coordSystem == GlassConstants._XYZ) {
                u_symbol.put("value", "m");
            }
            u_unit.put("symbol", u_symbol);
            u_param.put("unit", u_unit);
            JSONObject u_observedproperty = new JSONObject();
            u_observedproperty.put("id", "https://gnssproducts.epos.ubi.pt/faq");
            JSONObject u_obsp_label = new JSONObject();
            u_param.put("observedProperty", u_observedproperty);
            if (coordSystem == GlassConstants._ENU) {
                u_obsp_label.put("en", "Up component");
                parameters.put("Up", u_param);
            }
            if (coordSystem == GlassConstants._XYZ) {
                u_obsp_label.put("en", "Z component");
                parameters.put("Z", u_param);
            }
            u_observedproperty.put("label", u_obsp_label);
            covJSON.put("parameters", parameters);
        }

        //Ranges
        JSONObject ranges = new JSONObject();

        //East or X  range
        {
            JSONObject e_range = new JSONObject();
            e_range.put("type", "NdArray");
            e_range.put("dataType", "float");
            JSONArray e_axisNames = new JSONArray();
            e_axisNames.add("t");
            e_range.put("axisNames", e_axisNames);
            JSONArray e_shape = new JSONArray();
            e_shape.add(data.size());
            e_range.put("shape", e_shape);
            JSONArray e_values = new JSONArray();
            for (Estimated_coordinates datum : data) {
                // East is X
                e_values.add(datum.getX());
            }
            e_range.put("values", e_values);

            if (coordSystem == GlassConstants._ENU) ranges.put("E", e_range);
            if (coordSystem == GlassConstants._XYZ) ranges.put("X", e_range);
        }
        //North or Y  range
        {
            JSONObject n_range = new JSONObject();
            n_range.put("type", "NdArray");
            n_range.put("dataType", "float");
            JSONArray n_axisNames = new JSONArray();
            n_axisNames.add("t");
            n_range.put("axisNames", n_axisNames);
            JSONArray n_shape = new JSONArray();
            n_shape.add(data.size());
            n_range.put("shape", n_shape);
            JSONArray n_values = new JSONArray();
            for (Estimated_coordinates datum : data) {
                // North is Y
                n_values.add(datum.getY());
            }
            n_range.put("values", n_values);
            if (coordSystem == GlassConstants._ENU) ranges.put("N", n_range);
            if (coordSystem == GlassConstants._XYZ) ranges.put("Y", n_range);
        }
        //Up or Z  range
        {
            JSONObject u_range = new JSONObject();
            u_range.put("type", "NdArray");
            u_range.put("dataType", "float");
            JSONArray u_axisNames = new JSONArray();
            u_axisNames.add("t");
            u_range.put("axisNames", u_axisNames);
            JSONArray u_shape = new JSONArray();
            u_shape.add(data.size());
            u_range.put("shape", u_shape);
            JSONArray u_values = new JSONArray();
            for (Estimated_coordinates datum : data) {
                // Up is Z
                u_values.add(datum.getZ());
            }
            u_range.put("values", u_values);
            if (coordSystem == GlassConstants._ENU) ranges.put("U", u_range);
            if (coordSystem == GlassConstants._XYZ) ranges.put("Z", u_range);
        }
        covJSON.put("ranges", ranges);
        //End Ranges

        // Additional EPOS info
        JSONObject epos_info = new JSONObject();
        epos_info.put("type", "CovJSON Timeseries Values");
        epos_info.put("tcs", "wp10");
        // TODO: improve the URL, find a way to get it directly from source
        //e.g. http://localhost:8080/GlassFramework/webresources/products/timeseries/CASC/BFKH/weekly/enu/covjson?epoch_end=2016-12-31&epoch_start=2016-01-01
        //Version v = new Version();
        //String web_service_url = "https://" + v.getUrl() + ":" + v.getPort()
//        StringBuilder web_service_url = new StringBuilder().append("https://glass.epos.ubi.pt:8080/GlassFramework/webresources/products/timeseries/").append(marker).append("/").append(analysis_center).append("/").append(sampling_period).append("/").append(coordinates_system).append("/").append(format);
//        ArrayList<String> optional_url = new ArrayList<>();
//        if(!"".equals(reference_frame)) optional_url.add("reference_frame=" + reference_frame);
//        if(!"".equals(otl_model)) optional_url.add("otl_model=" + otl_model);
//        if(!"".equals(calibration_model)) optional_url.add("calibration_model=" + calibration_model);
//        if(!"".equals(cut_of_angle)) optional_url.add("cut_of_angle=" + cut_of_angle);
//        if(!"".equals(remove_outliers)) optional_url.add("remove_outliers=" + remove_outliers);
//        if(!"".equals(apply_offsets)) optional_url.add("apply_offsets=" + apply_offsets);
//        if(!"".equals(epoch_start)) optional_url.add("epoch_start=" + epoch_start);
//        if(!"".equals(epoch_end)) optional_url.add("epoch_end=" + epoch_end);
//
//        for(int i = 0; i < optional_url.size(); i++){
//            if(i == 0){
//                web_service_url.append("?");
//            }
//            else{
//                web_service_url.append("&");
//            }
//            web_service_url.append(optional_url.get(i));
//        }
//
//        epos_info.put("web_service_url", web_service_url.toString());
        epos_info.put("web_service_uri", requestUri.toString());    

        epos_info.put("format", "covjson");
        // 2004-12-22T00:00:00.000Z
        String date = new SimpleDateFormat("yyyy-MM-dd").format(Calendar.getInstance().getTime());
        String time = new SimpleDateFormat("HH:mm:ss").format(Calendar.getInstance().getTime());
        epos_info.put("date_created", date+"T"+time+".000Z");
        epos_info.put("usage", "timeseries");
        epos_info.put("resampling_method", "null");
        covJSON.put("epos:info", epos_info);


        //--- Call DatabaseT4Helper to get information from the DB instead of the arguments used
        //    to call this function.
        //metadata = new DatabaseT4Helper(data, version,"timeseries");

        Method_identification method_identification_obj = methodIdentificationMetadata(data);

        // Additional gnss-europe info
        JSONObject gnss_europe_info = new JSONObject();
        gnss_europe_info.put("marker", marker);
        gnss_europe_info.put("analysis_center", method_identification_obj.getAnalysis_center().getName());
        gnss_europe_info.put("sampling_period", sampling_period);
        gnss_europe_info.put("coordinate_type", coordinates_system);
        gnss_europe_info.put("format", format);
        gnss_europe_info.put("reference_frame", method_identification_obj.getReference_frame().getName());
        gnss_europe_info.put("DOI", method_identification_obj.getDoi());
        gnss_europe_info.put("version", String.format("%3.1f", method_identification_obj.getVersion()));
        gnss_europe_info.put("otl_model", otl_model);
        gnss_europe_info.put("calibration_model", calibration_model);
        gnss_europe_info.put("cut_of_angle", cut_of_angle);
        gnss_europe_info.put("remove_outliers", remove_outliers);
        gnss_europe_info.put("apply_offsets", apply_offsets);
        gnss_europe_info.put("epoch_start", epoch_start);
        gnss_europe_info.put("epoch_end", epoch_end);
        covJSON.put("gnss_europe:info", gnss_europe_info);


        return covJSON;
    }

    private Method_identification methodIdentificationMetadata(ArrayList<Estimated_coordinates> data){
        Method_identification method_identification = new Method_identification();
        Boolean method_identification_retrieved = false;

        for(Estimated_coordinates ec : data){
            if(ec.getMethod_identification() != null){
                method_identification = ec.getMethod_identification();
                method_identification_retrieved = true;
                break;
            }
        }

        if(!method_identification_retrieved){
            method_identification.setId(0);
            method_identification.setSoftware("--");
            method_identification.setVersion(0.0);
            method_identification.setCreation_date("--");
            method_identification.setDoi("--");
            method_identification.setAnalysis_center(new Analysis_Centers());
            method_identification.setReference_frame(new Reference_frame());
        }

        return method_identification;
    }

    public String getFullXMLTimeSeriesENU(ArrayList<Estimated_coordinates> data){


        StringBuilder xml = new StringBuilder();

        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        xml.append("<TimeSeries>");

        for (Estimated_coordinates aData : data) {
            xml.append("<coordinates>");
            xml.append("<epoch>" + aData.getEpoch() + "</epoch>");
            xml.append("<e>" + aData.getX() + "</e>");
            xml.append("<n>" + aData.getY() + "</n>");
            xml.append("<u>" + aData.getZ() + "</u>");
            xml.append("</coordinates>");
        }

        xml.append("</TimeSeries>");

        return xml.toString();
    }


    public String getFullXMLCoordinatesENU(ArrayList<Estimated_coordinates> data){

        StringBuilder xml = new StringBuilder();

        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        xml.append("<Coordinates>");

        for (Estimated_coordinates aData : data) {
            xml.append("<coordinates>");
            xml.append("<epoch>" + aData.getEpoch() + "</epoch>");
            xml.append("<e>" + aData.getX() + "</e>");
            xml.append("<n>" + aData.getY() + "</n>");
            xml.append("<u>" + aData.getZ() + "</u>");
            xml.append("</coordinates>");
        }

        xml.append("</Coordinates>");

        return xml.toString();
    }


    public JSONArray getVelocitiesFieldJSON(ArrayList<ArrayList<VelocityByPlateAndAc>> data)
    {
        JSONArray result = new JSONArray();

        if(data.isEmpty()){
            JSONArray aux = new JSONArray();
            result.addAll(aux); 
        } else{
            for (ArrayList<VelocityByPlateAndAc> list : data) {
                JSONArray aux = new JSONArray();
                for (VelocityByPlateAndAc aData : list) {
                    JSONObject js = new JSONObject();
                    js.put("Station", aData.getMarker());
                    js.put("Plate", aData.getPlate());
                    js.put("AnalysisCenter", aData.getAnalysis_center());
                    js.put("LengthVector_H", aData.getVector_lenght_H());
                    js.put("Angle_H", aData.getAngle_H());
                    js.put("LengthVector_V", aData.getVector_lenght_V());
                    js.put("Angle_V", aData.getAngle_V());
                    aux.add(js);
                }
    
                result.addAll(aux);
            }
        }
        return result;
    }

//    public static JSONArray getTimeSeriesJSONMIDAS(ArrayList<Estimated_coordinates> data)
//    {
//        JSONArray result = new JSONArray();
//
//        for (int i = 0; i < data.size(); i++){
//
//            JSONObject estimated_coordinates = new JSONObject();
//            estimated_coordinates.put("n", data.get(i).getX());
//            estimated_coordinates.put("e", data.get(i).getY());
//            estimated_coordinates.put("u", data.get(i).getZ());
//            estimated_coordinates.put("mjd", data.get(i).getMJD());
//
//            result.add(estimated_coordinates);
//        }
//        return result;
//    }


    public JSONArray getVelocitiesXYZJSON(ArrayList<Reference_Position_Velocities> data){

        JSONArray result = new JSONArray();

        for (Reference_Position_Velocities aData : data) {

            JSONObject mean_position_velocities = new JSONObject();
            mean_position_velocities.put("velx", aData.getVelx());
            mean_position_velocities.put("vely", aData.getVely());
            mean_position_velocities.put("velz", aData.getVelz());
            mean_position_velocities.put("velx_sigma", aData.getVelx_sigma());
            mean_position_velocities.put("vely_sigma", aData.getVely_sigma());
            mean_position_velocities.put("velz_sigma", aData.getVelz_sigma());
            mean_position_velocities.put("vel_rho_xy", aData.getVel_rho_xy());
            mean_position_velocities.put("vel_rho_xz", aData.getVel_rho_xz());
            mean_position_velocities.put("vel_rho_yz", aData.getVel_rho_yz());
//            mean_position_velocities.put("reference_position_x", data.get(i).getReference_position_x());
//            mean_position_velocities.put("reference_position_y", data.get(i).getReference_position_y());
//            mean_position_velocities.put("reference_position_z", data.get(i).getReference_position_z());
//            mean_position_velocities.put("reference_position_x_sigma", data.get(i).getReference_position_x_sigma());
//            mean_position_velocities.put("reference_position_y_sigma", data.get(i).getReference_position_y_sigma());
//            mean_position_velocities.put("reference_position_z_sigma", data.get(i).getReference_position_z_sigma());

            /*Solves Issue https://gitlab.com/gpseurope/EPOS_GLASS_Framework/-/issues/30 */
            /* still requires testing */
            mean_position_velocities.put("rf_frame", aData.getReference_frame().getName());
            mean_position_velocities.put("version", String.format("%.1f", aData.getMethod_identification().getVersion()));
            mean_position_velocities.put("DOI", String.format("%s", aData.getMethod_identification().getDoi()));

            result.add(mean_position_velocities);
        }
        return result;
    }

    public JSONArray getVelocitiesENUJSON(ArrayList<Reference_Position_Velocities> data){

        JSONArray result = new JSONArray();

        for (Reference_Position_Velocities aData : data) {

            JSONObject mean_position_velocities = new JSONObject();
            mean_position_velocities.put("n", aData.getVelx());
            mean_position_velocities.put("e", aData.getVely());
            mean_position_velocities.put("u", aData.getVelz());
            mean_position_velocities.put("SNd", aData.getVelx_sigma());
            mean_position_velocities.put("SEd", aData.getVely_sigma());
            mean_position_velocities.put("SUd", aData.getVelz_sigma());
            mean_position_velocities.put("Rne", aData.getVel_rho_xy());
            mean_position_velocities.put("Rnu", aData.getVel_rho_xz());
            mean_position_velocities.put("Reu", aData.getVel_rho_yz());
            mean_position_velocities.put("rf_frame", aData.getReference_frame().getName());
            mean_position_velocities.put("version", String.format("%.1f", aData.getMethod_identification().getVersion()));
            mean_position_velocities.put("DOI", String.format("%s", aData.getMethod_identification().getDoi()));
            //mean_position_velocities.put("version", aData.getVersion());

            result.add(mean_position_velocities);
        }
        return result;
    }

    public String getVelocitiesENUXML(ArrayList<Reference_Position_Velocities> data){

//        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
//        //"<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
//
//        xml = xml + "<VelocitiesNEU>";
//
//        for (int i=0; i<data.size();i++){
//
//            xml = xml + "<velocities>";
//            xml = xml + "<n>" + data.get(i).getVelx() + "</n>";
//            xml = xml + "<e>" + data.get(i).getVely() + "</e>";
//            xml = xml + "<u>" + data.get(i).getVelz() + "</u>";
//            xml = xml + "<SNd>" + data.get(i).getVelx_sigma() + "</SNd>";
//            xml = xml + "<SEd>" + data.get(i).getVely_sigma() + "</SEd>";
//            xml = xml + "<SUd>" + data.get(i).getVelz_sigma() + "</SUd>";
//            xml = xml + "<Rne>" + data.get(i).getVel_rho_xy() + "</Rne>";
//            xml = xml + "<Rnu>" + data.get(i).getVel_rho_xz() + "</Rnu>";
//            xml = xml + "<Reu>" + data.get(i).getVel_rho_yz() + "</Reu>";
//            xml = xml + "</velocities>";
//        }
//        xml = xml + "</VelocitiesNEU>";
//
//        return xml;

        StringBuilder xml = new StringBuilder();

        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        xml.append("<VelocitiesNEU>");

        for (Reference_Position_Velocities aData : data) {

            xml.append("<velocities>");
            xml.append("<n>").append(aData.getVelx()).append("</n>");
            xml.append("<e>").append(aData.getVely()).append("</e>");
            xml.append("<u>").append(aData.getVelz()).append("</u>");
            xml.append("<SNd>").append(aData.getVelx_sigma()).append("</SNd>");
            xml.append("<SEd>").append(aData.getVely_sigma()).append("</SEd>");
            xml.append("<SUd>").append(aData.getVelz_sigma()).append("</SUd>");
            xml.append("<Rne>").append(aData.getVel_rho_xy()).append("</Rne>");
            xml.append("<Rnu>").append(aData.getVel_rho_xz()).append("</Rnu>");
            xml.append("<Reu>").append(aData.getVel_rho_yz()).append("</Reu>");
            xml.append("</velocities>");
        }
        xml.append("</VelocitiesNEU>");

        return xml.toString();

    }

    public String getVelocitiesXYZXML(ArrayList<Reference_Position_Velocities> data){

//        String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
//        //"<?xml version=\"1.0\" encoding=\"UTF-8\"?>";
//
//        xml = xml + "<VelocitiesXYZ>";
//
//        for (int i=0; i<data.size();i++){
//
//            xml = xml + "<velocities>";
//            xml = xml + "<velx>" + data.get(i).getVelx() + "</velx>";
//            xml = xml + "<vely>" + data.get(i).getVely() + "</vely>";
//            xml = xml + "<velz>" + data.get(i).getVelz() + "</velz>";
//            xml = xml + "<velx_sigma>" + data.get(i).getVelx_sigma() + "</velx_sigma>";
//            xml = xml + "<vely_sigma>" + data.get(i).getVely_sigma() + "</vely_sigma>";
//            xml = xml + "<velz_sigma>" + data.get(i).getVelz_sigma() + "</velz_sigma>";
//            xml = xml + "<vel_rho_xy>" + data.get(i).getVel_rho_xy() + "</vel_rho_xy>";
//            xml = xml + "<vel_rho_xz>" + data.get(i).getVel_rho_xz() + "</vel_rho_xz>";
//            xml = xml + "<vel_rho_yz>" + data.get(i).getVel_rho_yz() + "</vel_rho_yz>";
//            xml = xml + "</velocities>";
//        }
//        xml = xml + "</VelocitiesXYZ>";
//
//        return xml;

        StringBuilder xml = new StringBuilder();

        xml.append("<?xml version=\"1.0\" encoding=\"UTF-8\"?>");
        xml.append("<VelocitiesXYZ>");

        for (Reference_Position_Velocities aData : data) {

            xml.append("<velocities>");
            xml.append("<velx>").append(aData.getVelx()).append("</velx>");
            xml.append("<vely>").append(aData.getVely()).append("</vely>");
            xml.append("<velz>").append(aData.getVelz()).append("</velz>");
            xml.append("<velx_sigma>").append(aData.getVelx_sigma()).append("</velx_sigma>");
            xml.append("<vely_sigma>").append(aData.getVely_sigma()).append("</vely_sigma>");
            xml.append("<velz_sigma>").append(aData.getVelz_sigma()).append("</velz_sigma>");
            xml.append("<vel_rho_xy>").append(aData.getVel_rho_xy()).append("</vel_rho_xy>");
            xml.append("<vel_rho_xz>").append(aData.getVel_rho_xz()).append("</vel_rho_xz>");
            xml.append("<vel_rho_yz>").append(aData.getVel_rho_yz()).append("</vel_rho_yz>");
            xml.append("</velocities>");
        }
        xml.append("</VelocitiesXYZ>");

        return xml.toString();
    }


    @SuppressWarnings("unchecked")
    JSONObject getDataAvailabilityJSON(HashMap<String, ArrayList<DataBaseT4Connection.Tuple>> result) {

        JSONObject stationData = new JSONObject();

        for(Map.Entry<String, ArrayList<DataBaseT4Connection.Tuple>> entry : result.entrySet()) {
            String station = entry.getKey();
            ArrayList<DataBaseT4Connection.Tuple> startAndEndDates = entry.getValue();

            JSONArray iterationStartAndEndDatesArray = new JSONArray();
            for (DataBaseT4Connection.Tuple tuple : startAndEndDates) {
                JSONObject iterationStartAndEndDates = new JSONObject();
                iterationStartAndEndDates.put("start", tuple.x);
                iterationStartAndEndDates.put("end", tuple.y);
                iterationStartAndEndDatesArray.add(iterationStartAndEndDates);
            }
            stationData.put(station, iterationStartAndEndDatesArray);

        }
        return stationData;
    }

    JSONObject getProductsAttributesJSON(HashMap<String, String> result) {

        JSONObject productsAttributes = new JSONObject();

        Set<String> keys = result.keySet();

        for (String key : keys)
        {
            if(key != null) {
                productsAttributes.put(key, result.get(key));

            }

        }
        return productsAttributes;
    }



    /*
     * GET FILE URL JSON
     * This method returns the the url to the file in JSON.
     */
    public String getFileURL(ArrayList<Rinex_file> data)
    {
        StringBuilder file_list = new StringBuilder();

        for (Rinex_file aData : data) {

            // Bad status files are excluded
          //  if(aData.getStatus() <=0)
          //      continue;

            //file_list.append("wget ");
            if(aData.getData_center_structure().getDataCenter().getProtocol().equals("ftp")){
                file_list.append("ftp://")
                    .append(aData.getData_center_structure().getDataCenter().getHostname())
                    .append("/")
                    .append(aData.getData_center_structure().getDirectory_naming())
                    .append("/")
                    .append(aData.getRelative_path())
                    .append("/")
                    .append(aData.getName())
                    .append(" --no-passive-ftp");
            }else{
                file_list.append(aData.getData_center_structure().getDataCenter().getProtocol())
                    .append("://")
                    .append(aData.getData_center_structure().getDataCenter().getHostname())
                    .append("/")
                    .append(aData.getData_center_structure().getDirectory_naming())
                    .append("/")
                    .append(aData.getRelative_path())
                    .append("/")
                    .append(aData.getName());
            }

            // Add file to the list
            file_list.append("\n");
        }

        return file_list.toString();
    }

}
