package Glass;

import Configuration.DBConsts;
import CustomClasses.ProductDataAvailability;
import CustomClasses.VelocityByPlateAndAc;
import EposTables.Estimated_coordinates;
import EposTables.Reference_Position_Velocities;
import EposTables.Station;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import javax.imageio.ImageIO;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Properties;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
* PRODUCTS
* This class is responsible for defining the URIs to query the Product EPOS database and
* returns the queries results.
*/
@Path("products")
public class Products {


    @Context
    private UriInfo context;


    /**
    * PRODUCTS
    * This is the default constructor for the Products class.
    */
    public Products(){
        
    }


    /**
     * <p><b>GET TIMESERIES FILE</b></p>
     *
     * Get <i>timeseries</i> files (which is a time series) for a given <b>station</b>, <b>analysis center</b>, <b>sampling period</b> and <b>format</b>.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>marker</b> - four letter station identification</li>
     *     <li><b>analysis_center</b> - name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>sampling_period</b> - name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)</li>
     *     <li><b>format</b> - name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)</li>
     * </ul>
     *
     *
     * <p>Optional parameters are:</p>
     * <ul>
     *     <li><b>reference_frame</b> - name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)</li>
     *     <li><b>otl_model</b> - name of the desired OTL model used in GNSS analysis(please refer to <i>products/attributes/otl_model</i> request for possible OTL models)</li>
     *     <li><b>calibration_model</b> - name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)</li>
     *     <li><b>cut_of_angle</b> - value of the desired cut of angle</li>
     *     <li><b>remove_outliers</b> - remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)</li>
     *     <li><b>apply_offsets</b> - apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)</li>
     *     <li><b>epoch_start</b> - beginning of epoch (date format YYYY-MM-DD)</li>
     *     <li><b>epoch_end</b> -  end of epoch (date format YYYY-MM-DD)</li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     *  <li>The reference frame will be  <i>IGS08</i>, <i>IGS14</i>, <i>free-network</i>, <i>IGb08</i> or <i>INGV_EU</i>.</li>
     *  <li>The OTL model used in GNSS analysis will be, for example,  <i>FES2004</i>, <i>FES2014b</i> or <i>GOT4.10c</i></li>
     *  <li>The antenna calibration model used in GNSS analysis will be, for example, <i>epn_14_1958.atx</i> or <i>Gigs08_wwww.atx</i></li>
     *  <li>If no start and/or end epoch date are given, the whole time series will be returned.</li>
     *  <li>OTL model and calibration model are two parameters which refine the search.</li>
     *  <li>Sampling period tells us if daily or weekly solutions should be obtained.</li>
     *  <li>Cut of angle also restricts the search to solutions with cut-off angle larger than the one given.</li>
     *  <li>Remove outliers is a boolean parameter which indicates if coordinates that are considered outliers should be included or discarded.</li>
     *  <li>if apply offsets is true, then apply the estimated offsets value.</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/timeseries/file/BLIX/UGA/daily/json?epoch_start=2015-02-23&remove_outliers=false
     *
     *
     * @param marker four letter station identification
     * @param analysis_center name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)
     * @param sampling_period name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)
     * @param format name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)
     * @param reference_frame name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)
     * @param otl_model name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)
     * @param calibration_model name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)
     * @param cut_of_angle value of the desired cut of angle
     * @param remove_outliers remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)
     * @param apply_offsets apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)
     * @param epoch_start beginning of epoch (date format YYYY-MM-DD)
     * @param epoch_end end of epoch (date format YYYY-MM-DD)
     * @return File with the time series objects in format specified in format field.
     * @author Andre Rodrigues, Machiel's documentation
     * @version 1.0
     * @since Unknown
     */
    @Path("timeseries/file/{marker}/{analysis_center}/{sampling_period}/{format}")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getTimeseriesFileByFormat(@PathParam("marker") String marker,
                                              @PathParam("analysis_center") String analysis_center,
                                              @PathParam("sampling_period") String sampling_period,
                                              @PathParam("format") String format,
                                              @DefaultValue("") @QueryParam("reference_frame") String reference_frame,
                                              @DefaultValue("") @QueryParam("otl_model") String otl_model,
                                              @DefaultValue("") @QueryParam("calibration_model") String calibration_model, //In the code this is known as antenna_model
                                              @DefaultValue("") @QueryParam("cut_of_angle") String cut_of_angle,
                                              @DefaultValue("") @QueryParam("epoch_start") String epoch_start,
                                              @DefaultValue("") @QueryParam("epoch_end") String epoch_end,
                                              @DefaultValue("") @QueryParam("remove_outliers") String remove_outliers,
                                              @DefaultValue("") @QueryParam("apply_offsets") String apply_offsets,
                                              @DefaultValue("") @QueryParam("version") String version) throws IOException, ParseException, java.text.ParseException {

        format = format.toLowerCase();
        if (!format.equals("xml") && !format.equals("json") && !format.equals("pbo") && !format.equals("midas") && !format.equals("hector")) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        marker=marker.toUpperCase();

        File f = new File("/opt/EPOS/timeseries/" +
                marker +"/"+ analysis_center +"/"+ sampling_period +"/"+ "xyz" +"/"+ format +"-"+ reference_frame +"-"+ otl_model +"-"+
                calibration_model +"-"+ cut_of_angle +"-"+ null +"-"+ null +"-"+ remove_outliers +"-"+
                apply_offsets +"."+format);

        if (f.exists() && !f.isDirectory()) {

            if(epoch_start!=null || epoch_end!=null){
                return Response.ok(String.valueOf(Tools.filterDate(f, epoch_start, epoch_end))).header("Content-Disposition", "attachment; filename=" + marker+"."+format).build();
            }
            else {
                return Response.ok(f).header("Content-Disposition", "attachment; filename=" + marker+"."+format).build();
            }
        }


        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList<Estimated_coordinates> res;

        try {
            res = dbc.getTimeseriesXYZBasedOnParameters(marker, analysis_center, sampling_period, format, reference_frame, otl_model, calibration_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries", version);
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        if (res == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        if (res.isEmpty()) {
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        Parsers parser = new Parsers();


        switch (format) {

            case "xml":
                String xml = parser.getFullXMLTimeSeries(res);
                return Response.ok(xml).header("Content-Disposition", "attachment; filename=" + marker+".xml").build();

            case "json":
                JSONArray json = parser.getTimeSeriesByFormatJSON(res);
                return Response.ok(String.valueOf(json)).header("Content-Disposition", "attachment; filename=" + marker+".json").build();

            case "pbo":
                String pbo = PBOHandler.PBOEstimatedCoordinates(res, marker, sampling_period);
                return Response.ok(pbo).header("Content-Disposition", "attachment; filename=" + marker+".pos").build();

            case "midas":
                String midas = PBOHandler.CreateMidasFile(marker,PBOHandler.PBOEstimatedCoordinates2MIDAS(res, marker, sampling_period));
                return Response.ok(midas).header("Content-Disposition", "attachment; filename=" + marker+".renu").build();

            case "hector":
                String hector = PBOHandler.CreateHectorFile(sampling_period,PBOHandler.PBOEstimatedCoordinates2MIDAS(res,marker, sampling_period));
                return Response.ok(hector).header("Content-Disposition", "attachment; filename=" + marker+".enu").build();

            default:
                return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     * <p><b>GET TIMESERIES</b></p>
     *
     * Get <i>timeseries</i> objects (which is a time series) for a given <b>station</b>, <b>analysis center</b>, <b>sampling period</b>, <b>coordinates system</b> and <b>format</b>.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>marker</b> - four letter station identification</li>
     *     <li><b>analysis_center</b> - name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>sampling_period</b> - name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)</li>
     *     <li><b>coordinates_system</b> - name of the desired coordinates system (either xyz or enu)</li>
     *     <li><b>format</b> - name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)</li>
     * </ul>
     *
     *
     * <p>Optional parameters are:</p>
     * <ul>
     *     <li><b>reference_frame</b> - name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)</li>
     *     <li><b>otl_model</b> - name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)</li>
     *     <li><b>calibration_model</b> - name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)</li>
     *     <li><b>cut_of_angle</b> - value of the desired cut of angle</li>
     *     <li><b>remove_outliers</b> - remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)</li>
     *     <li><b>apply_offsets</b> - apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)</li>
     *     <li><b>epoch_start</b> - beginning of epoch (date format YYYY-MM-DD)</li>
     *     <li><b>epoch_end</b> -  end of epoch (date format YYYY-MM-DD)</li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     *  <li>The reference frame will be  <i>IGS08</i>, <i>IGS14</i>, <i>free-network</i>, <i>IGb08</i> or <i>INGV_EU</i>.</li>
     *  <li>The OTL model used in GNSS analysis will be, for example,  <i>FES2004</i>, <i>FES2014b</i> or <i>GOT4.10c</i></li>
     *  <li>The antenna calibration model used in GNSS analysis will be, for example, <i>epn_14_1958.atx</i> or <i>Gigs08_wwww.atx</i></li>
     *  <li>If no start and/or end epoch date are given, the whole time series will be returned.</li>
     *  <li>OTL model and calibration model are two parameters which refine the search.</li>
     *  <li>Sampling period tells us if daily or weekly solutions should be obtained.</li>
     *  <li>Cut of angle also restricts the search to solutions with cut-off angle larger than the one given.</li>
     *  <li>Remove outliers is a boolean parameter which indicates if coordinates that are considered outliers should be included or discarded.</li>
     *  <li>if apply offsets is true, then apply the estimated offsets value.</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/timeseries/BLIX/UGA/daily/json?epoch_start=2015-02-23&remove_outliers=false
     *
     *
     * @param marker four letter station identification
     * @param analysis_center name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)
     * @param sampling_period name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)
     * @param coordinates_system name of the desired coordinates system (either xyz or enu)
     * @param format name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)
     * @param reference_frame name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)
     * @param otl_model name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)
     * @param calibration_model name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)
     * @param cut_of_angle value of the desired cut of angle
     * @param remove_outliers remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)
     * @param apply_offsets apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)
     * @param epoch_start beginning of epoch (date format YYYY-MM-DD)
     * @param epoch_end end of epoch (date format YYYY-MM-DD)
     * @return String with the time series objects in format specified in format field.
     * @author Andre Rodrigues, Machiel's documentation
     * @version 1.0
     * @since Unknown
     */
    @Path("timeseries/{marker}/{analysis_center}/{sampling_period}/{coordinates_system}/{format}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML, MediaType.TEXT_PLAIN})
    public Response getTimeseriesMulti(@Context UriInfo uriInfo,
                                       @PathParam("marker") String marker,
                                       @PathParam("analysis_center") String analysis_center,
                                       @PathParam("sampling_period") String sampling_period,
                                       @PathParam("coordinates_system") String coordinates_system,
                                       @PathParam("format") String format,
                                       @DefaultValue("") @QueryParam("reference_frame") String reference_frame,
                                       @DefaultValue("") @QueryParam("otl_model") String otl_model,
                                       @DefaultValue("") @QueryParam("calibration_model") String calibration_model, //In the code this is known as antenna_model
                                       @DefaultValue("") @QueryParam("cut_of_angle") String cut_of_angle,
                                       @DefaultValue("") @QueryParam("remove_outliers") String remove_outliers,
                                       @DefaultValue("") @QueryParam("apply_offsets") String apply_offsets,
                                       @DefaultValue("") @QueryParam("epoch_start") String epoch_start,
                                       @DefaultValue("") @QueryParam("epoch_end") String epoch_end,
                                       @DefaultValue("") @QueryParam("version") String version) throws IOException, ParseException, java.text.ParseException {

        format = format.toLowerCase();

        if(!format.equals("xml") && !format.equals("json") && !format.equals("covjson") && !format.equals("pbo")){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        //extend the above to hector and check that hector has xyz only as enu is a no no for hector

        marker=marker.toUpperCase();

        if(format.equals("pbo")){
            coordinates_system = "xyz";
        }


        File f = new File("/opt/EPOS/timeseries/" +
                marker +"/"+ analysis_center +"/"+ sampling_period +"/"+ coordinates_system +"/"+ format +"-"+ reference_frame +"-"+ otl_model +"-"+
                calibration_model +"-"+ cut_of_angle +"-"+ null +"-"+ null +"-"+ remove_outliers +"-"+
                apply_offsets +"."+format);

        if(f.exists() && !f.isDirectory()) {

            if(epoch_start!=null || epoch_end!=null){
                return Response.ok(Tools.filterDate(f, epoch_start, epoch_end)).build();
            }
            else {
                return Response.ok(f).build();
            }
        }


        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList <Estimated_coordinates> res;
        try {
            switch (coordinates_system) {
                case "xyz":
                    res = dbc.getTimeseriesXYZBasedOnParameters(marker, analysis_center, sampling_period, format, reference_frame, otl_model, calibration_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries", version);
                    break;
                case "enu":
                    res = dbc.getTimeseriesENUBasedOnParameters(marker, analysis_center, sampling_period, format, reference_frame, otl_model, calibration_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries", version);
                    break;
                default:
                    return Response.status(Response.Status.BAD_REQUEST).build();
            }
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        if (res == null) {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        if (res.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        Parsers parser = new Parsers();
        
        switch (format){

            case "pbo":
        
                String pbo = PBOHandler.PBOEstimatedCoordinates(res, marker, sampling_period);
                return Response.ok(pbo).header("Content-Disposition", "attachment; filename=" + marker+".pos").build();

            case "xml":

                String xml;
                switch (coordinates_system) {
                    case "xyz":
                        xml = parser.getFullXMLTimeSeries(res);
                        break;
                    case "enu":
                        xml = parser.getFullXMLTimeSeriesENU(res);
                        break;
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
                return Response.ok(xml, MediaType.APPLICATION_XML).build();


            case "json":
                JSONArray obj;
                switch (coordinates_system) {
                    case "xyz":
                        obj = parser.getTimeSeriesByFormatJSON(res);
                        break;
                    case "enu":
                        obj = parser.getTimeSeriesByFormatENUJSON(res);
                        break;
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
                return Response.ok(obj.toJSONString(), MediaType.APPLICATION_JSON).build();

            case "covjson":
                JSONObject cov_obj;
                int coordSystem;
                switch (coordinates_system) {
                    case "xyz":
                        coordSystem=GlassConstants._XYZ;
                        break;
                    case "enu":
                        coordSystem=GlassConstants._ENU;
                        break;
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
                cov_obj = parser.getTimeSeriesByFormatCovJSON(res,
                        marker, analysis_center, sampling_period, coordinates_system,
                        format, reference_frame, otl_model, calibration_model, cut_of_angle,
                        remove_outliers, apply_offsets, epoch_start, epoch_end, version, coordSystem, context.getRequestUri());
                return Response.ok(cov_obj.toJSONString(), MediaType.APPLICATION_JSON).build();
            case "hector":
                //Paul 15-10-2022
                // hector has to be in xyz coordinates NOT enu
                //this has no header as in the end point above
                String hector = PBOHandler.CreateHectorFile(sampling_period,PBOHandler.PBOEstimatedCoordinates2MIDAS(res,marker, sampling_period));
                return Response.ok(hector, MediaType.TEXT_PLAIN).build();
            default:
                return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }

    /**
     * <p><b>GET TIMESERIES RESIDUALS</b></p>
     *
     * Get <i>timeseries <b>residuals</b></i> objects (which is a time series) for a given <b>station</b>, <b>analysis center</b>, <b>sampling period</b>, <b>coordinates system</b> and <b>format</b>.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>marker</b> - four letter station identification</li>
     *     <li><b>analysis_center</b> - name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>sampling_period</b> - name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)</li>
     *     <li><b>coordinates_system</b> - name of the desired coordinates system (<b>ONLY enu</b>)</li>
     *     <li><b>format</b> - name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)</li>
     * </ul>
     *
     *
     * <p>Optional parameters are:</p>
     * <ul>
     *     <li><b>reference_frame</b> - name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)</li>
     *     <li><b>otl_model</b> - name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)</li>
     *     <li><b>calibration_model</b> - name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)</li>
     *     <li><b>cut_of_angle</b> - value of the desired cut of angle</li>
     *     <li><b>remove_outliers</b> - remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)</li>
     *     <li><b>apply_offsets</b> - apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)</li>
     *     <li><b>epoch_start</b> - beginning of epoch (date format YYYY-MM-DD)</li>
     *     <li><b>epoch_end</b> -  end of epoch (date format YYYY-MM-DD)</li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     *  <li>The reference frame will be  <i>IGS08</i>, <i>IGS14</i>, <i>free-network</i>, <i>IGb08</i> or <i>INGV_EU</i>.</li>
     *  <li>The OTL model used in GNSS analysis will be, for example,  <i>FES2004</i>, <i>FES2014b</i> or <i>GOT4.10c</i></li>
     *  <li>The antenna calibration model used in GNSS analysis will be, for example, <i>epn_14_1958.atx</i> or <i>Gigs08_wwww.atx</i></li>
     *  <li>If no start and/or end epoch date are given, the whole time series will be returned.</li>
     *  <li>OTL model and calibration model are two parameters which refine the search.</li>
     *  <li>Sampling period tells us if daily or weekly solutions should be obtained.</li>
     *  <li>Cut of angle also restricts the search to solutions with cut-off angle larger than the one given.</li>
     *  <li>Remove outliers is a boolean parameter which indicates if coordinates that are considered outliers should be included or discarded.</li>
     *  <li>if apply offsets is true, then apply the estimated offsets value.</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/timeseries-residuals/BLIX/UGA/daily/json?epoch_start=2015-02-23&remove_outliers=false
     *
     *
     * @param marker four letter station identification
     * @param analysis_center name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)
     * @param sampling_period name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)
     * @param format name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)
     * @param reference_frame name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)
     * @param otl_model name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)
     * @param calibration_model name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)
     * @param cut_of_angle value of the desired cut of angle
     * @param remove_outliers remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)
     * @param apply_offsets apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)
     * @param epoch_start beginning of epoch (date format YYYY-MM-DD)
     * @param epoch_end end of epoch (date format YYYY-MM-DD)
     * @return String with the time series residuals objects in format specified in format field.
     * @author Rafael Couto, Machiel's documentation
     * @version 1.0
     * @since Unknown
     */
    @Path("timeseries-residuals/{marker}/{analysis_center}/{sampling_period}/{format}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getTimeseriesResidualsMulti(@PathParam("marker") String marker,
                                       @PathParam("analysis_center") String analysis_center,
                                       @PathParam("sampling_period") String sampling_period,
                                       @PathParam("format") String format,
                                       @DefaultValue("") @QueryParam("reference_frame") String reference_frame,
                                       @DefaultValue("") @QueryParam("otl_model") String otl_model,
                                       @DefaultValue("") @QueryParam("calibration_model") String calibration_model, //In the code this is known as antenna_model
                                       @DefaultValue("") @QueryParam("cut_of_angle") String cut_of_angle,
                                       @DefaultValue("") @QueryParam("remove_outliers") String remove_outliers,
                                       @DefaultValue("") @QueryParam("apply_offsets") String apply_offsets,
                                       @DefaultValue("") @QueryParam("epoch_start") String epoch_start,
                                       @DefaultValue("") @QueryParam("epoch_end") String epoch_end,
                                        @DefaultValue("") @QueryParam("version") String version) throws IOException, ParseException, java.text.ParseException {

        format = format.toLowerCase();
        if(!format.equals("xml") && !format.equals("json") && !format.equals("pbo") && !format.equals("midas") && !format.equals("hector")){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        marker=marker.toUpperCase();

        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList <Estimated_coordinates> res;
        try {
                res = dbc.getTimeseriesResidualsXYZorENUBasedOnParameters(marker, analysis_center, sampling_period, format, reference_frame, otl_model, calibration_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries", "ENU", version);
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        Parsers parser = new Parsers();

        if (res == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity("Something went wrong.").build();
        }

        if (res.isEmpty()){
            return Response.noContent().build();
        }


        switch (format){

            case "xml":
                String xml = parser.getFullXMLTimeSeriesENU(res);
                return Response.ok(xml, MediaType.APPLICATION_XML).build();

            case "json":
                JSONArray obj = parser.getTimeSeriesByFormatENUJSON(res);
                return Response.ok(obj.toJSONString(), MediaType.APPLICATION_JSON).build();

            default:
                return Response.status(Response.Status.BAD_REQUEST).entity("Something went wrong. Possibly wrong format.").build();
        }
    }





    /**
     * <p><b>GET TIMESERIES RESIDUALS FILE</b></p>
     *
     * Get <i>timeseries <b>residuals</b> file</i> objects (which is a time series) for a given <b>station</b>, <b>analysis center</b>, <b>sampling period</b> and <b>format</b>.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>marker</b> - four letter station identification</li>
     *     <li><b>analysis_center</b> - name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>sampling_period</b> - name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)</li>
     *     <li><b>format</b> - name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)</li>
     * </ul>
     *
     *
     * <p>Optional parameters are:</p>
     * <ul>
     *     <li><b>reference_frame</b> - name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)</li>
     *     <li><b>otl_model</b> - name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)</li>
     *     <li><b>calibration_model</b> - name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)</li>
     *     <li><b>cut_of_angle</b> - value of the desired cut of angle</li>
     *     <li><b>remove_outliers</b> - remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)</li>
     *     <li><b>apply_offsets</b> - apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)</li>
     *     <li><b>epoch_start</b> - beginning of epoch (date format YYYY-MM-DD)</li>
     *     <li><b>epoch_end</b> -  end of epoch (date format YYYY-MM-DD)</li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     *  <li>The reference frame will be  <i>IGS08</i>, <i>IGS14</i>, <i>free-network</i>, <i>IGb08</i> or <i>INGV_EU</i>.</li>
     *  <li>The OTL model used in GNSS analysis will be, for example,  <i>FES2004</i>, <i>FES2014b</i> or <i>GOT4.10c</i></li>
     *  <li>The antenna calibration model used in GNSS analysis will be, for example, <i>epn_14_1958.atx</i> or <i>Gigs08_wwww.atx</i></li>
     *  <li>If no start and/or end epoch date are given, the whole time series will be returned.</li>
     *  <li>OTL model and calibration model are two parameters which refine the search.</li>
     *  <li>Sampling period tells us if daily or weekly solutions should be obtained.</li>
     *  <li>Cut of angle also restricts the search to solutions with cut-off angle larger than the one given.</li>
     *  <li>Remove outliers is a boolean parameter which indicates if coordinates that are considered outliers should be included or discarded.</li>
     *  <li>if apply offsets is true, then apply the estimated offsets value.</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/timeseries-residuals/file/BLIX/UGA/daily/json?epoch_start=2015-02-23&remove_outliers=false
     *
     *
     * @param marker four letter station identification
     * @param analysis_center name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)
     * @param sampling_period name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)
     * @param format name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)
     * @param reference_frame name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)
     * @param otl_model name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)
     * @param calibration_model name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)
     * @param cut_of_angle value of the desired cut of angle
     * @param remove_outliers remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)
     * @param apply_offsets apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)
     * @param epoch_start beginning of epoch (date format YYYY-MM-DD)
     * @param epoch_end end of epoch (date format YYYY-MM-DD)
     * @return String with the time series residuals objects in format specified in format field.
     * @author Rafael Couto, Machiel's documentation
     * @version 1.0
     * @since Unknown
     */
    @Path("timeseries-residuals/file/{marker}/{analysis_center}/{sampling_period}/{format}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getTimeseriesResidualsFileMulti(@PathParam("marker") String marker,
                                                @PathParam("analysis_center") String analysis_center,
                                                @PathParam("sampling_period") String sampling_period,
                                                @PathParam("format") String format,
                                                @DefaultValue("") @QueryParam("reference_frame") String reference_frame,
                                                @DefaultValue("") @QueryParam("otl_model") String otl_model,
                                                @DefaultValue("") @QueryParam("calibration_model") String calibration_model, //In the code this is known as antenna_model
                                                @DefaultValue("") @QueryParam("cut_of_angle") String cut_of_angle,
                                                @DefaultValue("") @QueryParam("remove_outliers") String remove_outliers,
                                                @DefaultValue("") @QueryParam("apply_offsets") String apply_offsets,
                                                @DefaultValue("") @QueryParam("epoch_start") String epoch_start,
                                                @DefaultValue("") @QueryParam("epoch_end") String epoch_end,
                                                @DefaultValue("") @QueryParam("version") String version) throws IOException, ParseException, java.text.ParseException {

        format = format.toLowerCase();
        if(!format.equals("xml") && !format.equals("json") && !format.equals("pbo") && !format.equals("midas") && !format.equals("hector")){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        marker=marker.toUpperCase();

        File f = new File("/opt/EPOS/timeseries-residuals/" +
                marker +"/"+ analysis_center +"/"+ sampling_period +"/"+ format +"-"+ reference_frame +"-"+ otl_model +"-"+
                calibration_model +"-"+ cut_of_angle +"-"+ null +"-"+ null +"-"+ remove_outliers +"-"+
                apply_offsets +"."+format);

        if(f.exists() && !f.isDirectory()) {

            if(epoch_start!=null || epoch_end!=null){
                return Response.ok(String.valueOf(Tools.filterDate(f, epoch_start, epoch_end))).header("Content-Disposition", "attachment; filename=" + marker +"."+format).build();
            }
            else {
                return Response.ok(f).header("Content-Disposition", "attachment; filename=" + marker +"."+format).build();
            }
        }

        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList <Estimated_coordinates> res = null;
        try {
            if(format.equals("xml") || format.equals("json")){
                res = dbc.getTimeseriesResidualsXYZorENUBasedOnParameters(marker, analysis_center, sampling_period, format, reference_frame, otl_model, calibration_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries", "ENU", version);
            }else {
                if(format.equals("midas") || format.equals("hector")){
                    res = dbc.getTimeseriesResidualsXYZorENUBasedOnParameters(marker, analysis_center, sampling_period, format, reference_frame, otl_model, calibration_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries", "XYZ", version);
                }
            }
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        Parsers parser = new Parsers();

        if (res == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).entity("Something went wrong.").build();
        }

        if (res.isEmpty()){
            return Response.noContent().build();
        }


        switch (format){

            case "xml":
                String xml = parser.getFullXMLTimeSeriesENU(res);
                return Response.ok(xml).header("Content-Disposition", "attachment; filename=" + marker +".xml").build();

            case "json":
                JSONArray obj = parser.getTimeSeriesByFormatENUJSON(res);
                return Response.ok(String.valueOf(obj)).header("Content-Disposition", "attachment; filename=" + marker +".json").build();

            case "midas":
                return Response.ok(PBOHandler.CreateMidasFile(marker, res)).header("Content-Disposition", "attachment; filename=" + marker +".renu").build();

            case "hector":
                return Response.ok(PBOHandler.CreateHectorFile(sampling_period,res)).header("Content-Disposition", "attachment; filename=" + marker +".enu").build();


            default:
                return Response.status(Response.Status.BAD_REQUEST).entity("Something went wrong. Possibly wrong format.").build();
        }
    }




    /**
     * <p><b>GET VELOCITIES FILES</b></p>
     *
     * Get <i>velocity</i> files (which is a time series) for a given <b>station</b>, <b>analysis center</b>, <b>coordinates system</b> and <b>format</b>.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>marker</b> - four letter station identification</li>
     *     <li><b>analysis_center</b> - name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>coordinates_system</b> - name of the desired coordinates system (either xyz or enu)</li>
     *     <li><b>format</b> - name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)</li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/velocities/file/BLIX/UGA/xyz/json
     *
     *
     * @param marker four letter station identification
     * @param analysis_center name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)
     * @param coordinates_system name of the desired coordinates system (either xyz or enu)
     * @param format name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)
     * @return File with the time series objects in format specified in format field.
     * @author Andre Rodrigues, Machiel's documentation
     * @version 1.0
     * @since Unknown
     */
    @Path("velocities/file/{marker}/{analysis_center}/{coordinates_system}/{format}")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM )
    public Response getVelocitiesMultifile(@PathParam("marker") String marker,
                                           @PathParam("analysis_center") String analysis_center,
                                           @PathParam("coordinates_system") String coordinates_system,
                                           @PathParam("format") String format,
                                           @DefaultValue("") @QueryParam("tectonic_plate") String tectonic_plate) {


        format = format.toLowerCase();
        if(!format.equals("pbo") && !format.equals("xml") && !format.equals("json")){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        marker=marker.toUpperCase();

        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList<Reference_Position_Velocities> res;

        try {
            switch (coordinates_system) {
                case "xyz":
                    res = dbc.getVelocitiesXYZ(marker, analysis_center, tectonic_plate);
                    break;
                case "enu":
                    res = dbc.getVelocitiesENU(marker, analysis_center, tectonic_plate);
                    break;
                default:
                    return Response.status(Response.Status.BAD_REQUEST).build();
            }
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
            //e.printStackTrace();
        }

        if (res.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }


        Parsers parser = new Parsers();


        switch (format){
            case "pbo":
                DataBaseConnection db1c = new DataBaseConnection();
                Station station = db1c.getStationsShortByMarker(marker);
                return Response.ok(PBOHandlerVelocities.pbovelocities(res, station, analysis_center)).header("Content-Disposition", "attachment; filename=" + marker + ".vel").build();

            case "xml":
                switch (coordinates_system) {
                    case "xyz":
                        return Response.ok(parser.getVelocitiesXYZXML(res)).header("Content-Disposition", "attachment; filename=" + marker + ".xml").build();
                    case "enu":
                        return Response.ok(parser.getVelocitiesENUXML(res)).header("Content-Disposition", "attachment; filename=" + marker + ".xml").build();
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
            case "json":
                JSONArray obj;
                switch (coordinates_system) {
                    case "xyz":
                        obj = parser.getVelocitiesXYZJSON(res);
                        break;
                    case "enu":
                        obj = parser.getVelocitiesENUJSON(res);
                        break;
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
                return Response.ok(String.valueOf(obj)).header("Content-Disposition", "attachment; filename=" + marker + ".json").build();

            default:
                return Response.status(Response.Status.BAD_REQUEST).build();
        }

    }

    /**
     * <p><b>GET VELOCITIES</b></p>
     *
     * Get <i>velocity</i> objects (which is a time series) for a given <b>station</b>, <b>analysis center</b>, <b>coordinates system</b> and <b>format</b>.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>marker</b> - four letter station identification</li>
     *     <li><b>analysis_center</b> - name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>coordinates_system</b> - name of the desired coordinates system (either xyz or enu)</li>
     *     <li><b>format</b> - name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)</li>
     *     <li><b>tectonic_plate</b> - tectonic_plate name of the tectonic plate which velocity is subtracted from the velocity.</li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/velocities/BLIX/UGA/xyz/json
     *
     *
     * @param marker four letter station identification
     * @param analysis_center name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)
     * @param coordinates_system name of the desired coordinates system (either xyz or enu)
     * @param format name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)
     * @param tectonic_plate name of the tectonic plate which velocity is subtracted from the velocity.
     * @return String with the time series objects in format specified in format field.
     * @author Andre Rodrigues, Machiel's documentation
     * @version 1.0
     * @since Unknown
     */
    @Path("velocities/{marker}/{analysis_center}/{coordinates_system}/{format}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getVelocitiesMulti(@PathParam("marker") String marker,
                                       @PathParam("analysis_center") String analysis_center,
                                       @PathParam("coordinates_system") String coordinates_system,
                                       @PathParam("format") String format,
                                       @DefaultValue("") @QueryParam("tectonic_plate") String tectonic_plate) {

        format = format.toLowerCase();
        if(!format.equals("xml") && !format.equals("json") && !format.equals("pbo")){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        marker=marker.toUpperCase();

        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList <Reference_Position_Velocities> res;


        try {
            switch (coordinates_system) {
                case "xyz":
                    res = dbc.getVelocitiesXYZ(marker, analysis_center, tectonic_plate);
                    break;
                case "enu":
                    res = dbc.getVelocitiesENU(marker, analysis_center, tectonic_plate);
                    break;
                default:
                    return Response.status(Response.Status.BAD_REQUEST).build();
            }
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        Parsers parser = new Parsers();

        if (res.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }


        switch (format){

            case "xml":
                switch (coordinates_system) {
                    case "xyz":
                        return Response.ok(parser.getVelocitiesXYZXML(res), MediaType.APPLICATION_XML).build();
                    case "enu":
                        return Response.ok(parser.getVelocitiesENUXML(res), MediaType.APPLICATION_XML).build();
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
            case "json":
                JSONArray obj;
                switch (coordinates_system) {
                    case "xyz":
                        obj = parser.getVelocitiesXYZJSON(res);
                        break;
                    case "enu":
                        obj = parser.getVelocitiesENUJSON(res);
                        break;
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
                return Response.ok(obj.toJSONString(), MediaType.APPLICATION_JSON).build();


            default:
                return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }


    /**
     * <p><b>GET COORDINATES FILES</b></p>
     *
     * Get <i>coordinate</i> files (which is a time series) for a given <b>station</b>, <b>analysis center</b>, <b>sampling period</b> and <b>format</b>.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>marker</b> - four letter station identification</li>
     *     <li><b>analysis_center</b> - name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>sampling_period</b> - name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)</li>
     *     <li><b>format</b> - name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)</li>
     * </ul>
     *
     *
     * <p>Optional parameters are:</p>
     * <ul>
     *     <li><b>reference_frame</b> - name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)</li>
     *     <li><b>otl_model</b> - name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)</li>
     *     <li><b>calibration_model</b> - name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)</li>
     *     <li><b>cut_of_angle</b> - value of the desired cut of angle</li>
     *     <li><b>remove_outliers</b> - remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)</li>
     *     <li><b>apply_offsets</b> - apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)</li>
     *     <li><b>epoch_start</b> - beginning of epoch (date format YYYY-MM-DD)</li>
     *     <li><b>epoch_end</b> -  end of epoch (date format YYYY-MM-DD)</li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     *  <li>The reference frame will be  <i>IGS08</i>, <i>IGS14</i>, <i>free-network</i>, <i>IGb08</i> or <i>INGV_EU</i>.</li>
     *  <li>The OTL model used in GNSS analysis will be, for example,  <i>FES2004</i>, <i>FES2014b</i> or <i>GOT4.10c</i></li>
     *  <li>The antenna calibration model used in GNSS analysis will be, for example, <i>epn_14_1958.atx</i> or <i>Gigs08_wwww.atx</i></li>
     *  <li>If no start and/or end epoch date are given, the whole time series will be returned.</li>
     *  <li>OTL model and calibration model are two parameters which refine the search.</li>
     *  <li>Sampling period tells us if daily or weekly solutions should be obtained.</li>
     *  <li>Cut of angle also restricts the search to solutions with cut-off angle larger than the one given.</li>
     *  <li>Remove outliers is a boolean parameter which indicates if coordinates that are considered outliers should be included or discarded.</li>
     *  <li>if apply offsets is true, then apply the estimated offsets value.</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/coordinates/file/BLIX/UGA/daily/json?epoch_start=2015-02-23&remove_outliers=false
     *
     *
     * @param marker four letter station identification
     * @param analysis_center name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)
     * @param sampling_period name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)
     * @param format name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)
     * @param reference_frame name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)
     * @param otl_model name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)
     * @param calibration_model name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)
     * @param cut_of_angle value of the desired cut of angle
     * @param remove_outliers remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)
     * @param apply_offsets apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)
     * @param epoch_start beginning of epoch (date format YYYY-MM-DD)
     * @param epoch_end end of epoch (date format YYYY-MM-DD)
     * @return File with the time series objects in format specified in format field.
     * @author Andre Rodrigues, Machiel's documentation
     * @version 1.0
     * @since Unknown
     */
    @Path("coordinates/file/{marker}/{analysis_center}/{sampling_period}/{format}")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getCoordinatesFileByFormat(@PathParam("marker") String marker,
                                               @PathParam("analysis_center") String analysis_center,
                                               @PathParam("sampling_period") String sampling_period,
                                               @PathParam("format") String format,
                                               @DefaultValue("") @QueryParam("reference_frame") String reference_frame,
                                               @DefaultValue("") @QueryParam("otl_model") String otl_model,
                                               @DefaultValue("") @QueryParam("calibration_model") String calibration_model, //In the code this is known as antenna_model
                                               @DefaultValue("") @QueryParam("cut_of_angle") String cut_of_angle,
                                               @DefaultValue("") @QueryParam("remove_outliers") String remove_outliers,
                                               @DefaultValue("") @QueryParam("apply_offsets") String apply_offsets,
                                               @DefaultValue("") @QueryParam("epoch_start") String epoch_start,
                                               @DefaultValue("") @QueryParam("epoch_end") String epoch_end,
                                               @DefaultValue("") @QueryParam("value") String value,
                                               @DefaultValue("") @QueryParam("version") String version) {

        format = format.toLowerCase();
        if(!format.equals("xml") && !format.equals("json") && !format.equals("pbo") && !format.equals("midas") && !format.equals("hector")){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        marker=marker.toUpperCase();

        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList <Estimated_coordinates> res;

        try {
            res = dbc.getTimeseriesXYZBasedOnParameters(marker, analysis_center, sampling_period, format, reference_frame, otl_model, calibration_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "coordinates", version);
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        if (res == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }

        if (res.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }
        Parsers parser = new Parsers();


        switch (format){

            case "xml":
                String xml = parser.getFullXMLCoordinates(res);
                return Response.ok(String.valueOf(xml)).header("Content-Disposition", "attachment; filename=" + marker+".xml").build();

            case "json":
                JSONArray json = parser.getTimeSeriesByFormatJSON(res);
                return Response.ok(String.valueOf(json)).header("Content-Disposition", "attachment; filename=" + marker+".json").build();

            case "pbo":
                return Response.ok(PBOHandler.PBOEstimatedCoordinates(res, marker, sampling_period)).header("Content-Disposition", "attachment; filename=" + marker+".pos").build();

            case "midas":
                return Response.ok(PBOHandler.CreateMidasFile(marker,PBOHandler.PBOEstimatedCoordinates2MIDAS(res, marker, sampling_period))).header("Content-Disposition", "attachment; filename=" + marker+".renu").build();

            case "hector":
                return Response.ok(PBOHandler.CreateHectorFile(sampling_period,PBOHandler.PBOEstimatedCoordinates2MIDAS(res,marker, sampling_period))).header("Content-Disposition", "attachment; filename=" + marker+".enu").build();

            default:
                return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }


    /**
     * <p><b>GET COORDINATES </b></p>
     *
     * Get <i>coordinate</i> objects (which is a time series) for a given <b>station</b>, <b>analysis center</b>, <b>sampling period</b>, <b>coordinates system</b> and <b>format</b>.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>marker</b> - four letter station identification</li>
     *     <li><b>analysis_center</b> - name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>sampling_period</b> - name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)</li>
     *     <li><b>coordinates_system</b> - name of the desired coordinates system (either xyz or enu)</li>
     *     <li><b>format</b> - name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)</li>
     * </ul>
     *
     *
     * <p>Optional parameters are:</p>
     * <ul>
     *     <li><b>reference_frame</b> - name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)</li>
     *     <li><b>otl_model</b> - name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)</li>
     *     <li><b>calibration_model</b> - name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)</li>
     *     <li><b>cut_of_angle</b> - value of the desired cut of angle</li>
     *     <li><b>remove_outliers</b> - remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)</li>
     *     <li><b>apply_offsets</b> - apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)</li>
     *     <li><b>epoch_start</b> - beginning of epoch (date format YYYY-MM-DD)</li>
     *     <li><b>epoch_end</b> -  end of epoch (date format YYYY-MM-DD)</li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     *  <li>The reference frame will be <i>IGS08</i>, <i>IGS14</i>, <i>free-network</i>, <i>IGb08</i> or <i>INGV_EU</i>.</li>
     *  <li>The OTL model used in GNSS analysis will be, for example,  <i>FES2004</i>, <i>FES2014b</i> or <i>GOT4.10c</i></li>
     *  <li>The antenna calibration model used in GNSS analysis will be, for example, <i>epn_14_1958.atx</i> or <i>Gigs08_wwww.atx</i></li>
     *  <li>If no start and/or end epoch date are given, the whole time series will be returned.</li>
     *  <li>OTL model and calibration model are two parameters which refine the search.</li>
     *  <li>Sampling period tells us if daily or weekly solutions should be obtained.</li>
     *  <li>Cut of angle also restricts the search to solutions with cut-off angle larger than the one given.</li>
     *  <li>Remove outliers is a boolean parameter which indicates if coordinates that are considered outliers should be included or discarded.</li>
     *  <li>if apply offsets is true, then apply the estimated offsets value.</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/coordinates/BLIX/UGA/daily/xyz/json?epoch_start=2015-02-23&remove_outliers=false
     *
     *
     * @param marker four letter station identification
     * @param analysis_center name of desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)
     * @param sampling_period name of the resolution of solutions (please refer to <i>products/attributes/sampling_period</i> request for possible sampling periods)
     * @param coordinates_system name of the desired coordinates system (either xyz or enu)
     * @param format name of the output format (please refer to <i>products/attributes/format</i> request for possible output formats)
     * @param reference_frame name of the desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)
     * @param otl_model name of the desired OTL model used in GNSS analysis (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)
     * @param calibration_model name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)
     * @param cut_of_angle value of the desired cut of angle
     * @param remove_outliers remove outliers from time series or not (1 or true to remove, 0 or false to include. If no value is given, outliers are removed by default)
     * @param apply_offsets apply estimated offsets to time series or not (1 or true to apply, 0 or false to not apply. If no value is given, offsets are not applied by default)
     * @param epoch_start beginning of epoch (date format YYYY-MM-DD)
     * @param epoch_end end of epoch (date format YYYY-MM-DD)
     * @return String with the time series objects in format specified in format field.
     * @author Andre Rodrigues, Machiel's documentation
     * @version 1.0
     * @since Unknown
     */
    @Path("coordinates/{marker}/{analysis_center}/{sampling_period}/{coordinates_system}/{format}")
    @GET
    @Produces({MediaType.APPLICATION_JSON, MediaType.APPLICATION_XML})
    public Response getCoordinatesMulti(@PathParam("marker") String marker,
                                        @PathParam("analysis_center") String analysis_center,
                                        @PathParam("sampling_period") String sampling_period,
                                        @PathParam("coordinates_system") String coordinates_system,
                                        @PathParam("format") String format,
                                        @DefaultValue("") @QueryParam("reference_frame") String reference_frame,
                                        @DefaultValue("") @QueryParam("otl_model") String otl_model,
                                        @DefaultValue("") @QueryParam("calibration_model") String calibration_model, //In the code this is known as antenna_model
                                        @DefaultValue("") @QueryParam("cut_of_angle") String cut_of_angle,
                                        @DefaultValue("") @QueryParam("remove_outliers") String remove_outliers,
                                        @DefaultValue("") @QueryParam("apply_offsets") String apply_offsets,
                                        @DefaultValue("") @QueryParam("epoch_start") String epoch_start,
                                        @DefaultValue("") @QueryParam("epoch_end") String epoch_end,
                                        @DefaultValue("") @QueryParam("version") String version) {

        format = format.toLowerCase();
        if(!format.equals("xml") && !format.equals("json") && !format.equals("covjson")){
            return Response.status(Response.Status.BAD_REQUEST).build();
        }
        marker=marker.toUpperCase();


        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList <Estimated_coordinates> res = null;
        try {
            switch (coordinates_system) {
                case "xyz":
                    res = dbc.getTimeseriesXYZBasedOnParameters(marker, analysis_center, sampling_period, format, reference_frame, otl_model, calibration_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "coordinates", version);
                    break;
                case "enu":
                    res = dbc.getTimeseriesENUBasedOnParameters(marker, analysis_center, sampling_period, format, reference_frame, otl_model, calibration_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "coordinates", version);
                    break;
                default:
                    return Response.status(Response.Status.BAD_REQUEST).build();
            }
        } catch (SQLException e) {
            Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
        Parsers parser = new Parsers();

        if (res == null)
        {
            return Response.status(Response.Status.BAD_REQUEST).build();
        }


        if (res.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        switch (format){

            case "xml":

                String xml;
                switch (coordinates_system) {
                    case "xyz":
                        xml = parser.getFullXMLCoordinates(res);
                        break;
                    case "enu":
                        xml = parser.getFullXMLCoordinatesENU(res);
                        break;
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
                return Response.ok(xml, MediaType.APPLICATION_XML).build();

            case "json":
                JSONArray obj;
                switch (coordinates_system) {
                    case "xyz":
                        obj = parser.getTimeSeriesByFormatJSON(res);
                        break;
                    case "enu":
                        obj = parser.getTimeSeriesByFormatENUJSON(res);
                        break;
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
                return Response.ok(obj.toJSONString(), MediaType.APPLICATION_JSON).build();
            case "covjson": //add covjson return to the raw coordinates
                JSONObject cov_obj;
                int coordSystem;
                switch (coordinates_system) {
                    case "xyz":
                        coordSystem=GlassConstants._XYZ;
                        break;
                    case "enu":
                        coordSystem=GlassConstants._ENU;
                        break;
                    default:
                        return Response.status(Response.Status.BAD_REQUEST).build();
                }
                cov_obj = parser.getTimeSeriesByFormatCovJSON(res,
                        marker, analysis_center, sampling_period, coordinates_system,
                        format, reference_frame, otl_model, calibration_model, cut_of_angle,
                        remove_outliers, apply_offsets, epoch_start, epoch_end, version, coordSystem, context.getRequestUri());
                return Response.ok(cov_obj.toJSONString(), MediaType.APPLICATION_JSON).build();
            default:
                return Response.status(Response.Status.BAD_REQUEST).build();
        }
    }


    /**
     * <p><b>GET STATIONS WITHS PRODUCTS </b></p>
     *
     * Get <i>station</i> objects which have available products.
     *
     * <br />
     * <p>Optional parameters are:</p>
     * <ul>
     *     <li><b>analysis_center</b> - name of the desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>calibration_model</b> - name of the desired antenna calibration model used in GNSS analysis (please refer to <i>products/attributes/calibration_model</i> request for possible calibration models)</li>
     *     <li><b>otl_model</b> - name of the desired otl model (please refer to <i>products/attributes/otl_model</i> request for possible OTL models)</li>
     *     <li><b>reference_frame</b> - name of desired reference frame (please refer to <i>products/attributes/reference_frame</i> request for possible reference frames)</li>
     *     <li><b>cut_of_angle</b> - desired value of cut of angle</li>
     *     <li><b>epoch_start</b> - stations with products that begin in specified epoch (date format YYYY-MM-DD)</li>
     *     <li><b>epoch_end</b> - stations with products that end in specified epoch (date format YYYY-MM-DD) </li>
     *     <li><b>coordinates</b> - type of bounding box:
     *          <ul>
     *              <li>rectangle:min_lat,max_lat,min_lon,max_lon (all coordinates must be provided in decimal degree format (<i>-8.1234</i>))</li>
     *              <li>circle:lat,lon,radius (coordinates must be provided in decimal degree format (<i>-8.1234</i>) and radius in meters)</li>
     *              <li>polygon:[coordinates_list] (all coordinates must be provided in decimal degree format (<i>-8.1234</i>))</li>
     *          </ul>
     *     </li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     *  <li>Please note that you can only use each optional parameter once.</li>
     *  <li>The reference frame will be <i>IGS08</i>, <i>IGS14</i>, <i>free-network</i>, <i>IGb08</i> or <i>INGV_EU</i>.</li>
     *  <li>The OTL model used in GNSS analysis will be, for example,  <i>FES2004</i>, <i>FES2014b</i> or <i>GOT4.10c</i></li>
     *  <li>The antenna calibration model used in GNSS analysis will be, for example, <i>epn_14_1958.atx</i> or <i>Gigs08_wwww.atx</i></li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Examples</b></p>
     * localhost:8080/GlassFramework/webresources/products/stations/analysis_centre=BFKH&epoch_start=2017-02-23
     * localhost:8080/GlassFramework/webresources/products/stations/coordinates_rectangle=35.99255408802867,44.5677027377636,-11.429776400327684,-6.515891700983048&epoch_start=2015-01-01
     *
     *
     * @param optional_parameters Parameter1=value1,value2&Parameter2=value3,value4&…
     * @return JSON string with the station objects
     * @author Rafael Couto
     * @version 1.0
     * @since Unknown
     */
    @Path("stations{optional_parameters:.*}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStations(@PathParam("optional_parameters") String optional_parameters) throws java.text.ParseException {
        DataBaseT4Connection dbc = new DataBaseT4Connection();

        if(optional_parameters.length()>0){
            if(optional_parameters.charAt(0)=='/'){
                optional_parameters = optional_parameters.substring(1);
            }
        }

        // Turn result to JSON array
        ArrayList<Station> res;
/*
What the xx is this doing here
        if(   optional_parameters.length()>1){
            optional_parameters = optional_parameters.substring(1);
        }
*/
        if (!optional_parameters.equals("") && !optional_parameters.equals(" "))
            res = dbc.getStationsBasedOnParameters(optional_parameters);
        else
            res = dbc.getAllStationsWithProducts();

        Parsers parser = new Parsers();

        if (res.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }


        JSONArray obj = parser.getShortJSON(dbc.checkPowerSpectral(res));

        return Response.ok(obj.toJSONString()).build();
    }


    /**
     * <p><b>GET STATIONS WITH PRODUCTS INSIDE BOUNDING BOX</b></p>
     *
     *  Get stations inside provided bounding box.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>min_lat</b> - minimum latitude in decimal degree format (<i>-8.1234</i>)</li>
     *     <li><b>max_lat</b> - maximum latitude in decimal degree format (<i>-8.1234</i>)</li>
     *     <li><b>min_lon</b> - minimum longitude in decimal degree format (<i>-8.1234</i>)</li>
     *     <li><b>max_lon</b> - maximum longitude in decimal degree format (<i>-8.1234</i>)</li>
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/stations/-3.52123/5.5123/-24.123123/5.41231
     *
     *
     * @param min_lat minimum latitude in decimal format
     * @param max_lat maximum latitude in decimal format
     * @param min_lon minimum longitude in decimal format
     * @param max_lon maximum longitude in decimal format
     * @return JSON string with the station objects
     * @author Rafael Couto
     * @version 1.0
     * @since Unknown
     */
    @Path("stations/{min_lat}/{max_lat}/{min_lon}/{max_lon}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getStationsByRectangleBoundingBox(@PathParam("min_lat") String min_lat,
                                                      @PathParam("max_lat") String max_lat,
                                                      @PathParam("min_lon") String min_lon,
                                                      @PathParam("max_lon") String max_lon) {

        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList<Station> res;

        res = dbc.getAllStationsWithProducts(min_lat, max_lat, min_lon, max_lon);

        Parsers parser = new Parsers();

        if (res.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        JSONArray obj = parser.getShortJSON(dbc.checkPowerSpectral(res));

        return Response.ok(obj.toJSONString()).build();
    }



    /**
     * <p><b>GET VELOCITY FIELD OF STATION(S)</b></p>
     *
     *  Get the velocity field for a given station or all stations.
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *  <li><b>marker</b> - four letter single station identification or <i>all</i> keyword for all stations</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/velocityfield/all
     * http://localhost:8080/GlassFramework/webresources/products/velocityfield/casc
     * http://localhost:8080/GlassFramework/webresources/products/velocityfield/casc,soph
     *
     * @param marker four letter single station identification or <i>all</i> keyword for all stations
     * @return JSON string with the velocity field object.
     * @author Andre Rodrigues, Machiel's documentation, Rewritten and corrected by Paul
     * @version 1.1
     * @since Unknown
     *
     */
    @Path("velocityfield/{marker}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getVelocityfield(@PathParam("marker") String marker) {

        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList <ArrayList<VelocityByPlateAndAc>> res;
        marker=marker.toUpperCase();

        try {
            res = dbc.VelocitiesField(marker);
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }

        if (res.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        Parsers parser = new Parsers();
        JSONArray obj = parser.getVelocitiesFieldJSON(res);
        String json = obj.toJSONString();

        return Response.ok(String.valueOf(json)).build();
    }



    /**
     * <p><b>GET Products  AVAILABILITY INDIVIDUAL</b></p>
     *
     * Gets time intervals where stations with TimeSeries have data.
     * @return JSON Array with the time intervals where stations have TimeSeries data and all
     * associated metadata on product/version etc
     * @author Paul Crocker
     * @since 16/03/2022
     *
     */
    @Path("/{productType}/data-availability")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTSAvailabilityIndividual(@PathParam("productType") String productType) {
        try {
            DataBaseT4Connection dbc = new DataBaseT4Connection();
            ArrayList<ProductDataAvailability> dList;
            switch (productType.toLowerCase()) {
                case "timeseries":
                    dList = dbc.getDataAvailabilityV2(GlassConstants.PRODUCTS_TIMESERIES);
                    break;
                case "coordinates":
                    dList = dbc.getDataAvailabilityV2(GlassConstants.PRODUCTS_COORDINATES);
                    break;
                case "alltimeseries":
                    dList = dbc.getDataAvailabilityV2(GlassConstants.PRODUCTS_ALLTIMESERIES);
                    break;
                case "velocities":
                    dList = dbc.getDataAvailabilityV2(GlassConstants.PRODUCTS_VELOCITES);
                    break;
                default:
                    return Response.status(400).entity("Use timeseries OR coordinates or alltimeseries or velocities").build();
            }
            return Response.ok( ProductDataAvailability.ArrayListToJson( dList ) ).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * <p><b>GET TimeSeries AVAILABILITY INDIVIDUAL</b></p>
     *
     * Gets individual time intervals where a station with TimeSeries have data.
     * @Param  String marker or Integer with the database staation id
     * @return JSON string with the time intervals where stations have timeseries data and all
     * associated metadata on product/version etc
     * @author Paul Crocker
     * @since 16/03/2022
     *
     */
    @Path("/{productType}/data-availability/{marker}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTSAvailabilityIndividual(@PathParam("productType") String productType, @PathParam("marker") String marker) {

        Integer idStation;
        try {
            idStation = Integer.parseInt(marker);
        } catch (NumberFormatException e) {
            //its a Marker
            JsonCache cache = new JsonCache();
            try {
                idStation = cache.getKeyByValue(marker);
            } catch (IOException ex) {
                return Response.ok(ex.getMessage()).build();
            }
        }
        System.out.println("Id = "+idStation);

        try {
            DataBaseT4Connection dbc = new DataBaseT4Connection();
            ArrayList<ProductDataAvailability> dList;
                switch (productType.toLowerCase()) {
                    case "timeseries":
                        dList = dbc.getDataAvailabilityIndividualV2(idStation, GlassConstants.PRODUCTS_TIMESERIES);
                        break;
                    case "coordinates":
                        dList = dbc.getDataAvailabilityIndividualV2(idStation, GlassConstants.PRODUCTS_COORDINATES);
                        break;
                    case "alltimeseries":
                        dList = dbc.getDataAvailabilityIndividualV2(idStation, GlassConstants.PRODUCTS_ALLTIMESERIES);
                        break;
                    case "velocities":
                        dList = dbc.getDataAvailabilityIndividualV2(idStation, GlassConstants.PRODUCTS_VELOCITES);
                        break;
                    default:
                        return Response.status(400).entity("Use timeseries OR cordinates or alltimeseries or velocities and a station id or station marker").build();
                }
                return Response.ok(ProductDataAvailability.ArrayListToJson(dList)).build();
        } catch (SQLException e) {
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
        }
    }

    /**
     * <p><b>GET DIFFERENT DB DATA BASED ON ATTRIBUTE</b></p>
     *
     * Gets attributes for application in other requests
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>attribute</b> - string with one of the possible attributes described below.
     * </ul>
     *
     * <p>Possible attributes:</p>
     * <ul>
     *     <li><b>network</b> - Gets all networks</li>
     *     <li><b>analysis_center</b> - Gets all available analysis centers</li>
     *     <li><b>reference_frame</b> - Gets all available reference frames</li>
     *     <li><b>otl_model</b> - Gets all available OTL models</li>
     *     <li><b>calibration_model</b> - Gets all available calibration models</li>
     *     <li><b>sampling_period</b> - Gets all available sampling periods</li>
     *     <li><b>format</b> - Gets all available output formats</li>
     * </ul>
     *
     * <p>Additional parameters:</p>
     * <ul>
     *      <li><b>type</b></li> - Specifies if it's only for 1 type (e.g. velocities)
     * </ul>
     *
     *
     * <p><b>Notes</b></p>
     * <ul>
     *  <li>Parameters are case sensitive</li>
     *  <li>The reference frame will be <i>IGS08</i>, <i>IGS14</i>, <i>free-network</i>, <i>IGb08</i> or <i>INGV_EU</i>.</li>
     *  <li>The OTL model used in GNSS analysis will be, for example,  <i>FES2004</i>, <i>FES2014b</i> or <i>GOT4.10c</i></li>
     *  <li>The antenna calibration model used in GNSS analysis will be, for example, <i>epn_14_1958.atx</i> or <i>Gigs08_wwww.atx</i></li>
     * </ul>
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/attributes/analysis_center
     *
     *
     * @return <i>JSON</i> String with the <i>attributes</i>.
     * @author Rafael Couto
     * @author Paul Crocker
     * @author José Manteigueiro
     * @since 15/12/2017
     * @modified 18/12/2020
     * by José Manteigueiro
     *
     */
    @Path("attributes/{attribute}")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getAttributes(@PathParam("attribute") String attribute,
                                  @QueryParam("type") @DefaultValue("") String type) {

        attribute = attribute.toLowerCase();
        if(!type.equals("")){
            type = type.toLowerCase();
        }

        if(type.equals("velocities")){
            attribute += "-velocities";
        }

        if(type.equals("timeseries")){
            attribute += "-timeseries";
        }


        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        HashMap<String, String> result;

        //Strip off any beginning or trailing spaces
        result = dbc.getAttributesBasedOnKeyword( attribute.trim() );

        Parsers parser = new Parsers();

        if (result.isEmpty()){
            return Response.status(Response.Status.NO_CONTENT).build();
        }

        JSONObject obj = parser.getProductsAttributesJSON(result);
        String json = obj.toJSONString();

        return Response.ok(String.valueOf(json)).build();

    }


    /**
     * <p><b>RETRIEVES POWER SPECTRAL DENSITY PLOTS</b></p>
     *
     * Get power spectral density plots
     *
     * <br />
     * <p>Parameters:</p>
     * <ul>
     *     <li><b>analysis_center</b> - name of the desired analysis center (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)</li>
     *     <li><b>marker</b> - the site which you want to download data from (please refer to <i>products/stations</i> request for possible site names)</li>
     * </ul>
     *
     *
     * <br />
     * <p><b>Example</b></p>
     * http://localhost:8080/GlassFramework/webresources/products/power-spectral-density/BLIX/WUT
     *
     * @param analysis_center the analysis center that produced the data for the creation of the power spectral density plots (please refer to <i>products/attributes/analysis_center</i> request for possible analysis centers)
     * @param marker the site which you want to download data from (please refer to <i>products/stations</i> request for possible site names)
     * @return <i>PNG</i> Image with power spectral density plots.
     * @author Rafael Couto
     * @since 15/12/2017
     *
     */
    @Path("power-spectral-density/{analysis_center}/{marker}")
    @GET
    @Produces({"image/png"})
    public Response getPowerSpectral(@PathParam("analysis_center") String analysis_center,
                                     @PathParam("marker") String marker) {

        String path= new StringBuilder().append(GlassConstants.PowerDensityPlotsPath).append(analysis_center).append("/psd_figures/").append(marker).toString();

        File PS_E = new File(path+  "_0.png");
        File PS_N = new File(path + "_1.png");
        File PS_U = new File(path + "_2.png");

        if(     PS_E.exists() && !PS_E.isDirectory() &&
                PS_N.exists() && !PS_N.isDirectory() &&
                PS_U.exists() && !PS_U.isDirectory()) {

            int singleImageWidth, singleImageHeight;

            Image[] buffImages = new Image[3];
            try {

                buffImages[0] = ImageIO.read(PS_E);
                buffImages[1] = ImageIO.read(PS_N);
                buffImages[2] = ImageIO.read(PS_U);

                singleImageWidth = buffImages[0].getWidth(null);
                singleImageHeight = buffImages[0].getHeight(null);

                BufferedImage finalImg = new BufferedImage(singleImageWidth*3, singleImageHeight, 5 );
                Graphics g = finalImg.createGraphics();

                g.drawImage(buffImages[0], singleImageWidth*0, 0, null);
                g.drawImage(buffImages[1], singleImageWidth*1, 0, null);
                g.drawImage(buffImages[2], singleImageWidth*2, 0, null);

                return Response.ok(finalImg).build();

            } catch (IOException e) {
                e.printStackTrace();
                return Response.status(Response.Status.NO_CONTENT).build();
            }

        } else {
            return Response.status(Response.Status.NO_CONTENT).build();
        }
    }
    @Path("time-series-plot/{analysis_center}/{marker}")
    @GET
    @Produces({"image/png"})
    public Response getTimeSeries(@PathParam("analysis_center") String analysis_center, @PathParam("marker") String marker,@DefaultValue("") @QueryParam("TectonicPlate") String TectonicPlate) {
        String path = "";
        if(analysis_center.equalsIgnoreCase("all")){
                 String marker_9 = JsonCache.getLongMarkerByMarker(marker);
                    path = new StringBuilder().append("/mnt/NAS/public/plots/ICS/").append(marker).append(".png").toString();
            File TS =  new File( path );
            if( TS.isDirectory() || (!TS.exists()) )  {
                path = new StringBuilder().append("/mnt/NAS/public/plots/ICS/").append(marker_9).append(".png").toString();
            }

        }else if(!(analysis_center.isEmpty()) && !(marker.isEmpty()) && (TectonicPlate.isEmpty())){
            String marker_9 = JsonCache.getLongMarkerByMarker(marker);
            path = new StringBuilder().append(GlassConstants.TimeSeriesPlotsPath).
                    append(analysis_center).append("/ABSO/").append(marker_9).append(".png").toString();

        }else if(!(analysis_center.isEmpty()) && !(marker.isEmpty()) && !(TectonicPlate.isEmpty())){
            path = new StringBuilder().append(GlassConstants.TimeSeriesPlotsPath).
                    append(analysis_center).append("/").append(TectonicPlate).append("/").append(marker).append(".png").toString();
        }

        try {
            File TS =  new File( path );
            if( TS.isDirectory() || (!TS.exists()) )  {
                //check this default file
                path= new StringBuilder().append(GlassConstants.TimeSeriesPlotsPath).append("default-no-ts.png").toString();
                TS =  new File( path );
            }
            BufferedImage  buffImage = ImageIO.read(TS);
            return Response.ok(buffImage).build();
        } catch (Exception e) {
            //It is an error to get here. Maybe default response image is missing or image is corrupted
            e.printStackTrace();
            return Response.status(Response.Status.NO_CONTENT).build();
        }
    }

    private String jsonstring;

    /**
     * <p><b>CREATE CUSTOMIZED DOWNLOAD REQUEST FILE</b></p>
     *
     * Creates a customized download request based on data specified on Products Gateway
     *
     * <br />
     * <p>POST parameter:</p>
     * <ul>
     *     <li><b>data</b> - JSON string with the parameters needed to process the customized download request.</li>
     * </ul>
     *
     *
     * @param data JSON string with the parameters needed to process the customized download request.
     * @return Job status
     * @author Rafael Couto
     * @since 21/12/2017
     */
    @Path("timeseries/create-download-file")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCreateTimeseriesDownload(@FormParam("data") String data) {


        Logger.getLogger(Products.class.getName()).log(Level.INFO, "REQUEST: timeseries/create-download-file - Started");

        this.jsonstring = data;

        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.submit(timeseriesDownloadTask);

        return Response.status(Response.Status.ACCEPTED).build();

    }

    /**
     * <p><b>CREATE CUSTOMIZED DOWNLOAD REQUEST FILE MAP</b></p>
     *
     * Creates a customized download request based on data specified on Products Gateway from rectangle selection on map
     *
     * <br />
     * <p>POST parameter:</p>
     * <ul>
     *     <li><b>data</b> - JSON string with the parameters needed to process the customized download request.</li>
     * </ul>
     *
     *
     * @param data JSON string with the parameters needed to process the customized download request from rectangle selection on map.
     * @return Job status
     * @author Andre Rodrigues
     * @since 04/04/2019
     */
    @Path("timeseries/create-download-file-map")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCreateTimeseriesDownloadMap(@FormParam("data") String data) {


        Logger.getLogger(Products.class.getName()).log(Level.INFO, "REQUEST: timeseries/create-download-file-map - Started");

        this.jsonstring = data;

        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.submit(task2);

        return Response.status(Response.Status.ACCEPTED).build();

    }


    /**
     * <p><b>DOWNLOAD CUSTOMIZED DOWNLOAD REQUEST FILE</b></p>
     *
     * Downloads customized download request based on request previously made on Products Gateway
     *
     * <br />
     * <p>Parameter:</p>
     * <ul>
     *     <li><b>filename</b> - name of the file with the customized timeseries to be downloaded. (this filename was sent by email to the person who asked for a customized download)</li>
     * </ul>
     *
     *
     * @param filename name of the file with the customized timeseries to be downloaded
     * @return Job status
     * @author Rafael Couto
     * @since 21/12/2017
     */
    @Path("timeseries/download-file/{filename}")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getTimeseriesDownload(@PathParam("filename") String filename) {

        File f = new File("/opt/EPOS/temp/"+filename);

        if(f.exists() && !f.isDirectory())
        {
            return Response.ok(f).header("Content-Type", "application/zip").build();
        }
        else{
            Response.status(Response.Status.NO_CONTENT).build();
        }
        return null;
    }





    /**
     * <b>Sends notification to user notifying him that customized download request based on data specified on Products Gateway is ready</b>
     * Returns email.
     *
     * @since 21/12/2017
     * Last modified: 21/12/2017
     * Author Rafael Couto
     */
    private final Callable<Integer> timeseriesDownloadTask = () -> {

        TimeseriesMultiple t = new TimeseriesMultiple();
        String zipfile = t.TimeSeriesMultiple(this.jsonstring);

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(this.jsonstring);
        JSONObject jsonObject = (JSONObject) obj;


        String smtpserver = "";
        Integer port = 0;
        String username = "";
        String password = "";
        String debug = "";

        smtpserver = DBConsts.GLASS_EMAIL_SMTP_SERVER;
        port = Integer.valueOf(DBConsts.GLASS_EMAIL_PORT);
        username = DBConsts.GLASS_EMAIL_ADDRESS;
        password = DBConsts.GLASS_EMAIL_PASSWORD;
        debug = DBConsts.GLASS_EMAIL_DEBUG;
        if(password.equals("") || username.equals("") || smtpserver.equals("")){
            System.out.println("> No email configuration loaded");
            return null;
        }

        try {

            Properties props =  System.getProperties();
            props.put("mail.smtp.host", smtpserver);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", port);
            props.put("mail.smtp.starttls.enable","true");
            props.put("mail.debug", debug);

            final String finalUsername = username;
            final String finalPassword = password;
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        @Override
                        protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                            return new javax.mail.PasswordAuthentication(finalUsername, finalPassword);
                        }
                    });

            System.out.println("Preparing email");

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse((String) jsonObject.get("email")));

            String subject;
            String emailmessage;
            File f = new File("/opt/EPOS/temp/"+zipfile);
            if (zipfile != null && !f.isDirectory() && f.exists()) {
                message.setSubject("[EPOS - Products Gateway] Your timeseries request is ready!");
                subject = "[EPOS - Products Gateway] Your customized timeseries request!";
                emailmessage = "\t\t\t\t\t<div class='movableContent'>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100%' colspan='3' align='center' style='padding-bottom:10px;padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t                  \t\t<h2 >EPOS - Products Gateway - Custom Download Request</h2>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='400' align='center'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                        "\t\t\t\t\t                  \t\t<p >Hi there,\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tYour customized timeseries request has been processed and is now ready for download. Be aware that this link will expire after some time.\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tPlease click the button below to access your data:</p>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200' align='center' style='padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<td bgcolor='#964296' align='center' style='border-radius:4px;' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t\t\t\t                  \t\t<a target='_blank' href='" + jsonObject.get("url") + "/timeseries/download-file/" + zipfile + "' class='link2'>Download</a>\n" +
                        "\t\t\t\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t</div>\n";
            } else {
                message.setSubject("[EPOS - Products Gateway] Your timeseries request returned no data!");
                subject = "[EPOS - Products Gateway] Your customized timeseries request!";
                emailmessage = "\t\t\t\t\t<div class='movableContent'>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100%' colspan='3' align='center' style='padding-bottom:10px;padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t                  \t\t<h2 >EPOS - Products Gateway - Custom Download Request</h2>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='400' align='center'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                        "\t\t\t\t\t                  \t\t<p >Hi there,\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tUnfortunately, your customized timeseries request returned no data. \n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tPlease try again using another combination of parameters:</p>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200' align='center' style='padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<td bgcolor='#964296' align='center' style='border-radius:4px;' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t\t\t\t                  \t\t<a target='_blank' href='" + jsonObject.get("pgw_url") + "' class='link2'>Go to Products Gateway</a>\n" +
                        "\t\t\t\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t</div>\n";
            }



            message.setContent("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n" +
                    "\n" +
                    "<html xmlns='http://www.w3.org/1999/xhtml'>\n" +
                    "<head>\n" +
                    "\t<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n" +
                    "\t<meta name='viewport' content='width=device-width, initial-scale=1.0'/>\n" +
                    "\t<title>"+subject+"</title>\n" +
                    "\t<style type='text/css'>\n" +
                    "\n" +
                    "\t\t@media screen and (max-width: 600px) {\n" +
                    "\t\t    table[class='container'] {\n" +
                    "\t\t        width: 95% !important;\n" +
                    "\t\t    }\n" +
                    "\t\t}\n" +
                    "\t\t#outlook a {padding:0;}\n" +
                    "\t\tbody{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}\n" +
                    "\t\t.ExternalClass {width:100%;}\n" +
                    "\t\t.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}\n" +
                    "\t\t#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}\n" +
                    "\t\timg {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}\n" +
                    "\t\ta img {border:none;}\n" +
                    "\t\t.image_fix {display:block;}\n" +
                    "\t\tp {margin: 1em 0;}\n" +
                    "\t\th1, h2, h3, h4, h5, h6 {color: black !important;}\n" +
                    "\t\th1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}\n" +
                    "\t\th1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {\n" +
                    "\t\t\tcolor: red !important; \n" +
                    "\t\t }\n" +
                    "\t\th1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {\n" +
                    "\t\t\tcolor: purple !important; \n" +
                    "\t\t}\n" +
                    "\t\ttable td {border-collapse: collapse;}\n" +
                    "\t\ttable { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }\n" +
                    "\t\ta {color: #000;}\n" +
                    "\t\t@media only screen and (max-device-width: 480px) {\n" +
                    "\t\t\ta[href^='tel'], a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: none;\n" +
                    "\t\t\t\t\t\tcolor: black; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: none;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t\t.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: default;\n" +
                    "\t\t\t\t\t\tcolor: orange !important; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: auto;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {\n" +
                    "\t\t\ta[href^='tel'], a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: none;\n" +
                    "\t\t\t\t\t\tcolor: blue; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: none;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t\t.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: default;\n" +
                    "\t\t\t\t\t\tcolor: orange !important;\n" +
                    "\t\t\t\t\t\tpointer-events: auto;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-min-device-pixel-ratio: 2) {\n" +
                    "\t\t\t/* Put your iPhone 4g styles in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:.75){\n" +
                    "\t\t\t/* Put CSS for low density (ldpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:1){\n" +
                    "\t\t\t/* Put CSS for medium density (mdpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:1.5){\n" +
                    "\t\t\t/* Put CSS for high density (hdpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t/* end Android targeting */\n" +
                    "\t\th2{\n" +
                    "\t\t\tcolor:#181818;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:22px;\n" +
                    "\t\t\tline-height: 22px;\n" +
                    "\t\t\tfont-weight: normal;\n" +
                    "\t\t}\n" +
                    "\t\ta.link1{\n" +
                    "\n" +
                    "\t\t}\n" +
                    "\t\ta.link2{\n" +
                    "\t\t\tcolor:#fff;\n" +
                    "\t\t\ttext-decoration:none;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:16px;\n" +
                    "\t\t\tcolor:#fff;border-radius:4px;\n" +
                    "\t\t}\n" +
                    "\t\tp{\n" +
                    "\t\t\tcolor:#555;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:16px;\n" +
                    "\t\t\tline-height:160%;\n" +
                    "\t\t}\n" +
                    "\t</style>\n" +
                    "\n" +
                    "<script type='colorScheme' class='swatch active'>\n" +
                    "  {\n" +
                    "    'name':'Default',\n" +
                    "    'bgBody':'ffffff',\n" +
                    "    'link':'fff',\n" +
                    "    'color':'555555',\n" +
                    "    'bgItem':'ffffff',\n" +
                    "    'title':'181818'\n" +
                    "  }\n" +
                    "</script>\n" +
                    "\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "\t<table cellpadding='0' width='100%' cellspacing='0' border='0' id='backgroundTable' class='bgBody'>\n" +
                    "\t<tr>\n" +
                    "\t\t<td>\n" +
                    "\t<table cellpadding='0' width='620' class='container' align='center' cellspacing='0' border='0'>\n" +
                    "\t<tr>\n" +
                    "\t\t<td>\t\t\n" +
                    "\n" +
                    "\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t<tr>\n" +
                    "\t\t\t\t<td class='movableContentContainer bgItem'>\n" +
                    "\n" +
                    "\t\t\t\t\t<div class='movableContent' style=\"background-color: #964296\">\n" +
                    "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t\t\t\t\t<tr height='40'>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top' align='center'>\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentImageEditable'>\n" +
                    "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                    "\t\t\t\t\t                  \t\t<img src='" + jsonObject.get("pgw_url") + "/assets/img/EPOS_logo_WP10_whitestroke.png'   alt='Logo'  data-default='placeholder' />\n" +
                    "\t\t\t\t\t                \t</div>\n" +
                    "\t\t\t\t\t              \t</div>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr height='25'>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t</table>\n" +
                    "\t\t\t\t\t</div>\n" +
                    "\n" +
                    emailmessage +
                    "\n" +
                    "\n" +
                    "\t\t\t\t\t<div class='movableContent'>\n" +
                    "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='100%' colspan='2' style='padding-top:65px;'>\n" +
                    "\t\t\t\t\t\t\t\t\t<hr style='height:1px;border:none;color:#333;background-color:#ddd;' />\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='60%' height='70' valign='middle' style='padding-bottom:20px;'>\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                    "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                    "\t\t\t\t\t                  \t\t<span style='font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;'>Sent to " + jsonObject.get("email") + " by <a target='_blank' href='" + jsonObject.get("pgw_url") + "' class='link2' style='font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;'> EPOS - Products Gateway</a></span>\n" +
                    "\t\t\t\t\t                \t</div>\n" +
                    "\t\t\t\t\t              \t</div>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t</table>\n" +
                    "\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\n" +
                    "\t\t\t\t</td>\n" +
                    "\t\t\t</tr>\n" +
                    "\t\t</table>\n" +
                    "\n" +
                    "\t\t\n" +
                    "\t\t\n" +
                    "\n" +
                    "\t</td></tr></table>\n" +
                    "\t\n" +
                    "\t\t</td>\n" +
                    "\t</tr>\n" +
                    "\t</table>\n" +
                    "\t<!-- End of wrapper table -->\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>\n", "text/html; charset=utf-8");

            Transport.send(message);

            System.out.println("Email sent");

        } catch (Exception e) {
            System.out.println("An error occurred when sending email: " + e);
            throw new RuntimeException(e);
        }

        return 1;
    };




    /**
     * <b>Sends notification to user notifying him that customized download request from rectangle map selection based on data specified on Products Gateway is ready</b>
     * Returns email.
     *
     * @since 21/12/2017
     * Last modified: 04/04/2019
     * Author Andre Rodrigues
     */
    private final Callable<Integer> task2 = () -> {

        TimeseriesMultiple t = new TimeseriesMultiple();
        String zipfile = t.TimeSeriesMultipleMap(this.jsonstring);

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(this.jsonstring);
        JSONObject jsonObject = (JSONObject) obj;


        String smtpserver = "";
        Integer port = 0;
        String username = "";
        String password = "";
        String debug = "";

        smtpserver = DBConsts.GLASS_EMAIL_SMTP_SERVER;
        port = Integer.valueOf(DBConsts.GLASS_EMAIL_PORT);
        username = DBConsts.GLASS_EMAIL_ADDRESS;
        password = DBConsts.GLASS_EMAIL_PASSWORD;
        debug = DBConsts.GLASS_EMAIL_DEBUG;
        if(password.equals("") || username.equals("") || smtpserver.equals("")){
            System.out.println("> No email configuration loaded");
            return null;
        }

        try {

            Properties props =  System.getProperties();
            props.put("mail.smtp.host", smtpserver);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", port);
            props.put("mail.smtp.starttls.enable","true");
            props.put("mail.debug", debug);

            final String finalUsername = username;
            final String finalPassword = password;
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        @Override
                        protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                            return new javax.mail.PasswordAuthentication(finalUsername, finalPassword);
                        }
                    });

            System.out.println("Preparing email");

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse((String) jsonObject.get("email")));

            String subject;
            String emailmessage;
            File f = new File("/opt/EPOS/temp/"+zipfile);
            if (zipfile != null && !f.isDirectory() && f.exists()) {
                subject = "[EPOS - Products Gateway] Your customized timeseries request!";
                emailmessage = "\t\t\t\t\t<div class='movableContent'>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100%' colspan='3' align='center' style='padding-bottom:10px;padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t                  \t\t<h2 >EPOS - Products Gateway - Custom Download Request</h2>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='400' align='center'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                        "\t\t\t\t\t                  \t\t<p >Hi there,\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tYour customized timeseries request has been processed and is now ready for download. \n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tPlease click the button below to access your data:</p>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200' align='center' style='padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<td bgcolor='#964296' align='center' style='border-radius:4px;' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t\t\t\t                  \t\t<a target='_blank' href='" + jsonObject.get("url") + "/timeseries/download-file/" + zipfile + "' class='link2'>Download</a>\n" +
                        "\t\t\t\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t</div>\n";
            } else {
                subject = "[EPOS - Products Gateway] Your customized timeseries request!";
                emailmessage = "\t\t\t\t\t<div class='movableContent'>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100%' colspan='3' align='center' style='padding-bottom:10px;padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t                  \t\t<h2 >EPOS - Products Gateway - Custom Download Request</h2>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='400' align='center'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                        "\t\t\t\t\t                  \t\t<p >Hi there,\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tUnfortunately, your customized timeseries request returned no data. \n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tPlease try again using another combination of parameters:</p>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200' align='center' style='padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<td bgcolor='#964296' align='center' style='border-radius:4px;' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t\t\t\t                  \t\t<a target='_blank' href='" + jsonObject.get("pgw_url") + "' class='link2'>Go to Products Gateway</a>\n" +
                        "\t\t\t\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t</div>\n";
            }



            message.setSubject("[EPOS - Products Gateway] Your timeseries request is ready!");
            message.setContent("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n" +
                    "\n" +
                    "<html xmlns='http://www.w3.org/1999/xhtml'>\n" +
                    "<head>\n" +
                    "\t<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n" +
                    "\t<meta name='viewport' content='width=device-width, initial-scale=1.0'/>\n" +
                    "\t<title>"+subject+"</title>\n" +
                    "\t<style type='text/css'>\n" +
                    "\n" +
                    "\t\t@media screen and (max-width: 600px) {\n" +
                    "\t\t    table[class='container'] {\n" +
                    "\t\t        width: 95% !important;\n" +
                    "\t\t    }\n" +
                    "\t\t}\n" +
                    "\t\t#outlook a {padding:0;}\n" +
                    "\t\tbody{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}\n" +
                    "\t\t.ExternalClass {width:100%;}\n" +
                    "\t\t.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}\n" +
                    "\t\t#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}\n" +
                    "\t\timg {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}\n" +
                    "\t\ta img {border:none;}\n" +
                    "\t\t.image_fix {display:block;}\n" +
                    "\t\tp {margin: 1em 0;}\n" +
                    "\t\th1, h2, h3, h4, h5, h6 {color: black !important;}\n" +
                    "\t\th1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}\n" +
                    "\t\th1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {\n" +
                    "\t\t\tcolor: red !important; \n" +
                    "\t\t }\n" +
                    "\t\th1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {\n" +
                    "\t\t\tcolor: purple !important; \n" +
                    "\t\t}\n" +
                    "\t\ttable td {border-collapse: collapse;}\n" +
                    "\t\ttable { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }\n" +
                    "\t\ta {color: #000;}\n" +
                    "\t\t@media only screen and (max-device-width: 480px) {\n" +
                    "\t\t\ta[href^='tel'], a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: none;\n" +
                    "\t\t\t\t\t\tcolor: black; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: none;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t\t.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: default;\n" +
                    "\t\t\t\t\t\tcolor: orange !important; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: auto;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {\n" +
                    "\t\t\ta[href^='tel'], a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: none;\n" +
                    "\t\t\t\t\t\tcolor: blue; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: none;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t\t.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: default;\n" +
                    "\t\t\t\t\t\tcolor: orange !important;\n" +
                    "\t\t\t\t\t\tpointer-events: auto;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-min-device-pixel-ratio: 2) {\n" +
                    "\t\t\t/* Put your iPhone 4g styles in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:.75){\n" +
                    "\t\t\t/* Put CSS for low density (ldpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:1){\n" +
                    "\t\t\t/* Put CSS for medium density (mdpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:1.5){\n" +
                    "\t\t\t/* Put CSS for high density (hdpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t/* end Android targeting */\n" +
                    "\t\th2{\n" +
                    "\t\t\tcolor:#181818;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:22px;\n" +
                    "\t\t\tline-height: 22px;\n" +
                    "\t\t\tfont-weight: normal;\n" +
                    "\t\t}\n" +
                    "\t\ta.link1{\n" +
                    "\n" +
                    "\t\t}\n" +
                    "\t\ta.link2{\n" +
                    "\t\t\tcolor:#fff;\n" +
                    "\t\t\ttext-decoration:none;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:16px;\n" +
                    "\t\t\tcolor:#fff;border-radius:4px;\n" +
                    "\t\t}\n" +
                    "\t\tp{\n" +
                    "\t\t\tcolor:#555;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:16px;\n" +
                    "\t\t\tline-height:160%;\n" +
                    "\t\t}\n" +
                    "\t</style>\n" +
                    "\n" +
                    "<script type='colorScheme' class='swatch active'>\n" +
                    "  {\n" +
                    "    'name':'Default',\n" +
                    "    'bgBody':'ffffff',\n" +
                    "    'link':'fff',\n" +
                    "    'color':'555555',\n" +
                    "    'bgItem':'ffffff',\n" +
                    "    'title':'181818'\n" +
                    "  }\n" +
                    "</script>\n" +
                    "\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "\t<table cellpadding='0' width='100%' cellspacing='0' border='0' id='backgroundTable' class='bgBody'>\n" +
                    "\t<tr>\n" +
                    "\t\t<td>\n" +
                    "\t<table cellpadding='0' width='620' class='container' align='center' cellspacing='0' border='0'>\n" +
                    "\t<tr>\n" +
                    "\t\t<td>\t\t\n" +
                    "\n" +
                    "\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t<tr>\n" +
                    "\t\t\t\t<td class='movableContentContainer bgItem'>\n" +
                    "\n" +
                    "\t\t\t\t\t<div class='movableContent' style=\"background-color: #964296\">\n" +
                    "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t\t\t\t\t<tr height='40'>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top' align='center'>\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentImageEditable'>\n" +
                    "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                    "\t\t\t\t\t                  \t\t<img src='" + jsonObject.get("pgw_url") + "/assets/img/EPOS_logo_WP10_whitestroke.png'   alt='Logo'  data-default='placeholder' />\n" +
                    "\t\t\t\t\t                \t</div>\n" +
                    "\t\t\t\t\t              \t</div>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr height='25'>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t</table>\n" +
                    "\t\t\t\t\t</div>\n" +
                    "\n" +
                    emailmessage +
                    "\n" +
                    "\n" +
                    "\t\t\t\t\t<div class='movableContent'>\n" +
                    "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='100%' colspan='2' style='padding-top:65px;'>\n" +
                    "\t\t\t\t\t\t\t\t\t<hr style='height:1px;border:none;color:#333;background-color:#ddd;' />\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='60%' height='70' valign='middle' style='padding-bottom:20px;'>\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                    "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                    "\t\t\t\t\t                  \t\t<span style='font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;'>Sent to " + jsonObject.get("email") + " by <a target='_blank' href='" + jsonObject.get("pgw_url") + "' class='link2' style='font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;'> EPOS - Products Gateway</a></span>\n" +
                    "\t\t\t\t\t                \t</div>\n" +
                    "\t\t\t\t\t              \t</div>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t</table>\n" +
                    "\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\n" +
                    "\t\t\t\t</td>\n" +
                    "\t\t\t</tr>\n" +
                    "\t\t</table>\n" +
                    "\n" +
                    "\t\t\n" +
                    "\t\t\n" +
                    "\n" +
                    "\t</td></tr></table>\n" +
                    "\t\n" +
                    "\t\t</td>\n" +
                    "\t</tr>\n" +
                    "\t</table>\n" +
                    "\t<!-- End of wrapper table -->\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>\n", "text/html; charset=utf-8");

            Transport.send(message);

            System.out.println("Email sent");

        } catch (Exception e) {
            System.out.println("An error occurred when sending email: " + e);
            throw new RuntimeException(e);
        }

        return 1;
    };

    /**
     * <p><b>CREATE CUSTOMIZED DOWNLOAD REQUEST VELOCITIES FILE</b></p>
     *
     * Creates a customized download request based on data specified on Products Gateway
     *
     * <br />
     * <p>POST parameter:</p>
     * <ul>
     *     <li><b>data</b> - JSON string with the parameters needed to process the customized download request.</li>
     * </ul>
     *
     *
     * @param data JSON string with the parameters needed to process the customized download request.
     * @return Job status
     * @author José Manteigueiro
     * @since 13/10/2020
     */
    @Path("velocities/create-download-file")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCreateVelocitiesDownload(@FormParam("data") String data) {


        Logger.getLogger(Products.class.getName()).log(Level.INFO, "REQUEST: velocities/create-download-file - Started (" + data + ")");

        this.jsonstring = data;

        ExecutorService executor = Executors.newFixedThreadPool(2);
        executor.submit(velocitiesDownloadTask);

        executor.shutdown();
        return Response.status(Response.Status.ACCEPTED).build();

    }

    /**
     * <p><b>CREATE CUSTOMIZED DOWNLOAD REQUEST FILE MAP</b></p>
     *
     * Creates a customized download request based on data specified on Products Gateway from rectangle selection on map
     *
     * <br />
     * <p>POST parameter:</p>
     * <ul>
     *     <li><b>data</b> - JSON string with the parameters needed to process the customized download request.</li>
     * </ul>
     *
     *
     * @param data JSON string with the parameters needed to process the customized download request from rectangle selection on map.
     * @return Job status
     * @author José Manteigueiro
     * @since 13/10/2020
     */
    @Path("velocities/create-download-file/map")
    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCreateVelocitiesDownloadMap(@FormParam("data") String data) {


        Logger.getLogger(Products.class.getName()).log(Level.INFO, "REQUEST: velocities/create-download-file/map - Started \n" + data);

        this.jsonstring = data;

        ExecutorService executor = Executors.newFixedThreadPool(1);
        executor.submit(velocitiesMapDownloadTask);

        return Response.status(Response.Status.ACCEPTED).build();

    }


    /**
     * <p><b>DOWNLOAD CUSTOMIZED DOWNLOAD REQUEST VELOCITIES FILE</b></p>
     *
     * Downloads customized download request based on request previously made on Products Gateway
     *
     * <br />
     * <p>Parameter:</p>
     * <ul>
     *     <li><b>filename</b> - name of the file with the customized velocities to be downloaded. (this filename was sent by email to the person who asked for a customized download)</li>
     * </ul>
     *
     *
     * @param filename name of the file with the customized velocities to be downloaded
     * @return Job status
     * @author José Manteigueiro
     * @since 13/10/2020
     */
    @Path("velocities/download-file/{filename}")
    @GET
    @Produces(MediaType.APPLICATION_OCTET_STREAM)
    public Response getVelocitiesDownload(@PathParam("filename") String filename) {

        File f = new File("/opt/EPOS/temp/"+filename);

        if(f.exists() && !f.isDirectory())
        {
            return Response.ok(f).header("Content-Type", "application/zip").build();
        }
        else{
            Response.status(Response.Status.NO_CONTENT).build();
        }
        return null;
    }

    /**
     * <b>Sends notification to user notifying him that customized download request based on data specified on Products Gateway is ready</b>
     * Returns email.
     *
     * @since 13/10/2020
     * Last modified: 13/10/2020
     * Author José Manteigueiro
     */
    private final Callable<Integer> velocitiesDownloadTask = () -> {

        VelocitiesMultiple v = new VelocitiesMultiple();
        String zipfile = v.VelocitiesMultiple(this.jsonstring);

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(this.jsonstring);
        JSONObject jsonObject = (JSONObject) obj;


        String smtpserver = "";
        Integer port = 0;
        String username = "";
        String password = "";
        String debug = "";

        smtpserver = DBConsts.GLASS_EMAIL_SMTP_SERVER;
        port = Integer.valueOf(DBConsts.GLASS_EMAIL_PORT);
        username = DBConsts.GLASS_EMAIL_ADDRESS;
        password = DBConsts.GLASS_EMAIL_PASSWORD;
        debug = DBConsts.GLASS_EMAIL_DEBUG;
        if(password.equals("") || username.equals("") || smtpserver.equals("")){
            System.out.println("> No email configuration loaded");
            return null;
        }

        try {

            Properties props =  System.getProperties();
            props.put("mail.smtp.host", smtpserver);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", port);
            props.put("mail.smtp.starttls.enable","true");
            props.put("mail.debug", debug);

            final String finalUsername = username;
            final String finalPassword = password;
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        @Override
                        protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                            return new javax.mail.PasswordAuthentication(finalUsername, finalPassword);
                        }
                    });

            System.out.println("Preparing email");

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse((String) jsonObject.get("email")));

            String subject;
            String emailmessage;
            File f = new File("/opt/EPOS/temp/"+zipfile);
            if (zipfile != null && !f.isDirectory() && f.exists()) {
                message.setSubject("[EPOS - Products Gateway] Your velocities request is ready!");
                subject = "[EPOS - Products Gateway] Your customized velocities request!";
                emailmessage = "\t\t\t\t\t<div class='movableContent'>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100%' colspan='3' align='center' style='padding-bottom:10px;padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t                  \t\t<h2 >EPOS - Products Gateway - Custom Download Request</h2>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='400' align='center'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                        "\t\t\t\t\t                  \t\t<p >Hi there,\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tYour customized velocities request has been processed and is now ready for download. Be aware that this link will expire after some time.\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tPlease click the button below to access your data:</p>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200' align='center' style='padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<td bgcolor='#964296' align='center' style='border-radius:4px;' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t\t\t\t                  \t\t<a target='_blank' href='" + jsonObject.get("url") + "/velocities/download-file/" + zipfile + "' class='link2'>Download</a>\n" +
                        "\t\t\t\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t</div>\n";
            } else {
                message.setSubject("[EPOS - Products Gateway] Your velocities request returned no data!");
                subject = "[EPOS - Products Gateway] Your customized velocities request!";
                emailmessage = "\t\t\t\t\t<div class='movableContent'>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100%' colspan='3' align='center' style='padding-bottom:10px;padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t                  \t\t<h2 >EPOS - Products Gateway - Custom Download Request</h2>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='400' align='center'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                        "\t\t\t\t\t                  \t\t<p >Hi there,\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tUnfortunately, your customized velocities request returned no data. \n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tPlease try again using another combination of parameters:</p>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200' align='center' style='padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<td bgcolor='#964296' align='center' style='border-radius:4px;' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t\t\t\t                  \t\t<a target='_blank' href='" + jsonObject.get("pgw_url") + "' class='link2'>Go to Products Gateway</a>\n" +
                        "\t\t\t\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t</div>\n";
            }



            message.setContent("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n" +
                    "\n" +
                    "<html xmlns='http://www.w3.org/1999/xhtml'>\n" +
                    "<head>\n" +
                    "\t<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n" +
                    "\t<meta name='viewport' content='width=device-width, initial-scale=1.0'/>\n" +
                    "\t<title>"+subject+"</title>\n" +
                    "\t<style type='text/css'>\n" +
                    "\n" +
                    "\t\t@media screen and (max-width: 600px) {\n" +
                    "\t\t    table[class='container'] {\n" +
                    "\t\t        width: 95% !important;\n" +
                    "\t\t    }\n" +
                    "\t\t}\n" +
                    "\t\t#outlook a {padding:0;}\n" +
                    "\t\tbody{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}\n" +
                    "\t\t.ExternalClass {width:100%;}\n" +
                    "\t\t.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}\n" +
                    "\t\t#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}\n" +
                    "\t\timg {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}\n" +
                    "\t\ta img {border:none;}\n" +
                    "\t\t.image_fix {display:block;}\n" +
                    "\t\tp {margin: 1em 0;}\n" +
                    "\t\th1, h2, h3, h4, h5, h6 {color: black !important;}\n" +
                    "\t\th1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}\n" +
                    "\t\th1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {\n" +
                    "\t\t\tcolor: red !important; \n" +
                    "\t\t }\n" +
                    "\t\th1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {\n" +
                    "\t\t\tcolor: purple !important; \n" +
                    "\t\t}\n" +
                    "\t\ttable td {border-collapse: collapse;}\n" +
                    "\t\ttable { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }\n" +
                    "\t\ta {color: #000;}\n" +
                    "\t\t@media only screen and (max-device-width: 480px) {\n" +
                    "\t\t\ta[href^='tel'], a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: none;\n" +
                    "\t\t\t\t\t\tcolor: black; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: none;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t\t.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: default;\n" +
                    "\t\t\t\t\t\tcolor: orange !important; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: auto;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {\n" +
                    "\t\t\ta[href^='tel'], a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: none;\n" +
                    "\t\t\t\t\t\tcolor: blue; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: none;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t\t.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: default;\n" +
                    "\t\t\t\t\t\tcolor: orange !important;\n" +
                    "\t\t\t\t\t\tpointer-events: auto;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-min-device-pixel-ratio: 2) {\n" +
                    "\t\t\t/* Put your iPhone 4g styles in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:.75){\n" +
                    "\t\t\t/* Put CSS for low density (ldpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:1){\n" +
                    "\t\t\t/* Put CSS for medium density (mdpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:1.5){\n" +
                    "\t\t\t/* Put CSS for high density (hdpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t/* end Android targeting */\n" +
                    "\t\th2{\n" +
                    "\t\t\tcolor:#181818;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:22px;\n" +
                    "\t\t\tline-height: 22px;\n" +
                    "\t\t\tfont-weight: normal;\n" +
                    "\t\t}\n" +
                    "\t\ta.link1{\n" +
                    "\n" +
                    "\t\t}\n" +
                    "\t\ta.link2{\n" +
                    "\t\t\tcolor:#fff;\n" +
                    "\t\t\ttext-decoration:none;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:16px;\n" +
                    "\t\t\tcolor:#fff;border-radius:4px;\n" +
                    "\t\t}\n" +
                    "\t\tp{\n" +
                    "\t\t\tcolor:#555;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:16px;\n" +
                    "\t\t\tline-height:160%;\n" +
                    "\t\t}\n" +
                    "\t</style>\n" +
                    "\n" +
                    "<script type='colorScheme' class='swatch active'>\n" +
                    "  {\n" +
                    "    'name':'Default',\n" +
                    "    'bgBody':'ffffff',\n" +
                    "    'link':'fff',\n" +
                    "    'color':'555555',\n" +
                    "    'bgItem':'ffffff',\n" +
                    "    'title':'181818'\n" +
                    "  }\n" +
                    "</script>\n" +
                    "\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "\t<table cellpadding='0' width='100%' cellspacing='0' border='0' id='backgroundTable' class='bgBody'>\n" +
                    "\t<tr>\n" +
                    "\t\t<td>\n" +
                    "\t<table cellpadding='0' width='620' class='container' align='center' cellspacing='0' border='0'>\n" +
                    "\t<tr>\n" +
                    "\t\t<td>\t\t\n" +
                    "\n" +
                    "\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t<tr>\n" +
                    "\t\t\t\t<td class='movableContentContainer bgItem'>\n" +
                    "\n" +
                    "\t\t\t\t\t<div class='movableContent' style=\"background-color: #964296\">\n" +
                    "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t\t\t\t\t<tr height='40'>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top' align='center'>\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentImageEditable'>\n" +
                    "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                    "\t\t\t\t\t                  \t\t<img src='" + jsonObject.get("pgw_url") + "/assets/img/EPOS_logo_WP10_whitestroke.png'   alt='Logo'  data-default='placeholder' />\n" +
                    "\t\t\t\t\t                \t</div>\n" +
                    "\t\t\t\t\t              \t</div>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr height='25'>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t</table>\n" +
                    "\t\t\t\t\t</div>\n" +
                    "\n" +
                    emailmessage +
                    "\n" +
                    "\n" +
                    "\t\t\t\t\t<div class='movableContent'>\n" +
                    "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='100%' colspan='2' style='padding-top:65px;'>\n" +
                    "\t\t\t\t\t\t\t\t\t<hr style='height:1px;border:none;color:#333;background-color:#ddd;' />\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='60%' height='70' valign='middle' style='padding-bottom:20px;'>\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                    "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                    "\t\t\t\t\t                  \t\t<span style='font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;'>Sent to " + jsonObject.get("email") + " by <a target='_blank' href='" + jsonObject.get("pgw_url") + "' class='link2' style='font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;'> EPOS - Products Gateway</a></span>\n" +
                    "\t\t\t\t\t                \t</div>\n" +
                    "\t\t\t\t\t              \t</div>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t</table>\n" +
                    "\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\n" +
                    "\t\t\t\t</td>\n" +
                    "\t\t\t</tr>\n" +
                    "\t\t</table>\n" +
                    "\n" +
                    "\t\t\n" +
                    "\t\t\n" +
                    "\n" +
                    "\t</td></tr></table>\n" +
                    "\t\n" +
                    "\t\t</td>\n" +
                    "\t</tr>\n" +
                    "\t</table>\n" +
                    "\t<!-- End of wrapper table -->\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>\n", "text/html; charset=utf-8");

            Transport.send(message);

            System.out.println("Email sent");

        } catch (Exception e) {
            System.out.println("An error occurred when sending email: " + e);
            throw new RuntimeException(e);
        }

        return 1;
    };


    /**
     * <b>Sends notification to user notifying him that customized download request based on data specified on Products Gateway is ready</b>
     * Returns email.
     *
     * @since 06/01/2021
     * Last modified: 06/01/2021
     * Author José Manteigueiro
     */
    private Callable<Integer> velocitiesMapDownloadTask = () -> {

        VelocitiesMultiple v = new VelocitiesMultiple();
        String zipfile = v.VelocitiesMultiple(this.jsonstring);

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(this.jsonstring);
        JSONObject jsonObject = (JSONObject) obj;


        String smtpserver = "";
        Integer port = 0;
        String username = "";
        String password = "";
        String debug = "";

        smtpserver = DBConsts.GLASS_EMAIL_SMTP_SERVER;
        port = Integer.valueOf(DBConsts.GLASS_EMAIL_PORT);
        username = DBConsts.GLASS_EMAIL_ADDRESS;
        password = DBConsts.GLASS_EMAIL_PASSWORD;
        debug = DBConsts.GLASS_EMAIL_DEBUG;
        if(password.equals("") || username.equals("") || smtpserver.equals("")){
            System.out.println("> No email configuration loaded");
            return null;
        }

        try {

            Properties props =  System.getProperties();
            props.put("mail.smtp.host", smtpserver);
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.port", port);
            props.put("mail.smtp.starttls.enable","true");
            props.put("mail.debug", debug);

            final String finalUsername = username;
            final String finalPassword = password;
            Session session = Session.getInstance(props,
                    new javax.mail.Authenticator() {
                        @Override
                        protected javax.mail.PasswordAuthentication getPasswordAuthentication() {
                            return new javax.mail.PasswordAuthentication(finalUsername, finalPassword);
                        }
                    });

            System.out.println("Preparing email");

            Message message = new MimeMessage(session);
            message.setFrom(new InternetAddress(username));
            message.setRecipients(Message.RecipientType.TO, InternetAddress.parse((String) jsonObject.get("email")));

            String subject;
            String emailmessage;
            File f = new File("/opt/EPOS/temp/"+zipfile);
            if (zipfile != null && !f.isDirectory() && f.exists()) {
                message.setSubject("[EPOS - Products Gateway] Your velocities request is ready!");
                subject = "[EPOS - Products Gateway] Your customized velocities request!";
                emailmessage = "\t\t\t\t\t<div class='movableContent'>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100%' colspan='3' align='center' style='padding-bottom:10px;padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t                  \t\t<h2 >EPOS - Products Gateway - Custom Download Request</h2>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='400' align='center'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                        "\t\t\t\t\t                  \t\t<p >Hi there,\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tYour customized velocities request has been processed and is now ready for download. Be aware that this link will expire after some time.\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tPlease click the button below to access your data:</p>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200' align='center' style='padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<td bgcolor='#964296' align='center' style='border-radius:4px;' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t\t\t\t                  \t\t<a target='_blank' href='" + jsonObject.get("url") + "/velocities/download-file/" + zipfile + "' class='link2'>Download</a>\n" +
                        "\t\t\t\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t</div>\n";
            } else {
                message.setSubject("[EPOS - Products Gateway] Your velocities request returned no data!");
                subject = "[EPOS - Products Gateway] Your customized velocities request!";
                emailmessage = "\t\t\t\t\t<div class='movableContent'>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100%' colspan='3' align='center' style='padding-bottom:10px;padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t                  \t\t<h2 >EPOS - Products Gateway - Custom Download Request</h2>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='400' align='center'>\n" +
                        "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                        "\t\t\t\t\t                  \t\t<p >Hi there,\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t                  \t\t\t<br/>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tUnfortunately, your customized velocities request returned no data. \n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<br />\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\tPlease try again using another combination of parameters:</p>\n" +
                        "\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='100'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                        "\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200' align='center' style='padding-top:25px;'>\n" +
                        "\t\t\t\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t<tr>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t<td bgcolor='#964296' align='center' style='border-radius:4px;' width='200' height='50'>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                        "\t\t\t\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                        "\t\t\t\t\t\t\t\t                  \t\t<a target='_blank' href='" + jsonObject.get("pgw_url") + "' class='link2'>Go to Products Gateway</a>\n" +
                        "\t\t\t\t\t\t\t\t                \t</div>\n" +
                        "\t\t\t\t\t\t\t\t              \t</div>\n" +
                        "\t\t\t\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t\t\t\t</td>\n" +
                        "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                        "\t\t\t\t\t\t\t</tr>\n" +
                        "\t\t\t\t\t\t</table>\n" +
                        "\t\t\t\t\t</div>\n";
            }



            message.setContent("<!DOCTYPE html PUBLIC '-//W3C//DTD XHTML 1.0 Strict//EN' 'http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd'>\n" +
                    "\n" +
                    "<html xmlns='http://www.w3.org/1999/xhtml'>\n" +
                    "<head>\n" +
                    "\t<meta http-equiv='Content-Type' content='text/html; charset=utf-8' />\n" +
                    "\t<meta name='viewport' content='width=device-width, initial-scale=1.0'/>\n" +
                    "\t<title>"+subject+"</title>\n" +
                    "\t<style type='text/css'>\n" +
                    "\n" +
                    "\t\t@media screen and (max-width: 600px) {\n" +
                    "\t\t    table[class='container'] {\n" +
                    "\t\t        width: 95% !important;\n" +
                    "\t\t    }\n" +
                    "\t\t}\n" +
                    "\t\t#outlook a {padding:0;}\n" +
                    "\t\tbody{width:100% !important; -webkit-text-size-adjust:100%; -ms-text-size-adjust:100%; margin:0; padding:0;}\n" +
                    "\t\t.ExternalClass {width:100%;}\n" +
                    "\t\t.ExternalClass, .ExternalClass p, .ExternalClass span, .ExternalClass font, .ExternalClass td, .ExternalClass div {line-height: 100%;}\n" +
                    "\t\t#backgroundTable {margin:0; padding:0; width:100% !important; line-height: 100% !important;}\n" +
                    "\t\timg {outline:none; text-decoration:none; -ms-interpolation-mode: bicubic;}\n" +
                    "\t\ta img {border:none;}\n" +
                    "\t\t.image_fix {display:block;}\n" +
                    "\t\tp {margin: 1em 0;}\n" +
                    "\t\th1, h2, h3, h4, h5, h6 {color: black !important;}\n" +
                    "\t\th1 a, h2 a, h3 a, h4 a, h5 a, h6 a {color: blue !important;}\n" +
                    "\t\th1 a:active, h2 a:active,  h3 a:active, h4 a:active, h5 a:active, h6 a:active {\n" +
                    "\t\t\tcolor: red !important; \n" +
                    "\t\t }\n" +
                    "\t\th1 a:visited, h2 a:visited,  h3 a:visited, h4 a:visited, h5 a:visited, h6 a:visited {\n" +
                    "\t\t\tcolor: purple !important; \n" +
                    "\t\t}\n" +
                    "\t\ttable td {border-collapse: collapse;}\n" +
                    "\t\ttable { border-collapse:collapse; mso-table-lspace:0pt; mso-table-rspace:0pt; }\n" +
                    "\t\ta {color: #000;}\n" +
                    "\t\t@media only screen and (max-device-width: 480px) {\n" +
                    "\t\t\ta[href^='tel'], a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: none;\n" +
                    "\t\t\t\t\t\tcolor: black; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: none;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t\t.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: default;\n" +
                    "\t\t\t\t\t\tcolor: orange !important; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: auto;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (min-device-width: 768px) and (max-device-width: 1024px) {\n" +
                    "\t\t\ta[href^='tel'], a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: none;\n" +
                    "\t\t\t\t\t\tcolor: blue; /* or whatever your want */\n" +
                    "\t\t\t\t\t\tpointer-events: none;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t\t.mobile_link a[href^='tel'], .mobile_link a[href^='sms'] {\n" +
                    "\t\t\t\t\t\ttext-decoration: default;\n" +
                    "\t\t\t\t\t\tcolor: orange !important;\n" +
                    "\t\t\t\t\t\tpointer-events: auto;\n" +
                    "\t\t\t\t\t\tcursor: default;\n" +
                    "\t\t\t\t\t}\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-min-device-pixel-ratio: 2) {\n" +
                    "\t\t\t/* Put your iPhone 4g styles in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:.75){\n" +
                    "\t\t\t/* Put CSS for low density (ldpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:1){\n" +
                    "\t\t\t/* Put CSS for medium density (mdpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t@media only screen and (-webkit-device-pixel-ratio:1.5){\n" +
                    "\t\t\t/* Put CSS for high density (hdpi) Android layouts in here */\n" +
                    "\t\t}\n" +
                    "\t\t/* end Android targeting */\n" +
                    "\t\th2{\n" +
                    "\t\t\tcolor:#181818;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:22px;\n" +
                    "\t\t\tline-height: 22px;\n" +
                    "\t\t\tfont-weight: normal;\n" +
                    "\t\t}\n" +
                    "\t\ta.link1{\n" +
                    "\n" +
                    "\t\t}\n" +
                    "\t\ta.link2{\n" +
                    "\t\t\tcolor:#fff;\n" +
                    "\t\t\ttext-decoration:none;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:16px;\n" +
                    "\t\t\tcolor:#fff;border-radius:4px;\n" +
                    "\t\t}\n" +
                    "\t\tp{\n" +
                    "\t\t\tcolor:#555;\n" +
                    "\t\t\tfont-family:Helvetica, Arial, sans-serif;\n" +
                    "\t\t\tfont-size:16px;\n" +
                    "\t\t\tline-height:160%;\n" +
                    "\t\t}\n" +
                    "\t</style>\n" +
                    "\n" +
                    "<script type='colorScheme' class='swatch active'>\n" +
                    "  {\n" +
                    "    'name':'Default',\n" +
                    "    'bgBody':'ffffff',\n" +
                    "    'link':'fff',\n" +
                    "    'color':'555555',\n" +
                    "    'bgItem':'ffffff',\n" +
                    "    'title':'181818'\n" +
                    "  }\n" +
                    "</script>\n" +
                    "\n" +
                    "</head>\n" +
                    "<body>\n" +
                    "\t<table cellpadding='0' width='100%' cellspacing='0' border='0' id='backgroundTable' class='bgBody'>\n" +
                    "\t<tr>\n" +
                    "\t\t<td>\n" +
                    "\t<table cellpadding='0' width='620' class='container' align='center' cellspacing='0' border='0'>\n" +
                    "\t<tr>\n" +
                    "\t\t<td>\t\t\n" +
                    "\n" +
                    "\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t<tr>\n" +
                    "\t\t\t\t<td class='movableContentContainer bgItem'>\n" +
                    "\n" +
                    "\t\t\t\t\t<div class='movableContent' style=\"background-color: #964296\">\n" +
                    "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t\t\t\t\t<tr height='40'>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top' align='center'>\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentImageEditable'>\n" +
                    "\t\t\t\t\t                \t<div class='contentEditable' align='center' >\n" +
                    "\t\t\t\t\t                  \t\t<img src='" + jsonObject.get("pgw_url") + "/assets/img/EPOS_logo_WP10_whitestroke.png'   alt='Logo'  data-default='placeholder' />\n" +
                    "\t\t\t\t\t                \t</div>\n" +
                    "\t\t\t\t\t              \t</div>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200' valign='top'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr height='25'>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t\t<td width='200'>&nbsp;</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t</table>\n" +
                    "\t\t\t\t\t</div>\n" +
                    "\n" +
                    emailmessage +
                    "\n" +
                    "\n" +
                    "\t\t\t\t\t<div class='movableContent'>\n" +
                    "\t\t\t\t\t\t<table cellpadding='0' cellspacing='0' border='0' align='center' width='600' class='container'>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='100%' colspan='2' style='padding-top:65px;'>\n" +
                    "\t\t\t\t\t\t\t\t\t<hr style='height:1px;border:none;color:#333;background-color:#ddd;' />\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t\t<tr>\n" +
                    "\t\t\t\t\t\t\t\t<td width='60%' height='70' valign='middle' style='padding-bottom:20px;'>\n" +
                    "\t\t\t\t\t\t\t\t\t<div class='contentEditableContainer contentTextEditable'>\n" +
                    "\t\t\t\t\t                \t<div class='contentEditable' align='left' >\n" +
                    "\t\t\t\t\t                  \t\t<span style='font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;'>Sent to " + jsonObject.get("email") + " by <a target='_blank' href='" + jsonObject.get("pgw_url") + "' class='link2' style='font-size:13px;color:#181818;font-family:Helvetica, Arial, sans-serif;line-height:200%;'> EPOS - Products Gateway</a></span>\n" +
                    "\t\t\t\t\t                \t</div>\n" +
                    "\t\t\t\t\t              \t</div>\n" +
                    "\t\t\t\t\t\t\t\t</td>\n" +
                    "\t\t\t\t\t\t\t</tr>\n" +
                    "\t\t\t\t\t\t</table>\n" +
                    "\t\t\t\t\t</div>\n" +
                    "\n" +
                    "\n" +
                    "\t\t\t\t</td>\n" +
                    "\t\t\t</tr>\n" +
                    "\t\t</table>\n" +
                    "\n" +
                    "\t\t\n" +
                    "\t\t\n" +
                    "\n" +
                    "\t</td></tr></table>\n" +
                    "\t\n" +
                    "\t\t</td>\n" +
                    "\t</tr>\n" +
                    "\t</table>\n" +
                    "\t<!-- End of wrapper table -->\n" +
                    "\n" +
                    "</body>\n" +
                    "</html>\n", "text/html; charset=utf-8");

            Transport.send(message);

            System.out.println("Email sent");

        } catch (Exception e) {
            System.out.println("An error occurred when sending email: " + e);
            throw new RuntimeException(e);
        }

        return 1;
    };



}
