package Glass;

import Configuration.DBConsts;
import Configuration.SiteConfig;
import CustomClasses.StationNotFound;
import CustomClasses.Tuple2;
import EposTables.*;
import org.json.simple.JSONArray;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.Request;
import javax.ws.rs.core.*;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/*
* STATIONS
* This class is responsible for defining the URIs to query the EPOS database and
* returns the queries results.
*/
@Path("stations")
public class Stations {
    
    @EJB
    private
    SiteConfig siteConfig;
    
    // =========================================================================
    //  FIELDS
    // =========================================================================
    @Context
    private UriInfo context;
    private String myIP = "";                   // This String will store the ip address of the machine
    private String DBConnectionString = null;   // This String is used for the connection with the database
    
    // =========================================================================
    //  CONSTRUCTORS
    // =========================================================================
    /*
    * STATIONS
    * This is the default constructor for the Stations class.
    */
    public Stations(){
        this.siteConfig = lookupSiteConfigBean();
        myIP = siteConfig.getLocalIpValue();
        DBConnectionString = siteConfig.getDBConnectionString();
    }

    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }



    /**
     * <p>Replaced by v2/station</p>
     * <b>GET STATIONS</b> - Returns a <i>JSON</i> String with all the Stations.
     *
     * @deprecated
     * @return <i>JSON</i> String with all the Stations.
     * @author Pedro Pereira
     * @version 1.0
     * @since Unknown
     */
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStations() throws ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList<Station> res = dbc.getStations();
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }
    
    /**
    * UPDATE STATIONS DICTIONARY
     *
     * Send station ID to update cache for a single station of "all" for all stations.
     *
     * @return <i>String</i> Success or error message
     * @author K-M NGO
     * Last modified by Nuno Pedro
     * Request version 1.1
     * In 21/03/2023
    */
    @Path("stations-json-dictionary/{attribute}")
    @GET
    public String updateStationsDictionary(@PathParam("attribute") String attribute) {
        JsonCache mycache = new JsonCache();
        if (attribute.equals("all")) {
            mycache.updateAllCache();
        } else {
            try {
                mycache.updateCache(attribute, true);
            }catch (StationNotFound snf){
                snf.printStackTrace();
                return snf.getMessage();
            } catch (NumberFormatException | IOException e) {
                e.printStackTrace();
                return "The attribute should be an integer, long marker name or 'all'";
            }
        }
        return "Success on updating cache!";
    }

    /**
     * <b>GET STATION DataBase Id's </b> - Returns the station id(s) that match a specified marker or string.
     * marker/string must be <=9chars
     */

    /**
     * <p>Replaced by v2/station</p>
     * <b>GET STATIONS SHORT</b> - Returns a <i>JSON</i> String with all the Stations, but with less information.
     *
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>Date_from, Date_to</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>City, State, Country</li>
     *     <li>Agency (or Agencies)</li>
     *     <li>Network(s)</li>
     * </ul>
     *
     * @deprecated
     * @return <i>JSON</i> String with all the Stations, but with less information.
     * @author José Manteigueiro
     * @version 1.1
     * @since 17/10/2017
     */
    @Path("stations-short")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsShort(@Context Request request) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList<Station> res = dbc.getStationsShort();
        JSONArray obj = parser.getShortJSON(res);
        
        //Init the hashmap cache

        /* CROCKER 8/11 Why is this cache call here ?? I am removing it
           Also there are NO usages of this function ..anywhere !
        JsonCache mycache = new JsonCache();
        String statuscache = mycache.checkStatusCache();
        if(statuscache.equalsIgnoreCase("empty"))
        {    
            ArrayList<Integer> all_id_station  = dbc.getAllIdStations();
            mycache.InitCache(all_id_station);
        }
        */
        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/station</p>
     * <b>GET STATIONS XML</b> - Returns a <i>XML</i> file with all the Stations.
     * @deprecated
     * @return <i>XML</i> file with all the Stations.
     * @author Pedro Pereira
     * @version 1.0
     * @since Unknown
     */
    @Path("stations-xml")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsXML() {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = null;
        try {
            res = dbc.getStations();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String xml = parser.getFullXML(res);

        return xml;
    }

    /**
     * <p>Replaced by v2/station</p>
     * <b>GET STATIONS CSV</b> - Returns a <i>CSV</i> file with all the Stations.
     * @deprecated
     * @return <i>CSV</i> with all the Stations.
     * @throws IOException ...
     * @author Pedro Pereira
     * @version 1.0
     * @since Unknown
     */
    @Path("stations-csv")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCSV() throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = null;
        try {
            res = dbc.getStations();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String csv = parser.getFullCSV(res);
        
        File file = new File("stations.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/station</p>
     * <p><b>GET STATIONS SHORT CSV</b> - Returns a <i>CSV</i> file with all the Stations, but with less information.</p>
     *
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>Date_from, Date_to</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>City, State, Country</li>
     *     <li>Agency (or Agencies)</li>
     *     <li>Network(s)</li>
     * </ul>
     * @deprecated
     * @return <i>CSV</i> file with all the Stations, with less information.
     * @throws IOException ...
     * @author José Manteigueiro
     * @version 1.1
     * @since 17/10/2017
     */
    @Path("stations-short-csv")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsShortCSV() throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsShort();
        String csv = parser.getStationsShortCSV(res);

        File file = new File("stations_short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/marker</p>
     * <p><b>GET STATIONS MARKER</b> - Returns a <i>JSON</i> String with all the Stations that match the given Marker.</p>
     *
     * @param marker String - Marker of the <b>Station</b> e.g. NICE
     * @deprecated
     * @return <i>JSON</i> String with the Stations that match the given Marker.
     * @author Pedro Pereira
     * @version 1.0
     * @since Unknown
     */

    @Path("marker/{marker}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsMarker(@PathParam("marker") String marker) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = null;
        try {
            res = dbc.getStationMarker(marker);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/marker</p>
     * <p><b>GET STATIONS MARKER SHORT</b> - Returns a <i>JSON</i> String with all the Stations that contain a given Marker, but with less information.</p>
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param marker String - Marker of the <b>Station</b>
     * @deprecated
     * @return <i>JSON</i> String with all matching Stations, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     */
    @Path("marker-short/{marker}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsMarkerShort(@PathParam("marker") String marker) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = null;
        try {
            res = dbc.getStationMarker(marker);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        JSONArray obj = parser.getShortJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/marker</p>
     * <p><b>GET STATIONS MARKER XML</b> - Returns a <i>XML</i> file with all the Stations that contain a given Marker.</p>
     * @param marker String - Marker of the <b>Station</b>
     * @return <i>XML</i> file with all matching Stations.
     * @deprecated
     * @author Unknown
     * @version 1.0
     * @since Unknown
     */
    @Path("marker-xml/{marker}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsMarkerXML(@PathParam("marker") String marker) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = null;
        try {
            res = dbc.getStationMarker(marker);
        } catch (ParseException e) {
            e.printStackTrace();
        }

        return parser.getFullXML(res);
    }

    /**
     * <p>Replaced by v2/marker</p>
     * <p><b>GET STATIONS MARKER CSV</b> - Returns a <i>CSV</i> file with all the Stations that contain a given Marker.</p>
     * @param marker String - Marker of the <b>Station</b>
     * @return <i>CSV</i> file with all matching Stations.
     * @deprecated
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     */
    @Path("marker-csv/{marker}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsMarkerCSV(@PathParam("marker") String marker) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = null;
        try {
            res = dbc.getStationMarker(marker);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String csv = parser.getFullCSV(res);
        
        File file = new File("marker.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/marker</p>
     * <p><b>GET STATIONS MARKER SHORT CSV</b> - Returns a <i>CSV</i> file with all the Stations that contain a given Marker, but with less information.</p>
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param marker String - Marker of the <b>Station</b>
     * @return <i>CSV</i> file with all matching Stations, with less information.
     * @deprecated
     * @author Unknown
     * @version 1.0
     * @since Unknown
     */
    @Path("marker-short-csv/{marker}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsMarkerShortCSV(@PathParam("marker") String marker) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = null;
        try {
            res = dbc.getStationMarker(marker);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        String csv = parser.getShortCSV(res);
        
        File file = new File("marker-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/site</p>
     * <p><b>GET STATIONS SITE</b> - Returns a <i>JSON</i> String with all the Stations that contain a given Site.</p>
     *
     * @param site String - Site of the <b>Station</b>
     * @return <i>JSON</i> String with all matching Stations.
     * @deprecated
     * @author José Manteigueiro
     * @version 1.1
     * @since 16/07/2017
     */
    @Path("site/{site}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsSite(@PathParam("site") String site) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationSite(site);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/site</p>
     * <p><b>GET STATIONS SITE SHORT</b> - Returns a <i>JSON</i> String with all the Stations that contain a given Site, but with less information.</p>
     *
     * @param site String - Site of the <b>Station</b>
     * @deprecated
     * @return <i>JSON</i> String with all matching Stations, with less information.
     * @author José Manteigueiro
     * @version 1.1
     * @since 16/07/2017
     */
    @Path("site-short/{site}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsSiteShort(@PathParam("site") String site) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationSite(site);
        JSONArray obj = parser.getShortJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/site</p>
     * <p><b>GET STATIONS SITE XML</b> - Returns a <i>XML</i> file with all the Stations that contain a given Site.</p>
     *
     * @param site String - Site of the <b>Station</b>
     * @deprecated
     * @return <i>XML</i> file with all matching Stations.
     * @author José Manteigueiro
     * @version 1.1
     * @since 16/07/2017
     */
    @Path("site-xml/{site}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsSiteXML(@PathParam("site") String site) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationSite(site);

        return parser.getFullXML(res);
    }

    /**
     * <p>Replaced by v2/site</p>
     * <p><b>GET STATIONS SITE CSV</b> - Returns a <i>CSV</i> file with all the Stations that contain a given Site.</p>
     *
     * @param site String - Site of the <b>Station</b>
     * @return <i>CSV</i> file with all matching Stations.
     * @deprecated
     * @throws IOException ...
     * @author José Manteigueiro
     * @version 1.1
     * @since 16/07/2017
     */
    @Path("site-csv/{site}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsSiteCSV(@PathParam("site") String site) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationSite(site);
        String csv = parser.getFullCSV(res);
        
        File file = new File("site.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/site</p>
     * <p><b>GET STATIONS SITE SHORT CSV</b> - Returns a <i>CSV</i> file with all the Stations that contain a given Site, but with less information.</p>
     *
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param site String - Site of the <b>Station</b>
     * @return <i>CSV</i> file with all matching Stations, with less information.
     * @throws IOException ...
     * @deprecated
     * @author José Manteigueiro
     * @version 1.1
     * @since 16/07/2017
     */
    @Path("site-short-csv/{site}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsSiteShortCSV(@PathParam("site") String site) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationSite(site);
        String csv = parser.getShortCSV(res);
        
        File file = new File("site-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES</b> - Returns a <i>JSON</i> String with all the Stations within an interval of coordinates.</p>
     *
     * <p><b>minLat</b> - minimum Latitude value</p>
     * <p><b>maxLat</b> - maximum Latitude value</p>
     * <p><b>minLon</b> - minimum Longitude value</p>
     * <p><b>minLat</b> - maximum Longitude value</p>
     *
     * @param minLat e.g. 42.9364
     * @param maxLat e.g. 46.7472
     * @param minLon e.g. -4.4675
     * @param maxLon e.g. 6.3667
     * @return <i>JSON</i> String with all the Stations within an interval of coordinates.
     * @author Unknown
     * @deprecated
     * @version 1.0
     * @since Unknown
     */
    @Path("coordinates/{minLat}&{maxLat}&{minLon}&{maxLon}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCoordinates(@PathParam("minLat") float minLat,
                                         @PathParam("maxLat") float maxLat, @PathParam("minLon") float minLon,
                                         @PathParam("maxLon") float maxLon) throws ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationCoordinates(minLat, maxLat, minLon, maxLon);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES SHORT</b> - Returns a <i>JSON</i> String with all the Stations within an interval of coordinates, but with less information.</p>
     *
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * <p><b>minLat</b> - minimum Latitude value</p>
     * <p><b>maxLat</b> - maximum Latitude value</p>
     * <p><b>minLon</b> - minimum Longitude value</p>
     * <p><b>minLat</b> - maximum Longitude value</p>
     *
     * @param minLat e.g. 42.9364
     * @param maxLat e.g. 46.7472
     * @param minLon e.g. -4.4675
     * @param maxLon e.g. 6.3667
     * @deprecated
     * @return <i>JSON</i> String with all the Stations within an interval of coordinates, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     */
    @Path("coordinates-short/{minLat}&{maxLat}&{minLon}&{maxLon}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCoordinatesShort(@PathParam("minLat") float minLat,
                                              @PathParam("maxLat") float maxLat, @PathParam("minLon") float minLon,
                                              @PathParam("maxLon") float maxLon) throws ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationCoordinates(minLat, maxLat, minLon, maxLon);
        JSONArray obj = parser.getShortJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES XML</b> - Returns a <i>XML</i> file with all the Stations within an interval of coordinates.</p>
     *
     * <p><b>minLat</b> - minimum Latitude value</p>
     * <p><b>maxLat</b> - maximum Latitude value</p>
     * <p><b>minLon</b> - minimum Longitude value</p>
     * <p><b>minLat</b> - maximum Longitude value</p>
     *
     * @param minLat e.g. 42.9364
     * @param maxLat e.g. 46.7472
     * @param minLon e.g. -4.4675
     * @param maxLon e.g. 6.3667
     * @return <i>XML</i> file with all the Stations within an interval of coordinates.
     * @deprecated
     * @author Unknown
     * @version 1.0
     * @since Unknown
     */
    @Path("coordinates-xml/{minLat}&{maxLat}&{minLon}&{maxLon}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsCoordinatesXML(@PathParam("minLat") float minLat,
                                            @PathParam("maxLat") float maxLat, @PathParam("minLon") float minLon,
                                            @PathParam("maxLon") float maxLon) throws ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationCoordinates(minLat, maxLat, minLon, maxLon);

        return parser.getFullXML(res);
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES CSV</b> - Returns a <i>CSV</i> file with all the Stations within an interval of coordinates.</p>
     *
     * <p><b>minLat</b> - minimum Latitude value</p>
     * <p><b>maxLat</b> - maximum Latitude value</p>
     * <p><b>minLon</b> - minimum Longitude value</p>
     * <p><b>minLat</b> - maximum Longitude value</p>
     *
     * @param minLat e.g. 42.9364
     * @param maxLat e.g. 46.7472
     * @param minLon e.g. -4.4675
     * @param maxLon e.g. 6.3667
     * @return <i>CSV</i> file with all the Stations within an interval of coordinates.
     * @throws IOException
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("coordinates-csv/{minLat}&{maxLat}&{minLon}&{maxLon}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCoordinatesCSV(@PathParam("minLat") float minLat,
                                              @PathParam("maxLat") float maxLat, @PathParam("minLon") float minLon,
                                              @PathParam("maxLon") float maxLon) throws IOException, ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationCoordinates(minLat, maxLat, minLon, maxLon);
        String csv = parser.getFullCSV(res);
        
        File file = new File("coordinates.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES SHORT CSV</b> - Returns a <i>CSV</i> file with all the Stations within an interval of coordinates, but with less information.</p>
     *
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * <p><b>minLat</b> - minimum Latitude value</p>
     * <p><b>maxLat</b> - maximum Latitude value</p>
     * <p><b>minLon</b> - minimum Longitude value</p>
     * <p><b>minLat</b> - maximum Longitude value</p>
     *
     * @param minLat e.g. 42.9364
     * @param maxLat e.g. 46.7472
     * @param minLon e.g. -4.4675
     * @param maxLon e.g. 6.3667
     * @return <i>CSV</i> file with all the Stations within an interval of coordinates, with less information.
     * @throws IOException
     * @author Unknown
     * @deprecated
     * @version 1.0
     * @since Unknown
     */
    @Path("coordinates-short-csv/{minLat}&{maxLat}&{minLon}&{maxLon}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCoordinatesShortCSV(@PathParam("minLat") float minLat,
                                                   @PathParam("maxLat") float maxLat, @PathParam("minLon") float minLon,
                                                   @PathParam("maxLon") float maxLon) throws IOException, ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationCoordinates(minLat, maxLat, minLon, maxLon);
        String csv = parser.getShortCSV(res);
        
        File file = new File("coordinates-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES CIRCLE</b> - Returns a <i>JSON</i> String with all the Stations within a circle of a specified radius.</p>
     * <p>The center of the circle is the specified coordinates.</p>
     *
     * @param lat Float - Latitude value of the center of the circle, e.g. 43.214
     * @param lon Float - Longitude value of the center of the circle, e.g. -1.9
     * @param radius Float - Radius value of the circle, in km, e.g. 5
     * @return <i>JSON</i> String with all the Stations within the circle.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("coordinates-circle/{lat}&{lon}&{radius}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCoordinatesCircle(@PathParam("lat") float lat,
                                               @PathParam("lon") float lon, @PathParam("radius") float radius) throws ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationCoordinates(lat, lon, radius);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES CIRCLE SHORT</b> - Returns a <i>JSON</i> String with all the Stations within a circle of a specified radius, but with less information.</p>
     * <p>The center of the circle is the specified coordinates.</p>
     *
     * @param lat Float - Latitude value of the center of the circle, e.g. 43.214
     * @param lon Float - Longitude value of the center of the circle, e.g. -1.9
     * @param radius Float - Radius value of the circle, in km, e.g. 5
     * @return <i>JSON</i> String with all the Stations within the circle, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("coordinates-circle-short/{lat}&{lon}&{radius}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCoordinatesCircleShort(@PathParam("lat") float lat,
                                                    @PathParam("lon") float lon, @PathParam("radius") float radius) throws ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationCoordinates(lat, lon, radius);
        JSONArray obj = parser.getShortJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES CIRCLE XML</b> - Returns a <i>XML</i> file with all the Stations within a circle of a specified radius.</p>
     * <p>The center of the circle is the specified coordinates.</p>
     *
     * @param lat Float - Latitude value of the center of the circle, e.g. 43.214
     * @param lon Float - Longitude value of the center of the circle, e.g. -1.9
     * @param radius Float - Radius value of the circle, in km, e.g. 5
     * @return <i>XML</i> file with all the Stations within the circle.
     * @author Unknown
     * @version 1.0
     * @deprecated
     * @since Unknown
     */
    @Path("coordinates-circle-xml/{lat}&{lon}&{radius}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsCoordinatesCircleXML(@PathParam("lat") float lat,
                                                  @PathParam("lon") float lon, @PathParam("radius") float radius) throws ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationCoordinates(lat, lon, radius);

        return parser.getFullXML(res);
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES CIRCLE CSV</b> - Returns a <i>CSV</i> file with all the Stations within a circle of a specified radius.</p>
     * <p>The center of the circle is the specified coordinates.</p>
     *
     * @param lat Float - Latitude value of the center of the circle, e.g. 43.214
     * @param lon Float - Longitude value of the center of the circle, e.g. -1.9
     * @param radius Float - Radius value of the circle, in km, e.g. 5
     * @return <i>CSV</i> file with all the Stations within the circle.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("coordinates-circle-csv/{lat}&{lon}&{radius}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCoordinatesCircleCSV(@PathParam("lat") float lat,
                                                    @PathParam("lon") float lon, @PathParam("radius") float radius) throws IOException, ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationCoordinates(lat, lon, radius);
        String csv = parser.getFullCSV(res);
        
        File file = new File("coordinates.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES CIRCLE SHORT CSV</b> - Returns a <i>CSV</i> file with all the Stations within a circle of a specified radius, but with less information.</p>
     * <p>The center of the circle is the specified coordinates.</p>
     *
     * @param lat Float - Latitude value of the center of the circle, e.g. 43.214
     * @param lon Float - Longitude value of the center of the circle, e.g. -1.9
     * @param radius Float - Radius value of the circle, in km, e.g. 5
     * @return <i>CSV</i> file with all the Stations within the circle, with less information.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("coordinates-circle-short-csv/{lat}&{lon}&{radius}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCoordinatesCircleShortCSV(@PathParam("lat") float lat,
                                                         @PathParam("lon") float lon, @PathParam("radius") float radius) throws IOException, ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationCoordinates(lat, lon, radius);
        String csv = parser.getShortCSV(res);
        
        File file = new File("coordinates-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS COORDINATES POLYGON</b> - Returns a <i>JSON</i> String with all the Stations within a specified polygon.</p>
     *
     *
     * <p></p><a href="../../../images/coordinates_polygon_order.jpg" target="_blank">Polygon Order</a></p>
     * @param polygonString <p>String - with the polygon definition following the structure (must be this exact structure):</p>
     *                      <p>PointA_lat:PointA_lon&PointB_lat:PointB_lon&PointC_lat:PointC_lon&PointD_lat:PointD_lon&PointE_lat:PointE_lon</p>
     *
     * @return <i>JSON</i> String with all the Stations within the specified polygon.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("coordinates-polygon/{polygon}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCoordinatesPolygon(@PathParam("polygon") String polygonString) throws ParseException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        
        // Save polygon coordinates
        ArrayList<Coordinates> polygon = new ArrayList();
        polygon = Parsers.parsePolygon(polygonString);
        
        // Turn result to JSON array
        ArrayList res = dbc.getStationCoordinates(polygon);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/coordinates</p>
     * <p><b>GET STATIONS DATE</b> - Returns a <i>JSON</i> String with all the stations within an interval of dates.
     * <p>Dates are in format <b>YYYY/MM/DD</b>, e.g. 2015/03/12</p>
     *
     * <p>Only stations that <b>were active</b> between the dates will appear.</p>
     * <p>If date_to is <b>today's</b> date, active stations will appear.</p>
     *
     * @param date_from YYYY/MM/DD
     * @param date_to YYYY/MM/DD
     * @return <i>JSON</i> String with all the stations within an interval of dates.
     * @author José Manteigueiro
     * @version 1.1
     * @since 02/10/2017
     * @deprecated
     */
    @Path("date/{date_from}&{date_to}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsDate(@PathParam("date_from") String date_from,
                                  @PathParam("date_to") String date_to) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationDate(date_from, date_to);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/date</p>
     * <p><b>GET STATIONS DATE SHORT</b> - Returns a <i>JSON</i> String with all the stations within an interval of dates, but with less information.
     *
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * <p>Dates are in format <b>YYYY/MM/DD</b>, e.g. 2015/03/12</p>
     *
     * <p>Only stations that <b>were active</b> between the dates will appear.</p>
     * <p>If date_to is <b>today's</b> date, active stations will appear.</p>
     *
     * @param date_from YYYY/MM/DD
     * @param date_to YYYY/MM/DD
     * @return <i>JSON</i> String with all the stations within an interval of dates, with less information.
     * @author José Manteigueiro
     * @version 1.1
     * @since 02/10/2017
     * @deprecated
     */
    @Path("date-short/{date_from}&{date_to}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsDateShort(@PathParam("date_from") String date_from,
                                       @PathParam("date_to") String date_to) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationDate(date_from, date_to);
        JSONArray obj = parser.getShortJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/date</p>
     * <p><b>GET STATIONS DATE XML</b> - Returns a <i>XML</i> file with all the stations within an interval of dates.
     *
     * <p>Dates are in format <b>YYYY/MM/DD</b>, e.g. 2015/03/12</p>
     *
     * <p>Only stations that <b>were active</b> between the dates will appear.</p>
     * <p>If date_to is <b>today's</b> date, active stations will appear.</p>
     *
     * @param date_from YYYY/MM/DD
     * @param date_to YYYY/MM/DD
     * @return <i>XML</i> file with all the stations within an interval of dates.
     * @author José Manteigueiro
     * @version 1.1
     * @since 02/10/2017
     * @deprecated
     */
    @Path("date-xml/{date_from}&{date_to}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsDateXML(@PathParam("date_from") String date_from,
                                     @PathParam("date_to") String date_to) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationDate(date_from, date_to);

        return parser.getFullXML(res);
    }

    /**
     * <p>Replaced by v2/date</p>
     * <p><b>GET STATIONS DATE CSV</b> - Returns a <i>CSV</i> String with all the stations within an interval of dates.
     *
     * <p>Dates are in format <b>YYYY/MM/DD</b>, e.g. 2015/03/12</p>
     *
     * <p>Only stations that <b>were active</b> between the dates will appear.</p>
     * <p>If date_to is <b>today's</b> date, active stations will appear.</p>
     *
     * @param date_from YYYY/MM/DD
     * @param date_to YYYY/MM/DD
     * @return <i>CSV</i> file with all the stations within an interval of dates.
     * @author José Manteigueiro
     * @version 1.1
     * @since 02/10/2017
     * @deprecated
     */
    @Path("date-csv/{date_from}&{date_to}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsDateCSV(@PathParam("date_from") String date_from,
                                       @PathParam("date_to") String date_to) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationDate(date_from, date_to);
        String csv = parser.getFullCSV(res);
        
        File file = new File("date.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/date</p>
     * <p><b>GET STATIONS DATE SHORT CSV</b> - Returns a <i>CSV</i> file with all the stations within an interval of dates, but with less information.
     *
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * <p>Dates are in format <b>YYYY/MM/DD</b>, e.g. 2015/03/12</p>
     *
     * <p>Only stations that <b>were active</b> between the dates will appear.</p>
     * <p>If date_to is <b>today's</b> date, active stations will appear.</p>
     *
     * @param date_from YYYY/MM/DD
     * @param date_to YYYY/MM/DD
     * @return <i>CSV</i> file with all the stations within an interval of dates, with less information.
     * @author José Manteigueiro
     * @version 1.1
     * @deprecated
     * @since 02/10/2017
     */
    @Path("date-short-csv/{date_from}&{date_to}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsDateShortCSV(@PathParam("date_from") String date_from,
                                            @PathParam("date_to") String date_to) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationDate(date_from, date_to);
        String csv = parser.getShortCSV(res);
        
        File file = new File("date-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/network</p>
     * <p><b>GET STATIONS NETWORK</b> - Returns a <i>JSON</i> String with all the stations on the specified network.
     *
     * @param network String - network to which the Stations belong
     * @return <i>JSON</i> String with all the stations on the specified network.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("network/{network}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsNetwork(@PathParam("network") String network) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsNetwork(network);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/network</p>
     * <p><b>GET STATIONS NETWORK SHORT</b> - Returns a <i>JSON</i> String with all the stations on the specified network, but with less information.
     *
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param network String - network to which the Stations belong
     * @return <i>JSON</i> String with all the stations on the specified network, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("network-short/{network}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsNetworkShort(@PathParam("network") String network) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsNetwork(network);
        JSONArray obj = parser.getShortJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/network</p>
     * <p><b>GET STATIONS NETWORK XML</b> - Returns a <i>XML</i> file with all the stations on the specified network.
     *
     * @param network String - network to which the Stations belong
     * @return <i>XML</i> file with all the stations on the specified network.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("network-xml/{network}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsNetworkXML(@PathParam("network") String network) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationsNetwork(network);

        return parser.getFullXML(res);
    }

    /**
     * <p>Replaced by v2/network</p>
     * <p><b>GET STATIONS NETWORK CSV</b> - Returns a <i>CSV</i> file with all the stations on the specified network.
     *
     * @param network String - network to which the Stations belong
     * @return <i>CSV</i> file with all the stations on the specified network.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("network-csv/{network}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsNetworkCSV(@PathParam("network") String network) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsNetwork(network);
        String csv = parser.getFullCSV(res);
        
        File file = new File("network.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/network</p>
     * <p><b>GET STATIONS NETWORK SHORT CSV</b> - Returns a <i>CSV</i> file with all the stations on the specified network, but with less information.
     *
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param network String - network to which the Stations belong
     * @return <i>CSV</i> file with all the stations on the specified network, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("network-short-csv/{network}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsNetworkShortCSV(@PathParam("network") String network) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsNetwork(network);
        String csv = parser.getShortCSV(res);
        
        File file = new File("network-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/agency</p>
     * <p><b>GET STATIONS AGENCY</b> - Returns a <i>JSON</i> String with all the stations on the specified agency.
     *
     * @param agency String - agency to which the Stations belong
     * @return <i>JSON</i> String with all the stations on the specified agency.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("agency/{agency}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsAgency(@PathParam("agency") String agency) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList<Station> res = dbc.getStationsAgency(agency);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/agency</p>
     * <p><b>GET STATIONS AGENCY SHORT</b> - Returns a <i>JSON</i> String with all the stations on the specified agency, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param agency String - agency to which the Stations belong
     * @return <i>JSON</i> String with all the stations on the specified agency, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("agency-short/{agency}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsAgencyShort(@PathParam("agency") String agency) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList<Station> res = dbc.getStationsAgency(agency);
        JSONArray obj = parser.getShortJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/agency</p>
     *
     * @param agency
     * @return <i>XML</i> file with all the stations on the specified agency.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("agency-xml/{agency}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsAgencyXML(@PathParam("agency") String agency) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList<Station> res = dbc.getStationsAgency(agency);

        return parser.getFullXML(res);
    }

    /**
     * <p>Replaced by v2/agency</p>
     * <p><b>GET STATIONS AGENCY CSV</b> - Returns a <i>CSV</i> file with all the stations on the specified agency.
     *
     * @param agency String - agency to which the Stations belong
     * @return <i>CSV</i> file with all the stations on the specified agency.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("agency-csv/{agency}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsAgencyCSV(@PathParam("agency") String agency) throws IOException {

        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        ArrayList<Station> res = dbc.getStationsAgency(agency);

        // Turn result to CSV
        String csv = parser.getFullCSV(res);

        String fileName = agency+".csv";
        File file = new File(fileName);
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response .ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
                        .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"" ) //optional
                        .build();
    }

    /**
     * <p>Replaced by v2/agency</p>
     * <p><b>GET STATIONS AGENCY SHORT CSV</b> - Returns a <i>CSV</i> file with all the stations on the specified agency, but with less information.
     *
     * @param agency String - agency to which the Stations belong
     * @return <i>CSV</i> file with all the stations on the specified agency, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("agency-short-csv/{agency}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsAgencyShortCSV(@PathParam("agency") String agency) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList<Station> res = dbc.getStationsAgency(agency);
        String csv = parser.getShortCSV(res);

        String fileName = agency+"-short.csv";
        File file = new File(fileName);
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response .ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
                        .header("Content-Disposition", "attachment; filename=\"" + fileName + "\"" ) //optional
                        .build();
    }

    /**
     * <p>Replaced by v2/type</p>
     * <p><b>GET STATIONS TYPE</b> - Returns a <i>JSON</i> String with all the stations of the specified type.
     *
     * The 3 types of the stations are:
     * <ul>
     *     <li>GPS/GNSS Continuous</li>
     *     <li>GNSS</li>
     *     <li>GNSS Continuous</li>
     * </ul>
     *
     * @param type String - type of the Stations (e.g. GNSS Continuous)
     * @return <i>JSON</i> String with all the Stations of the specified type.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("type/{type}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsType(@PathParam("type") String type) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsType(type);
        JSONArray obj = parser.getFullJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }

    /**
     * <p>Replaced by v2/type</p>
     * <p><b>GET STATIONS TYPE SHORT</b> - Returns a <i>JSON</i> String with all the stations of the specified type, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>City, State, Country</li>
     *     <li>Agency(ies)</li>
     *     <li>Network(s)</li>
     * </ul>
     *
     * The 3 types of the stations are:
     * <ul>
     *     <li>GPS/GNSS Continuous</li>
     *     <li>GNSS</li>
     *     <li>GNSS Continuous</li>
     * </ul>
     *
     * @param type String - type of the Stations (e.g. GNSS Continuous)
     * @return <i>JSON</i> String with all the Stations of the specified type, with less information.
     * @author José Manteigueiro
     * @version 1.1
     * @since 19/10/2017
     * @deprecated
     */
    @Path("type-short/{type}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsTypeShort(@PathParam("type") String type) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsTypeShort(type);
        JSONArray obj = parser.getShortJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }

    /**
     * <p>Replaced by v2/type</p>
     * <p><b>GET STATIONS TYPE XML</b> - Returns a <i>XML</i> file with all the stations of the specified type.
     *
     *
     * The 3 types of the stations are:
     * <ul>
     *     <li>GPS/GNSS Continuous</li>
     *     <li>GNSS</li>
     *     <li>GNSS Continuous</li>
     * </ul>
     *
     * @param type String - type of the Stations (e.g. GNSS Continuous)
     * @return <i>XML</i> file with all the Stations of the specified type.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("type-xml/{type}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsTypeXML(@PathParam("type") String type) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationsType(type);
        String xml = parser.getFullXML(res);
        
        return xml;
    }

    /**
     * <p>Replaced by v2/type</p>
     * <p><b>GET STATIONS TYPE CSV</b> - Returns a <i>CSV</i> file with all the stations of the specified type.
     *
     * The 3 types of the stations are:
     * <ul>
     *     <li>GPS/GNSS Continuous</li>
     *     <li>GNSS</li>
     *     <li>GNSS Continuous</li>
     * </ul>
     *
     * @param type String - type of the Stations (e.g. GNSS Continuous)
     * @return <i>CSV</i> file with all the Stations of the specified type.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("type-csv/{type}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsTypeCSV(@PathParam("type") String type) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList<Station> res = dbc.getStationsType(type);
        String csv = parser.getFullCSV(res);
        
        File file = new File("type.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/type</p>
     * <p><b>GET STATIONS TYPE SHORT CSV</b> - Returns a <i>CSV</i> file with all the stations of the specified type, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>City, State, Country</li>
     *     <li>Agency(ies)</li>
     *     <li>Network(s)</li>
     * </ul>
     *
     * The 3 types of the stations are:
     * <ul>
     *     <li>GPS/GNSS Continuous</li>
     *     <li>GNSS</li>
     *     <li>GNSS Continuous</li>
     * </ul>
     *
     * @param type String - type of the Stations (e.g. GNSS Continuous)
     * @return <i>CSV</i> file with all the Stations of the specified type, with less information.
     * @throws IOException ..
     * @author José Manteigueiro
     * @version 1.1
     * @since 19/10/2017
     * @deprecated
     */
    @Path("type-short-csv/{type}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsTypeShortCSV(@PathParam("type") String type) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsTypeShort(type);
        String csv = parser.getStationsShortCSV(res);

        File file = new File("Stations_Type_"+type.replace(" ","_")+"_Short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/antenna</p>
     * <p><b>GET STATIONS ANTENNA TYPE</b> - Returns a <i>JSON</i> String with all the stations of the specified antenna type (name).
     *
     * @param antennaType String - antenna type (name) of the Stations (e.g. LEIGS09)
     * @return <i>JSON</i> String with all the Stations with the specified antenna type (name).
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("antenna_type/{antennaType}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsAntennaType(@PathParam("antennaType") String antennaType) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList<Station> res = dbc.getStationsAntennaType(antennaType);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/antenna</p>
     * <p><b>GET STATIONS ANTENNA TYPE SHORT</b> - Returns a <i>JSON</i> String with all the stations of the specified antenna type (name), but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     * @param antennaType String - antenna type (name) of the Stations (e.g. LEIGS09)
     * @return <i>JSON</i> String with all the Stations with the specified antenna type (name), with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("antenna_type-short/{antennaType}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsAntennaTypeShort(@PathParam("antennaType") String antennaType) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList<Station> res = dbc.getStationsAntennaTypeShort(antennaType);
        JSONArray obj = parser.getShortJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/antenna</p>
     * <p><b>GET STATIONS ANTENNA TYPE XML</b> - Returns a <i>XML</i> file with all the stations of the specified antenna type (name).
     *
     * @param antennaType String - antenna type (name) of the Stations (e.g. LEIGS09)
     * @return <i>XML</i> file with all the Stations with the specified antenna type (name).
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("antenna_type-xml/{antennaType}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsAntennaTypeXML(@PathParam("antennaType") String antennaType) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList<Station> res = dbc.getStationsAntennaType(antennaType);

        return parser.getFullXML(res);
    }

    /**
     * <p>Replaced by v2/antenna</p>
     * <p><b>GET STATIONS ANTENNA TYPE CSV</b> - Returns a <i>CSV</i> file with all the stations of the specified antenna type (name).
     *
     * @param antennaType String - antenna type (name) of the Stations (e.g. LEIGS09)
     * @return <i>CSV</i> file with all the Stations with the specified antenna type (name).
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("antenna_type-csv/{antennaType}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsAntennaTypeCSV(@PathParam("antennaType") String antennaType) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList<Station> res = dbc.getStationsAntennaType(antennaType);
        String csv = parser.getFullCSV(res);
        
        File file = new File("antenna_type.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/antenna</p>
     * <p><b>GET STATIONS ANTENNA TYPE SHORT CSV</b> - Returns a <i>CSV</i> file with all the stations of the specified antenna type (name), but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param antennaType String - antenna type (name) of the Stations (e.g. LEIGS09)
     * @return <i>CSV</i> file with all the Stations with the specified antenna type (name), with less information.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("antenna_type-short-csv/{antennaType}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsAntennaTypeShortCSV(@PathParam("antennaType") String antennaType) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList<Station> res = dbc.getStationsAntennaTypeShort(antennaType);
        String csv = parser.getShortCSV(res);
        
        File file = new File("antenna_type-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response .ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
                        .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
                        .build();
    }

    /**
     * <p>Replaced by v2/receiver</p>
     * <p><b>GET STATIONS RECEIVER TYPE</b> - Returns a <i>JSON</i> String with all the stations that have the specified receiver type (name).
     *
     * @param receiverType String - receiver type (name) of the Stations (e.g. ALTUS APS-3)
     * @return <i>JSON</i> String with all the Stations with the specified receiver type (name).
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("receiver_type/{receiverType}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsReceiverType(@PathParam("receiverType") String receiverType) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsReceiverType(receiverType);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/receiver</p>
     * <p><b>GET STATIONS RECEIVER TYPE SHORT</b> - Returns a <i>JSON</i> String with all the stations that have the specified receiver type (name), but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param receiverType String - receiver type (name) of the Stations (e.g. ALTUS APS-3)
     * @return <i>JSON</i> String with all the Stations with the specified receiver type (name), with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("receiver_type-short/{receiverType}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsReceiverTypeShort(@PathParam("receiverType") String receiverType) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsReceiverType(receiverType);
        JSONArray obj = parser.getShortJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/receiver</p>
     * <p><b>GET STATIONS RECEIVER TYPE XML</b> - Returns a <i>XML</i> file with all the stations that have the specified receiver type (name).
     *
     * @param receiverType String - receiver type (name) of the Stations (e.g. ALTUS APS-3)
     * @return <i>XML</i> file with all the Stations with the specified receiver type (name).
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("receiver_type-xml/{receiverType}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsReceiverTypeXML(@PathParam("receiverType") String receiverType) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationsReceiverType(receiverType);
        String xml = parser.getFullXML(res);
        
        return xml;
    }

    /**
     * <p>Replaced by v2/receiver</p>
     * <p><b>GET STATIONS RECEIVER TYPE CSV</b> - Returns a <i>CSV</i> file with all the stations that have the specified receiver type (name).
     *
     * @param receiverType String - receiver type (name) of the Stations (e.g. ALTUS APS-3)
     * @return <i>CSV</i> file with all the Stations with the specified receiver type (name).
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("receiver_type-csv/{receiverType}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsReceiverTypeCSV(@PathParam("receiverType") String receiverType) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationsReceiverType(receiverType);
        String csv = parser.getFullCSV(res);
        
        File file = new File("receiver_type.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/receiver</p>
     * <p><b>GET STATIONS RECEIVER TYPE SHORT CSV</b> - Returns a <i>CSV</i> file with all the stations that have the specified receiver type (name), but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     * @param receiverType String - receiver type (name) of the Stations (e.g. ALTUS APS-3)
     * @return <i>CSV</i> file with all the Stations with the specified receiver type (name), with less information.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("receiver_type-short-csv/{receiverType}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsReceiverTypeShortCSV(@PathParam("receiverType") String receiverType) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationsReceiverType(receiverType);
        String csv = parser.getShortCSV(res);
        
        File file = new File("receiver_type-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/radome</p>
     * <p><b>GET STATIONS RADOME TYPE</b> - Returns a <i>JSON</i> String with all the stations that have the specified radome type (name).
     *
     * @param radomeType String - radome type (name) of the Stations (e.g. LEIC)
     * @return <i>JSON</i> String with all the Stations with the specified radome type (name).
     * @author José Manteigueiro
     * @version 1.1
     * @since 12/07/2017
     * @deprecated
     */
    @Path("radome_type/{radomeType}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsRadomeType(@PathParam("radomeType") String radomeType) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsRadomeType(radomeType);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/radome</p>
     * <p><b>GET STATIONS RADOME TYPE SHORT</b> - Returns a <i>JSON</i> String with all the stations that have the specified radome type (name), but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param radomeType String - radome type (name) of the Stations (e.g. LEIC)
     * @return <i>JSON</i> String with all the Stations with the specified radome type (name), with less information.
     * @author José Manteigueiro
     * @version 1.1
     * @since 12/07/2017
     * @deprecated
     */
    @Path("radome_type-short/{radomeType}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsRadomeTypeShort(@PathParam("radomeType") String radomeType) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsRadomeType(radomeType);
        JSONArray obj = parser.getShortJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }

    /**
     * <p>Replaced by v2/radome</p>
     * <p><b>GET STATIONS RADOME TYPE XML</b> - Returns a <i>XML</i> file with all the stations that have the specified radome type (name).
     *
     * @param radomeType String - radome type (name) of the Stations (e.g. LEIC)
     * @return <i>XML</i> file with all the Stations with the specified radome type (name).
     * @author José Manteigueiro
     * @version 1.1
     * @since 12/07/2017
     * @deprecated
     */
    @Path("radome_type-xml/{radomeType}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsRadomeTypeXML(@PathParam("radomeType") String radomeType) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationsRadomeType(radomeType);
        String xml = parser.getFullXML(res);
        
        return xml;
    }

    /**
     * <p>Replaced by v2/radome</p>
     * <p><b>GET STATIONS RADOME TYPE CSV</b> - Returns a <i>CSV</i> file with all the stations that have the specified radome type (name).
     *
     * @param radomeType String - radome type (name) of the Stations (e.g. LEIC)
     * @return <i>CSV</i> file with all the Stations with the specified radome type (name).
     * @throws IOException ...
     * @author José Manteigueiro
     * @version 1.1
     * @since 12/07/2017
     * @deprecated
     */
    @Path("radome_type-csv/{radomeType}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsRadomeTypeCSV(@PathParam("radomeType") String radomeType) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsRadomeType(radomeType);
        String csv = parser.getFullCSV(res);
        
        File file = new File("radome_type.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/radome</p>
     * <p><b>GET STATIONS RADOME TYPE SHORT CSV</b> - Returns a <i>CSV</i> file with all the stations that have the specified radome type (name), but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param radomeType String - radome type (name) of the Stations (e.g. LEIC)
     * @return <i>CSV</i> file with all the Stations with the specified radome type (name), with less information.
     * @throws IOException ...
     * @author José Manteigueiro
     * @version 1.1
     * @since 12/07/2017
     * @deprecated
     */
    @Path("radome_type-short-csv/{radomeType}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsRadomeTypeShortCSV(@PathParam("radomeType") String radomeType) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsRadomeType(radomeType);
        String csv = parser.getShortCSV(res);
        
        File file = new File("radome_type-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/country</p>
     * <p><b>GET STATIONS COUNTRY</b> - Returns a <i>JSON</i> String with all the Stations that belong to the specified country.
     *
     * @param country String - Stations country(e.g. Italy)
     * @return <i>JSON</i> String with all the Stations that belong to the specified country.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("country/{country}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCountry(@PathParam("country") String country) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsCountry(country);
        JSONArray obj = parser.getFullJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }

    /**
     * <p>Replaced by v2/country</p>
     * <p><b>GET STATIONS COUNTRY SHORT</b> - Returns a <i>JSON</i> String with all the Stations that belong to the specified country, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     * @param country String - Stations country(e.g. Italy)
     * @return <i>JSON</i> String with all the Stations that belong to the specified country, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("country-short/{country}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCountryShort(@PathParam("country") String country) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsCountry(country);
        JSONArray obj = parser.getShortJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }

    /**
     * <p>Replaced by v2/country</p>
     * <p><b>GET STATIONS COUNTRY XML</b> - Returns a <i>XML</i> file with all the Stations that belong to the specified country.
     *
     * @param country String - Stations country(e.g. Italy)
     * @return <i>XML</i> file with all the Stations that belong to the specified country.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("country-xml/{country}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsCountryXML(@PathParam("country") String country) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationsCountry(country);
        String xml = parser.getFullXML(res);
        
        return xml;
    }

    /**
     * <p>Replaced by v2/country</p>
     * <p><b>GET STATIONS COUNTRY CSV</b> - Returns a <i>CSV</i> file with all the Stations that belong to the specified country.
     *
     * @param country String - Stations country(e.g. Italy)
     * @return <i>CSV</i> file with all the Stations that belong to the specified country.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("country-csv/{country}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCountryCSV(@PathParam("country") String country) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsCountry(country);
        String csv = parser.getFullCSV(res);
        
        File file = new File("country.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/country</p>
     * <p><b>GET STATIONS COUNTRY SHORT CSV</b> - Returns a <i>CSV</i> file with all the Stations that belong to the specified country, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param country String - Stations country(e.g. Italy)
     * @return <i>CSV</i> file with all the Stations that belong to the specified country, with less information.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("country-short-csv/{country}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCountryShortCSV(@PathParam("country") String country) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsCountry(country);
        String csv = parser.getShortCSV(res);
        
        File file = new File("country-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/state</p>
     * <p><b>GET STATIONS STATE</b> - Returns a <i>JSON</i> String with all the Stations from a specified state.
     *
     * @param state String - Stations state(e.g. Dublin)
     * @return <i>JSON</i> String with all the Stations that belong to the specified state.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("state/{state}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsState(@PathParam("state") String state) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsState(state);
        JSONArray obj = parser.getFullJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }

    /**
     * <p>Replaced by v2/state</p>
     * <p><b>GET STATIONS STATE SHORT</b> - Returns a <i>JSON</i> String with all the Stations from a specified state, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param state String - Stations state(e.g. Dublin)
     * @return <i>JSON</i> String with all the Stations that belong to the specified state, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("state-short/{state}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsStateShort(@PathParam("state") String state) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsState(state);
       JSONArray obj = parser.getShortJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }

    /**
     * <p>Replaced by v2/state</p>
     * <p><b>GET STATIONS STATE XML</b> - Returns a <i>XML</i> file with all the Stations from a specified state.
     *
     * @param state String - Stations state(e.g. Dublin)
     * @return <i>XML</i> file with all the Stations that belong to the specified state.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("state-xml/{state}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsStateXML(@PathParam("state") String state) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationsState(state);
        String xml = parser.getFullXML(res);
        
        return xml;
    }

    /**
     * <p>Replaced by v2/state</p>
     * <p><b>GET STATIONS STATE CSV</b> - Returns a <i>CSV</i> file with all the Stations from a specified state.
     *
     * @param state String - Stations state(e.g. Dublin)
     * @return <i>CSV</i> file with all the Stations that belong to the specified state.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("state-csv/{state}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsStateCSV(@PathParam("state") String state) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsState(state);
        String csv = parser.getFullCSV(res);
        
        File file = new File("state.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/state</p>
     * <p><b>GET STATIONS STATE SHORT CSV</b> - Returns a <i>CSV</i> file with all the Stations from a specified state, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param state String - Stations state(e.g. Dublin)
     * @return <i>CSV</i> file with all the Stations that belong to the specified state, but with less information.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("state-short-csv/{state}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsStateShortCSV(@PathParam("state") String state) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsState(state);
        String csv = parser.getShortCSV(res);
        
        File file = new File("state-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();

        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
                .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
                .build();
    }

    /**
     * <p>Replaced by v2/city</p>
     * <p><b>GET STATIONS CITY</b> - Returns a <i>JSON</i> String with all the Stations from a specified city.
     *
     * @param city String - Stations city(e.g. Sofia)
     * @return <i>JSON</i> String with all the Stations that belong to the specified city.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("city/{city}")
    //@GET
    // @Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCity(@PathParam("city") String city) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList<Station> res = dbc.getStationsCity(city);
        JSONArray obj = parser.getFullJSON(res);

        return obj.toJSONString();
    }

    /**
     * <p>Replaced by v2/city</p>
     * <p><b>GET STATIONS CITY SHORT</b> - Returns a <i>JSON</i> String with all the Stations from a specified city, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param city String - Stations city(e.g. Sofia)
     * @return <i>JSON</i> String with all the Stations that belong to the specified city, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("city-short/{city}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCityShort(@PathParam("city") String city) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getStationsCity(city);
        JSONArray obj = parser.getShortJSON(res);
        String json = obj.toJSONString();

        return json;
    }


    /**
     * <p>Replaced by v2/city</p>
     * <p><b>GET STATIONS CITY XML</b> - Returns a <i>XML</i> file with all the Stations from a specified city.
     *
     * @param city String - Stations city(e.g. Sofia)
     * @return <i>XML</i> file with all the Stations that belong to the specified city.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("city-xml/{city}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsCityXML(@PathParam("city") String city) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getStationsCity(city);
        String xml = parser.getFullXML(res);

        return xml;
    }

    /**
     * <p>Replaced by v2/city</p>
     * <p><b>GET STATIONS CITY CSV</b> - Returns a <i>CSV</i> file with all the Stations from a specified city.
     *
     * @param city String - Stations city(e.g. Sofia)
     * @return <i>CSV</i> file with all the Stations that belong to the specified city.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("city-csv/{city}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCityCSV(@PathParam("city") String city) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList<Station> res = dbc.getStationsCity(city);
        String csv = parser.getFullCSV(res);

        File file = new File("city.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();

        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
                .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
                .build();
    }

    /**
     * <p>Replaced by v2/city</p>
     * <p><b>GET STATIONS CITY SHORT CSV</b> - Returns a <i>CSV</i> file with all the Stations from a specified city, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * @param city String - Stations city(e.g. Sofia)
     * @return <i>CSV</i> file with all the Stations that belong to the specified city, with less information.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("city-short-csv/{city}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCityShortCSV(@PathParam("city") String city) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getStationsCity(city);
        String csv = parser.getShortCSV(res);

        File file = new File("city-short.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();

        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
                .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
                .build();
    }

    /**
     * <p>Replaced by v2/combination</p>
     * <p><b>GET STATIONS COMBINATION</b> - Returns a <i>JSON</i> String with all the Stations with the given parameters.
     * <p><b>{request}</b> – should be replaced by the request String.</p>
     * <p><i>e.g.</i> marker:NICE,STA3&country:Portugal</p>
     * <br>
     * <p>When creating the request String the format used should be: Parameter1:value1,value2&Parameter2:value3,value4&…</p>
     * <p>The parameters available for the request String are:</p>
     * <ul>
     *     <li>marker</li>
     *     <li>site</li>
     *     <li>height</li>
     *     <li>installed_date</li>
     *     <li>removed_date</li>
     *     <li>network</li>
     *     <li>agency</li>
     *     <li>antenna_type</li>
     *     <li>receiver_type</li>
     *     <li>radome_type</li>
     *     <li>country</li>
     *     <li>state</li>
     *     <li>city</li>
     *     <li>coordinates</li>
     *     <li>satellite</li>
     *     <li>type</li>
     * </ul>
     * <br>
     * <p>For the coordinates, the request String should be built as following:</p>
     * <p>coordinates=coordinates_type:values</p>
     * <p>Where coordinates_type can be:</p>
     * <ul>
     *     <li>rectangle</li>
     *     <li>circle</li>
     *     <li>polygon</li>
     * </ul>
     * <br>
     * <p>Rectangle type String:</p>
     * <ul>
     *     <li>coordinates=rectangle:minLat,maxLat,minLon,maxLon</li>
     * </ul>
     * <br>
     * <p>Circle type String:<p>
     * <ul>
     *     <li>coordinates=circle:lat,lon,radius</li>
     * </ul>
     * <br>
     * <p>Polygon type String:</p>
     * <ul>
     *     <li>coordinates=polygon:PointA_lat!PointA_lon,PointB_lat!PointB_lon,PointC_lat!PointC_lon,PointD_lat!PointD_lon,PointE_lat!PointE_lon</li>
     * </ul>
     * <p>Where the points A to E are connected the following way: <p></p><a href="../../../images/coordinates_polygon_order.jpg" target="_blank">Polygon Order</a></p></p>
     * <p>NOTE: the point order on the must follow this exact order, point B (connects to A) must be declared after A and point C (connects with B) after B, and so on…</p>
     *
     * @param request Parameter1:value1,value2&Parameter2:value3,value4&…
     * @return <i>JSON</i> String with all the Stations with the given parameters.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("combination/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCombination(@PathParam("request") String request) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String json = "";
        ArrayList res = dbc.getStationsCombined(request);
        JSONArray obj = parser.getFullJSON(res);
        json = obj.toJSONString();

        return json;
    }

    /**
     * <p>Replaced by v2/combination</p>
     * <p><b>GET STATIONS COMBINATION SHORT</b> - Returns a <i>JSON</i> String with all the Stations with the given parameters, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * <p><b>{request}</b> – should be replaced by the request String.</p>
     * <p><i>e.g.</i> marker:NICE,STA3&country:Portugal</p>
     * <br>
     * <p>When creating the request String the format used should be: Parameter1:value1,value2&Parameter2:value3,value4&…</p>
     * <p>The parameters available for the request String are:</p>
     * <ul>
     *     <li>marker</li>
     *     <li>site</li>
     *     <li>height</li>
     *     <li>installed_date</li>
     *     <li>removed_date</li>
     *     <li>network</li>
     *     <li>agency</li>
     *     <li>antenna_type</li>
     *     <li>receiver_type</li>
     *     <li>radome_type</li>
     *     <li>country</li>
     *     <li>state</li>
     *     <li>city</li>
     *     <li>coordinates</li>
     *     <li>satellite</li>
     *     <li>type</li>
     * </ul>
     * <br>
     * <p>For the coordinates, the request String should be built as following:</p>
     * <p>coordinates=coordinates_type:values</p>
     * <p>Where coordinates_type can be:</p>
     * <ul>
     *     <li>rectangle</li>
     *     <li>circle</li>
     *     <li>polygon</li>
     * </ul>
     * <br>
     * <p>Rectangle type String:</p>
     * <ul>
     *     <li>coordinates=rectangle:minLat,maxLat,minLon,maxLon</li>
     * </ul>
     * <br>
     * <p>Circle type String:<p>
     * <ul>
     *     <li>coordinates=circle:lat,lon,radius</li>
     * </ul>
     * <br>
     * <p>Polygon type String:</p>
     * <ul>
     *     <li>coordinates=polygon:PointA_lat!PointA_lon,PointB_lat!PointB_lon,PointC_lat!PointC_lon,PointD_lat!PointD_lon,PointE_lat!PointE_lon</li>
     * </ul>
     * <p>Where the points A to E are connected the following way: <p></p><a href="../../../images/coordinates_polygon_order.jpg" target="_blank">Polygon Order</a></p></p>
     * <p>NOTE: the point order on the must follow this exact order, point B (connects to A) must be declared after A and point C (connects with B) after B, and so on…</p>
     *
     * @param request Parameter1:value1,value2&Parameter2:value3,value4&…
     * @return <i>JSON</i> String with all the Stations with the given parameters, with less information.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("combination-short/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getStationsCombinationShort(@PathParam("request") String request) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String json = "";
        ArrayList res = dbc.getStationsCombined(request);
        JSONArray obj = parser.getShortJSON(res);
        json = obj.toJSONString();

        return json;
    }

    /**
     * <p>Replaced by v2/combination</p>
     * <p><b>GET STATIONS COMBINATION XML</b> - Returns a <i>XML</i> file with all the Stations with the given parameters.
     * <p><b>{request}</b> – should be replaced by the request String.</p>
     * <p><i>e.g.</i> marker:NICE,STA3&country:Portugal</p>
     * <br>
     * <p>When creating the request String the format used should be: Parameter1:value1,value2&Parameter2:value3,value4&…</p>
     * <p>The parameters available for the request String are:</p>
     * <ul>
     *     <li>marker</li>
     *     <li>site</li>
     *     <li>height</li>
     *     <li>installed_date</li>
     *     <li>removed_date</li>
     *     <li>network</li>
     *     <li>agency</li>
     *     <li>antenna_type</li>
     *     <li>receiver_type</li>
     *     <li>radome_type</li>
     *     <li>country</li>
     *     <li>state</li>
     *     <li>city</li>
     *     <li>coordinates</li>
     *     <li>satellite</li>
     *     <li>type</li>
     * </ul>
     * <br>
     * <p>For the coordinates, the request String should be built as following:</p>
     * <p>coordinates=coordinates_type:values</p>
     * <p>Where coordinates_type can be:</p>
     * <ul>
     *     <li>rectangle</li>
     *     <li>circle</li>
     *     <li>polygon</li>
     * </ul>
     * <br>
     * <p>Rectangle type String:</p>
     * <ul>
     *     <li>coordinates=rectangle:minLat,maxLat,minLon,maxLon</li>
     * </ul>
     * <br>
     * <p>Circle type String:<p>
     * <ul>
     *     <li>coordinates=circle:lat,lon,radius</li>
     * </ul>
     * <br>
     * <p>Polygon type String:</p>
     * <ul>
     *     <li>coordinates=polygon:PointA_lat!PointA_lon,PointB_lat!PointB_lon,PointC_lat!PointC_lon,PointD_lat!PointD_lon,PointE_lat!PointE_lon</li>
     * </ul>
     * <p>Where the points A to E are connected the following way: <p></p><a href="../../../images/coordinates_polygon_order.jpg" target="_blank">Polygon Order</a></p></p>
     * <p>NOTE: the point order on the must follow this exact order, point B (connects to A) must be declared after A and point C (connects with B) after B, and so on…</p>
     *
     * @param request Parameter1:value1,value2&Parameter2:value3,value4&…
     * @return <i>XML</i> file with all the Stations with the given parameters.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("combination-xml/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getStationsCombinationXML(@PathParam("request") String request) {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String xml = "";
        ArrayList res = dbc.getStationsCombined(request);
        xml = parser.getFullXML(res);

        return xml;
    }

    /**
     * <p>Replaced by v2/combination</p>
     * <p><b>GET STATIONS COMBINATION CSV</b> - Returns a <i>CSV</i> file with all the Stations with the given parameters.
     * <p><b>{request}</b> – should be replaced by the request String.</p>
     * <p><i>e.g.</i> marker:NICE,STA3&country:Portugal</p>
     * <br>
     * <p>When creating the request String the format used should be: Parameter1:value1,value2&Parameter2:value3,value4&…</p>
     * <p>The parameters available for the request String are:</p>
     * <ul>
     *     <li>marker</li>
     *     <li>site</li>
     *     <li>height</li>
     *     <li>installed_date</li>
     *     <li>removed_date</li>
     *     <li>network</li>
     *     <li>agency</li>
     *     <li>antenna_type</li>
     *     <li>receiver_type</li>
     *     <li>radome_type</li>
     *     <li>country</li>
     *     <li>state</li>
     *     <li>city</li>
     *     <li>coordinates</li>
     *     <li>satellite</li>
     *     <li>type</li>
     * </ul>
     * <br>
     * <p>For the coordinates, the request String should be built as following:</p>
     * <p>coordinates=coordinates_type:values</p>
     * <p>Where coordinates_type can be:</p>
     * <ul>
     *     <li>rectangle</li>
     *     <li>circle</li>
     *     <li>polygon</li>
     * </ul>
     * <br>
     * <p>Rectangle type String:</p>
     * <ul>
     *     <li>coordinates=rectangle:minLat,maxLat,minLon,maxLon</li>
     * </ul>
     * <br>
     * <p>Circle type String:<p>
     * <ul>
     *     <li>coordinates=circle:lat,lon,radius</li>
     * </ul>
     * <br>
     * <p>Polygon type String:</p>
     * <ul>
     *     <li>coordinates=polygon:PointA_lat!PointA_lon,PointB_lat!PointB_lon,PointC_lat!PointC_lon,PointD_lat!PointD_lon,PointE_lat!PointE_lon</li>
     * </ul>
     * <p>Where the points A to E are connected the following way: <p></p><a href="../../../images/coordinates_polygon_order.jpg" target="_blank">Polygon Order</a></p></p>
     * <p>NOTE: the point order on the must follow this exact order, point B (connects to A) must be declared after A and point C (connects with B) after B, and so on…</p>
     *
     * @param request Parameter1:value1,value2&Parameter2:value3,value4&…
     * @return <i>CSV</i> file with all the Stations with the given parameters.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("combination-csv/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCombinationCSV(@PathParam("request") String request) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String csv = "";
        ArrayList res = dbc.getStationsCombined(request);
        csv = parser.getFullCSV(res);
        
        File file = new File("stations.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    /**
     * <p>Replaced by v2/combination</p>
     * <p><b>GET STATIONS COMBINATION SHORT CSV</b> - Returns a <i>CSV</i> file with all the Stations with the given parameters, but with less information.
     * <p>Only includes:</p>
     *
     * <ul>
     *     <li>Station Name</li>
     *     <li>Station Marker</li>
     *     <li>X, Y, Z</li>
     *     <li>Lat, Lon, Altitude</li>
     *     <li>Country</li>
     * </ul>
     *
     * <p><b>{request}</b> – should be replaced by the request String.</p>
     * <p><i>e.g.</i> marker:NICE,STA3&country:Portugal</p>
     * <br>
     * <p>When creating the request String the format used should be: Parameter1:value1,value2&Parameter2:value3,value4&…</p>
     * <p>The parameters available for the request String are:</p>
     * <ul>
     *     <li>marker</li>
     *     <li>site</li>
     *     <li>height</li>
     *     <li>installed_date</li>
     *     <li>removed_date</li>
     *     <li>network</li>
     *     <li>agency</li>
     *     <li>antenna_type</li>
     *     <li>receiver_type</li>
     *     <li>radome_type</li>
     *     <li>country</li>
     *     <li>state</li>
     *     <li>city</li>
     *     <li>coordinates</li>
     *     <li>satellite</li>
     *     <li>type</li>
     * </ul>
     * <br>
     * <p>For the coordinates, the request String should be built as following:</p>
     * <p>coordinates=coordinates_type:values</p>
     * <p>Where coordinates_type can be:</p>
     * <ul>
     *     <li>rectangle</li>
     *     <li>circle</li>
     *     <li>polygon</li>
     * </ul>
     * <br>
     * <p>Rectangle type String:</p>
     * <ul>
     *     <li>coordinates=rectangle:minLat,maxLat,minLon,maxLon</li>
     * </ul>
     * <br>
     * <p>Circle type String:<p>
     * <ul>
     *     <li>coordinates=circle:lat,lon,radius</li>
     * </ul>
     * <br>
     * <p>Polygon type String:</p>
     * <ul>
     *     <li>coordinates=polygon:PointA_lat!PointA_lon,PointB_lat!PointB_lon,PointC_lat!PointC_lon,PointD_lat!PointD_lon,PointE_lat!PointE_lon</li>
     * </ul>
     * <p>Where the points A to E are connected the following way: <p></p><a href="../../../images/coordinates_polygon_order.jpg" target="_blank">Polygon Order</a></p></p>
     * <p>NOTE: the point order on the must follow this exact order, point B (connects to A) must be declared after A and point C (connects with B) after B, and so on…</p>
     *
     * @param request Parameter1:value1,value2&Parameter2:value3,value4&…
     * @return <i>CSV</i> file with all the Stations with the given parameters, with less information.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("combination-short-csv/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getStationsCombinationShortCSV(@PathParam("request") String request) throws IOException {
        DataBaseConnection dbc = new DataBaseConnection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String csv = "";
        ArrayList res = dbc.getStationsCombined(request);
        csv = parser.getShortCSV(res);
        
        File file = new File("stations.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }
    
    // =========================================================================
    // T2 QUERY METHODS
    // =========================================================================
    /*
    * GET ALL FILES
    * Returns all the files.
    *
    * É PRECISO VERIFICCAR PQ A TABELA files NAO EXISTE AGORA É
    * rinex_files E other_files
    *
    */

    /**
     * <p><b>T2 GET ALL FILES</b> - Returns a <i>JSON</i> String with all files.
     *
     * @return <i>JSON</i> String with all files.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("files")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getAllFiles() {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getRinexFiles();
        JSONArray obj = parser.getFileFullJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }

    /**
     * <p><b>T2 GET ALL FILES XML</b> - Returns a <i>XML</i> file with all files.
     *
     * @return <i>XML</i> file with all files.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("files-xml")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getAllFilesXML() {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getRinexFiles();
        String xml = parser.getFileFullXML(res);
        
        return xml;
    }

    /**
     * <p><b>T2 GET ALL FILES CSV</b> - Returns a <i>CSV</i> file with all files.
     *
     * <p>Note: It might take a while</p>
     *
     * @return <i>CSV</i> file with all files.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("files-csv")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getAllFilesCSV() throws IOException {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getRinexFiles();
        String csv = parser.getFileFullCSV(res);
        
        File file = new File("files.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    public Tuple2 getFilesDateRange(String date_from,
                                    String date_to,
                                    String format,
                                    int pageNumber,
                                    int perPageNumber,
                                    int bad_files
                                    ) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getFilesDateRange(date_from, date_to,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;

    }
    public Tuple2 getFilesDatePublished(String date_from,
                                                         String date_to,
                                                         String format,
                                                         int pageNumber,
                                                         int perPageNumber,
                                                         int bad_files
    ) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getFilesDatePublished(date_from, date_to,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;

    }


    /**
     * <p><b>T2 GET ALL FILES PUBLISHED DATE</b> - Returns a <i>JSON</i> String with all files in a data range.
     * <p>Dates are in format <b>YYYY/MM/DD</b>, e.g. 2015/03/12</p>
     *
     * @param date_from YYYY/MM/DD
     * @param date_to YYYY/MM/DD
     * @return <i>JSON</i> String with all files in a data range.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("files/published_date/{date_from}&{date_to}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getFilesPublishedDate(@PathParam("date_from") String date_from,
                                        @PathParam("date_to") String date_to) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getFilesPublishDate(date_from, date_to);
        JSONArray obj = parser.getFileFullJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }

    /**
     * <p><b>T2 GET ALL FILES PUBLISHED DATE XML</b> - Returns a <i>XML</i> file with all files in a data range.
     * <p>Dates are in format <b>YYYY/MM/DD</b>, e.g. 2015/03/12</p>
     *
     * @param date_from YYYY/MM/DD
     * @param date_to YYYY/MM/DD
     * @return <i>XML</i> file with all files in a data range.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("files/published_date-xml/{date_from}&{date_to}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getFilesPublishedDateXML(@PathParam("date_from") String date_from, 
            @PathParam("date_to") String date_to) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to XML
        ArrayList res = dbc.getFilesPublishDate(date_from, date_to);
        String xml = parser.getFileFullXML(res);
        
        return xml;
    }

    /**
     * <p><b>T2 GET ALL FILES PUBLISHED DATE CSV</b> - Returns a <i>CSV</i> file with all files in a data range.
     * <p>Dates are in format <b>YYYY/MM/DD</b>, e.g. 2015/03/12</p>
     *
     * @param date_from YYYY/MM/DD
     * @param date_to YYYY/MM/DD
     * @return <i>CSV</i> file with all files in a data range.
     * @throws IOException ...
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
    @Path("files/published_date-csv/{date_from}&{date_to}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public Response getFilesPublishedDateCSV(@PathParam("date_from") String date_from,
                                             @PathParam("date_to") String date_to) throws IOException {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        ArrayList res = dbc.getFilesPublishDate(date_from, date_to);
        String csv = parser.getFileFullCSV(res);
        
        File file = new File("files.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();
        
        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();
    }

    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Tuple2 getFilesFormat(String Format,String format,int pageNumber,int perPageNumber, int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex =  dbc.getRinexFilesFormat(Format,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    public Tuple2 getFilesMarker(String marker,String format,int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesStationMarker(marker,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    /**
     * <p><b>T2 GET ALL FILES MARKER DATA</b> - Returns a <i>JSON</i> String with all files with the given markers and date.
     * <p><b>request</b> - String that specifies the markers and datas of the Stations</p>
     *
     * @deprecated Needs an example!
     * @param request e.g. ??
     * @return <i>JSON</i> String with all files with the given markers and date.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     *
     */
    @Path("files/marker-data/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getFilesMarkerData(@PathParam("request") String request) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String json = "";
        ArrayList res = dbc.getFilesMarkerDatesCombined(request);
        JSONArray obj = parser.getFileFullJSON(res);
        json = obj.toJSONString();

        return json;
    }


    /**
     * <p><b>T2 GET ALL FILES DATA LIST</b> - Returns a <i>LIST</i> with all files with the given date.
     * <p><b>request</b> - String that specifies the date of the Stations</p>
     *
     * @deprecated What kind of list is returned? What is the request parameter?
     * @param request e.g. ??
     * @return <i>LIST</i> with all files with the given date.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     */
    @Path("files/marker-data-list/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getFilesMarkerDataList(@PathParam("request") String request) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        // Turn result to JSON array
        String list = "";
        ArrayList res = dbc.getFilesMarkerDatesCombined(request);
        list = dbc.getURL(res);

        return list;
    }


    public Tuple2 getFilesName(String name,String format,int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        String result;
        // Turn result to JSON array
        try {
            count_and_rinex = dbc.getRinexFilesStationName(name,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    /**
     * <p><b>T2 GET ALL FILES COORDINATES</b> - Returns a <i>JSON</i> String with all files with the given coordinates.
     * <p><b>minLat</b> - minimum Latitude value</p>
     * <p><b>maxLat</b> - maximum Latitude value</p>
     * <p><b>minLon</b> - minimum Longitude value</p>
     * <p><b>minLat</b> - maximum Longitude value</p>
     *
     * @param minLat e.g. 42.9364
     * @param maxLat e.g. 46.7472
     * @param minLon e.g. -4.4675
     * @param maxLon e.g. 6.3667
     * @return <i>JSON</i> String with all files with the given coordinates.
     * @author Unknown
     * @version 1.0
     * @deprecated
     * @since Unknown
     */
    //@Path("files/coordinates/{minLat}&{maxLat}&{minLon}&{maxLon}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Tuple2 getFilesCoordinates(float minLat,
                                     float maxLat,
                                      float minLon,
                                      float maxLon,
                                      String format,
                                      int pageNumber,
                                      int perPageNumber,
                                      int bad_files
                                                        ) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesCoordinates(minLat, maxLat, minLon, maxLon,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }


    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Tuple2 getFilesCoordinatesData(String request,String format,int pageNumber,int perPageNumber,int bad_files) throws ParseException, SocketException, SQLException, ClassNotFoundException {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getFilesCoordinatesDateCombined(request,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException | ParseException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }


    //@GET
    //@Produces(MediaType.TEXT_PLAIN + ";charset=utf-8")
    public Tuple2 getFilesCoordinatesDataText(String request,String format,int pageNumber,int perPageNumber,int bad_files) throws ParseException {
        Parsers parser = new Parsers();
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getFilesCoordinatesDateCombined(request,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    /**
     * <p><b>T2 GET ALL FILES STATION DATES</b> - Returns a <i>JSON</i> String with all files from a Station with the given date interval of the Station installation.
     * <p>Dates are in format <b>YYYY/MM/DD</b>, e.g. 2015/03/12</p>
     *
     * @param date_from YYYY/MM/DD
     * @param date_to YYYY/MM/DD
     * @return <i>JSON</i> String with all files from a Station with the given date interval of the Station installation.
     * @author Unknown
     * @version 1.0
     * @since Unknown
     * @deprecated
     */
   // @Path("files/station_dates/{date_from}&{date_to}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Tuple2 getFilesStationDates(String date_from,
                                       String date_to,
                                       String format,
                                       int pageNumber,
                                       int perPageNumber,
                                       int bad_files
                                       ) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getFilesStationDates(date_from, date_to,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }
    public Tuple2 getFilesStationNetwork(String network,String format,int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesStationNetwork(network, pageNumber, perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    public Tuple2 getFilesStationType(String station_type,String format,int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesStationType(station_type, pageNumber, perPageNumber, bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    public Tuple2 getFilesAntennaType(String antenna_type,String format, int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesAntennaType(antenna_type,pageNumber,perPageNumber,bad_files);;
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }





    public Tuple2 getFilesReceiverType(String receiver_type,String format,int pageNumber,int perPageNumber, int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesReceiverType(receiver_type, pageNumber, perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    public Tuple2 getFilesRadomeType(String radome_type,String format,int pageNumber,int perPageNumber, int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesRadomeType(radome_type,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }






    public Tuple2 getFilesCountry(String country,String format,int pageNumber,int perPageNumber, int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        // Turn result to JSON array
        try {
            count_and_rinex = dbc.getRinexFilesCountry(country,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }



    public Tuple2<Integer, String> getFilesState(String state, String format, int pageNumber, int perPageNumber, int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesState(state,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }





    public Tuple2<Integer, String> getFilesCity(String city, String format, int pageNumber, int perPageNumber, int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesCity(city,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }





    public Tuple2 getFilesAgency(String agency, String format,int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesAgency(agency, pageNumber, perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }


    public Tuple2 getFilesCombination( String request,String format,int pageNumber,int perPageNumber,int bad_files) throws ParseException {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getFilesCombined(request,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }
    
   // @Path("files/combinationT3/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public Tuple2 getFilesCombinationT3( String request, String format, int pageNumber, int perPageNumber,int bad_files) throws ParseException {
        DataBaseT3Connection dbc = new DataBaseT3Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getFilesCombinedT3(request,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileT3FullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileT3FullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileT3FullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (ParseException | SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;

    }

    @Path("files/combinationT3-csv/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
    public String getFilesCombinationT3CSV(@PathParam("request") String request)
            throws IOException, ParseException, SQLException, ClassNotFoundException {
        DataBaseT3Connection dbc = new DataBaseT3Connection();
        Parsers parser = new Parsers();
        // Turn result to CSV
        String csv = "";
        Tuple2<Integer,ArrayList<Rinex_file>> res = dbc.getFilesCombinedT3(request,0,50,1);
        csv = parser.getFileT3FullCSV(res.getSecond());

        /*File file = new File("files.csv");
        FileWriter fw = new FileWriter(file.getAbsoluteFile());
        BufferedWriter bw = new BufferedWriter(fw);
        bw.write(csv);
        bw.close();

        return Response.ok(file, MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8")
      .header("Content-Disposition", "attachment; filename=\"" + file.getName() + "\"" ) //optional
      .build();*/
        return csv;
    }
    
    @Path("files/combinationT3-xml/{request}")
    //@GET
    //@Produces(MediaType.APPLICATION_XML + ";charset=utf-8")
    public String getFilesCombinationT3XML(@PathParam("request") String request) throws ParseException, SocketException, SQLException, ClassNotFoundException {
        DataBaseT3Connection dbc = new DataBaseT3Connection();
        Parsers parser = new Parsers();
        // Turn result to XML
        String xml = "";
        Tuple2<Integer,ArrayList<Rinex_file>> res = dbc.getFilesCombinedT3(request,0,50,1);
        xml = parser.getFileT3FullXML(res.getSecond());
        return xml;
    }




    public Tuple2 getSamplingFrequency(String sampling_freq,String format,int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex =  dbc.getFilesSampling(sampling_freq,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    /*
    * GET FILES length
    * Returns all the files with the given parameters.
    */
    public Tuple2 getLength(String length,String format,int pageNumber,int perPageNumber, int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getFilesLength(length,pageNumber,perPageNumber,bad_files);
                switch (format) {
                    case "json":
                        result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                        break;
                    case "xml":
                        result = parser.getFileFullXML(count_and_rinex.getSecond());
                        break;
                    case "csv":
                        result = parser.getFileFullCSV(count_and_rinex.getSecond());
                        break;
                    case "script":
                        result = parser.getFileURL(count_and_rinex.getSecond());
                        break;
                    default:
                        result = "Error: " + format + " Format Unknown";
                        break;
            }
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    /**
     * @param provider ..
     * @return ..
     * @deprecated
     */
    @Path("files/provider/{provider}")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getProvider(@PathParam("provider") String provider) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String json = "";
        ArrayList res = dbc.getFilesProvider(provider);
        JSONArray obj = parser.getFileFullJSON(res);
        json = obj.toJSONString();

        return json;
    }

    /*
    * GET STATIONS WITH EMBARGO
    * Returns all the stations.
    */

    
    /**
     * <p>Replaced by v2/list</p>
     * GET FILE TYPE LIST
     * Returns all triplet (format,sampling_window,sampling_frequency) in the file type.
     * @deprecated
     */
    @Path("files/file-type-list")
    //@GET
    //@Produces(MediaType.APPLICATION_JSON + ";charset=utf-8")
    public String getFileTypeList() {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        ArrayList res = dbc.getFileTypeList();
        JSONArray obj =parser.getFileTypeJSON(res);
        String json = obj.toJSONString();
        
        return json;
    }


    Tuple2 getFilesMD5(String md5,String format,int pageNumber,int perPageNumber, int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesMD5(md5,pageNumber,perPageNumber, bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;

    }

    Tuple2 getFilesFileName(String file_name,String format,int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Turn result to JSON array
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesFileName(file_name,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }
    Tuple2 getFilesbyAcronymDataCenter(String acronym,String format,int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        // Choice a Format
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        try {
            count_and_rinex = dbc.getRinexFilesFileDataCenterAcronym(acronym,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }

    Tuple2 getFilesFileNameWithMd5(String file_name, String md5checksum,String format,int pageNumber,int perPageNumber,int bad_files) {
        DataBaseT2Connection dbc = new DataBaseT2Connection();
        Parsers parser = new Parsers();
        String result;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2();
        // Turn result to JSON array
        try {
            count_and_rinex = dbc.getRinexFilesFileNameWithMd5(file_name, md5checksum,pageNumber,perPageNumber,bad_files);
            switch (format) {
                case "json":
                    result = parser.getFileFullJSON(count_and_rinex.getSecond()).toJSONString();
                    break;
                case "xml":
                    result = parser.getFileFullXML(count_and_rinex.getSecond());
                    break;
                case "csv":
                    result = parser.getFileFullCSV(count_and_rinex.getSecond());
                    break;
                case "script":
                    result = parser.getFileURL(count_and_rinex.getSecond());
                    break;
                default:
                    result = "Error: " + format + " Format Unknown";
                    break;
            }
        } catch (SocketException | SQLException | ClassNotFoundException ex) {
            result = ex.getMessage();
        }
        Tuple2<Integer,String> count_and_string = new Tuple2<>(count_and_rinex.getFirst(),result);
        return count_and_string;
    }
}

