/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Glass;

import Configuration.DBConsts;
import Configuration.SiteConfig;
import CustomClasses.StationNotFound;
import CustomClasses.markerinfo;
import EposTables.Connections;
import EposTables.Station;
import org.bouncycastle.pqc.crypto.xmss.XMSSKeyGenerationParameters;

import javax.naming.Context;
import javax.naming.InitialContext;
import javax.xml.crypto.Data;
import java.io.*;
import java.net.SocketException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.*;
import java.util.concurrent.locks.ReentrantLock;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kngo
 * @author Paul Crocker Optimizatons, New RunThread for Initializing
 */
public class JsonCache extends Thread {

    private static JsonCache instance;
    private static Object monitor = new Object();
    public  static Map<Integer, Station> cache = Collections.synchronizedMap(new HashMap<Integer, Station>());

    private static final String CACHEFILE = "./cache.json";

    private static boolean cacheInitRunning=false;
    private static  ReentrantLock lock = new ReentrantLock();

    // -1 not started
    // -2 reading cache file
    // -3 cache file could not be saved - inconsistent state
    // 0 is finished
    // other value is current processing id
    private static int initIdProcessing=-1;

    //private ArrayList<Integer> stations_Ids;
    private Connection c;

    public boolean iscacheInitRunning() {
        return cacheInitRunning;
    }

    public boolean iscacheEmpty(){
        return  cache.isEmpty();
    }
    public int cacheSize(){
        return  cache.size();
    }

    public String cacheSatus(){
        String msg = "Size: "+cache.size()+ "  Initializing: ";
        if (initIdProcessing==0) msg+=" Finished";
        else if (initIdProcessing==-1) msg+=" Not Yet Started or in Undefined State";
        else if (initIdProcessing==-2) msg+=" Reading the Cache File";
        else if (initIdProcessing==-3) msg+=" InConsistent State";
        else msg +=" Ongoing: Processing Station Id "+initIdProcessing;
        return msg;
    }

    public JsonCache() {

    }
    /*public JsonCache(ArrayList<Integer> _stations_Ids,Connection _c) {*/
    public JsonCache(Connection _c) {
        //this.stations_Ids= _stations_Ids;
        this.c=_c;
    }

    public synchronized void run(){

        //We must avoid running the initialize process more than once
        //need a mutex
        if (initIdProcessing==0) return; //cache is finished initializing
        lock.lock();
        try {
            if (cacheInitRunning) { return; } //Cache Init already called. Finally will release the lock
            cacheInitRunning=true;
        } finally {
            lock.unlock();
        }

        System.out.println("Cache Initialize is now running");

        InitCache(c);
    }


    private static void put(int idStation, Station station){
        cache.put(idStation, station);
    }

    public Station get(int idStation){
        return cache.get(idStation);
    }

    public ArrayList getAllStations(){
        ArrayList<Station> allStations = new ArrayList<>(cache.values());
        return allStations;
    }

    public ArrayList getAllIds(){
        Set<java.lang.Integer> a = cache.keySet();
        ArrayList<Integer> arr = new ArrayList<>();
        arr.addAll(a);
        return arr;
    }

    public int getMaxId(){
        Integer max = Collections.max(cache.keySet());
        return max;
    }


    /* Crocker: Method to look up Marker in the Cache : throw I/O exception if not found*/
    public Integer getKeyByValue(String marker) throws IOException {
        synchronized (cache) {
            for (Map.Entry<Integer, Station> entry : cache.entrySet()) {
                Station s = entry.getValue();
                if (s.getMarkerLongName().toUpperCase().contains(marker.toUpperCase()))
                    return entry.getKey();
            }
        }
        //throw with error string in json Format
        throw new IOException( "{\"Error\":\""+marker+" Not Found in Cache\"}" );
    }

    public List getMarkerandKeyByValue(String marker) throws IOException {
        List<markerinfo> marker_and_id = new ArrayList<markerinfo>();
        markerinfo mf = new markerinfo();
        synchronized (cache) {
            for (Map.Entry<Integer, Station> entry : cache.entrySet()) {
                Station s = entry.getValue();
                if (s.getMarkerLongName().toUpperCase().contains(marker.toUpperCase())) {
                    mf.setId(entry.getKey());
                    mf.setLongmarker(s.getMarkerLongName());
                    marker_and_id.add(mf);
                }
            }

            return marker_and_id;
        }
    }

    public static String getLongMarkerByMarker(String marker) {

        String long_marker = "";
        synchronized (cache) {
            for (Map.Entry<Integer, Station> entry : cache.entrySet()) {
                Station s = entry.getValue();
                if (s.getMarker().toUpperCase().contains(marker.toUpperCase())) {
                    long_marker = s.getMarkerLongName();
                }
            }

            return long_marker;
        }
    }

    public void clear(int idStation) {
        cache.put(idStation, null);
    }

    public void clear() {
        cache.clear();
    }

    //public void InitCache(ArrayList<Integer> stations_Ids, Connection c){

    //public synchronized void InitCache(Connection c){
    public static void InitCache(Connection c){

        boolean noCacheFile=false;
        System.out.println("Checking Cache File is available"+System.getProperty("user.dir")+"  File:+"+CACHEFILE);
        File file = new File(CACHEFILE);

            // Checks if cache file is available
            if (file.exists() && !file.isDirectory()) {
                System.out.println("Existing Cache file is available in the Directory:");
                try {
                    // Reads from cache file
                    initIdProcessing=-2;
                    cache = readCacheFile();
                } catch (Exception e) {
                    //If cache file is corrupted or not accessible deletes file and tries to load cache again.
                    System.err.println("Something Has Gone Badly wrong @170: cache File will be Deleted");
                    Logger.getLogger(JsonCache.class.getName()).log(Level.SEVERE, "Deleting Cache File", e);
                    e.printStackTrace();
                    file.delete();
                    System.err.println("Trying to Stop Application. Try restarting the domain/application");
                    throw new  IllegalArgumentException();
                }
            } else {
                try {
                    System.out.println("No cache file available. " + file.getCanonicalPath());
                    noCacheFile=true;
                } catch (IOException e) {
                    System.out.println("No cache file available. " + "UnKnownPath");
                }
            }
            ArrayList<Integer> stations_Ids;
            DataBaseConnection dbc;
            DataBaseT4Connection dc4;
            try {
                 dc4=new DataBaseT4Connection(); //need this to instantiate or will throw an exception later and the init process runs twice...
                 dbc = new DataBaseConnection();
                stations_Ids = dbc.getAllIdStations(c);
                System.out.println("There are "+ stations_Ids.size() + " to process");

                System.out.println("Begin Cache Initialization : Creating New cache file OR Verifying Existing");

                int cntNewStn=0;
                int cnt=0;
                boolean cacheChanged=false;
                //Loads data for each station
                for (Integer stations_Id : stations_Ids) {
                    cnt++;
                    if (0==cnt%50) //every 50 station send a message to console
                        System.out.println("Init Cache : Cached-Station-ID:"+ stations_Id);

                    initIdProcessing = stations_Id;

                    //check station is in cache or not
                    if (cache.containsKey(stations_Id))
                        continue;
                    else {
                        try {
                            //on creating the cache use new method to persist connection
                            Station station = executeQuery(stations_Id, dbc, c);
                            put(stations_Id, station);
                            /* this throws throws StationNotFound in case of error */
                            cntNewStn++;
                            cacheChanged = true;
                            if (50 == cntNewStn) {   //every 50 stations save the cache
                                cntNewStn = 0;
                                saveCacheFile(cache);
                            }
                        } catch (StationNotFound stationNotFound) {
                            //stationNotFound.printStackTrace();
                            System.err.println("A station was not found.  Maybe  deletes/inserts to the DB happened during cache initalize ...id"+ stations_Id);
                            ;
                        } catch (IOException e) {
                            System.err.println("Saving The Cache File Went Wrong @222- You should probably delete it and start again...");
                            e.printStackTrace();
                            initIdProcessing = -3;
                            return;
                        }
                    }
                } //End ALL Station Loop
                System.out.println("Init Cache : Cached-Station Init/Check Finished:");
                /* After creating the cache save it */
                if (cacheChanged || noCacheFile) {
                    try {
                        System.out.println("Finsihed Init Cache: Cache Created in Memory. Saving to file");
                        //Stores cache in file
                        saveCacheFile(cache);
                    } catch (IOException e) {
                        //If error occurred deletes cache file
                        System.err.println("Finished Init Cache: Saving The Cache File Went Wrong @238- You should probably delete it and start again ...");
                        e.printStackTrace();
                        initIdProcessing = -3;
                        return;
                    }
                }
                else {
                    System.out.println("Finished Init Cache: Cache is Unchanged : Saving not Necessary");
                }

                initIdProcessing = 0; //indicate cache processing has finished
            }
            catch( Exception e){
                    System.out.println(e.getMessage());
            }


        }


    private Station executeQuery(int id_station) throws StationNotFound {

        DataBaseConnection dbc = new DataBaseConnection();
        ArrayList res = dbc.getStationId(id_station);
        if(res.isEmpty()){
            throw new StationNotFound("Station with id: " + id_station + " was not found in the database.");
        }

        return (Station) res.get(0);
    }

    /* New Method to persist the database connection */
    private static Station executeQuery(int id_station, DataBaseConnection dbc, Connection c) throws StationNotFound {

        ArrayList res = dbc.getStationId(id_station, c);
        if(res.isEmpty()){
            throw new StationNotFound("Station with. id: " + id_station + " was not found in the database.");
        }

        return (Station) res.get(0);
    }


    public static JsonCache getInstance() {

        if (instance == null) {
            synchronized (monitor) {
                if (instance == null) {
                    instance = new JsonCache();
                }
            }
        }

        return instance;
    }

    /*
     Update Cached
     Given a station ID the method will either substitute or insert the station object info the cache
     The method can either save the new cache to a file or not - in which case it should be saved later.
     Code redone by Paul
     */
    synchronized void updateCache(String id_station, boolean updateCacheFile) throws StationNotFound, NumberFormatException, IOException {

        if (initIdProcessing!=0) return; //can't update cache if we are still creating it

        int id = 0;
        Scanner sc = new Scanner(id_station);
        if(sc.hasNextInt()) {
            id = Integer.parseInt(id_station);
        }
        else
        {
            int id_new = getKeyByValue(id_station);
            id = id_new;
        }
        if (cache.containsKey(id)) {
            this.clear(id);
            this.put(id, this.executeQuery(id));
        } else {
            this.put(id, this.executeQuery(id));
        }

        if (cache.containsKey(id)){
            this.clear(id);
            this.put(id, this.executeQuery(id));
        } else {
            this.put(id, this.executeQuery(id));
        }

        if (updateCacheFile) {
            File file = new File(CACHEFILE);
            try {
                //Stores cache in file
                this.saveCacheFile(cache);
            } catch (Exception e) {
                //If error occurred deletes cache file
                System.err.println("Something Has Gone Badly wrong @316: cache File will be Deleted");
                e.printStackTrace();
                Logger.getLogger(JsonCache.class.getName()).log(Level.SEVERE, "Deleting Cache File after Update", e);
                file.delete();
                //throw new  IllegalArgumentException();
            }
        }
    }

    /**
     * Updates entire cache at once
     * Author: Rafael Couto - Code Corrected by Paul
     */
    synchronized public void updateAllCache() {

        if (initIdProcessing!=0) return; //can't update cache if we are still creating it

        DataBaseConnection dbc = new DataBaseConnection();
        ArrayList<Integer> all_id_station  = dbc.getAllIdStations();

        //Updates each station
        for (Integer anAll_id_station : all_id_station) {
            try {
                this.updateCache(anAll_id_station + "", false);
            } catch (StationNotFound errstn) {
                errstn.printStackTrace();
            }   catch (NumberFormatException | IOException errNumFormat) {
                System.out.println("Serious Error in the Database or Results Returned");
                errNumFormat.printStackTrace();
            }
        }

        File file = new File(CACHEFILE);
        try {
            //Stores cache in file
            this.saveCacheFile(cache);
        } catch (IOException e) {
            //If error occurred deletes cache file
            System.err.println("Something Has Gone Badly wrong @354: cache File will be Deleted");
            file.delete();
            e.printStackTrace();
        }

    }

    /**
     * Saves cache to file
     * @author Rafael Couto
     */
    private static void saveCacheFile(Map<Integer, Station> stations)
            throws IOException
    {
        try (ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(CACHEFILE))) {
            os.writeObject(stations);
        }
    }

    /**
     * Loads saved cache file to memory
     * @author Rafael Couto
     */
    private static Map<Integer, Station> readCacheFile()
            throws ClassNotFoundException, IOException
    {
        try (ObjectInputStream is = new ObjectInputStream(new FileInputStream(CACHEFILE))) {
            return (Map<Integer, Station>) is.readObject();
        }
    }

    /**
     * Updates entire cache at once
     * Author: Rafael Couto - Code Corrected by Paul
     */
    synchronized public boolean checkCacheConsistency() {

        if (initIdProcessing!=0) return false; //can't update cache if we are still creating it

        DataBaseConnection dbc = new DataBaseConnection();
        ArrayList<Integer> all_id_station  = dbc.getAllIdStations();
        boolean consistency = true;

        System.out.println("cache Size="+ this.cacheSize()+" stations in database="+all_id_station.size());
        //Updates each station
        for (Integer id: all_id_station) {
            if ( ! cache.containsKey(id) )
            {
                int theId=id;
                System.out.println("Station with id="+theId+ " is not in the database");
                consistency = false;
            }
        }
        return consistency;
    }

}
