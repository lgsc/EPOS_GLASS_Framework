package Glass;

import Configuration.DBConsts;
import Configuration.SiteConfig;
import EposTables.*;

import java.net.SocketException;
import java.sql.*;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.json.JsonArray;
import javax.json.JsonObject;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import static javax.swing.UIManager.getInt;

/*
 * DATABASE CONNECTION T0
 * This class is responsible for the EPOS database connection and provides several
 * methods for executing queries on the database.
 */
public class DataBaseT0Connection {

    @EJB
    SiteConfig siteConfig;
    private String myIP = null;      // This String will store the ip address of the machine
    private String DBConnectionString=null;

    private Statement s_node = null;            // Statement for the node queries on the EPOS database
    private ResultSet rs_node = null;           // Result Set for the node queries on the EPOS database
    private Statement s_connections = null;     // Statement for the connections queries on the EPOS database
    private ResultSet rs_connections = null;    // Result Set for the connections queries on the EPOS database
    private Statement s_failedQueries = null;   // Statement for the failed queries queries on the EPOS database
    private ResultSet rs_failedQueries = null;  // Result Set for the failed queries queries on the EPOS database

    DataBaseT0Connection() {
        this.siteConfig = lookupSiteConfigBean();
        myIP=  siteConfig.getLocalIpValue(); //"10.0.7.65";
        DBConnectionString = siteConfig.getDBConnectionString();
        DBConnectionString = siteConfig.getDBConnectionString();
    }

    // =========================================================================
    // IP & CONNECTION WITH DATABASE
    // =========================================================================
    private Connection NewConnection(Boolean AutoComit) throws SocketException, ClassNotFoundException, SQLException {

        // Create connection with the local database
        Class.forName("org.postgresql.Driver");
        Connection c = DriverManager.getConnection(DBConnectionString,DBConsts.DB_USERNAME, DBConsts.DB_PASSWORD);
        c.setAutoCommit(AutoComit);
        return c;
    }

    private Connection NewConnection() throws SocketException, ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user",DBConsts.DB_USERNAME);
        props.setProperty("password",DBConsts.DB_PASSWORD);
        Connection c = DriverManager.getConnection(DBConnectionString, props);
        c.setAutoCommit(false);
        return c;
    }

    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    // =========================================================================
    // T0 METHODS
    // =========================================================================

    /*
     * EXECUTE NODE QUERY
     * This method executes a given query and returns node's.
     */
    ArrayList<Node> executeNodeQuery(String query, ArrayList<Node> nodes,
                                     Connection c) throws SQLException
    {
        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************

        // Initialized statement for select
        s_node = c.createStatement();
        rs_node = s_node.executeQuery(query);

        // Check if there are any results
        while (rs_node.next() == true)
        {
            Node n = new Node();
            n.setId(rs_node.getInt("id"));
            n.setName(rs_node.getString("name"));
            n.setHost(rs_node.getString("host"));
            n.setPort(rs_node.getString("port"));
            n.setDbname(rs_node.getString("dbname"));
            n.setUsername(rs_node.getString("username"));
            n.setPassword("");
            n.setContact_name(rs_node.getString("contact_name"));
            n.setUrl(rs_node.getString("url"));
            n.setEmail(rs_node.getString("email"));
            // get data centers connected to this node
            ArrayList<Data_center> dataCenters =  getDatacenterByNodeID(rs_node.getInt("id"));
            n.setData_centers(dataCenters);
            nodes.add(n);
        }

        rs_node.close();
        s_node.close();

        return nodes;
    }

    /*
     * GET NODES
     * This method retuns a ArrayList with data from all nodes on the database.
     */
    ArrayList<Node> getNodes()
    {
        Connection c = null;
        // ArrayList to store the results
        ArrayList<Node> nodes = new ArrayList();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM node ORDER BY id;";

            // Execute query
            nodes = executeNodeQuery(query, nodes, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        // Return the result
        return nodes;
    }

    /*
     * GET NODE INFO
     * This method retuns the data from a node.
     */
    Node getNodeInfo(String node_name)
    {
        Connection c = null;
        // Node to store the data
        ArrayList<Node> nodes = new ArrayList();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM node WHERE name='" + node_name + "' ORDER BY id;";

            // Execute query
            nodes = executeNodeQuery(query, nodes, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return nodes.get(0);
    }

    /*
     * EXECUTE UPDATE NODE QUERY
     * This method executes a given update query.
     */
    void executeUpdateNodeQuery(String query, Connection c) throws SQLException
    {
        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************

        // Initialized statement for select
        s_node = c.createStatement();
        s_node.executeUpdate(query);

        s_node.close();
        c.commit();

        return;
    }

    int getGapNode(){
        Connection c = null;
        int val = 0;
        ResultSet rs = null;
        try {
            c = NewConnection();
            String query = "SELECT  id + 1 as val FROM node n1\n" +
                    "WHERE NOT EXISTS (\n" +
                    "    SELECT * FROM node n2\n" +
                    "    WHERE  n2.id = n1.id + 1\n" +
                    ")\n" +
                    "LIMIT 1";

            s_connections = c.createStatement();
            rs = s_connections.executeQuery(query);

            while (rs.next() == true){
                val = rs.getInt("val");
            }

        }
        catch (SQLException | ClassNotFoundException | SocketException ex){
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            try{
                s_connections.close();
            }
            catch (SQLException ex){
                Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            }

            if( c != null){
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if( rs != null){
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return val;

    }

    /*
     * DELETE NODE
     * This method deletes a T0 node with the given name.
     */
    boolean deleteNode(int id)
    {
        Connection c = null;
        PreparedStatement preparedStatement = null;

        boolean val = true;
        try{
            c = NewConnection();
            c.setAutoCommit(false);
            // Queries to be executed
            String qDelNode = "DELETE FROM node WHERE id= ? ";
            String qDelCon = "DELETE FROM connections WHERE destiny = ?"; // source node is always the same and cant be deleted
            String qDelNodeDC = "DELETE FROM node_data_center WHERE id_node = ?";

            preparedStatement = c.prepareStatement(qDelNodeDC);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

            preparedStatement = c.prepareStatement(qDelCon);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

            preparedStatement = c.prepareStatement(qDelNode);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

            c.commit();
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

        }
        return true;
    }

    /*
     * INSERT NODE
     * This method inserts a T0 node.
     */
    boolean insertNode(Node node)
    {
        Connection _c = null;
        PreparedStatement _ps = null;
        int val = 0;
        try{
            _c = NewConnection();
            _c.setAutoCommit(false);
            _ps = _c.prepareStatement("INSERT INTO node (name, host, port, dbname, username, password, email, url, contact_name ) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?);");

            _ps.setString(1, node.getName());
            _ps.setString(2, node.getHost());
            _ps.setString(3, node.getPort());
            _ps.setString(4, node.getDbname());
            _ps.setString(5, node.getUsername());
            _ps.setString(6, node.getPassword());
            _ps.setString(7, node.getEmail());
            _ps.setString(8, node.getUrl());
            _ps.setString(9, node.getContact_name());

            val = _ps.executeUpdate();
            _c.commit();

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            val = 0;
        } finally {
            if (_ps != null) {
                try {
                    _ps.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (_c != null) {
                try {
                    _c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return val == 1;
    }

    /*
     * Check if already exist a node with the same host, port, dbname
     *
     * returns true if exists, false otherwise
     *
     * */

    boolean checkHost_port_dbname(Node node)
            throws SQLException, SocketException, ClassNotFoundException {

        Connection _c = null;
        PreparedStatement _ps = null;
        ResultSet _rs = null;
        int _retId = -1;

        try {
            _c = NewConnection();
            _ps = _c.prepareStatement("SELECT * FROM node WHERE host = ? AND port = ? AND dbname = ?;");
            _ps.setString(1, node.getHost());
            _ps.setString(2, node.getPort());
            _ps.setString(3, node.getDbname());

            _rs = _ps.executeQuery();

            // Just need 1 value to confirm
            if (_rs.next()) {
                _retId = _rs.getInt("id");
            }
        } catch (SQLException _sqlException){
            System.out.println("Error " + _sqlException.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
        } finally {
            if ( _rs != null) {
                try {
                    _rs.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if ( _ps != null) {
                try {
                    _ps.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if ( _c != null) {
                try {
                    _c.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
        }
        return _retId != node.getId();
    }

    boolean checkSameName( Connection c, PreparedStatement preparedStatement , Node node)
            throws SQLException, SocketException, ClassNotFoundException {
        String query = "Select * from node where name like ?";

        c = NewConnection();
        preparedStatement = c.prepareStatement(query);
        preparedStatement.setString(1, node.getName());

        ResultSet  rscheck = preparedStatement.executeQuery();
        while (rscheck.next()) {
            return rscheck.getInt("id") != node.getId();
        }
        return false;
    }

    /*
     * UPDATE NODE
     * This method updates a T0 node.
     */
    boolean updateNode(Node node, String old_node_name)
    {
        Connection c = null;

        try{
            c = NewConnection();
            String query = "";
            // Query to be executed
            if( old_node_name.equals(" ")){
                if(node.getPassword().equals("")){
                    query = "UPDATE node SET name='" + node.getName() +
                            "', host='" + node.getHost() +
                            "', port='" + node.getPort() +
                            "', dbname='" + node.getDbname() +
                            "', username='" + node.getUsername() +
                            "', email='" + node.getEmail() +
                            "', url='" + node.getUrl() +
                            "', contact_name='" + node.getContact_name() +
                            "' WHERE id='" + node.getId() + "';";
                }
                else {
                    query = "UPDATE node SET name='" + node.getName() +
                            "', host='" + node.getHost() +
                            "', port='" + node.getPort() +
                            "', dbname='" + node.getDbname() +
                            "', username='" + node.getUsername() +
                            "', password='" + node.getPassword() +
                            "', email='" + node.getEmail() +
                            "', url='" + node.getUrl() +
                            "', contact_name='" + node.getContact_name() +
                            "' WHERE id='" + node.getId() + "';";
                }
            }
            else{
                query = "UPDATE node SET name='" + node.getName() + "', host='" + node.getHost() +
                        "', port='" + node.getPort() + "', dbname='" + node.getDbname() + "', username='" + node.getUsername() +
                        "', password='" + node.getPassword() + "' WHERE name='" + old_node_name + "';";
            }


            // Execute query
            executeUpdateNodeQuery(query, c);
            return true;
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }


    int getGapConnections(){
        Connection c = null;
        int val = 0;
        ResultSet rs = null;
        try {
            c = NewConnection();
            String query = "SELECT  id + 1 as val FROM connections c1\n" +
                    "WHERE NOT EXISTS (\n" +
                    "    SELECT * FROM connections c2\n" +
                    "    WHERE  c2.id = c1.id + 1\n" +
                    ")\n" +
                    "LIMIT 1";

            s_connections = c.createStatement();
            rs = s_connections.executeQuery(query);

            while (rs.next()){
                val = rs.getInt("val");
            }

        }
        catch (SQLException | ClassNotFoundException | SocketException ex){
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            try{
                s_connections.close();
            }
            catch (SQLException ex){
                Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            }

            if( c != null){
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if( rs != null){
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return val;

    }

    int getGapDataCenter(){
        Connection c = null;
        int val = 0;
        ResultSet rs = null;
        try {
            c = NewConnection();
            String query = "SELECT  id + 1 as val FROM data_center d1\n" +
                    "       WHERE NOT EXISTS (\n" +
                    "           SELECT * FROM data_center d2\n" +
                    "           WHERE  d2.id = d1.id + 1\n" +
                    "       )\n" +
                    "       LIMIT 1";

            s_connections = c.createStatement();
            rs = s_connections.executeQuery(query);

            while (rs.next() == true){
                val = rs.getInt("val");
            }

        }
        catch (SQLException | ClassNotFoundException | SocketException ex){
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            try{
                s_connections.close();
            }
            catch (SQLException ex){
                Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            }

            if( c != null){
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if( rs != null){
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return val;

    }

    /*
     * INSERT CONNECTION
     * This method inserts a T0 connection.
     */
    boolean insertConnection(Connections connections, ArrayList<Station> stationArray)
    {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        int val;
        try{
            c = NewConnection();
            String query = "INSERT INTO connections (source, destiny, station ,metadata) VALUES(?, ?, ?, ?)";
            preparedStatement = c.prepareStatement(query);
            preparedStatement.setInt(1, connections.getSource().getId());
            preparedStatement.setInt(2, connections.getDestiny().getId());
            preparedStatement.setString(4, connections.getMetadata());
                for (Station s: stationArray){
                preparedStatement.setInt(3, s.getId());
                preparedStatement.executeUpdate();
            }
            c.commit();
            val = 1;
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            val = 0;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return val != 0;

    }

    /*
     * INSERT Data Center
     * This method inserts a T0 Data Center.
     */
    private int insertDataCenter(Data_center dc, Connection c, PreparedStatement preparedStatement, ResultSet resultSet) throws SQLException  {

        String query = "INSERT INTO data_center (acronym, hostname, root_path, name, protocol, id_agency) VALUES (?, ?, ?, ?, ?, ?) ";

        preparedStatement = c.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, dc.getAcronym());
        preparedStatement.setString(2, dc.getHostname());
        preparedStatement.setString(3, dc.getRoot_path());
        preparedStatement.setString(4, dc.getName());
        preparedStatement.setString(5, dc.getProtocol());
        preparedStatement.setInt(6, dc.getAgency().getId());
        int affRows = preparedStatement.executeUpdate();

        if (affRows > 0) {
            ResultSet rs = preparedStatement.getGeneratedKeys();
            if (rs.next()){
                return rs.getInt(1);  // returns ids after being "inserted"
            }
            else
                return -1;
        }
        else
            return -1;

    }

    /*
     * EXECUTE CONNECTIONS QUERY
     * This method executes a given query and returns connections.
     */
    ArrayList executeConnectionsQuery(String query, ArrayList<Connections> connections,
                                      Connection c) throws SQLException
    {
        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************

        // Initialized statement for select
        s_connections = c.createStatement();
        rs_connections = s_connections.executeQuery(query);
        DataBaseConnection dbct1 = new DataBaseConnection();

        // Check if there are any results
        while (rs_connections.next() == true)
        {
            Connections cs = new Connections();
            cs.setId(rs_connections.getInt("id"));

            // Get source node info
            ArrayList<Node> source = new ArrayList();
            String sourceQuery = "SELECT * FROM node WHERE id=" + rs_connections.getInt("source") + ";";
            source = executeNodeQuery(sourceQuery, source, c);
            cs.setSource(source.get(0));

            // Get destiny node info
            ArrayList<Node> destiny = new ArrayList();
            String destinyQuery = "SELECT * FROM node WHERE id=" + rs_connections.getInt("destiny") + ";";
            destiny = executeNodeQuery(destinyQuery, destiny, c);
            cs.setDestiny(destiny.get(0));

            // Get station reference
            // Fabio pereira on 14/12/2017 not necessary all this info, only id
            //ArrayList<Station> station = new ArrayList();
//            String stationQuery = "SELECT * FROM station WHERE id=" + rs_connections.getInt("station") + ";";
//            station = dbct1.executeStationQuery(stationQuery, c);

            ArrayList<Station> station;
            if( rs_connections.getInt(("station")) != 0 ) {
                String stationQuery = "Select id, name, markerlongname(marker, monument_num, receiver_num, country_code  ) from station where id= ?";
                station = dbct1.getStationsName(stationQuery, c, rs_connections.getInt("station"));
                cs.setStation(station.get(0));
            }
            else {
                station = new ArrayList<>();
                Station st = new Station();
                station.add(st);
                cs.setStation(station.get(0));
            }


            // Set metadata type
            cs.setMetadata(rs_connections.getString("metadata"));

            connections.add(cs);
        }

        rs_connections.close();
        s_connections.close();

        return connections;
    }

    /*
     * GET CONNECTIONS
     * This method retuns a ArrayList with data from all connections on the database.
     */
    ArrayList<Connections> getConnections()
    {
        Connection c = null;
        // ArrayList to store the results
        ArrayList<Connections> connections = new ArrayList();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM connections ORDER BY id;";

            // Execute query
            connections = executeConnectionsQuery(query, connections, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return connections;
    }

    /*
     * UPDATE CONNECTION
     * This method updates a T0 connection.
     */
    boolean updateConnection(Connections cs, int connection_id)
    {
        Connection c = null;
        PreparedStatement preparedStatement = null;
        try{
            c = NewConnection();
            String query = ("UPDATE connections SET source = ?, destiny = ?, metadata = ?");
            // Query to be executed
            if( cs.getStation().getId() != 0){
                query = query + ", station = ?";
            }
            query = query + " WHERE id = ?";
            /*String query = "UPDATE connections SET source=" + cs.getSource().getId() +
                    ", destiny=" + cs.getDestiny().getId() +
                    ", station=" + cs.getStation().getId() +
                    ", metadata='" + cs.getMetadata() +
                    "' WHERE id=" + connection_id + ";";*/

            // Execute query
            // executeUpdateConnectionQuery(query.toString(), c);
            preparedStatement = c.prepareStatement(query);
            preparedStatement.setInt(1, cs.getSource().getId());
            preparedStatement.setInt(2, cs.getDestiny().getId());
            preparedStatement.setString(3, cs.getMetadata());
            if( cs.getStation().getId() != 0 ) {
                preparedStatement.setInt(4, cs.getStation().getId());
                preparedStatement.setInt(5, connection_id);

            }
            else {
                preparedStatement.setInt(4, connection_id);
            }
            preparedStatement.executeUpdate();
            c.commit();
            return true;
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    /*
     * EXECUTE UPDATE CONNECTION QUERY
     * This method executes a given update query.
     */
    void executeUpdateConnectionQuery(String query, Connection c) throws SQLException
    {

        s_connections = c.createStatement();
        s_connections.executeUpdate(query);
        s_connections.close();
        c.commit();

        return;
    }

    /*
     * DELETE CONNECTION
     * This method deletes a T0 connetion with the given id.
     */
    boolean deleteConnection(int connection_id)
    {
        Connection c = null;

        try{
            c = NewConnection();

            // Query to be executed
            String query = "DELETE FROM connections WHERE id=" + connection_id + ";";

            // Execute query
            executeUpdateConnectionQuery(query, c);
            return true;
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    /*
     * GET FAILED QUERIES
     * This method retuns a ArrayList with data from all failed queries on the database.
     */
    ArrayList<Failed_queries> getFailedQueries()
    {
        Connection c = null;
        // ArrayList to store the results
        ArrayList<Failed_queries> failed_queries = new ArrayList<>();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "select * from failed_queries f\n" +
                    "inner join node n on f.destiny = n.id " +
                    "order by f.id";

            // Execute query
            failed_queries = executeFailedQueriesQuery(query, failed_queries, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        // Return the result
        return failed_queries;
    }

    ArrayList<Queries> getQueueQueries()
    {
        Connection c = null;
        // ArrayList to store the results
        ArrayList<Queries> queriesArrayList = new ArrayList<>();
        ResultSet rs = null;
        Statement statement = null;

        try{
            c = NewConnection();
            // Query to be executed
            String query = "select q.id, q.query, q.metadata, q.destiny as nDid, n.name as nName, s.id as sid, s.name, s.marker from queries q\n" +
                    "                    left join station s on q.station_id = s.id\n" +
                    "                    left join node n on n.id = q.destiny\n" +
                    "                    order by q.id";
            // Execute query
            statement = c.createStatement();
            rs = statement.executeQuery(query);
            while (rs.next()) {
                Station s = new Station();
                if(rs.getString("sid") != null){
                    s.setId(rs.getInt("sid"));
                    s.setName(rs.getString("name"));
                    s.setMarker(rs.getString("marker"));
                }
                Node n = new Node();
                if(rs.getString("ndid") != null){
                    n.setId(rs.getInt("ndid"));
                    n.setName(rs.getString("nName"));
                }
                Queries q = new Queries(rs.getInt(1), rs.getString(2), rs.getString(3), s, n);

                queriesArrayList.add(q);

            }


        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (statement != null) {
                try {
                    statement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        // Return the result
        return queriesArrayList;
    }

    ArrayList<Station> getStations()
    {
        Connection c = null;
        // ArrayList to store the results
        ArrayList<Station> stations = new ArrayList<>();
        DataBaseConnection dbct1 = new DataBaseConnection();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT id, name, markerlongname(marker, monument_num, receiver_num, country_code  ) FROM station ORDER BY id;";

            // Execute query
            stations = dbct1.getStationsName(query, c, 0);


        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        // Return the result
        return stations;
    }

    ArrayList<Station> getStationsMarker(){

        Connection c = null;
        ResultSet rs = null;
        Statement statement_station = null;
        String query = "select markerlongname(marker, monument_num, receiver_num, country_code  ) from station";
        ArrayList<Station> ret = new ArrayList<>();

        try{
            c = NewConnection();
            statement_station = c.createStatement();
            rs = statement_station.executeQuery(query);

            while (rs.next()){
                Station s = new Station();
                s.setMarkerLongName(rs.getString("markerlongname"));
                ret.add(s);
            }

        } catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(statement_station != null){
                try {
                    statement_station.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    /*
     * EXECUTE FAILED QUERIES QUERY
     * This method executes a given query and returns the failed queries.
     */
    private ArrayList<Failed_queries> executeFailedQueriesQuery(String query, ArrayList<Failed_queries> failed_queries,
                                                                Connection c) throws SQLException
    {
        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************

        // Initialized statement for select
        s_failedQueries = c.createStatement();
        rs_failedQueries = s_failedQueries.executeQuery(query);
        DataBaseConnection dbct1 = new DataBaseConnection();

        // Check if there are any results
        while (rs_failedQueries.next())
        {
            Failed_queries fq = new Failed_queries();
            fq.setId(rs_failedQueries.getInt("id"));
            fq.setQuery(rs_failedQueries.getString("query"));

            // Get destiny node info only if destiny is not null
            if (rs_failedQueries.getString("destiny") != null) {
                ArrayList<Node> destiny = new ArrayList<>();
                String destinyQuery = "SELECT * FROM node WHERE id=" + rs_failedQueries.getInt("destiny") + ";";
                destiny = executeNodeQuery(destinyQuery, destiny, c);
                fq.setDestiny(destiny.get(0));
            }

            fq.setTimeStamp(rs_failedQueries.getTimestamp("timestamp"));
            fq.setReason(rs_failedQueries.getString("reason"));

            failed_queries.add(fq);
        }

        rs_failedQueries.close();
        s_failedQueries.close();

        return failed_queries;
    }

    ArrayList<Station> getStationsDataCenter(int id){
        Connection c = null;
        String query = "select s1.id, s1.name, markerlongname(marker,monument_num,receiver_num, country_code) from station s1\n" +
                "INNER JOIN datacenter_station c2 ON s1.id = c2.id_station\n" +
                "WHERE c2.id_datacenter = ?\n";
        PreparedStatement ps = null;
        ArrayList<Station> ret = new ArrayList<>();
        ResultSet rs = null;
        try{
            c = NewConnection();
            ps = c.prepareStatement(query);
            ps.setInt(1,id);
            rs = ps.executeQuery();
            while ( rs.next() ) {
                Station station = new Station();
                station.setId(rs.getInt("id"));
                station.setName(rs.getString("name"));
                station.setMarkerLongName(rs.getString("markerlongname"));
                ret.add(station);
            }
        }
        catch (SQLException | SocketException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if ( c != null ){
                try{
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if ( rs != null ){
                try{
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if ( ps != null ){
                try{
                    ps.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return ret;
    }

    ArrayList<Data_center> getDataCenter(){
        Connection c = null;
        ResultSet rs = null;
        Statement statement_dc = null;
        ArrayList<Data_center> ret = new ArrayList<>();
        String query = "select id, name, acronym from data_center";

        try{
            c = NewConnection();
            statement_dc = c.createStatement();
            rs = statement_dc.executeQuery(query);

            while ( rs.next()){
                Data_center dc = new Data_center();
                dc.setId(rs.getInt("id"));
                dc.setName(rs.getString("name"));
                dc.setAcronym(rs.getString("acronym"));
                ret.add(dc);
            }
        }
        catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(statement_dc != null){
                try {
                    statement_dc.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


        return ret;
    }

    ArrayList<Data_center> getDataCenterbyAcronym(String acronym){
        Connection c = null;
        ResultSet rs = null;
        PreparedStatement statement_dc = null;
        ArrayList<Data_center> ret = new ArrayList<>();
        String query = "select id, name, acronym from data_center where acronym = ?";

        try{
            c = NewConnection();
            statement_dc = c.prepareStatement(query);
            statement_dc.setString(1,acronym);
            rs = statement_dc.executeQuery();

            while ( rs.next()){
                Data_center dc = new Data_center();
                dc.setId(rs.getInt("id"));
                dc.setName(rs.getString("name"));
                dc.setAcronym(rs.getString("acronym"));
                ret.add(dc);
            }
        }
        catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(statement_dc != null){
                try {
                    statement_dc.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


        return ret;
    }

    ArrayList<Data_center> getDatacenterByNodeID(int nodeID){

        Connection c = null;
        ArrayList<Data_center> dataCenters  = new ArrayList<>();
        String query = "Select dc.id, name from data_center dc " +
                "INNER JOIN  node_data_center ndc ON dc.id = ndc.id_data_center " +
                "WHERE  ndc.id_node = ?";
        Statement statement = null;
        ResultSet resultSet = null;
        PreparedStatement preparedStatement = null;

        try{
            c = NewConnection();
            preparedStatement = c.prepareStatement(query);
            preparedStatement.setInt(1, nodeID);
            resultSet = preparedStatement.executeQuery();

            while (resultSet.next()){
                Data_center dc = new Data_center();
                dc.setId(resultSet.getInt("id"));
                dc.setName(resultSet.getString("name"));
                dataCenters.add(dc);
            }

        }
        catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(resultSet != null){
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return dataCenters;


    }

    int getGapDataCenterStation() {
        Connection c = null;
        int val = 0;
        ResultSet rs = null;
        try {
            c = NewConnection();
            String query = "SELECT  id + 1 as val FROM datacenter_station c1\n" +
                    "WHERE NOT EXISTS (\n" +
                    "    SELECT * FROM datacenter_station c2\n" +
                    "    WHERE  c2.id = c1.id + 1\n" +
                    ")\n" +
                    "LIMIT 1";

            s_connections = c.createStatement();
            rs = s_connections.executeQuery(query);

            while (rs.next() == true){
                val = rs.getInt("val");
            }

        }
        catch (SQLException | ClassNotFoundException | SocketException ex){
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally {
            try{
                s_connections.close();
            }
            catch (SQLException ex){
                Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            }

            if( c != null){
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if( rs != null){
                try {
                    rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        return val;
    }

    boolean deleteDataCenter(int id) {
        // transaction
        // first delete associations with node
        // second delete associations with structure
        // third delete DataCenter
        Connection c = null;
        PreparedStatement preparedStatement = null;

        String deleteDCNode = "DELETE from node_data_center where id_data_center = ?";
        String deleteDCStruct = "DELETE from data_center_structure where id_data_center = ?";
        String deleteDC = "DELETE from data_center where id = ?";
        boolean ok = true;
        try {
            c = NewConnection();
            c.setAutoCommit(false);

            preparedStatement = c.prepareStatement(deleteDCNode);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

            preparedStatement = c.prepareStatement(deleteDCStruct);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

            preparedStatement = c.prepareStatement(deleteDC);
            preparedStatement.setInt(1, id);
            preparedStatement.executeUpdate();

            c.commit();
            return true;

        }

        catch (SQLException | ClassNotFoundException | SocketException e) {
            e.printStackTrace();
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, "error deleting data center", e);
            return false;
        }
        finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

    }

    boolean insertDataCenterStation(Datacenter_station dc) {

        Connection c = null;
        PreparedStatement preparedStatement = null;
        int val = 0;
        try{
            c = NewConnection();
            int stationID = dc.getStation().getId();
            if ( stationID == -1 ){
                String query = "INSERT INTO datacenter_station (id, id_datacenter, datacenter_type, markerlongname) VALUES (?, ?, ?, ?) ";
                preparedStatement = c.prepareStatement(query);
                preparedStatement.setInt(1, dc.getId());
                preparedStatement.setInt(2, dc.getDatacenter().getId());
                preparedStatement.setString(3, dc.getDatacenter_type());
                preparedStatement.setString(4, dc.getMarkerLongName());
                val = preparedStatement.executeUpdate();
                c.commit();
            }
            else {
                String query = "INSERT INTO datacenter_station (id, id_station, id_datacenter, datacenter_type, markerlongname) VALUES (?, ?, ?, ?, ?) ";
                preparedStatement = c.prepareStatement(query);
                preparedStatement.setInt(1, dc.getId());
                preparedStatement.setInt(2, dc.getStation().getId());
                preparedStatement.setInt(3, dc.getDatacenter().getId());
                preparedStatement.setString(4, dc.getDatacenter_type());
                preparedStatement.setString(5, dc.getMarkerLongName());
                val = preparedStatement.executeUpdate();
                c.commit();
            }
        } catch (SocketException | SQLException | ClassNotFoundException e ) {
            e.printStackTrace();
            System.out.println("Error "+ e.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, e);
            val = 0;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if ( preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            return val == 1;
        }


    }

    void insertNodeDataCenter(int[] values, Connection c, PreparedStatement preparedStatement) throws SQLException {
        String query =  "INSERT INTO node_data_center VALUES(?, ?)";
        preparedStatement = c.prepareStatement(query);
        preparedStatement.setInt(1, values[0]);
        preparedStatement.setInt(2, values[1]);
        preparedStatement.executeUpdate();
    }

    ArrayList<String[]> getNodeDC(){
        Connection c = null;
        ResultSet rs = null;
        Statement statement_dc = null;
        ArrayList<String[]> ret = new ArrayList<>();
        String query = "select ndc.id_node ,n.name, ndc.id_data_center ,c2.name as nameDC  from node_data_center ndc" +
                " INNER JOIN node n ON ndc.id_node = n.id" +
                " INNER JOIN data_center c2 ON ndc.id_data_center = c2.id";

        try{
            c = NewConnection();
            statement_dc = c.createStatement();
            rs = statement_dc.executeQuery(query);

            while (rs.next()){
                String[] vals = new String[4];
                vals[0] = String.valueOf(rs.getInt("id_node"));
                vals[1] = rs.getString("name");
                vals[2] = String.valueOf(rs.getInt("id_data_center"));
                vals[3] = rs.getString("namedc");
                ret.add(vals);


            }
        }
        catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(statement_dc != null){
                try {
                    statement_dc.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


        return ret;

    }

    Data_center_structure getDatacenterByID(int id){
        Connection c = null;
        PreparedStatement preparedStatement = null;
        String query = "select dc.id, dc.name, dc.acronym, dc.hostname, dc.protocol, dc.root_path, dc.id_agency, " +
                "a.name as AgencyName, dcs.directory_naming, ft.id ,ft.format, ft.sampling_frequency, " +
                "ft.sampling_window, dcS.id as dcSID\n" +
                "from data_center dc\n" +
                "left join data_center_structure dcS on dc.id = dcS.id_data_center\n" +
                "left join file_type ft on dcS.id_file_type = ft.id\n" +
                "left join agency a on dc.id_agency = a.id\n" +
                "where dc.id = ?";
        String query2 = "SELECT * from node_data_center\n" +
                "inner join node n on n.id = node_data_center.id_node\n" +
                "where id_data_center = ?";
        ResultSet rs = null;

        Data_center_structure dataCenterStructure = null;

        try {
            c = NewConnection();
            preparedStatement = c.prepareStatement(query, ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
            preparedStatement.setInt(1, id);
            rs = preparedStatement.executeQuery();
            if (rs.next()) {
                Agency a = new Agency();
                a.setId(rs.getInt(7));
                a.setName(rs.getString(8));

                Data_center dc = new Data_center();
                dc.setId(rs.getInt(1));
                dc.setName(rs.getString(2));
                dc.setAcronym(rs.getString(3));
                dc.setHostname(rs.getString(4));
                dc.setProtocol(rs.getString(5));
                dc.setRoot_path(rs.getString(6));
                dc.setAgency(a);


                ArrayList<DirStruct> dirStructArrayList = new ArrayList<>();
                if (rs.getString(9) != null ){
                    rs.beforeFirst();
                    while (rs.next()) {
                        File_type fileType = new File_type();
                        fileType.setId(rs.getInt(10));
                        fileType.setFormat(rs.getString(11));
                        fileType.setSampling_frequency(rs.getString(13));
                        fileType.setSampling_window(rs.getString(12));
                        DirStruct dirStruct = new DirStruct();
                        dirStruct.dirname = rs.getString(9);
                        dirStruct.fileType = fileType;
                        dirStruct.id = rs.getInt(14);
                        dirStructArrayList.add(dirStruct);
                    }
                }


                preparedStatement = c.prepareStatement(query2);
                preparedStatement.setInt(1, id);
                rs = preparedStatement.executeQuery();
                ArrayList<Node> nodeArrayList = new ArrayList<>();

                while (rs.next()) {
                    Node n = new Node();
                    n.setId(rs.getInt(1));
                    n.setName(rs.getString(4));
                    nodeArrayList.add(n);
                }

                dataCenterStructure = new Data_center_structure(dc, dirStructArrayList, nodeArrayList);
            }


        } catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(preparedStatement != null){
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return dataCenterStructure;

    }


    boolean updateDataCenter(Data_center dc, JsonArray dirStruct, JsonArray nodes){
        Connection c = null;
        PreparedStatement preparedStatement = null;
        PreparedStatement pst = null;
        ResultSet rs = null;
        String query = "UPDATE data_center set acronym = ?, hostname = ?, root_path = ?, name = ?, protocol = ?, id_agency = ? WHERE  id = ?";
        int val;
        boolean checkInsert = false;
        try {
            c = NewConnection();
            c.setAutoCommit(false);
            preparedStatement = c.prepareStatement(query);
            preparedStatement.setString(1, dc.getAcronym());
            preparedStatement.setString(2, dc.getHostname());
            preparedStatement.setString(3, dc.getRoot_path());
            preparedStatement.setString(4, dc.getName());
            preparedStatement.setString(5, dc.getProtocol());
            preparedStatement.setInt(6, dc.getAgency().getId());
            preparedStatement.setInt(7, dc.getId());
            val = preparedStatement.executeUpdate();
            if (val != 0) {
                // delete node data center if was also deleted in NM
                String getNodesDC = "SELECT * FROM node_data_center where id_data_center = ?";
                preparedStatement = c.prepareStatement(getNodesDC);
                preparedStatement.setInt(1, dc.getId());
                rs = preparedStatement.executeQuery();
                while (rs.next()){
                    int idNDB = rs.getInt(1); // value in DB
                    for (int i = 0; i < nodes.size(); i++) {
                        int idNNM = nodes.getInt(i); // value from NodeManager
                        if (idNDB == idNNM) {
                            break;
                        }
                        if (i == (nodes.size() - 1)){
                            String removeQuery = "DELETE FROM node_data_center where id_data_center = ? and id_node = ?";
                            pst = c.prepareStatement(removeQuery);
                            pst.setInt(1, dc.getId());
                            pst.setInt(2, idNDB);
                            pst.executeUpdate();
                        }
                    }
                }

                // associate node with this data center
                for (int i = 0; i < nodes.size() ; i++) {
                    String check = "SELECT * from node_data_center where id_data_center = ? and id_node = ?";
                    preparedStatement = c.prepareStatement(check);
                    preparedStatement.setInt(1, dc.getId());
                    preparedStatement.setInt(2, nodes.getInt(i));
                    rs = preparedStatement.executeQuery();

                    if (!rs.next()) {
                        int[] values = new int[]{nodes.getInt(i), dc.getId()};
                        insertNodeDataCenter(values, c, preparedStatement);
                    }
                }

                // get associations for this dataCenter
                String getDCStruct = "SELECT * FROM data_center_structure where id_data_center = ?";
                preparedStatement = c.prepareStatement(getDCStruct);
                preparedStatement.setInt(1, dc.getId());
                rs = preparedStatement.executeQuery();
                while (rs.next()) {
                    int id = rs.getInt("id");
                    for (int i = 0; i < dirStruct.size() ; i++) {
                        int dirID = dirStruct.getJsonObject(i).getInt("_id");
                        if (id == dirID){
                            break;
                        }
                        // remove this association if it was also deleted on NM
                        if (i == (dirStruct.size() - 1)){
                            String removeQuery = "DELETE FROM data_center_structure where id = ? ";
                            pst = c.prepareStatement(removeQuery);
                            pst.setInt(1, id);
                            pst.executeUpdate();
                        }


                    }
                }

                for (int i = 0; i < dirStruct.size() ; i++) {
                    String dirname = dirStruct.getJsonObject(i).getString("_directoryNaming");
                    int dirID = dirStruct.getJsonObject(i).getInt("_id");
                     // associate datacenter with dir Struct
                    JsonObject fileTypejJsonObject = dirStruct.getJsonObject(i).getJsonObject("_fileType");
                    int fileID = fileTypejJsonObject.getInt("_id");
                    String format = fileTypejJsonObject.getString("_format");
                    String samplingWindow = fileTypejJsonObject.getString("_samplingWindow");
                    String samplingFrequency = fileTypejJsonObject.getString("_samplingFrequency");
                    File_type fileType = new File_type(fileID, format, samplingWindow, samplingFrequency);
                    int newFileID;
                    if (fileID == -1) {
                        //create new file type
                        newFileID = insertFileType(fileType, c, preparedStatement, rs);
                        if (newFileID != -1) {
                            checkInsert = false;
                        }
                        else {
                            checkInsert = true;
                            break;
                        }
                    }
                    else {
                        newFileID = fileID;
                    }
                    if ( dirID == -1) { // new data center
                        Data_center_structure dataCenterStructure = new Data_center_structure(0, newFileID, dirname, "");
                        dataCenterStructure.setDataCenter(dc);
                        checkInsert = insertDataCenterStruct(dataCenterStructure, c, preparedStatement);
                        if (checkInsert) { // failed
                            break;
                        }
                    }
                    else{ // update dir Struct
                        String up_dcStruct = "UPDATE data_center_structure set id_file_type = ?, directory_naming = ? WHERE id = ?";
                        pst = c.prepareStatement(up_dcStruct);
                        pst.setInt(1, newFileID);
                        pst.setString(2, dirname);
                        pst.setInt(3, dirID);
                        pst.executeUpdate();
                    }

                }


                if (checkInsert) {
                    c.rollback();
                }
                else {
                    c.commit();
                }
            }
            else {
                c.rollback();
            }
            return true;
        } catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            return false;
        }
        finally {
            if( c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if( preparedStatement != null ){
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (pst != null) {
                try {
                    pst.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    ArrayList<Agency> getAllAgencies () {
        Connection c = null;
        ResultSet rs = null;
        Statement statement_dc = null;
        ArrayList<Agency> agencyArrayList = new ArrayList<>();
        String query = "select id, name, abbreviation from agency";

        try{
            c = NewConnection();
            statement_dc = c.createStatement();
            rs = statement_dc.executeQuery(query);

            while (rs.next()){
                Agency a = new Agency();
                a.setName(rs.getString("name"));
                a.setId(rs.getInt("id"));
                if (rs.getString("abbreviation") == null)
                    a.setAbbreviation("");
                else
                    a.setAbbreviation(rs.getString("abbreviation"));
                agencyArrayList.add(a);
            }

        }
        catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(statement_dc != null){
                try {
                    statement_dc.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return agencyArrayList;
    }

    ArrayList<File_type> getFileTypes(){
        ArrayList<File_type> fileTypeArrayList = new ArrayList<>();
        Connection c = null;
        ResultSet rs = null;
        Statement statement_dc = null;
        String query = "select * from file_type";

        try{
            c = NewConnection();
            statement_dc = c.createStatement();
            rs = statement_dc.executeQuery(query);

            while (rs.next()){
                File_type fileType = new File_type(rs.getInt("id"), rs.getString("format"), rs.getString("sampling_window"), rs.getString("sampling_frequency"));
                fileTypeArrayList.add(fileType);
            }

        }
        catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        finally {
            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(statement_dc != null){
                try {
                    statement_dc.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return fileTypeArrayList;
    }

    /*
     * this method creates a new datacenter and also creates a data center structure(dirStruct) to make the
     * connection between data center and file type
     *
     * Receives as input, a datacenter object and a JsonArray which contains the dirStruct
     * */
    boolean newDataCenter(Data_center dc, JsonArray dirStruct, JsonArray nodes){

        Connection c = null;
        PreparedStatement preparedStatement = null;
        Statement statement = null;
        ResultSet resultSet = null;
        boolean check =  false; // true -> fail; false -> accept

        try {
            c = NewConnection();
            c.setAutoCommit(false);
            int newID = insertDataCenter(dc, c, preparedStatement, resultSet);
            if (newID != -1) {
                dc.setId(newID);
                // associate node with this data center
                for (int i = 0; i < nodes.size(); i++) {
                    int id = nodes.getInt(i);
                    int[] values = new int[]{id, newID};
                    insertNodeDataCenter(values, c, preparedStatement);
                }

                // new DataCenter Structure
                for (int i = 0; i < dirStruct.size() ; i++) {
                    String dirname = dirStruct.getJsonObject(i).getString("_directoryNaming");
                    JsonObject fileTypejJsonObject  = dirStruct.getJsonObject(i).getJsonObject("_fileType");
                    int fileID =  fileTypejJsonObject.getInt("_id");
                    String format  = fileTypejJsonObject.getString("_format");
                    String samplingWindow  = fileTypejJsonObject.getString("_samplingWindow");
                    String samplingFrequency  = fileTypejJsonObject.getString("_samplingFrequency");
                    File_type fileType = new File_type(fileID, format, samplingWindow, samplingFrequency);
                    int newFileID;
                    if (fileID == -1) {
                        //create new file type
                        newFileID = insertFileType(fileType, c, preparedStatement, resultSet);
                        if (newFileID != -1) {
                            check = false;
                        }
                        else {
                            check = true;
                            break;
                        }
                    }
                    else{
                        newFileID = fileID;
                    }
                    Data_center_structure dataCenterStructure = new Data_center_structure(0, newFileID, dirname, "");
                    dataCenterStructure.setDataCenter(dc);
                    check = insertDataCenterStruct(dataCenterStructure, c, preparedStatement);
                    if (check) { // failed
                        break;
                    }
                }
                if (check) {
                    c.rollback();
                }
                else {
                    c.commit();
                }
            }
            else {
                c.rollback();
            }
        }
        catch (SocketException | SQLException | ClassNotFoundException e1) {
            if (c != null){
                try {
                    Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", e1);
                    c.rollback();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

        }
        finally {
            if (c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if ( preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if (resultSet != null) {
                try {
                    resultSet.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }

            // return message error to web page
            return check;
        }


    }

    private int  insertFileType(File_type fileType, Connection c, PreparedStatement preparedStatement,
                                ResultSet resultSet) throws SQLException {
        String queryInsert = "INSERT INTO file_type(format, sampling_window, sampling_frequency) VALUES (?, ?, ?)";

        preparedStatement = c.prepareStatement(queryInsert, Statement.RETURN_GENERATED_KEYS);
        preparedStatement.setString(1, fileType.getFormat());
        preparedStatement.setString(2, fileType.getSampling_window());
        preparedStatement.setString(3, fileType.getSampling_frequency());
        int rows = preparedStatement.executeUpdate();

        if (rows > 0) {
            resultSet = preparedStatement.getGeneratedKeys();
            if (resultSet.next())
                return resultSet.getInt("id");
            else
                return -1;
        }
        else
            return -1;
    }

    boolean insertDataCenterStruct(Data_center_structure dataCenterStructure, Connection c,
                                   PreparedStatement preparedStatement) throws SQLException {

        // check if doesnt already exists a entry for this dc id, file id and dirName
        String queryCheck = "SELECT * FROM data_center_structure WHERE id_data_center = ? AND id_file_type = ? " +
                "AND directory_naming like ?";

        preparedStatement = c.prepareStatement(queryCheck);
        preparedStatement.setInt(1, dataCenterStructure.getDataCenter().getId());
        preparedStatement.setInt(2, dataCenterStructure.getId_file_type());
        preparedStatement.setString(3, dataCenterStructure.getDirectory_naming());
        ResultSet resultSet = preparedStatement.executeQuery();
        if (resultSet.next())
            return true;

        String queryInsert = "INSERT INTO data_center_structure(id_data_center, id_file_type, directory_naming) " +
                "VALUES (?, ?, ?)";

        preparedStatement = c.prepareStatement(queryInsert);
        preparedStatement.setInt(1, dataCenterStructure.getDataCenter().getId());
        preparedStatement.setInt(2, dataCenterStructure.getId_file_type());
        preparedStatement.setString(3, dataCenterStructure.getDirectory_naming());
        int rows = preparedStatement.executeUpdate();
        return rows <= 0;

    }

    protected LinkedHashMap<Data_center, ArrayList<Station>> getStationsForDataCenter() {
        String query = "SELECT  dc.id as dcID, dc.acronym, dc.hostname, dc.root_path, dc.name as dcName, " +
                "dc.protocol, s2.id as sID,\n" +
                "  s2.name as sName, s2.marker as smarker\n" +
                "from datacenter_station dcSt\n" +
                "inner join station s2 on dcSt.id_station = s2.id\n" +
                "right join data_center dc on dcSt.id_datacenter = dc.id\n" +
                "order by dcID ASC";

        Statement statement = null;
        Connection c = null;
        ResultSet rs = null;
        LinkedHashMap<Data_center, ArrayList<Station>> listHashMap = new LinkedHashMap<>();

        try {
            c = NewConnection();

            statement = c.createStatement();
            rs = statement.executeQuery(query);
            while (rs.next()){
                Data_center dataCenter = new Data_center(rs.getInt("dcid"), rs.getString("dcname"),
                        rs.getString("root_path"), rs.getString("acronym"), rs.getString("hostname"),
                        rs.getString("protocol"));

                if ( rs.getInt("sid") != 0){
                    Station station = new Station();
                    station.setId(rs.getInt("sid"));
                    station.setName(rs.getString("sname"));
                    station.setMarker(rs.getString("smarker"));

                    if (listHashMap.containsKey(dataCenter)){
                        ArrayList<Station> aSt = listHashMap.get(dataCenter);
                        aSt.add(station);
                        listHashMap.replace(dataCenter, aSt);
                    }
                    else{
                        // create array list
                        // add station to list and add
                        ArrayList<Station> stations = new ArrayList<>();
                        stations.add(station);
                        listHashMap.put(dataCenter, stations);
                    }
                }
                else {
                    if( !listHashMap.containsKey(dataCenter)){
                        ArrayList<Station> stations = new ArrayList<>();
                        listHashMap.put(dataCenter, stations);
                    }
                }
            }
        }
        catch (SocketException | SQLException | ClassNotFoundException e) {
            e.printStackTrace();
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "getStationsforDataCenter", e);
        }
        finally {
            if(c != null){
                try {
                    c.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(rs != null){
                try {
                    rs.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
            if(statement != null){
                try {
                    statement.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }

        return listHashMap;
    }


    boolean deleteQuery(int id) {

        Connection c = null;
        PreparedStatement preparedStatement = null;
        String query = "DELETE FROM failed_queries f WHERE f.id = ?";

        try{
            c = NewConnection();

            // Execute query
            preparedStatement = c.prepareStatement(query);
            preparedStatement.setInt(1, id);
            int retValue = preparedStatement.executeUpdate();
            if (retValue == 1 ) {
                c.commit();
                return true;
            }
            else
                return false;
        }
        catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
            return false;
        }
        finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
            if (preparedStatement != null) {
                try {
                    preparedStatement.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT0Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

    }

    public class DirStruct {
        private int id;
        private String dirname;
        private File_type fileType;

        public String getDirname() {
            return dirname;
        }

        public void setDirname(String dirname) {
            this.dirname = dirname;
        }

        public File_type getFileType() {
            return fileType;
        }

        public void setFileType(File_type fileType) {
            this.fileType = fileType;
        }

        public int getId() {
            return id;
        }

        public void setId(int id) {
            this.id = id;
        }
    }


}


