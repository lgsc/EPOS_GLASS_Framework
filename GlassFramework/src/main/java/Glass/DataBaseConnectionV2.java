package Glass;

import Configuration.DBConsts;
import Configuration.SiteConfig;
import CustomClasses.StationQuery;
import EposTables.*;
import Geometry.Point;

import javax.ejb.EJB;
import javax.naming.Context;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.net.SocketException;
import java.sql.*;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.Properties;

import CustomClasses.*;

import static Glass.GlassConstants.oldDBVersion;

@SuppressWarnings("Duplicates")
class DataBaseConnectionV2 {

    private Statement s = null;                 // Statement for queries on the EPOS database
    private ResultSet rs = null;                // Result Set for the queries on the EPOS database

    @EJB
    private SiteConfig siteConfig;
    private String DBConnectionString = null;

    DataBaseConnectionV2() {
        this.siteConfig = lookupSiteConfigBean();
        DBConnectionString = siteConfig.getDBConnectionString();
    }


    private SiteConfig lookupSiteConfigBean() {
        try {
            Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }


    private Connection NewConnection() throws SocketException, ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user",DBConsts.DB_USERNAME);
        props.setProperty("password",DBConsts.DB_PASSWORD);
        props.setProperty("logUnclosedConnections","true");
        Connection c = DriverManager.getConnection(DBConnectionString, props);
        c.setAutoCommit(false);
        return c;
    }


    private double getDistanceCoordinates(double lat1, double lon1, double lat2, double lon2)
    {
        double R = 6372.8; // In kilometers
        double dLat = Math.toRadians(lat2 - lat1);
        double dLon = Math.toRadians(lon2 - lon1);
        lat1 = Math.toRadians(lat1);
        lat2 = Math.toRadians(lat2);

        double a = Math.pow(Math.sin(dLat / 2),2) + Math.pow(Math.sin(dLon / 2),2) * Math.cos(lat1) * Math.cos(lat2);
        double c = 2 * Math.asin(Math.sqrt(a));
        return R * c;
    }

    StringBuilder conditions(StringBuilder query, StationQuery sq, Connection c){

        Boolean firstQuery = true;
        Boolean dateRangeFlag = true;
        StringBuilder initial = query;
        query = new StringBuilder("");

        if(sq.getFullV()){
            initial = new StringBuilder("SELECT s.*,")
                    .append(" markerlongname(s.marker, s.monument_num, s.receiver_num, s.country_code) AS markerlongname ")
                    //.append(" array_to_string(array_agg(distinct(rf.reference_date)), ',') as reference_dates ")
                    .append(" FROM station s ")
                    //.append(" LEFT JOIN rinex_file rf ON rf.id_station = s.id ")
                    .append(" INNER JOIN location l ON l.id = s.id_location ")
                    .append(" INNER JOIN coordinates crd ON crd.id = l.id_coordinates ")
                    .append(" INNER JOIN city ct on ct.id = l.id_city ")
                    .append(" INNER JOIN state stt on stt.id = ct.id_state ")
                    .append(" INNER JOIN country cnt on cnt.id = stt.id_country ")
                    .append(" where s.id in (")
                        .append(" select sn.id_station from station_network sn ")
                        .append(" INNER JOIN station s on s.id = sn.id_station")
                        .append(" INNER JOIN network net on sn.id_network = net.id ")
                        .append(" INNER JOIN station_contact sc on s.id = sc.id_station ")
                        .append(" INNER JOIN contact cntc on sc.id_contact = cntc.id ")
                        .append(" INNER JOIN agency agn on cntc.id_agency = agn.id ");
        }


        if(!(sq.getCoordinates().isEmpty())) {
            if (!(sq.getCoordIDs().isEmpty())) {
                ArrayList<Integer> ids = sq.getCoordIDs();
                query.append(" WHERE (");
                firstQuery = false;
                for (int i = 0; i < ids.size(); i++) {
                    query.append(" crd.id = '").append(ids.get(i)).append("' ");
                    if (i < ids.size() - 1)
                        query.append(" OR ");
                }
                //If there are no matching stations --> don't show any station
                if (ids.size() == 0)
                    query.append(" crd.id = '-1' ");
                query.append(" ) ");
            }
        }

        if(sq.getMinLat() != -999) {
            if(firstQuery){
                firstQuery = false;
                query.append(" WHERE ");
            }
            else
                query.append(" AND ");
            query.append(" crd.lat >= '").append(sq.getMinLat()).append("' ");
        }

        if(sq.getMaxLat() != -999) {
            if(firstQuery){
                firstQuery = false;
                query.append(" WHERE ");
            }
            else
                query.append(" AND ");

            query.append(" crd.lat <= '").append(sq.getMaxLat()).append("' ");
        }

        if(sq.getMinLon() != -999) {
            if(firstQuery){
                firstQuery = false;
                query.append(" WHERE ");
            }
            else
                query.append(" AND ");

            query.append(" crd.lon >= '").append(sq.getMinLon()).append("' ");
        }

        if(sq.getMaxLon() != -999) {
            if(firstQuery){
                firstQuery = false;
                query.append(" WHERE ");
            }
            else
                query.append(" AND ");

            query.append(" crd.lon <= '").append(sq.getMaxLon()).append("' ");
        }

        if(sq.getMaxAlt() != -999){
            if(firstQuery){
                firstQuery = false;
                query.append(" WHERE ");
            }
            else
                query.append(" AND ");

            query.append(" crd.altitude <= '").append(sq.getMaxAlt()).append("'");
        }

        if(sq.getMinAlt() != -999){
            if(firstQuery){
                firstQuery = false;
                query.append(" WHERE ");
            }
            else
                query.append(" AND ");

            query.append(" crd.altitude >= '").append(sq.getMinAlt()).append("'");
        }


        //Fixes issues 18 and 25. Also now simplifies the code
        // This doesn't take in count if the antenna/radom/receiver is the actual one
        // Returns every station that at some point had that item
        Tuple2<String,Boolean> reply;
        reply = itemQuery( sq.getRadome(), GlassConstants.getDbItemAttributeRadomeId(), firstQuery);
        firstQuery= reply.getSecond();
        query.append(reply.getFirst());
        reply = itemQuery( sq.getAntenna(), GlassConstants.getDbItemAttributeAntennaId(), firstQuery);
        firstQuery=reply.getSecond();
        query.append(reply.getFirst());
        reply = itemQuery( sq.getReceiver(), GlassConstants.getDbItemAttributeReceiverId(), firstQuery);
        firstQuery=reply.getSecond();
        query.append(reply.getFirst());

        if(!(sq.getStationType().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" s.id_station_type = ( ")
                    .append(" select id from station_type where name = ")
                    .append(" '").append(sq.getStationType()).append("') ");
        }

        if(!(sq.getNetworks().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( ");
            ArrayList<String> networks = sq.getNetworks();
            for(int i=0; i < networks.size(); i++){
                query.append(" net.name ilike '").append(networks.get(i));
                if(i+1 < networks.size())
                    query.append("%' OR ");
                else
                    query.append("%' ");
            }
            query.append(" ) ");
        }

        if(!(sq.getAgencies().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( ");
            ArrayList<String> agencies = sq.getAgencies();
            for(int i=0; i < agencies.size(); i++){
                query.append(" agn.name ilike '").append(agencies.get(i));
                if(i+1 < agencies.size())
                    query.append("%' OR ");
                else
                    query.append("%' ");
            }
            query.append(" ) ");
        }

        if(!(sq.getMarkers().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( ");
            ArrayList<String> markers = sq.getMarkers();
            for(int i=0; i < markers.size(); i++){
                query.append(" s.marker ilike '").append(markers.get(i));
                if(i+1 < markers.size())
                    query.append("%' OR ");
                else
                    query.append("%' ");
            }
            query.append(" ) ");
        }

        if(!(sq.getNames().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( ");
            ArrayList<String> names = sq.getNames();
            for(int i=0; i < names.size(); i++){
                query.append(" s.name ilike '").append(names.get(i));
                if(i+1 < names.size())
                    query.append("%' OR ");
                else
                    query.append("%' ");
            }
            query.append(" ) ");

        }

        if(!(sq.getDate_from().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( date_to IS NULL OR date_to > '").append(sq.getDate_from()).append("') ");
        }

        if(!(sq.getDate_to().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( date_from <= '").append(sq.getDate_to()).append("') ");
        }
        
        if(!(sq.getCountry().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( ");
            ArrayList<String> countries = sq.getCountry();
            for(int i = 0; i< countries.size(); i++){
                query.append(" cnt.name ilike '").append(countries.get(i)).append("%' ");
                if (i < countries.size() - 1)
                    query.append(" OR ");
            }
            query.append(" ) ");
        }

        if(!(sq.getState().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( ");
            ArrayList<String> states = sq.getState();
            for(int i = 0; i< states.size(); i++){
                query.append(" stt.name ilike '").append(states.get(i)).append("%' ");
                if (i < states.size() - 1)
                    query.append(" OR ");
            }
            query.append(" ) ");
        }

        // Check if there are cities
        if(!(sq.getCity().isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( ");
            ArrayList<String> cities = sq.getCity();
            for(int i = 0; i< cities.size(); i++){
                query.append(" ct.name ilike '").append(cities.get(i)).append("%' ");
                if (i < cities.size() - 1)
                    query.append(" OR ");
            }
            query.append(" ) ");
        }

        //Check if there's a min installation date
        if(!(sq.getInstalledDateMin().isEmpty())) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( date_from >= '").append(sq.getInstalledDateMin()).append("') ");
        }

        //Check if there's a max installation date
        if(!(sq.getInstalledDateMax().isEmpty())) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( date_from <= '").append(sq.getInstalledDateMax()).append("') ");
        }

        //Check if there's a min removed date
        if(!(sq.getRemovedDateMin().isEmpty())) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( date_to >= '").append(sq.getRemovedDateMin()).append("') ");
        }

        //Check if there's a min removed date
        if(!(sq.getRemovedDateMax().isEmpty())) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            query.append(" ( date_to <= '").append(sq.getRemovedDateMax()).append("') ");
        }


        //Check if there's a satellite
        if(!(sq.getSatellites().isEmpty())) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            //query.append(" ( date_to >= '").append(sq.getRemovedDateMin()).append("') ");
            query.append(" s.id in ( ")
                    .append(" select si.id_station from station_item si")
                    .append(" inner join item it on it.id = si.id_item ")
                    .append(" inner join item_attribute ia on ia.id_item = it.id ")
                    .append(" inner join attribute atr on atr.id = ia.id_attribute")
                    .append(" where atr.name = 'satellite_system' AND (");


            ArrayList<String> satellites = sq.getSatellites();
            for (int i = 0; i < satellites.size(); i++) {
                query.append(" ia.value_varchar ilike '%").append(satellites.get(i)).append("%' ");
                if (i + 1 < satellites.size())
                    query.append(" AND "); // e.g. SBAS WITH BDS, and not the ones that do not have bds
                    //query.append(" OR ");
                else
                    query.append(" ) ) ");
            }
        }


        //Inversed networks
        if(!(sq.getInvertedNetworks().isEmpty())) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            ArrayList<String> invertedNetworks = sq.getInvertedNetworks();
            for(int i = 0; i < invertedNetworks.size(); i++) {
                query.append(" s.id in ( ")
                        .append("select sn.id_station from station_network sn where sn.id_network not in ")
                        .append("(select id from network where name ilike '").append(invertedNetworks.get(i)).append("%')")
                        .append(" except select sn.id_station from station_network sn where sn.id_network in ")
                        .append("(select id from network where name ilike '").append(invertedNetworks.get(i)).append("%' ) ) ");
                if (i + 1 < invertedNetworks.size())
                    query.append(" AND ");
            }
        }

        //Station lifetime
        if(sq.getLifetime()!=0){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            query.append(sq.getLifetime())
                    .append(" <= (date_part('day', CASE WHEN (s.date_to is null) THEN current_date ELSE s.date_to end - s.date_from)) ");
        }


        //ALL the below code needs to be optimized for the view and code added to complete the optoins for the now view
        //also the stations with velocities should pass an analysis center as option.
        if(sq.getStationsWith() > 0) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            switch ( sq.getStationsWith() ) {
                //Stations with Rinex Files
                case 1:
                    query.append("s.id in ( select distinct(id_station) from rinex_file order by id_station) "); //rinex files
                    break;
                //Stations with velocities
                case 2:
                    query.append("s.id in ( select distinct(id_station) from reference_position_velocities order by id_station) "); //vel products
                    break;
                //Stations with Timeseries or Coordinates (Cleaned and Raw TimeSeries)
                case 3:
                    if (sq.getProductACAbr().equals("ALL") )  //ALL is the default.
                    {
                        //using  either the optimized view or directly the estimated coordinates table
                        if ( GlassConstants.PRODUCT_OPTIMIZE_2)
                            query.append("s.id in ( select distinct(id_station) from view_stations_with_products order by id_station) "); //ts products
                        else
                            query.append("s.id in ( select distinct(id_station) from estimated_coordinates order by id_station) "); //ts products
                        break;
                    }else {
                        //using  either the optimized view or directly the estimated coordinates table
                        if ( GlassConstants.PRODUCT_OPTIMIZE_2) {
                            query.append("s.id in ( select distinct(id_station) from view_stations_with_products "); //ts products
                            //its easy using the view
                            query.append(" where abbreviation = '" + sq.getProductACAbr()+ "'");
                        }
                        else {
                            query.append("s.id in ( select distinct(id_station) from estimated_coordinates "); //ts products
                            //I need to get the analysis center from the method identification table
                            //thus the query os more difficult
                            //this needs to be done still
                        }
                        query.append(" order by id_station) ");
                        break;
                    }
                //products - stations with ts OR velocities
                //union selects by default only distinct values
                case 4:
                    query.append("s.id in ( " +
                            "select id_station from estimated_coordinates  UNION " +
                            "select id_station from reference_position_velocities order by id_station  ) ");
                    break;
                //products - stations with ts AND velocities
                //uses the inner join ... use smaller velocity table to speed up query ... still very slow
                case 5:
                    query.append("s.id in ( " +
                            "select distinct(vel.id_station) from reference_position_velocities as vel inner join " +
                            "estimated_coordinates as ts  on ts.id_station = vel.id_station order by id_station ) "); //products
                    break;
                default:
                    //this would be an illegal value
            }
        }

        //T2
        //File Type
        if(!sq.getT2FileType().isEmpty()){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            query.append(" s.id in ( SELECT rf.id_station FROM rinex_file as rf ")
                    .append("INNER JOIN file_type AS ft ON rf.id_file_type = ft.id ")
                    .append("WHERE ft.format = '")
                    .append(sq.getT2FileType()).append("') ");
        }

        //Sampling Window
        if(!sq.getT2SamplingWindow().isEmpty()){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            query.append(" s.id in ( SELECT rf.id_station FROM rinex_file as rf ")
                    .append("INNER JOIN file_type as ft on rf.id_file_type = ft.id ")
                    .append("WHERE ft.sampling_window ='")
                    .append(sq.getT2SamplingWindow()).append("') ");
        }

        //Sampling Frequency
        if(!sq.getT2SamplingFrequency().isEmpty()){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            query.append(" s.id in ( SELECT rf.id_station FROM rinex_file as rf ")
                    .append("INNER JOIN file_type as ft on rf.id_file_type = ft.id ")
                    .append("WHERE ft.sampling_frequency ='")
                    .append(sq.getT2SamplingFrequency()).append("') ");
        }

        //Date range Start
        if(!sq.getT2DateStart().isEmpty() && dateRangeFlag) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            query.append("s.id in ( SELECT rf.id_station FROM rinex_file as rf ")
                    .append(" WHERE reference_date >= '")
                    .append(sq.getT2DateStart())
                    .append("' ");

            //Date range End
            if(!sq.getT2DateEnd().isEmpty()) {
                query.append(" AND reference_date <= '")
                        .append(sq.getT2DateEnd())
                        .append(" 23:59:59' ");
            } else {
                final Calendar cal = Calendar.getInstance();
                cal.add(Calendar.DATE, -1);
                DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                query.append(" AND reference_date <= '")
                        .append(dateFormat.format(cal.getTime()))
                        .append(" 23:59:59' ");
            }

            /*if(sq.getT2minimumObservationYears()!=0){
                StringBuilder tempquery = new StringBuilder("select distinct id_station as id, count(distinct reference_date) as total from rinex_file ");
                tempquery.append(" where reference_date >= '").append(sq.getT2DateStart()).append("' and reference_date <= '").append(sq.getT2DateEnd()).append("' ").toString();
                tempquery.append(" group by id_station");
                ArrayList<Integer> minObsIDs = executeQueryMinObsYearsAndGetIDs(tempquery.toString(), c, sq.getT2minimumObservationYears());
                sq.setT2minimumObservationYears(0);
                query.append(" AND (");

                if(minObsIDs.size()==0)
                    query.append(" s.id = -1 ");

                for(int i = 0; i<minObsIDs.size(); i++) {
                    if (i < minObsIDs.size() - 1) {
                        query.append(" s.id = '").append(minObsIDs.get(i)).append("' OR ");
                    } else {
                        query.append(" s.id = '").append(minObsIDs.get(i)).append("' ");
                    }
                }
                query.append(")");
            }

            if(sq.getT2minimumObservationDays()!=0){
                StringBuilder tempquery = new StringBuilder("select distinct id_station as id, count(distinct reference_date) as total from rinex_file ");
                tempquery.append(" where reference_date >= '").append(sq.getT2DateStart()).append("' and reference_date <= '").append(sq.getT2DateEnd()).append("' ").toString();
                tempquery.append(" group by id_station");
                ArrayList<Integer> minObsIDs = executeQueryMinObsAndGetIDs(tempquery.toString(), c, sq.getT2minimumObservationDays());
                sq.setT2minimumObservationDays(0);
                query.append(" AND (");

                if(minObsIDs.size()==0)
                    query.append(" s.id = -1 ");

                for(int i = 0; i<minObsIDs.size(); i++){
                    if(i < minObsIDs.size()-1){
                        query.append(" s.id = '").append(minObsIDs.get(i)).append("' OR ");
                    }else{
                        query.append(" s.id = '").append(minObsIDs.get(i)).append("' ");
                    }
                }
                query.append(") ");
            }*/

            /*if(sq.getT2DataAvailability().isEmpty()){
                String endtime, starttime;
                starttime = "'" + sq.getT2DateStart() + "'";
                endtime = " '" + sq.getT2DateEnd() + "' ";

                StringBuilder tempquery = new StringBuilder("select s.id, ");
                tempquery.append("count(distinct reference_date)*100/(date ")
                        .append(endtime)
                        .append(" - date ")
                        .append(starttime)
                        .append(") as total from rinex_file rf inner join station s on s.id = rf.id_station WHERE reference_date >= ")
                        .append(starttime)
                        .append(" AND reference_date <= ")
                        .append(endtime)
                        .append(" group by s.id");

                float dataAvail;
                try{
                    dataAvail = Float.parseFloat(sq.getT2DataAvailability());
                }catch(NumberFormatException e){
                    dataAvail = 0;
                }


                ArrayList<Integer> dataAvailabilityIDs = executeQueryMinObsAndGetIDs(tempquery.toString(), c, dataAvail);

                query.append(" AND (");
                if(dataAvailabilityIDs.size()==0) //No stations match the criteria
                    query.append(" s.id = '-1' ");

                for(int i=0; i < dataAvailabilityIDs.size(); i++) {
                    if(dataAvailabilityIDs.size()-1 == i)
                        query.append(" s.id = '").append(dataAvailabilityIDs.get(i)).append("' ");
                    else
                        query.append(" s.id = '").append(dataAvailabilityIDs.get(i)).append("' OR ");
                }
                query.append(")");
            }*/
            query.append(") ");
        }



        //Minimum observation days
        if(sq.getT2minimumObservationDays()!=0){

            StringBuilder tempquery = new StringBuilder("select distinct id_station as id, count(distinct reference_date) as total from rinex_file ");
            if(!sq.getT2DateStart().isEmpty() && !sq.getT2DateEnd().isEmpty()){
                tempquery.append(" where reference_date >= '").append(sq.getT2DateStart()).append("' and reference_date <= '").append(sq.getT2DateEnd()).append("' ").toString();
                dateRangeFlag = false;
            }
            tempquery.append(" group by id_station");
            ArrayList<Integer> minObsIDs = executeQueryMinObsAndGetIDs(tempquery.toString(), c, sq.getT2minimumObservationDays());


            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            query.append("(");
            if(minObsIDs.size()==0)
                query.append(" s.id = -1 ");

            for(int i = 0; i<minObsIDs.size(); i++){
                if(i < minObsIDs.size()-1){
                    query.append(" s.id = '").append(minObsIDs.get(i)).append("' OR ");
                }else{
                    query.append(" s.id = '").append(minObsIDs.get(i)).append("' ");
                }
            }
            query.append(")");
        }

        //Minimum observation years
        if(sq.getT2minimumObservationYears()!=0){

            StringBuilder tempquery = new StringBuilder("select distinct id_station as id, count(distinct reference_date) as total from rinex_file ");
            if(!sq.getT2DateStart().isEmpty() && !sq.getT2DateEnd().isEmpty()){
                tempquery.append(" where reference_date >= '").append(sq.getT2DateStart()).append("' and reference_date <= '").append(sq.getT2DateEnd()).append("' ").toString();
                dateRangeFlag = false;
            }
            tempquery.append(" group by id_station");
            ArrayList<Integer> minObsIDs = executeQueryMinObsYearsAndGetIDs(tempquery.toString(), c, sq.getT2minimumObservationYears());


            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            query.append("(");
            if(minObsIDs.size()==0)
                query.append(" s.id = -1 ");

            for(int i = 0; i<minObsIDs.size(); i++){
                if(i < minObsIDs.size()-1){
                    query.append(" s.id = '").append(minObsIDs.get(i)).append("' OR ");
                }else{
                    query.append(" s.id = '").append(minObsIDs.get(i)).append("' ");
                }
            }
            query.append(")");
        }


        //Data availability (only if there's a date range)
        if(!sq.getT2DataAvailability().isEmpty()) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            String endtime, starttime;
            if(sq.getT2DateStart().isEmpty())
                starttime = " s.date_from ";
                //starttime = " ( select date_from from station where id = s.id) ";
            else
                starttime = "'" + sq.getT2DateStart() + "'";
                /*
                DateTimeFormatter formatterStart = DateTimeFormatter.ofPattern("yyyyMMdd");
                LocalDate start = LocalDate.parse(starttime, formatterStart);
                starttime = start.toString();
                */


            if(sq.getT2DateEnd().isEmpty())
                endtime = " s.date_to ";
                //endtime = " (select date_to from station where id = s.id) ";
            else
                endtime = " '" + sq.getT2DateEnd() + "' ";
                /*
                DateTimeFormatter formatterEnd = DateTimeFormatter.ofPattern("yyyyMMdd");
                LocalDate end = LocalDate.parse(endtime, formatterEnd);
                endtime = end.toString();
                */
            StringBuilder tempquery = new StringBuilder("select s.id, ");

            if(sq.getT2DateStart().isEmpty()) {
                tempquery.append(" count(distinct reference_date)*100/(date_part('day',case when s.date_to is null then current_date else s.date_to end - s.date_from)")
                        .append(") as total from rinex_file rf inner join station s on s.id = rf.id_station WHERE reference_date >= ")
                        .append(starttime)
                        .append(" AND reference_date <= case when s.date_to is null then current_date else s.date_to end group by s.id");
            }
            else {
                tempquery.append("count(distinct reference_date)*100/(date (")
                        .append(endtime)
                        .append(") - date (")
                        .append(starttime)
                        .append(")) as total from rinex_file rf inner join station s on s.id = rf.id_station WHERE reference_date >= ")
                        .append(starttime)
                        .append(" AND reference_date <= ")
                        .append(endtime)
                        .append(" group by s.id");
            }



            float dataAvail;
            try{
                dataAvail = Float.parseFloat(sq.getT2DataAvailability());

            }catch(NumberFormatException e){
                dataAvail = 0;
            }

            ArrayList<Integer> dataAvailabilityIDs = executeQueryMinObsAndGetIDs(tempquery.toString(), c, dataAvail);

            query.append("(");
            if(dataAvailabilityIDs.size()==0) //No stations match the criteria
                query.append(" s.id = '-1' ");

            for(int i=0; i < dataAvailabilityIDs.size(); i++) {
                if(dataAvailabilityIDs.size()-1 == i)
                    query.append(" s.id = '").append(dataAvailabilityIDs.get(i)).append("' ");
                else
                    query.append(" s.id = '").append(dataAvailabilityIDs.get(i)).append("' OR ");
            }
            query.append(")");
        }
        
        //Status file
        if(sq.getT2statusfile()!=-10){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }

            query.append(" s.id in ( SELECT id_station FROM rinex_file ")
                    .append("WHERE status ='")
                    .append(sq.getT2statusfile()).append("') ");
        }
        
        
        //T3
        //observations types
        /*if(!(sq.getT3Observationtype().isEmpty())) {
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
        
            query.append(" s.id in ( SELECT DISTINCT rf.id_station FROM gnss_obsnames as gnss ")
                    .append("JOIN qc_observation_summary_s as qcobssum_s ON qcobssum_s.id_gnss_obsnames = gnss.id ")
                    .append("JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id = qcobssum_s.id_qc_constellation_summary ")
                    .append("JOIN qc_report_summary as qcrepsum ON qcrepsum.id = qcconstsum.id_qc_report_summary ")
                    .append("JOIN rinex_file as rf ON rf.id = qcrepsum.id_rinexfile ")
                    .append("WHERE gnss.obstype ='")
                    .append(sq.getT3Observationtype()).append("') ");
        }*/    

        if(sq.getFullV())
            query.append(") ");
        
        return initial.append(query);
    }

    private Tuple2<String,Boolean> itemQuery( ArrayList<String> itemType, int DbItemAttribute, boolean firstQuery){
        StringBuilder query= new StringBuilder();
        if(!(itemType.isEmpty())){
            if (firstQuery) {
                query.append(" WHERE ");
                firstQuery = false;
            } else {
                query.append(" AND ");
            }
            // #18 added distinct below
            query.append(" s.id in ( ")
                    .append(" SELECT distinct si.id_station FROM station_item si ")
                    .append(" INNER JOIN item_attribute ia ON ia.id_item = si.id_item ")
                    .append(" WHERE ia.id_attribute='")
                    .append(DbItemAttribute)
                    .append("' AND (");

            for (int i = 0; i < itemType.size(); i++) {
                query.append(" ia.value_varchar = '").append(itemType.get(i));
                if (i + 1 < itemType.size())
                    query.append("' OR ");
                else
                    query.append("' ) ");
            }
            query.append(" ) "); //paul ISSUE #18
        }
        String queryToAppend=query.toString();
        return (new Tuple2<>( queryToAppend, firstQuery));
    }

    /*
    Paul Crocker
    Uses the Cache
     */
    ArrayList<Station> getStationsShortV3() {
        JsonCache mycache = new JsonCache();
        return mycache.getAllStations();
    }


    ArrayList<Station> getStationsShortV2(StationQuery sq){

        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Station> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();

            //Create the query to be executed. Only the necessary data will be fetched
            //Station name, station marker, networks, agencies, date_from, date_to, lat, lon, altitude, city, state, country
            StringBuilder query = new StringBuilder("select s.id, s.name, s.marker, array_to_string(array_agg(distinct(net.name)), ' & ') as networks,")
                    .append(" array_to_string(array_agg(distinct(agn.name)), ' & ') as agencies, s.date_from, s.date_to, ")
                    .append(" markerlongname(s.marker, s.monument_num, s.receiver_num, s.country_code) AS markerlongname, ")
                    .append(" crd.x, crd.y, crd.z, crd.lat, crd.lon, crd.altitude, ct.name as city, stt.name as state, ")
                    .append(" cnt.name as country from station s inner join location l on s.id_location = l.id inner join coordinates crd on l.id_coordinates = crd.id ")
                    .append(" inner join city ct on l.id_city = ct.id inner join state stt on ct.id_state = stt.id inner join country cnt on stt.id_country = cnt.id ")
                    .append(" inner join station_network sn on s.id = sn.id_station inner join network net on sn.id_network = net.id ")
                    .append(" inner join station_contact sc on s.id = sc.id_station inner join contact cntc on sc.id_contact = cntc.id ")
                    .append(" inner join agency agn on cntc.id_agency = agn.id  inner join station_type styp on styp.id = s.id_station_type ");

            query = conditions(query, sq, c);

            //Append the group by
            query.append(" group by s.id, s.name, s.marker, s.date_to, s.date_from, crd.x, crd.y, ")
                    .append("crd.z, crd.lat, crd.lon, crd.altitude, ct.name, stt.name, cnt.name ");
            //Append the order
            query.append(" order by s.id;");

            //System.out.println("In getStationsShortV2");
            //System.out.println(query.toString());

            if (!GlassConstants.UseCacheIngetStationsShortV2) {
                //don't bother with the cache
                results = executeQueryStationsShortV2(query.toString(), c);
            }
            else {
                //Check for cached version

                ArrayList<Integer> result_ids = executeQueryAndGetIDs(query.toString(), c);

                System.out.println("Station List Size for get all stations " + result_ids.size());

                //Search results T1 in dictionary
                JsonCache mycache = new JsonCache();

                Set<Integer> a = new HashSet<>();
                a.addAll(result_ids);
                ArrayList<Integer> allIds = mycache.getAllIds();

                if (result_ids.size() > 0) {
                    for (Integer result_id : result_ids) {
                        //for (Integer result_id : allIds) {
                        if (!a.contains(result_id))
                            System.out.println("Station with id not in query " + result_id);
                        try {
                            //Check in cache
                            Station myStation = mycache.get(result_id);
                            if (myStation != null)
                                results.add(myStation);
                            else
                                throw new Exception("Not in cache: " + result_id);
                        } catch (Exception e) {
                            //Something has gone wrong Execute the query via sql
                            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, e.toString());
                            System.out.println("Something has gone wrong in DataBAseConnectionV2 Execute the query via sql getStationsShortV2");
                            results = executeQueryStationsShortV2(query.toString(), c);
                        }
                    }
                }
            }

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<Station> executeQueryStationsShortV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<Station> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            Station s = new Station();

            // Station data
            s.setId(rs.getInt("id"));
            s.setName(rs.getString("name"));
            s.setMarker(rs.getString("marker"));
            try {
                s.setMarkerLongName(rs.getString("markerlongname"));
            }catch(Exception e){
                System.err.format("Error generating markerlongname for marker: " + s.getMarker());
                s.setMarkerLongName("---");
            }
            s.setDate_from(rs.getString("date_from"));
            s.setDate_to(rs.getString("date_to"));

            // Location data
            Country country = new Country(0, rs.getString("country"), "");
            State state = new State(0, country, rs.getString("state"));
            City city = new City(0, state, rs.getString("city"));
            Coordinates coordinates = new Coordinates(0, rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getDouble("lat"), rs.getDouble("lon"), rs.getDouble("altitude"));
            Location location = new Location(0, city, coordinates, null, "");
            s.setLocation(location);

            Agency agency = new Agency(0, rs.getString("agencies"), "", "", "", "");
            Contact contact = new Contact(0, "", "", "", "", "", "", agency, "");
            Station_contact station_contact = new Station_contact(0, s.getId(), contact, "");
            s.addStationContacts(station_contact);

            Network network = new Network(0, rs.getString("networks"), null);
            Station_network station_network = new Station_network(0, s.getId(), network);
            s.addStationNetwork(station_network);

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    ArrayList<Station> getStationsFullV2(StationQuery sq){

        //Connection
        Connection c = null;
        PreparedStatement _ps = null;
        ResultSet _rs = null;
        Statement _s = null;

        //ArrayList for the results
        ArrayList<Station> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();

            //Create the query to be executed. Only the necessary data will be fetch
/*
                    StringBuilder query = new StringBuilder().append("SELECT s.*,")
                    .append(" array_to_string(array_agg(distinct(CAST(rf.reference_date as date))), ' & ')")
                    .append(" as reference_dates FROM rinex_file rf, station s");
*/
            StringBuilder query = new StringBuilder();
            query = conditions(query, sq, c);

            //Append the order
            query.append(" group by s.id order by s.id;");

            //Check for cached version
            ArrayList<Integer> result_ids = executeQueryAndGetIDs(query.toString(), c);
           
            if (result_ids.size() > 0)
            {
                //T3 Treatment
                ArrayList<Integer> result_idsT3 = new ArrayList();
                if ((sq.getT3Observationtype().size()>0)||(sq.getT3Frequency().size()>0)||(sq.getT3Channel().size()>0)||(sq.getT3Constellation().size()>0)||(sq.getT3Ratioepoch()>0)||(sq.getT3Elevangle()>0)||(sq.getT3Multipathvalue()>0)||(sq.getT3Nbcycleslips()>0)||(sq.getT3Nbclockjumps()!= 0)||(sq.getT3Spprms()>0))
                {
                    StringBuilder queryT3 = new StringBuilder("SELECT DISTINCT st.marker "
                            + "FROM rinex_file as rf "
                            + "JOIN station as st ON st.id = rf.id_station "
                            + "JOIN file_type as ft ON ft.id=rf.id_file_type "
                            + "JOIN data_center_structure as dtcs ON dtcs.id=rf.id_data_center_structure "
                            + "JOIN data_center as dtc ON dtc.id=dtcs.id_data_center "
                            + "LEFT JOIN qc_report_summary as qcrepsum ON qcrepsum.id_rinexfile = rf.id "
                            + "JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id_qc_report_summary = qcrepsum.id "
                            + "JOIN constellation as const ON const.id = qcconstsum.id_constellation "
                            + "LEFT JOIN qc_observation_summary_s as qcobssum_s ON qcobssum_s.id_qc_constellation_summary = qcconstsum.id "
                            + "LEFT JOIN qc_observation_summary_c as qcobssum_c ON qcobssum_c.id_qc_constellation_summary = qcconstsum.id "
                            + "LEFT JOIN qc_observation_summary_d as qcobssum_d ON qcobssum_d.id_qc_constellation_summary = qcconstsum.id "
                            + "LEFT JOIN qc_observation_summary_l as qcobssum_l ON qcobssum_l.id_qc_constellation_summary = qcconstsum.id "
                            + "JOIN gnss_obsnames as gnss ON (gnss.id = qcobssum_s.id_gnss_obsnames) OR (gnss.id = qcobssum_c.id_gnss_obsnames) OR (gnss.id = qcobssum_d.id_gnss_obsnames) OR (gnss.id = qcobssum_l.id_gnss_obsnames) "
                            + "WHERE");


                    // Check constellation
                    if(sq.getT3Constellation().size()>0)
                    {    
                        for(int index=0; index<sq.getT3Constellation().size(); index++)
                        {   
                            if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                            {
                                queryT3.append(" const.name = '").append(sq.getT3Constellation().get(index)).append("'");
                            }else{
                                queryT3.append(" AND const.name = '").append(sq.getT3Constellation().get(index)).append("'");
                            }
                        }
                    }
                    // Check frequency
                    if(sq.getT3Frequency().size()>0)
                    {    
                        for(int index=0; index<sq.getT3Frequency().size(); index++)
                        {
                            
                            if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                            {
                                queryT3.append(" gnss.frequency_band = '").append(sq.getT3Frequency().get(index)).append("'");
                            }else{
                                queryT3.append(" AND gnss.frequency_band = '").append(sq.getT3Frequency().get(index)).append("'");
                            }
                        }
                    }
                    // Check channel
                    if(sq.getT3Channel().size()>0)
                    {    
                        for(int index=0; index<sq.getT3Channel().size(); index++)
                        {
                            if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                            {
                                if (sq.getT3Channel().get(index).equalsIgnoreCase("No Attr"))
                                {
                                    queryT3.append(" gnss.channel IS NULL");
                                }else{
                                    queryT3.append(" gnss.channel = '").append(sq.getT3Channel().get(index)).append("'");
                                }    
                            }else{
                                if (sq.getT3Channel().get(index).equalsIgnoreCase("No Attr"))
                                {
                                    queryT3.append(" AND gnss.channel IS NULL");
                                }else{
                                    queryT3.append(" AND gnss.channel = '").append(sq.getT3Channel().get(index)).append("'");
                                }    
                            }
                        }
                    }
                    // Check observation type
                    if(sq.getT3Observationtype().size()>0)
                    {    
                        for(int index=0; index<sq.getT3Observationtype().size(); index++)
                        {
                            if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                            {
                                if (sq.getT3Observationtype().get(index).equalsIgnoreCase("P/C"))
                                {    
                                    queryT3.append(" (gnss.obstype = 'P' OR gnss.obstype = 'C')");
                                }else{
                                    queryT3.append(" gnss.obstype = '").append(sq.getT3Observationtype().get(index)).append("'");
                                }    
                            }else{
                                if (sq.getT3Observationtype().get(index).equalsIgnoreCase("P/C"))
                                {    
                                    queryT3.append(" AND (gnss.obstype = 'P' OR gnss.obstype = 'C')");
                                }else{
                                    queryT3.append(" AND gnss.obstype = '").append(sq.getT3Observationtype().get(index)).append("'");
                                }    
                            }  
                        }
                    }
                    // Check epoch completeness
                    if(sq.getT3Ratioepoch()>0)
                    {
                        if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                        {
                            queryT3.append(" (qcrepsum.obs_have * 100)/qcrepsum.obs_expt > ").append(sq.getT3Ratioepoch());
                        }else{
                            queryT3.append(" AND (qcrepsum.obs_have * 100)/qcrepsum.obs_expt > ").append(sq.getT3Ratioepoch());
                        }
                    }

                    // Check elevation angle
                    if(sq.getT3Elevangle()>0)
                    {
                        if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                        {
                            queryT3.append(" qcrepsum.obs_elev < ").append(sq.getT3Elevangle());
                        }else{
                            queryT3.append(" AND qcrepsum.obs_elev < ").append(sq.getT3Elevangle());
                        }
                    }

                    // Check Multipath
                    if(sq.getT3Multipathvalue()>0)
                    {
                        if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                        {
                            queryT3.append(" ((gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) AND (SELECT AVG(qcobssum_c.cod_mpth)\n").append("FROM qc_observation_summary_c as qcobssum_c\n").append("JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id = qcobssum_c.id_qc_constellation_summary\n").append("JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_c.id_gnss_obsnames\n").append("JOIN constellation as const ON const.id = qcconstsum.id_constellation\n").append("WHERE (gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) < ").append(sq.getT3Multipathvalue());
                        }else{
                            queryT3.append(" AND ((gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) AND (SELECT AVG(qcobssum_c.cod_mpth)\n").append("FROM qc_observation_summary_c as qcobssum_c\n").append("JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id = qcobssum_c.id_qc_constellation_summary\n").append("JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_c.id_gnss_obsnames\n").append("JOIN constellation as const ON const.id = qcconstsum.id_constellation\n").append("WHERE (gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) < ").append(sq.getT3Multipathvalue());
                        }
                    }

                    // Check cycle slips
                    if(sq.getT3Nbcycleslips()>0)
                    {
                        if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                        {
                            queryT3.append(" qcrepsum.cyc_slps = ").append(sq.getT3Nbcycleslips());
                        }else{
                            queryT3.append(" AND qcrepsum.cyc_slps = ").append(sq.getT3Nbcycleslips());
                        }
                    }

                    // Check clock jumps
                    if(sq.getT3Nbclockjumps()>0)
                    {
                        if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                        {
                            queryT3.append(" qcrepsum.clk_jmps = ").append(sq.getT3Nbclockjumps());
                        }else{
                            queryT3.append(" AND qcrepsum.clk_jmps = ").append(sq.getT3Nbclockjumps());
                        }
                    }

                    // Check SPP RMS
                    if(sq.getT3Spprms()>0)
                    {
                        if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE"))
                        {
                            queryT3.append(" const.name = 'GPS' AND sqrt((qcconstsum.x_rms*qcconstsum.x_rms) + (qcconstsum.y_rms*qcconstsum.y_rms) + (qcconstsum.z_rms*qcconstsum.z_rms)) < ").append(sq.getT3Spprms());
                        }else{
                            queryT3.append(" AND const.name = 'GPS' AND sqrt((qcconstsum.x_rms*qcconstsum.x_rms) + (qcconstsum.y_rms*qcconstsum.y_rms) + (qcconstsum.z_rms*qcconstsum.z_rms)) < ").append(sq.getT3Spprms());
                        }
                    }


                    try {
                        _ps = c.prepareStatement("SELECT marker FROM station WHERE id = ?;");

                        if (queryT3.substring(queryT3.length() - 5).equalsIgnoreCase("WHERE")) {
                            for (int i = 0; i < result_ids.size(); i++) {
                                _ps.setInt(1, result_ids.get(i)); // find marker because id station is not the same on each node
                                _rs = _ps.executeQuery();
                                while (_rs.next()) {
                                    if (i == 0)
                                        queryT3.append(" st.marker='").append(_rs.getString("marker")).append("'");
                                    else
                                        queryT3.append(" OR st.marker='").append(_rs.getString("marker")).append("'");
                                }
                            }
                        } else {
                            for (int i = 0; i < result_ids.size(); i++) {
                                _ps.setInt(1, result_ids.get(i));
                                _rs = _ps.executeQuery();
                                while (_rs.next()) {
                                    if (i == 0)
                                        queryT3.append(" AND (st.marker='").append(_rs.getString("marker")).append("'");
                                    else
                                        queryT3.append(" OR st.marker='").append(_rs.getString("marker")).append("'");
                                }
                            }
                            queryT3.append(")");
                        }
                    } catch (SQLException _sqlException){
                        System.out.println("Error " + _sqlException.getMessage());
                        Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                    } finally {
                        if(_rs != null) {
                            try {
                                _rs.close();
                            } catch (SQLException _sqlException) {
                                Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                            }
                        }

                        if(_ps != null){
                            try {
                                _ps.close();
                            }
                            catch (SQLException _sqlException){
                                Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                            }
                        }
                    }


                    queryT3.append(";");

                    String querynode = "SELECT DISTINCT no.host,no.port,no.dbname,no.username,no.password "
                                    + "FROM node as no "
                                    + "JOIN connections as cx ON cx.destiny=no.id "
                                    + "JOIN station as st ON st.id=cx.station "
                                    + "WHERE cx.metadata='T1'";
   
                    for (int i=0; i<result_ids.size(); i++)
                    {
                        if (i==0)
                            querynode = querynode + " AND (cx.station=" + result_ids.get(i);
                        else
                            querynode = querynode + " OR cx.station=" + result_ids.get(i);
                    }
                    querynode = querynode + ")";
                    
                    querynode = querynode + ";";

                    Statement s_node = c.createStatement();
                    ResultSet rs_node = s_node.executeQuery(querynode);

                    Connection cnode = null;
                    while (rs_node.next())
                    {
                        try{ 
                            Class.forName("org.postgresql.Driver");
                            String url = "jdbc:postgresql://" + rs_node.getString("host") + ":" + rs_node.getString("port") + "/" + rs_node.getString("dbname");
                            Properties props = new Properties();
                            props.setProperty("user",rs_node.getString("username"));
                            props.setProperty("password",rs_node.getString("password"));
                            props.setProperty("loginTimeout","2");
                            cnode = DriverManager.getConnection(url, props);
                            cnode.setAutoCommit(false);
                            ArrayList<String> results_from_node = executeQueryAndGetMarkers(queryT3.toString(), cnode);//Execute from each leaf node
                            _ps = c.prepareStatement("SELECT id FROM station WHERE marker = ?;");
                            for (String value : results_from_node) {
                                _ps.setString(1, value);
                                _rs = _ps.executeQuery();
                                while (_rs.next()) {
                                    result_idsT3.add(_rs.getInt("id"));
                                }
                            }

                        }catch (Exception e) {
                             System.out.println("Unable to connect to DB");
                        }finally {
                            if (_rs != null){
                                try{
                                    _rs.close();
                                }catch(SQLException _sqlException){
                                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                                }
                            }
                            if (_ps != null){
                                try{
                                    _ps.close();
                                }catch(SQLException _sqlException){
                                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                                }
                            }
                            if (cnode != null){
                                try{
                                    cnode.close();
                                }catch(SQLException _sqlException){
                                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                                }
                            }
                        }


                        //Execute from sub level node
                        /*Statement s_subnode = cnode.createStatement();
                        ResultSet rs_subnode = s_subnode.executeQuery(querynode);
                        while (rs_subnode.next())
                        {
                            Class.forName("org.postgresql.Driver");
                            Connection csubnode = DriverManager.getConnection("jdbc:postgresql://" + rs_subnode.getString("host") + ":" + rs_subnode.getString("port") + "/" + rs_subnode.getString("dbname"),rs_subnode.getString("username"), rs_subnode.getString("password"));
                            cnode.setAutoCommit(false);
                            ArrayList<String> results_from_subnode = new ArrayList();
                            results_from_subnode = executeQueryAndGetMarkers(queryT3, csubnode);//Execute from each leaf node
                            for(int i=0; i<results_from_subnode.size(); i++)
                            {
                                Statement s = c.createStatement();
                                ResultSet rs = s.executeQuery("SELECT id FROM station WHERE marker='" + results_from_subnode.get(i) + "';");
                                while (rs.next())
                                {
                                    result_idsT3.add(rs.getInt("id"));
                                }
                            }    
                        }
                        rs_subnode.close();
                        s_subnode.close();*/
                    }

                    rs_node.close();
                    s_node.close();

                    //Execute from itself
                    ArrayList<String> results_from_node = new ArrayList();
                    results_from_node = executeQueryAndGetMarkers(queryT3.toString(), c);

                    try {
                        _ps = c.prepareStatement("SELECT id FROM station WHERE marker = ?;");
                        for (int i = 0; i < results_from_node.size(); i++) {
                            _ps.setString(1, results_from_node.get(i));
                            _rs = _ps.executeQuery();
                            while (_rs.next()) {
                                result_idsT3.add(_rs.getInt("id"));
                            }
                        }
                        result_ids = result_idsT3;
                    } catch (SQLException _sqlException) {
                        System.out.println("Error " + _sqlException.getMessage());
                        Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                    } finally {
                        if(_rs != null) {
                            try {
                                _rs.close();
                            } catch (SQLException _sqlException) {
                                Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                            }
                        }

                        if(_ps != null){
                            try {
                                _ps.close();
                            }
                            catch (SQLException _sqlException){
                                Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                            }
                        }
                    }
                }

                //End of T3 treatment
            }
            //Search results T1 in dictionary
            JsonCache mycache = new JsonCache();

            if (result_ids.size() > 0){
                for (Integer result_id : result_ids) {
                    try {
                        //Check in cache
                        Station myStation = mycache.get(result_id);
                        if(myStation!=null)
                            results.add(myStation);
                        else
                            throw new Exception("@889 DataBaseConnectionV2  Not in cache: " + result_id);
                    } catch (Exception e) {
                        //Execute the query
                        results = executeQueryStationsFullV2(query.toString(), c);
                        break;
                    }
                }
            }

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (_rs != null) {
                try {
                    _rs.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (_ps != null) {
                try {
                    _ps.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }

            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<Integer> executeQueryAndGetIDs(String query, Connection c){

        Statement s_local;           // Statement for queries on the EPOS database
        ResultSet rs;                // Result Set for the queries on the EPOS database
        ArrayList<Integer> results = new ArrayList<>();

        try {
            // Create connection with the local database
            //Class.forName("org.postgresql.Driver");
            //c = DriverManager.getConnection("jdbc:postgresql://" + myIP + ":" +
            //        db_port + "/" + db_name, db_username, db_password);
            //c.setAutoCommit(false);

            // Initialized statement for select
            s_local = c.createStatement();
            rs = s_local.executeQuery(query);

            // Check if there are any queries
            while (rs.next())
                results.add(rs.getInt("id"));

            // Close connections
            rs.close();
            s_local.close();

        } catch ( Exception e ) {
            System.err.println("Query Has Thrown Exception :"+query);
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }

        return results;
    }
    
    private ArrayList<String> executeQueryAndGetMarkers(String query, Connection c){

        Statement s_local;           // Statement for queries on the EPOS database
        ResultSet rs;                // Result Set for the queries on the EPOS database
        ArrayList<String> results = new ArrayList<>();

        try {
            // Create connection with the local database
            //Class.forName("org.postgresql.Driver");
            //c = DriverManager.getConnection("jdbc:postgresql://" + myIP + ":" +
            //        db_port + "/" + db_name, db_username, db_password);
            //c.setAutoCommit(false);

            // Initialized statement for select
            s_local = c.createStatement();
            rs = s_local.executeQuery(query);

            // Check if there are any queries
            while (rs.next())
                results.add(rs.getString("marker"));

            // Close connections
            rs.close();
            s_local.close();

        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }

        return results;
    }

    private ArrayList<Integer> executeQueryMinObsAndGetIDs(String query, Connection c, float minObsValue){

        Statement s_local;           // Statement for queries on the EPOS database
        ResultSet rs;                // Result Set for the queries on the EPOS database
        ArrayList<Integer> results = new ArrayList<>();

        try {
            // Create connection with the local database
            //Class.forName("org.postgresql.Driver");
            //c = DriverManager.getConnection("jdbc:postgresql://" + myIP + ":" +
            //        db_port + "/" + db_name, db_username, db_password);
            //c.setAutoCommit(false);

            // Initialized statement for select
            s_local = c.createStatement();
            rs = s_local.executeQuery(query);

            // Check if there are any queries
            while (rs.next())
                if(rs.getFloat("total") > minObsValue)
                    results.add(rs.getInt("id"));

            // Close connections
            rs.close();
            s_local.close();

        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }

        return results;
    }

    private ArrayList<Integer> executeQueryMinObsYearsAndGetIDs(String query, Connection c, float minObsValue){

        Statement s_local;           // Statement for queries on the EPOS database
        ResultSet rs;                // Result Set for the queries on the EPOS database
        ArrayList<Integer> results = new ArrayList<>();

        try {
            // Create connection with the local database
            //Class.forName("org.postgresql.Driver");
            //c = DriverManager.getConnection("jdbc:postgresql://" + myIP + ":" +
            //        db_port + "/" + db_name, db_username, db_password);
            //c.setAutoCommit(false);

            // Initialized statement for select
            s_local = c.createStatement();
            rs = s_local.executeQuery(query);
            float years;

            // Check if there are any queries
            while (rs.next()) {
                years = rs.getFloat("total") / 365;
                if (years > minObsValue)
                    results.add(rs.getInt("id"));
            }
            // Close connections
            rs.close();
            s_local.close();

        } catch ( Exception e ) {
            System.err.println( e.getClass().getName()+": "+ e.getMessage() );
        }

        return results;
    }

    private ArrayList<Station> executeQueryStationsFullV2(String query, Connection c) throws SQLException{
        {
            //ArrayList for the results
            ArrayList<Station> results = new ArrayList<>();

            Statement s_local;           // Statement for queries on the EPOS database
            ResultSet rs;                // Result Set for the queries on the EPOS database
            Statement s_conditions;      // Statement for the condition queries on the EPOS database
            ResultSet rs_conditions;     // Result Set for the condition queries on the EPOS database
            Statement s_local_ties;      // Statement for the local_ties queries on the EPOS database
            ResultSet rs_local_ties;     // Result Set for the local_ties queries on the EPOS database
            Statement s_coll_inst;       // Statement for the Instrument_collocation queries on the EPOS database
            ResultSet rs_coll_inst;      // Result Set for the Instrument_collocation queries on the EPOS database
            Statement s_user_group_st = null;   // Statement for the user_group_station queries on the EPOS database
            ResultSet rs_user_group_st;  // Result Set for the user_group_station queries on the EPOS database
            Statement s_log;             // Statement for the log queries on the EPOS database
            ResultSet rs_log;            // Result Set for the log queries on the EPOS database
            Statement s_station_cont;    // Statement for the station contacts queries on the EPOS database
            ResultSet rs_station_cont;   // Result Set for the station contacts queries on the EPOS database
            Statement s_station_net;     // Statement for the station network queries on the EPOS database
            ResultSet rs_station_net;    // Result Set for the station network queries on the EPOS database
            Statement s_station_item;    // Statement for the station item queries on the EPOS database
            ResultSet rs_station_item;   // Result Set for the station item queries on the EPOS database
            Statement s_item;            // Statement for the item queries on the EPOS database
            ResultSet rs_item;           // Result Set for the item queries on the EPOS database
            Statement s_item_attribute;  // Statement for the item attribute queries on the EPOS database
            ResultSet rs_item_attribute; // Result Set for the item attribute queries on the EPOS database
            Statement s_document;        // Statement for the document queries on the EPOS database
            ResultSet rs_document;       // Result Set for the document on the EPOS database
            Statement s_station_col;     // Statement for the station collocation queries on the EPOS database
            ResultSet rs_station_col;    // Result Set for the station collocation on the EPOS database
            Statement s_col_offset;      // Statement for the collocation offset queries on the EPOS database
            ResultSet rs_col_offset;     // Result Set for the collocation offset on the EPOS database
            Statement s_s_colocated;     // Statement for the station colocated queries on the EPOS database
            ResultSet rs_s_colocated;    // Result Set for the station colocated on the EPOS database
            Statement s_network;         // Statement for the network queries on the EPOS database
            ResultSet rs_network;        // Result Set for the network queries on the EPOS database
            Statement s_multi;
            ResultSet rs_multi;
            City city;                      // Auxiliary object for City
            State state;                    // Auxiliary object for State
            Country country;                // Auxiliary object for Country
            Coordinates coordinates;        // Auxiliary object for Coordinates
            Tectonic tectonic;              // Auxiliary object for Tectonic
            Bedrock bedrock;                // Auxiliary object for Bedrock

            // *********************************************************************
            // STEP 1
            // Get the IP address of the machine.
            // *********************************************************************
            //getLocalIP();

            // *********************************************************************
            // STEP 2
            // Check data from query.
            // *********************************************************************
            try {
                // Create connection with the local database
                //Class.forName("org.postgresql.Driver");
                //c = DriverManager.getConnection("jdbc:postgresql://" + myIP + ":" +
                //        db_port + "/" + db_name, db_username, db_password);
                //c.setAutoCommit(false);

                // Initialized statement for select
                s_local = c.createStatement();
                rs = s_local.executeQuery(query);
                
                String reference_dates_string = ""; 
                
                // Check if there are any queries
                while (rs.next())
                {

                    // Create Station object
                    Station s = new Station();
                    s.setId(rs.getInt("id"));
                    s.setName(rs.getString("name"));
                    s.setMarker(rs.getString("marker"));
                    s.setDescription(rs.getString("description"));
                    s.setDate_from(rs.getString("date_from"));
                    s.setDate_to(rs.getString("date_to"));
                    s.setComment(rs.getString("comment"));
                    s.setIers_domes(rs.getString("iers_domes"));
                    s.setCpd_num(rs.getString("cpd_num"));
                    s.setMonument_num(rs.getInt("monument_num"));
                    s.setReceiver_num(rs.getInt("receiver_num"));
                    s.setCountry_code(rs.getString("country_code"));
                    try {
                        s.setMarkerLongName(rs.getString("markerlongname"));
                    }catch(Exception e){
                        System.err.format("Error generating markerlongname for marker: " + s.getMarker());
                        s.setMarkerLongName("---");
                    }

                    //String rinex_files_dates_string = rs.getString("reference_dates");
                    //s.addRinexFiles(rinex_files_dates_string);
                    
                    Statement s_rinex = c.createStatement();
                    ResultSet rs_rinex = s_rinex.executeQuery("(SELECT MIN(reference_date) as res\n" +
                                                                "FROM rinex_file\n" +
                                                                "WHERE id_station = " + rs.getInt("id") + "\n" +
                                                                " )\n" +
                                                                "UNION ALL\n" +
                                                                "(select unnest(array[r.reference_date,r.reference_date + (next_date - reference_date)]) as my_date\n" +
                                                                "from (select r.*, lead(reference_date) over (order by reference_date) as next_date\n" +
                                                                "      from rinex_file r where id_station = " + rs.getInt("id") + "\n" +
                                                                "     ) r\n" +
                                                                "where next_date > reference_date + interval '1 day'\n" +
                                                                ")\n" +
                                                                "UNION ALL\n" +
                                                                "(SELECT MAX(reference_date)\n" +
                                                                "FROM rinex_file\n" +
                                                                "WHERE id_station = " + rs.getInt("id") + "\n" +
                                                                ")");
                    while (rs_rinex.next())
                    {
                        reference_dates_string += rs_rinex.getString("res") + ",";
                    }
                    reference_dates_string = reference_dates_string.substring(0, reference_dates_string.length() - 1);
                    s.addRinexFiles(reference_dates_string);
                    reference_dates_string = "";
                    
                    s_rinex.close();
                    rs_rinex.close();
                    
                    //Number of rinex files
                    
                    Statement s_nbfiles = c.createStatement();
                    ResultSet rs_nbfiles = s_nbfiles.executeQuery("select count(reference_date) from rinex_file where id_station =" + rs.getInt("id")+";");
                    rs_nbfiles.next();                                            
                    int nbrinex = rs_nbfiles.getInt(1);
                    s.setNbRinexFiles(nbrinex);
                    
                    s_nbfiles.close();
                    rs_nbfiles.close();
                    
                    // Initialized statement for select
                    s_multi = c.createStatement();

                    rs_multi = s_multi.executeQuery(new StringBuilder().append("SELECT ").append("loc.description as loc_desc, ").append("tec.plate_name as plate_name, ").append("coord.x, coord.y, coord.z, coord.lat, coord.lon, coord.altitude, ").append("cit.name as city, ").append("stat.name as state, ").append("count.name as country, count.iso_code, ").append("mon.description as mon_desc, mon.inscription, mon.height, mon.foundation, mon.foundation_depth, ").append("stt.name as stt_name, stt.type as stt_type, ").append("geo.characteristic, geo.fracture_spacing, geo.fault_zone, geo.distance_to_fault, ").append("bed.condition as bed_condition, bed.type as bed_type ").append("FROM station as s ").append("JOIN location as loc ON s.id_location = loc.id ").append("JOIN coordinates as coord ON loc.id_coordinates = coord.id ").append("JOIN city as cit ON loc.id_city = cit.id ").append("JOIN state as stat ON cit.id_state = stat.id ").append("JOIN country as count ON stat.id_country = count.id ").append("JOIN tectonic as tec ON loc.id_tectonic = tec.id ").append("JOIN monument as mon ON s.id_monument = mon.id ").append("JOIN station_type as stt ON s.id_station_type = stt.id ").append("JOIN geological as geo ON s.id_geological = geo.id ").append("JOIN bedrock as bed ON geo.id_bedrock = bed.id ").append("WHERE s.id = ").append(rs.getInt("id")).append(";").toString());

                    while (rs_multi.next())
                    {

                        country = new Country(0, rs_multi.getString("country"), rs_multi.getString("iso_code"));
                        state = new State(0, country, rs_multi.getString("state"));
                        city = new City(0, state, rs_multi.getString("city"));
                        coordinates = new Coordinates(0, rs_multi.getFloat("x"), rs_multi.getFloat("y"), rs_multi.getFloat("z"), rs_multi.getFloat("lat"), rs_multi.getFloat("lon"), rs_multi.getFloat("altitude"));
                        tectonic = new Tectonic(0, rs_multi.getString("plate_name"));
                        bedrock = new Bedrock(0, rs_multi.getString("bed_condition"), rs_multi.getString("bed_type"));


                        s.setStation_type(0, rs_multi.getString("stt_name"), rs_multi.getString("stt_type"));
                        s.setLocation(new Location(0, city, coordinates, tectonic, rs_multi.getString("loc_desc")));
                        s.setMonument(new Monument(0, rs_multi.getString("mon_desc"), rs_multi.getString("inscription"), rs_multi.getString("height"), rs_multi.getString("foundation"), rs_multi.getString("foundation_depth")));
                        s.setGeological(new Geological(0, bedrock, rs_multi.getString("characteristic"), rs_multi.getString("fracture_spacing"), rs_multi.getString("fault_zone"), rs_multi.getString("distance_to_fault")));
                    }

                    rs_multi.close();
                    s_multi.close();

                    // *************************************************************
                    // Handle Colocation Offset
                    // *************************************************************
                    // Initialized statement for select
                    s_station_col = c.createStatement();
                    rs_station_col = s_station_col.executeQuery(new StringBuilder().append("SELECT * FROM station_colocation WHERE id_station = ").append(s.getId()).append(";").toString());
                    while (rs_station_col.next())
                    {
                        Station_colocation station_colocation;
                        Colocation_offset colocation_offset;
                        Station station_colocated;

                        // Initialized statement for select
                        s_col_offset = c.createStatement();
                        colocation_offset = new Colocation_offset();
                        rs_col_offset = s_col_offset.executeQuery(new StringBuilder().append("SELECT * FROM colocation_offset WHERE id_station_colocation = ").append(rs_station_col.getInt("id")).append(";").toString());
                        if (rs_col_offset.next())
                        {
                            // Save Colocation Offset data
                            colocation_offset = new Colocation_offset(rs_col_offset.getInt("id"), rs_col_offset.getFloat("offset_x"), rs_col_offset.getFloat("offset_y"), rs_col_offset.getFloat("offset_z"), rs_col_offset.getString("date_measured"));
                        }

                        rs_col_offset.close();
                        s_col_offset.close();

                        // Initialized statement for select
                        s_s_colocated = c.createStatement();
                        rs_s_colocated = s_s_colocated.executeQuery(new StringBuilder().append("SELECT * FROM station WHERE id=").append(rs_station_col.getInt("id_station_colocated")).append(";").toString());
                        station_colocated = new Station();
                        while (rs_s_colocated.next())
                        {
                            // Save Station Colocated data
                            station_colocated = new Station(rs_s_colocated.getInt("id"), rs_s_colocated.getString("name"), rs_s_colocated.getString("marker"), "", rs_s_colocated.getString("description"),
                                    rs_s_colocated.getString("date_from"), rs_s_colocated.getString("date_to"), rs_s_colocated.getString("comment"),
                                    rs_s_colocated.getString("iers_domes"), rs_s_colocated.getString("cpd_num"), rs_s_colocated.getInt("monument_num"),
                                    rs_s_colocated.getInt("receiver_num"), rs_s_colocated.getString("country_code"));
                        }

                        rs_s_colocated.close();
                        s_s_colocated.close();

                        // Save Station Collocation data
                        station_colocation = new Station_colocation(rs_station_col.getInt("id"), s, station_colocated);

                        // Add Station Collocation to Colocation Offset
                        colocation_offset.setStation_colocation(station_colocation);

                        // Add Collocation Offset to list
                        s.addStationColocationOffset(colocation_offset);
                    }

                    rs_station_col.close();
                    s_station_col.close();

                    // *************************************************************
                    // Handle Local Conditions and Effects
                    // *************************************************************

                    // Initialized statement for select
                    s_conditions = c.createStatement();
                    rs_conditions = s_conditions.executeQuery(new StringBuilder().append("SELECT ").append("condition.id AS condition_id, condition.id_station AS condition_id_station, condition.date_from AS condition_date_from, condition.date_to AS condition_date_to, condition.degradation AS condition_degradation, condition.comments AS condition_comments, ").append("effects.id AS effects_id, effects.type AS effects_type FROM condition ").append("INNER JOIN effects on effects.id = condition.id_effect ").append("WHERE condition.id_station = ").append(s.getId()).append(";").toString());

                    while (rs_conditions.next())
                    {
                        Effects effect = new Effects(rs_conditions.getInt("effects_id"), rs_conditions.getString("effects_type"));
                        // Add to conditions list
                        s.addCondition(new Condition(rs_conditions.getInt("condition_id"), s.getId(), effect, rs_conditions.getString("condition_date_from"), rs_conditions.getString("condition_date_to"), rs_conditions.getString("condition_degradation"), rs_conditions.getString("condition_comments")));
                    }

                    rs_conditions.close();
                    s_conditions.close();

                    // *************************************************************
                    // Handle Local Ties
                    // *************************************************************

                    // Initialized statement for select
                    s_local_ties = c.createStatement();
                    rs_local_ties = s_local_ties.executeQuery(new StringBuilder().append("SELECT * FROM local_ties WHERE id_station=").append(s.getId()).append(";").toString());
                    while (rs_local_ties.next())
                    {

                        // Add to local_ties list
                        s.addLocalTie(new Local_ties(
                                rs_local_ties.getInt("id"),
                                s.getId(),
                                rs_local_ties.getString("name"),
                                rs_local_ties.getString("usage"),
                                rs_local_ties.getString("cpd_num"),
                                rs_local_ties.getString("iers_domes"),
                                rs_local_ties.getFloat("dx"),
                                rs_local_ties.getFloat("dy"),
                                rs_local_ties.getFloat("dz"),
                                rs_local_ties.getFloat("accuracy"),
                                rs_local_ties.getString("survey_method"),
                                rs_local_ties.getString("date_at"),
                                rs_local_ties.getString("comment")));
                    }

                    rs_local_ties.close();
                    s_local_ties.close();

                    // *************************************************************
                    // Handle Collocation Instrument
                    // *************************************************************

                    // Initialized statement for select
                    s_coll_inst = c.createStatement();
                    rs_coll_inst = s_coll_inst.executeQuery( "SELECT * FROM instrument_collocation WHERE id_station=" + s.getId() + ";" );
                    while (rs_coll_inst.next())
                    {

                        // Add to Instrument_collocation list
                        s.addCollocationInstrument(new Instrument_collocation(
                                rs_coll_inst.getInt("id"),
                                rs_coll_inst.getInt("id_station"),
                                rs_coll_inst.getString("type"),
                                rs_coll_inst.getString("status"),
                                rs_coll_inst.getString("date_from"),
                                rs_coll_inst.getString("date_to"),
                                rs_coll_inst.getString("comment")));
                    }

                    rs_coll_inst.close();
                    s_coll_inst.close();


                    // *************************************************************
                    // Handle Access Control
                    // *************************************************************

                    /**
                     * Last modified: 05/12/2019 by José Manteigueiro
                     */

                    // Initialized statement for select
                    if(oldDBVersion)
                    // DB Version < 1.3.0
                    {
                        s_user_group_st = c.createStatement();
                        rs_user_group_st = s_user_group_st.executeQuery("select " +
                                "ugroupstation.id as ugroupstation_id, " +
                                "ugroupstation.id_stations as ugroupstation_idstations, " +
                                "ugroup.id as ugroup_id, " +
                                "ugroup.name as ugroup_name " +
                                "from user_group_station as ugroupstation " +
                                "inner join user_group as ugroup " +
                                "on ugroup.id = ugroupstation.id_user_groups " +
                                "where ugroupstation.id_stations = "
                                + s.getId() +";");
                        while (rs_user_group_st.next()) {
                            // *********************************************************
                            // Handle User Group
                            // *********************************************************

                            User_group user_group = new User_group(rs_user_group_st.getInt("ugroup_id"), rs_user_group_st.getString("ugroup_name"));
                            // Add to user_group_station list
                            s.addUserGroupStation(new User_group_station(rs_user_group_st.getInt("ugroupstation_id"), user_group, rs_user_group_st.getInt("ugroupstation_idstations")));
                        }

                        rs_user_group_st.close();
                        s_user_group_st.close();
                    }else{
                        s_user_group_st = c.createStatement();
                        rs_user_group_st = s_user_group_st.executeQuery("select " +
                                "ugroupstation.id as ugroupstation_id, " +
                                "ugroupstation.id_station as ugroupstation_idstation, " +
                                "ugroup.id as ugroup_id, " +
                                "ugroup.name as ugroup_name " +
                                "from user_group_station as ugroupstation " +
                                "inner join user_group as ugroup " +
                                "on ugroup.id = ugroupstation.id_user_group " +
                                "where ugroupstation.id_station = "
                                + s.getId() +";");
                        while (rs_user_group_st.next()) {
                            // *********************************************************
                            // Handle User Group
                            // *********************************************************

                            User_group user_group = new User_group(rs_user_group_st.getInt("ugroup_id"), rs_user_group_st.getString("ugroup_name"));
                            // Add to user_group_station list
                            s.addUserGroupStation(new User_group_station(rs_user_group_st.getInt("ugroupstation_id"), user_group, rs_user_group_st.getInt("ugroupstation_idstation")));
                        }

                        rs_user_group_st.close();
                        s_user_group_st.close();
                    }

                    // *************************************************************
                    // Handle Site Logs
                    // Modified on 22/03/2021 by José Manteigueiro
                    // *************************************************************

                    PreparedStatement _psLog = c.prepareStatement("SELECT l.*, lt.id AS lt_id, lt.name AS lt_name FROM log l INNER JOIN log_type lt ON l.id_log_type = lt.id WHERE l.id_station = ?;");
                    PreparedStatement _psContact = c.prepareStatement("SELECT c.*, a.abbreviation, a.address, a.infos, a.www, a.name AS agency_name, a.id AS agency_id FROM contact c INNER JOIN agency a ON a.id = c.id_agency WHERE c.id = ?;");
                    ResultSet _rsLog = null, _rsContact = null;
                    Agency _agency;
                    Contact _contact;
                    Log _log;

                    try {
                        _psLog.setInt(1, s.getId());
                        _rsLog = _psLog.executeQuery();

                        while (_rsLog.next()) {
                            // Log Object
                            _log = new Log();
                            _log.setId(_rsLog.getInt("id"));
                            try {
                                // May be NULL
                                _log.setDate(_rsLog.getString("date"));
                            } catch (SQLException _sqlException) {
                                continue;
                            }
                            _log.setTitle(_rsLog.getString("title"));
                            _log.setId_station(_rsLog.getInt("id_station"));
                            _log.setModified(_rsLog.getString("modified"));
                            _log.setPrevious(_rsLog.getString("previous"));
                            _log.setLog_type(_rsLog.getInt("lt_id"), _rsLog.getString("lt_name"));

                            try {
                                // id_contact may be null
                                _psContact.setInt(1, _rsLog.getInt("id_contact"));
                                _rsContact = _psContact.executeQuery();
                                if (_rsContact.next()) {
                                    _agency = new Agency();
                                    _agency.setId(_rsContact.getInt("agency_id"));
                                    _agency.setName(_rsContact.getString("agency_name"));
                                    _agency.setAbbreviation(_rsContact.getString("abbreviation"));
                                    _agency.setAddress(_rsContact.getString("address"));
                                    _agency.setInfos(_rsContact.getString("infos"));
                                    _agency.setWww(_rsContact.getString("www"));

                                    _contact = new Contact();
                                    _contact.setId(_rsContact.getInt("id"));
                                    _contact.setName(_rsContact.getString("name"));
                                    _contact.setComment(_rsContact.getString("comment"));
                                    _contact.setEmail(_rsContact.getString("email"));
                                    _contact.setGsm(_rsContact.getString("gsm"));
                                    _contact.setPhone(_rsContact.getString("phone"));
                                    _contact.setRole(_rsContact.getString("role"));
                                    _contact.setTitle(_rsContact.getString("title"));

                                    _contact.setAgency(_agency);

                                    _log.setContact(_contact); //paul
                                }
                            }
                            catch (SQLException _sqlException) {
                                Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                                _log.setContact(new Contact());
                            }

                            // Add to Log list
                            s.addLog(_log);
                        }
                    }
                    catch (SQLException _sqlException) {
                        Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                    }
                    finally {
                        if (_rsContact != null) {
                            try {
                                _rsContact.close();
                            } catch (SQLException _sqlException) {
                                Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                            }
                        }
                        if (_rsLog != null) {
                            try {
                                _rsLog.close();
                            } catch (SQLException _sqlException) {
                                Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                            }
                        }
                        if (_psContact != null) {
                            try {
                                _psContact.close();
                            } catch (SQLException _sqlException) {
                                Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                            }
                        }
                        if (_psLog != null) {
                            try {
                                _psLog.close();
                            } catch (SQLException _sqlException) {
                                Logger.getLogger(DataBaseConnection.class.getName()).log(Level.WARNING, null, _sqlException);
                            }
                        }
                    }

                    // *************************************************************
                    // Handle Organisational
                    // *************************************************************

                    // Initialized statement for select
                    s_station_cont = c.createStatement();
                    rs_station_cont = s_station_cont.executeQuery(new StringBuilder().append("SELECT ").append("stc.role as stc_role, ").append("cont.name as cont_name, cont.title, cont.email, cont.phone, cont.gsm, cont.comment, cont.role as cont_role, ").append("ag.name as ag_name, ag.abbreviation, ag.address, ag.www, ag.infos ").append("FROM station_contact as stc ").append("JOIN contact as cont on cont.id = stc.id_contact ").append("JOIN agency as ag on ag.id = cont.id_agency ").append("WHERE stc.id_station = ").append(s.getId()).append(";").toString());

                    while (rs_station_cont.next())
                    {
                        Agency agency = new Agency(
                                0,
                                rs_station_cont.getString("ag_name"),
                                rs_station_cont.getString("abbreviation"),
                                rs_station_cont.getString("address"),
                                rs_station_cont.getString("www"),
                                rs_station_cont.getString("infos"));

                        Contact contact = new Contact(
                                0,
                                rs_station_cont.getString("cont_name"),
                                rs_station_cont.getString("title"),
                                rs_station_cont.getString("email"),
                                rs_station_cont.getString("phone"),
                                rs_station_cont.getString("gsm"),
                                rs_station_cont.getString("comment"),
                                agency,
                                rs_station_cont.getString("cont_role"));

                        // Add to station contacts list
                        s.addStationConacts(new Station_contact(0, s.getId(), contact, rs_station_cont.getString("stc_role")));
                    }

                    rs_station_cont.close();
                    s_station_cont.close();

                    // Check if id_contact is present in network table
                    s_network = c.createStatement();
                    rs_network = s_network.executeQuery( "SELECT count(net.id) FROM network as net JOIN station_network as stn on net.id=stn.id_network WHERE net.id_contact is not null AND stn.id_station =" + s.getId() + ";" );
                    rs_network.next();
                    int nb_id_contact_in_network = rs_network.getInt(1);
                    // Initialized statement for select
                    s_station_net = c.createStatement();
                    if(nb_id_contact_in_network>0)
                    {

                        rs_station_net = s_station_net.executeQuery(new StringBuilder().append("SELECT ").append("net.name as net_name, ").append("cont.name as cont_name, cont.title, cont.email, cont.phone, cont.gsm, cont.comment, cont.role as cont_role, ").append("ag.name as ag_name, ag.abbreviation, ag.address, ag.www, ag.infos ").append("FROM station_network as stn ").append("JOIN network as net on net.id=stn.id_network ").append("JOIN contact as cont on cont.id=net.id_contact ").append("JOIN agency as ag on ag.id=cont.id_agency ").append("WHERE stn.id_station = ").append(s.getId()).append(";").toString());

                        while (rs_station_net.next())
                        {

                            Agency agency = new Agency(
                                    0,
                                    rs_station_net.getString("ag_name"),
                                    rs_station_net.getString("abbreviation"),
                                    rs_station_net.getString("address"),
                                    rs_station_net.getString("www"),
                                    rs_station_net.getString("infos"));

                            Contact contact = new Contact(
                                    0,
                                    rs_station_net.getString("cont_name"),
                                    rs_station_net.getString("title"),
                                    rs_station_net.getString("email"),
                                    rs_station_net.getString("phone"),
                                    rs_station_net.getString("gsm"),
                                    rs_station_net.getString("comment"),
                                    agency,
                                    rs_station_net.getString("cont_role"));


                            // Save Network data
                            Network network = new Network(0, rs_station_net.getString("net_name"), contact);

                            // Add to station network list
                            s.addStationNetwork(new Station_network(0, s.getId(), network));
                        }
                    }else{
                        rs_station_net = s_station_net.executeQuery(new StringBuilder().append("SELECT net.name as net_name \n").append("FROM station_network as stn \n").append("JOIN network as net on net.id=stn.id_network \n").append("WHERE stn.id_station=").append(s.getId()).append(";").toString());

                        while (rs_station_net.next() == true)
                        {
                            Station_network station_network = new Station_network();
                            Network network = new Network();


                            // Save Network data
                            network.setName(rs_station_net.getString("net_name"));

                            // Save Station Network

                            station_network.setNetwork(network);

                            // Add to station network list
                            s.addStationNetwork(station_network);
                        }
                    }

                    rs_station_net.close();
                    s_station_net.close();

                    // *************************************************************
                    // Handle Items
                    // *************************************************************

                    // Initialized statement for select
                    s_station_item = c.createStatement();
                    rs_station_item = s_station_item.executeQuery(new StringBuilder().append("SELECT ").append("si.id AS si_id, si.date_from AS si_date_from, si.date_to AS si_date_to, ").append("i.id AS i_id, i.comment AS i_comment,it.id AS it_id, ").append("it.name AS it_name ").append("FROM station_item AS si ").append("INNER JOIN item AS i ON i.id = si.id_item ").append("INNER JOIN item_type AS it ON it.id = i.id_item_type ").append("WHERE si.id_station = ").append(s.getId()).append(";").toString());

                    //While station item
                    while (rs_station_item.next())
                    {

                        Item_type item_type = new Item_type(rs_station_item.getInt("it_id"), rs_station_item.getString("it_name"));
                        Item item = new Item(rs_station_item.getInt("i_id"), item_type, rs_station_item.getString("i_comment"));

                        //Initialized statement for selecting item_attribute
                        s_item_attribute = c.createStatement();
                        rs_item_attribute = s_item_attribute.executeQuery(new StringBuilder().append("SELECT ").append("ia.id AS ia_id, ia.id_item AS ia_id_item,ia.date_from AS ia_date_from, ia.date_to AS ia_date_to, ia.value_varchar AS ia_value_varchar, ia.value_date AS ia_value_date, ia.value_numeric AS ia_value_numeric, ").append("a.id AS a_id, a.name AS a_name ").append("FROM item_attribute AS ia ").append("JOIN attribute AS a ON a.id = ia.id_attribute ").append("WHERE ia.id_item = ").append(rs_station_item.getInt("i_id")).append(";").toString());

                        //while attributes
                        while (rs_item_attribute.next()) {

                            Attribute attribute = new Attribute(rs_item_attribute.getInt("a_id"), rs_item_attribute.getString("a_name"));
                            Item_attribute item_attribute = new Item_attribute(
                                    rs_item_attribute.getInt("ia_id"),
                                    rs_item_attribute.getInt("ia_id_item"),
                                    attribute,
                                    rs_item_attribute.getString("ia_date_from"),
                                    rs_item_attribute.getString("ia_date_to"),
                                    rs_item_attribute.getString("ia_value_varchar"),
                                    rs_item_attribute.getString("ia_value_date"),
                                    rs_item_attribute.getFloat("ia_value_numeric"));
                            item.AddItemAttribute(item_attribute);

                        }

                        // Add station item to the list
                        s.addStationItem(new Station_item(
                                rs_station_item.getInt("si_id"),
                                s.getId(),
                                item,
                                rs_station_item.getString("si_date_from"),
                                rs_station_item.getString("si_date_to")));

                    }

                    rs_station_item.close();
                    s_station_item.close();

                    // *************************************************************
                    // Handle Documents
                    // *************************************************************

                    // Initialized statement for select
                    s_document = c.createStatement();
                    rs_document = s_document.executeQuery(new StringBuilder().append("SELECT ").append("d.id AS d_id, d.date AS d_date, d.title AS d_title, d.description AS d_description, d.link AS d_link, d.id_station AS d_id_station, d.id_item AS d_id_item, d.id_document_type AS d_id_document_type, ").append("dt.id as dt_id, dt.name AS dt_name ").append("FROM document AS d ").append("INNER JOIN document_type AS dt ON dt.id = d.id_document_type ").append("WHERE id_station = ").append(s.getId()).append(";").toString());

                    while (rs_document.next())
                    {

                        Document_type document_type = new Document_type(rs_document.getInt("dt_id"), rs_document.getString("dt_name"));

                        // *********************************************************
                        // Handle Item
                        // *********************************************************
                        Item item = new Item();

                        // Add document to station
                        s.addDocument(new Document(
                                rs_document.getInt("d_id"),
                                rs_document.getString("d_date"),
                                rs_document.getString("d_title"),
                                rs_document.getString("d_description"),
                                rs_document.getString("d_link"),
                                s.getId(),
                                item,
                                document_type));
                    }

                    rs_document.close();
                    s_document.close();

                    // Add object to ArrayList
                    results.add(s);
                }

                // Close connections
                rs.close();
                s_local.close();

            } catch ( Exception e ) {
                System.err.println( e.getClass().getName()+": "+ e.getMessage() );
            }

            return results;
        }
    }

    ArrayList<Station> getStationListV2(StationQuery sq, Boolean data){

        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Station> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            StringBuilder query = new StringBuilder();

            Boolean firstQuery = true;

            //Create the query to be executed. Only the necessary data will be fetch
            //Station name, station marker, networks, agencies, date_from, date_to, lat, lon, altitude, city, state, country
            if(sq == null) {
                
                query.append("select ug.name as epos, s.id, s.name, s.marker, s.date_to, ")
                     .append(" markerlongname(s.marker, s.monument_num, s.receiver_num, s.country_code) AS markerlongname ")
                     .append(" from station s ");
                
                if(oldDBVersion){
                    query.append("left join user_group_station ugs on ugs.id_stations = s.id ")
                         .append("left join user_group ug on ug.id = ugs.id_user_groups ");
                }else{
                    query.append("left join user_group_station ugs on ugs.id_station = s.id ")
                         .append("left join user_group ug on ug.id = ugs.id_user_group");
                }
                if(data) {
                    query.append(" WHERE EXISTS (SELECT 1 FROM rinex_file rf WHERE rf.id_station = s.id)");
                }
                query.append(" order by s.name;");
            }
            else{
                query.append("select ug.name as epos, s.id, s.name, s.marker, s.date_to, ")
                     .append("markerlongname(s.marker, s.monument_num, s.receiver_num, s.country_code) AS markerlongname ")
                     .append(" from station s ");
                if(oldDBVersion){
                    query.append("left join user_group_station ugs on ugs.id_stations = s.id ")
                         .append("left join user_group ug on ug.id = ugs.id_user_groups ");
                }else{
                    query.append("left join user_group_station ugs on ugs.id_station = s.id ")
                         .append("left join user_group ug on ug.id = ugs.id_user_group ");
                }
                    query.append(" WHERE ");

                // Name Filter
                if(sq.getNames().size() > 0) {
                    ArrayList<String> names = sq.getNames();
                    for (int i = 0; i < names.size(); i++) {
                        if(i+1 == names.size())
                            query.append(" s.name ilike '").append(names.get(i)).append("%' ");
                        else
                            query.append(" s.name ilike '").append(names.get(i)).append("%' OR ");
                    }
                    firstQuery = false;
                }

                // Marker Filter
                if(sq.getMarkers().size() > 0) {
                    if(!firstQuery)
                        query.append(" OR ");
                    ArrayList<String> markers = sq.getMarkers();
                    for (int i = 0; i < markers.size(); i++) {
                        if(i+1 == markers.size())
                            query.append(" s.marker = '").append(markers.get(i)).append("' ");
                        else
                            query.append(" s.marker = '").append(markers.get(i)).append("' OR ");
                    }
                    firstQuery = false;
                }

                // Markerlongname Filter
                if(sq.getMarkerlongnames().size() > 0) {
                    if(!firstQuery)
                        query.append(" OR ");
                    ArrayList<String> markerlnames = sq.getMarkerlongnames();
                    for (int i = 0; i < markerlnames.size(); i++) {
                        if(i+1 == markerlnames.size())
                            query.append(" markerlongname(s.marker, s.monument_num, s.receiver_num, s.country_code) ilike '").append(markerlnames.get(i)).append("%' ");
                        else
                            query.append(" markerlongname(s.marker, s.monument_num, s.receiver_num, s.country_code) ilike '").append(markerlnames.get(i)).append("%' OR ");
                    }
                    firstQuery = false;
                }

                /* EPOS Filter

                    1 - EPOS
                    2 - Non-EPOS
                */

                if(!sq.getShowEpos()){
                    if(!firstQuery)
                        query.append(" AND ");
                    query.append(" ug.id = 2");
                }
                if(!sq.getShowNonEpos()){
                    if(!firstQuery)
                        query.append(" AND ");
                    query.append(" ug.id = 1");
                }
                query.append(" ORDER BY s.name");
            }
            //Execute the query

            results = executeStationListV2(query.toString(), c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }


    private ArrayList<Station> executeStationListV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<Station> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            Station s = new Station();

            // Station data
            s.setId(rs.getInt("id"));
            s.setName(rs.getString("name"));
            s.setMarker(rs.getString("marker"));
            s.setDate_to(rs.getString("date_to"));
            try {
                s.setMarkerLongName(rs.getString("markerlongname"));
            }catch(Exception e){
                System.err.format("Error generating markerlongname for marker: " + s.getMarker());
                s.setMarkerLongName("---");
            }

            // Try to get EPOS information
            try{
                s.setUserGroupStationName(rs.getString("epos"));
            }catch (Exception e){
                System.err.format("Error obtaining user group station for marker: " + s.getMarker());
            }

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    ArrayList<Agency> getAgencyListV2(){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Agency> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "select name, abbreviation, address, www, infos from agency order by name";

            //Execute the query
            results = executeAgencyListV2(query, c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<Agency> executeAgencyListV2(String query, Connection c) throws SQLException{

        //ArrayList for the results
        ArrayList<Agency> results = new ArrayList<>();

        try {
            // Initialize statement, execute query for the result set
            s = c.createStatement();
            rs = s.executeQuery(query);

            while (rs.next()) {
                Agency s = new Agency();

                // Agency data
                s.setName(rs.getString("name"));
                s.setAbbreviation(rs.getString("abbreviation"));
                s.setAddress(rs.getString("address"));
                s.setWww(rs.getString("www"));
                s.setInfos(rs.getString("infos"));

                // Add to the result set
                results.add(s);
            }

        }catch ( SQLException ex) {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }finally {
            // Close connections
            rs.close();
            s.close();
        }

        return results;
    }

    ArrayList<Network> getNetworkListV2(){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Network> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "select name from network order by name";

            //Execute the query
            results = executeNetworkListV2(query, c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<Network> executeNetworkListV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<Network> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            Network s = new Network();

            // Station data
            s.setName(rs.getString("name"));

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    ArrayList<Country> getCountryListV2(){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Country> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "select name, iso_code from country order by name";

            //Execute the query
            results = executeCountryListV2(query, c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<Country> executeCountryListV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<Country> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            Country s = new Country();

            // Station data
            s.setName(rs.getString("name"));
            s.setIso_code(rs.getString("iso_code"));

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    ArrayList<State> getStateListV2(){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<State> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "select s.name, c.id as country_id, c.name as country_name, c.iso_code from state s inner join country c on c.id = s.id_country order by s.name;";

            //Execute the query
            results = executeStateListV2(query, c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<State> executeStateListV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<State> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            State s = new State();
            s.setName(rs.getString("name"));
            s.setCountry(rs.getInt("country_id"), rs.getString("country_name"), rs.getString("iso_code"));

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    ArrayList<City> getCityListV2(){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<City> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "select ci.name, st.name as state_name, st.id as state_id, ct.name as country_name, ct.iso_code, ct.id as country_id from city ci inner join state st on st.id = ci.id_state inner join country ct on ct.id = st.id_country order by ci.name;";

            //Execute the query
            results = executeCityListV2(query, c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<City> executeCityListV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<City> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            Country ct = new Country();
            ct.setId(rs.getInt("country_id"));
            ct.setIso_code(rs.getString("iso_code"));
            ct.setName(rs.getString("country_name"));

            City s = new City();
            s.setName(rs.getString("name"));
            s.setState(rs.getInt("state_id"), ct, rs.getString("state_name"));

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }


    /**
     * Last modified: 05/12/2018 by José Manteigueiro
     * Modified for version gnss-europe-v1.1.0 of DB
     *  Only queries for value_varchar of receiver now
     */
    ArrayList<Receiver_type> getReceiverTypeListV2(){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Receiver_type> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "SELECT DISTINCT value_varchar FROM item_attribute WHERE id_attribute = " + GlassConstants.getDbItemAttributeReceiverId() + ";";

            //Execute the query
            results = executeReceiverTypeListV2(query, c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }


    /**
     * Last modified: 05/12/2018 by José Manteigueiro
     * Modified for version gnss-europe-v1.1.0 of DB
     *  Dropped 'igs_defined' and 'model' from the result set
     * TODO: There are blank spaces in value_varchar field that mess with 'distinct' query option
     */
    private ArrayList<Receiver_type> executeReceiverTypeListV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<Receiver_type> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            Receiver_type s = new Receiver_type();
            s.setName(rs.getString("value_varchar"));
            //s.setIgs_defined(rs.getString("igs_defined"));
            //s.setModel(rs.getString("model"));

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * Last modified: 05/12/2018 by José Manteigueiro
     * Modified for version gnss-europe-v1.1.0 of DB
     * Only queries for value_varchar of antenna now
     */
    ArrayList<Antenna_type> getAntennaTypeListV2(){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Antenna_type> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "SELECT DISTINCT value_varchar FROM item_attribute WHERE id_attribute = " + GlassConstants.getDbItemAttributeAntennaId() + ";";

            //Execute the query
            results = executeAntennaTypeListV2(query, c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * Last modified: 05/12/2018 by José Manteigueiro
     * Modified for version gnss-europe-v1.1.0 of DB
     * Dropped 'igs_defined' and 'model' from the result set
     * TODO: There are blank spaces in value_varchar field that mess with 'distinct' query option
     */
    private ArrayList<Antenna_type> executeAntennaTypeListV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<Antenna_type> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            Antenna_type s = new Antenna_type();
            s.setName(rs.getString("value_varchar"));
            //s.setIgs_defined(rs.getString("igs_defined"));
            //s.setModel(rs.getString("model"));

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    /**
     * Last modified: 05/12/2018 by José Manteigueiro
     * Modified for version gnss-europe-v1.1.0 of DB
     *  Only queries for value_varchar of radome now
     */
    ArrayList<Radome_type> getRadomeTypeListV2(){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Radome_type> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "SELECT DISTINCT value_varchar FROM item_attribute WHERE id_attribute = " + GlassConstants.getDbItemAttributeRadomeId() + ";";

            //Execute the query
            results = executeRadomeTypeListV2(query, c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /**
     * Last modified: 05/12/2018 by José Manteigueiro
     * Modified for version gnss-europe-v1.1.0 of DB
     * Dropped 'igs_defined' and 'model' from the result set
     * TODO: There are blank spaces in value_varchar field that mess with 'distinct' query option
     */
    private ArrayList<Radome_type> executeRadomeTypeListV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<Radome_type> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            Radome_type s = new Radome_type();
            s.setName(rs.getString("value_varchar"));
            //s.setDescription(rs.getString("description"));
            //s.setIgs_defined(rs.getString("igs_defined"));

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    ArrayList<File_type> getFileTypeListV2(){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<File_type> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "select id, format, sampling_window, sampling_frequency from file_type order by id;";

            //Execute the query
            results = executeFileTypeListV2(query, c);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<File_type> executeFileTypeListV2(String query, Connection c) throws SQLException{
        //ArrayList for the results
        ArrayList<File_type> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);

        while(rs.next()){
            File_type s = new File_type();
            s.setFormat(rs.getString("format"));
            s.setSampling_window(rs.getString("sampling_window"));
            s.setSampling_frequency(rs.getString("sampling_frequency"));

            // Add to the result set
            results.add(s);
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    ArrayList<Integer> getCoordinatesRectangleV2(double minLat, double minLon, double maxLat, double maxLon){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Integer> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "select * from coordinates;";

            //Execute the query
            results = executeCoordinatesRectangleV2(query, c, minLat, minLon, maxLat, maxLon);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<Integer> executeCoordinatesRectangleV2(String query, Connection c, double minLat, double minLon, double maxLat, double maxLon) throws SQLException{
        //ArrayList for the results
        ArrayList<Integer> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);
        double lat, lon;
        boolean inside = false;

        while(rs.next()){
            lat = rs.getDouble("lat");
            lon = rs.getDouble("lon");

            if(lat <= maxLat && lat >= minLat && lon <= maxLon && lon >= minLon)
                inside = true;
            if(inside) {
                // Add to the result set
                results.add(rs.getInt("id"));
                inside = false;
            }
        }

        // Close connections
        rs.close();
        s.close();

        //No results for given coordinates
        //Return no data to user
        if(results.isEmpty())
            results.add(-1);

        return results;
    }

    ArrayList<Integer> getCoordinatesCircleV2(double centerLat, double centerLon, double radius){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Integer> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = "select * from coordinates;";

            //Execute the query
            results = executeCoordinatesCircleV2(query, c, centerLat, centerLon, radius);

        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    private ArrayList<Integer> executeCoordinatesCircleV2(String query, Connection c, double centerLat, double centerLon, double radius) throws SQLException{
        //ArrayList for the results
        ArrayList<Integer> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);
        double lat, lon, distance;

        while(rs.next()){
            lat = rs.getFloat("lat");
            lon = rs.getFloat("lon");
            distance = getDistanceCoordinates(centerLat, centerLon, lat, lon);
            if(distance <= radius) {
                // Add to the result set
                results.add(rs.getInt("id"));
            }
        }

        // Close connections
        rs.close();
        s.close();

        //No results for given coordinates
        //Return no data to user
        if(results.isEmpty())
            results.add(-1);

        return results;
    }

    private ArrayList<Integer> executeCoordinatesPolygonV2(String query, Connection c, ArrayList polygon) throws SQLException{
        //ArrayList for the results
        ArrayList<Integer> results = new ArrayList<>();

        // Initialize statement, execute query for the result set
        s = c.createStatement();
        rs = s.executeQuery(query);
        double lat, lon;

        while(rs.next()){
            lat = rs.getFloat("lat");
            lon = rs.getFloat("lon");
            if (Point.insidePolygon(lat, lon, polygon))
                results.add(rs.getInt("id"));
        }

        // Close connections
        rs.close();
        s.close();

        //No results for given coordinates
        //Return no data to user
        if(results.isEmpty())
            results.add(-1);

        return results;
    }

    ArrayList<Integer> getCountryV2(StationQuery sq){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Integer> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            String query;

            //Get data
            ArrayList<String> countries = sq.getCountry();

            //Create the query to be executed. Only the necessary data will be fetch
            query = "select s.id from station s inner join location l on l.id = s.id_location inner join city on city.id = l.id_city inner join state on state.id = city.id_state inner join country on country.id = state.id_country where ";

            for(int i = 0; i < countries.size(); i++) {
                query = query + " country.name ilike '" + countries.get(i) + "%' ";
                if (i < countries.size()-1)
                    query = query + (" OR ");
            }


            //Execute query
            s = c.createStatement();
            rs = s.executeQuery(query);

            while(rs.next())
                results.add(rs.getInt("id"));

            // Close connections
            rs.close();
            s.close();
        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    ArrayList<Integer> getStateV2(StationQuery sq){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Integer> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            StringBuilder query;

            //Get data
            ArrayList<String> states = sq.getState();

            //Create the query to be executed. Only the necessary data will be fetch
            query = new StringBuilder().append("select s.id from station s inner join location l on l.id = s.id_location ").append(" inner join city on city.id = l.id_city ").append(" inner join state on state.id = city.id_state ").append(" where ");

            for(int i = 0; i < states.size(); i++) {
                query.append(" state.name ilike '" + states.get(i) + "%' ");
                if (i < states.size()-1)
                    query.append(" OR ");
            }


            //Execute query
            s = c.createStatement();
            rs = s.executeQuery(query.toString());

            while(rs.next())
                results.add(rs.getInt("id"));

            // Close connections
            rs.close();
            s.close();
        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    ArrayList<Integer> getCityV2(StationQuery sq){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Integer> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            StringBuilder query;

            //Get data
            ArrayList<String> cities = sq.getCity();

            //Create the query to be executed. Only the necessary data will be fetch
            query = new StringBuilder().append("select s.id from station s inner join location l on l.id = s.id_location ").append(" inner join city on city.id = l.id_city ").append(" where ");

            for(int i = 0; i < cities.size(); i++) {
                query.append(" city.name ilike '" + cities.get(i) + "%' ");
                if (i < cities.size()-1)
                    query.append(" OR ");
            }


            //Execute query
            s = c.createStatement();
            rs = s.executeQuery(query.toString());

            while(rs.next())
                results.add(rs.getInt("id"));

            // Close connections
            rs.close();
            s.close();
        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    ArrayList<Integer> getStationTypeV2(StationQuery sq){
        //Connection
        Connection c = null;

        //ArrayList for the results
        ArrayList<Integer> results = new ArrayList<>();

        try
        {
            //Start the connection
            c = NewConnection();
            StringBuilder query;

            //Create the query to be executed. Only the necessary data will be fetch
            query = new StringBuilder().append("select distinct s.id from station s inner join station_type st where st.id = s.id_station_type ");
            query.append(" WHERE ");

            query.append(" st.name ilike '").append(sq.getStationType()).append("%' ");

            //Execute query
            s = c.createStatement();
            rs = s.executeQuery(query.toString());

            while(rs.next())
                results.add(rs.getInt("id"));

            // Close connections
            rs.close();
            s.close();
        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    ArrayList<Integer> getInsidePolygonIDs(ArrayList<Coordinates> polygon)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Station> results = new ArrayList<>();
        // ArrayList with all Coordinates
        ArrayList<Coordinates> coordinates_all;
        // ArrayList with ids from polygon coordinates
        ArrayList<Integer> in_polygon = new ArrayList<>();

        try {
            c = NewConnection();

            // Query to be executed
            StringBuilder query = new StringBuilder("SELECT * FROM coordinates ORDER BY id;");

            // Execute the query to get list of coordinates
            coordinates_all = executeCoordinatesQuery(query.toString(), c);

            // Check for coordinates inside the polygon
            for (Coordinates coordinate : coordinates_all) {
                if (Point.insidePolygon(coordinate.getLat(), coordinate.getLon(), polygon))
                    in_polygon.add(coordinate.getId());
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        //No results for given coordinates
        //Return no data to user
        if(in_polygon.isEmpty())
            in_polygon.add(-1);

        return in_polygon;
    }

    private ArrayList<Coordinates> executeCoordinatesQuery(String query, Connection c) throws SQLException
    {
        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************

        ArrayList<Coordinates> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add state to result ArrayList
            results.add(new Coordinates(
                    rs.getInt("id"),
                    rs.getDouble("x"),
                    rs.getDouble("y"),
                    rs.getDouble("z"),
                    rs.getDouble("lat"),
                    rs.getDouble("lon"),
                    rs.getDouble("altitude")));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }

    Station getStationByMarker(String station_marker){

        //Connection
        Connection c = null;

        //ArrayList for the results
        Station result = new Station();

        try
        {
            //Start the connection
            c = NewConnection();

            //Create the query to be executed. Only the necessary data will be fetch
            //Station name, station marker, networks, agencies, date_from, date_to, lat, lon, altitude, city, state, country
            StringBuilder query = new StringBuilder("select s.id, s.name, s.marker, array_to_string(array_agg(distinct(net.name)), ' & ') as networks,")
                    .append(" array_to_string(array_agg(distinct(agn.name)), ' & ') as agencies, s.date_from, s.date_to, ")
                    .append(" crd.x, crd.y, crd.z, crd.lat, crd.lon, crd.altitude, ct.name as city, stt.name as state, ")
                    .append(" cnt.name as country from station s inner join location l on s.id_location = l.id inner join coordinates crd on l.id_coordinates = crd.id ")
                    .append(" inner join city ct on l.id_city = ct.id inner join state stt on ct.id_state = stt.id inner join country cnt on stt.id_country = cnt.id ")
                    .append(" inner join station_network sn on s.id = sn.id_station inner join network net on sn.id_network = net.id ")
                    .append(" inner join station_contact sc on s.id = sc.id_station inner join contact cntc on sc.id_contact = cntc.id ")
                    .append(" inner join agency agn on cntc.id_agency = agn.id  inner join station_type styp on styp.id = s.id_station_type ");

            query.append(" where s.marker = '").append(station_marker).append("' ");

            //Append the group by
            query.append(" group by s.id, s.name, s.marker, s.date_to, s.date_from, crd.x, crd.y, ")
                    .append("crd.z, crd.lat, crd.lon, crd.altitude, ct.name, stt.name, cnt.name ");


            // Initialize statement, execute query for the result set
            s = c.createStatement();
            rs = s.executeQuery(query.toString());

            while(rs.next()){
                Station s = new Station();

                // Station data
                s.setId(rs.getInt("id"));
                s.setName(rs.getString("name"));
                s.setMarker(rs.getString("marker"));
                s.setDate_from(rs.getString("date_from"));
                s.setDate_to(rs.getString("date_to"));

                // Location data
                Country country = new Country(0, rs.getString("country"), "");
                State state = new State(0, country, rs.getString("state"));
                City city = new City(0, state, rs.getString("city"));
                Coordinates coordinates = new Coordinates(0, rs.getDouble("x"), rs.getDouble("y"), rs.getDouble("z"), rs.getDouble("lat"), rs.getDouble("lon"), rs.getDouble("altitude"));
                Location location = new Location(0, city, coordinates, null, "");
                s.setLocation(location);

                Agency agency = new Agency(0, rs.getString("agencies"), "", "", "", "");
                Contact contact = new Contact(0, "", "", "", "", "", "", agency, "");
                Station_contact station_contact = new Station_contact(0, s.getId(), contact, "");
                s.addStationContacts(station_contact);

                Network network = new Network(0, rs.getString("networks"), null);
                Station_network station_network = new Station_network(0, s.getId(), network);
                s.addStationNetwork(station_network);

                // Add to the result set
                result = s;
            }

            // Close connections
            rs.close();
            s.close();
        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return result;
    }
    
    String getMarkerLongName(String station_marker){

        //Connection
        Connection c = null;

        //String for the result
        String markerlongname = null;

        try
        {
            //Start the connection
            c = NewConnection();

            PreparedStatement prepS = c.prepareStatement(
                    "SELECT markerlongname(s.marker, s.monument_num, s.receiver_num, s.country_code) AS markerlongname" +
                    " FROM station s WHERE s.marker ILIKE ?");


            prepS.setString(1, "" + station_marker + "%");
            ResultSet rs = prepS.executeQuery();

            if (rs.next())
                markerlongname = rs.getString("markerlongname");

            rs.close();
            prepS.close();
        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return markerlongname;
    }

    /*
    This Function now assumes that the connection has already been opened and will be reused.
     */
    Map<String, Integer> getAttributeIDs(Connection c) throws SQLException {
        HashMap res = new HashMap();

        //Connection
        //Connection c = null;
        //try {
            //c = NewConnection();
            // Initialize statement
            s = c.createStatement();

            // execute query for the result set
            rs = s.executeQuery("SELECT id FROM attribute WHERE \"name\" = 'antenna_type';");

            int antennaID = 0;
            while (rs.next()) {
                antennaID = rs.getInt("id");
            }

            // Close result set
            rs.close();

            rs = s.executeQuery("SELECT id FROM attribute WHERE \"name\" = 'radome_type';");
            int radomeID = 0;
            while (rs.next()) {
                radomeID = rs.getInt("id");
            }


            // Close result set
            rs.close();

            rs = s.executeQuery("SELECT id FROM attribute WHERE \"name\" = 'receiver_type';");
            int receiverID = 0;
            while (rs.next()) {
                receiverID = rs.getInt("id");
            }

            s.close();

            res.put("antenna", antennaID);
            res.put("radome", radomeID);
            res.put("receiver", receiverID);
            return res;
       /* } catch ( ClassNotFoundException e) {
            e.printStackTrace();
            return res;
        }*/
        /*finally{
            try{
                c.close();
            } catch (SQLException e) {
                System.out.println("Error Closing getAttributeIDs Sql Connection");
                throw e;
            }
        }*/
    }

    int getStationIdByMarker(String station_marker){

        Connection _c = null;
        PreparedStatement _ps = null;
        ResultSet _rs = null;
        int _id = -1;

        try
        {
            //Start the connection
            _c = NewConnection();
            _ps = _c.prepareStatement("SELECT id FROM station WHERE marker = ?;");
            _ps.setString(1, station_marker);
            _rs = _ps.executeQuery();

            if(_rs.next()){
                _id = rs.getInt("id");
            }
        }
        catch (SocketException | ClassNotFoundException | SQLException ex)
        {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, ex);
        }
        finally
        {
            if (_rs != null) {
                try {
                    _rs.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if (_ps != null) {
                try {
                    _ps.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
            if (_c != null) {
                try {
                    _c.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseConnection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }
        }

        return _id;
    }
}
