package Glass;

import Configuration.DBConsts;
import Configuration.SiteConfig;
import CustomClasses.FilesInfo;
import CustomClasses.Tuple2;
import EposTables.*;
import java.net.SocketException;
import java.sql.*;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.time.temporal.ChronoUnit;
import java.util.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;

import static java.lang.Integer.parseInt;

/*
* DATABASE CONNECTION T2
* This class is responsible for the EPOS database connection and provides several
* methods for executing queries on the database.
*/
public class DataBaseT2Connection {
    
    @EJB
    SiteConfig siteConfig;
    private String myIP = null;      // This String will store the ip address of the machine
    private String DBConnectionString=null;
    
    private Connection c = null;                // Connection to EPOS database
    private Statement s = null;                 // Statement for queries on the EPOS database
    private ResultSet rs = null;                // Result Set for the queries on the EPOS database
    private Statement s_file_generated = null;  // Statement for the file generated queries on the EPOS database
    private ResultSet rs_file_generated = null; // Result Set for the file generated on the EPOS database
    private Statement s_file = null;            // Statement for the file queries on the EPOS database
    private ResultSet rs_file = null;           // Result Set for the file on the EPOS database
    private Statement s_file_type = null;       // Statement for the file type queries on the EPOS database
    private ResultSet rs_file_type = null;      // Result Set for the file type on the EPOS database
    private Statement s_station = null;         // Statement for the file station queries on the EPOS database
    private ResultSet rs_station = null;        // Result Set for the file station queries on the EPOS database
    private Statement s_rinex_file = null;      // Statement for the rinex file queries on the EPOS database
    private ResultSet rs_rinex_file = null;     // Result Set for the rinex file on the EPOS database
    private Statement s_quality_file = null;    // Statement for the quality file queries on the EPOS database
    private ResultSet rs_quality_file = null;   // Result Set for the quality file on the EPOS database
    private Statement s_data_center = null;     // Statement for the data center queries on the EPOS database
    private ResultSet rs_data_center = null;    // Result Set for the data center on the EPOS database
    private Statement s_data_center_structure = null;     // Statement for the data center structure queries on the EPOS database
    private ResultSet rs_data_center_structure = null;    // Result Set for the data center structure on the EPOS database

    //HUGO
    private Statement s_other_files = null;     // Statement for the other files queries on the EPOS database
    private ResultSet rs_other_files = null;    // Result Set for the other files on the EPOS database
    private Statement s_rinex_errors = null;     // Statement for the rinex errors queries on the EPOS database
    private ResultSet rs_rinex_errors = null;    // Result Set for the rinex errors on the EPOS database
    private Statement s_rinex_error_types = null;     // Statement for the rinex error types queries on the EPOS database
    private ResultSet rs_rinex_error_types = null;    // Result Set for the rinex error types on the EPOS database
    private Statement s_node = null;     
    private ResultSet rs_node = null;
    
    public DataBaseT2Connection() {
       this.siteConfig = lookupSiteConfigBean();
       myIP= //"10.0.7.65";
            siteConfig.getLocalIpValue();
       DBConnectionString = siteConfig.getDBConnectionString();
    }
     
    // =========================================================================
    // IP & CONNECTION WITH DATABASE
    // =========================================================================
    private Connection NewConnection(Boolean AutoComit) throws SocketException, ClassNotFoundException, SQLException {

        // Create connection with the local database
        Class.forName("org.postgresql.Driver");
        Connection c = DriverManager.getConnection(DBConnectionString,DBConsts.DB_USERNAME, DBConsts.DB_PASSWORD);
        c.setAutoCommit(AutoComit);
        return c;
    }
    
    private Connection NewConnection() throws SocketException, ClassNotFoundException, SQLException {
        Class.forName("org.postgresql.Driver");
        Properties props = new Properties();
        props.setProperty("user",DBConsts.DB_USERNAME);
        props.setProperty("password",DBConsts.DB_PASSWORD);
        Connection c = DriverManager.getConnection(DBConnectionString, props);
        c.setAutoCommit(false);
        return c;
    }
    
    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }
    
    // =========================================================================
    // T2 METHODS
    // =========================================================================
    /*
    * EXECUTE RINEX FILES QUERY
    * This method executes a given query on the rinex file inserted on the EPOS
    * database and returns a rinex file.
    */
    private ArrayList executeRinexFilesForCombinationQuery(ArrayList<Rinex_file> results, String query, 
            Connection c) throws SQLException
    {
        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************

        // *********************************************************************
        // Handle Files
        // *********************************************************************
        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);
        
        
        // Check if there are any results
        while (rs.next() == true)
        {
            // Create Rinex File object
            Rinex_file rinex_file = new Rinex_file();
            // *****************************************************************
            // Handle File Type
            // *****************************************************************
            File_type file_type = new File_type();
            
            file_type.setFormat(rs.getString("format"));
            file_type.setSampling_window(rs.getString("sampling_window"));
            file_type.setSampling_frequency(rs.getString("sampling_frequency"));
           
            
            // *****************************************************************
            // Handle Data Center
            // *****************************************************************
            Data_center data_center = new Data_center();
            
            data_center.setAcronym(rs.getString("acronym"));
            data_center.setHostname(rs.getString("hostname"));
            data_center.setProtocol(rs.getString("protocol"));
            
            
            // *****************************************************************
            // Handle Data Center Structure
            // *****************************************************************
            Data_center_structure data_center_structure = new Data_center_structure();
            
            data_center_structure.setDirectory_naming(rs.getString("directory_naming"));
            
         
            // Add to Data_center
            data_center_structure.setDataCenter(data_center);
            
            // *****************************************************************
            // Handle Rinex File
            // *****************************************************************
            rinex_file.setName(rs.getString("name"));
            rinex_file.setData_center_structure(data_center_structure.getId(), data_center_structure.getDirectory_naming(),data_center_structure.getDataCenter());
            rinex_file.setFile_size(rs.getInt("file_size"));
            rinex_file.setFile_type(file_type.getId(), file_type.getFormat(),
                    file_type.getSampling_window(), file_type.getSampling_frequency());
            rinex_file.setRelative_path(rs.getString("relative_path"));
            rinex_file.setReference_date(rs.getString("reference_date"));
            rinex_file.setCreation_date(rs.getString("creation_date")); 
            rinex_file.setPublished_date(rs.getString("published_date"));
            rinex_file.setRevision_date(rs.getString("revision_date"));
            rinex_file.setMd5checksum(rs.getString("md5checksum"));
            //rinex_file.setMd5uncompressed(rs.getString("md5uncompressed"));
            rinex_file.setStatus(rs.getInt("status"));
            
            results.add(rinex_file);
                           
        }
        
        rs.close();
        s.close();
        
        return results;
    }
    
    /*
    * EXECUTE RINEX FILES QUERY
    * This method executes a given query on the stations inserted on the EPOS
    * dataase and returns a ArrayList with the results.
    */
    private ArrayList executeRinexFilesQuery(ArrayList<Rinex_file> results, String query,
            Connection c) throws SQLException
    {
        // *********************************************************************
        // STEP 1
        // Check data from query.
        // *********************************************************************

        // *********************************************************************
        // Handle Files
        // *********************************************************************
        // Initialized statement for select

        s = c.createStatement();
        rs = s.executeQuery(query);
        
        // Check if there are any results
        while (rs.next() == true)
        {
            // Create Rinex File object
            Rinex_file rinex_file = new Rinex_file();
            Quality_file quality_file = new Quality_file();
            
            // *****************************************************************
            // Handle Quality File
            // *****************************************************************
            s_quality_file = c.createStatement();
            rs_quality_file = s_quality_file.executeQuery(new StringBuilder().append("SELECT * FROM ").append("quality_file WHERE id_rinexfile=").append(rs.getInt("id")).toString());
            
            while (rs_quality_file.next())
            {
                quality_file.setId(rs_quality_file.getInt("id"));
                quality_file.setName(rs_quality_file.getString("name"));
                quality_file.setFile_size(rs_quality_file.getInt("file_size"));
                quality_file.setRelative_path(rs_quality_file.getString("relative_path"));
                quality_file.setCreation_date(rs_quality_file.getString("creation_date"));
                quality_file.setRevision_time(rs_quality_file.getString("revision_time"));
                quality_file.setMd5checksum(rs_quality_file.getString("md5checksum"));
                quality_file.setStatus(rs_quality_file.getInt("status"));
                
                // *****************************************************************
                // Handle Data Center Structure
                // *****************************************************************
                Data_center_structure data_center_structure = new Data_center_structure();
                s_data_center_structure = c.createStatement();
                rs_data_center_structure = s_data_center_structure.executeQuery(new StringBuilder().append("SELECT * FROM ").append("data_center_structure WHERE id_data_center=").append(rs_quality_file.getInt("id_data_center_structure")).toString());
                rs_data_center_structure.next();

                data_center_structure.setId(rs_data_center_structure.getInt("id"));
                data_center_structure.setDirectory_naming(rs_data_center_structure.getString("directory_naming"));

                // *****************************************************************
                // Handle Data Center
                // *****************************************************************
                Data_center data_center = new Data_center();
                s_data_center = c.createStatement();
                rs_data_center = s_data_center.executeQuery(new StringBuilder().append("SELECT * FROM ").append("data_center WHERE id=").append(rs_quality_file.getInt("id_data_center")).toString());

                while (rs_data_center.next())
                {
                    data_center.setId(rs_data_center.getInt("id"));
                    data_center.setAcronym(rs_data_center.getString("acronym"));
                    data_center.setHostname(rs_data_center.getString("hostname"));
                    data_center.setProtocol(rs_data_center.getString("protocol"));
                    data_center.setName(rs_data_center.getString("name"));
                    data_center.setRoot_path(rs_data_center.getString("root_path"));
                }
                
                rs_data_center.close();
                s_data_center.close();
                
                quality_file.setData_center_structure(data_center_structure.getId(),data_center_structure.getDirectory_naming(), data_center);
                        
               
                // *************************************************************
                // Handle File Type
                // *************************************************************
                File_type file_type = new File_type();
                s_file_type = c.createStatement();
                rs_file_type = s_file_type.executeQuery(new StringBuilder().append("SELECT * FROM ").append("file_type WHERE id=").append(rs_quality_file.getInt("id_file_type")).toString());
                
                while (rs_file_type.next())
                {
                    file_type.setId(rs_file_type.getInt("id"));
                    file_type.setFormat(rs_file_type.getString("format"));
                    file_type.setSampling_window(rs_file_type.getString("sampling_window"));
                    file_type.setSampling_frequency(rs_file_type.getString("sampling_frequency"));
                }
                
                rs_file_type.close();
                s_file_type.close();
                
                quality_file.setFile_type(file_type.getId(), file_type.getFormat(),file_type.getSampling_window(), file_type.getSampling_frequency());
            }
            
            // *****************************************************************
            // Handle File Type
            // *****************************************************************
            File_type file_type = new File_type();
            s_file_type = c.createStatement();
            rs_file_type = s_file_type.executeQuery(new StringBuilder().append("SELECT * FROM ").append("file_type WHERE id=").append(rs.getInt("id_file_type")).toString());
            
            while (rs_file_type.next())
            {
                file_type.setId(rs_file_type.getInt("id"));
                file_type.setFormat(rs_file_type.getString("format"));
                file_type.setSampling_window(rs_file_type.getString("sampling_window"));
                file_type.setSampling_frequency(rs_file_type.getString("sampling_frequency"));
            }
            
            rs_file_type.close();
            s_file_type.close();
            
            // *****************************************************************
            // Handle Rinex File
            // *****************************************************************
            rinex_file.setId(rs.getInt("id"));
            rinex_file.setName(rs.getString("name"));
            rinex_file.setId_station(rs.getInt("id_station"));
            
            // *****************************************************************
            // Handle Station Marker
            // *****************************************************************
            s_station = c.createStatement();
            rs_station = s_station.executeQuery(new StringBuilder().append("SELECT * FROM station ").append("WHERE id=").append(rs.getInt("id_station")).toString());
                    
            // Check if there are any results
            while (rs_station.next())
            {
                // Save Station data
                rinex_file.setStation_marker(rs_station.getString("marker"));
            }
                    
            rs_station.close();
            s_station.close();


            // *****************************************************************
            // Handle Data Center Structure
            // *****************************************************************
            Data_center_structure data_center_structure = new Data_center_structure();
            s_data_center_structure = c.createStatement();
            rs_data_center_structure = s_data_center_structure.executeQuery(new StringBuilder().append("SELECT * FROM ").append("data_center_structure WHERE id=").append(rs.getInt("id_data_center_structure")).toString());
            rs_data_center_structure.next();

            data_center_structure.setId(rs_data_center_structure.getInt("id"));
            data_center_structure.setDirectory_naming(rs_data_center_structure.getString("directory_naming"));

            // *****************************************************************
            // Handle Data Center
            // *****************************************************************
            Data_center data_center = new Data_center();
            s_data_center = c.createStatement();
            rs_data_center = s_data_center.executeQuery(new StringBuilder().append("SELECT * FROM ").append("data_center WHERE id=").append(rs_data_center_structure.getInt("id_data_center")).toString());

            while (rs_data_center.next())
            {
                data_center.setId(rs_data_center.getInt("id"));
                data_center.setAcronym(rs_data_center.getString("acronym"));
                data_center.setHostname(rs_data_center.getString("hostname"));
                data_center.setProtocol(rs_data_center.getString("protocol"));
            }

            rs_data_center.close();
            s_data_center.close();
            
         
            // Add to Data_center
            //data_center_structure.setDataCenter(data_center);
            
            rinex_file.setData_center_structure(data_center_structure.getId(),
                    data_center_structure.getDirectory_naming(), data_center); //data_center
            rinex_file.setFile_size(rs.getInt("file_size"));
            rinex_file.setFile_type(file_type.getId(), file_type.getFormat(),
                    file_type.getSampling_window(), file_type.getSampling_frequency());
            rinex_file.setRelative_path(rs.getString("relative_path"));
            rinex_file.setReference_date(rs.getString("reference_date"));
            rinex_file.setCreation_date(rs.getString("creation_date")); //HUGO
            rinex_file.setPublished_date(rs.getString("published_date"));
            rinex_file.setRevision_date(rs.getString("revision_date"));
            rinex_file.setMd5checksum(rs.getString("md5checksum"));
            rinex_file.setMd5uncompressed(rs.getString("md5uncompressed"));
            rinex_file.setStatus(rs.getInt("status"));
            
            results.add(rinex_file);

            //creation date missing, but date exists.. same?;
        }
        
        rs.close();
        s.close();
        
        return results;
    }
    
    /*
    * GET RINEX FILES
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public ArrayList getRinexFiles()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        
        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM rinex_file ORDER BY id";

            //Execute query
            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        return results;
    }


    /*
    * GET RINEX FILES AGENCY
    * This method retuns a ArrayList with all the rinex files from the given agency.
    */
    public Tuple2 getRinexFilesAgency(String agency,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();

            // Get station ids
            String query = "select * from station where id in " +
                    "(select id_station from station_contact where station_contact.id_contact in " +
                    "(select id from contact where contact.id_agency in " +
                    "(select id from agency where agency.\"name\" ilike '" +
                    agency + "')));";

            ArrayList<Station> stations = new ArrayList();
            stations = dbct1.executeStationQuery(query, c);
            c = NewConnection();
            if(stations.size() <= 0)
                return count_and_rinex;

            // Remove duplicated Ids
            Set<Integer> hs = new HashSet<>();
            for (Station station : stations)
                hs.add(station.getId());

            ArrayList<Integer> station_ids_i = new ArrayList<>();
            station_ids_i.addAll(hs);

            // Get rinex file query
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<station_ids_i.size(); i++)
            {
                if (i == station_ids_i.size() - 1){
                    query = query + station_ids_i.get(i) + "";
                    query_count = query_count + station_ids_i.get(i) + "";
                }

                else{
                    query = query + station_ids_i.get(i) + " OR id_station=";
                    query_count = query_count + station_ids_i.get(i) + " OR id_station=";
            }

            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ");";
            }
            query = query + " ORDER BY creation_date";


            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);

            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET RINEX FILES ANTENNA TYPE
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public Tuple2 getRinexFilesAntennaType(String antennaType,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();
        
            // ArrayList with Stations
            ArrayList<Station> station_list = new ArrayList();

            /* This query is weird and does not seem to work. I did a new one under @JM
            // Query to be executed
            String query = "SELECT * FROM station WHERE id=("
                    + "SELECT id_station FROM station_item WHERE id_item=("
                    + "SELECT id_item FROM filter_antenna WHERE id_antenna_type=("
                    + "SELECT id FROM antenna_type WHERE name='" + antennaType 
                    + "'))) ORDER BY id;";
            */

            String query = "select * from station "
                + "inner join station_item on station_item.id_station = station.id "
                + "where station_item.id_item in ( "
                    + "select item.id from item "
                    + "inner join item_attribute on item_attribute.id_item = item.id "
                    + "inner join filter_antenna on filter_antenna.id_item_attribute = item_attribute.id "
                    + "where filter_antenna.id_antenna_type in ( "
                        + "select id from antenna_type where name ilike '" + antennaType
                        + "') "
                    + ")"
                + " ORDER BY station.id;";



            // Execute query
            station_list = dbct1.executeStationQuery( query, c);
            c = NewConnection();

            // Check if the type exists
            if(station_list.isEmpty()) return count_and_rinex;

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<station_list.size(); i++)
            {
                if (i == station_list.size() - 1){
                    query = query + station_list.get(i).getId() + "";
                query_count = query_count + station_list.get(i).getId() + "";}
                else {
                    query = query + station_list.get(i).getId() + " OR id_station=";
                    query_count = query_count + station_list.get(i).getId() + " OR id_station=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ");";
            }
           query = query + " ORDER BY creation_date";

            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
            if (pageNumber>0)
               query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET RINEX FILES CITY
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public Tuple2 getRinexFilesCity(String city,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        
        // ArrayList with Stations
        ArrayList<Station> station_list = new ArrayList();

        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();
        
            // Query to be executed
            String query = "select * from station where id_location in " +
                    "(select id from location where location.id_city in " +
                    "(select id from city where city.name ilike '" + city + "')) " +
                    "order by station.id;";


            // Execute query
            station_list = dbct1.executeStationQuery(query, c);
            c = NewConnection();
            // Check if the type exists
            if(station_list.size() <= 0)
                return count_and_rinex;

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<station_list.size(); i++)
            {
                if (i == station_list.size() - 1) {
                    query = query + station_list.get(i).getId() + "";
                    query_count = query_count + station_list.get(i).getId() + "";
                }
                else{
                    query = query + station_list.get(i).getId() + " OR id_station=";
                    query_count = query_count + station_list.get(i).getId() + " OR id_station=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ")";
            }
            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);

            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }

        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET RINEX FILES COORDINATES
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public Tuple2 getRinexFilesCoordinates(float minLat, float maxLat,
            float minLon, float maxLon,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();
        
            // Get Stations ID's
            ArrayList<Integer> stationList = new ArrayList();
            String query = "SELECT id FROM station WHERE id_location in ("
                    + "SELECT id FROM Location WHERE id_coordinates in ("
                    + "SELECT id FROM Coordinates WHERE lat>=" + minLat +""
                    + " AND lat<=" + maxLat + " AND lon>=" + minLon +
                    " AND lon<=" + maxLon + ")) ORDER BY id;";
            stationList = dbct1.executeIDQuery(query, c);
            if(stationList.isEmpty())return count_and_rinex;
            // Get Rinex File

            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT * FROM rinex_file WHERE (id_station=";
            for (int i=0; i<stationList.size(); i++)
            {
                if (i == stationList.size() - 1) {
                    query = query + stationList.get(i) + "";
                    query_count = query_count + stationList.get(i) + "";
                }
                else {
                    query = query + stationList.get(i) + " OR id_station=";
                    query_count = query_count + stationList.get(i) + " OR id_station=";

                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0";
            }else{
                query = query + ")";
                query_count = query_count + ")";
            }
            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET RINEX FILES COUNTRY
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public Tuple2 getRinexFilesCountry(String country,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        // ArrayList with Stations
        ArrayList<Station> station_list = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();
        
            // Query to be executed
            String query = "SELECT * FROM station WHERE id_location in ("
                    + "SELECT location.id FROM location WHERE id_city in ("
                    + "SELECT city.id FROM city WHERE id_state in ("
                    + "SELECT state.id FROM state WHERE id_country in ("
                    + "SELECT country.id FROM country WHERE name='" + country
                    + "')))) ORDER BY id;";

            // Execute query
            station_list = dbct1.executeStationQuery(query, c);

            // Check if the type exists
            if(station_list.size() <= 0)
                return count_and_rinex;

            // Connection is closed in executeStationQuery
            c = NewConnection();

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<station_list.size(); i++)
            {
                if (i == station_list.size() - 1) {
                    query = query + station_list.get(i).getId() + "";
                    query_count = query_count + station_list.get(i).getId() + "";
                }
                else{
                    query = query + station_list.get(i).getId() + " OR id_station=";
                    query_count = query_count + station_list.get(i).getId() + " OR id_station=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ")";
            }
            query = query + " ORDER BY creation_date";
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );
            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET FILES PUBLISH DATE
    * This method retuns a ArrayList with data from all files on the database.
    */
    public ArrayList getRinexFilesPublishDate(String date_from, String date_to)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        
        try{
            c = NewConnection();
        
            // Query to be executed
            String query = "SELECT * FROM rinex_file WHERE published_date>='" + date_from + 
                    "' AND published_date<='" + date_to + "' ORDER BY id";

            //Execute query
            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        return results;
    }
    
    /*
    * GET RINEX FILES RADOME TYPE
    * This method retuns a ArrayList with data from all files on the database.
    */
    public Tuple2 getRinexFilesRadomeType(String radomeType,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();
        
            // ArrayList with Stations
            ArrayList<Station> station_list = new ArrayList();

            // Query to be executed
            String query = "SELECT * FROM station WHERE station.id in ("
                    + "SELECT id_station FROM station_item WHERE station_item.id_item in ("
                    + "SELECT id_item FROM filter_radome WHERE filter_radome.id_radome_type in ("
                    + "SELECT id FROM radome_type WHERE radome_type.name='" + radomeType
                    + "'))) ORDER BY id;";

            // Execute query
            station_list = dbct1.executeStationQuery(query, c);
            if(station_list.size() <= 0)
                return count_and_rinex;
            c = NewConnection();

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<station_list.size(); i++)
            {
                if (i == station_list.size() - 1) {
                    query = query + station_list.get(i).getId() + "";
                    query_count = query_count + station_list.get(i).getId() + "";
                }
                else{
                    query = query + station_list.get(i).getId() + " OR id_station=";
                    query_count = query_count + station_list.get(i).getId() + " OR id_station=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ")";
            }
            query = query + " ORDER BY creation_date";
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    
    /*
    * GET RINEX FILES RECEIVER TYPE
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public Tuple2 getRinexFilesReceiverType(String receiverType,int pageNumber,int perPageNumber, int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();
        
            // ArrayList with Stations
            ArrayList<Station> station_list = new ArrayList();

            // Query to be executed
            String query = "SELECT * FROM station WHERE id in ("
                    + "SELECT id_station FROM station_item WHERE id_item in ("
                    + "SELECT id_item FROM filter_receiver WHERE id_receiver_type in ("
                    + "SELECT id FROM receiver_type WHERE name ilike '" + receiverType
                    + "'))) ORDER BY id;";

            // Execute query
            station_list = dbct1.executeStationQuery( query, c);
            c = NewConnection();
            // Check if the type exists
            if(station_list.size() <= 0)
                return count_and_rinex;

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<station_list.size(); i++)
            {
                if (i == station_list.size() - 1) {
                    query = query + station_list.get(i).getId() + "";
                    query_count = query_count + station_list.get(i).getId() + "";
                }
                else{
                    query = query + station_list.get(i).getId() + " OR id_station=";
                    query_count = query_count + station_list.get(i).getId() + " OR id_station=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0";
            }else{
                query = query + ")";
                query_count = query_count + ");";
            }
            query = query + " ORDER BY creation_date";
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET FILES STATE
    * This method retuns a ArrayList with data from all files on the database.
    */
    public Tuple2 getRinexFilesState(String state,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        // ArrayList with Stations
        ArrayList<Station> station_list = new ArrayList();

        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();
        
            // Query to be executed
            String query = "SELECT * FROM station WHERE station.id_location IN (" +
                    "SELECT location.id FROM location WHERE location.id_city IN (" +
                    "SELECT city.id FROM city WHERE city.id_state IN (" +
                    "SELECT state.id FROM state WHERE state.name ilike '" +
                    state +
                    "')));";

            // Execute query
            station_list = dbct1.executeStationQuery( query, c);
            c = NewConnection();
            // Check if the type exists
            if(station_list.size() <= 0)
                return count_and_rinex;

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<station_list.size(); i++)
            {
                if (i == station_list.size() - 1) {
                    query = query + station_list.get(i).getId() + "";
                    query_count = query_count + station_list.get(i).getId() + "";
                }
                else{
                    query = query + station_list.get(i).getId() + " OR id_station=";
                    query_count = query_count + station_list.get(i).getId() + " OR id_station=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ");";
            }
            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );
            results = executeRinexFilesQuery(results, query, c);

            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET RINEX FILES STATION DATES
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public ArrayList getRinexFilesStationDates(String date_from, String date_to)
    {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        
        try{
            c = NewConnection();
        
            // Get Stations ID's
            ArrayList<Integer> stationList = new ArrayList();
            String query = "SELECT id FROM station WHERE date_from>='" + date_from
                    + "' AND date_to<='" + date_to + "' ORDER BY id;";
            stationList = dbct1.executeIDQuery(query, c);

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE id_station=";
            for (int i=0; i<stationList.size(); i++)
            {
                if (i == stationList.size() - 1)
                    query = query + stationList.get(i) + ";";
                else
                    query = query + stationList.get(i) + " OR id_station=";
            }
            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        
        return results;
    }
    
    /*
    * GET RINEX FILES STATION MARKER
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public Tuple2 getRinexFilesStationMarker(String marker, int pageNumber, int perPageNumber, int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        try{
            c = NewConnection();
        
            // Get Stations ID's
            ArrayList<Integer> stationList = new ArrayList();
            String query = "SELECT * FROM station WHERE marker LIKE '%" + marker + "%' ORDER BY id";
            stationList = dbct1.executeIDQuery(query, c);
            if(stationList.isEmpty()) return count_and_rinex;
            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<stationList.size(); i++)
            {
                if (i == stationList.size() - 1){
                    query = query + stationList.get(i) + "";
                    query_count = query_count + stationList.get(i) + ""; }
                else {
                    query = query + stationList.get(i) + " OR id_station=";
                    query_count = query_count + stationList.get(i) + " OR id_station=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ");";
            }
            query = query + " ORDER BY creation_date";

            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

            if (pageNumber>0)
               query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }

    /*
    * GET RINEX FILES STATION NAME
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public Tuple2 getRinexFilesStationName(String name,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        try{
            c = NewConnection();
        
            // Get Stations ID's
            ArrayList<Integer> stationList = new ArrayList();
            // % only after the name so that: "nic" finds "nice"
            String query = "SELECT * FROM station WHERE name LIKE '" + name + "%' ORDER BY id";
            stationList = dbct1.executeIDQuery(query, c);
            if(stationList.isEmpty()) return count_and_rinex;

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<stationList.size(); i++)
            {
                if (i == stationList.size() - 1){
                    query = query + stationList.get(i) + "";
                    query_count = query_count + stationList.get(i) + ""; }
                else{
                    query = query + stationList.get(i) + " OR id_station=";
                    query_count = query_count + stationList.get(i) + " OR id_station=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ");";
            }
            query = query + " ORDER BY creation_date";
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );


            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET RINEX FILES STATION NETWORK
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public Tuple2 getRinexFilesStationNetwork(String network,int pageNumber,int perPageNumber,int bad_files) throws SQLException, SocketException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        int totalCount = 0;
        
        try{
            c = NewConnection();
        
            // Get Network ID
            String query = "SELECT id FROM network WHERE name='" + network + "';";
            ArrayList<Integer> networks_list = new ArrayList();
            networks_list = dbct1.executeIDQuery(query, c);

            // Check if the network exists
           if(networks_list.size() <= 0)
                return  count_and_rinex;

            // Get Stations ID's
            ArrayList<Integer> stationList = new ArrayList();
            query = "SELECT id_station FROM station_network WHERE id_network=" + 
                    networks_list.get(0) + ";";
            stationList = dbct1.executeIDStationQuery(query, c);
            if(stationList.size() <= 0) return  count_and_rinex;
            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<stationList.size(); i++)
            {
                if (i == stationList.size() - 1){
                    query = query + stationList.get(i) + "";
                    query_count = query_count + stationList.get(i) + ""; }
                else {
                    query = query + stationList.get(i) + " OR id_station=";
                    query_count = query_count + stationList.get(i) + " OR id_station=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ");";
            }
            query = query + " ORDER BY creation_date";
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET RINEX FILES STATION TYPE
    * This method retuns a ArrayList with data from all files on the database.
    */
    public Tuple2 getRinexFilesStationType(String type,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();
        
            // Get Station Type ID
            String query = "SELECT id FROM station_type WHERE name ILIKE '" + type + "';";
            ArrayList<Integer> type_list = new ArrayList();
            type_list = dbct1.executeIDQuery(query, c);

            // Check if the type exists
            if(type_list.size() <= 0)
                return count_and_rinex;

            // Get Stations ID's
            ArrayList<Integer> stationList = new ArrayList();
            query = "SELECT * FROM station WHERE id_station_type=" + 
                    type_list.get(0) + " ORDER BY id;";
            stationList = dbct1.executeIDQuery(query, c);
            if(stationList.isEmpty()) return count_and_rinex;

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<stationList.size(); i++)
            {
                if (i == stationList.size() - 1){
                    query = new StringBuilder().append(query).append(stationList.get(i)).append("").toString();
                query_count = new StringBuilder().append(query_count).append(stationList.get(i)).append("").toString();}
                else {
                    query = new StringBuilder().append(query).append(stationList.get(i)).append(" OR id_station=").toString();
                    query_count = new StringBuilder().append(query_count).append(stationList.get(i)).append(" OR id_station=").toString();
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0;";
            }else{
                query = query + ")";
                query_count = query_count + ")";
            }
            query = query + " ORDER BY creation_date";
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    /*
    * GET RINEX FILES TYPE
    * This method retuns a ArrayList with data from all rinex files on the database.
    */
    public Tuple2 getRinexFilesFormat(String type,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        
        try{
            c = NewConnection();
        
            // Get Stations ID's
            ArrayList<Integer> typeList = new ArrayList();
            String query = "SELECT * FROM file_type WHERE format ilike '%" + type + "' ORDER BY id";
            typeList = dbct1.executeIDQuery(query, c);
            if(typeList.isEmpty())return count_and_rinex;

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_file_type=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_file_type=";
            for (int i=0; i<typeList.size(); i++)
            {
                if (i == typeList.size() - 1) {
                    query = query + typeList.get(i) + "";
                    query_count = query_count + typeList.get(i) + "";
                }
                else {
                    query = query + typeList.get(i) + " OR id_file_type=";
                    query_count = query_count + typeList.get(i) + " OR id_file_type=";
                }
            }
            if(bad_files == 1){
                query = query + ") and status > 0";
                query_count = query_count + ") and status > 0";
            }else{
                query = query + ")";
                query_count = query_count + ")";
            }
            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
        
        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }

    //// HUGO
    /*
    * GET FILES
    * This method retuns a ArrayList with data from all files on the database based on the sampling type.
    */
    public Tuple2 getFilesSampling(String sampling,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM rinex_file INNER JOIN file_type ON (rinex_file.id_file_type = file_type.id) WHERE file_type.sampling_frequency='" + sampling +"'";
            String query_count = "SELECT COUNT(*) FROM rinex_file INNER JOIN file_type ON (rinex_file.id_file_type = file_type.id) WHERE file_type.sampling_frequency='" + sampling +"'";
            if(bad_files == 1){
                query = query + " and status > 0";
                query_count = query_count + " and status > 0";
            }

            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );
            //Execute query
            results = executeRinexFilesQuery(results, query, c);
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }

    /*
    * GET FILES
    * This method retuns a ArrayList with data from all files on the database based on the lenght type.
    */
    public Tuple2 getFilesLength(String length,int pageNumber,int perPageNumber, int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM rinex_file INNER JOIN file_type ON (rinex_file.id_file_type = file_type.id) WHERE file_type.sampling_window='" + length +"'";
            String query_count = "SELECT COUNT(*) FROM rinex_file INNER JOIN file_type ON (rinex_file.id_file_type = file_type.id) WHERE file_type.sampling_window='" + length +"'";

            if(bad_files == 1){
                query = query + " and status > 0";
                query_count = query_count + " and status > 0";
            }
            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );
            //Execute query
            results = executeRinexFilesQuery(results, query, c);
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }

    /*
* GET FILES
* This method retuns a ArrayList with data from all files on the database based on the lenght type.
*/
    public ArrayList getFilesProvider(String provider)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM public.rinex_file JOIN public.station ON public.rinex_file.id_station = public.station.id \n" +
                    "JOIN public.datacenter_station ON public.station.id = public.datacenter_station.id_station WHERE public.datacenter_station.datacenter_type='" + provider +"'";

            //Execute query
            results = executeRinexFilesQuery(results, query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    /*
    * GET FILES
    * This method retuns a ArrayList with data from all files on the database based on the lenght type.
    */
    public ArrayList getFilesLatencyPlus(String seconds)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM rinex_file INNER JOIN file_type ON (rinex_file.id_file_type = file_type.id) WHERE file_type.sampling_window='" + seconds +"'";

            //Execute query
            results = executeRinexFilesQuery(results, query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    public Tuple2 getFilesDateRange(String date_from, String date_to,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM rinex_file WHERE reference_date>='" + date_from +
                    "' AND reference_date<='" + date_to + "'";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE reference_date>='" + date_from +
                    "' AND reference_date<='" + date_to + "'";
            if(bad_files == 1){
                query = query + " and status > 0";
                query_count = query_count + " and status > 0";
            }

            query = query + " ORDER BY id";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );
            //Execute query
            results = executeRinexFilesQuery(results, query, c);

            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }

    public Tuple2 getFilesDatePublished(String date_from, String date_to,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM rinex_file WHERE published_date>='" + date_from +
                    "' AND published_date<='" + date_to + "'";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE published_date>='" + date_from +
                    "' AND published_date<='" + date_to + "'";
            if(bad_files == 1){
                query = query + " and status > 0";
                query_count = query_count + " and status > 0";
            }
            query = query + " ORDER BY id";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );
            //Execute query
            results = executeRinexFilesQuery(results, query, c);
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }

    public ArrayList getFilesPublishDate(String date_from, String date_to)
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM rinex_file WHERE published_date>='" + date_from +
                    "' AND published_date<='" + date_to + "' ORDER BY id";

            //Execute query
            results = executeRinexFilesQuery(results, query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    public ArrayList getFilesMarkerDatesCombined(String request)
    {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        ArrayList<Integer> results_date_range = new ArrayList();
        ArrayList<Integer> results_marker = new ArrayList();
        RequestT2 request_holder = new RequestT2(request);
        String query = "";
        int counter = 0;

        try{
            c = NewConnection();

            // Parse request
            request_holder.parseRequest();
            //request_holder.requestToString();

            // Check date range
            if (request_holder.date_range_list.size() == 2)
            {
                query = "SELECT id FROM rinex_file WHERE reference_date>='" + request_holder.date_range_list.get(0) +
                        "' AND reference_date<='" + request_holder.date_range_list.get(1) + "' ORDER BY id";
            }
            results_date_range.addAll(dbct1.executeIDQuery(query, c));


            // Check marker
            for(int index=0; index<request_holder.charCode_list.size(); index++)
            {
                // Get Stations ID's
                ArrayList<Integer> stationList = new ArrayList();
                query = "SELECT * FROM station WHERE marker LIKE '%" +
                        request_holder.charCode_list.get(index) + "%' ORDER BY id";
                stationList = dbct1.executeIDQuery(query, c);

                ArrayList<Integer> files_generatedList = new ArrayList();

                // Get File Generated ID's
                for(int i=0; i<stationList.size(); i++)
                {
                    query = "SELECT * FROM rinex_file WHERE id_station=" + stationList.get(i) + " ORDER BY id";
                    files_generatedList = dbct1.executeIDQuery(query, c);

                    // Get Files
                    for(int j=0; j<files_generatedList.size(); j++)
                    {
                        query = "SELECT * FROM rinex_file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
                        ArrayList<Rinex_file> subresults = new ArrayList();
                        subresults = executeRinexFilesQuery(subresults, query, c);

                        // Save results to main ArrayList
                        for(int k=0; k<subresults.size(); k++)
                        {
                            results_marker.add(subresults.get(k).getId());
                        }
                    }
                }
            }

            ArrayList<Integer> files_Ids = new ArrayList();
            for (int i=0; i<results_date_range.size(); i++)
            {
                if (results_marker.contains(results_date_range.get(i)) == true)
                    files_Ids.add(results_date_range.get(i));
            }


            // Create station query
            if (files_Ids.size() > 0)
            {
                query = "SELECT * FROM file WHERE ";
                for (int i=0; i<files_Ids.size(); i++)
                {
                    if (i==0)
                        query = query + "id=" + files_Ids.get(i);
                    else
                        query = query + " OR id=" + files_Ids.get(i);
                }
                query = query + ";";

                //Execute query
                results = executeRinexFilesQuery(results, query, c);
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

    public String getURL(ArrayList<Rinex_file> data)
    {
        String result = "";

        for (int i=0; i<data.size(); i++)
        {
            result += data.get(i).getRelative_path() + "\\" + data.get(i).getName() + "\n";
        }

        return result;
    }

    public ArrayList getFilesCoordinates(float minLat, float maxLat,
                                         float minLon, float maxLon)
    {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();

        try{
            c = NewConnection();

            // Get Stations ID's
            ArrayList<Integer> stationList = new ArrayList();
            String query = "SELECT id FROM station WHERE id_location= ("
                    + "SELECT id FROM Location WHERE id_coordinates= ("
                    + "SELECT id FROM Coordinates WHERE lat>=" + minLat +""
                    + " AND lat<=" + maxLat + " AND lon>=" + minLon +
                    " AND lon<=" + maxLon + ")) ORDER BY id;";
            stationList = dbct1.executeIDQuery(query, c);

            ArrayList<Integer> files_generatedList = new ArrayList();

            // Get File Generated ID's
            for(int i=0; i<stationList.size(); i++)
            {
                query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
                files_generatedList = dbct1.executeIDQuery(query, c);

                // Get Files
                for(int j=0; j<files_generatedList.size(); j++)
                {
                    query = "SELECT * FROM rinex_file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
                    ArrayList<Rinex_file> subresults = new ArrayList();
                    subresults = executeRinexFilesQuery(subresults, query, c);

                    // Save results to main ArrayList
                    for(int k=0; k<subresults.size(); k++)
                    {
                        results.add(subresults.get(k));
                    }
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }

        public Tuple2 getFilesCoordinatesDateCombined(String request,int pageNumber,int perPageNumber,int bad_files) throws ParseException, SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        ArrayList<Integer> results_date_range = new ArrayList();
        ArrayList<Integer> results_coordinates = new ArrayList();
        ArrayList<Integer> results_coordinates_file_gen = new ArrayList();
        ArrayList<Integer> results_coordinates_stations = new ArrayList();
        RequestT2 request_holder = new RequestT2(request);
        String query = "";
        int counter = 0;
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);

        try{
            c = NewConnection();

            // Parse request
            request_holder.parseRequest();
            //request_holder.requestToString();

            // Check date range
            if (request_holder.date_range_list.size() == 2)
            {
                query = "SELECT id FROM file WHERE data_start_time>='" + request_holder.date_range_list.get(0) +
                        "' AND data_stop_time<='" + request_holder.date_range_list.get(1) + "' ORDER BY id";
            }
            System.out.println(query);
            if (query.isEmpty() == false)
                results_date_range.addAll(dbct1.executeIDQuery(query, c));

            // Check coordinates
            switch (request_holder.coordinates_type)
            {
                // Handle rectangle queries
                case "rectangle":
                    ArrayList<Station> coord_rec = new ArrayList();
                    coord_rec = dbct1.getStationCoordinates(request_holder.minLat, request_holder.maxLat,
                            request_holder.minLon, request_holder.maxLon);
                    for (int i=0; i<coord_rec.size(); i++)
                        results_coordinates_stations.add(coord_rec.get(i).getId());
                    break;

                // Handle circle queries
                case "circle":
                    ArrayList<Station> coord_cir = new ArrayList();
                    coord_cir = dbct1.getStationCoordinates(request_holder.lat, request_holder.lon,
                            request_holder.radius);
                    for (int i=0; i<coord_cir.size(); i++)
                        results_coordinates_stations.add(coord_cir.get(i).getId());
                    break;

                // Handle polygon queries
                case "polygon":
                    ArrayList<Station> coord_pol = new ArrayList();
                    coord_pol = dbct1.getStationCoordinates(request_holder.coordinates_list);
                    for (int i=0; i<coord_pol.size(); i++)
                        results_coordinates_stations.add(coord_pol.get(i).getId());
                    break;
            }

            query = "SELECT * FROM file_generated WHERE id_station=";
            for (int i=0; i<results_coordinates_stations.size(); i++)
            {
                if (i<results_coordinates_stations.size()-1)
                    query = query + results_coordinates_stations.get(i) + " OR id_station=";
                else
                    query = query + results_coordinates_stations.get(i);
            }
            query = query + " ORDER BY id;";
            results_coordinates_file_gen = dbct1.executeIDQuery(query, c);

            query = "SELECT * FROM file WHERE id_file_generated=";
            for (int i=0; i<results_coordinates_file_gen.size(); i++)
            {
                if (i<results_coordinates_file_gen.size()-1)
                    query = query + results_coordinates_file_gen.get(i) + " OR id_file_generated=";
                else
                    query = query + results_coordinates_file_gen.get(i);
            }
            query = query + " ORDER BY id;";
            results_coordinates = dbct1.executeIDQuery(query, c);

            // Count valid parameters
            if (results_date_range.size() > 0)
                counter++;
            if (request_holder.coordinates_type.contentEquals("rectangle") ||
                    request_holder.coordinates_type.contentEquals("circle") ||
                    request_holder.coordinates_type.contentEquals("polygon"))
                counter++;


            ArrayList<Integer> files_Ids = new ArrayList();
            for (int i=0; i<results_date_range.size(); i++)
            {
                if (results_coordinates.contains(results_date_range.get(i)) == true)
                    files_Ids.add(results_date_range.get(i));
            }


            // Create station query
            if (files_Ids.size() > 0)
            {
                query = "SELECT * FROM file WHERE (";
                String query_count = "SELECT COUNT(*) FROM file WHERE (";
                for (int i=0; i<files_Ids.size(); i++)
                {
                    if (i==0) {
                        query = query + "id=" + files_Ids.get(i);
                        query_count = query_count + "id=" + files_Ids.get(i);
                    }
                    else {
                        query = query + " OR id=" + files_Ids.get(i);
                        query_count = query_count + " OR id=" + files_Ids.get(i);
                    }
                }
                if(bad_files == 1){
                    query = query + ") and status > 0";
                    query_count = query_count + ") and status > 0";
                }else{
                    query = query + ")";
                    query_count = query_count + ")";
                }
                query = query + " ORDER BY creation_date";

                if (pageNumber>0)
                    query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

                //Execute query
                results = executeRinexFilesQuery(results, query, c);
                try (Statement stmt = c.createStatement();
                     ResultSet rs = stmt.executeQuery(query_count)) {
                    if (rs.next()) {
                        totalCount = rs.getInt(1);
                    }
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }

            count_and_rinex.setFirst(totalCount);
            count_and_rinex.setSecond(results);
            return count_and_rinex;
    }
        
    /*
    * GET FILES COMBINED
    * This method retuns a ArrayList with all the files with the given parameter.
    */
    public Tuple2 getFilesCombined(String request,int pageNumber,int perPageNumber,int bad_files) throws ParseException, SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        Statement stmt = null;

        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);

        RequestT2 request_holder = new RequestT2(request);
        String query = "";
        String dates_range0 = "";
        String dates_range1 = "";
        String dates_pub0 = "";
        String dates_pub1 = "";

        // Merge results
        Set<Integer> stations_Ids = new HashSet<Integer>();

        try {

            c = NewConnection();

            // Parse request
            request_holder.parseRequest();

            // Check marker
            if (request_holder.charCode_list.size() > 0) {
                ArrayList<Integer> results_marker = new ArrayList<Integer>();
                PreparedStatement idstfromMarker = c.prepareStatement("SELECT id FROM station WHERE marker ILIKE ? ORDER BY id");

                for (int index = 0; index < request_holder.charCode_list.size(); index++) {
                    // //query = "SELECT * FROM station WHERE marker LIKE '%" + request_holder.marker_list.get(index) + "%' ORDER BY id;";
                    // query = "SELECT * FROM station WHERE marker ILIKE '" + request_holder.charCode_list.get(index) + "%' ORDER BY id;";
                    // results_marker.addAll(dbct1.executeIDQuery(query, c));

                    // ArrayList<Integer> idList = new ArrayList<>();
                    String markerRequest = request_holder.charCode_list.get(index);
                    idstfromMarker.setString(1, markerRequest + "%");

                    // Initialized statement for select
                    try {
                        rs = idstfromMarker.executeQuery();
                        // Check if there are any results
                        while (rs.next()) {
                            //idList.add(rs.getInt("id"));
                            results_marker.add(rs.getInt("id"));
                        }

                        rs.close();
                    } catch (SQLException e) {
                        System.out.println("Strange Error 1");
                        e.printStackTrace();
                        throw e;
                    }
                }
                idstfromMarker.close();
                stations_Ids.addAll(results_marker); //this is the first parameters to test and to insert into the set
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check site
            if (request_holder.siteName_list.size() > 0) {
                ArrayList<Integer> results_site = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.siteName_list.size(); index++) {
                    //query = "SELECT * FROM station WHERE name LIKE '%" + request_holder.site_list.get(index) + "%' ORDER BY id;";
                    query = "SELECT id FROM station WHERE name ILIKE '" + request_holder.siteName_list.get(index) + "%' ORDER BY id;";
                    results_site.addAll(dbct1.executeIDQuery(query, c));
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_site);
                else stations_Ids.retainAll(results_site);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }


            // Check height
            if (request_holder.height_list.size() > 0) {
                ArrayList<Integer> results_height = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.height_list.size(); index++) {
                    // Get list of Monument IDs
                    query = "SELECT id FROM monument WHERE height='" + request_holder.height_list.get(index) + "' ORDER BY id;";
                    ArrayList<Integer> results_monument = new ArrayList();
                    results_monument = dbct1.executeIDQuery(query, c);
                    if (results_monument.size() > 0) {
                        // Get list of Station IDs
                        query = "SELECT id FROM station WHERE ";
                        for (int i = 0; i < results_monument.size(); i++) {
                            if (i == 0)
                                query = query + "id_monument=" + results_monument.get(i);
                            else
                                query = query + " OR id_monument=" + results_monument.get(i);
                        }
                        query = query + ";";
                        results_height.addAll(dbct1.executeIDQuery(query, c));
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_height);
                else stations_Ids.retainAll(results_height);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check latitude
            if (request_holder.latitude_list.size() > 0) {
                ArrayList<Integer> results_latitude = new ArrayList<Integer>();
                if (request_holder.minLatitude != 0.0f && request_holder.maxLatitude != 0.0f) {
                    query = "SELECT id FROM coordinates WHERE lat>" + request_holder.minLatitude + " AND lat<" + request_holder.maxLatitude + ";";
                } else if (request_holder.minLatitude != 0.0f && request_holder.maxLatitude == 0.0f) {
                    query = "SELECT id FROM coordinates WHERE lat>" + request_holder.minLatitude + ";";
                } else if (request_holder.minLatitude == 0.0f && request_holder.maxLatitude != 0.0f) {
                    query = "SELECT id FROM coordinates WHERE lat<" + request_holder.maxLatitude + ";";
                }

                ArrayList<Integer> results_coordinatesid_lat = new ArrayList();
                results_coordinatesid_lat = dbct1.executeIDQuery(query, c);

                if (results_coordinatesid_lat.size() > 0) {
                    // Get location list
                    query = "SELECT id FROM location WHERE ";
                    for (int i = 0; i < results_coordinatesid_lat.size(); i++) {
                        if (i == 0)
                            query = query + "id_coordinates=" + results_coordinatesid_lat.get(i);
                        else
                            query = query + " OR id_coordinates=" + results_coordinatesid_lat.get(i);
                    }
                    query = query + ";";
                    ArrayList<Integer> results_location = new ArrayList();
                    results_location = dbct1.executeIDQuery(query, c);

                    if (results_location.size() > 0) {
                        query = "SELECT id FROM station WHERE ";
                        for (int i = 0; i < results_location.size(); i++) {
                            if (i == 0)
                                query = query + "id_location=" + results_location.get(i);
                            else
                                query = query + " OR id_location=" + results_location.get(i);
                        }
                        query = query + ";";
                        results_latitude.addAll(dbct1.executeIDQuery(query, c));
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_latitude);
                else stations_Ids.retainAll(results_latitude);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check longitude
            if (request_holder.longitude_list.size() > 0) {
                ArrayList<Integer> results_longitude = new ArrayList<Integer>();

                if ((request_holder.minLongitude != 0.0f) && (request_holder.maxLongitude != 0.0f)) {
                    query = "SELECT id FROM coordinates WHERE lon>" + request_holder.minLongitude + " AND lon<" + request_holder.maxLongitude + ";";
                } else if (request_holder.minLongitude != 0.0f && request_holder.maxLongitude == 0.0f) {
                    query = "SELECT id FROM coordinates WHERE lon>" + request_holder.minLongitude + ";";
                } else if (request_holder.minLongitude == 0.0f && request_holder.maxLongitude != 0.0f) {
                    query = "SELECT id FROM coordinates WHERE lon<" + request_holder.maxLongitude + ";";
                }

                ArrayList<Integer> results_coordinatesid_lon = new ArrayList();
                results_coordinatesid_lon = dbct1.executeIDQuery(query, c);

                if (results_coordinatesid_lon.size() > 0) {
                    // Get location list
                    query = "SELECT id FROM location WHERE ";
                    for (int i = 0; i < results_coordinatesid_lon.size(); i++) {
                        if (i == 0)
                            query = query + "id_coordinates=" + results_coordinatesid_lon.get(i);
                        else
                            query = query + " OR id_coordinates=" + results_coordinatesid_lon.get(i);
                    }
                    query = query + ";";
                    ArrayList<Integer> results_location = new ArrayList();
                    results_location = dbct1.executeIDQuery(query, c);

                    if (results_location.size() > 0) {
                        query = "SELECT id FROM station WHERE ";
                        for (int i = 0; i < results_location.size(); i++) {
                            if (i == 0)
                                query = query + "id_location=" + results_location.get(i);
                            else
                                query = query + " OR id_location=" + results_location.get(i);
                        }
                        query = query + ";";
                        results_longitude.addAll(dbct1.executeIDQuery(query, c));
                    }
                }

                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_longitude);
                else stations_Ids.retainAll(results_longitude);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check altitude
            if (request_holder.altitude_list.size() > 0) {
                ArrayList<Integer> results_altitude = new ArrayList<Integer>();
                if (request_holder.minAlt != 0.0f && request_holder.maxAlt != 0.0f) {
                    query = "SELECT id FROM coordinates WHERE altitude>" + request_holder.minAlt + " AND altitude<" + request_holder.maxAlt + ";";
                } else if (request_holder.minAlt != 0.0f && request_holder.maxAlt == 0.0f) {
                    query = "SELECT id FROM coordinates WHERE altitude>" + request_holder.minAlt + ";";
                } else if (request_holder.minAlt == 0.0f && request_holder.maxAlt != 0.0f) {
                    query = "SELECT id FROM coordinates WHERE altitude<" + request_holder.maxAlt + ";";
                }

                ArrayList<Integer> results_coordinatesid = new ArrayList();
                results_coordinatesid = dbct1.executeIDQuery(query, c);

                if (results_coordinatesid.size() > 0) {
                    // Get location list
                    query = "SELECT id FROM location WHERE ";
                    for (int i = 0; i < results_coordinatesid.size(); i++) {
                        if (i == 0)
                            query = query + "id_coordinates=" + results_coordinatesid.get(i);
                        else
                            query = query + " OR id_coordinates=" + results_coordinatesid.get(i);
                    }
                    query = query + ";";
                    ArrayList<Integer> results_location = new ArrayList();
                    results_location = dbct1.executeIDQuery(query, c);

                    if (results_location.size() > 0) {
                        query = "SELECT id FROM station WHERE ";
                        for (int i = 0; i < results_location.size(); i++) {
                            if (i == 0)
                                query = query + "id_location=" + results_location.get(i);
                            else
                                query = query + " OR id_location=" + results_location.get(i);
                        }
                        query = query + ";";
                        results_altitude.addAll(dbct1.executeIDQuery(query, c));
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_altitude);
                else stations_Ids.retainAll(results_altitude);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check installed date
            if (request_holder.installdate == true) {
                ArrayList<Integer> results_installed_date = new ArrayList<Integer>();
                if (request_holder.minInstall != "" && request_holder.maxInstall != "") {
                    query = "SELECT id FROM station WHERE date_from>'" + request_holder.minInstall + "' AND date_from<'" + request_holder.maxInstall + "';";
                } else if (request_holder.minInstall == "" && request_holder.maxInstall != "") {
                    query = "SELECT id FROM station WHERE date_from<'" + request_holder.maxInstall + "';";
                } else if (request_holder.minInstall != "" && request_holder.maxInstall == "") {
                    query = "SELECT id FROM station WHERE date_from>'" + request_holder.minInstall + "';";
                }
                results_installed_date.addAll(dbct1.executeIDQuery(query, c));
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_installed_date);
                else stations_Ids.retainAll(results_installed_date);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check removed date
            if (request_holder.removedate == true) {
                ArrayList<Integer> results_removed_date = new ArrayList<Integer>();
                DateFormat dateformat = new SimpleDateFormat("yyyy-MM-dd");
                String date = dateformat.format(new Date());
                Date mydatemin = null;
                Date mydatemax = null;
                try {
                    if (request_holder.minRemoved != "") {
                        mydatemin = dateformat.parse(request_holder.minRemoved);
                    } else {
                        mydatemin = dateformat.parse(date);
                    }
                    if (request_holder.maxRemoved != "") {
                        mydatemax = dateformat.parse(request_holder.maxRemoved);
                    } else {
                        mydatemin = dateformat.parse(date);
                    }
                    Date currentdate = dateformat.parse(date);
                    if ((mydatemin.after(currentdate) || mydatemin.equals(currentdate)) || (mydatemax.after(currentdate) || mydatemax.equals(currentdate))) {
                        query = "SELECT * FROM station WHERE NULLIF(date_to, date_to) IS NULL;";
                        results_removed_date.addAll(dbct1.executeIDQuery(query, c));
                    } else {
                        if (request_holder.minRemoved != "" && request_holder.maxRemoved != "") {
                            query = "SELECT * FROM station WHERE date_to>'" + request_holder.minRemoved + "' AND date_to<'" + request_holder.maxRemoved + "';";
                        } else if (request_holder.minRemoved == "" && request_holder.maxRemoved != "") {
                            query = "SELECT * FROM station WHERE date_to<'" + request_holder.maxRemoved + "';";
                        } else if (request_holder.minRemoved != "" && request_holder.maxRemoved == "") {
                            query = "SELECT * FROM station WHERE date_to>'" + request_holder.minRemoved + "';";
                        }
                        results_removed_date.addAll(dbct1.executeIDQuery(query, c));
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    throw e;
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_removed_date);
                else stations_Ids.retainAll(results_removed_date);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check network
            if (request_holder.network_list.size() > 0) {
                ArrayList<Integer> results_network = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.network_list.size(); index++) {
                    query = "SELECT id_station FROM station_network WHERE id_network IN ("
                            + "SELECT id FROM network WHERE name ILIKE '" + request_holder.network_list.get(index) + "%');";

                    ArrayList<Integer> results_station_ids = new ArrayList();
                    results_station_ids = dbct1.executeIDStationQuery(query, c);
                    for (int i = 0; i < results_station_ids.size(); i++) {
                        if (!results_network.contains(results_station_ids.get(i)))
                            results_network.add(results_station_ids.get(i));
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_network);
                else stations_Ids.retainAll(results_network);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check inverse network
            if (request_holder.inverse_networks_list.size() > 0) {
                ArrayList<Integer> results_inverse_networks = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.inverse_networks_list.size(); index++) {
                    query = "SELECT DISTINCT id_station FROM station_network WHERE id_station NOT IN (SELECT id_station FROM station_network WHERE id_network in ( SELECT id FROM network  WHERE name ILIKE '" + request_holder.inverse_networks_list.get(index) + "%'));";

                    ArrayList<Integer> results_station_ids = new ArrayList();
                    results_station_ids = dbct1.executeIDStationQuery(query, c);
                    for (int i = 0; i < results_station_ids.size(); i++) {
                        if (!results_inverse_networks.contains(results_station_ids.get(i)))
                            results_inverse_networks.add(results_station_ids.get(i));
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_inverse_networks);
                else stations_Ids.retainAll(results_inverse_networks);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check agency
            if (request_holder.agency_list.size() > 0) {
                ArrayList<Integer> results_agency = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.agency_list.size(); index++) {
                    ArrayList<Integer> results_contacts = new ArrayList();
                    // Get list of Contact IDs
                    query = "SELECT id FROM agency WHERE name ILIKE '" + request_holder.agency_list.get(index) + "%';";
                    ArrayList<Integer> results_agency_id = new ArrayList();
                    results_agency_id = dbct1.executeIDQuery(query, c);
                    if (results_agency_id.size() > 0) {

                        // Get list of Station IDs
                        query = "SELECT id FROM contact  WHERE ";
                        for (int i = 0; i < results_agency_id.size(); i++) {
                            if (i == 0)
                                query = query + "id_agency=" + results_agency_id.get(i);
                            else
                                query = query + " OR id_agency=" + results_agency_id.get(i);
                        }
                        query = query + ";";
                        results_contacts = dbct1.executeIDQuery(query, c);

                    }

                    if (results_contacts.size() > 0) {

                        // Get list of Station IDs
                        query = "SELECT id_station FROM station_contact WHERE ";
                        for (int i = 0; i < results_contacts.size(); i++) {
                            if (i == 0)
                                query = query + "id_contact=" + results_contacts.get(i);
                            else
                                query = query + " OR id_contact=" + results_contacts.get(i);
                        }
                        query = query + ";";
                        results_agency.addAll(dbct1.executeIDStationQuery(query, c));

                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_agency);
                else stations_Ids.retainAll(results_agency);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check antenna type
            if (request_holder.antennaType_list.size() > 0) {
                ArrayList<Integer> results_antenna_type = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.antennaType_list.size(); index++) {
                    // Get item list
                    query = "SELECT item.id FROM filter_antenna\n" +
                            "INNER JOIN antenna_type ON antenna_type.id = filter_antenna.id_antenna_type \n" +
                            "INNER JOIN item_attribute ON item_attribute.id = filter_antenna.id_item_attribute \n" +
                            "INNER JOIN item ON item.id = item_attribute.id_item \n" +
                            "WHERE antenna_type.name ILIKE'" + request_holder.antennaType_list.get(index) + "'";
                    ArrayList<Integer> results_item = new ArrayList();
                    results_item = dbct1.executeIDQuery(query, c);

                    if (results_item.size() > 0) {
                        query = "SELECT * FROM station_item WHERE ";
                        for (int i = 0; i < results_item.size(); i++) {
                            if (i == 0)
                                query = query + "id_item=" + results_item.get(i);
                            else
                                query = query + " OR id_item=" + results_item.get(i);
                        }
                        query = query + ";";
                        results_antenna_type.addAll(dbct1.executeIDStationQuery(query, c));
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_antenna_type);
                else stations_Ids.retainAll(results_antenna_type);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check receiver type
            if (request_holder.receiverType_list.size() > 0) {
                ArrayList<Integer> results_receiver_type = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.receiverType_list.size(); index++) {
                    // Get item list
                    query = "SELECT item.id FROM filter_receiver\n" +
                            "INNER JOIN receiver_type ON receiver_type.id = filter_receiver.id_receiver_type \n" +
                            "INNER JOIN item_attribute ON item_attribute.id = filter_receiver.id_item_attribute \n" +
                            "INNER JOIN item ON item.id = item_attribute.id_item \n" +
                            "WHERE receiver_type.name ILIKE'" + request_holder.receiverType_list.get(index) + "'";
                    ArrayList<Integer> results_item = new ArrayList();
                    results_item = dbct1.executeIDQuery(query, c);

                    if (results_item.size() > 0) {
                        query = "SELECT * FROM station_item WHERE ";
                        for (int i = 0; i < results_item.size(); i++) {
                            if (i == 0)
                                query = query + "id_item=" + results_item.get(i);
                            else
                                query = query + " OR id_item=" + results_item.get(i);
                        }
                        query = query + ";";
                        results_receiver_type.addAll(dbct1.executeIDStationQuery(query, c));
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_receiver_type);
                else stations_Ids.retainAll(results_receiver_type);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check radome type
            if (request_holder.radomeType_list.size() > 0) {
                ArrayList<Integer> results_radome_type = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.radomeType_list.size(); index++) {
                    // Get item list
                    query = "SELECT item.id FROM filter_radome\n" +
                            "INNER JOIN radome_type ON radome_type.id = filter_radome.id_radome_type \n" +
                            "INNER JOIN item_attribute ON item_attribute.id = filter_radome.id_item_attribute \n" +
                            "INNER JOIN item ON item.id = item_attribute.id_item \n" +
                            "WHERE radome_type.name ILIKE'" + request_holder.radomeType_list.get(index) + "'";
                    ArrayList<Integer> results_item = new ArrayList();
                    results_item = dbct1.executeIDQuery(query, c);

                    if (results_item.size() > 0) {
                        query = "SELECT * FROM station_item WHERE ";
                        for (int i = 0; i < results_item.size(); i++) {
                            if (i == 0)
                                query = query + "id_item=" + results_item.get(i);
                            else
                                query = query + " OR id_item=" + results_item.get(i);
                        }
                        query = query + ";";
                        results_radome_type.addAll(dbct1.executeIDStationQuery(query, c));
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_radome_type);
                else stations_Ids.retainAll(results_radome_type);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check country
            if (request_holder.country_list.size() > 0) {
                ArrayList<Integer> results_country = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.country_list.size(); index++) {
                    // Get state list
                    query = "SELECT * FROM state WHERE id_country=("
                            + "SELECT id FROM country WHERE name ILIKE '" + request_holder.country_list.get(index) + "%');";
                    ArrayList<Integer> results_states = new ArrayList();
                    results_states = dbct1.executeIDQuery(query, c);

                    if (results_states.size() > 0) {
                        query = "SELECT id FROM city WHERE ";
                        // Get city list
                        for (int i = 0; i < results_states.size(); i++) {
                            if (i == 0)
                                query = query + "id_state=" + results_states.get(i);
                            else
                                query = query + " OR id_state=" + results_states.get(i);
                        }
                        query = query + ";";
                        ArrayList<Integer> results_cities = new ArrayList();
                        results_cities = dbct1.executeIDQuery(query, c);

                        if (results_cities.size() > 0) {
                            // Get location list
                            query = "SELECT id FROM location WHERE ";
                            for (int i = 0; i < results_cities.size(); i++) {
                                if (i == 0)
                                    query = query + "id_city=" + results_cities.get(i);
                                else
                                    query = query + " OR id_city=" + results_cities.get(i);
                            }
                            query = query + ";";
                            ArrayList<Integer> results_location = new ArrayList();
                            results_location = dbct1.executeIDQuery(query, c);

                            if (results_location.size() > 0) {
                                query = "SELECT id FROM station WHERE ";
                                for (int i = 0; i < results_location.size(); i++) {
                                    if (i == 0)
                                        query = query + "id_location=" + results_location.get(i);
                                    else
                                        query = query + " OR id_location=" + results_location.get(i);
                                }
                                query = query + ";";
                                results_country.addAll(dbct1.executeIDQuery(query, c));
                            }
                        }
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_country);
                else stations_Ids.retainAll(results_country);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check state
            if (request_holder.state_list.size() > 0){
                ArrayList<Integer> results_state = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.state_list.size(); index++) {
                    // Get state list
                    query = "SELECT id FROM state WHERE name ILIKE '" + request_holder.state_list.get(index) + "%';";
                    ArrayList<Integer> results_states = new ArrayList();
                    results_states = dbct1.executeIDQuery(query, c);

                    if (results_states.size() > 0) {
                        // Get city list
                        query = "SELECT * FROM city WHERE ";
                        for (int i = 0; i < results_states.size(); i++) {
                            if (i == 0)
                                query = query + "id_state=" + results_states.get(i);
                            else
                                query = query + " OR id_state=" + results_states.get(i);
                        }
                        query = query + ";";
                        ArrayList<Integer> results_cities = new ArrayList();
                        results_cities = dbct1.executeIDQuery(query, c);

                        if (results_cities.size() > 0) {
                            // Get location list
                            query = "SELECT id FROM location WHERE ";
                            for (int i = 0; i < results_cities.size(); i++) {
                                if (i == 0)
                                    query = query + "id_city=" + results_cities.get(i);
                                else
                                    query = query + " OR id_city=" + results_cities.get(i);
                            }
                            query = query + ";";
                            ArrayList<Integer> results_location = new ArrayList();
                            results_location = dbct1.executeIDQuery(query, c);

                            if (results_location.size() > 0) {
                                query = "SELECT id FROM station WHERE ";
                                for (int i = 0; i < results_location.size(); i++) {
                                    if (i == 0)
                                        query = query + "id_location=" + results_location.get(i);
                                    else
                                        query = query + " OR id_location=" + results_location.get(i);
                                }
                                query = query + ";";
                                results_state.addAll(dbct1.executeIDQuery(query, c));
                            }
                        }
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_state);
                else stations_Ids.retainAll(results_state);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check city
            if (request_holder.city_list.size() > 0) {
                ArrayList<Integer> results_city = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.city_list.size(); index++) {
                    // Get state list
                    query = "SELECT id FROM city WHERE name ILIKE '" + request_holder.city_list.get(index) + "%';";
                    ArrayList<Integer> results_cities = new ArrayList();
                    results_cities = dbct1.executeIDQuery(query, c);

                    if (results_cities.size() > 0) {
                        // Get location list
                        query = "SELECT id FROM location WHERE ";
                        for (int i = 0; i < results_cities.size(); i++) {
                            if (i == 0)
                                query = query + "id_city=" + results_cities.get(i);
                            else
                                query = query + " OR id_city=" + results_cities.get(i);
                        }
                        query = query + ";";
                        ArrayList<Integer> results_location = new ArrayList();
                        results_location = dbct1.executeIDQuery(query, c);

                        if (results_location.size() > 0) {
                            query = "SELECT id FROM station WHERE ";
                            for (int i = 0; i < results_location.size(); i++) {
                                if (i == 0)
                                    query = query + "id_location=" + results_location.get(i);
                                else
                                    query = query + " OR id_location=" + results_location.get(i);
                            }
                            query = query + ";";
                            results_city.addAll(dbct1.executeIDQuery(query, c));
                        }
                    }
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_city);
                else stations_Ids.retainAll(results_city);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check coordinates
            if (!request_holder.coordinates_type.isEmpty() )
            {
                ArrayList<Integer> results_coordinates = new ArrayList<Integer>();
                switch (request_holder.coordinates_type) {
                    // Handle rectangle queries
                    case "rectangle":
                        ArrayList<Station> coord_rec = new ArrayList();
                        coord_rec = dbct1.getStationCoordinates(request_holder.minLat, request_holder.maxLat,
                                request_holder.minLon, request_holder.maxLon);
                        for (int i = 0; i < coord_rec.size(); i++)
                            results_coordinates.add(coord_rec.get(i).getId());
                        break;

                    // Handle circle queries
                    case "circle":
                        ArrayList<Station> coord_cir = new ArrayList();
                        coord_cir = dbct1.getStationCoordinates(request_holder.lat, request_holder.lon,
                                request_holder.radius);
                        for (int i = 0; i < coord_cir.size(); i++)
                            results_coordinates.add(coord_cir.get(i).getId());
                        break;

                    // Handle polygon queries
                    case "polygon":
                        ArrayList<Station> coord_pol = new ArrayList();
                        coord_pol = dbct1.getStationCoordinates(request_holder.coordinates_list);
                        for (int i = 0; i < coord_pol.size(); i++)
                            results_coordinates.add(coord_pol.get(i).getId());
                        break;
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_coordinates);
                else stations_Ids.retainAll(results_coordinates);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check satellite
            if (request_holder.satellite_list.size() > 0) {
                ArrayList<Integer> results_satellite = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.satellite_list.size(); index++) {

                    query = "SELECT DISTINCT st_it.id_station\n" +
                            "FROM station_item as st_it\n" +
                            "JOIN item as it ON it.id = st_it.id_item\n" +
                            "JOIN item_attribute as it_at ON it_at.id_item = it.id AND it_at.value_varchar ILIKE '%" + request_holder.satellite_list.get(index) + "%';";
                    results_satellite.addAll(dbct1.executeIDStationQuery(query, c));
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_satellite);
                else stations_Ids.retainAll(results_satellite);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check station type
            if (request_holder.station_type_list.size() > 0) {
                ArrayList<Integer> results_station_type = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.station_type_list.size(); index++) {
                    // Query to be executed
                    query = "SELECT id FROM station WHERE id_station_type=("
                            + "SELECT id FROM station_type WHERE name='" + request_holder.station_type_list.get(index) + "') "
                            + "ORDER BY id;";

                    //Execute query
                    results_station_type.addAll(dbct1.executeIDQuery(query, c));
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_station_type);
                else stations_Ids.retainAll(results_station_type);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }


            // Check date range
            if (request_holder.date_range_list.size() > 0) {
                ArrayList<Integer> results_date_range = new ArrayList<Integer>();
                String strdate_range = "";
                for (int index = 0; index < request_holder.date_range_list.size(); index++) {
                    strdate_range += request_holder.date_range_list.get(index);
                }

                if (strdate_range.substring(0, 1).equalsIgnoreCase(",") == true)//check if the user has entered the dates_range0
                {
                    dates_range0 = "";
                } else {
                    dates_range0 = strdate_range.substring(0, 10);
                }

                if (strdate_range.substring(strdate_range.length() - 1).equalsIgnoreCase(",") == true)//check if the user has entered the dates_range1
                {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();//take the current date
                    dates_range1 = dateFormat.format(date);
                } else {
                    if (strdate_range.substring(0, 1).equalsIgnoreCase(",") == true) {
                        dates_range1 = strdate_range.substring(1, 11);
                    } else {
                        dates_range1 = strdate_range.substring(11, 21);
                    }
                }
                if (dates_range0.equalsIgnoreCase("") == true) {
                    query = "SELECT DISTINCT id_station FROM rinex_file WHERE reference_date<='" + dates_range1 + " 23:59:59'";
                } else {
                    query = "SELECT DISTINCT id_station FROM rinex_file WHERE reference_date>='" + dates_range0 + "' AND reference_date<='" + dates_range1 + " 23:59:59'";
                }
                results_date_range.addAll(dbct1.executeIDStationQuery(query, c));
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_date_range); else stations_Ids.retainAll(results_date_range);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // Check publish date
            if (request_holder.publish_date_list.size() > 0) {
                ArrayList<Integer> results_publish_date = new ArrayList<Integer>();
                String strdate_range = "";
                for (int index = 0; index < request_holder.publish_date_list.size(); index++) {
                    strdate_range += request_holder.publish_date_list.get(index);
                }

                if (strdate_range.substring(0, 1).equalsIgnoreCase(",") == true)//check if the user has entered the dates_range0
                {
                    dates_pub0 = "";
                } else {
                    dates_pub0 = strdate_range.substring(0, 10);
                }

                if (strdate_range.substring(strdate_range.length() - 1).equalsIgnoreCase(",") == true)//check if the user has entered the dates_range1
                {
                    DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                    Date date = new Date();//take the current date
                    dates_pub1 = dateFormat.format(date);
                } else {
                    if (strdate_range.substring(0, 1).equalsIgnoreCase(",") == true) {
                        dates_pub1 = strdate_range.substring(1, 11);
                    } else {
                        dates_pub1 = strdate_range.substring(11, 21);
                    }
                }
                if (dates_pub0.equalsIgnoreCase("") == true) {
                    query = "SELECT DISTINCT id_station FROM rinex_file WHERE published_date<='" + dates_pub1 + " 23:59:59'";
                } else {
                    query = "SELECT DISTINCT id_station FROM rinex_file WHERE published_date>='" + dates_pub0 + "' AND published_date<='" + dates_pub1 + " 23:59:59'";
                }
                results_publish_date.addAll(dbct1.executeIDStationQuery(query, c));
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_publish_date); else stations_Ids.retainAll(results_publish_date);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }




            if (request_holder.file_type_list.size()>0) {
                    ArrayList<Integer> results_file_type = new ArrayList<Integer>();
                    // Check file type
                    for (int index = 0; index < request_holder.file_type_list.size(); index++) {
                        query = "SELECT DISTINCT rf.id_station FROM rinex_file as rf JOIN file_type as ft ON rf.id_file_type = ft.id WHERE ft.format = '" +
                                request_holder.file_type_list.get(index) + "'";
                        results_file_type.addAll(dbct1.executeIDStationQuery(query, c));
                    }
                    if (stations_Ids.isEmpty()) stations_Ids.addAll(results_file_type); else stations_Ids.retainAll(results_file_type);
                    if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
                }

                // Check sampling frequency
            if (request_holder.sampling_frequency_list.size()>0) {
                ArrayList<Integer> results_sampling_frequency = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.sampling_frequency_list.size(); index++) {
                    query = "SELECT DISTINCT rf.id_station FROM rinex_file as rf JOIN file_type as ft ON rf.id_file_type = ft.id WHERE ft.sampling_frequency = '" +
                            request_holder.sampling_frequency_list.get(index) + "'";
                    results_sampling_frequency.addAll(dbct1.executeIDStationQuery(query, c));
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_sampling_frequency); else stations_Ids.retainAll(results_sampling_frequency);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

             // Check sampling window
            if (request_holder.sampling_frequency_list.size()>0) {
                ArrayList<Integer> results_sampling_window = new ArrayList<Integer>();
                for (int index = 0; index < request_holder.sampling_frequency_list.size(); index++) {
                    query = "SELECT DISTINCT rf.id_station FROM rinex_file as rf JOIN file_type as ft ON rf.id_file_type = ft.id WHERE ft.sampling_window = '" +
                            request_holder.sampling_window_list.get(index) + "'";
                    results_sampling_window.addAll(dbct1.executeIDStationQuery(query, c));
                }
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_sampling_window); else stations_Ids.retainAll(results_sampling_window);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */ return count_and_rinex;
            }

            // Check status file
            if (request_holder.statusfile_list.size()>0) {
                ArrayList<Integer> results_statusfile = new ArrayList<Integer>();
                query = "( " + parseInt(request_holder.statusfile_list.get(0));
                for (int index = 1; index < request_holder.statusfile_list.size(); index++) {
                    query += ", " + parseInt(request_holder.statusfile_list.get(index));
                }
                query += " )";
                query = "SELECT DISTINCT rf.id_station FROM rinex_file as rf WHERE rf.status in " + query ;
                results_statusfile.addAll(dbct1.executeIDStationQuery(query, c));
                if (stations_Ids.isEmpty()) stations_Ids.addAll(results_statusfile); else stations_Ids.retainAll(results_statusfile);
                if (stations_Ids.isEmpty()) /* there are no stations matching the parameters return empty list */  return count_and_rinex;
            }

            // No results .. do we want to continue if parameter count is zero
            // if request_holder.minimumObservation == NUL && request_holder.data_availability_list == NULL and

            
            // Count valid parameters
            {
                int counter=0;
                counter += request_holder.charCode_list.size();
                counter += request_holder.siteName_list.size();
                counter += request_holder.height_list.size();
                counter += request_holder.latitude_list.size();
                counter += request_holder.longitude_list.size();
                counter += request_holder.altitude_list.size();
                counter += request_holder.network_list.size();
                counter += request_holder.agency_list.size();
                counter += request_holder.antennaType_list.size();
                counter += request_holder.receiverType_list.size();
                counter += request_holder.radomeType_list.size();
                counter += request_holder.country_list.size();
                counter += request_holder.state_list.size();
                counter += request_holder.city_list.size();
                counter += request_holder.satellite_list.size();
                counter += request_holder.station_type_list.size();
                counter += request_holder.inverse_networks_list.size();
                counter += request_holder.date_range_list.size();
                counter += request_holder.publish_date_list.size();
                counter += request_holder.file_type_list.size();
                counter += request_holder.sampling_frequency_list.size();
                counter += request_holder.sampling_window_list.size();
                counter += request_holder.statusfile_list.size();

                if (request_holder.installdate == true)  counter++;
                if (request_holder.removedate == true)   counter++;
                if (request_holder.coordinates_type.contentEquals("rectangle") ||
                        request_holder.coordinates_type.contentEquals("circle") ||
                        request_holder.coordinates_type.contentEquals("polygon"))
                    counter++;

                if ( 0==counter && (request_holder.minimumObservation>0 || request_holder.data_availability_value != 0.0) )
                {
                    //add all stations
                    String queryallstation = "Select id from station";
                    try {
                        s = c.createStatement();
                        rs = s.executeQuery(queryallstation);
                        while (rs.next()) {
                            stations_Ids.add(rs.getInt("id"));
                        }
                    } catch (SQLException e) {
                        e.printStackTrace();
                        throw e;
                    }
                }
            }


            if(stations_Ids.isEmpty()){
                return count_and_rinex;
            }


            // Create rinex files query
            if ((request_holder.constellation_list.size() > 0)||
                    (request_holder.frequency_list.size() > 0)||
                    (request_holder.observationtype_list.size() > 0)||
                    (request_holder.ratioepoch > 0.0)||
                    (request_holder.elevangle > 0.0)||
                    (request_holder.multipathvalue > 0.0)||
                    (request_holder.nbcycleslips > 0)||
                    (request_holder.nbclockjumps > 0)||
                    (request_holder.spprms > 0.0))//T3
            {
                query = "SELECT DISTINCT rf.name,rf.file_size,rf.relative_path,rf.reference_date,rf.creation_date,rf.published_date,rf.revision_date,rf.md5checksum,rf.status,ft.format,ft.sampling_window,ft.sampling_frequency,dtc.acronym,dtc.hostname,dtc.protocol,dtcs.directory_naming "
                            + "FROM rinex_file as rf "
                            + "JOIN station as st ON st.id = rf.id_station "
                            + "JOIN file_type as ft ON ft.id=rf.id_file_type "
                            + "JOIN data_center_structure as dtcs ON dtcs.id=rf.id_data_center_structure "
                            + "JOIN data_center as dtc ON dtc.id=dtcs.id_data_center "
                            + "LEFT JOIN qc_report_summary as qcrepsum ON qcrepsum.id_rinexfile = rf.id "
                            + "JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id_qc_report_summary = qcrepsum.id "
                            + "JOIN constellation as const ON const.id = qcconstsum.id_constellation "
                            + "LEFT JOIN qc_observation_summary_s as qcobssum_s ON qcobssum_s.id_qc_constellation_summary = qcconstsum.id "
                            + "LEFT JOIN qc_observation_summary_c as qcobssum_c ON qcobssum_c.id_qc_constellation_summary = qcconstsum.id "
                            + "LEFT JOIN qc_observation_summary_d as qcobssum_d ON qcobssum_d.id_qc_constellation_summary = qcconstsum.id "
                            + "LEFT JOIN qc_observation_summary_l as qcobssum_l ON qcobssum_l.id_qc_constellation_summary = qcconstsum.id "
                            + "JOIN gnss_obsnames as gnss ON (gnss.id = qcobssum_s.id_gnss_obsnames) OR (gnss.id = qcobssum_c.id_gnss_obsnames) OR (gnss.id = qcobssum_d.id_gnss_obsnames) OR (gnss.id = qcobssum_l.id_gnss_obsnames) "
                            + "WHERE";
            }else{//No T3
                query = "SELECT rf.name,rf.file_size,rf.relative_path,rf.reference_date,rf.creation_date,rf.published_date,rf.revision_date,rf.md5checksum,rf.status,ft.format,ft.sampling_window,ft.sampling_frequency,dtc.acronym,dtc.hostname,dtc.protocol,dtcs.directory_naming "
                            + "FROM rinex_file as rf "
                            + "JOIN file_type as ft ON ft.id=rf.id_file_type "
                            + "JOIN data_center_structure as dtcs ON dtcs.id=rf.id_data_center_structure "
                            + "JOIN data_center as dtc ON dtc.id=dtcs.id_data_center "
                            + "WHERE";
            }
            
            
            // Check date range
            if(request_holder.date_range_list.size()>0){    
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    if(dates_range0.equalsIgnoreCase("")==true)
                    {
                        query = query + " rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }else{
                        query = query + " rf.reference_date>='" + dates_range0 + "' AND rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }    
                }else{
                    if(dates_range0.equalsIgnoreCase("")==true)
                    {
                        query = query + " AND rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }else{
                        query = query + " AND rf.reference_date>='" + dates_range0 + "' AND rf.reference_date<='" +dates_range1 + " 23:59:59" + "'";
                    }    
                }    

            }
            // Check published date
            if(request_holder.publish_date_list.size()>0){
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    if(dates_pub0.equalsIgnoreCase("")==true)
                    {
                        query = query + " rf.published_date<='" +dates_pub1 + " 23:59:59" + "'";
                    }else{
                        query = query + " rf.published_date>='" + dates_pub0 + "' AND rf.published_date<='" +dates_pub1 + " 23:59:59" + "'";
                    }
                }else{
                    if(dates_pub0.equalsIgnoreCase("")==true)
                    {
                        query = query + " AND rf.published_date<='" +dates_pub1 + " 23:59:59" + "'";
                    }else{
                        query = query + " AND rf.published_date>='" + dates_pub0 + "' AND rf.published_date<='" +dates_pub1 + " 23:59:59" + "'";
                    }
                }

            }
            
            // Check file type
            if(request_holder.file_type_list.size()>0){
                for(int index=0; index<request_holder.file_type_list.size(); index++)
                {
                    
                    if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    {
                        query = query + " ft.format ='" + request_holder.file_type_list.get(index) + "'";
                    }else{
                        query = query + " AND ft.format ='" + request_holder.file_type_list.get(index) + "'";
                    } 

                }
            }    
            // Check sampling frequency
            if(request_holder.sampling_frequency_list.size()>0){
                for(int index=0; index<request_holder.sampling_frequency_list.size(); index++)
                {
                    if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    {
                        query = query + " ft.sampling_frequency ='" + request_holder.sampling_frequency_list.get(index) + "'";
                    }else{
                        query = query + " AND ft.sampling_frequency ='" + request_holder.sampling_frequency_list.get(index) + "'";
                    }
                }
            }
            // Check sampling window
            if(request_holder.sampling_window_list.size()>0){
                for(int index=0; index<request_holder.sampling_window_list.size(); index++)
                {
                    if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    {
                        query = query + " ft.sampling_window ='" + request_holder.sampling_window_list.get(index) + "'";
                    }else{
                        query = query + " AND ft.sampling_window ='" + request_holder.sampling_window_list.get(index) + "'";
                    }
                }
            }

            // Check data availability
            // Modified by José Manteigueiro on 12/11/2020
            if(request_holder.data_availability_value != 0.0)
            {
                String[] _dates = null;
                String _startDate = null, _endDate = null;
                Boolean _startDateBoolean = false, _endDateBoolean = false;
                DateFormat _dateFormat = new SimpleDateFormat("yyyy-MM-dd");
                Double _dataAvailability = request_holder.data_availability_value;
                StringBuilder _query;
                Set<Integer> _dataAvailabilityStationIDs = new HashSet<>();

                _dates = request_holder.date_range_list.get(0).split(",");

                // Has start date defined, but no end date
                if(_dates.length == 1){
                    try{
                        // Expected format is YYYY-MM-DD
                        _startDate = _dateFormat.format(_dateFormat.parse(_dates[0]));
                        _startDateBoolean = true;
                    }catch (IllegalArgumentException _iae){
                        System.out.println("IllegalArgumentException on _startDate for dataAvailability in DBT2Connection");
                        System.out.println(_iae.getMessage());
                        _iae.printStackTrace();
                        throw _iae;
                    }catch (IndexOutOfBoundsException _ioobe){
                        System.out.println("IndexOutOfBoundsException on _startDate for dataAvailability in DBT2Connection");
                        System.out.println(_ioobe.getMessage());
                        _ioobe.printStackTrace();
                        throw _ioobe;
                    }
                }

                // Has start, end, or both dates defined
                // Additional values will be discarded for data_availability
                // start date only comes in the format of YYYY-MM-DD,
                // end date only comes in the fornmat of ,YYYY-MM-DD
                // both dates come in the format of YYYY-MM-DD,YYYY-MM-DD
                else if(_dates.length >= 2) {

                    // Only has starting date
                    if(_dates[1].equals("")) {
                        try {
                            // Expected format is YYYY-MM-DD
                            _startDate = _dateFormat.format(_dateFormat.parse(_dates[0]));
                            _startDateBoolean = true;
                        } catch (IllegalArgumentException _iae) {
                            System.out.println("IllegalArgumentException on _startDate for dataAvailability in DBT2Connection");
                            System.out.println(_iae.getMessage());
                            _iae.printStackTrace();
                            throw _iae;
                        } catch (IndexOutOfBoundsException _ioobe) {
                            System.out.println("IndexOutOfBoundsException on _startDate for dataAvailability in DBT2Connection");
                            System.out.println(_ioobe.getMessage());
                            _ioobe.printStackTrace();
                            throw _ioobe;
                        }
                    }

                    // Only has ending date
                    if(_dates[0].equals("")) {
                        try {
                            // Expected format is YYYY-MM-DD
                            _endDate = _dateFormat.format(_dateFormat.parse(_dates[1]));
                            _endDateBoolean = true;
                        } catch (IllegalArgumentException _iae) {
                            System.out.println("IllegalArgumentException on _endDate for dataAvailability in DBT2Connection");
                            System.out.println(_iae.getMessage());
                            _iae.printStackTrace();
                            throw _iae;
                        } catch (IndexOutOfBoundsException _ioobe) {
                            System.out.println("IndexOutOfBoundsException on _endDate for dataAvailability in DBT2Connection");
                            System.out.println(_ioobe.getMessage());
                            _ioobe.printStackTrace();
                            throw _ioobe;
                        }
                    }

                    if(!_dates[0].equals("") && !_dates[1].equals("")){
                        try {
                            // Expected format is YYYY-MM-DD
                            _startDate = _dateFormat.format(_dateFormat.parse(_dates[0]));
                            _startDateBoolean = true;
                        } catch (IllegalArgumentException _iae) {
                            System.out.println("IllegalArgumentException on _startDate for dataAvailability in DBT2Connection");
                            System.out.println(_iae.getMessage());
                            _iae.printStackTrace();
                            throw _iae;
                        } catch (IndexOutOfBoundsException _ioobe) {
                            System.out.println("IndexOutOfBoundsException on _startDate for dataAvailability in DBT2Connection");
                            System.out.println(_ioobe.getMessage());
                            _ioobe.printStackTrace();
                            throw _ioobe;
                        }

                        try {
                            // Expected format is YYYY-MM-DD
                            _endDate = _dateFormat.format(_dateFormat.parse(_dates[1]));
                            _endDateBoolean = true;
                        } catch (IllegalArgumentException _iae) {
                            System.out.println("IllegalArgumentException on _endDate for dataAvailability in DBT2Connection");
                            System.out.println(_iae.getMessage());
                            _iae.printStackTrace();
                            throw _iae;
                        } catch (IndexOutOfBoundsException _ioobe) {
                            System.out.println("IndexOutOfBoundsException on _endDate for dataAvailability in DBT2Connection");
                            System.out.println(_ioobe.getMessage());
                            _ioobe.printStackTrace();
                            throw _ioobe;
                        }
                    }
                }

                /*
                    Query

                    select s.marker
                    from station s
                    where (select
                    cast(count(rf.id) as decimal)/
                    cast(
                    case when date_to is null then
                    DATE_PART('day', current_date - s.date_from)
                    else DATE_PART('day', s.date_to - s.date_from)
                    end
                    as decimal) as data_availability
                    from rinex_file rf
                    where s.id = rf.id_station
                    ) > 0.001

                 */

                // If data availability is superior to 100(%), limit it to 100(%)
                if (_dataAvailability > 100.0) {
                    _dataAvailability = 100.0;
                }

                // Turn data availability into [0-1]
                _dataAvailability /= 100.0;

                // EQUAL OR LARGER THAN
                _query = new StringBuilder("SELECT s.id FROM station s WHERE ");

                // Get the number of files
                _query.append("(SELECT CAST(COUNT(rf.id) AS DECIMAL) / ");

                // Both dates
                if(_startDateBoolean && _endDateBoolean){
                    _query.append(" CAST(DATE_PART('day', ")
                            .append("( CASE WHEN '").append(_endDate).append(" 00:00:00' > s.date_to OR s.date_to IS NULL THEN ")
                            .append(" CURRENT_DATE ")
                            .append(" ELSE '").append(_endDate).append(" 00:00:00' END ) - ")
                            .append(" ( CASE WHEN '").append(_startDate).append(" 00:00:00' > s.date_from THEN '")
                            .append(_startDate).append(" 00:00:00' ")
                            .append(" ELSE s.date_from END )) AS DECIMAL) AS data_availability")
                            .append(" FROM rinex_file rf WHERE s.id = rf.id_station AND ")
                            .append(" rf.reference_date >= '").append(_startDate).append(" 00:00:00' AND ")
                            .append(" rf.reference_date <= '").append(_endDate).append(" 23:59:59' ");
                }

                // Only starting date
                if(_startDateBoolean && !_endDateBoolean){
                    _query.append(" CAST(DATE_PART('day', ")
                            .append("( CASE WHEN s.date_to IS NULL THEN ")
                            .append(" CURRENT_DATE ")
                            .append(" ELSE s.date_to END ) - ")
                            .append(" ( CASE WHEN '").append(_startDate).append(" 00:00:00' > s.date_from THEN '")
                            .append(_startDate).append(" 00:00:00' ")
                            .append(" ELSE s.date_from END )) AS DECIMAL) AS data_availability")
                            .append(" FROM rinex_file rf WHERE s.id = rf.id_station AND ")
                            .append(" rf.reference_date >= '").append(_startDate).append(" 00:00:00' ");
                }

                // Only ending date
                if(!_startDateBoolean && _endDateBoolean){
                    _query.append(" CAST(DATE_PART('day', ")
                            .append("( CASE WHEN '").append(_endDate).append(" 00:00:00' > s.date_to OR s.date_to IS NULL THEN ")
                            .append(" CURRENT_DATE ")
                            .append(" ELSE '").append(_endDate).append(" 00:00:00' END ) - ")
                            .append(" s.date_from) AS DECIMAL) AS data_availability")
                            .append(" FROM rinex_file rf WHERE s.id = rf.id_station AND ")
                            .append(" rf.reference_date <= '").append(_endDate).append(" 23:59:59' ");
                }

                // Append dataAvailability value
                _query.append(") >= ").append(_dataAvailability).append(";");

                s = c.createStatement();
                rs = s.executeQuery(_query.toString());
                while(rs.next()){
                    try{
                        _dataAvailabilityStationIDs.add(rs.getInt("id"));
                    }catch(SQLException _sqle){
                        System.out.println("SQLException on _dataAvailabilityStationIDs for dataAvailability in DBT2Connection");
                        System.out.println(_sqle.getMessage());
                        _sqle.printStackTrace();
                       throw _sqle;
                    }
                }

                // This is the first condition
                if (stations_Ids.isEmpty()){
                    stations_Ids.addAll(_dataAvailabilityStationIDs);
                }
                // Inner join the previous Set with the new one
                else {
                    stations_Ids.retainAll(_dataAvailabilityStationIDs);
                }

                // No results matching the parameters, return an empty ArrayList
                if (stations_Ids.isEmpty())
                    return count_and_rinex;

            }

            /* No idea of what this is, TODO
            // check station ids
            // to reduce sql string size will move to rf.id_station in (1,2) instead of OR's..
            if(stations_Ids.size() > 0) {
                String listOfStationIds="( rf.id_station in ( ";
                boolean first=true;
                for (Iterator<Integer> sIdIt = stations_Ids.iterator(); sIdIt.hasNext();) {
                    if (first) {
                        listOfStationIds += sIdIt.next();
                        first=!first;
                    } else {
                        listOfStationIds +=  "," + sIdIt.next();
                    }
                }
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    query += listOfStationIds + ") )";
                else
                    query += " AND " + listOfStationIds + ") )";
            }
            */
            
            // Check minimum observation
            // this will remove any stations that do not fit this criteria .. so shoudl be before ?????
            if(request_holder.minimumObservation>0.0)
            {
                String t1 = String.join(",", stations_Ids.toString());
                String t2 = t1.replace("[", "(");
                String sId = t2.replace("]", ")");

                String querycount="Select id_station, COUNT(DISTINCT reference_date) from rinex_file ";

                    if(request_holder.date_range_list.size()>0)
                    {
                        if(dates_range0.equalsIgnoreCase("")==true)//the user hasn't entered the dates_range0
                            querycount += " where  reference_date <='" + dates_range1 + " 23:59:59" + "' AND id_station= in " + sId;
                        else
                            querycount += " where reference_date >='" + dates_range0 + "' AND reference_date <='" + dates_range1 + " 23:59:59" + "' AND id_station= in " + sId;
                    }else{
                        querycount += " where id_station in " + sId ;
                    }    
                    try {
                            querycount += " group by id_station";
                            s = c.createStatement();
                            rs = s.executeQuery(querycount);

                            while (rs.next())    
                            {
                                int station = rs.getInt(1);
                                float nbyear = ((float) rs.getInt(2)) / 365;

                                if(nbyear < request_holder.minimumObservation)
                                {
                                    stations_Ids.remove(station);
                                }
                            }

                        } catch (SQLException e) {
                            throw e;
                        }
                    rs.close();
                    s.close();    

            }

            // Check status of file
            if(request_holder.statusfile_list.size()>0){
                boolean first = true;
                String searchSql="( ";
                for(int index=0; index<request_holder.statusfile_list.size(); index++)
                {
                    if (first) {
                        searchSql+= " rf.status =" + parseInt(request_holder.statusfile_list.get(index));
                        first=!first;
                    } else {
                        searchSql+=  " OR rf.status =" + parseInt(request_holder.statusfile_list.get(index));
                    }
                }
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    query += searchSql+ " )";
                else
                    query += " AND " + searchSql+ ")";
            }
            
            ///////////////T3////////////////////
            // Check constellation
            for(int index=0; index<request_holder.constellation_list.size(); index++)
            {   
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " const.name = '" + request_holder.constellation_list.get(index) + "'";
                }else{
                    query = query + " AND const.name = '" + request_holder.constellation_list.get(index) + "'";
                }
            }

            // Check frequency
            for(int index=0; index<request_holder.frequency_list.size(); index++)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " gnss.frequency_band = '" + request_holder.frequency_list.get(index) + "'";
                }else{
                    query = query + " AND gnss.frequency_band = '" + request_holder.frequency_list.get(index) + "'";
                }
            }
            
            // Check channel
            for(int index=0; index<request_holder.channel_list.size(); index++)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    if (request_holder.channel_list.get(index).equalsIgnoreCase("No Attr"))
                    {
                        query = query + " gnss.channel IS NULL";
                    }else{    
                        query = query + " gnss.channel = '" + request_holder.channel_list.get(index) + "'";
                    }    
                }else{
                    if (request_holder.channel_list.get(index).equalsIgnoreCase("No Attr"))
                    {
                        query = query + " AND gnss.channel IS NULL";
                    }else{
                        query = query + " AND gnss.channel = '" + request_holder.channel_list.get(index) + "'";
                    }    
                }
            }
            
            // Check observation type
            for(int index=0; index<request_holder.observationtype_list.size(); index++)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    if (request_holder.observationtype_list.get(index).equalsIgnoreCase("P/C"))
                    {    
                        query = query + " (gnss.obstype = 'P' OR gnss.obstype = 'C')";
                    }else{
                        query = query + " gnss.obstype = '" + request_holder.observationtype_list.get(index) + "'";
                    }    
                }else{
                    if (request_holder.observationtype_list.get(index).equalsIgnoreCase("P/C"))
                    {    
                        query = query + " AND (gnss.obstype = 'P' OR gnss.obstype = 'C')";
                    }else{
                        query = query + " AND gnss.obstype = '" + request_holder.observationtype_list.get(index) + "'";
                    }    
                }  
            }
            
            // Check epoch completeness
            if(request_holder.ratioepoch>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " (qcrepsum.obs_have * 100)/qcrepsum.obs_expt > " + request_holder.ratioepoch;
                }else{
                    query = query + " AND (qcrepsum.obs_have * 100)/qcrepsum.obs_expt > " + request_holder.ratioepoch;
                }
            }
            
            // Check elevation angle
            if(request_holder.elevangle>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " qcrepsum.obs_elev < " + request_holder.elevangle;
                }else{
                    query = query + " AND qcrepsum.obs_elev < " + request_holder.elevangle;
                }
            }
            
            // Check Multipath
            if(request_holder.multipathvalue>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " ((gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) AND (SELECT AVG(qcobssum_c.cod_mpth)\n" +
                        "FROM qc_observation_summary_c as qcobssum_c\n" +
                        "JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id = qcobssum_c.id_qc_constellation_summary\n" +
                        "JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_c.id_gnss_obsnames\n" +
                        "JOIN constellation as const ON const.id = qcconstsum.id_constellation\n" +
                        "WHERE (gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) < " + request_holder.multipathvalue;
                }else{
                    query = query + " AND ((gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) AND (SELECT AVG(qcobssum_c.cod_mpth)\n" +
                        "FROM qc_observation_summary_c as qcobssum_c\n" +
                        "JOIN qc_constellation_summary as qcconstsum ON qcconstsum.id = qcobssum_c.id_qc_constellation_summary\n" +
                        "JOIN gnss_obsnames as gnss ON gnss.id = qcobssum_c.id_gnss_obsnames\n" +
                        "JOIN constellation as const ON const.id = qcconstsum.id_constellation\n" +
                        "WHERE (gnss.name = 'C1' AND const.name = 'GPS') OR (gnss.name = 'P2' AND const.name = 'GPS')) < " + request_holder.multipathvalue;
                }
            }
            
            // Check cycle slips
            if(request_holder.nbcycleslips>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " qcrepsum.cyc_slps = " + request_holder.nbcycleslips;
                }else{
                    query = query + " AND qcrepsum.cyc_slps = " + request_holder.nbcycleslips;
                }
            }
            
            // Check clock jumps
            if(request_holder.nbclockjumps>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " qcrepsum.clk_jmps = " + request_holder.nbclockjumps;
                }else{
                    query = query + " AND qcrepsum.clk_jmps = " + request_holder.nbclockjumps;
                }
            }
            
            // Check SPP RMS
            if(request_holder.spprms>0.0)
            {
                if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                {
                    query = query + " const.name = 'GPS' AND sqrt((qcconstsum.x_rms*qcconstsum.x_rms) + (qcconstsum.y_rms*qcconstsum.y_rms) + (qcconstsum.z_rms*qcconstsum.z_rms)) < " + request_holder.spprms;
                }else{
                    query = query + " AND const.name = 'GPS' AND sqrt((qcconstsum.x_rms*qcconstsum.x_rms) + (qcconstsum.y_rms*qcconstsum.y_rms) + (qcconstsum.z_rms*qcconstsum.z_rms)) < " + request_holder.spprms;
                }
            }

            if ( stations_Ids.isEmpty() )
                return count_and_rinex;
            else
            {
                if ((request_holder.constellation_list.size() > 0)||(request_holder.frequency_list.size() > 0)||(request_holder.observationtype_list.size() > 0)||(request_holder.ratioepoch > 0.0)||(request_holder.elevangle > 0.0)||(request_holder.multipathvalue > 0.0)||(request_holder.nbcycleslips > 0)||(request_holder.nbclockjumps > 0)||(request_holder.spprms > 0.0))//T3
                {
                    if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    {
                        boolean first=true;
                        for (Iterator<Integer> sIdIt = stations_Ids.iterator(); sIdIt.hasNext();)
                        {
                            Integer sId=sIdIt.next();
                            s = c.createStatement();
                            //paul .. this had + + below
                            rs = s.executeQuery("SELECT marker FROM station WHERE id=" + sId);//find marker because id station is not the same on each node
                            while (rs.next()) {
                                if (first){
                                    query = query + " st.marker='" + rs.getString("marker") + "'";
                                    first = false;
                                }
                                else
                                    query = query + " OR st.marker='" + rs.getString("marker") + "'";
                            }
                            
                        } 
                    }else{
                        boolean first=true;
                        for (Iterator<Integer> sIdIt = stations_Ids.iterator(); sIdIt.hasNext();)
                        {
                            Integer sId=sIdIt.next();
                            s = c.createStatement();
                            rs = s.executeQuery("SELECT marker FROM station WHERE id=" + sId);
                            while (rs.next())
                            {
                                if (first) {
                                    query = query + " AND (st.marker='" + rs.getString("marker") + "'";
                                    first = false;
                                }
                                else
                                    query = query + " OR st.marker='" + rs.getString("marker") + "'";
                            }
                            
                        }
                        query = query + ")";
                    }
                }else{    
                    if (query.substring(query.length() - 5).equalsIgnoreCase("WHERE"))
                    {
                        boolean first=true;
                        for (Iterator<Integer> sIdIt = stations_Ids.iterator(); sIdIt.hasNext();)
                        {
                            Integer sId=sIdIt.next();
                            if (first) {
                                query = query + " rf.id_station=" + sId;
                                first = false;
                            }
                            else
                                query = query + " OR rf.id_station=" + sId;
                        }    
                    }else{
                        boolean first=true;
                        for (Iterator<Integer> sIdIt = stations_Ids.iterator(); sIdIt.hasNext();)
                        {
                            Integer sId=sIdIt.next();
                            if (first) {
                                query = query + " AND (rf.id_station=" + sId;
                                first = false;
                            }
                            else
                                query = query + " OR rf.id_station=" + sId;
                        }
                        query = query + ")";
                    }
                }    
            }

            if(bad_files == 1){
                query = query + " and rf.status > 0";
            }
            String modifiedQuery = query.replaceFirst("SELECT.*?FROM", "SELECT COUNT(*) FROM");
            query = query + " ORDER BY rf.creation_date";

            try (Statement stmt2 = c.createStatement();
                 ResultSet rs2 = stmt2.executeQuery(modifiedQuery)) {
                if (rs2.next()) {
                    totalCount = rs2.getInt(1);
                }
            }
            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            if ((request_holder.constellation_list.size() > 0)||(request_holder.frequency_list.size() > 0)||(request_holder.channel_list.size() > 0)||(request_holder.observationtype_list.size() > 0)||(request_holder.ratioepoch > 0.0)||(request_holder.elevangle > 0.0)||(request_holder.multipathvalue > 0.0)||(request_holder.nbcycleslips > 0)||(request_holder.nbclockjumps > 0)||(request_holder.spprms > 0.0))//T3
            {
                String querynode = "SELECT DISTINCT no.host,no.port,no.dbname,no.username,no.password "
                                + "FROM node as no "
                                + "JOIN connections as cx ON cx.destiny=no.id "
                                + "JOIN station as st ON st.id=cx.station "
                                + "WHERE cx.metadata='T1'";
                
                if (stations_Ids.size() > 0)
                {
                    boolean first=true;
                    for (Iterator<Integer> sIdIt = stations_Ids.iterator(); sIdIt.hasNext();)
                    {
                        Integer sId=sIdIt.next();
                        if (first) {
                            querynode = querynode + " AND (cx.station=" + sId;
                            first = false;
                        }
                        else
                            querynode = querynode + " OR cx.station=" + sId;
                    }
                    querynode = querynode + ")";
                }

                querynode = querynode + ";";
                
                s_node = c.createStatement();
                rs_node = s_node.executeQuery(querynode);
                Connection cnode = null;
                while (rs_node.next())
                {
                    try{ 
                        Class.forName("org.postgresql.Driver");
                        String url = "jdbc:postgresql://" + rs_node.getString("host") + ":" + rs_node.getString("port") + "/" + rs_node.getString("dbname");
                        Properties props = new Properties();
                        props.setProperty("user",rs_node.getString("username"));
                        props.setProperty("password",rs_node.getString("password"));
                        props.setProperty("loginTimeout","2");
                        cnode = DriverManager.getConnection(url, props);
                        cnode.setAutoCommit(false);
                        ArrayList<Rinex_file> results_from_node = new ArrayList();
                        results_from_node = executeRinexFilesForCombinationQuery(results_from_node, query, cnode);//Execute from each leaf node
                    
                        for(int i=0; i<results_from_node.size(); i++)
                        {
                            results.add(results_from_node.get(i));
                        }
                    }catch (Exception e) {
                         System.out.println("Unable to connect to DB");
                         throw e;
                    } 
                    //Execute from sub level node
                    /*Statement s_subnode = cnode.createStatement();
                    ResultSet rs_subnode = s_subnode.executeQuery(querynode);
                    while (rs_subnode.next())
                    {
                        Class.forName("org.postgresql.Driver");
                        Connection csubnode = DriverManager.getConnection("jdbc:postgresql://" + rs_subnode.getString("host") + ":" + rs_subnode.getString("port") + "/" + rs_subnode.getString("dbname"),rs_subnode.getString("username"), rs_subnode.getString("password"));
                        cnode.setAutoCommit(false);
                        ArrayList<Rinex_file> results_from_subnode = new ArrayList();
                        results_from_subnode = executeRinexFilesForCombinationQuery(results_from_subnode, query, csubnode);//Execute from each leaf node
                        for(int i=0; i<results_from_subnode.size(); i++)
                        {
                            results.add(results_from_subnode.get(i));
                        }    
                    }
                    rs_subnode.close();
                    s_subnode.close();*/
                }

                rs_node.close();
                s_node.close();
                
                //Execute from itself
                ArrayList<Rinex_file> results_from_node = new ArrayList();
                results_from_node = executeRinexFilesForCombinationQuery(results_from_node, query, c);
                for(int i=0; i<results_from_node.size(); i++)
                {
                    results.add(results_from_node.get(i));
                }    
            }else{
                //Execute query
                results = executeRinexFilesForCombinationQuery(results, query, c);
            }    
            
        } catch (SocketException | ClassNotFoundException | SQLException e) {
            System.out.println(e.getMessage());
            e.printStackTrace();
            throw e;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    


    public Tuple2 getFilesStationDates(String date_from, String date_to,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        try{
            c = NewConnection();

            // Get Stations ID's
            ArrayList<Integer> stationList = new ArrayList();
            /*Old query

            String query = "SELECT id FROM station WHERE date_from>='" + date_from
                    + "' AND date_to<='" + date_to + "' ORDER BY id;";

             */

            // This is the new query. Only stations that were active in the entire time are shown
            String query = "SELECT id FROM station WHERE" +
                    " date_from <= '" + date_from + "'" +
                    " AND (date_to >= '" + date_to + "'" +
                    " OR date_to IS NULL);";

            stationList = dbct1.executeIDQuery(query, c);

            // Check if there are any stations
            if(stationList.size() <= 0)
                return count_and_rinex;

            // Get Rinex File
            query = "SELECT * FROM rinex_file WHERE (id_station=";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE (id_station=";
            for (int i=0; i<stationList.size(); i++)
            {
                if (i == stationList.size() - 1) {
                    query = new StringBuilder().append(query).append(stationList.get(i)).append(")").toString();
                    query_count = new StringBuilder().append(query).append(stationList.get(i)).append(")").toString();
                }
                else {
                    query = new StringBuilder().append(query).append(stationList.get(i)).append(" OR id_station=").toString();
                    query_count = new StringBuilder().append(query).append(stationList.get(i)).append(" OR id_station=").toString();
                }
            }
            if(bad_files == 1){
                query = query + " and status > 0";
                query_count = query_count + " and status > 0";
            }
            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);

            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }
            /*
            // Get File Generated ID's
            for(int i=0; i<stationList.size(); i++)
            {
                query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
                files_generatedList = dbct1.executeIDQuery(query, c);

                // Get Files
                for(int j=0; j<files_generatedList.size(); j++)
                {
                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
                    ArrayList<Rinex_file> subresults = new ArrayList();
                    subresults = executeRinexFilesQuery(subresults, query, c);

                    // Save results to main ArrayList
                    for(int k=0; k<subresults.size(); k++)
                    {
                        results.add(subresults.get(k));
                    }
                }
            }
            */

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    
    public ArrayList getFileTypeList()
    {
        Connection c = null;
        // ArrayList with the results of the query
        ArrayList<File_type> results = new ArrayList();

        try{
            c = NewConnection();

            // Query to be executed
            String query = "SELECT * FROM file_type ORDER BY id;";

            //Execute query
            results = executeFileTypeQuery(query, c);

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error "+ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }
    
    /**
     * EXECUTE FILE TYPE QUERY
     * This method executes a given query on the file type inserted on the EPOS
     * database and returns a ArrayList with the results.
     */
    private ArrayList<File_type> executeFileTypeQuery(String query, Connection c) throws SQLException
    {
        ArrayList<File_type> results = new ArrayList<>();

        // Initialized statement for select
        s = c.createStatement();
        rs = s.executeQuery(query);

        // Check if there are any queries
        while (rs.next())
        {
            // Add file type to result ArrayList
            results.add(new File_type(
                    rs.getInt("id"),
                    rs.getString("format"),
                    rs.getString("sampling_window"),
                    rs.getString("sampling_frequency")
            ));
        }

        // Close connections
        rs.close();
        s.close();

        return results;
    }


    public Tuple2 getRinexFilesMD5(String md5,int pageNumber,int perPageNumber, int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);

        try {
            c = NewConnection();

            // Get Rinex File
            String query = "SELECT * FROM rinex_file WHERE md5checksum = '" + md5 + "'";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE md5checksum = '" + md5 + "'";

            if(bad_files == 1){
                query = query + " and status > 0";
                query_count = query_count + " and status > 0";
            }

            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

            results = executeRinexFilesQuery(results, query, c);


        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error " + ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }


    public Tuple2 getRinexFilesFileName(String file_name,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);
        try {
            c = NewConnection();

            // Get Rinex File
            String query = "SELECT * FROM rinex_file WHERE name = '" + file_name + "'";
            String query_count = "SELECT COUNT(*) FROM rinex_file WHERE name = '" + file_name + "'";

            if(bad_files == 1){
                query = query + " and status > 0";
                query_count = query_count + " and status > 0";
            }

            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);

            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error " + ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }
    public Tuple2 getRinexFilesFileDataCenterAcronym(String acronym,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);

        try {
            c = NewConnection();

            // Get Rinex File
            String query = "SELECT rf.* " +
                    "FROM rinex_file rf " +
                    "INNER JOIN data_center_structure dcs ON dcs.id = rf.id_data_center_structure " +
                    "INNER JOIN data_center dc ON dc.id = dcs.id_data_center " +
                    "WHERE dc.acronym = '" + acronym + "'";
            String query_count = "SELECT COUNT(rf.*) " +
                    "FROM rinex_file rf " +
                    "INNER JOIN data_center_structure dcs ON dcs.id = rf.id_data_center_structure " +
                    "INNER JOIN data_center dc ON dc.id = dcs.id_data_center " +
                    "WHERE dc.acronym = '" + acronym + "'";
            if(bad_files == 1){
                query = query + " and status > 0";
                query_count = query_count + "and status > 0;";
            }
           query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error " + ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }

        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }



    public Tuple2 getRinexFilesFileNameWithMd5(String file_name, String md5checksum,int pageNumber,int perPageNumber,int bad_files) throws SocketException, SQLException, ClassNotFoundException {
        Connection c = null;
        DataBaseConnection dbct1 = new DataBaseConnection();
        // ArrayList with the results of the query
        ArrayList<Rinex_file> results = new ArrayList();
        int totalCount = 0;
        Tuple2<Integer,ArrayList<Rinex_file>> count_and_rinex = new Tuple2<>(0,results);

        try {
            c = NewConnection();

            // Get Rinex File
            String query = "SELECT * FROM rinex_file " +
                    "WHERE name = '" + file_name +
                    "' and md5checksum = '" + md5checksum + "'";
            String query_count = "SELECT COUNT(*) FROM rinex_file " +
                    "WHERE name = '" + file_name +
                    "' and md5checksum = '" + md5checksum + "'";
            if(bad_files == 1){
                query = query + " and status > 0";
                query_count = query_count + " and status > 0";
            }
            query = query + " ORDER BY creation_date";

            if (pageNumber>0)
                query = query +( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            results = executeRinexFilesQuery(results, query, c);
            try (Statement stmt = c.createStatement();
                 ResultSet rs = stmt.executeQuery(query_count)) {
                if (rs.next()) {
                    totalCount = rs.getInt(1);
                }
            }

        } catch (SocketException | ClassNotFoundException | SQLException ex) {
            System.out.println("Error " + ex.getMessage());
            throw ex;
        } finally {
            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    throw ex;
                }
            }
        }
        count_and_rinex.setFirst(totalCount);
        count_and_rinex.setSecond(results);
        return count_and_rinex;
    }

    public ArrayList<FilesInfo> getFilesInfo(String marker_long_name, List<Integer> statusInts, String datacenter_acronym, String date_from, String date_to, int pageNumber, int perPageNumber) throws OutOfMemoryError{
        Connection c = null;
        PreparedStatement ps = null;
        String[] status_array;
        ArrayList<String> stored_procedures_array = new ArrayList<>();
        ArrayList<Integer> stored_procedures_int_array = new ArrayList<>();
        ArrayList<String> stored_procedures_date_array = new ArrayList<>();
        ArrayList<String> stored_procedures_dc_array = new ArrayList<>();

        // ArrayList with the results of the query
        ArrayList<FilesInfo> results = new ArrayList();
        FilesInfo file_info_aux;
        String errors_aux;

        try {
            c = NewConnection();

            // Get Rinex Files Info
            StringBuilder query = new StringBuilder();
            query.append("select markerlongname(st.marker, st.monument_num, st.receiver_num, st.country_code), rf.status, rf.id_station, rf.\"name\", rf.reference_date, dc.acronym, array_to_string(array_agg(distinct(ret.error_type)), ' & ') as errors\n");
            query.append("from station st, rinex_errors re\n");
            query.append("inner join rinex_error_types ret on ret.id = re.id_error_type\n");
            query.append("right join rinex_file rf on\n");
            query.append("rf.id  = re.id_rinex_file\n");
            query.append("inner join data_center_structure dcs on dcs.id = rf.id_data_center_structure\n");
            query.append("inner join data_center dc on dc.id = dcs.id_data_center\n");
            query.append("where st.id = rf.id_station\n");

            // Additional conditions
            if(!marker_long_name.equals("")) {
                query.append("and markerlongname(st.marker, st.monument_num, st.receiver_num, st.country_code) ilike ? \n");
                stored_procedures_array.add(marker_long_name + "%");
            }

            if(!date_from.equals("")){
                query.append("and rf.reference_date >= ? \n");
                stored_procedures_date_array.add(date_from);
            }

            if(!date_to.equals("")){
                query.append("and rf.reference_date <= ? \n");
                stored_procedures_date_array.add(date_to);
            }

            if(statusInts.size()>0){
                query.append("and (");
                for(int i=0; i<statusInts.size(); i++){
                    stored_procedures_int_array.add( statusInts.get(i) );
                    query.append("rf.status = ? ");
                    if ((i+1)<statusInts.size())
                        query.append(" OR ");
                    else
                        query.append(")\n");
                }
            }
            if(!datacenter_acronym.equals("")) {
                query.append("and dc.acronym = ? \n");
                stored_procedures_dc_array.add(datacenter_acronym);
            }

            // Append group by's
            query.append("group by rf.id_station, rf.\"name\", rf.reference_date, rf.status, rf.relative_path, st.marker,dc.acronym, ");
            query.append("st.monument_num, st.receiver_num, st.country_code ");
            query.append(" order by rf.id_station ");

            if (pageNumber>0)
                query.append( " OFFSET " +((pageNumber-1)*perPageNumber) + " FETCH NEXT " +perPageNumber+ "  ROWS ONLY" );

            query.append(" \n");

            // Create the prepared statement
            ps = c.prepareStatement(query.toString());

            int j=1;
            for(int i=0; i<stored_procedures_array.size(); i++){
                ps.setString(i+1, stored_procedures_array.get(i));
            }
            j+=stored_procedures_array.size();

            for(int i=0; i<stored_procedures_date_array.size(); i++){
                ps.setObject(i+j, LocalDate.parse(stored_procedures_date_array.get(i)).atStartOfDay());
            }
            j+=stored_procedures_date_array.size();

            for(int i=0; i<stored_procedures_int_array.size(); i++){
                ps.setInt(i+j, stored_procedures_int_array.get(i));
            }
            j+=stored_procedures_int_array.size();
            for(int i=0; i<stored_procedures_dc_array.size(); i++){
                ps.setString(i+j, stored_procedures_dc_array.get(i));
            }


            ResultSet rs = ps.executeQuery();


            while(rs.next()){
                file_info_aux = new FilesInfo(
                        rs.getString("name"),
                        rs.getString("markerlongname"),
                        rs.getInt("status"),
                        rs.getString("reference_date"),
                        rs.getString("acronym")
                );
                errors_aux = rs.getString("errors");
                file_info_aux.setErrors(errors_aux.split("&"));
                results.add(file_info_aux);
            }

        } catch (ClassNotFoundException | SQLException | SocketException | DateTimeParseException ex) {
            System.out.println("Error " + ex.getMessage());
            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }

            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException _sqlException) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, _sqlException);
                }
            }

            if (c != null) {
                try {
                    c.close();
                } catch (SQLException ex) {
                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        }

        return results;
    }
}

///*
//
//
//
//    */
///***************************************************************************
//     ***************************************************************************
//     ***************************************************************************
//     *
//     * >>>>>>>> OLD T2 METHODS BELLOW
//     *
//     ***************************************************************************
//     ***************************************************************************
//     **************************************************************************//*
//
//
//    */
///*
//    * EXECUTE FILES QUERY
//    * This method executes a given query on the stations inserted on the EPOS
//    * dataase and returns a ArrayList with the results.
//    *//*
//
//    private ArrayList executeFilesQuery(ArrayList<File> results, String query,
//                                        Connection c) throws SQLException
//    {
//        // *********************************************************************
//        // STEP 1
//        // Check data from query.
//        // *********************************************************************
//
//        // *********************************************************************
//        // Handle Files
//        // *********************************************************************
//        // Initialized statement for select
//        s = c.createStatement();
//        rs = s.executeQuery(query);
//
//        // Check if there are any results
//        while (rs.next() == true)
//        {
//            // Create File object
//            File file = new File();
//            File_generated file_generated = new File_generated();
//            File_type file_type = new File_type();
//
//            // *****************************************************************
//            // Handle File Generated
//            // *****************************************************************
//            s_file_generated = c.createStatement();
//            rs_file_generated = s_file_generated.executeQuery("SELECT * FROM "
//                    + "file_generated WHERE id=" + rs.getInt("id_file_generated"));
//
//            // Check if there are any results
//            while (rs_file_generated.next() == true)
//            {
//                // *************************************************************
//                // Handle File Type
//                // *************************************************************
//                s_file_type = c.createStatement();
//                rs_file_type = s_file_type.executeQuery("SELECT * FROM "
//                        + "file_type WHERE id=" + rs_file_generated.getInt("id_file_type"));
//
//                // Check if there are any results
//                while (rs_file_type.next() == true)
//                {
//                    // Save File Type
//                    file_type.setId(rs_file_type.getInt("id"));
//                    //file_type.setName(rs_file_type.getString("name"));
//                    file_type.setFormat(rs_file_type.getString("format"));
//                }
//
//                rs_file_type.close();
//                s_file_type.close();
//
//                // *************************************************************
//                // Handle Station Marker
//                // *************************************************************
//                s_station = c.createStatement();
//                rs_station = s_station.executeQuery("SELECT * FROM station "
//                        + "WHERE id=" + rs_file_generated.getInt("id_station"));
//
//                // Check if there are any results
//                while (rs_station.next() == true)
//                {
//                    // Save Station data
//                    file_generated.setId_station(rs_station.getInt("id"));
//                    file_generated.setStation_marker(rs_station.getString("marker"));
//                }
//
//                rs_station.close();
//                s_station.close();
//
//                // Save File Generated
//                file_generated.setId(rs_file_generated.getInt("id"));
//                file_generated.setFile_type(file_type.getId(), file_type.getFormat());
//            }
//
//            rs_file_generated.close();
//            s_file_generated.close();
//
//            // Save File info
//            file.setId(rs.getInt("id"));
//            file.setName(rs.getString("name"));
//            file.setPath(rs.getString("path"));
//            file.setFile_size(rs.getInt("file_size"));
//            file.setData_start_time(rs.getString("data_start_time"));
//            file.setData_stop_time(rs.getString("data_stop_time"));
//            file.setPublished_date(rs.getString("published_date"));
//            file.setRevision_time(rs.getString("revision_time"));
//            file.setEmbargo_after_date(rs.getString("embargo_after_date"));
//            file.setEmbargo_duration_hours(rs.getInt("embargo_duration_hours"));
//            file.setAccess_permission_id(rs.getInt("access_permission_id"));
//            file.setFile_generated(file_generated.getId(),
//                    file_generated.getId_station(),
//                    file_generated.getFile_type(),
//                    file_generated.getStation_marker());
//            file.setMd5checksum(rs.getString("md5checksum"));
//
//            // Add File to the result list
//            results.add(file);
//        }
//
//        rs.close();
//        s.close();
//
//        return results;
//    }
//
//    */
///*
//    * EXECUTE GENERATED FILES QUERY
//    * This method executes a given query on the stations inserted on the EPOS
//    * database and returns a ArrayList with the results.
//    *//*
//
//    private ArrayList executeGeneratedFilesQuery(ArrayList<File> results, String query,
//                                                 Connection c) throws SQLException
//    {
//        // *********************************************************************
//        // STEP 1
//        // Check data from query.
//        // *********************************************************************
//
//        // *********************************************************************
//        // Handle Files
//        // *********************************************************************
//        // Initialized statement for select
//        s = c.createStatement();
//        rs = s.executeQuery(query);
//
//        // Check if there are any results
//        while (rs.next() == true)
//        {
//            // Create File object
//            File file = new File();
//            File_generated file_generated = new File_generated();
//            File_type file_type = new File_type();
//
//            // *****************************************************************
//            // Handle File Type
//            // *****************************************************************
//            s_file_type = c.createStatement();
//            rs_file_type = s_file_type.executeQuery("SELECT * FROM "
//                    + "file_type WHERE id=" + rs.getInt("id_file_type"));
//
//            // Check if there are any results
//            while (rs_file_type.next() == true)
//            {
//                // Save File Type
//                file_type.setId(rs_file_type.getInt("id"));
//                //file_type.setName(rs_file_type.getString("name"));
//                file_type.setFormat(rs_file_type.getString("format"));
//            }
//
//            rs_file_type.close();
//            s_file_type.close();
//
//            // *****************************************************************
//            // Handle Station Marker
//            // *****************************************************************
//            s_station = c.createStatement();
//            rs_station = s_station.executeQuery("SELECT * FROM station "
//                    + "WHERE id=" + rs.getInt("id_station"));
//
//            // Check if there are any results
//            while (rs_station.next() == true)
//            {
//                // Save Station data
//                file_generated.setId_station(rs_station.getInt("id"));
//                file_generated.setStation_marker(rs_station.getString("marker"));
//            }
//
//            rs_station.close();
//            s_station.close();
//
//            // Save File Generated
//            file_generated.setId(rs.getInt("id"));
//            file_generated.setFile_type(file_type.getId(),  file_type.getFormat());
//
//            // *****************************************************************
//            // Handle File
//            // *****************************************************************
//            s_file = c.createStatement();
//            rs_file = s_file.executeQuery("SELECT * FROM file "
//                    + "WHERE id_file_generated=" + rs.getInt("id"));
//
//            // Check if there are any results
//            while (rs_file.next() == true)
//            {
//                // Save File data
//                file.setId(rs_file.getInt("id"));
//                file.setName(rs_file.getString("name"));
//                file.setPath(rs_file.getString("path"));
//                file.setFile_size(rs_file.getInt("file_size"));
//                file.setData_start_time(rs_file.getString("data_start_time"));
//                file.setData_stop_time(rs_file.getString("data_stop_time"));
//                file.setPublished_date(rs_file.getString("published_date"));
//                file.setRevision_time(rs_file.getString("revision_time"));
//                file.setEmbargo_after_date(rs_file.getString("embargo_after_date"));
//                file.setEmbargo_duration_hours(rs_file.getInt("embargo_duration_hours"));
//                file.setAccess_permission_id(rs_file.getInt("access_permission_id"));
//                file.setFile_generated(file_generated.getId(),
//                        file_generated.getId_station(),
//                        file_generated.getFile_type(),
//                        file_generated.getStation_marker());
//                file.setMd5checksum(rs_file.getString("md5checksum"));
//                results.add(file);
//            }
//
//            rs_file.close();
//            s_file.close();
//            //results.add(file);
//        }
//
//        rs.close();
//        s.close();
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFiles()
//    {
//        Connection c = null;
//        // ArrayList with the results of the query
//        ArrayList<Rinex_file> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Query to be executed
//            String query = "SELECT * FROM rinex_file ORDER BY id";
//
//            //Execute query
//            //results = executeFilesQuery(results, query, c);
//            results = executeRinexFilesQuery(results, query, c);
//
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES AGENCY
//    * This method retuns a ArrayList with all the files from the given agency.
//    *//*
//
//    public ArrayList getFilesAgency(String agency)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            String query = "SELECT * FROM agency WHERE name='" + agency + "' ORDER BY id;";
//            ArrayList<Agency> agencies = new ArrayList();
//            agencies = dbct1.executeAgencyQuery(agencies, query, c);
//
//            // Set agency contacts query
//            query = "SELECT * FROM contact WHERE id_agency=";
//            for (int i=0; i<agencies.size(); i++)
//            {
//                if (i == agencies.size() - 1)
//                    query = query + agencies.get(i).getId() + ";";
//                else
//                    query = query + agencies.get(i).getId() + " OR id_agency=";
//            }
//
//            // ArrayList with contact
//            ArrayList<Contact> contacts = new ArrayList();
//
//            // Get contacts
//            contacts = dbct1.executeContactQuery(query, contacts, c);
//
//            // Set network query
//            query = "SELECT * FROM network WHERE id_contact=";
//            for (int i=0; i<contacts.size(); i++)
//            {
//                if (i == contacts.size() - 1)
//                    query = query + contacts.get(i).getId() + ";";
//                else
//                    query = query + contacts.get(i).getId() + " OR id_contact=";
//            }
//
//            // Get networks
//            ArrayList<Network> networks = new ArrayList();
//            networks = dbct1.executeNetworkQuery(query, networks, c);
//
//            // Set station id query
//            query = "SELECT * FROM station_network WHERE id_network=";
//            for (int i=0; i<networks.size(); i++)
//            {
//                if (i == networks.size() - 1)
//                    query = query + networks.get(i).getId() + ";";
//                else
//                    query = query + networks.get(i).getId() + " OR id_network=";
//            }
//
//            // Get station ids
//            ArrayList<Integer> station_ids = new ArrayList();
//            station_ids = dbct1.executeStationNetworkQuery(query, station_ids, c);
//
//            // Remove duplicated Ids
//            Set<Integer> hs = new HashSet<>();
//            hs.addAll(station_ids);
//            station_ids.clear();
//            station_ids.addAll(hs);
//
//            // Set file generated query
//            query = "SELECT * FROM file_generated WHERE id_station=";
//            for (int i=0; i<station_ids.size(); i++)
//            {
//                if (i == station_ids.size() - 1)
//                    query = query + station_ids.get(i) + ";";
//                else
//                    query = query + station_ids.get(i) + " OR id_station=";
//            }
//
//            // Get files ids
//            if (station_ids.size() > 0)
//                results = executeGeneratedFilesQuery(results, query, c);
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES ANTENNA TYPE
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesAntennaType(String antennaType)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // ArrayList with Stations
//            ArrayList<Station> station_list = new ArrayList();
//
//            // Query to be executed
//            String query = "SELECT * FROM station WHERE id=("
//                    + "SELECT id_station FROM station_item WHERE id_item=("
//                    + "SELECT id_item FROM filter_antenna WHERE id_antenna_type=("
//                    + "SELECT id FROM antenna_type WHERE name='" + antennaType
//                    + "'))) ORDER BY id;";
//
//            // Execute query
//            station_list = dbct1.executeStationQuery( query, c);
//
//            // Check if the type exists
//            if(station_list.size() <= 0)
//                return results;
//
//            // ArrayList of Giles Generated
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<station_list.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES CITY
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesCity(String city)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        // ArrayList with Stations
//        ArrayList<Station> station_list = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Query to be executed
//            String query = "SELECT * FROM station WHERE id_location=("
//                    + "SELECT id FROM location WHERE id=("
//                    + "SELECT id FROM city WHERE name='" + city
//                    + "')) ORDER BY id;";
//
//            // Execute query
//            station_list = dbct1.executeStationQuery( query, c);
//
//            // Check if the type exists
//            if(station_list.size() <= 0)
//                return results;
//
//            // ArrayList of Giles Generated
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<station_list.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES COMBINED
//    * This method retuns a ArrayList with all the files with the given parameter.
//    *//*
//
//    public ArrayList getFilesCombined(String request)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//        ArrayList<Integer> results_date_range = new ArrayList();
//        ArrayList<Integer> results_publish_date = new ArrayList();
//        ArrayList<Integer> results_file_type = new ArrayList();
//        ArrayList<Integer> results_marker = new ArrayList();
//        ArrayList<Integer> results_site = new ArrayList();
//        ArrayList<Integer> results_dates_in_range = new ArrayList();
//        ArrayList<Integer> results_network = new ArrayList();
//        ArrayList<Integer> results_site_type = new ArrayList();
//        ArrayList<Integer> results_antenna_type = new ArrayList();
//        ArrayList<Integer> results_receiver_type = new ArrayList();
//        ArrayList<Integer> results_radome_type = new ArrayList();
//        ArrayList<Integer> results_country = new ArrayList();
//        ArrayList<Integer> results_state = new ArrayList();
//        ArrayList<Integer> results_city = new ArrayList();
//        ArrayList<Integer> results_agency = new ArrayList();
//        RequestT2 request_holder = new RequestT2(request);
//        String query = "";
//        int counter = 0;
//
//        try{
//            c = NewConnection();
//
//            // Parse request
//            request_holder.parseRequest();
//            request_holder.requestToString();
//
//            // Check date range
//            for(int index=0; index<request_holder.date_range_list.size(); index++)
//            {
//                String[] dates = request_holder.date_range_list.get(index).split("|");
//                query = "SELECT id FROM file WHERE data_start_time>='" + dates[0] +
//                        "' AND data_stop_time<='" + dates[1] + "' ORDER BY id";
//                results_date_range.addAll(dbct1.executeIDQuery(query, c));
//            }
//
//            // Check publish date
//            for(int index=0; index<request_holder.publish_date_list.size(); index++)
//            {
//                String[] dates = request_holder.publish_date_list.get(index).split("|");
//                query = "SELECT * FROM file WHERE published_date>='" + dates[0] +
//                        "' AND published_date<='" + dates[1] + "' ORDER BY id";
//                results_publish_date.addAll(dbct1.executeIDQuery(query, c));
//            }
//
//            // Check file type
//            for(int index=0; index<request_holder.file_type_list.size(); index++)
//            {
//                // Get type ID's
//                ArrayList<Integer> typeList = new ArrayList();
//                query = "SELECT * FROM file_type WHERE name = '" +
//                        request_holder.file_type_list.get(index) + "' ORDER BY id";
//                typeList = dbct1.executeIDQuery(query, c);
//
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<typeList.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_file_type=" + typeList.get(i) + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_file_type.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check marker
//            for(int index=0; index<request_holder.charCode_list.size(); index++)
//            {
//                // Get Stations ID's
//                ArrayList<Integer> stationList = new ArrayList();
//                query = "SELECT * FROM station WHERE marker LIKE '%" +
//                        request_holder.charCode_list.get(index) + "%' ORDER BY id";
//                stationList = dbct1.executeIDQuery(query, c);
//
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<stationList.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_marker.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check site
//            for(int index=0; index<request_holder.siteName_list.size(); index++)
//            {
//                // Get Stations ID's
//                ArrayList<Integer> stationList = new ArrayList();
//                query = "SELECT * FROM station WHERE name LIKE '%" +
//                        request_holder.siteName_list.get(index) + "%' ORDER BY id";
//                stationList = dbct1.executeIDQuery(query, c);
//
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<stationList.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_site.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check dates in range
//            for(int index=0; index<request_holder.datesInRange_list.size(); index++)
//            {
//                // Get Stations ID's
//                ArrayList<Integer> stationList = new ArrayList();
//                String[] dates = request_holder.datesInRange_list.get(index).split("|");
//                query = "SELECT id FROM station WHERE date_from>='" + dates[0]
//                        + "' AND date_to<='" + dates[1] + "' ORDER BY id;";
//                stationList = dbct1.executeIDQuery(query, c);
//
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<stationList.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_dates_in_range.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check networks
//            for(int index=0; index<request_holder.network_list.size(); index++)
//            {
//                // Get Network ID
//                query = "SELECT id FROM network WHERE name='" +
//                        request_holder.network_list.get(index) + "';";
//                ArrayList<Integer> networks_list = new ArrayList();
//                networks_list = dbct1.executeIDQuery(query, c);
//
//                // Check if the network exists
//                if(networks_list.size() <= 0)
//                    return results;
//
//                // Get Stations ID's
//                ArrayList<Integer> stationList = new ArrayList();
//                query = "SELECT id_station FROM station_network WHERE id_network=" +
//                        networks_list.get(0) + ";";
//                stationList = dbct1.executeIDStationQuery(query, c);
//
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<stationList.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_network.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check site type
//            for(int index=0; index<request_holder.siteType_list.size(); index++)
//            {
//                // Get Station Type ID
//                query = "SELECT id FROM station_type WHERE name='" +
//                        request_holder.siteType_list.get(index) + "';";
//                ArrayList<Integer> type_list = new ArrayList();
//                type_list = dbct1.executeIDQuery(query, c);
//
//                // Check if the type exists
//                if(type_list.size() <= 0)
//                    return results;
//
//                // Get Stations ID's
//                ArrayList<Integer> stationList = new ArrayList();
//                query = "SELECT * FROM station WHERE id_station_type=" +
//                        type_list.get(0) + " ORDER BY id;";
//                stationList = dbct1.executeIDQuery(query, c);
//
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<stationList.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_site_type.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check antenna type
//            for(int index=0; index<request_holder.antennaType_list.size(); index++)
//            {
//                // ArrayList with Stations
//                ArrayList<Station> station_list = new ArrayList();
//
//                // Query to be executed
//                query = "SELECT * FROM station WHERE id=("
//                        + "SELECT id_station FROM station_item WHERE id_item=("
//                        + "SELECT id_item FROM filter_antenna WHERE id_antenna_type=("
//                        + "SELECT id FROM antenna_type WHERE name='"
//                        + request_holder.antennaType_list.get(index)
//                        + "'))) ORDER BY id;";
//
//                // Execute query
//                station_list = dbct1.executeStationQuery( query, c);
//
//                // Check if the type exists
//                if(station_list.size() <= 0)
//                    return results;
//
//                // ArrayList of Giles Generated
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<station_list.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_antenna_type.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check receiver type
//            for(int index=0; index<request_holder.receiverType_list.size(); index++)
//            {
//                // ArrayList with Stations
//                ArrayList<Station> station_list = new ArrayList();
//
//                // Query to be executed
//                query = "SELECT * FROM station WHERE id=("
//                        + "SELECT id_station FROM station_item WHERE id_item=("
//                        + "SELECT id_item FROM filter_receiver WHERE id_receiver_type=("
//                        + "SELECT id FROM receiver_type WHERE name='"
//                        + request_holder.receiverType_list.get(index)
//                        + "'))) ORDER BY id;";
//
//                // Execute query
//                station_list = dbct1.executeStationQuery( query, c);
//
//                // Check if the type exists
//                if(station_list.size() <= 0)
//                    return results;
//
//                // ArrayList of Giles Generated
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<station_list.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_receiver_type.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check radome type
//            for(int index=0; index<request_holder.radomeType_list.size(); index++)
//            {
//                // ArrayList with Stations
//                ArrayList<Station> station_list = new ArrayList();
//
//                // Query to be executed
//                query = "SELECT * FROM station WHERE id=("
//                        + "SELECT id_station FROM station_item WHERE id_item=("
//                        + "SELECT id_item FROM filter_radome WHERE id_radome_type=("
//                        + "SELECT id FROM radome_type WHERE name='"
//                        + request_holder.radomeType_list.get(index)
//                        + "'))) ORDER BY id;";
//
//                // Execute query
//                station_list = dbct1.executeStationQuery( query, c);
//
//                // Check if the type exists
//                if(station_list.size() <= 0)
//                    return results;
//
//                // ArrayList of Giles Generated
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<station_list.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_radome_type.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check country
//            for(int index=0; index<request_holder.nation_list.size(); index++)
//            {
//                // ArrayList with Stations
//                ArrayList<Station> station_list = new ArrayList();
//
//                // Query to be executed
//                query = "SELECT * FROM station WHERE id_location=("
//                        + "SELECT id FROM location WHERE id=("
//                        + "SELECT id FROM city WHERE id_state=("
//                        + "SELECT id FROM state WHERE id_country=("
//                        + "SELECT id FROM country WHERE name='"
//                        + request_holder.nation_list.get(index)
//                        + "')))) ORDER BY id;";
//
//                // Execute query
//                station_list = dbct1.executeStationQuery( query, c);
//
//                // Check if the type exists
//                if(station_list.size() <= 0)
//                    return results;
//
//                // ArrayList of Giles Generated
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<station_list.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_country.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check state
//            for(int index=0; index<request_holder.state_list.size(); index++)
//            {
//                // ArrayList with Stations
//                ArrayList<Station> station_list = new ArrayList();
//
//                // Query to be executed
//                query = "SELECT * FROM station WHERE id_location=("
//                        + "SELECT id FROM location WHERE id=("
//                        + "SELECT id FROM city WHERE id_state=("
//                        + "SELECT id FROM state WHERE name='"
//                        + request_holder.state_list.get(index)
//                        + "'))) ORDER BY id;";
//
//                // Execute query
//                station_list = dbct1.executeStationQuery( query, c);
//
//                // Check if the type exists
//                if(station_list.size() <= 0)
//                    return results;
//
//                // ArrayList of Giles Generated
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<station_list.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_state.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check city
//            for(int index=0; index<request_holder.city_list.size(); index++)
//            {
//                // ArrayList with Stations
//                ArrayList<Station> station_list = new ArrayList();
//
//                // Query to be executed
//                query = "SELECT * FROM station WHERE id_location=("
//                        + "SELECT id FROM location WHERE id=("
//                        + "SELECT id FROM city WHERE name='"
//                        + request_holder.city_list.get(index)
//                        + "')) ORDER BY id;";
//
//                // Execute query
//                station_list = dbct1.executeStationQuery( query, c);
//
//                // Check if the type exists
//                if(station_list.size() <= 0)
//                    return results;
//
//                // ArrayList of Giles Generated
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<station_list.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results.add(subresults.get(k));
//                            results_city.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            // Check agency
//            for(int index=0; index<request_holder.agency_list.size(); index++)
//            {
//                // ArrayList with Agencies
//                ArrayList<File> file_agency_list = new ArrayList();
//
//                // Get file agency list
//                file_agency_list = getFilesAgency(request_holder.agency_list.get(index));
//
//                // Save results
//                for(int k=0; k<file_agency_list.size(); k++)
//                {
//                    results.add(file_agency_list.get(k));
//                    results_agency.add(file_agency_list.get(k).getId());
//                }
//            }
//
//            // Count valid parameters
//            if (results_date_range.size() > 0)
//                counter++;
//            if (results_publish_date.size() > 0)
//                counter++;
//            if (results_file_type.size() > 0)
//                counter++;
//            if (results_marker.size() > 0)
//                counter++;
//            if (results_site.size() > 0)
//                counter++;
//            if (results_dates_in_range.size() > 0)
//                counter++;
//            if (results_network.size() > 0)
//                counter++;
//            if (results_site_type.size() > 0)
//                counter++;
//            if (results_antenna_type.size() > 0)
//                counter++;
//            if (results_receiver_type.size() > 0)
//                counter++;
//            if (results_radome_type.size() > 0)
//                counter++;
//            if (results_country.size() > 0)
//                counter++;
//            if (results_state.size() > 0)
//                counter++;
//            if (results_city.size() > 0)
//                counter++;
//            if (results_agency.size() > 0)
//                counter++;
//
//            // Merge results
//            ArrayList<Integer> merged_Ids = new ArrayList();
//            merged_Ids.addAll(results_date_range);
//            merged_Ids.addAll(results_publish_date);
//            merged_Ids.addAll(results_file_type);
//            merged_Ids.addAll(results_marker);
//            merged_Ids.addAll(results_site);
//            merged_Ids.addAll(results_dates_in_range);
//            merged_Ids.addAll(results_network);
//            merged_Ids.addAll(results_site_type);
//            merged_Ids.addAll(results_antenna_type);
//            merged_Ids.addAll(results_receiver_type);
//            merged_Ids.addAll(results_radome_type);
//            merged_Ids.addAll(results_country);
//            merged_Ids.addAll(results_state);
//            merged_Ids.addAll(results_city);
//            merged_Ids.addAll(results_agency);
//
//            // Check frequency of results
//            ArrayList<Integer> files_Ids = new ArrayList();
//            for (int i=0; i<merged_Ids.size(); i++)
//            {
//                if (Collections.frequency(merged_Ids, merged_Ids.get(i)) == counter)
//                    if (files_Ids.contains(merged_Ids.get(i)) == false)
//                        files_Ids.add(merged_Ids.get(i));
//            }
//
//            // Create station query
//            if (files_Ids.size() > 0)
//            {
//                query = "SELECT * FROM file WHERE ";
//                for (int i=0; i<files_Ids.size(); i++)
//                {
//                    if (i==0)
//                        query = query + "id=" + files_Ids.get(i);
//                    else
//                        query = query + " OR id=" + files_Ids.get(i);
//                }
//                query = query + ";";
//
//                //Execute query
//                results = executeFilesQuery(results, query, c);
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES MARKER DATES COMBINED
//    * This method retuns a ArrayList with all the files with the given parameters.
//    *//*
//
//    public ArrayList getFilesMarkerDatesCombined(String request)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//        ArrayList<Integer> results_date_range = new ArrayList();
//        ArrayList<Integer> results_marker = new ArrayList();
//        RequestT2 request_holder = new RequestT2(request);
//        String query = "";
//        int counter = 0;
//
//        try{
//            c = NewConnection();
//
//            // Parse request
//            request_holder.parseRequest();
//            //request_holder.requestToString();
//
//            // Check date range
//            if (request_holder.date_range_list.size() == 2)
//            {
//                query = "SELECT id FROM file WHERE data_start_time>='" + request_holder.date_range_list.get(0) +
//                        "' AND data_stop_time<='" + request_holder.date_range_list.get(1) + "' ORDER BY id";
//            }
//            results_date_range.addAll(dbct1.executeIDQuery(query, c));
//
//
//            // Check marker
//            for(int index=0; index<request_holder.charCode_list.size(); index++)
//            {
//                // Get Stations ID's
//                ArrayList<Integer> stationList = new ArrayList();
//                query = "SELECT * FROM station WHERE marker LIKE '%" +
//                        request_holder.charCode_list.get(index) + "%' ORDER BY id";
//                stationList = dbct1.executeIDQuery(query, c);
//
//                ArrayList<Integer> files_generatedList = new ArrayList();
//
//                // Get File Generated ID's
//                for(int i=0; i<stationList.size(); i++)
//                {
//                    query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                    files_generatedList = dbct1.executeIDQuery(query, c);
//
//                    // Get Files
//                    for(int j=0; j<files_generatedList.size(); j++)
//                    {
//                        query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                        ArrayList<File> subresults = new ArrayList();
//                        subresults = executeFilesQuery(subresults, query, c);
//
//                        // Save results to main ArrayList
//                        for(int k=0; k<subresults.size(); k++)
//                        {
//                            results_marker.add(subresults.get(k).getId());
//                        }
//                    }
//                }
//            }
//
//            ArrayList<Integer> files_Ids = new ArrayList();
//            for (int i=0; i<results_date_range.size(); i++)
//            {
//                if (results_marker.contains(results_date_range.get(i)) == true)
//                    files_Ids.add(results_date_range.get(i));
//            }
//
//
//            // Create station query
//            if (files_Ids.size() > 0)
//            {
//                query = "SELECT * FROM file WHERE ";
//                for (int i=0; i<files_Ids.size(); i++)
//                {
//                    if (i==0)
//                        query = query + "id=" + files_Ids.get(i);
//                    else
//                        query = query + " OR id=" + files_Ids.get(i);
//                }
//                query = query + ";";
//
//                //Execute query
//                results = executeFilesQuery(results, query, c);
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES COORDINATES
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesCoordinates(float minLat, float maxLat,
//                                         float minLon, float maxLon)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Get Stations ID's
//            ArrayList<Integer> stationList = new ArrayList();
//            String query = "SELECT id FROM station WHERE id_location= ("
//                    + "SELECT id FROM Location WHERE id_coordinates= ("
//                    + "SELECT id FROM Coordinates WHERE lat>=" + minLat +""
//                    + " AND lat<=" + maxLat + " AND lon>=" + minLon +
//                    " AND lon<=" + maxLon + ")) ORDER BY id;";
//            stationList = dbct1.executeIDQuery(query, c);
//
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<stationList.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES COORDINATES DATE COMBINED
//    * This method retuns a ArrayList with all the files with the given parameter.
//    *//*
//
//    public ArrayList getFilesCoordinatesDateCombined(String request)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//        ArrayList<Integer> results_date_range = new ArrayList();
//        ArrayList<Integer> results_coordinates = new ArrayList();
//        ArrayList<Integer> results_coordinates_file_gen = new ArrayList();
//        ArrayList<Integer> results_coordinates_stations = new ArrayList();
//        RequestT2 request_holder = new RequestT2(request);
//        String query = "";
//        int counter = 0;
//
//        try{
//            c = NewConnection();
//
//            // Parse request
//            request_holder.parseRequest();
//            //request_holder.requestToString();
//
//            // Check date range
//            if (request_holder.date_range_list.size() == 2)
//            {
//                query = "SELECT id FROM file WHERE data_start_time>='" + request_holder.date_range_list.get(0) +
//                        "' AND data_stop_time<='" + request_holder.date_range_list.get(1) + "' ORDER BY id";
//            }
//            System.out.println(query);
//            if (query.isEmpty() == false)
//                results_date_range.addAll(dbct1.executeIDQuery(query, c));
//
//            // Check coordinates
//            switch (request_holder.coordinates_type)
//            {
//                // Handle rectangle queries
//                case "rectangle":
//                    ArrayList<Station> coord_rec = new ArrayList();
//                    coord_rec = dbct1.getStationCoordinates(request_holder.minLat, request_holder.maxLat,
//                            request_holder.minLon, request_holder.maxLon);
//                    for (int i=0; i<coord_rec.size(); i++)
//                        results_coordinates_stations.add(coord_rec.get(i).getId());
//                    break;
//
//                // Handle circle queries
//                case "circle":
//                    ArrayList<Station> coord_cir = new ArrayList();
//                    coord_cir = dbct1.getStationCoordinates(request_holder.lat, request_holder.lon,
//                            request_holder.radius);
//                    for (int i=0; i<coord_cir.size(); i++)
//                        results_coordinates_stations.add(coord_cir.get(i).getId());
//                    break;
//
//                // Handle polygon queries
//                case "polygon":
//                    ArrayList<Station> coord_pol = new ArrayList();
//                    coord_pol = dbct1.getStationCoordinates(request_holder.coordinates_list);
//                    for (int i=0; i<coord_pol.size(); i++)
//                        results_coordinates_stations.add(coord_pol.get(i).getId());
//                    break;
//            }
//
//            query = "SELECT * FROM file_generated WHERE id_station=";
//            for (int i=0; i<results_coordinates_stations.size(); i++)
//            {
//                if (i<results_coordinates_stations.size()-1)
//                    query = query + results_coordinates_stations.get(i) + " OR id_station=";
//                else
//                    query = query + results_coordinates_stations.get(i);
//            }
//            query = query + " ORDER BY id;";
//            results_coordinates_file_gen = dbct1.executeIDQuery(query, c);
//
//            query = "SELECT * FROM file WHERE id_file_generated=";
//            for (int i=0; i<results_coordinates_file_gen.size(); i++)
//            {
//                if (i<results_coordinates_file_gen.size()-1)
//                    query = query + results_coordinates_file_gen.get(i) + " OR id_file_generated=";
//                else
//                    query = query + results_coordinates_file_gen.get(i);
//            }
//            query = query + " ORDER BY id;";
//            results_coordinates = dbct1.executeIDQuery(query, c);
//
//            // Count valid parameters
//            if (results_date_range.size() > 0)
//                counter++;
//            if (request_holder.coordinates_type.contentEquals("rectangle") ||
//                    request_holder.coordinates_type.contentEquals("circle") ||
//                    request_holder.coordinates_type.contentEquals("polygon"))
//                counter++;
//
//
//            ArrayList<Integer> files_Ids = new ArrayList();
//            for (int i=0; i<results_date_range.size(); i++)
//            {
//                if (results_coordinates.contains(results_date_range.get(i)) == true)
//                    files_Ids.add(results_date_range.get(i));
//            }
//
//
//            // Create station query
//            if (files_Ids.size() > 0)
//            {
//                query = "SELECT * FROM file WHERE ";
//                for (int i=0; i<files_Ids.size(); i++)
//                {
//                    if (i==0)
//                        query = query + "id=" + files_Ids.get(i);
//                    else
//                        query = query + " OR id=" + files_Ids.get(i);
//                }
//                query = query + ";";
//
//                //Execute query
//                results = executeFilesQuery(results, query, c);
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES COUNTRY
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesCountry(String country)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        // ArrayList with Stations
//        ArrayList<Station> station_list = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Query to be executed
//            String query = "SELECT * FROM station WHERE id_location=("
//                    + "SELECT id FROM location WHERE id=("
//                    + "SELECT id FROM city WHERE id_state=("
//                    + "SELECT id FROM state WHERE id_country=("
//                    + "SELECT id FROM country WHERE name='" + country
//                    + "')))) ORDER BY id;";
//
//            // Execute query
//            station_list = dbct1.executeStationQuery( query, c);
//
//            // Check if the type exists
//            if(station_list.size() <= 0)
//                return results;
//
//            // ArrayList of Giles Generated
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<station_list.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES DATE RANGE
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesDateRange(String date_from, String date_to)
//    {
//        Connection c = null;
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Query to be executed
//            String query = "SELECT * FROM file WHERE data_start_time>='" + date_from +
//                    "' AND data_stop_time<='" + date_to + "' ORDER BY id";
//
//            //Execute query
//            results = executeFilesQuery(results, query, c);
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES PUBLISH DATE
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesPublishDate(String date_from, String date_to)
//    {
//        Connection c = null;
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Query to be executed
//            String query = "SELECT * FROM file WHERE published_date>='" + date_from +
//                    "' AND published_date<='" + date_to + "' ORDER BY id";
//
//            //Execute query
//            results = executeFilesQuery(results, query, c);
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES RADOME TYPE
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesRadomeType(String radomeType)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // ArrayList with Stations
//            ArrayList<Station> station_list = new ArrayList();
//
//            // Query to be executed
//            String query = "SELECT * FROM station WHERE id=("
//                    + "SELECT id_station FROM station_item WHERE id_item=("
//                    + "SELECT id_item FROM filter_radome WHERE id_radome_type=("
//                    + "SELECT id FROM radome_type WHERE name='" + radomeType
//                    + "'))) ORDER BY id;";
//
//            // Execute query
//            station_list = dbct1.executeStationQuery( query, c);
//
//            // Check if the type exists
//            if(station_list.size() <= 0)
//                return results;
//
//            // ArrayList of Giles Generated
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<station_list.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES RECEIVER TYPE
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesReceiverType(String receiverType)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // ArrayList with Stations
//            ArrayList<Station> station_list = new ArrayList();
//
//            // Query to be executed
//            String query = "SELECT * FROM station WHERE id=("
//                    + "SELECT id_station FROM station_item WHERE id_item=("
//                    + "SELECT id_item FROM filter_receiver WHERE id_receiver_type=("
//                    + "SELECT id FROM receiver_type WHERE name='" + receiverType
//                    + "'))) ORDER BY id;";
//
//            // Execute query
//            station_list = dbct1.executeStationQuery( query, c);
//
//            // Check if the type exists
//            if(station_list.size() <= 0)
//                return results;
//
//            // ArrayList of Giles Generated
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<station_list.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES STATE
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesState(String state)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        // ArrayList with Stations
//        ArrayList<Station> station_list = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Query to be executed
//            String query = "SELECT * FROM station WHERE id_location=("
//                    + "SELECT id FROM location WHERE id=("
//                    + "SELECT id FROM city WHERE id_state=("
//                    + "SELECT id FROM state WHERE name='" + state
//                    + "'))) ORDER BY id;";
//
//            // Execute query
//            station_list = dbct1.executeStationQuery( query, c);
//
//            // Check if the type exists
//            if(station_list.size() <= 0)
//                return results;
//
//            // ArrayList of Giles Generated
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<station_list.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + station_list.get(i).getId() + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES STATION DATES
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesStationDates(String date_from, String date_to)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Get Stations ID's
//            ArrayList<Integer> stationList = new ArrayList();
//            String query = "SELECT id FROM station WHERE date_from>='" + date_from
//                    + "' AND date_to<='" + date_to + "' ORDER BY id;";
//            stationList = dbct1.executeIDQuery(query, c);
//
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<stationList.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES STATION MARKER
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesStationMarker(String marker)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Get Stations ID's
//            ArrayList<Integer> stationList = new ArrayList();
//            String query = "SELECT * FROM station WHERE marker LIKE '%" + marker + "%' ORDER BY id";
//            stationList = dbct1.executeIDQuery(query, c);
//
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<stationList.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES STATION NAME
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesStationName(String name)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Get Stations ID's
//            ArrayList<Integer> stationList = new ArrayList();
//            String query = "SELECT * FROM station WHERE name LIKE '%" + name + "%' ORDER BY id";
//            stationList = dbct1.executeIDQuery(query, c);
//
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<stationList.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES STATION NETWORK
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesStationNetwork(String network)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Get Network ID
//            String query = "SELECT id FROM network WHERE name='" + network + "';";
//            ArrayList<Integer> networks_list = new ArrayList();
//            networks_list = dbct1.executeIDQuery(query, c);
//
//            // Check if the network exists
//            if(networks_list.size() <= 0)
//                return results;
//
//            // Get Stations ID's
//            ArrayList<Integer> stationList = new ArrayList();
//            query = "SELECT id_station FROM station_network WHERE id_network=" +
//                    networks_list.get(0) + ";";
//            stationList = dbct1.executeIDStationQuery(query, c);
//
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<stationList.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES STATION TYPE
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesStationType(String type)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Get Station Type ID
//            String query = "SELECT id FROM station_type WHERE name='" + type + "';";
//            ArrayList<Integer> type_list = new ArrayList();
//            type_list = dbct1.executeIDQuery(query, c);
//
//            // Check if the type exists
//            if(type_list.size() <= 0)
//                return results;
//
//            // Get Stations ID's
//            ArrayList<Integer> stationList = new ArrayList();
//            query = "SELECT * FROM station WHERE id_station_type=" +
//                    type_list.get(0) + " ORDER BY id;";
//            stationList = dbct1.executeIDQuery(query, c);
//
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<stationList.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_station=" + stationList.get(i) + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    */
///*
//    * GET FILES TYPE
//    * This method retuns a ArrayList with data from all files on the database.
//    *//*
//
//    public ArrayList getFilesType(String type)
//    {
//        Connection c = null;
//        DataBaseConnection dbct1 = new DataBaseConnection();
//        // ArrayList with the results of the query
//        ArrayList<File> results = new ArrayList();
//
//        try{
//            c = NewConnection();
//
//            // Get Stations ID's
//            ArrayList<Integer> typeList = new ArrayList();
//            String query = "SELECT * FROM file_type WHERE name = '" + type + "' ORDER BY id";
//            typeList = dbct1.executeIDQuery(query, c);
//
//            ArrayList<Integer> files_generatedList = new ArrayList();
//
//            // Get File Generated ID's
//            for(int i=0; i<typeList.size(); i++)
//            {
//                query = "SELECT * FROM file_generated WHERE id_file_type=" + typeList.get(i) + " ORDER BY id";
//                files_generatedList = dbct1.executeIDQuery(query, c);
//
//                // Get Files
//                for(int j=0; j<files_generatedList.size(); j++)
//                {
//                    query = "SELECT * FROM file WHERE id_file_generated=" + files_generatedList.get(j) + " ORDER BY id";
//                    ArrayList<File> subresults = new ArrayList();
//                    subresults = executeFilesQuery(subresults, query, c);
//
//                    // Save results to main ArrayList
//                    for(int k=0; k<subresults.size(); k++)
//                    {
//                        results.add(subresults.get(k));
//                    }
//                }
//            }
//
//        } catch (SocketException | ClassNotFoundException | SQLException ex) {
//            System.out.println("Error "+ex.getMessage());
//            Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//        } finally {
//            if (c != null) {
//                try {
//                    c.close();
//                } catch (SQLException ex) {
//                    Logger.getLogger(DataBaseT2Connection.class.getName()).log(Level.SEVERE, null, ex);
//                }
//            }
//        }
//
//        return results;
//    }
//
//    public String getURL(ArrayList<File> data)
//    {
//        String result = "";
//
//        for (int i=0; i<data.size(); i++)
//        {
//            result += data.get(i).getPath() + "\\" + data.get(i).getName() + "\n";
//        }
//
//        return result;
//    }
//*/
