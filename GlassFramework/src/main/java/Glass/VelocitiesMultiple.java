package Glass;

/*
 ***************************************************
 **                                               **
 **    Created by José Manteigueiro on 15/10/2020 **
 **                                               **
 **    jose.manteigueiro@c4g-pt.eu                **
 **                                               **
 **    Covilhã, Portugal                          **
 **                                               **
 ***************************************************
 */

import Configuration.SiteConfig;
import CustomClasses.Geobox;
import EposTables.Reference_Position_Velocities;
import EposTables.Station;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.zeroturnaround.zip.ZipUtil;
import org.zeroturnaround.zip.commons.FileUtils;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.core.Response;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.logging.Level;
import java.util.logging.Logger;

public class VelocitiesMultiple {
    @EJB
    SiteConfig siteConfig;

    public VelocitiesMultiple() {
        this.siteConfig = lookupSiteConfigBean();
    }
    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    /**
     * Creates velocities file for customized request (From Products Gateway)
     * @param jsonstring
     * @return string with zip file name
     * @throws ParseException
     * @throws SQLException
     * @throws IOException
     */
    public String VelocitiesMultiple(String jsonstring) throws ParseException, SQLException, IOException {

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(jsonstring);
        JSONObject jsonObject = (JSONObject) obj;
        DataBaseT4Connection _dbct4 = new DataBaseT4Connection();


        ArrayList<String> analysis_centers = new ArrayList<>();
        int _fileCounter = 0;

        // Output Variables
        boolean COORDINATES_SYSTEM_ENU = false;
        boolean COORDINATES_SYSTEM_XYZ = false;

        boolean FORMAT_JSON = false;
        boolean FORMAT_PBO = false;

        boolean GEOBOXES = false;

        // Mandatory Analysis Centers
        JSONArray msg;
        Iterator<String> iterator;
        if ((msg = (JSONArray) jsonObject.get("analysisCenters")) != null) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                analysis_centers.add(iterator.next());
            }
        }

        // Analysis Centers is empty? Then consider every analysis center
        if ( analysis_centers.isEmpty() ){
            iterator = _dbct4.getAttributesBasedOnKeyword("analysis_center-velocities").values().iterator();
            while (iterator.hasNext()) {
                analysis_centers.add(iterator.next());
            }
        }

        //  Check for formats
        //  Possible formats: json, pbo
        if ((msg = (JSONArray) jsonObject.get("formats")) != null) {
            iterator = msg.iterator();
            String format;
            while (iterator.hasNext()) {
                format = iterator.next();
                if (format.equals("json") || format.equals("JSON")) {
                    FORMAT_JSON = true;
                } else if (format.equals("pbo") || format.equals("PBO")) {
                    FORMAT_PBO = true;
                }
            }
        } else {
            // all True
            FORMAT_JSON = true;
            FORMAT_PBO = true;
        }

        //  If none is true, then enable all
        if(!FORMAT_JSON && !FORMAT_PBO){
            FORMAT_JSON = true;
            FORMAT_PBO = true;
        }

        // Check for coordinateSystems
        // Possible values: enu, xyz
        if ((msg = (JSONArray) jsonObject.get("coordinateSystems")) != null) {
            iterator = msg.iterator();
            String coordinate_systems;
            while (iterator.hasNext()) {
                coordinate_systems = iterator.next();
                if (coordinate_systems.equals("xyz") || coordinate_systems.equals("XYZ")) {
                    COORDINATES_SYSTEM_XYZ = true;
                } else if (coordinate_systems.equals("enu") || coordinate_systems.equals("ENU")) {
                    COORDINATES_SYSTEM_ENU = true;
                }
            }
        }

        //  If none is true, then enable all
        if(!COORDINATES_SYSTEM_ENU && !COORDINATES_SYSTEM_XYZ){
            COORDINATES_SYSTEM_ENU = true;
            COORDINATES_SYSTEM_XYZ = true;
        }

        // TODO: add more than 1 geobox
        // Check for geoboxes (multiple)
        Geobox gb = null;
        if ((jsonObject.get("geoBoxes")) != null) {
            double minLat, minLon, maxLat, maxLon;
            JSONObject jobj = (JSONObject) jsonObject.get("geoBoxes");
            try {
                minLat = Double.parseDouble(String.valueOf(jobj.get("minLat")));
                maxLat = Double.parseDouble(String.valueOf(jobj.get("maxLat")));
                minLon = Double.parseDouble(String.valueOf(jobj.get("minLon")));
                maxLon = Double.parseDouble(String.valueOf(jobj.get("maxLon")));

                // Geobox class
                gb = new Geobox(minLat, maxLat, minLon, maxLon);
                GEOBOXES = true;
            } catch(NullPointerException npe){
                // Geobox had no values


            } catch(NumberFormatException nfe){
                // Geobox values couldn't be formatted to doubles


            }

        }


        String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder builder = new StringBuilder();
        int count = 20;
        boolean folder_created = false;
        String generatedString;

        do {
            while (count-- != 0) {
                int character = (int) (Math.random() * ALPHA_NUMERIC_STRING.length());
                builder.append(ALPHA_NUMERIC_STRING.charAt(character));
            }

            generatedString = builder.toString();

            if (!new File("/opt/EPOS/temp/" + generatedString).exists()) {
                (new File("/opt/EPOS")).mkdir();
                (new File("/opt/EPOS/temp")).mkdir();
                (new File("/opt/EPOS/temp/" + generatedString)).mkdir();
                folder_created = true;
            }
        } while (!folder_created);

        ArrayList<Reference_Position_Velocities> results;
        DataBaseConnection _dbc = null;
        Station _station = null;
        if (FORMAT_PBO) {
            _dbc = new DataBaseConnection();
        }

        Parsers parsers = new Parsers();
        ArrayList<String> markers;
        ArrayList<Reference_Position_Velocities> _rpvs;
        ArrayList<Reference_Position_Velocities> _rpvs_enu;

        JSONArray _parsedResults;

        System.out.println("Creating velocities zip file");
        for (String _analysis_center : analysis_centers) {

            // Get list of stations that have velocities for this analysis center
            markers = _dbct4.getStationMarkersOfStationsWithVelocitiesForAnalysisCenter(_analysis_center);

            // If GEOBOXES is true, restrict the markers
            if(GEOBOXES && gb != null)
                markers = _dbct4.filterStationMarkersByGeobox(markers, gb);

            for (String marker : markers) {

                // Create the folders
                (new File("/opt/EPOS/temp/" + generatedString + "/" + _analysis_center)).mkdir();
                (new File("/opt/EPOS/temp/" + generatedString + "/" + _analysis_center + "/" + marker)).mkdir();

                _rpvs = null;

                if (FORMAT_JSON) {


                    // If XYZ is selected as output coordinates system
                    if (COORDINATES_SYSTEM_XYZ) {

                        // Get RPVs
                        _rpvs = _dbct4.getVelocitiesXYZ(marker, _analysis_center, "");

                        // If JSON is selected as output format
                        if (!_rpvs.isEmpty()) {
                            _parsedResults = parsers.getVelocitiesXYZJSON(_rpvs);

                            // Write the file
                            try (Writer writer =
                                         new BufferedWriter(
                                                 new OutputStreamWriter(
                                                         new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + _analysis_center + "/" + marker + "/" + _analysis_center + "-" + marker + "-" + "XYZ" + "-" + "JSON" + "." + "json"), StandardCharsets.UTF_8)
                                         )
                            ) {
                                writer.write(String.valueOf(_parsedResults));
                                _fileCounter++;
                            }
                        }
                    }

                    // If ENU is selected as output coordinates system
                    if (COORDINATES_SYSTEM_ENU) {

                        // Get RPVs
                        _rpvs_enu = _dbct4.getVelocitiesENU(marker, _analysis_center, "EURA");

                        // If JSON is selected as output format
                        if (!_rpvs_enu.isEmpty()) {
                            _parsedResults = parsers.getVelocitiesENUJSON(_rpvs_enu);

                            // Write the file
                            try (Writer writer =
                                         new BufferedWriter(
                                                 new OutputStreamWriter(
                                                         new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + _analysis_center + "/" + marker + "/" + _analysis_center + "-" + marker + "-" + "ENU" + "-" + "JSON" + "." + "json"), StandardCharsets.UTF_8)
                                         )
                            ) {
                                writer.write(String.valueOf(_parsedResults));
                                _fileCounter++;
                            }
                        }
                    }
                }

                if (FORMAT_PBO) {
                    if (_rpvs == null) {
                        _rpvs = _dbct4.getVelocitiesXYZ(marker, _analysis_center, "");
                    }

                    if (!_rpvs.isEmpty()) {
                        _station = _dbc.getStationsShortByMarker(marker);

                        // Write the file
                        try (Writer writer =
                                     new BufferedWriter(
                                             new OutputStreamWriter(
                                                     new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + _analysis_center + "/" + marker + "/" + _analysis_center + "-" + marker + "-PBO.vel"), StandardCharsets.UTF_8)
                                     )
                        ) {
                            writer.write(PBOHandlerVelocities.pbovelocities(_rpvs, _station, _analysis_center));
                            _fileCounter++;
                        }

                    }

                }
            }
        }

        File f = new File("/opt/EPOS/temp/" + generatedString);
        if (f.isDirectory() && f.exists()) {
            if (f.list().length == 0) {
                if (f.delete()) {
                    System.out.println("    > no data has been collected");
                } else {
                    System.out.println("    > no data has been collected and an error occurred");
                }
                return null;
            } else {
                System.out.println("    > data has been collected");
                ZipUtil.pack(f, new File("/opt/EPOS/temp/" + generatedString + ".zip"));
                FileUtils.deleteDirectory(new File("/opt/EPOS/temp/" + generatedString));
                return generatedString + ".zip";
            }
        }

        return null;

    }
}



