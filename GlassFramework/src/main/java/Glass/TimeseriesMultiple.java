package Glass;

/*
 ***************************************************
 **                                               **
 **      Created by Andre Rodrigues on 12/14/17   **
 **                                               **
 **      andre.rodrigues@ubi.pt                   **
 **                                               **
 **      Covilha, Portugal                        **
 **                                               **
 ***************************************************
 */

import Configuration.SiteConfig;
import EposTables.Estimated_coordinates;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.zeroturnaround.zip.ZipUtil;
import org.zeroturnaround.zip.commons.FileUtils;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class TimeseriesMultiple {
    @EJB
    SiteConfig siteConfig;

    public TimeseriesMultiple() {
        this.siteConfig = lookupSiteConfigBean();
    }
    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    /**
     * Creates timeseries for customized request (From Products Gateway)
     * @param jsonstring
     * @return string with zip file name
     * @throws ParseException
     * @throws SQLException
     * @throws IOException
     */
    public String TimeSeriesMultiple(String jsonstring) throws ParseException, SQLException, IOException {

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(jsonstring);
        JSONObject jsonObject = (JSONObject) obj;

        DataBaseT4Connection dc = new DataBaseT4Connection();

        ArrayList<String> networks = new ArrayList<>();
        ArrayList<String> analysis_centers = new ArrayList<>();
        ArrayList<String> reference_frames = new ArrayList<>();
        ArrayList<String> otl_models = new ArrayList<>();
        ArrayList<String> antenna_models = new ArrayList<>();
        ArrayList<String> sampling_periods = new ArrayList<>();
        ArrayList<String> formats = new ArrayList<>();
        String remove_outliers = null;

        JSONArray msg = (JSONArray) jsonObject.get("networks");
        Iterator<String> iterator = msg.iterator();
        while (iterator.hasNext()) {
            networks.add(iterator.next());
        }

        if ((msg = (JSONArray) jsonObject.get("analysisCenters")) != null) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                analysis_centers.add(iterator.next());
            }
        } else {
            List<String> valueList = new ArrayList<>(dc.getAttributesBasedOnKeyword("analysis_center").values());
            analysis_centers.addAll(valueList);
        }

        if ((msg = (JSONArray) jsonObject.get("referenceFrames")) != null) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                reference_frames.add(iterator.next());
            }
        } else {
            List<String> valueList = new ArrayList<>(dc.getAttributesBasedOnKeyword("reference_frame").values());
            reference_frames.addAll(valueList);
        }

        if ((msg = (JSONArray) jsonObject.get("otlModels")) != null ) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                otl_models.add(iterator.next());
            }
        } else {
            List<String> valueList = new ArrayList<>(dc.getAttributesBasedOnKeyword("otl_model").values());
            otl_models.addAll(valueList);
        }

        if ((msg = (JSONArray) jsonObject.get("antennaModels")) != null ) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                antenna_models.add(iterator.next());
            }
        } else {
            List<String> valueList = new ArrayList<>(dc.getAttributesBasedOnKeyword("antenna_model").values());
            antenna_models.addAll(valueList);
            if (antenna_models.isEmpty()) antenna_models = null;
        }

        if ((msg = (JSONArray) jsonObject.get("samplingPeriods")) != null) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                sampling_periods.add(iterator.next());
            }
        } else {
            List<String> valueList = new ArrayList<>(dc.getAttributesBasedOnKeyword("sampling_period").values());
            sampling_periods.addAll(valueList);
        }

        if ((msg = (JSONArray) jsonObject.get("formats")) != null) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                formats.add(iterator.next());
            }
        } else {
            formats.add("xml");
            formats.add("json");
            formats.add("pbo");
            formats.add("midas");
            formats.add("hector");
        }


        if (jsonObject.get("outlierSelection") == null) {
            remove_outliers = "true";
        } else {
            if (jsonObject.get("outlierSelection").equals("on")) {
                remove_outliers = "false";
            } else {
                remove_outliers = "true";
            }
        }


        ArrayList<String> stations = new ArrayList<>();
        DataBaseConnection dbc = new DataBaseConnection();

        for (String network : networks) {
            ArrayList<String> stationsNameByNetwork = dbc.getStationsNameByNetwork(network);

            for (String stationNameByNetwork : stationsNameByNetwork) {
                if (!stations.contains(stationNameByNetwork))
                    stations.add(stationNameByNetwork);
            }
        }

        String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder builder = new StringBuilder();
        int count = 20;
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }

        String generatedString = builder.toString();

        if (!new java.io.File("/opt/EPOS/temp/"+generatedString).exists()) {
            (new java.io.File("/opt/EPOS")).mkdir();
            (new java.io.File("/opt/EPOS/temp")).mkdir();
            (new java.io.File("/opt/EPOS/temp/"+generatedString)).mkdir();

        }

        ArrayList<Estimated_coordinates> res;

        Parsers parsers = new Parsers();

        String cut_of_angle = null;
        String epoch_start = null;
        String epoch_end = null;
        String apply_offsets = null;
        // TODO: handle version
        String version = "";

        System.out.println("Creating zip file");
        for (String station : stations) {

            System.out.println("  > " + station);

            for (String analysis_center : analysis_centers) {

                System.out.println("   > " + analysis_center);

                for (String reference_frame : reference_frames) {

                    System.out.println("    > " + reference_frame);

                    for (String otl_model : otl_models) {

                        System.out.println("     > " + otl_model);

                        for (String antenna_model : antenna_models) {

                            System.out.println("      > " + antenna_model);

                            for (String sampling_period : sampling_periods) {

                                System.out.println("       > " + sampling_period);

                                res = dc.getTimeseriesXYZBasedOnParameters(
                                                                            station,
                                                                            analysis_center,
                                                                            sampling_period,
                                                                            "xyz",
                                                                            reference_frame,
                                                                            otl_model,
                                                                            antenna_model,
                                                                            cut_of_angle,
                                                                            epoch_start,
                                                                            epoch_end,
                                                                            remove_outliers,
                                                                            apply_offsets,
                                                                            "timeseries",
                                                                            version);

                                if (res.isEmpty()) {
                                    System.out.println("        > no timeseries available for: " + station + ", " + analysis_center + ", " + sampling_period + ", " + "xyz" + ", " + reference_frame + ", " + otl_model + ", " + antenna_model + ", " +  cut_of_angle + ", " + epoch_start + ", " + epoch_end + ", " + remove_outliers + ", " + apply_offsets + ", " + "timeseries");
                                    continue;
                                }
                                System.out.println("        > timeseries are available for: " + station + ", " + analysis_center + ", " + sampling_period + ", " + "xyz" + ", " + reference_frame + ", " + otl_model + ", " + antenna_model + ", " +  cut_of_angle + ", " + epoch_start + ", " + epoch_end + ", " + remove_outliers + ", " + apply_offsets + ", " + "timeseries");

                                (new java.io.File("/opt/EPOS/temp/"+generatedString+"/"+station)).mkdir();
                                (new java.io.File("/opt/EPOS/temp/"+generatedString+"/"+station+"/"+analysis_center)).mkdir();


                                epoch_start = res.get(0).getEpoch();
                                epoch_end = res.get(res.size()-1).getEpoch();

                                //XML
                                if (formats.contains("xml")) {
                                    try (Writer writer =
                                                 new BufferedWriter(
                                                    new OutputStreamWriter(
                                                        new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "xml"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(parsers.getFullXMLTimeSeries(res));
                                    }
                                }

                                //JSON
                                if (formats.contains("json")) {
                                    try (Writer writer =
                                                 new BufferedWriter(
                                                         new OutputStreamWriter(
                                                            new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "json"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(String.valueOf(parsers.getTimeSeriesByFormatJSON(res)));
                                    }
                                }

                                //PBO
                                if (formats.contains("pbo")) {
                                    try (Writer writer =
                                                 new BufferedWriter(
                                                         new OutputStreamWriter(
                                                            new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "pbo"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(PBOHandler.PBOEstimatedCoordinates(res, station, sampling_period));
                                    }
                                }

                                //MIDAS
                                if (formats.contains("midas")) {
                                    try (Writer writer =
                                                 new BufferedWriter(
                                                         new OutputStreamWriter(
                                                            new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "midas"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(PBOHandler.CreateMidasFile(station, PBOHandler.PBOEstimatedCoordinates2MIDAS(res, station, sampling_period)));
                                    }
                                }

                                //HECTOR
                                if (formats.contains("hector")) {

                                    try (Writer writer =
                                                 new BufferedWriter(
                                                         new OutputStreamWriter(
                                                            new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "hector"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(PBOHandler.CreateHectorFile(sampling_period, PBOHandler.PBOEstimatedCoordinates2MIDAS(res, station, sampling_period)));
                                    }
                                }


                            }
                        }
                    }
                }
            }
        }


        File f = new File("/opt/EPOS/temp/"+generatedString);
        if (f.isDirectory() && f.exists()) {
            if (f.list().length == 0) {
                if (f.delete()) {
                    System.out.println("    > no data has been collected");
                } else {
                    System.out.println("    > no data has been collected and an error occurred");
                }
                return null;
            } else {
                System.out.println("    > data has been collected");
                ZipUtil.pack(f, new File("/opt/EPOS/temp/"+generatedString+".zip"));
                FileUtils.deleteDirectory(new File("/opt/EPOS/temp/"+generatedString));
                return generatedString+".zip";
            }
        } else {
            return null;
        }

    }






    /**
     * Creates timeseries for customized request (From Products Gateway) on map selection
     *
     * @author Andre Rodrigues
     * @param jsonstring
     * @return string with zip file name
     * @throws ParseException
     * @throws SQLException
     * @throws IOException
     */
    public String TimeSeriesMultipleMap(String jsonstring) throws ParseException, SQLException, IOException {

        JSONParser parser = new JSONParser();
        Object obj = parser.parse(jsonstring);
        JSONObject jsonObject = (JSONObject) obj;

        DataBaseT4Connection dc = new DataBaseT4Connection();

        ArrayList<String> stations = new ArrayList<>();
        ArrayList<String> networks = new ArrayList<>();
        ArrayList<String> analysis_centers = new ArrayList<>();
        ArrayList<String> reference_frames = new ArrayList<>();
        ArrayList<String> otl_models = new ArrayList<>();
        ArrayList<String> antenna_models = new ArrayList<>();
        ArrayList<String> sampling_periods = new ArrayList<>();
        ArrayList<String> formats = new ArrayList<>();
        String remove_outliers = null;


        JSONArray msg = (JSONArray) jsonObject.get("stations");
        Iterator<String> iterator = msg.iterator();
        while (iterator.hasNext()) {
            stations.add(iterator.next());
        }

        msg = (JSONArray) jsonObject.get("networks");
        iterator = msg.iterator();
        while (iterator.hasNext()) {
            networks.add(iterator.next());
        }

        msg = (JSONArray) jsonObject.get("analysisCenters");
        iterator = msg.iterator();
        while (iterator.hasNext()) {
            analysis_centers.add(iterator.next());
        }

        if ((msg = (JSONArray) jsonObject.get("referenceFrames")) != null) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                reference_frames.add(iterator.next());
            }
        } else {
            List<String> valueList = new ArrayList<>(dc.getAttributesBasedOnKeyword("reference_frame").values());
            reference_frames.addAll(valueList);
        }

        if ((msg = (JSONArray) jsonObject.get("otlModels")) != null ) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                otl_models.add(iterator.next());
            }
        } else {
            List<String> valueList = new ArrayList<>(dc.getAttributesBasedOnKeyword("otl_model").values());
            otl_models.addAll(valueList);
        }

        if ((msg = (JSONArray) jsonObject.get("antennaModels")) != null ) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                antenna_models.add(iterator.next());
            }
        } else {
            List<String> valueList = new ArrayList<>(dc.getAttributesBasedOnKeyword("antenna_model").values());
            antenna_models.addAll(valueList);
            if (antenna_models.isEmpty()) antenna_models = null;
        }

        if ((msg = (JSONArray) jsonObject.get("samplingPeriods")) != null) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                sampling_periods.add(iterator.next());
            }
        } else {
            List<String> valueList = new ArrayList<>(dc.getAttributesBasedOnKeyword("sampling_period").values());
            sampling_periods.addAll(valueList);
        }

        if ((msg = (JSONArray) jsonObject.get("formats")) != null) {
            iterator = msg.iterator();
            while (iterator.hasNext()) {
                formats.add(iterator.next());
            }
        } else {
            formats.add("xml");
            formats.add("json");
            formats.add("pbo");
            formats.add("midas");
            formats.add("hector");
        }


        if (jsonObject.get("outlierSelection") == null) {
            remove_outliers = "true";
        } else {
            if (jsonObject.get("outlierSelection").equals("on")) {
                remove_outliers = "false";
            } else {
                remove_outliers = "true";
            }
        }


//        ArrayList<String> stations = new ArrayList<>();
//        DataBaseConnection dbc = new DataBaseConnection();
//
//        for (String network : networks) {
//            ArrayList<String> stationsNameByNetwork = dbc.getStationsNameByNetwork(network);
//
//            for (String stationNameByNetwork : stationsNameByNetwork) {
//                if (!stations.contains(stationNameByNetwork)) {
//                    if(stationsMap.contains(stationNameByNetwork)){
//                        stations.add(stationNameByNetwork);
//                    }
//
//                }
//
//            }
//        }

        String ALPHA_NUMERIC_STRING = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
        StringBuilder builder = new StringBuilder();
        int count = 20;
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }

        String generatedString = builder.toString();

        if (!new java.io.File("/opt/EPOS/temp/"+generatedString).exists()) {
            (new java.io.File("/opt/EPOS")).mkdir();
            (new java.io.File("/opt/EPOS/temp")).mkdir();
            (new java.io.File("/opt/EPOS/temp/"+generatedString)).mkdir();

        }

        ArrayList<Estimated_coordinates> res;

        Parsers parsers = new Parsers();

        String cut_of_angle = null;
        String epoch_start = null;
        String epoch_end = null;
        String apply_offsets = null;
        // TODO: handle version
        String version = "";

        System.out.println("Creating zip file");
        for (String station : stations) {

            System.out.println("  > " + station);

            for (String analysis_center : analysis_centers) {

                System.out.println("   > " + analysis_center);

                for (String reference_frame : reference_frames) {

                    System.out.println("    > " + reference_frame);

                    for (String otl_model : otl_models) {

                        System.out.println("     > " + otl_model);

                        for (String antenna_model : antenna_models) {

                            System.out.println("      > " + antenna_model);

                            for (String sampling_period : sampling_periods) {

                                System.out.println("       > " + sampling_period);

                                res = dc.getTimeseriesXYZBasedOnParameters(
                                        station,
                                        analysis_center,
                                        sampling_period,
                                        "xyz",
                                        reference_frame,
                                        otl_model,
                                        antenna_model,
                                        cut_of_angle,
                                        epoch_start,
                                        epoch_end,
                                        remove_outliers,
                                        apply_offsets,
                                        "timeseries",
                                        version);

                                if (res.isEmpty()) {
                                    System.out.println("        > no timeseries available for: " + station + ", " + analysis_center + ", " + sampling_period + ", " + "xyz" + ", " + reference_frame + ", " + otl_model + ", " + antenna_model + ", " +  cut_of_angle + ", " + epoch_start + ", " + epoch_end + ", " + remove_outliers + ", " + apply_offsets + ", " + "timeseries");
                                    continue;
                                }
                                System.out.println("        > timeseries are available for: " + station + ", " + analysis_center + ", " + sampling_period + ", " + "xyz" + ", " + reference_frame + ", " + otl_model + ", " + antenna_model + ", " +  cut_of_angle + ", " + epoch_start + ", " + epoch_end + ", " + remove_outliers + ", " + apply_offsets + ", " + "timeseries");

                                (new java.io.File("/opt/EPOS/temp/"+generatedString+"/"+station)).mkdir();
                                (new java.io.File("/opt/EPOS/temp/"+generatedString+"/"+station+"/"+analysis_center)).mkdir();


                                epoch_start = res.get(0).getEpoch();
                                epoch_end = res.get(res.size()-1).getEpoch();

                                //XML
                                if (formats.contains("xml")) {
                                    try (Writer writer =
                                                 new BufferedWriter(
                                                         new OutputStreamWriter(
                                                                 new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "xml"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(parsers.getFullXMLTimeSeries(res));
                                    }
                                }

                                //JSON
                                if (formats.contains("json")) {
                                    try (Writer writer =
                                                 new BufferedWriter(
                                                         new OutputStreamWriter(
                                                                 new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "json"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(String.valueOf(parsers.getTimeSeriesByFormatJSON(res)));
                                    }
                                }

                                //PBO
                                if (formats.contains("pbo")) {
                                    try (Writer writer =
                                                 new BufferedWriter(
                                                         new OutputStreamWriter(
                                                                 new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "pbo"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(PBOHandler.PBOEstimatedCoordinates(res, station, sampling_period));
                                    }
                                }

                                //MIDAS
                                if (formats.contains("midas")) {
                                    try (Writer writer =
                                                 new BufferedWriter(
                                                         new OutputStreamWriter(
                                                                 new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "midas"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(PBOHandler.CreateMidasFile(station, PBOHandler.PBOEstimatedCoordinates2MIDAS(res, station, sampling_period)));
                                    }
                                }

                                //HECTOR
                                if (formats.contains("hector")) {

                                    try (Writer writer =
                                                 new BufferedWriter(
                                                         new OutputStreamWriter(
                                                                 new FileOutputStream("/opt/EPOS/temp/" + generatedString + "/" + station + "/" + analysis_center + "/" + sampling_period + "-" + reference_frame + "-" + otl_model + "-" + antenna_model + "-" + cut_of_angle + "-" + epoch_start + "-" + epoch_end + "-" + remove_outliers + "-" + apply_offsets + "." + "hector"), StandardCharsets.UTF_8)
                                                 )
                                    ) {
                                        writer.write(PBOHandler.CreateHectorFile(sampling_period, PBOHandler.PBOEstimatedCoordinates2MIDAS(res, station, sampling_period)));
                                    }
                                }


                            }
                        }
                    }
                }
            }
        }


        File f = new File("/opt/EPOS/temp/"+generatedString);
        if (f.isDirectory() && f.exists()) {
            if (f.list().length == 0) {
                if (f.delete()) {
                    System.out.println("    > no data has been collected");
                } else {
                    System.out.println("    > no data has been collected and an error occurred");
                }
                return null;
            } else {
                System.out.println("    > data has been collected");
                ZipUtil.pack(f, new File("/opt/EPOS/temp/"+generatedString+".zip"));
                FileUtils.deleteDirectory(new File("/opt/EPOS/temp/"+generatedString));
                return generatedString+".zip";
            }
        } else {
            return null;
        }

    }

}



