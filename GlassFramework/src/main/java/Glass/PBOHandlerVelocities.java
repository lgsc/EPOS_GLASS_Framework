package Glass;

import EposTables.Reference_Position_Velocities;
import EposTables.Station;
import org.apache.commons.math3.linear.MatrixUtils;
import org.apache.commons.math3.linear.RealMatrix;

import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;

/**
 * Created by arodrigues on 9/12/17.
 */
public class PBOHandlerVelocities {

    /**
     * Writes the pbo header.
     * Based on Machiel's docs.
     *
     * @return            String with the PBO header
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */

    public static String write_PBO_header(String fname, String reference_frame, String release_date) {
        //---------------------------------------------------------------
        // Save the velocities in PBO format
        // param fname is file with the reference positions and velocities
        // param reference_frame is ITRF2014, etc.
        // param release_date is the date when the fname file was created.

        //--- Results are saved in file pointed to by fp_out

        StringBuilder str = new StringBuilder();
        str.append("PBO Velocity file from " + fname);
        str.append(" Reference Frame: " + reference_frame);
        str.append("\nFormat Version: 1.1.0\n");

        //--- Remove dashes and double colons.
        String pbo_date = release_date.replace("-", "");
        pbo_date = pbo_date.replace(":", "");
        pbo_date = pbo_date.replace(" ", "");
        str.append("Release Date: "+pbo_date+"\n");
        str.append("Start Field Description\n");
        str.append("Dot# 4-character identifier for a given station\n");
        str.append("Name 16-character station name\n");
        str.append("Ref_epoch Date and time at which the station position is as given in ref_XYZ and ref_NEU. Format is YYYYMMDDhhmmss.\n");
        str.append("Ref_jday Reference epoch, represented as Modified Julian Day number (MJD)\n");
        str.append("Ref_X Reference X coordinate at Ref_epoch, meters\n");
        str.append("Ref_Y Reference Y coordinate at Ref_epoch, meters\n");
        str.append("Ref_Z Reference Z coordinate at Ref_epoch, meters\n");
        str.append("Ref_Nlat Reference North latitude WGS-84 ellipsoid, decimal degrees\n");
        str.append("Ref_Elong Reference East Longitude WGS-84 ellipsoid, decimal degrees\n");
        str.append("Ref_Up Reference Height WGS-84 ellipsoid, meters\n");
        str.append("dX/dt X component of station velocity, meters/yr\n");
        str.append("dY/dt Y component of station velocity, meters/yr\n");
        str.append("dZ/dt Z component of station velocity, meters/yr\n");
        str.append("SXd Standard deviation of X velocity, meters/yr\n");
        str.append("SYd Standard deviation of Y velocity, meters/yr\n");
        str.append("SZd Standard deviation of Z velocity, meters/yr\n");
        str.append("Rxy Correlation of X and Y velocity\n");
        str.append("Rxz Correlation of X and Z velocity\n");
        str.append("Ryz Correlation of Y and Z velocity\n");
        str.append("dN/dt North component of station velocity, meters/yr\n");
        str.append("dE/dt East component of station velocity, meters/yr\n");
        str.append("dU/dt Vertical component of station velocity, meters/yr\n");
        str.append("SNd Standard deviation of North velocity, meters/yr\n");
        str.append("SEd Standard deviation of East velocity, meters/yr\n");
        str.append("SUd Standard deviation of vertical velocity, meters/yr\n");
        str.append("Rne Correlation of North and East velocity\n");
        str.append("Rnu Correlation of North and vertical velocity\n");
        str.append("Reu Correlation of East and vertical velocity\n");
        str.append("first_epoch Epoch of first data used to derive the station velocity, in the same format as ref_epoch.\n");
        str.append("last_epoch Epoch of last data used to derive the station velocity, in the same format as ref_epoch.\n");
        str.append("End Field Description\n");
        str.append("Marker      Name          Ref_epoch    Ref_jday       Ref_X              Ref_Y            Ref_Z           Ref_Nlat       Ref_Elong        Ref_Up...    dX/dt      dY/dt    dZ/dt      SXd       SYd       SZd      Rxy     Rxz     Rzy     dN/dt     dE/dt     dU/dt      SNd       SEd       SUd      Rne     Rnu     Reu     first_epoch     last_epoch\n");

        return str.toString();
    }


    /**
     * Generates the pbo content
     * Based on Machiel's docs.
     *
     * @return            String with PBO content
     * @author            Andre Rodrigues
     * @author            Machiel Bos
     *
     */

    public static String pbovelocities(ArrayList<Reference_Position_Velocities> rpv, Station station, String analysis_centre){
        //--- Some information should be given here (manually)

        // TODO: Mudar para stringbuilder
        String fname = "";
        double ref_epoch_modifiedJulianDay;
        String pbo="";
        String ref_epoch;
        Date date;

        double rad = Math.PI /180.0;
        for (int i=0; i< rpv.size(); i++) {

            if(i==0){
                try {
                    fname = rpv.get(0).getProduct_files().getUrl();
                }catch(Exception e){
                    fname = "";
                }
            }


            // --- The X, Y and Z reference_positions
            double X = rpv.get(i).getReference_position_x();
            double Y = rpv.get(i).getReference_position_y();
            double Z = rpv.get(i).getReference_position_z();

            //--- velx, vely and velz
            double V_X = rpv.get(i).getVelx();
            double V_Y = rpv.get(i).getVely();
            double V_Z = rpv.get(i).getVelz();

            double varXX = Math.pow(rpv.get(i).getVelx_sigma(), 2);
            double varXY = rpv.get(i).getVel_rho_xy() * (rpv.get(i).getVelx_sigma() * rpv.get(i).getVely_sigma());
            double varYY = Math.pow(rpv.get(i).getVely_sigma(), 2);
            double varXZ = rpv.get(i).getVel_rho_xz() * (rpv.get(i).getVelx_sigma() * rpv.get(i).getVelz_sigma());
            double varYZ = rpv.get(i).getVel_rho_yz() * (rpv.get(i).getVely_sigma() * rpv.get(i).getVelz_sigma());
            double varZZ = Math.pow(rpv.get(i).getVelz_sigma(), 2);

            //--- The uncertainties in the positions
            double pos_x_sigma = Math.sqrt(varXX);
            double pos_y_sigma = Math.sqrt(varYY);
            double pos_z_sigma = Math.sqrt(varZZ);

            //--- The cross-correlations in the positions
            double pos_rho_xy = rpv.get(i).getReference_position_rho_xy();
            double pos_rho_xz = rpv.get(i).getReference_position_rho_xz();
            double pos_rho_yz = rpv.get(i).getReference_position_rho_yz();

            //---- The uncertainties in the velocities
            double vel_x_sigma = rpv.get(i).getVelx_sigma();
            double vel_y_sigma = rpv.get(i).getVely_sigma();
            double vel_z_sigma = rpv.get(i).getVelz_sigma();

            //--- The cross-correlations in the velocities
            double vel_rho_xy = rpv.get(i).getVel_rho_xy();
            double vel_rho_xz = rpv.get(i).getVel_rho_xz();
            double vel_rho_yz = rpv.get(i).getVel_rho_yz();


            //--- Convert Cartesian velocities into local East, North and Up
            double[] LBH = PBOHandler.convertXYZ2LBH(X, Y, Z);

            double lon = LBH[0] / rad;  // longitude in degrees
            double lat = LBH[1] / rad;  // latitude in degrees
            double cl = Math.cos(LBH[0]);
            double sl = Math.sin(LBH[0]);
            double ct = Math.cos(LBH[1]);
            double st = Math.sin(LBH[1]);
            double h = LBH[2]; // height above ellipsoid in metres

            //--- Transform covariance matrix
            RealMatrix R = MatrixUtils.createRealMatrix(new double[][]{
                    {-sl, cl, 0},
                    {-st * cl, -st * sl, ct},
                    {ct * cl, ct * sl, st}
            });


            //--- Construct the full XYZ covariance matrix
            RealMatrix VCV_XYZ = MatrixUtils.createRealMatrix(new double[][]{
                    {varXX, varXY, varXZ},
                    {varXY, varYY, varYZ},
                    {varXZ, varYZ, varZZ}
            });

            //--- rotate and convert metres to millimetres
            double V_E = -sl * V_X + cl * V_Y;
            double V_N = -cl * st * V_X - st * sl * V_Y + ct * V_Z;
            double V_U = cl * ct * V_X + ct * sl * V_Y + st * V_Z;

            //--- Compute the covariance matrix in East, North and Up frame
            RealMatrix VCV_ENU = R.multiply(VCV_XYZ).multiply(R.transpose());

            //--- Diagonal contains Se, Sn and Su
            double SNd = Math.sqrt(VCV_ENU.getEntry(1, 1));
            double SEd = Math.sqrt(VCV_ENU.getEntry(0, 0));
            double SUd = Math.sqrt(VCV_ENU.getEntry(2, 2));

            //--- Compute cross-correlations
            double Rne = VCV_ENU.getEntry(0, 1) / (SNd * SEd);
            double Rnu = VCV_ENU.getEntry(1, 2) / (SNd * SUd);
            double Reu = VCV_ENU.getEntry(0, 2) / (SUd * SEd);


            HashMap<String, String> hm = new HashMap<String, String>();

            DataBaseT4Connection dt4 = new DataBaseT4Connection();


            try {
                hm = dt4.getStartEndDates(station.getMarker(), analysis_centre);
            } catch (SQLException e) {
                e.printStackTrace();
            }


            String date1 = hm.get("start");
            String date2 = hm.get("end");

            String date1parsedA = date1.replace("-","");
            String date2parsedA = date2.replace("-","");

            String date1parsedB = date1parsedA.replace(":","");
            String date2parsedB = date2parsedA.replace(":","");

            String date1parsedC = date1parsedB.replace(" ","");
            String date2parsedC = date2parsedB.replace(" ","");

            pbo = write_PBO_header(fname, rpv.get(0).getReference_frame().getName(), rpv.get(0).getMethod_identification().getCreation_date());

            // Try to obtain ref_epoch from reference_frames
            // If it fails, obtain it from RPV
            // Won't fail, but in case it does, gets assigned to null
            try {
                ref_epoch = rpv.get(0).getReference_frame().getEpoch();
            }catch (Exception e) {
                try{
                    ref_epoch = rpv.get(0).getRef_epoch();
                }catch (Exception e2) {
                    ref_epoch = null;
                }
            }


            if(ref_epoch == null){
                ref_epoch = "1970-01-01";
                ref_epoch_modifiedJulianDay = 40587.0; // Default 1970-01-01
            }else{
                try {
                    date = new SimpleDateFormat("yyyy-MM-dd").parse(ref_epoch);
                    ref_epoch_modifiedJulianDay = PBOHandler.calculateMJD(date);
                } catch (ParseException e) {
                    ref_epoch_modifiedJulianDay = 40587.0; // Default 1970-01-01
                }
            }

            //--- full name
            String fullname = station.getName();

            pbo=pbo+station.getMarker()+"  "+ String.format("%16s",fullname) + "  "+ ref_epoch +"  "+ref_epoch_modifiedJulianDay+"  ";
            pbo=pbo+String.format("%15.5f", X)+"  "+String.format("%15.5f", Y)+"  "+String.format("%15.5f",Z)+"  ";
            pbo=pbo+String.format("%15.10f", lat)+"  "+String.format("%15.10f", lon)+"  "+String.format("%13.10f", h)+"  ";
            pbo=pbo+String.format("%8.5f", V_X)+"  "+String.format("%8.5f", V_Y)+"  "+String.format("%8.5f", V_Z)+"  ";
            pbo=pbo+String.format("%8.5f", vel_x_sigma)+"  "+String.format("%8.5f", vel_y_sigma)+"  "+String.format("%8.5f", vel_z_sigma)+"  ";
            pbo=pbo+String.format("%6.3f", vel_rho_xy)+"  "+String.format("%6.3f", vel_rho_xz)+"  "+String.format("%6.3f", vel_rho_yz)+"  ";
            pbo=pbo+String.format("%8.5f", V_N)+"  "+String.format("%8.5f", V_E)+"  "+String.format("%8.5f", V_U)+"  ";
            pbo=pbo+String.format("%8.5f", SNd)+"  "+String.format("%8.5f", SEd)+"  "+String.format("%8.5f", SUd)+"  ";
            pbo=pbo+String.format("%6.3f", Rne)+"  "+String.format("%6.3f", Rnu)+"  "+String.format("%6.3f", Reu)+"  ";

            pbo=pbo+date1parsedC+"  "+date2parsedC+"\n";
        }

        return pbo;
    }
}
