package Glass;

import Configuration.SiteConfig;
import CustomClasses.*;
import org.json.simple.JSONArray;

import javax.ejb.EJB;
import javax.naming.InitialContext;
import javax.naming.NamingException;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriInfo;
import java.io.IOException;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


@SuppressWarnings("Duplicates")
@Path("files")
public class FilesV2 {

    @EJB
    private
    SiteConfig siteConfig = lookupSiteConfigBean();

    // =========================================================================
    //  FIELDS
    // =========================================================================
    @Context
    private UriInfo context;

    public FilesV2() {
        /* Gives access to network and connectivity */
        //String myIP = siteConfig.getLocalIpValue();
        //String DBConnectionString = siteConfig.getDBConnectionString();
    }

    @SuppressWarnings("Duplicates")
    private SiteConfig lookupSiteConfigBean() {
        try {
            javax.naming.Context c = new InitialContext();
            return (SiteConfig) c.lookup(GlassConstants.FRAMEWORKCONFIGNAME);
        } catch (NamingException ne) {
            Logger.getLogger(getClass().getName()).log(Level.SEVERE, "exception caught", ne);
            throw new RuntimeException(ne);
        }
    }

    /**
     * <b>GET FILES V2</b> - Returns the files(s) that match the criteria.
     *
     * <p>
     * <b>object_type - </b> the type of the criteria
     * <ul>
     *     <li>agency</li>
     *     <li>antenna</li>
     *     <li>city</li>
     *     <li>combination</li>
     *     <li>coordinates<b>º</b></li>
     *     <li>coordinates_data</li>
     *     <li>coordinates_data_list</li>
     *     <li>country</li>
     *     <li>date_range<b>ºº</b></li>
     *     <li>file_name</li>
     *     <li>format</li>
     *     <li>length</li>
     *     <li>marker</li>
     *     <li>md5</li>
     *     <li>name</li>
     *     <li>network</li>
     *     <li>published_date<b>ºº</b></li>
     *     <li>radome</li>
     *     <li>receiver</li>
     *     <li>sampling_frequency</li>
     *     <li>state</li>
     *     <li>station_date<b>ºº</b></li>
     *     <li>type</li>
     * </ul>
     *
     * <b>º</b> - this object uses "minLat=X&maxLat=X&minLon=X&maxLon=X" as it's object instance.
     * <b>ºº</b> - this objects use "date_from=X&date_to=X" as it's object instance.
     * </p>
     *
     * <p>
     * <b>object_instance - </b> the value of the criteria <i>(e.g. for country criteria may be France)</i>
     * </p>
     *
     *
     * <p>
     * <b>output_format - </b> the format of the output
     * <ul>
     *     <li>csv</li>
     *     <li>json</li>
     *     <li>script<b>ª</b></li>
     *     <li>xml</li>
     * </ul>
     *
     * <b>ª</b> - this format is not valid for all object_type
     * </p>
     *
     *
     * @param output_format xml, json, csv or script
     * @param object_type marker, country, network, ..
     * @param object_instance SOPH, Portugal, EUREF, ..
     * @return Files
     * @throws IOException
     */
    @Produces({MediaType.TEXT_PLAIN + ";charset=utf-8", MediaType.APPLICATION_JSON + ";charset=utf-8", MediaType.APPLICATION_XML + ";charset=utf-8", MediaType.APPLICATION_OCTET_STREAM + ";charset=utf-8"} )
    @Path("{object_type}/{object_instance}/{output_format}")
    @GET
    public Response Files(//@Context HttpHeaders headers,
                         //@HeaderParam("accept") String accept,
                         @PathParam("output_format") String output_format,
                         @PathParam("object_type") String object_type,
                         @PathParam("object_instance") String object_instance,
                         @DefaultValue("1")   @QueryParam("bad_files") String bad_files,
                         @DefaultValue("")   @QueryParam("page") String page,
                         @DefaultValue("")   @QueryParam("perpage") String perPage,
                         @DefaultValue("") @QueryParam("md5") String md5checksum) throws IOException, ParseException, SQLException, ClassNotFoundException {

        Stations stations = new Stations();
        String date_from = "", date_to = "";
        int pageNumber = 0, perPageNumber = 50;
        Tuple2<Integer, Integer> validate ;
        int bad_files_int;
        int total_pages = 0;

        try {
            bad_files_int = Validator.ValidateBadFileExcluder(bad_files);
            validate = Validator.ValidatePageEntries( page.trim(), perPage.trim() );
            pageNumber=validate.getFirst();
            perPageNumber=validate.getSecond();
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        if (object_type == null || object_instance == null)
            return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();

        Tuple2<Integer,String> r = new Tuple2<>();
        object_type = object_type.trim().toLowerCase();
        output_format = output_format.trim().toLowerCase();
        /*
         Return csv, xml, or script - need to be correctly written else json will be returned.
         */

        if(output_format.equalsIgnoreCase("url_list")) output_format = "script";
        switch(object_type){
            case "name":
                switch(output_format){
                case "csv":  r = stations.getFilesName(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("Current-Page", pageNumber)
                            .header("Total-per-Page", perPageNumber)
                            .header("Total-Elements", r.getFirst())
                            .header("Total-Pages", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesName(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("Current-Page", pageNumber)
                            .header("Total-per-Page", perPageNumber)
                            .header("Total-Elements", r.getFirst())
                            .header("Total-Pages", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesName(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("Current-Page", pageNumber)
                            .header("Total-per-Page", perPageNumber)
                            .header("Total-Elements", r.getFirst())
                            .header("Total-Pages", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesName(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("Current-Page", pageNumber)
                            .header("Total-per-Page", perPageNumber)
                            .header("Total-Elements", r.getFirst())
                            .header("Total-Pages", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }
                /* default type is json */
            case "station-marker":
            case "marker":
                switch(output_format){
                case "csv":
                    r = stations.getFilesMarker(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                        .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                        .header("Current-Page", pageNumber)
                        .header("Total-per-Page", perPageNumber)
                        .header("Total-Elements", r.getFirst())
                        .header("Total-Pages", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                        .build();
                case "xml":
                    r = stations.getFilesMarker(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                        .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                        .build();
                case "script":
                    r = stations.getFilesMarker(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                        .header("Content-Type", MediaType.TEXT_PLAIN)
                        .header("Current-Page", pageNumber)
                        .header("Total-per-Page", perPage)
                        .header("Total-Elements", r.getFirst())
                        .header("Total-Pages", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                        .build();
                default:
                    r = stations.getFilesMarker(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-content-type-options","nosniff")
                            .header("x-frame-options","SAMEORIGIN")
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "network": switch(output_format){
                case "csv":  r = stations.getFilesStationNetwork(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesStationNetwork(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesStationNetwork(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesStationNetwork(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "type": switch(output_format){
                case "csv":  r = stations.getFilesStationType(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesStationType(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesStationType(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesStationType(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "antenna": switch(output_format){
                case "csv":  r = stations.getFilesAntennaType(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesAntennaType(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesAntennaType(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesAntennaType(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "receiver": switch(output_format){
                case "csv":  r = stations.getFilesReceiverType(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesReceiverType(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesReceiverType(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesReceiverType(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "radome": switch(output_format){
                case "csv":  r = stations.getFilesRadomeType(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesRadomeType(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesRadomeType(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesRadomeType(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "country": switch(output_format){
                case "csv":  r = stations.getFilesCountry(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesCountry(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesCountry(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesCountry(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "state": switch(output_format){
                case "csv":  r = stations.getFilesState(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesState(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesState(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesState(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "city": switch(output_format){
                case "csv":  r = stations.getFilesCity(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesCity(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesCity(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesCity(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "agency": switch(output_format){
                case "csv":  r = stations.getFilesAgency(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesAgency(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesAgency(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesAgency(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "combination": switch(output_format){
                case "csv":  r = stations.getFilesCombination(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesCombination(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesCombination(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesCombination(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }
            
            case "combinationt3":
                switch(output_format){
                    case "csv":  r = stations.getFilesCombinationT3(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    case "xml":  r = stations.getFilesCombinationT3(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_XML)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    case "script":  r = stations.getFilesCombinationT3(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.TEXT_PLAIN)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    default:  r = stations.getFilesCombinationT3(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_JSON)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
            }

            case "sampling_frequency": switch(output_format){
                case "csv":  r = stations.getSamplingFrequency(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getSamplingFrequency(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getSamplingFrequency(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getSamplingFrequency(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "length": switch(output_format){
                case "csv":  r = stations.getLength(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getLength(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getLength(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getLength(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "coordinates_data_list":switch(output_format){
                case "csv":  r = stations.getFilesCoordinatesDataText(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesCoordinatesDataText(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesCoordinatesDataText(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesCoordinatesDataText(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "coordinates_data":switch(output_format){
                case "csv":  r = stations.getFilesCoordinatesData(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesCoordinatesData(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesCoordinatesData(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesCoordinatesData(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            /* default type is json */
            case "format":switch(output_format){
                case "csv":  r = stations.getFilesFormat(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesFormat(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesFormat(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesFormat(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "coordinates":
                String minLat="", maxLat="", minLon="", maxLon="";

                try{
                    String[] params = object_instance.toLowerCase().split("&");

                    if(params.length != 4)
                        throw new Exception("Invalid parameters[1]");

                    for(int i=0; i<4; i++){
                        if(params[i].contains("minlat"))
                            minLat = params[i];
                        else if(params[i].contains("maxlat"))
                            maxLat = params[i];
                        else if(params[i].contains("minlon"))
                            minLon = params[i];
                        else if(params[i].contains("maxlon"))
                            maxLon = params[i];
                        else
                            throw new Exception("Invalid parameters[2]");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
                }

                /* default type is json */
                switch(output_format){
                    case "csv": r =  stations.getFilesCoordinates(
                            Float.parseFloat(minLat.split("=")[1]),
                            Float.parseFloat(maxLat.split("=")[1]),
                            Float.parseFloat(minLon.split("=")[1]),
                            Float.parseFloat(maxLon.split("=")[1]),
                            "csv",
                            pageNumber,
                            perPageNumber,
                            bad_files_int
                            );
                           return Response.ok(r.getSecond())
                                   .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                                   .header("x-pagination-current-page", pageNumber)
                                   .header("x-pagination-per-page", perPage)
                                   .header("x-pagination-total-count", r.getFirst())
                                   .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                   .build();

                    case "xml":  r =  stations.getFilesCoordinates(
                            Float.parseFloat(minLat.split("=")[1]),
                            Float.parseFloat(maxLat.split("=")[1]),
                            Float.parseFloat(minLon.split("=")[1]),
                            Float.parseFloat(maxLon.split("=")[1]),
                            "xml",
                            pageNumber,
                            perPageNumber,
                            bad_files_int
                    );
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_XML)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    default: r =  stations.getFilesCoordinates(
                            Float.parseFloat(minLat.split("=")[1]),
                            Float.parseFloat(maxLat.split("=")[1]),
                            Float.parseFloat(minLon.split("=")[1]),
                            Float.parseFloat(maxLon.split("=")[1]),
                            "xml",
                            pageNumber,
                            perPageNumber,
                            bad_files_int
                                        );
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_JSON)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
            }

            // Reference date
            case "date_range":
                try{
                    String[] params = object_instance.toLowerCase().split("&");

                    if(params.length != 2)
                        throw new Exception("Invalid parameters[1]");

                    for(int i=0; i<2; i++){
                        if(params[i].contains("date_from"))
                            date_from = params[i].split("=")[1];
                        else if(params[i].contains("date_to"))
                            date_to = params[i].split("=")[1];
                        else
                            throw new Exception("Invalid parameters[2]");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
                }

                /* default type is json */
                switch(output_format){
                    case "csv":  r = stations.getFilesDateRange(date_from,date_to, "csv", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    case "xml":  r = stations.getFilesDateRange(date_from,date_to, "xml", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_XML)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    case "script":  r = stations.getFilesDateRange(date_from,date_to, "script", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.TEXT_PLAIN)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    default:  r = stations.getFilesDateRange(date_from,date_to, "json", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_JSON)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                }

            case "station_dates":
                try{
                    String[] params = object_instance.toLowerCase().split("&");

                    if(params.length != 2)
                        throw new Exception("Invalid parameters[1]");

                    for(int i=0; i<2; i++){
                        if(params[i].contains("date_from"))
                            date_from = params[i].split("=")[1];
                        else if(params[i].contains("date_to"))
                            date_to = params[i].split("=")[1];
                        else
                            throw new Exception("Invalid parameters[2]");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
                }

                /* default type is json */
                switch(output_format){
                    case "csv":  r = stations.getFilesStationDates(date_from,date_to, "csv", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    case "xml":  r = stations.getFilesStationDates(date_from,date_to, "xml", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_XML)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    case "script":  r = stations.getFilesStationDates(date_from,date_to, "script", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.TEXT_PLAIN)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    default:  r = stations.getFilesStationDates(date_from,date_to, "json", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_JSON)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                }

/* Change to published_date*/
            case "published_date":
                try{
                    String[] params = object_instance.toLowerCase().split("&");

                    if(params.length != 2)
                        throw new Exception("Invalid parameters[1]");

                    for(int i=0; i<2; i++){
                        if(params[i].contains("date_from"))
                            date_from = params[i].split("=")[1];
                        else if(params[i].contains("date_to"))
                            date_to = params[i].split("=")[1];
                        else
                            throw new Exception("Invalid parameters[2]");
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
                }

                /* default type is json */
                switch(output_format){
                    case "csv":  r = stations.getFilesDatePublished(date_from,date_to, "csv", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    case "xml":  r = stations.getFilesDatePublished(date_from,date_to, "xml", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_XML)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    case "script":  r = stations.getFilesDatePublished(date_from,date_to, "script", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.TEXT_PLAIN)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    default:  r = stations.getFilesDatePublished(date_from,date_to, "json", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_JSON)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                }

            /* default type is json */
            case "md5": switch(output_format){
                case "csv":  r = stations.getFilesMD5(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesMD5(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesMD5(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesMD5(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "file_name": switch(output_format){
                case "json":
                    if(!md5checksum.equals("")){
                        r = stations.getFilesFileNameWithMd5(object_instance, md5checksum, "json", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_JSON)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    }
                    r = stations.getFilesFileName(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                return Response.ok(r.getSecond())
                        .header("Content-Type", MediaType.APPLICATION_JSON)
                        .header("x-pagination-current-page", pageNumber)
                        .header("x-pagination-per-page", perPage)
                        .header("x-pagination-total-count", r.getFirst())
                        .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                        .build();
                case "xml":
                    if(!md5checksum.equals("")){
                        r = stations.getFilesFileNameWithMd5(object_instance, md5checksum, "xml", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.APPLICATION_XML)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    }
                    r = stations.getFilesFileName(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "csv":
                    if(!md5checksum.equals("")){
                        r = stations.getFilesFileNameWithMd5(object_instance, md5checksum, "csv", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    }
                    r = stations.getFilesFileName(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();

                case "script":
                    if(!md5checksum.equals("")){
                        r = stations.getFilesFileNameWithMd5(object_instance, md5checksum, "script", pageNumber, perPageNumber, bad_files_int);
                        return Response.ok(r.getSecond())
                                .header("Content-Type", MediaType.TEXT_PLAIN)
                                .header("x-pagination-current-page", pageNumber)
                                .header("x-pagination-per-page", perPage)
                                .header("x-pagination-total-count", r.getFirst())
                                .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                                .build();
                    }
                    r = stations.getFilesFileName(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default: r = stations.getFilesFileName(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            case "data_center_acronym": switch(output_format){
                case "csv":  r = stations.getFilesbyAcronymDataCenter(object_instance, "csv", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", GlassConstants.TEXT_CSV_TYPE)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "xml":  r = stations.getFilesbyAcronymDataCenter(object_instance, "xml", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_XML)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                case "script":  r = stations.getFilesbyAcronymDataCenter(object_instance, "script", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.TEXT_PLAIN)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
                default:  r = stations.getFilesbyAcronymDataCenter(object_instance, "json", pageNumber, perPageNumber, bad_files_int);
                    return Response.ok(r.getSecond())
                            .header("Content-Type", MediaType.APPLICATION_JSON)
                            .header("x-pagination-current-page", pageNumber)
                            .header("x-pagination-per-page", perPage)
                            .header("x-pagination-total-count", r.getFirst())
                            .header("x-pagination-page-count", Validator.calculateTotalPages(r.getFirst(),perPageNumber))
                            .build();
            }

            default: Response.status(Response.Status.BAD_REQUEST).entity("Invalid object type.").build();
        }

        return Response.status(Response.Status.BAD_REQUEST).entity("Bad request.").build();
    }

    /*
    From the swagger
    date_from/to of the files, including. Format YYYY-MM-DD e.g. 2020-01-01
     */
    @Path("info")
    @GET
    public Response FilesInfo(
            @DefaultValue("") @QueryParam("marker_long_name") String marker_long_name,
            @DefaultValue("") @QueryParam("status") String status,
            @DefaultValue("") @QueryParam("date_from") String date_from,
            @DefaultValue("") @QueryParam("date_to") String date_to,
            @DefaultValue("") @QueryParam("datacenter_acronym") String datacenter_acronym,
            @DefaultValue("")   @QueryParam("page") String page,
            @DefaultValue("")   @QueryParam("perpage") String perPage
    ) {
        int pageNumber = 0, perPageNumber = 50;
        List<Integer> statusInts = new ArrayList<>();
        Tuple2<Integer, Integer> validate ;

        try {
            validate = Validator.ValidatePageEntries( page.trim(), perPage.trim() );
            pageNumber=validate.getFirst();
            perPageNumber=validate.getSecond();
            //date validations
            date_from = date_from.trim();
            date_to = date_to.trim();
            if (!date_from.isEmpty()) {
                Validator.ValidateLocalDateFormat(date_from);
            }
            if (!date_to.isEmpty()) {
                Validator.ValidateLocalDateFormat(date_to);
            }

            // status validation
            if (!status.trim().equals("")) {
                statusInts = Validator.ValidateStatus(status);
            }

        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        DataBaseT2Connection dbt2c = new DataBaseT2Connection();
        ArrayList<FilesInfo> raw_result;
        try {
            raw_result = dbt2c.getFilesInfo(marker_long_name, statusInts,datacenter_acronym, date_from, date_to,pageNumber,perPageNumber);
        } catch (OutOfMemoryError e){
            return Response.serverError().entity("Server has no available resources to process this. Try limiting the conditions.").build();
        }

        if(raw_result.isEmpty()){
            return Response.ok("[]", MediaType.APPLICATION_JSON).build();
        }

        ParsersV2 parser = new ParsersV2();

        String result = parser.getFilesInfoJSON(raw_result);

        return Response.ok(result, MediaType.APPLICATION_JSON).build();
    }

    /*
    End Point to return rinex files with a QC report containing a NULL multipath
    Inputs
    marker (9 char)
    Optional : ?date_from=yyyy-mm-dd2&date_to=yyyy-mm-dd
    Optional pageNumber -->
    page number using 1-based indexing 0 is the default and means get everything
    perpage number of records per page  Default value : 50
    Returns json on successful processing
     */
    @Produces({ MediaType.APPLICATION_JSON + ";charset=utf-8"})
    @Path("/QC/MultiPathNULL/{marker}")
    @GET
    public Response QCMultiPathZero(
                          @PathParam("marker") String markerIn,
                          @DefaultValue("")   @QueryParam("page") String page,
                          @DefaultValue("")   @QueryParam("perpage") String perPage,
                          @DefaultValue("")   @QueryParam("date_from") String date_from,
                          @DefaultValue("")   @QueryParam("date_to")    String date_to) {

        int pageNumber = 0, perPageNumber = 50;
        String marker ="";
        String dates = "";
        Tuple2<Integer, Integer> validate ;
        try  {
            marker = Validator.ValidateMarker9(markerIn);  /* Marker must be 9chars - no Wildcards */
            validate = Validator.ValidatePageEntries( page.trim(), perPage.trim() );
            pageNumber=validate.getFirst();
            perPageNumber=validate.getSecond();

            date_from=date_from.trim();
            date_to=date_to.trim();
            if (!date_from.isEmpty()) {
                Validator.ValidateLocalDateFormat(date_from);
                dates += " and rf.reference_date >= '" + date_from + "' ";
            }
            if (!date_to.isEmpty()) {
                Validator.ValidateLocalDateFormat(date_to);
                dates += " and  rf.reference_date <= '" + date_to + "' ";
            }
        } catch (ValidatorException e) {
            return Response.status(e.responseStatus).entity(e.getMessage()).build();
        }

        JsonCache myCache = new JsonCache();
        List<markerinfo> marker_and_id;
        try {
            marker_and_id = myCache.getMarkerandKeyByValue(marker);
        } catch (IOException ex) {
            // 500 Internal Server Error
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity( "Cache Lookup Failed\n"+ex.getMessage()).build();
        }

        if (marker_and_id.size() != 1)
            return Response.status(Response.Status.BAD_REQUEST).entity("{ \"Invalid Request\":\" "+ marker +": marker not found or must be unique 9char\" }").build();

        int id = marker_and_id.get(0).id;

        DataBaseT3QCConnection dbtcqc = new DataBaseT3QCConnection();

        try {
            ArrayList res = dbtcqc.getFilesT3QCcodmpth(id, dates, pageNumber, perPageNumber);
            Parsers parser = new Parsers();
            JSONArray obj = parser.getFileT3FullJSON(res);
            return Response.ok(obj.toJSONString(), MediaType.APPLICATION_JSON).build();
        }catch (Exception ex){
            return Response.status(Response.Status.INTERNAL_SERVER_ERROR).entity(ex.getMessage()).build();
        }
    }
}
