package Glass;

/*
*****************************************************
**                                                 **
**      Created by Andre Rodrigues on 11/8/17      **
**                                                 **
**      andre.rodrigues@ubi.pt                     **
**                                                 **
**      Covilha, Portugal                          **
**                                                 **
*****************************************************
*/

import EposTables.Estimated_coordinates;
import org.json.simple.JSONArray;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.sql.SQLException;
import java.util.ArrayList;

public class ScriptTimeSeries {


    /**
     * Creates timeseries file for specific station and set of properties
     * @param coordinates_system
     * @param site
     * @param agency
     * @param sampling_period
     * @param format
     * @param reference_frame
     * @param otl_model
     * @param antenna_model
     * @param cut_of_angle
     * @param epoch_start
     * @param epoch_end
     * @param remove_outliers
     * @param apply_offsets
     * @param data_type
     * @return
     * @throws SQLException
     */
    public static boolean ScriptCreatetimeseries(String coordinates_system, String site, String agency, String sampling_period, String format, String reference_frame, String otl_model, String antenna_model, String cut_of_angle, String epoch_start, String epoch_end, String remove_outliers, String apply_offsets, String data_type) throws Exception {

        DataBaseT4Connection dbc = new DataBaseT4Connection();
        // Turn result to JSON array
        ArrayList<Estimated_coordinates> res = null;
        // TODO: handle version on this script
        String version="";


        switch (coordinates_system) {
            case "xyz":
                res = dbc.getTimeseriesXYZBasedOnParameters(site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries", version);
                break;
            case "enu":
                res = dbc.getTimeseriesENUBasedOnParameters(site, agency, sampling_period, format, reference_frame, otl_model, antenna_model, cut_of_angle, epoch_start, epoch_end, remove_outliers, apply_offsets, "timeseries", version);
                break;
        }

        if (res.isEmpty()) {
            return false;
        }

        Parsers parser = new Parsers();


        if (!new java.io.File("/opt/EPOS/timeseries/"+site+"/"+ agency +"/"+ sampling_period +"/"+ coordinates_system+"/").exists()) {
            (new java.io.File("/opt/EPOS")).mkdir();
            (new java.io.File("/opt/EPOS/timeseries")).mkdir();
            (new java.io.File("/opt/EPOS/timeseries/"+site)).mkdir();
            (new java.io.File("/opt/EPOS/timeseries/"+site+"/"+ agency)).mkdir();
            (new java.io.File("/opt/EPOS/timeseries/"+site+"/"+ agency +"/"+ sampling_period)).mkdir();
            (new java.io.File("/opt/EPOS/timeseries/"+site+"/"+ agency +"/"+ sampling_period+"/"+ coordinates_system)).mkdir();
        }

        switch (format) {

            case "xml":
                try {
                    try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                            new FileOutputStream("/opt/EPOS/timeseries/" +
                                    site +"/"+ agency +"/"+ sampling_period +"/"+ coordinates_system +"/"+ format +"-"+ reference_frame +"-"+ otl_model +"-"+
                                    antenna_model +"-"+ cut_of_angle +"-"+ epoch_start +"-"+ epoch_end +"-"+ remove_outliers +"-"+
                                    apply_offsets +"."+format), StandardCharsets.UTF_8))) {
                        writer.write(parser.getFullXMLTimeSeries(res));
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            case "json":
                JSONArray obj = new JSONArray();

                switch (coordinates_system) {
                    case "xyz":
                        try {
                            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                                    new FileOutputStream("/opt/EPOS/timeseries/" +
                                            site +"/"+ agency +"/"+ sampling_period +"/"+ coordinates_system +"/"+ format +"-"+ reference_frame +"-"+ otl_model +"-"+
                                            antenna_model +"-"+ cut_of_angle +"-"+ epoch_start +"-"+ epoch_end +"-"+ remove_outliers +"-"+
                                            apply_offsets +"."+format), StandardCharsets.UTF_8))) {
                                writer.write(String.valueOf(parser.getTimeSeriesByFormatJSON(res)));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                    case "enu":
                        try {
                            try (Writer writer = new BufferedWriter(new OutputStreamWriter(
                                    new FileOutputStream("/opt/EPOS/timeseries/" +
                                            site +"/"+ agency +"/"+ sampling_period +"/"+ coordinates_system +"/"+ format +"-"+ reference_frame +"-"+ otl_model +"-"+
                                            antenna_model +"-"+ cut_of_angle +"-"+ epoch_start +"-"+ epoch_end +"-"+ remove_outliers +"-"+
                                            apply_offsets +"."+format), StandardCharsets.UTF_8))) {
                                writer.write(String.valueOf(parser.getTimeSeriesByFormatENUJSON(res)));
                            }
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                        break;
                }
        }
        return true;
    }

}