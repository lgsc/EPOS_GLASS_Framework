## README

# Procedure

1 - The GLASS.conf should be modified awith your local node credentials and inserted in the config folder of the glassfish domain.

2 - Update the service agreement html file to display information about the stations that the node is willing to host.

3 - Update the swagger.json so that the server addresses match you local installation.

4 - The .war file should be compiled again once the service agreement and swagger.json are modified accordingly to your preferences. 

The command is :

jar -uvf GlassFramework.war swagger.json serviceagreement.html

5 - The war file may then be deployed on the server.


# Compatibility

- Compatible with version gnss-europe-v1.3.0 of the Database (https://gitlab.com/gpseurope/database)
