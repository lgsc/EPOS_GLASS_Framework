# EPOS Glass Framework

This document describes the usage of the Glass Framework developed for EPOS-IP WP10 by the University of Beira Interior. 

GLASS (Geodetic Linkage Advanced Software System) is an Integrated software package that is deployed in a GNSS infrastructures contributing to EPOS in order to manage GNSS data and metadata.

This API is implemented in JavaEE using the Maven Framework support and Galssfish as the reference web Server. It handles all the requests that retrive data and metadata from the [**Data Gateway (DGW)**](https://gitlab.com/gpseurope/GLASS-web-client) and the [**Products Gateway (PGW)**](https://gitlab.com/segalubi/Products_Portal) using the [**GNSS-Europe** database](https://gitlab.com/gpseurope/database) and the Local Glass Nodes. Data insertion is handled by a seperate application ([fwss](https://gitlab.com/gpseurope/fwss))

**The software and this accompanying manual are work in progress and may contain bugs, errors and omissions.**

**Contributors:**
 - Paul Crocker (crocker@segal.ubi.pt)
 - Luis Carvalho
 - Fernando Geraldes
 - Nuno Pedro

 **Former Contributors:**
 - Pedro Pereira
 - André Rodrigues
 - José Manteigueiro 
 - Khai-Minh Ngo
  - Rafael Couto 

 **Document created:** 12. February 2018

**Last update:** 12. May 2023

# 1. Introduction

## 1.1 Current version
The current version of the Glass Framework is [release 1.6.0](https://gitlab.com/gpseurope/EPOS_GLASS_Framework/tags/1.6.0). This documentation should reflect the status of the system in this version.

## 1.2 Current version of the database
To use the Glass Framework you will need to set up a database. The current version of the API is compatible with the version [v1.3](https://gitlab.com/gpseurope/database/tags/v1.3.0) of the [**GNSS-Europe** database](https://gitlab.com/gpseurope/database).

## 1.3 Files in the Project

Java source Folders *(src/main/java)*:

	- Configuration - Contains the configuration files where database connection properties are loaded and where current build data can be retrieved;
	- CustomClasses - Contains classes that do not directly reflect the database scheme structure but a product of n-tables combination;
	- EposTables ---- Contains classes that represent the tables in the database;
	- Geometry ------ Contains classes to assert geo-referenced points inside defined borders using various shapes
	- Glass --------- Contains classes with all the requests and methods that are part of the API; 

Other files and folders:

	- web/ ---- Folder with the web interface for the node management platform.
	- pom.xml - Contains the Maven dependencies.

# 2. Installation and Configuration

## 2.1 Java OpenJDK
Please install Java 1.8 OpenJDK in order to be able to compile the project in further step.
For linux distros with yum package manager:

	$ sudo yum -y install java-1.8.0-openjdk 
	$ sudo yum -y install java-1.8.0-openjdk-devel

For linux distros with aptitude package manager:
    
	$ sudo apt-get install openjdk-8-jre
	$ sudo apt-get install java-1.8.0-openjdk-devel

## 2.2 Web Application Server 

## 2.2.1 Payara 

Currently using version 5 community edition from https://repo1.maven.org/maven2/fish/payara/distributions/payara/5.2022.5/payara-5.2022.5.zip as the default installation.

A new version for Payara 6 community edition will be available shortly. 

More information about payara server: https://www.payara.fish/learn/getting-started-with-payara/

## 2.2.2 Oracle GlassFish.

Use Version 4 or 5

Example

 1. Download GlassFish:
 
		$ wget http://download.oracle.com/glassfish/4.1.2/release/glassfish-4.1.2.zip

 2. Extract the GlassFish archive:
 
		$ unzip glassfish-4.2.1.zip

 4. Save the extracted folder in a directory of your choice.


## 2.3 Maven

If you are compiling the source code then Please install Maven 3.5.2 in order to be able to compile the GlassFramework API on your local machine.

Please refer to: https://maven.apache.org/

## 2.4 Database 

Please configure the **GNSS-Europe** database [v1.3.0](https://gitlab.com/gpseurope/database/tags/v1.3.0) in your postgreSQL local installation.

After creation and configuration of the database make sure you supply a valid user to the system. If you wish to add a new user, you can use the following SQL statements to create one.
    
    CREATE ROLE <username> WITH LOGIN ENCRYPTED PASSWORD '<password>';
    GRANT select, insert, update, delete, trigger ON ALL TABLES IN SCHEMA public TO <username>;
    GRANT ALL ON ALL SEQUENCES IN SCHEMA public TO <username>;

*NOTE: make sure to replace `<username>` and `<password>` by desired username and password, respectively*

Please refer to: https://www.postgresql.org/
and to: https://gitlab.com/gpseurope/database 

## 2.5 Compile and deploy

There are two options. Most users will want to use the pre-compiled packaged solution, a Web Archive (war) file, however otheres may wish to compile the source code using maven.

### 2.5.1 Use available packaged solution

You can use a packaged version of GLASS-API which is already included in this repository in the `dist` folder. In order to install such package you'll have to:

 1. To connect with the database the GLASS.conf file, included in the `dist` folder, should be customized and moved to **<PATH_TO_GLASSFISH>/glassfish/domains/[YOUR_DOMAIN_(usually domain1)]/config/**.

 2. To enable GLASS-API to send emails to users the GLASS.conf file, included in the `dist` folder, should also be customized and moved to **<PATH_TO_GLASSFISH>/glassfish/domains/[YOUR_DOMAIN_(usually domain1)]/config/**.

 3. Other GLASS.conf file options may be ignored - see more below for more details
 
 4. Customize site features in  the .war file : Please see https://gitlab.com/gpseurope/EPOS_GLASS_Framework/-/tree/master/dist  for more details.   

 5. To deploy, use the admin console or simply move the `.war` file included in the `dist` folder to the `autodeploy` folder of your **Glassfish** installation:

	    $ mv <WAR_FILE> <PATH_TO_GLASSFISH>/glassfish/domains/[YOUR_DOMAIN_(usually domain1)]/autodeploy


### 2.5.2 Compile Manually

 1. In the `/GlassFramework` folder run the maven packaging goal to create the `.war` file.

		$ mvn package


### 2.5.3 The Glass.conf file

 2. To connect with the database a GLASS.conf file should be created inside the folder **<PATH_TO_GLASSFISH>/glassfish/domains/[YOUR_DOMAIN_(usually domain1)]/config/**. The file should look like the following: 
  
		# DATABASE
		dbip=[DATABASE_SERVER_IP]
		dbport=[DATABASE_PORT]
		database=[DATABASE_NAME]
		dbuser=[DATABASE_USERNAME]
		dbpassword=[DATABASE_PASSWORD]

 3. To enable GLASS-API to send emails to users the same GLASS.conf file should also include the following: 

		# EMAIL
		username=[EMAIL_ADDRESS]
		password=[EMAIL_PASSWORD]
		smtpserver=[SMTP_SERVER]
		port=587
		debug=false
 
 4. For it to work with Log files, the following lines in the same GLASS.conf should also be edited:

		# GEODESY / LOG PATH

		## Path to the metadata directory ( /srv/epos/metadata/sitelogs => /srv/epos )
		filepath=/srv/epos

		## Relative path to sitelogs inside filepath ( /srv/epos/metadata/sitelogs => metadata/sitelogs )
		sitelogs_relative_path=metadata/sitelogs

		## If logs are using the markerlongname ( e.g. CASC00PRT.log )
		## true OR false
		log_markerlongname=false

		## If logs are in lowercase ( e.g. casc00.log / casc00prt.log )
		## true OR false
		log_lowercase=true


 5. Move the generated `.war` file (check output of previous command) to the `autodeploy` folder of your **Glassfish** installation:

	    $ mv <GENERATED_WAR_FILE> <PATH_TO_GLASSFISH>/glassfish/domains/[YOUR_DOMAIN_(usually domain1)]/autodeploy

 6. Start the GlassFish server by running the following command:

		$ <PATH_TO_GLASSFISH>/bin/asadmin start-domain

 7. After successful start of GlassFish, go to your browser and navigate to the link below were you should now see the node management platform.

	    http://<SERVER_IP>:8080/GlassFramework

# 3. Useful tips

 - Use the *View Requests* options from the node management tool to check all the available requests this API offers

## License

This project is licensed under a [Creative Commons Attribution-ShareAlike 4.0 International License](https://creativecommons.org/licenses/by-sa/4.0/). 

## Acknowledgments

* This project has received funding from the European Union's Horizon 2020 research and innovation program under grant agreement N° 676564


